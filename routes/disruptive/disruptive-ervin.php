<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Disruptive;
use App\Http\Controllers\PerifericoController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

Route::get('/disruptive/ervin', function () {
    return view('disruptive.faq.index');
});

//rutas para vistas user disruptive
Route::prefix('user')->middleware(['auth', 'user', 'verify_user'])->name('user.')->group(function () {
    //ruta para home dashboard index
    Route::get('/', [Disruptive\DashboardController::class, 'index'])->name('home');

    //ruta para home dashboard index paneles de prueba
    Route::get('/panel/{panel}', [Disruptive\DashboardController::class, 'panelesTest'])->name('home.test');

    //ruta para top referrals index
    Route::get('/top-referrals', [Disruptive\TopReferralsController::class, 'index'])->name('top-referrals');

    //ruta para FaQ index
    Route::get('/frequentquestions', [Disruptive\FAQController::class, 'index'])->name('listado.faq');

    //ruta para bonus index
    Route::get('/bonus', [Disruptive\BonusController::class, 'index'])->name('bonus');

    //ruta para cooperativa index
    Route::get('/cooperativa', [Disruptive\CooperativaController::class, 'index'])->name('cooperativa');

    //ruta para f101 index
    Route::get('/f101', [Disruptive\F101Controller::class, 'index'])->name('f101');

    //ruta para f101 index
    Route::get('/maps', [Disruptive\MapsController::class, 'index'])->name('maps');

    //ruta para progress-token index
    Route::get('/progress', [Disruptive\ProgressTokenController::class, 'index'])->name('progress');
});

//ruta para ver los estados de los servicios api
Route::get('/services/status/', [PerifericoController::class, 'statusServicesPublic'])->name('statusServices');

//rutas para vistas admin disruptive
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    //ruta para vista graficos vhartjs
    Route::get('/analytics', [Admin\AnalyticController::class, 'index'])->name('analytics');

    //ruta para servicios admin
    Route::get('/services/status', [PerifericoController::class, 'statusServices'])->name('statusServices');
    //ruta para vista grafico tipo de pagos
    Route::get('/pagos', [Admin\Analytics\TiposPagosController::class, 'index'])->name('tipoPagos');

    //ruta para vista grafico ventas de paquetes
    Route::get('/package', [Admin\Analytics\VentaPackagesController::class, 'index'])->name('package');

    //ruta para vista grafico ingrsos
    Route::get('/ingresos', [Admin\Analytics\IngresosController::class, 'index'])->name('ingresos');

    //ruta para vista grafico ingrsos
    Route::get('/bonus', [Admin\Analytics\BonusUsdController::class, 'index'])->name('bonusUsd');
});
