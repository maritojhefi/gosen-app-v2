<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\Country;
use App\Models\Paquete;
use App\Models\Setting;
use App\Models\LogsGosen;
use App\Models\Transaction;
use App\Helpers\GosenHelper;
use App\Models\BonusReferral;
use Orhanerday\OpenAi\OpenAi;
use App\Events\MapAdminUsersEvent;
use App\Mail\WelcomeDisruptiveMail;
use App\Http\Controllers\Disruptive;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\BonusDollarController;
use App\Http\Controllers\Livewire\Admin\WhatsappCorreoComponent;
use App\Http\Controllers\Livewire\Admin\WhatsappReportsComponent;
use App\Http\Controllers\Livewire\User\NftUserGeneratedComponent;
use App\Http\Controllers\Livewire\Admin\MailsUserBlockedComponent;
use App\Http\Controllers\Livewire\Admin\WhatsappCorreoListComponent;
use App\Http\Controllers\Livewire\Admin\WhatsappConversationsComponent;

Route::get('/disruptive/rodrick', function () {
    return 'Hola rodrick, esta ruta solo se ve desde el inquilino disruptive, mas no desde la central';
});
Route::get('/user/back/{userId}', [PerifericoController::class, 'autoLogInBedesk'])->name('user.autoLogInBedesk');
Route::prefix('user')->middleware(['auth', 'user', 'verify_user'])->name('disruptive.')->group(function () {
    //Aqui van todas sus rutas
    Route::get('/referral', [Disruptive\ReferralController::class, 'index'])->name('referrals');
    Route::get('/package', [Disruptive\PackagesController::class, 'index'])->name('package');
    Route::get('/buy/package/{package}/{pack}', [Disruptive\PackagesController::class, 'buy'])->name('buy.package');
    Route::get('/profile/{var}', [Disruptive\ProfileController::class, 'account'])->name('user.profile');
    Route::get('/package/info', [PerifericoController::class, 'packageInfo'])->name('package.info');
    Route::get('/confirm/dolars-withdraw/{id}', [UserController::class, 'confirmUsdWithdraw'])->name('confirm.withdraw.usd');
    Route::post('/user/gift', [Disruptive\DashboardController::class, 'firstLogin'])->name('first.login');
});
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    Route::get('/disruptive/settings/', [SettingController::class, 'settingDisruptive'])->name('disruptive.settings');
    Route::post('/disruptive/settings/status/progress/{tipo}/{card}', [SettingController::class, 'changeStatus']);
    Route::get('/admin/permissions', [SettingController::class, 'adminInfo'])->name('permission');
    Route::get('/bonus/withdraw/{filtro}', [BonusDollarController::class, 'WithdrawBonusDollars'])->name('withdraw');
    Route::post('/excel/withdraw/filter', [UsersController::class, 'exportarWithdrawExcel'])->name('excel.withdraw');
    Route::get('/whatsapp/user', WhatsappReportsComponent::class)->name('whatsapp.user.reports');
    Route::get('/mail-whatsapp/create', WhatsappCorreoComponent::class)->name('mail.whatsapp.user');
    Route::get('/mail-whatsapp/list/scheduled', WhatsappCorreoListComponent::class)->name('mail.whatsapp.scheduled');
    Route::get('/mail/blocked', MailsUserBlockedComponent::class)->name('mails-blocked');
    Route::get('/whatsapp/conversations/users', WhatsappConversationsComponent::class)->name('conversations.whatsapp');
});
Route::get('/package', [Disruptive\PackagesController::class, 'index'])->middleware('auth', 'user', 'verify_user')->name('package');
Route::middleware([])->group(function () {
    Route::get('/term/condition', [PerifericoController::class, 'termCondition'])->name('terms');
    Route::get('/privacy/policy', [PerifericoController::class, 'privacyPolicy'])->name('privacy');
    Route::get('/crear/cookies/disruptive', [PerifericoController::class, 'crearCookies']);
});
Route::get(
    '/crear/pais/prueba',
    function () {
        Country::create([
            'name' => 'Rusia',
            'latitude' => '61.52401',
            'longitude' => '105.318756',
            'users' => '55',
            'flag' => 'https://static.vecteezy.com/system/resources/previews/002/431/839/original/illustration-of-the-cuba-flag-free-vector.jpg',
        ]);
        $array = GosenHelper::mapaGeneralUsuariosPorPais();
        event(new MapAdminUsersEvent($array));
    }
);
Route::get('/change/value', function () {
    $value = Setting::where('field', 'pmc_fx_exrates')->first();
    $value->value = '{"currencies":{"ADA":2.467,"AUD":1.553,"BCH":0.008725,"BNB":0.003129,"BRL":5.21,"BTC":4.897e-5,"CAD":1.356,"CHF":0.9956,"CLP":964.03,"COP":4648.5,"CZK":24.62,"DKK":8.549,"EGP":27.89,"ETH":0.0006333,"EUR":1.007,"GBP":0.8666,"HKD":7.819,"HUF":407.22,"IDR":15612.56,"INR":82.94,"JMD":184.17,"JPY":147.52,"KES":124.03,"LTC":0.01823,"MXN":19.69,"MYR":4.714,"NAD":21.52,"NGN":827.88,"NOK":10.28,"NZD":1.701,"PHP":57.49,"PKR":161.18,"PLN":4.738,"RUB":61.23,"SEK":11.78,"SGD":1.401,"SOL":0.03061,"THB":37.73,"TRX":15.96,"TRY":18.79,"UNI":0.1364,"XLM":9.091,"XMR":0.006672,"XRP":2.188,"ZAR":18.31,"BUSD":0.9999,"CAKE":0.219,"DASH":0.02411,"DOGE":7.257,"LINK":0.1299,"USDC":1,"USDT":1,"WAVES":0.2967,"USD":1,"BN.USD":1}}';
    $value->save();
});
Route::get(
    '/prueba/web/tipos',
    function () {
        $user = User::create([
            'username' => 'alberto9874',
            'name' => 'alberto',
            'email' => 'albertin',
            'email_verified_at' => Carbon::now(),
            'password' => '$2y$10$DVi9Daz.oRZts5fvq8ovA.sEzAlEy0G0L7uLa4T4XEdQUBjt8thPO',
            'status' => 'active',
            'registerMethod' => 'Internal',
            'social_id' => null,
            'mobile' => null,
            'dateOfBirth' => null,
            'nationality' => 'Bolivia',
            'lastLogin' => Carbon::now(),
            'walletType' => null,
            'walletAddress' => null,
            'role' => 'user',
            'contributed' => null,
            'tokenBalance' => null,
            'referral' => 18,
            'referralInfo' => '{"user":18,"name":"Rodri Gomez","time":"2022-10-28T19:47:28.343613Z"}',
            'google2fa' => 0,
            'google2fa_secret' => null,
            'type' => 'main',
            'remember_token' => null,
        ]);
        GosenHelper::registerLocation($user);
    }
);
Route::get('/prueba/generar/nft/dall-e', function () {
    // $prompt = "aliens del area 53 en un caballo";
    // $cantidad = 1;
    // try {
    //     $open_ai = new OpenAi(env('OPEN_AI_KEY', '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'));
    //     $response = $open_ai->image([
    //         "prompt" => $prompt,
    //         "n" => $cantidad,
    //         //número de imágenes
    //         "size" => "512x512",
    //         //dimensiones de la imagen
    //         "response_format" => "b64_json" //use "url" for less credit usage
    //     ]);
    //     $response = json_decode($response);
    //     dd($response);
    //     // return $response;
    // } catch (\Throwable $th) {
    //     LogsGosen::create([
    //         'titulo' => 'Dall-e Error ',
    //         'log' => $th->getMessage()
    //     ]);
    //     dd($th->getMessage());
    //     // return $th->getMessage();
    // }
});
Route::get(
    '/prueba/email/staging/disruptive',
    function () {
        dd(Cache::get('records_cache_key'));

        // $fechaConParametro = GosenHelper::iniciofindesemana(2);
        // $fechaDesdeeDomingo = GosenHelper::iniciofindesemana();
        // dd($fechaConParametro, $fechaDesdeeDomingo);
        // // $bonusReferral = BonusReferral::find(3);
        // // $icono = GosenHelper::getIconGiftUserReferrals($bonusReferral->regalo_tipo_id);
        // // Mail::to($user->email)
        // //     ->send(new WelcomeDisruptiveMail($user));
        // $transaction = Transaction::find(16);
        // // Divide la cadena en partes usando ":" como delimitador
        // $encontrartrans = explode(':', $transaction->details);
        // // El número estará en el segundo elemento del array resultante
        // $transactionNft = Transaction::find(trim($encontrartrans[1]));
        // return view('mail.layout-2022.mailBonusStagingUser', compact('transaction', 'transactionNft'));
    }
);
Route::get(
    '/prueba/email/request/transaction/buy',
    function () {
        // $tnx = Transaction::find(500);
        // $user = User::where('id', $tnx->user)->first();
        // $sigla = Setting::where('field', 'token_symbol')->first();

        // $tc = new App\Helpers\TokenCalculate();
        // $btcc = 'btc';
        // $token_prices = $tc->calc_token(1, 'price');
        // $curre = $tnx->currency;
        // $precioUsd = $tnx->amount;
        // $btcPrice = $tnx->amount;

        // $paquete = Paquete::find($tnx->package);
        // $priceInUsd =  (($paquete->valor_usd * ($paquete->descuento) / 100) == 0 ? $paquete->valor_usd : ($paquete->valor_usd - ($paquete->valor_usd * ($paquete->descuento) / 100))) . ' ' . 'USD';
        // return view('mail.layout-2022.mailRequestTransaction', compact('priceInUsd', 'tnx', 'user', 'sigla', 'btcPrice', 'precioUsd',));
    }
);

Route::get(
    '/login/user',
    function () {
        Auth::loginUsingId(92);
    }
);
Route::get(
    '/login/admin',
    function () {
        Auth::loginUsingId(1);
    }
);