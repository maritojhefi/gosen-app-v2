<?php

use Carbon\Carbon;
use App\Models\User;
use GuzzleHttp\Client;
use App\Models\Setting;
use App\Helpers\GosenHelper;
use App\Models\PaymentMethod;
use App\Events\NotificationEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OpenAIController;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\Livewire\BonusStaggingComponent;
use App\Http\Controllers\Livewire\User\NotificationsListComponent;


Route::get('/disruptive/mario', function () {
    return 'Hola mario, esta ruta solo se ve desde el inquilino disruptive, mas no desde la central';
});

Route::get('/register/login', [PerifericoController::class, 'login'])->name('register.login');
Route::prefix('user')->middleware(['auth', 'user'])->group(function () {
    //Aqui van todas sus rutas
    Route::get('/notifications/all', NotificationsListComponent::class)->name('all.notifications');
});
Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::post('/guardar/usuarios/bloqueados', [PerifericoController::class, 'guardarBloqueados'])->name('guardar.paginasBloqueadas');

    Route::get('/bonus/stagging/index', BonusStaggingComponent::class)->name('admin.bonus.stagging.list');
});
Route::get('/actualizar/setting/rates', function () {
    DB::table('settings')->where('field', 'pmc_fx_exrates')->update(['value' => '{"currencies":{"ADA":2.856,"AUD":1.503,"BCH":0.00979,"BNB":0.003486,"BRL":5.36,"BTC":5.927e-5,"CAD":1.328,"CHF":0.9497,"CLP":884.16,"COP":4853.6,"CZK":24.02,"DKK":7.335,"EGP":26.6,"ETH":0.0007947,"EUR":0.9728,"GBP":0.8518,"HKD":7.841,"HUF":382.84,"IDR":15686.53,"INR":81.29,"JMD":222.93,"JPY":140.08,"KES":132.72,"LTC":0.01648,"MXN":19.56,"MYR":4.613,"NAD":23.71,"NGN":704.46,"NOK":9.96,"NZD":1.647,"PHP":56.78,"PKR":213.9,"PLN":4.56,"RUB":60.96,"SEK":10.41,"SGD":2.137,"SOL":0.06158,"THB":36.39,"TRX":17.95,"TRY":19.05,"UNI":0.1739,"XLM":10.66,"XMR":0.007945,"XRP":2.657,"ZAR":17.83,"BUSD":0.9995,"CAKE":0.2475,"DASH":0.02818,"DOGE":12.07,"LINK":0.1473,"USDC":1,"USDT":1.002,"WAVES":0.4288,"USD":1}}']);

    return back();
});
Route::get('/detener/campana', [PerifericoController::class, 'detenerCampana'])->name('detenerCampana');
Route::get('/change/notification', [PerifericoController::class, 'changeNotification'])->name('change.notification');

Route::get('/mensaje/websocket', function () {
    $niveles = 'avanzado';
    $cantidadTokens = 4;
    $user = User::find(8);
    GosenHelper::createNotification(8, GosenHelper::contenidoNotificacion('email_verificado', [$niveles, $cantidadTokens, 0]));
    event(new NotificationEvent());
});
Route::get('/pruebas/cookie', function () {
    // $base = null;
    // dd(array_merge(['access_key' => _joaat(env_file('code')) . app_key(), 'app' => app_info('key'), 'ver' => app_info('version')], PaymentMethod::getApiData($base)));
    $base = null;
    $cl = new Client();
    $response = $cl->request('GET', 'https://data.exratesapi.com/rates', [
        'headers' => ['X-Api-Signature' => base64_encode(gdmn())],
        'query' => array_merge(['access_key' => '2b93eacc7d125a745hcP2b28fba7691594d9', 'app' => '5hcPWdxQ', 'ver' => app_info('version')], PaymentMethod::getApiData($base)),
    ]);

    $getBody = json_decode($response->getBody(), true);
    // dd($getBody);
    $rates = $getBody['rates'];
    foreach ($rates as $symbol => $value) {
        try {
            $field = 'pmc_rate_'.strtolower($symbol);
            Setting::where('field', $field)->update(['value' => $value]);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $th) {
            Setting::create(['field' => $field, 'value' => $value]);
            $active = 'pmc_active_'.strtolower($symbol);
            Setting::create(['field' => $active, 'value' => 1]);
        }
    }
});

Route::get('/prueba/ai', [OpenAIController::class, 'crearImagenes']);

Route::get('/pruebas/bonos/{id}', function ($id) {
    $user = User::find($id);
    GosenHelper::refreshBonusGift($user);
});

Route::get('/pruebas/redirect/{id}/{usuario}', function ($id, $usuario) {
    //dd($request);
    // dd(Carbon::parse(gmdate("Y-m-d\TH:i:s\Z", 1674139533))->format('d-m-y'));
    GosenHelper::createNotification($usuario, GosenHelper::contenidoNotificacion($id, ['Mario', 2, 0]));
});
