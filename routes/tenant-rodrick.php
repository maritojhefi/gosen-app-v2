

<?php

use App\Http\Controllers\Auth;
use App\Http\Controllers\User;
use App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\LevelBonusDollarController;
use App\Http\Controllers\Livewire\Admin\NewComponent;
use App\Http\Controllers\Livewire\DiccionarioComponent;
use App\Http\Controllers\Livewire\Admin\ValorationComponent;

Route::prefix('user')->middleware(['auth', 'user'])->name('user.')->group(function () {
    //aqui rutas para el lado cliente
    Route::prefix('exportar')->group(function () {
        Route::post('/pdf', [UserController::class, 'exportarPDF'])->name('pdf');
    });
    Route::get('/valoration', [User\ValorationController::class, 'create'])->name('createvalo');
    Route::post('/valoration', [User\ValorationController::class, 'store'])->name('storevalo');
    Route::post('/update/{imagen}/image', [UserController::class, 'uploadFoto'])->name('imagen');
    Route::post('/destroy/foto/{imagen}', [UserController::class, 'eliminarfoto'])->name('imageneliminada');
    Route::get('/bonus', [UserController::class, 'bonusDolares'])->name('bonus');
    Route::post('/bonus/withdraw', [UserController::class, 'withdrawBonus'])->name('bonus.withdraw');
    Route::post('/confirm/wallet/', [PerifericoController::class, 'sendMailWalletConfirm'])->name('send.mail.wallet');
    Route::post('/withdraw/bonus/canceled', [Admin\BonusDollarController::class, 'withdrawBonusCanceled'])->name('withdraw.dollars.cancel');
    Route::get('/cerrar/sesion/{sesion}', [PerifericoController::class, 'cerrarSesion'])->name('cerrar.sesion');
});
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    //aqui sus rutas para el lado admin
    Route::prefix('exportar')->group(function () {
        Route::get('/excel', [UsersController::class, 'exportarExcel'])->name('excel');
        Route::get('/pdf', [UsersController::class, 'exportarPDF'])->name('pdf');
    });
    Route::get('/valorations', ValorationComponent::class)->name('valorations');
    Route::get('/news', NewComponent::class)->name('news');
    Route::get('/reset/copy/{id}', [PerifericoController::class, 'sendMail'])->name('send.mail');
    Route::get('/bonus/{filtro}', [Admin\BonusDollarController::class, 'bonusDolares'])->name('bonus');
    Route::post('/setting/footer/auth', [SettingController::class, 'settingAuthFooter'])->name('setting.auth.footer');
    Route::post('/withdraw/bonus/approved', [Admin\BonusDollarController::class, 'withdrawBonusApproved'])->name('withdraw.dollars');
    Route::post('/withdraw/bonus/canceled', [Admin\BonusDollarController::class, 'withdrawBonusCanceled'])->name('withdraw.dollars.cancel');
    Route::get('/send/massive', [Admin\AdminController::class, 'massiveMailCreator'])->name('send.massive.mail');
    Route::get('/history/sent/mail', [Admin\AdminController::class, 'historyMailSent'])->name('history.mail.sent');
    Route::post('/send/mails', [Admin\AdminController::class, 'sendMails'])->name('send.mails.type');

    Route::get('/diccionario', DiccionarioComponent::class)->name('diccionario');
});
Route::middleware([])->group(function () {
    Route::get('/level/bonus/doll/', [LevelBonusDollarController::class, 'save'])->name('lvlbonus.dollars');
    Route::get('/update/wallet/{id}', [PerifericoController::class, 'actualizarBilletera'])->name('update.wallet');
    Route::get('/reset/user/{id}', [PerifericoController::class, 'ResetearContraMail'])->name('reset.password');
    Route::post('/update/user/', [PerifericoController::class, 'updatePassword'])->name('update.password');
    Route::get('/user/referral/nfound', [LoginController::class, 'userWithoutReferral'])->name('user.nreferral');
    Route::get('/resend/mail/', [PerifericoController::class, 'ResendEmail'])->name('resend.verify.mail');
    Route::get('/revisar/username/{username}', [PerifericoController::class, 'revisarUsername']);
    Route::get('/revisar/correo/{correo}', [PerifericoController::class, 'revisarCorreo']);
});
