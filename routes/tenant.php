<?php

declare(strict_types=1);

use App\Http\Controllers\User;
use App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Auth\VerifyController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Auth\SocialAuthController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([])->group(function () {
    Route::get('/locale', [PublicController::class, 'set_lang'])->name('language');

    // Authenticates Routes
    Route::get('/auth/{service}', [SocialAuthController::class, 'redirect'])->name('social.login');
    Route::get('/auth/{service}/callback', [SocialAuthController::class, 'callback'])->name('social.login.callback');
    Route::post('/auth/social/register', [SocialAuthController::class, 'register'])->name('social.register');
    Auth::routes();
    Route::get('/', [LoginController::class, 'checkLoginState'])->name('home');
    Route::get('verify/', [VerifyController::class, 'index'])->name('verify');
    Route::get('verify/resend', [VerifyController::class, 'resend'])->name('verify.resend');
    Route::get('verify/{id}/{token}', [VerifyController::class, 'verify'])->name('verify.email');
    Route::get('verify/success', [LoginController::class, 'verified'])->name('verified');
    Route::get('register/success', [LoginController::class, 'registered'])->name('registered');
    Route::any('log-out', [LoginController::class, 'logout'])->name('log-out');
    // Google 2FA Routes
    Route::get('/login/2fa', [SocialAuthController::class, 'show_2fa_form'])->middleware('auth')->name('auth.2fa');
    Route::get('/login/2fa/reset', [SocialAuthController::class, 'show_2fa_reset_form'])->name('auth.2fa.reset');
    Route::post('/login/2fa/reset', [SocialAuthController::class, 'reset_2fa']);
    Route::post('/login/2fa', function () {
        return redirect()->route('home');
    })->middleware(['auth', 'g2fa']);

    // if(is_maintenance()){
    Route::get('admin/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('admin/login', [LoginController::class, 'login']);
    Route::post('admin/logout', [LoginController::class, 'logout'])->name('admin.logout');
    Route::get('admin/login/2fa', [SocialAuthController::class, 'show_2fa_form'])->middleware('auth')->name('admin.auth.2fa');
    Route::post('admin/login/2fa', function () {
        return redirect()->route('home');
    })->middleware(['auth', 'g2fa']);
});

// }

// User Routes
Route::prefix('user')->middleware(['auth', 'user', 'verify_user'])->name('user.')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('home');
    Route::get('/account/{var}', [UserController::class, 'account'])->name('account');
    Route::get('/account/activity/activity', [UserController::class, 'account_activity'])->name('account.activity.asd');
    Route::get('/contribute', [User\TokenController::class, 'index'])->name('token');
    Route::get('/contribute/cancel/{gateway?}', [User\TokenController::class, 'payment_cancel'])->name('payment.cancel');
    Route::get('/transactions', [User\TransactionController::class, 'index'])->name('transactions');
    Route::get('/kyc', [User\KycController::class, 'index'])->name('kyc');
    Route::get('/kyc/application', [User\KycController::class, 'application'])->name('kyc.application');
    Route::get('/kyc/application/view', [User\KycController::class, 'view'])->name('kyc.application.view');
    Route::get('/kyc-list/documents/{file}/{doc}', [User\KycController::class, 'get_documents'])->name('kycs.file');
    Route::get('/password/confirm/{token}', [UserController::class, 'password_confirm'])->name('password.confirm');
    // Referral v1.0.3 > v1.1.1
    Route::get('/referral', [UserController::class, 'referral'])->name('referral');
    // My Token v1.1.2
    Route::get('/account/balance/token', [UserController::class, 'mytoken_balance'])->name('token.balance.token');

    // User Ajax Request
    Route::name('ajax.')->prefix('ajax')->group(function () {
        Route::post('/account/wallet-form', [UserController::class, 'get_wallet_form'])->name('account.wallet');
        Route::post('/account/update', [UserController::class, 'account_update'])->name('account.update')->middleware('demo_user');
        Route::post('/contribute/access', [User\TokenController::class, 'access'])->name('token.access');
        Route::post('/contribute/payment', [User\TokenController::class, 'payment'])->name('payment');

        Route::post('/transactions/delete/{id}', [User\TransactionController::class, 'destroy'])->name('transactions.delete')->middleware('demo_user');
        Route::post('/transactions/view', [User\TransactionController::class, 'show'])->name('transactions.view');
        Route::post('/kyc/submit', [User\KycController::class, 'submit'])->name('kyc.submit');
        Route::post('/account/activity', [UserController::class, 'account_activity_delete'])->name('account.activity.delete')->middleware('demo_user');
    });
});

Route::prefix('admin')->middleware(['auth', 'admin', 'g2fa'])->name('admin.')->group(function () {
    Route::get('/', [Admin\AdminController::class, 'index'])->name('home');
    Route::any('/system-info', [Admin\AdminController::class, 'system_info'])->name('system');
    Route::any('/apps-register', [SettingController::class, 'app_register'])->name('niolite');
    Route::get('/profile', [Admin\AdminController::class, 'profile'])->name('profile');
    Route::get('/profile/activity', [Admin\AdminController::class, 'activity'])->name('profile.activity');
    Route::get('/password/confirm/{token}', [Admin\AdminController::class, 'password_confirm'])->name('password.confirm');
    Route::get('/transactions/{state?}', [Admin\TransactionController::class, 'index'])->name('transactions');
    Route::get('/check-transaction/online', [Admin\TransactionController::class, 'checkStatus'])->name('transactions.check');
    Route::get('/stages/settings', [Admin\IcoController::class, 'settings'])->name('stages.settings');
    Route::get('/pages', [Admin\PageController::class, 'index'])->name('pages');
    Route::get('/settings', [SettingController::class, 'index'])->middleware(['super_admin'])->name('settings');
    Route::get('/settings/email', [Admin\EmailSettingController::class, 'index'])->middleware(['super_admin'])->name('settings.email');
    Route::get('/settings/referral', [SettingController::class, 'referral_setting'])->middleware(['super_admin'])->name('settings.referral'); // v1.1.2
    Route::get('/settings/rest-api', [SettingController::class, 'api_setting'])->middleware(['super_admin'])->name('settings.api'); // v1.0.6
    Route::get('/payment-methods', [Admin\PaymentMethodController::class, 'index'])->middleware(['super_admin'])->name('payments.setup');
    Route::get('/payment-methods/edit/{slug}', [Admin\PaymentMethodController::class, 'edit'])->middleware(['super_admin'])->name('payments.setup.edit');
    Route::get('/stages', [Admin\IcoController::class, 'index'])->name('stages');
    Route::get('/stages/{id}', [Admin\IcoController::class, 'edit_stage'])->name('stages.edit');
    Route::get('/users/{role?}', [UsersController::class, 'index'])->name('users'); //v1.1.0
    Route::get('/users/wallet/change-request', [UsersController::class, 'wallet_change_request'])->name('users.wallet.change');
    Route::get('/kyc-list/{status?}', [Admin\KycController::class, 'index'])->name('kycs'); //v1.1.0
    Route::get('/kyc-list/documents/{file}/{doc}', [Admin\KycController::class, 'get_documents'])->name('kycs.file');
    Route::get('/transactions/view/{id}', [Admin\TransactionController::class, 'show'])->name('transactions.view');
    Route::get('/users/{id?}/{type?}', [UsersController::class, 'show'])->name('users.view');
    Route::get('/kyc/view/{id}/{type}', [Admin\KycController::class, 'show'])->name('kyc.view');
    Route::get('/pages/{slug}', [Admin\PageController::class, 'edit'])->name('pages.edit');
    Route::get('/export/{table?}/{format?}', [ExportController::class, 'export'])->middleware(['demo_user', 'super_admin'])->name('export'); // v1.1.0
    Route::get('/languages', [Admin\LanguageController::class, 'index'])->name('lang.manage'); // v1.1.3
    Route::get('/languages/translate/{code}', [Admin\LanguageController::class, 'translator'])->name('lang.translate'); // v1.1.3

    /* Regalo*/
    Route::get('/gifts/settings', [Admin\RegaloController::class, 'index'])->name('regalos.settings');
    Route::get('/gifts/{id}', [Admin\RegaloController::class, 'edit_stage'])->name('regalos.edit');
    Route::get('/users-access/{id?}/{type?}', [UsersController::class, 'Access_User'])->name('users.access');

    Route::post('/users/{id?}', [UsersController::class, 'menu'])->name('users.menu');
    /* Admin Ajax Route */
    Route::name('ajax.')->prefix('ajax')->group(function () {
        Route::post('/users/view', [UsersController::class, 'status'])->name('users.view')->middleware('demo_user');
        Route::post('/users/showinfo', [UsersController::class, 'show'])->name('users.show');
        Route::post('/users/delete/all', [UsersController::class, 'delete_unverified_user'])->name('users.delete')->middleware('demo_user');
        Route::post('/users/email/send', [UsersController::class, 'send_email'])->name('users.email')->middleware('demo_user');
        Route::post('/users/insert', [UsersController::class, 'store'])->middleware(['super_admin', 'demo_user'])->name('users.add');
        Route::post('/profile/update', [Admin\AdminController::class, 'profile_update'])->name('profile.update')->middleware('demo_user');
        Route::post('/profile/activity', [Admin\AdminController::class, 'activity_delete'])->name('profile.activity.delete')->middleware('demo_user');
        Route::post('/users/wallet/action', [UsersController::class, 'wallet_change_request_action'])->name('users.wallet.action');
        Route::post('/payment-methods/view', [Admin\PaymentMethodController::class, 'show'])->middleware('super_admin')->name('payments.view');
        Route::post('/payment-methods/update', [Admin\PaymentMethodController::class, 'update'])->middleware(['super_admin', 'demo_user'])->name('payments.update');
        Route::post('/payment-methods/quick-update', [Admin\PaymentMethodController::class, 'quick_update'])->middleware(['super_admin', 'demo_user'])->name('payments.qupdate');
        Route::post('/kyc/view', [Admin\KycController::class, 'ajax_show'])->name('kyc.ajax_show');
        Route::post('/stages/update', [Admin\IcoController::class, 'update'])->name('stages.update')->middleware('demo_user');
        Route::post('/stages/pause', [Admin\IcoController::class, 'pause'])->name('stages.pause')->middleware('demo_user');
        Route::post('/stages/active', [Admin\IcoController::class, 'active'])->name('stages.active')->middleware('demo_user');
        Route::post('/stages/meta/update', [Admin\IcoController::class, 'update_options'])->name('stages.meta.update')->middleware('demo_user');
        Route::post('/stages/settings/update', [Admin\IcoController::class, 'update_settings'])->name('stages.settings.update')->middleware('demo_user');
        Route::post('/stages/actions', [Admin\IcoController::class, 'stages_action'])->name('stages.actions'); //v1.1.2
        Route::post('/kyc/update', [Admin\KycController::class, 'update'])->name('kyc.update')->middleware('demo_user');
        Route::post('/transactions/update', [Admin\TransactionController::class, 'update'])->name('transactions.update')->middleware('demo_user');

        Route::post('/transactions/adjust', [Admin\TransactionController::class, 'adjustment'])->name('transactions.adjustement');
        Route::post('/settings/email/template/view', [Admin\EmailSettingController::class, 'show_template'])->middleware('super_admin')->name('settings.email.template.view');
        Route::post('/transactions/view', [Admin\TransactionController::class, 'show'])->name('transactions.view');
        Route::post('/transactions/insert', [Admin\TransactionController::class, 'store'])->name('transactions.add')->middleware('demo_user');
        Route::post('/pages/upload', [Admin\PageController::class, 'upload_zone'])->name('pages.upload')->middleware('demo_user');
        Route::post('/pages/view', [Admin\PageController::class, 'show'])->name('pages.view');
        Route::post('/pages/update', [Admin\PageController::class, 'update'])->name('pages.update')->middleware('demo_user');
        Route::post('/settings/update', [SettingController::class, 'update'])->middleware(['super_admin', 'demo_user'])->name('settings.update');
        // Settings UpdateMeta v1.1.0
        Route::post('/settings/meta/update', [SettingController::class, 'update_meta'])->middleware(['super_admin', 'demo_user'])->name('settings.meta.update');
        Route::post('/settings/email/update', [Admin\EmailSettingController::class, 'update'])->middleware(['super_admin', 'demo_user'])->name('settings.email.update');
        Route::post('/settings/email/test', [Admin\EmailSettingController::class, 'sendTestEmail'])->middleware(['super_admin', 'demo_user'])->name('settings.email.test');
        Route::post('/settings/email/template/update', [Admin\EmailSettingController::class, 'update_template'])->middleware(['super_admin', 'demo_user'])->name('settings.email.template.update');
        Route::post('/languages', [Admin\LanguageController::class, 'language_action'])->middleware(['demo_user'])->name('lang.action'); // v1.1.3
        Route::post('/languages/translate', [Admin\LanguageController::class, 'language_action'])->middleware(['demo_user'])->name('lang.translate.action'); // v1.1.3

        Route::post('/gifts/actions', [Admin\RegaloController::class, 'view_action'])->name('regalo.view.actions');
        Route::post('/gifts/update', [Admin\RegaloController::class, 'update'])->name('regalo.update');
    });

    //Clear Cache facade value:
    Route::get('/clear', function () {
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('route:clear');
        $exitCode = Artisan::call('view:clear');

        $data = ['msg' => 'success', 'message' => ___('Cache Cleared and Optimized!')];

        if (request()->ajax()) {
            return response()->json($data);
        }

        return back()->with([$data['msg'] => $data['message']]);
    })->name('clear.cache');
});

Route::name('public.')->group(function () {
    Route::get('/check/updater', [PublicController::class, 'update_check']);
    Route::get('/insert/database', [PublicController::class, 'database'])->name('database');
    Route::get('/kyc-application', [PublicController::class, 'kyc_application'])->name('kyc');
    Route::get('/invite', [PublicController::class, 'referral'])->name('referral');
    Route::post('/kyc-application/file-upload', [User\KycController::class, 'upload'])->name('kyc.file.upload');
    Route::post('/kyc-application/submit', [User\KycController::class, 'submit'])->name('kyc.submit');
    Route::get('/qrgen.png', [PublicController::class, 'qr_code'])->name('qrgen');

    Route::get('white-paper', function () {
        $filename = get_setting('site_white_paper');
        $path = storage_path('app/public/'.$filename);
        if (! file_exists($path)) {
            abort(404);
        }
        $file = \File::get($path);
        $type = \File::mimeType($path);
        $response = response($file, 200)->header('Content-Type', $type);

        return $response;
    })->name('white.paper');

    Route::get('/{slug}', [PublicController::class, 'site_pages'])->name('pages');
});

// Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::post('/kyc/file-upload', [User\KycController::class, 'upload'])->name('kyc.file.upload');
});
