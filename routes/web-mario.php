<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\TokensRewardController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

Route::get('/prueba/mario', function () {
    return 'este es tu archivo de rutas Mario'; //ruta de prueba para revisar archivo de rutas para Mario
});
Route::get('/prueba/traduccion', [PerifericoController::class, 'pruebaTraduccion']);
//No olviden agrupar y ordenar sus rutas, tambien agregar comentarios a sus rutas para aclaraciones breves sobre lo que hacen :)

Route::prefix('user')->middleware(['auth', 'user'])->name('user.')->group(function () {
    //aqui sus rutas para el lado user
    Route::get('/cambiarSidebar', [PerifericoController::class, 'cambiarSidebar']);
    Route::prefix('rewardtoken')->name('reward.')->group(function () {
        Route::get('/video1', [TokensRewardController::class, 'video1'])->name('video1');
        Route::get('/video2', [TokensRewardController::class, 'video2'])->name('video2');
        Route::get('/video3', [TokensRewardController::class, 'video3'])->name('video3');
        Route::get('/pdf', [TokensRewardController::class, 'pdf'])->name('pdf');
        Route::get('/modal', [TokensRewardController::class, 'modal'])->name('modal');
    });
});
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    //aqui sus rutas para el lado admin
    // Route::get('/users/log/in/{id}', [UsersController::class, 'loginAsUser'])->name('users.log.in');

    Route::get('/cambiarSidebar', [PerifericoController::class, 'cambiarSidebar']);
    //ruta para cambiar estado de niveles dolares ajax
    Route::get('settings/change/status/{id}', [PerifericoController::class, 'changeStatus']);
});

Route::middleware([])->group(function () {
    Route::get('/regalar/tokens', [PerifericoController::class, 'rutaReturnVideo'])->name('rutaReturnVideo');
    //Ruta para traducir textos
    Route::get('/traducir/{texto}', [PerifericoController::class, 'traduccionGlobal']);

    Route::get('/revisar/correo/{correo}', [PerifericoController::class, 'revisarCorreo']);
});
