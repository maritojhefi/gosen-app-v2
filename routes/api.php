<?php

use App\Helpers\CircuitosWhatsappHelper;
use App\Http\Controllers\APIController;
use App\Models\PaymentMethod;
use App\Models\Setting;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/stage', [APIController::class, 'stage'])->name('stage');
Route::get('/stage/full', [APIController::class, 'stage_full'])->name('stage.full');

Route::get('/bonus', [APIController::class, 'bonuses'])->name('bonus');
Route::get('/price', [APIController::class, 'prices'])->name('price');
Route::get('/refresh/rates', function () {
    $base = null;
    $cl = new Client();
    $response = $cl->request('GET', 'https://data.exratesapi.com/rates', [
        'headers' => ['X-Api-Signature' => base64_encode(gdmn())],
        'query' => array_merge(['access_key' => '2b93eacc7d125a745hcP2b28fba7691594d9', 'app' => '5hcPWdxQ', 'ver' => app_info('version')], PaymentMethod::getApiData($base)),
    ]);
    if ($response->getStatusCode() == 200) {
        $getBody = json_decode($response->getBody(), true);
        $rates = $getBody['rates'];
        foreach ($rates as $symbol => $value) {
            try {
                $field = 'pmc_rate_'.strtolower($symbol);
                Setting::where('field', $field)->update(['value' => $value]);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $th) {
                Setting::create(['field' => $field, 'value' => $value]);
                $active = 'pmc_active_'.strtolower($symbol);
                Setting::create(['field' => $active, 'value' => 1]);
            }
        }
    }
});
Route::post('/circuito/soporte', function (Request $request) {
    $json = json_decode(json_encode($request->all()));
    // WhatsappApiLog::create([
    //     'titulo' => 'exitoso',
    //     'log' => json_encode($json)
    // ]);
    CircuitosWhatsappHelper::circuitoMenuWhatsapp($json);
});
Route::any('/{any?}', function () {
    throw new App\Exceptions\APIException('Enter a valid endpoint', 400);
})->where('any', '.*');
