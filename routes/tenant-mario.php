<?php

use App\Models\LogsGosen;
use App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\TokensRewardController;
use App\Http\Controllers\Livewire\Admin\PackagesCrudComponent;

Route::get('/prueba/mario', function () {
    return 'este es tu archivo de rutas Mario'; //ruta de prueba para revisar archivo de rutas para Mario
});
Route::get('/prueba/traduccion', [PerifericoController::class, 'pruebaTraduccion']);
//No olviden agrupar y ordenar sus rutas, tambien agregar comentarios a sus rutas para aclaraciones breves sobre lo que hacen :)

Route::prefix('user')->middleware(['auth', 'user'])->name('user.')->group(function () {
    //aqui sus rutas para el lado user
    Route::get('/cambiarSidebar', [PerifericoController::class, 'cambiarSidebar']);
    Route::prefix('rewardtoken')->name('reward.')->group(function () {
        Route::get('/video1', [TokensRewardController::class, 'video1'])->name('video1');
        Route::get('/video2', [TokensRewardController::class, 'video2'])->name('video2');
        Route::get('/video3', [TokensRewardController::class, 'video3'])->name('video3');
        Route::get('/pdf', [TokensRewardController::class, 'pdf'])->name('pdf');
        Route::get('/modal', [TokensRewardController::class, 'modal'])->name('modal');
    });
    Route::get('/admins/log/in/', [PerifericoController::class, 'loginAsAdmin'])->name('back');
});
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    //aqui sus rutas para el lado admin
    Route::get('/users/log/in/{id}', [UsersController::class, 'loginAsUser'])->name('users.log.in');

    Route::get('/cambiarSidebar', [PerifericoController::class, 'cambiarSidebar']);
    //ruta para cambiar estado de niveles dolares ajax
    Route::get('settings/change/status/{id}', [PerifericoController::class, 'changeStatus']);

    Route::get('/packages', PackagesCrudComponent::class)->name('packages');
});

Route::middleware([])->group(function () {
    Route::get('/regalar/tokens', [PerifericoController::class, 'rutaReturnVideo'])->name('rutaReturnVideo');
    //Ruta para traducir textos
    Route::get('/traducir/{texto}', [PerifericoController::class, 'traduccionGlobal']);

    Route::get('/revisar/correo/{correo}', [PerifericoController::class, 'revisarCorreo']);
});

Route::get('/prueba/json', function () {
    $input = 'asd';
    $espanol = 'asd';
    $ingles = 'asd';
    $portugues = 'asd';
    $idiomas = [
        'es' => $espanol,
        'en' => $ingles,
        'pr' => $portugues,
        // 'ru' => $ruso,
        // 'it' => $italiano,
        // 'fr' => $frances,
        // 'hi' => $hindu,
        // // 'ar' => $arabe,
        // 'tr' => $turkish,
        // 'de' => $german,
        // 'zh' => $chinese

    ];
    foreach ($idiomas as $idioma => $traduccion) {
        $traduccion = str_replace('/[\'^£$%&*}{#~><>|=_+¬]/', '', $traduccion);
        $jsonString = file_get_contents(base_path('resources/lang/'.$idioma.'.json'));
        $data = json_decode($jsonString, true);
        $cantidadOriginal = count($data);
        // Update Key
        if (! isset($data[$input])) {
            $data[$input] = str_replace('\\', '', $traduccion);
            $newJsonString = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            $cantidadNueva = count(json_decode($newJsonString, true));
            try {
                if ($cantidadNueva >= $cantidadOriginal) {
                    file_put_contents(base_path('resources/lang/'.$idioma.'.json'), stripslashes($newJsonString));
                }
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'Fallo de traduccion',
                    'log' => 'llave: '.$input,
                ]);
            }
        }

        //usleep(2000000);
    }
});
