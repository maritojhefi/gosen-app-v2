<?php

use App\Http\Controllers\User;
use App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\PerifericoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\SettingController;

//No olviden agrupar y ordenar sus rutas, tambien agregar comentarios a sus rutas para aclaraciones breves sobre lo que hacen :)

//ruta para exportar archivos PDF y EXCEL
Route::prefix('exportar')->group(function () {
    Route::get('/excel', [Admin\TransactionController::class, 'exportarExcel'])->name('excel');
    Route::get('/pdf', [Admin\TransactionController::class, 'exportarPdf'])->name('pdf');
    Route::get('/pdf-transaction-details/{id}', [User\TransactionController::class, 'exportarTransactionDetailsPdf'])->name('pdf-transaction-details');
});

//rutas para las vistas lado admin
Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    Route::get('/delete/question/{id}', [Admin\FrequentQuestionsController::class, 'eliminar']);
    Route::resource('/frequentquestions', Admin\FrequentQuestionsController::class);
    //ruta para crear usuarios
    Route::get('/user-create', [UsersController::class, 'create'])->name('user-create');
    //ruta top-referrals
    Route::get('/top-referrals', [UsersController::class, 'topReferrals'])->name('top-referrals');
    //ruta eventos
    Route::resource('/eventos', Admin\EventoController::class);
    //ruta para eliminar evento
    Route::get('/delete/evento/{id}', [Admin\EventoController::class, 'eliminar']);
    //ruta para eliminar evento de la tabla hijo
    Route::get('/delete/event/{id}', [Admin\EventoController::class, 'eliminarEvento']);
    //ruta vista nueva page
    Route::get('/pages/add/new-page', [Admin\PageController::class, 'addPage'])->name('add.page');
    //ruta create new page
    Route::post('/pages/add/create-page', [Admin\PageController::class, 'store'])->name('pages.store');
    //ruta para eliminar la page
    Route::get('/delete/pag/{id}', [Admin\PageController::class, 'eliminarPage']);
    //ruta actualizar estado de las main pages
    Route::get('/update/status/{id}', [Admin\PageController::class, 'updateMainPage']);
    //ruta para ver withdraw
    Route::get('/index', [SettingController::class, 'manageWithdraw'])->name('withdraw.settings');
    //ruta para activar y desactivar withdraw
    Route::get('/update/status/withdraw/{id}', [SettingController::class, 'updateSwitchWithdraw']);
});

//rutas para las vistas lado usuario
Route::prefix('user')->middleware(['auth', 'user'])->group(function () {
    Route::get('/frequentquestions', [Admin\FrequentQuestionsController::class, 'indexCliente'])->name('listado.faq');
    //ruta top-referrals
    Route::get('/top-referrals', [UserController::class, 'topReferralindex'])->name('top-referrals');
    //ruta para eliminar evento de la tabla hijo
    Route::get('/delete/event/{id}', [Admin\EventoController::class, 'eliminarEvento']);
    //ruta marketing
    Route::get('/marketing', [UserController::class, 'marketing'])->name('marketing');
    //ruta para reset password
    Route::get('/reset/copy/{id}', [PerifericoController::class, 'sendMail'])->name('send.mail');
    //ruta para cambio de correo
    Route::get('/change/mail/{id}', [PerifericoController::class, 'sendChangeMail'])->name('send.Changemail');
    //ruta descarga banner
    Route::get('/download/banner/{bnr}/{num}', [PerifericoController::class, 'downloadBanner'])->name('download.banner');
    //ruta para cambiar el theme_color
    Route::get('/settings/update_theme_color/{color}', [SettingController::class, 'updateThemeColor'])->name('user.theme_color');
    //ruta para actualizar foto de perfil del usuario crop
    Route::post('/update/image/perfil', [UserController::class, 'uploadPerfil'])->name('update.imagen.perfil');
});

Route::middleware([])->group(function () {
    //ruta para crear cookies
    Route::get('/crear/cookie', [UserController::class, 'crearCookie']);
    //ruta para cambio de correo
    Route::get('/reset/mail/{id}', [PerifericoController::class, 'ChangeMail'])->name('change.mail');
    //ruta para update de correo
    Route::post('/update/mail/', [PerifericoController::class, 'UpdateMail'])->name('update.mail');
    //ruta para vista confirmacion del cambio de email
    Route::get('/reset/mail/{id}/{receptor}', [PerifericoController::class, 'confirmationMail'])->name('confirm.mail');

    Route::get('/invite/{landing}/{username}', [PublicController::class, 'referral2']);
});
