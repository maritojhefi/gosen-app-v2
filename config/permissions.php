<?php
/**
 * Here is the permissions control
 *
 * uses: 'manage_user' => ['ControllerName'] @array
 */

return [
    'dashboard' => ['Admin\AdminController'],

    'manage_tranx' => ['Admin\TransactionController'],
    'manage_kyc' => ['Admin\KycController'],
    'manage_stage' => ['Admin\IcoController'],
    'manage_setting' => [
        'Admin\PageController',
        'Admin\SettingController',
        'Admin\LanguageController',
        'Admin\PaymentMethodController',
        'Admin\IcoController@settings',
        'Admin\IcoController@update_settings',
    ],
    'manage_user' => [
        'Admin\UsersController',
    ],
    'view_user' => [
        'Admin\UsersController@index',
        'Admin\UsersController@show',
    ],
    'add_user' => [
        'Admin\UsersController@create',
        'Admin\UsersController@store',
    ],
    'view_tranx' => [
        'Admin\TransactionController@index',
        'Admin\TransactionController@show',
    ],
    'add_tranx' => [
        'Admin\TransactionController@store',
    ],
    'update_tranx' => [
        'Admin\TransactionController@update',
        'Admin\TransactionController@refund',
    ],
    'view_kyc' => [
        'Admin\KycController@index',
        'Admin\KycController@ajax_show',
        'Admin\KycController@show',
    ],
    'update_kyc' => [
        'Admin\KycController@update',
    ],
    'manage_withdraw' => [
        'Withdraw\WithdrawController',
    ],
    'view_withdraw' => [
        'Withdraw\WithdrawController@index',
        'Withdraw\WithdrawController@view_details',
    ],
    'update_withdraw' => [
        'Withdraw\WithdrawController@update_transaction',
    ],
    'manage_bonusdollar' => [
        'Admin\BonusDollarController',
    ],
    'view_all' => [
        'Admin\UsersController@index',
        'Admin\TransactionController@index',
        'Admin\KycController@index',
        'Withdraw\WithdrawController@index',
        'Admin\BonusDollarController@bonusDolares',
    ],
    'translator' => [
        'Livewire\DiccionarioComponent',
    ],
    'suspend_user' => [
        'Admin\UsersController@status',
    ],
];
