<style>
    .img-thumbnail {
        border-radius: 5rem;
    }

    .guttar-20px {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }

    .btn-grp {
        justify-content: center;
    }

    @media (max-width: 437px) {
        .file-upload {
            margin-left: -38px;
        }

        .btndelete {
            margin-left: 18px;
        }

        .btnsave {
            margin-left: 27px;
        }
    }

    @media (max-width: 380px) {
        .file-upload {
            margin-left: 8px;
        }

        .btndelete {
            margin-left: 24px;
        }
    }
</style>
<div class="content-area card" style="text-align: center">
    <div class="card-innr">
        <div class="card-head pb-0">
            <h5> {{ ___('User Image') }} <img loading="lazy" src="{{ asset('assets/images/image.gif') }}" alt=""
                    style="width:60px"> {{ $user->foto ? '' : '(' . ___('no photo yet') . ')' }}</h5>
        </div>
        <div class="d-sm-flex justify-content-between align-items-center">
            <form action={{ route('user.imagen', $user->id) }} method="POST" enctype="multipart/form-data"
                class=col-lg-12>
                @csrf
                <div class="col-lg-12">
                    <div class="card-body pt-0">
                        <div class="row">
                            @if ($user->foto == null)
                                <div class="col-lg-12" id="fotoActual">
                                    <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}" class="img-thumbnail"
                                        style="max-width:150px; height:auto">
                                </div>
                                <div class="" id="gif">
                                </div>
                                <div class="" id="preview"
                                    style="max-width:150px; height:auto; margin:auto;
                                display:block;">

                                </div>
                            @else
                                <div class="col-lg-12" id="fotoActual">
                                    <img loading="lazy" src="{{ asset2('imagenes/perfil/' . $user->foto) }}" class="img-thumbnail"
                                        style="max-width:150px; height:autodisplay: flex;
                                    justify-content: center;
                                    align-items: center;">
                                </div>
                                <div class="" id="gif">
                                </div>
                                <div class="" id="preview"
                                    style="max-width:150px; height:auto; margin:auto;
                                display:block;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <ul class="btn-grp">
                            <li>
                                <div class="btnsave" id="btnSave">
                                    <button class="btn btn-sm btn-auto btn-primary d-none" id="btnSave2"
                                        type="submit"><span class="material-icons">{{ ___('Save') }} &nbsp; <i
                                                class="fa fa-download"></i></span>
                                    </button>
                                </div>
                            </li>
                            <li>
                                {{-- <a class="file-upload btn btn-sm btn-auto btn-primary" id="btnUpload"> --}}
                                {{-- <div class="file-upload"> --}}
                                <label for="upload" class="file-upload btn btn-sm btn-auto btn-primary mb-0"
                                    id="btnUpload">
                                    {{ ___('File Upload') }} &nbsp;
                                    <i class="fa fa-file"></i>

                                </label>
                                <input style="display: none" id="upload" class="file-upload__input" type="file"
                                    name="foto">
                                {{-- </div> --}}
                                {{-- </a> --}}
                            </li>
            </form>
            <li>
                <div class="btndelete" id="btnDelete">
                    @if ($user->foto)
                        <form action="{{ route('user.imageneliminada', $user->id) }}" method="POST">
                            @csrf
                            <button class="btn btn-sm btn-auto btn-danger" type="submit"><span
                                    class="material-icons">{{ ___('Delete') }} &nbsp; <i class="fa fa-trash"></i></span>
                            </button>
                        </form>
                    @endif
                </div>
            </li>
            <li>
                <div class="" id="btnCancel"></div>
            </li>
            </ul>
        </div>
    </div>
</div>
</div>{{-- .card-innr --}}
</div>
@push('footer')
    <script>
        document.getElementById("upload").onchange = function(e) {
            // Creamos el objeto de la clase FileReader
            var reader = new FileReader();
            // Leemos el archivo subido y se lo pasamos a nuestro fileReader
            reader.readAsDataURL(e.target.files[0]);
            // Le decimos que cuando este listo ejecute el código interno
            reader.onload = function() {
                var fotoActual = document.getElementById('fotoActual')
                var preview = document.getElementById('preview')
                var gifprev = document.getElementById('gif')
                var btnUpload = document.getElementById('btnUpload')
                var btnDelete = document.getElementById('btnDelete')
                var btnCancel = document.getElementById('btnCancel')
                var btnSave2 = document.getElementById('btnSave2')
                var text = document.createTextNode(" Cancel")
                fotoActual.classList.remove("col-lg-12");
                fotoActual.classList.add("col-lg-5");
                gifprev.classList.add("col-lg-2");
                preview.classList.add("auto");
                image = document.createElement('img');
                gif = document.createElement('img');
                url = document.createElement('a');
                gif.src = "{{ asset('assets/images/change.gif') }}"
                image.src = reader.result;
                image.classList.add("img-thumbnail");
                url.classList.add("btn");
                url.classList.add("btn-sm");
                url.classList.add("btn-auto");
                url.classList.add("btn-danger");
                url.innerHTML = '<i class="fa fa-ban"></i>';
                url.href = ''
                image.classList.add("img-fluid");
                btnUpload.classList.add("d-none");
                btnDelete.classList.add("d-none");
                btnSave2.classList.remove("d-none");
                preview.innerHTML = '';
                gifprev.innerHTML = '';
                preview.append(image);
                gifprev.append(gif);
                btnCancel.append(url);
                url.appendChild(text);
            };
        }
    </script>
@endpush
