<div wire:ignore.self class="col-lg-4 col-sm-4">
    <div class="content-area" style="box-shadow:4px 4px rgb(176, 174, 174); margin:10px; border-radius:10px;">
        <div class="card-innr">
            <div class="card-head">
                <div class="card" style="background-color:rgb(215, 226, 226);">
                    <div class="card-body row" style="align-items: stretch">
                        <div class="col-10">
                            <h4 class="card-title">{{ __($item->titulo) }}
                            </h4>
                        </div>
                        <div class="col-1">
                            <div class="relative d-inline-block">
                                <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                        class="ti ti-more-alt"></em></a>
                                <div class="toggle-class dropdown-content dropdown-content-top-left">
                                    <ul class="dropdown-list ">
                                        <li>
                                            <a href="#" wire:click="edit({{ $item->id }})" data-toggle="modal" data-target='#Modalcrear'>
                                                <i class="fa fa-edit" >
                                                </i>{{___('Edit')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" wire:click="destroy({{ $item->id }})">
                                                <i class="fa fa-trash">
                                                </i>{{___('Delete')}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col-3">
                            @php
                                $actual = date('Y-m-d');
                                $inicio = date('Y-m-d', strtotime($item->fechaInicio));
                                $fin = date('Y-m-d', strtotime($item->fechaFin));
                                
                            @endphp
                            @if ($inicio <= $actual && $fin >= $actual)
                                <span class="badge badge-pill badge-success">
                                    {{___('On date')}}</span>
                            @elseif ($actual > $fin)
                                <span class="badge badge-pill badge-danger">
                                    {{___('Out of date')}}</span>
                            @elseif ($inicio > $actual)
                                <span class="badge badge-pill badge-info">
                                    {{___('Coming soon')}}</span>
                            @endif
                        </div>
                        <br>
                        
                        <span><i class="fa fa-calendar"></i>
                            {{ date('j F', strtotime($item->fechaInicio)) }}
                            -
                            {{ date('j F', strtotime($item->fechaFin)) }}
                        </span>

                    </div>

                </div>


            </div>
            <div class="">
                @if ($item->id == $mostrar)
                    <p>
                        {{ __($item->descripcion) }}
                    </p>
                @else
                    <p>
                        {{ Str::limit(__($item->descripcion), $cantidadTexto) }}
                        @if (strlen(__($item->descripcion)) > 70)
                            <a href="#" class="badge badge-xs" style="color: black"
                                wire:click="mostrarTodo({{ $item->id }})">{{___('More')}}</a>
                        @endif
                    </p>
                @endif
                <div>
                    @if ($item->imagenes)
                        @if ($item->imagenes->count() > 0)
                            <div class="container" >
                                <div class="row">
                                    @foreach ($item->imagenes as $foto)
                                        <div class="col-4 ">
                                            <div class="card">
                                                <img loading="lazy" src="{{ asset2('imagenes/'.$foto->nombre) }}" style="width:100;height:auto;">
                                                <div class="card-footer text-muted">
                                                    <a href="#" class="b"
                                                        wire:click="destroyimage({{ $foto->id }})">
                                                        <i class="fa fa-trash text-danger text-center"></i>
                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <td>
                                <button class="btn">
                                    <i class="fa fa-camera"></i>
                                    {{___('(Without image)')}}
                                    <span class="badge badge-primary"></span>
                                </button>
                            </td>
                        @endif
                    @endif
                </div>
            </div>
        </div><!-- .card-innr -->
    </div><!-- .card -->
</div><!-- .col -->
