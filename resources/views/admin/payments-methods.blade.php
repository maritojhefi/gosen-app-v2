@extends('layouts.admin')
@section('title', ___('Payment Methods'))

@section('content')
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            <div class="card content-area">
                <div class="card-innr">
                    <div class="card-head d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">{{ ___('Payment Methods') }}</h4>
                        <a href="javascript:void(0)" class="btn btn-sm btn-auto managee btn-primary"
                            data-type="manage_currency"><em class="fas fa-coins"></em><span
                                class=" d-sm-inline-block d-none">{{ ___('Manage Currency') }}</span></a>
                    </div>
                    @if (is_demo_user() || is_demo_preview())
                        <div class="gaps-1-5x"></div>
                        <div class="alert alert-danger">{!! __('messages.demo_payment_note') !!}</div>
                    @endif
                    <div class="gaps-1x"></div>
                    <div class="row guttar-vr-30px{{ empty($methods) ? ' justify-content-center' : '' }}">
                        @forelse($methods as $method)
                            <div class="col-xl-4 col-md-6">
                                {!! $method !!}
                            </div>
                        @empty
                            <div class="bg-light text-center rounded pdt-5x pdb-5x">
                                <p><em class="ti ti-package fs-24"></em><br><strong
                                        class="mt-4 fs-20 text-head">{{ ___('Opps!') }}</strong><br>{{ ___('No available payment module package') }}!
                                </p>
                                <p><a class="link" href="https://softnio.com/contact/"
                                        target="_blank">{{ ___('Contact us our support team') }}.</a></p>
                            </div>
                        @endforelse
                    </div>
                    <div class="gaps-0x"></div>
                </div>
            </div>
        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection
@push('footer')
    <script>
        $(document).ready(function() {
            var b = $("#ajax-modal")
            var access_url = "{{ route('admin.ajax.payments.view') }}";
            $(".managee").click(function(c) {
                c.preventDefault();
                $(this).addClass('disabled');
                $(this).html(
                    `<em class="fas fa-coins"></em><span class=" d-sm-inline-block d-none">{{ ___('Manage Currency') }}</span><span class="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>`
                );
                var d = $(this).data("type");
                $.ajax({
                    method: "post",
                    data: {
                        _token: csrf_token,
                        req_type: d,
                    },
                    url: access_url,
                    success: function(a) {
                        b.html(a);
                        $('#manages').modal('show')
                    },
                    error: function(a, b, c) {
                        console.log(a, b, c),
                            show_toast("error", "Sorry, something went wrong!\n" + c);
                    }
                })
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var O = $(".quick-act");
            var b = $("#ajax-modal")
            var trnx_adjust_url = "{{ route('admin.ajax.payments.qupdate') }}";
            0 < O.length && "undefined" != typeof quick_update_url &&
                O.click(function() {
                    var b = $(this);
                    $.ajax({
                        method: "post",
                        data: {
                            _token: csrf_token,
                            type: b.data("name")
                        },
                        url: trnx_adjust_url,
                        success: function(a) {
                            show_toast(a.msg, a.message),
                                setTimeout(function() {
                                    window.location.reload();
                                }, 300);
                        },
                        error: function() {
                            show_toast("error", "Sorry, something went wrong!");
                        }
                    })
                });
        });
    </script>
@endpush