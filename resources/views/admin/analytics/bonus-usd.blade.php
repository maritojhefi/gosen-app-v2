@extends('layouts.admin')
@section('title', ___('Bonus USD'))
@section('content')
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            @include('layouts.messages')
            <div class="row">
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{___('Bonus USD')}}</h4>
                            <div class="text-center mt-2">
                                <div class="row">
                                    <div class="col-4">Total</div>
                                    <div class="col-4">Ingresos</div>
                                    <div class="col-4">Ingresos Bonus USD</div>
                                </div>
                                <div class="row">
                                    <div class="col-4">Fecha</div>
                                    <div class="col-4">{{ $total_usd }}</div>
                                    <div class="col-4">{{ $total_bonus }}</div>
                                </div>
                            </div>

                            <div data-simplebar="init" style="max-height: 550px;">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper"
                                                style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">
                                                    <ul class="list-unstyled activity-wid">
                                                        @foreach ($ingresos_usd as $value)
                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                <div class="col-4">
                                                                    {{ $value['fecha'] }}
                                                                </div>
                                                                <div class="col-4">
                                                                    
                                                                    {{ $value['usd']}}
                                                                    
                                                                </div>
                                                                <div class="col-4">
                                                                    {{ $value['bonus']}}
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="reg-statistic-graph card card-full-height">
                        <div class="card-innr">
                            <div class="card-head has-aside">
                                <h4 class="card-title card-title-sm">{{___('Bonus USD')}}</h4>
                                <div class="card-opt">
                                    <a href="#" class="link ucap link-light toggle-tigger toggle-caret">{{ isset($_GET['user']) ? $_GET['user'] : 15 }} {{___('Days')}}</a>
                                    <div class="toggle-class dropdown-content">
                                        <ul class="dropdown-list">
                                            <li><a href="{{ url()->current() }}?user=7">7 {{___('Days')}}</a></li>
                                            <li><a href="{{ url()->current() }}?user=15">15 {{___('Days')}}</a></li>
                                            <li><a href="{{ url()->current() }}?user=30">30 {{___('Days')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-statistics mb-0" style="height: 512px">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
    const ctx = document.getElementById('myChart');
    let array = [<?=$data_usd['days']?>];
    let array2 = [<?=$data_usd['data']?>];
    let array3 = [<?=$bonus_usd['data']?>];
    Charts(array, array2)
    function Charts(array, array2) {
        Charts1 = new Chart(ctx, {
            type: 'line',
            data: {
                labels: array,
                datasets: [
                    {
                        label: 'Ingresos',
                        data: array2,
                        borderColor: '#126bb0',
                        backgroundColor: '#5093c7',
                    },
                    {
                        label: 'Bonus USD',
                        data: array3,
                        borderColor: '#d12828',
                        backgroundColor: '#cc7474',
                    },
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
            }
        });
    }
</script>
@endpush