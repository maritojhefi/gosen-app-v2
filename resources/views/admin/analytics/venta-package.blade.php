@extends('layouts.admin')
@section('title', ___('Venta de paquetes'))
@section('content')
    <div class="page-content">
        @include('vendor.notice')
        @include('layouts.messages')
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-innr">
                        <h4 class="card-title">{{ ucfirst(___('Venta de paquetes')) }}</h4>
                        <div class="text-center mb-3">
                            <div class="row">
                                <div class="col-4"><strong>{{ ___('Total') }}</strong></div>
                                <div class="col-4"><strong>{{ ___('Disponibles') }}</strong></div>
                                <div class="col-4"><strong>{{ ___('Vendidos') }}</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-4">{{ array_sum($infoTotales) }}</div>
                                <div class="col-4">{{ $TotalpaquetesDisponibles }}</div>
                                <div class="col-4"><span>{{ $porcentajeTotal }} %</span></div>
                            </div>
                        </div>
                        <div data-simplebar="init" style="max-height: 689px;">
                            <div class="simplebar-wrapper" style="margin: 0px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper"
                                            style="height: auto; overflow: hidden scroll;">
                                            <div class="simplebar-content" style="padding: 0px;">
                                                <ul class="list-unstyled activity-wid">
                                                    <li class="list-group-item d-flex justify-content-between align-items-center text-white"
                                                        style="background: {{ asset(theme_color('sidebar')) }}; opacity:0.6">
                                                        <div class="col-3">
                                                            {{ ___('Paquete') }}
                                                            <small>({{ ___('Nombre') }})</small>
                                                        </div>
                                                        <div class="col-3">
                                                            {{ ___('Total') }}
                                                            <small>({{ ___('Paquete') }})</small>
                                                        </div>
                                                        <div class="col-3">
                                                            {{ ___('Vendido') }}
                                                            <small>({{ ___('Paquete') }})</small>
                                                        </div>
                                                        <div class="col-3">
                                                            {{ ___('Disponible') }}
                                                            <small>(Stock)</small>
                                                        </div>
                                                    </li>
                                                    @php
                                                    $contador = 1;
                                                    @endphp
                                                    @foreach ($paquetes as $item)
                                                        <li
                                                            class="list-group-item d-flex justify-content-between align-items-center">
                                                            <div class="col-3">
                                                                {{ $item->nombre }}
                                                            </div>
                                                            <div class="col-3">
                                                                {{ $infoTotales[$contador] }}
                                                            </div>
                                                            <div class="col-3">
                                                                {{ $infoTotales[$contador] - $item->stock }}
                                                                <small class="fas fa-info-circle"
                                                                    style="color: #868e83; font-size: 11px;"
                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                    title=""
                                                                    data-original-title="{{ round((($infoTotales[$contador] - $item->stock) / $infoTotales[$contador]) * 100) }} %"></small>
                                                            </div>
                                                            <div class="col-3">
                                                                <span> {{ $item->stock }}</span>
                                                            </div>
                                                        </li>
                                                        @php
                                                        $contador++;
                                                        @endphp
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-innr pb-0">
                        <h4 class="card-title">{{ ucfirst(___('Venta de paquetes')) }}</h4>
                    </div>
                    <div class="card-innr pt-0" style="height: 680px">
                        <canvas id="myChartPagos"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('footer')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var paquetesInfo = {!! json_encode($paquetes) !!}
        var totales = {!! json_encode($infoTotales) !!};
        var stock = {!! json_encode($stock) !!};
        var arrayLabels = [];
        paquetesInfo.forEach(function(paquete) {
            arrayLabels.push('Paquetes' + ' ' + paquete.nombre + ' ' + 'Disponibles',
                'Paquetes' + ' ' + paquete.nombre + ' ' + 'Vendidos');
        });
        var arrayDataset = [];
        paquetesInfo.forEach(function(item, index) {
            var randomHue = Math.floor(Math.random() * 360);
            arrayDataset.push({
                backgroundColor: [`hsl(${randomHue}, 100%, 60%)`, `hsl(${randomHue}, 100%, 35%)`],
                data: [item.stock, totales[index + 1] - item.stock]
            });
        });
        const data = {
            labels: arrayLabels,
            datasets: arrayDataset
        };
        const ctx = document.getElementById('myChartPagos');
        var Charts = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        labels: {
                            generateLabels: function(chart) {
                                // Obtener la lista de etiquetas predeterminada
                                const original = Chart.overrides.pie.plugins.legend.labels.generateLabels;
                                const labelsOriginal = original.call(this, chart);
                                // Cree una matriz de colores utilizados en los conjuntos de datos del gráfico
                                let datasetColors = chart.data.datasets.map(function(e) {
                                    return e.backgroundColor;
                                });
                                datasetColors = datasetColors.flat();
                                // Modifica el color y oculta el estado de cada etiqueta.
                                labelsOriginal.forEach(label => {
                                    // Hay el doble de etiquetas que conjuntos de datos. Esto convierte el índice de la etiqueta al índice del conjunto de datos correspondiente
                                    label.datasetIndex = (label.index - label.index % 2) / 2;
                                    // El estado oculto debe coincidir con el estado oculto del conjunto de datos
                                    label.hidden = !chart.isDatasetVisible(label.datasetIndex);
                                    // Cambiar el color para que coincida con el conjunto de datos
                                    label.fillStyle = datasetColors[label.index];
                                });
                                return labelsOriginal;
                            }
                        },
                        onClick: function(mouseEvent, legendItem, legend) {
                            // alternar la visibilidad del conjunto de datos de lo que es actualmente
                            legend.chart.getDatasetMeta(
                                legendItem.datasetIndex
                            ).hidden = legend.chart.isDatasetVisible(legendItem.datasetIndex);
                            legend.chart.update();
                        }
                    },
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                const labelIndex = (context.datasetIndex * 2) + context.dataIndex;
                                return context.chart.data.labels[labelIndex] + ': ' + context.formattedValue;
                            }
                        }
                    }
                }
            },
        });
        // Ajustar el tamaño del contenedor del gráfico
        Charts.canvas.parentNode.style.height = '100%';
        Charts.canvas.parentNode.style.width = '100%';
    </script>
@endpush
