@extends('layouts.admin')
@section('title', ___('Tipos de pagos'))
@section('content')
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            @include('layouts.messages')
            <div class="row">
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-innr">
                            <h4 class="card-title">{{ ___('Tipos de pagos') }}</h4>
                            <div data-simplebar="init" style="max-height: 550px;">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper"
                                                style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">
                                                    <ul class="list-unstyled activity-wid">
                                                        @php
                                                            $contador = 1;
                                                        @endphp
                                                        @foreach ($array_pagos as $moneda)
                                                            <li
                                                                class="list-group-item d-flex justify-content-between align-items-center">
                                                                <div class="col-1 p-0">
                                                                    <small> # {{ $contador }}</small>
                                                                </div>
                                                                <div class="col-2 p-0 text-center">
                                                                    <img loading="lazy"
                                                                        src="{{ asset('imagenes/logos/' . strtoupper($moneda['imagen']) . '.png') }}"
                                                                        alt=""
                                                                        style="border-radius: 50%;width: 28px;height: 28px;">
                                                                </div>
                                                                <div class="col-3 p-0">
                                                                    <small>
                                                                        {{ $moneda['moneda'] }}
                                                                    </small>
                                                                </div>
                                                                <div class="col-3 text-end p-0">
                                                                    <small>
                                                                        {{ $moneda['monto_moneda'] }}
                                                                    </small>
                                                                </div>
                                                                <div class="col-3 text-end p-0">
                                                                    <small id="">{{ $moneda['monto_usd'] }}
                                                                        $</small>
                                                                </div>
                                                            </li>
                                                            @php
                                                                $contador++;
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-innr pb-0">
                            <h4 class="card-title">{{ ___('Tipos de pagos') }}</h4>
                        </div>
                        <div class="card-innr pt-0" style="height: 541px">
                            <canvas id="myChartPagos"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        const ctx = document.getElementById('myChartPagos');
        let array = {!! json_encode($label_moneda) !!}
        let array2 = {!! json_encode($label_cantidad) !!}
        Charts(array, array2)

        function Charts(array, array2) {
            Charts1 = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: array,
                    datasets: [{
                        label: 'USD',
                        data: array2,
                        borderWidth: 1,
                        borderColor: '#00000',
                        backgroundColor: '#51C1C0',
                    }]
                },
                options: {
                    indexAxis: 'y',
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
        // Echo.channel('refresh-analytics').listen('.analytics.admin.event', (data) => {
        //     Charts1.destroy();
        //     Charts(data.array, data.array2);
        //     for (let i = 0; i < data.array.length; i++) {
        //         $('#' + data.array[i]).text(data.array2[i]);
        //     }
        // });
    </script>
@endpush
