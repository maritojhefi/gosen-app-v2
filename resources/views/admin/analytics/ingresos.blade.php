@extends('layouts.admin')
@section('title', ___('Ingresos'))
@section('content')
    @php
        $base_cur = base_currency();
        $sm_class = strlen($stage->stage->total_tokens) > 12 ? ' sm' : '';
    @endphp
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            @include('layouts.messages')
            <div class="row">
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ ___('Ingresos USD') }}</h4>
                            <div id="scroll-vertical-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="dataTables_scroll">
                                            <div class="dataTables_scrollHead"
                                                style="overflow: hidden; position: relative; border: 0px; width: 100%;">
                                                <div class="dataTables_scrollHeadInner"
                                                    style="box-sizing: content-box; padding-right: 17px;">
                                                    <table class="table dt-responsive nowrap w-100 dataTable no-footer"
                                                        role="grid" style="margin-left: 0px;">
                                                        <thead>
                                                            <tr class="table-info text-center">
                                                                <td>{{ ___('TOTAL') }}:</td>
                                                                <td>{{ $total_usd }}</td>
                                                                <td>USD</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="dataTables_scrollBody"
                                                style="position: relative; overflow: auto; max-height: 455px; width: 100%;">
                                                <table id="scroll-vertical-datatable"
                                                    class="table dt-responsive nowrap w-100 dataTable no-footer dtr-inline text-center"
                                                    role="grid" aria-describedby="scroll-vertical-datatable_info"
                                                    style="width: 100%;">
                                                    <tbody>
                                                        @foreach ($ingresos_usd as $value)
                                                            <tr>
                                                                <td>{{ $value['fecha'] }}</td>
                                                                <td>{{ $value['usd'] }}</td>
                                                                <td>USD</td>
                                                            </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="reg-statistic-graph card card-full-height">
                        <div class="card-innr">
                            <div class="card-head has-aside">
                                <h4 class="card-title card-title-sm">{{ ___('Ingresos USD') }}</h4>
                                <div class="card-opt">
                                    <a href="#"
                                        class="link ucap link-light toggle-tigger toggle-caret">{{ isset($_GET['user']) ? $_GET['user'] : 15 }}
                                        {{ ___('Days') }}</a>
                                    <div class="toggle-class dropdown-content">
                                        <ul class="dropdown-list">
                                            <li><a href="{{ url()->current() }}?user=7">7 {{ ___('Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?user=15">15 {{ ___('Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?user=30">30 {{ ___('Days') }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-statistics mb-0" style="height: 480px">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ ___('Ingresos de tokens') }}</h4>
                            <div id="scroll-vertical-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="dataTables_scroll">
                                            <div class="dataTables_scrollHead"
                                                style="overflow: hidden; position: relative; border: 0px; width: 100%;">
                                                <div class="dataTables_scrollHeadInner"
                                                    style="box-sizing: content-box; padding-right: 17px;">
                                                    <table class="table dt-responsive nowrap w-100 dataTable no-footer"
                                                        role="grid" style="margin-left: 0px;">
                                                        <thead>
                                                            <tr class="table-info text-center">
                                                                <td>{{ ___('TOTAL') }}:</td>
                                                                <td>{{ $total_tokens }}</td>
                                                                <td>{{ token_symbol() }}</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="dataTables_scrollBody"
                                                style="position: relative; overflow: auto; max-height: 455px; width: 100%;">
                                                <table id="scroll-vertical-datatable"
                                                    class="table dt-responsive nowrap w-100 dataTable no-footer dtr-inline text-center"
                                                    role="grid" aria-describedby="scroll-vertical-datatable_info"
                                                    style="width: 100%;">
                                                    <tbody>
                                                        @foreach ($ingresos_tokens as $value)
                                                            <tr>
                                                                <td>{{ $value['fecha'] }}</td>
                                                                <td>{{ $value['tokens'] }}</td>
                                                                <td>{{ token_symbol() }}</td>
                                                            </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="token-sale-graph card card-full-height">
                        <div class="card-innr">
                            <div class="card-head has-aside">
                                <h4 class="card-title card-title-sm">{{ ___('Token Sale Graph') }}</h4>
                                <div class="card-opt">
                                    <a href="#"
                                        class="link ucap link-light toggle-tigger toggle-caret">{{ isset($_GET['chart']) ? $_GET['chart'] : 15 }}
                                        {{ ___('Days') }}</a>
                                    <div class="toggle-class dropdown-content">
                                        <ul class="dropdown-list">
                                            <li><a href="{{ url()->current() }}?chart=7">{{ ___('7 Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?chart=15">{{ ___('15 Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?chart=30">{{ ___('30 Days') }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-tokensale chart-tokensale-long" style="height: 480px">
                                <canvas id="tknSale"></canvas>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ ___('Ingresos Bonus Staking') }}</h4>
                            <div id="scroll-vertical-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="dataTables_scroll">
                                            <div class="dataTables_scrollHead"
                                                style="overflow: hidden; position: relative; border: 0px; width: 100%;">
                                                <div class="dataTables_scrollHeadInner"
                                                    style="box-sizing: content-box; padding-right: 17px;">
                                                    <table class="table dt-responsive nowrap w-100 dataTable no-footer"
                                                        role="grid" style="margin-left: 0px;">
                                                        <thead>
                                                            <tr class="table-info text-center">
                                                                <td>{{ ___('TOTAL') . ' Tokens' }}:</td>
                                                                <td>{{ $total_ingresos_staking }}</td>
                                                                <td>{{ token_symbol() }}</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="dataTables_scrollBody"
                                                style="position: relative; overflow: auto; max-height: 455px; width: 100%;">
                                                <table id="scroll-vertical-datatable"
                                                    class="table dt-responsive nowrap w-100 dataTable no-footer dtr-inline text-center"
                                                    role="grid" aria-describedby="scroll-vertical-datatable_info"
                                                    style="width: 100%;">
                                                    <tbody>
                                                        @foreach ($ingresos_staking as $value)
                                                            <tr>
                                                                <td>{{ $value['fecha'] }}</td>
                                                                <td>{{ $value['tokens'] }}</td>
                                                                <td>{{ token_symbol() }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="reg-statistic-graph card card-full-height">
                        <div class="card-innr">
                            <div class="card-head has-aside">
                                <h4 class="card-title card-title-sm">{{ ___('Ingresos bonus staking') }}</h4>
                                <div class="card-opt">
                                    <a href="#"
                                        class="link ucap link-light toggle-tigger toggle-caret">{{ isset($_GET['user']) ? $_GET['user'] : 15 }}
                                        {{ ___('Days') }}</a>
                                    <div class="toggle-class dropdown-content">
                                        <ul class="dropdown-list">
                                            <li><a href="{{ url()->current() }}?user=7">7 {{ ___('Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?user=15">15 {{ ___('Days') }}</a></li>
                                            <li><a href="{{ url()->current() }}?user=30">30 {{ ___('Days') }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-statistics mb-0" style="height: 480px">
                                <canvas id="myChart2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script type="text/javascript">
        var tnx_labels = [<?= $trnxs->chart->days ?>],
            tnx_data = [<?= $trnxs->chart->data ?>],
            theme_color = {
                base: "<?= theme_color('base') ?>",
                text: "<?= theme_color('text') ?>",
                heading: "<?= theme_color('heading') ?>"
            },
            phase_data = [{{ round($stage->stage->soldout, 2) }},
                {{ $stage->stage->total_tokens - $stage->stage->soldout > 0 ? round($stage->stage->total_tokens - $stage->stage->soldout, 2) : 0 }}
            ];
    </script>

    <script src="{{ asset('assets/js/admin.chart.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const ctx = document.getElementById('myChart');
        let array = [<?= $data_usd['days'] ?>];
        let array2 = [<?= $data_usd['data'] ?>];
        Charts(array, array2)

        function Charts(array, array2) {
            Charts = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: array,
                    datasets: [{
                        data: array2,
                        label: 'USD',
                        borderColor: '{{ asset(theme_color()) }}',
                        backgroundColor: '#fff',
                        pointStyle: 'circle',
                        pointRadius: 6,
                        pointHoverRadius: 10
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false
                        },
                    },
                    scales: {
                        y: {
                            beginAtZero: true
                        },
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
    </script>
    <script>
        const ctx2 = document.getElementById('myChart2');
        let array3 = [<?= $data_staking['days'] ?>];
        let array4 = [<?= $data_staking['data'] ?>];
        Charts(array3, array4)

        function Charts(array, array2) {
            Charts = new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: array3,
                    datasets: [{
                        data: array4,
                        label: 'USD',
                        borderColor: '{{ asset(theme_color()) }}',
                        backgroundColor: '#fff',
                        pointStyle: 'circle',
                        pointRadius: 6,
                        pointHoverRadius: 10
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false
                        },
                    },
                    scales: {
                        y: {
                            beginAtZero: true
                        },
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
    </script>
@endpush
