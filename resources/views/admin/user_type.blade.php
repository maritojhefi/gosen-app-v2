@extends('layouts.admin')
@section('title', ___('Type User'))

@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr card-innr-fix">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">{{___('User')}}<em class="ti ti-angle-right fs-14"></em> <small class="tnx-id">{{ set_id($user->id) }}</small></h4>
                    <div class="d-flex align-items-center guttar-20px">
                        <div class="flex-col d-sm-block d-none">
                            <a href="{{ (url()->previous()) ? url()->previous() : route('admin.users') }}" class="btn btn-sm btn-auto btn-primary"><em class="fas fa-arrow-left mr-3"></em>{{___('Back')}}</a>
                        </div>
                        <div class="flex-col d-sm-none">
                            <a href="{{route('admin.users')}}" class="btn btn-icon btn-sm btn-primary"><em class="fas fa-arrow-left"></em></a>
                        </div>
                        
                    </div>
                </div>
                <div class="gaps-1-5x"></div>
                <div class="data-details d-md-flex">
                    <div class="fake-class">
                        
                        <!--<form method="POST" action="{{ route('admin.users.menu', [$user->id] ) }}">-->
                        <form method="POST" action="{{ route('admin.users.menu', $user->id)  }}">
                            

                            @csrf
                            <label>
                                {{___('Menu')}} <br><br>
                                
                                <select class="select select-block select-bordered" name="menu_show_as">
                                    @foreach ($menu as $item)
                                        <option value="{{ $item->id }}" {{ ( $item->id == $user->type_user + 1) ? 'selected' : '' }}> {{ $item->valor }} </option>
                                    @endforeach   
                                    <!--<option {{ $user->type_user == 'basico' ? 'selected ' : '' }}value="basico">Básico</option>
                                    <option {{ $user->type_user == 'intermedio' ? 'selected ' : '' }}value="intermedio">Intermedio</option>
                                    <option {{ $user->type_user == 'avanzado' ? 'selected ' : '' }}value="avanzado">Avanzado</option>-->
                                </select>
                            </label>
                            
                            <br><br>
                            <button type="submit" class="btn btn-primary">{{ ___('Update') }}</button>
                        </form>
                    
                    </div>
                    
                </div>
                <div class="gaps-3x"></div>
              
                
            </div>{{-- .card-innr --}}
        </div>{{-- .card --}}
    </div>{{-- .container --}}
</div>{{-- .page-content --}}


@endsection
