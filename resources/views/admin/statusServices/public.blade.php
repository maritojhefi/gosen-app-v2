<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Font Awesome Icons -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
    <title>Estado de los servicios de Disruptive</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css"
        integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Estilos personalizados -->

    <style>
        body {
            font-family: 'Roboto', sans-serif;
            background-color: #333333;
        }

        .container {
            background-color: #555555;
            border-radius: 10px;
        }

        .card {
            border: solid 1px #555555;
            background-color: #333333 !important;
            border-radius: 10px;
        }

        .contenedor-estado {
            background-color: #424949 !important;
            border-radius: 10px;
            padding: 6px 0px 1px 0px;
        }

        .card-title {
            font-size: 2rem;
        }

        .card-subtitle {
            color: #EAEDED;
        }

        .card:hover {
            transform: scale(1.05);
        }
    </style>
    <script>
        window.PUSHER_APP_KEY = '{{ config('broadcasting.connections.pusher.key') }}';
        window.APP_DEBUG = {{ config('app.debug') ? 'true' : 'false' }};
    </script>
    <script src="{{ asset('assets/websockets.js') }}"></script>
    <script>
        Echo.channel('refresh-status').listen('.refresh.event', (data) => {
            toastr.warning('Hay cambios en los servicios, actualizando pagina...');
            setTimeout(() => {
                location.reload();
            }, 3000);
        });
    </script>
</head>

<body class="dark-mode">
    <div class="container mt-5">
        <div class="row p-3">
            <h4 class="mb-3 text-center mt-3 mb-4 text-white">Estado de los servicios de Disruptive</h4>
            @foreach ($servicios as $servicio)
                @php
                    $estadoServicio = $servicio->color == 'success' ? 'Activo' : ($servicio->color == 'warning' ? 'Advertencia' : 'Inactivo');
                @endphp
                <div class="col-sm-6 col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-header bg-{{ $servicio->color }} text-white fw-bolder">
                            {{ strtoupper($servicio->service) }}
                        </div>
                        <div class="card-body">
                            <div class="contenedor-estado">
                                <h1 class="card-title text-center text-{{ $servicio->color }} fw-bold">
                                    {{ $estadoServicio }}
                                </h1>
                            </div>
                            <div class="mt-3">
                                <p class="card-subtitle text-center">Última actualización: {{ $servicio->updated_at }}
                                </p>
                                <p class="card-subtitle text-center mb-1">Código de respuesta:
                                    <span class="text-{{ $servicio->color }} fw-bold">{{ $servicio->status }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"
        integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>
