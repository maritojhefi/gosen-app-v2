@extends('layouts.admin')
@section('title', ___('Edit Page'))

@push('header')
<link preload href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<style>
    .select2-container {
    font-family: fontAwesome,"Roboto",sans-serif;
}
.select2-container .select2-selection--single {
    height: 42px!important;
}
</style>
@endpush

@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head has-aside">
                    <h4 class="card-title">{{___('Edit Page')}} "<span class="text-primary">{{ __($page_data->menu_title) }}</span>"</h4>
                    <div class="card-opt">
                        <ul class="btn-grp btn-grp-block guttar-20px">
                            <li>
                                <a href="{{ route('admin.pages') }}" class="btn btn-auto  btn-primary btn-sm"><i class="fas fa-arrow-left"></i><span>{{___('Back')}}</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="{{ route('admin.ajax.pages.update') }}" class="validate-modern" method="POST" id="update_page">
                    @csrf
                    <input type="hidden" name="page_id" value="{{ $page_data->id }}">
                    <div class="msg-box"></div>
                    <div class="input-item input-with-label">
                        <label for="menu_title" class="input-item-label">{{___('Menu Name')}}</label>
                        <div class="input-wrap">
                            <input name="menu_title" id="menu_title" class="input-bordered" required value="{{ __($page_data->menu_title) }}" type="text">
                        </div>
                    </div>
                    <div class="input-item input-with-label">
                        <label for="menu_title" class="input-item-label">{{___('Slug')}}</label>
                        <div class="input-wrap">
                            <input name="custom_slug" id="custom_slug" class="input-bordered" required value="{{ $page_data->custom_slug }}"{{ ($page_data->slug=='referral') ? ' readonly' : '' }} type="text">
                        </div>
                    </div>
                    <div class="input-item input-with-label">
                        <label for="title" class="input-item-label">{{___('Page Title')}}</label>
                        <div class="input-wrap">
                            <input name="title" id="title" class="input-bordered" value="{{ $page_data->title }}" type="text" required="">
                        </div>
                    </div>
                    <div class="input-item  input-with-label">
                        <label for="description" class="input-item-label">{{___('Page Content')}}</label>
                        <div class="input-wrap">
                            <textarea id="description" name="description" class="input-bordered input-textarea editor" >{{ $page_data->description }}</textarea>
                            <span class="input-note"><strong>Available Short-codes : </strong> [[token_name]], [[token_symbol]], [[site_name]], [[site_email]], [[whitepaper_download_link]], [[whitepaper_download_button]]</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="input-item input-with-label">
                                <label for="status" class="input-item-label">{{___('Visibility')}}</label>
                                <select name="status" id="status" class="select select-bordered select-block">
                                    <option {{ $page_data->status == 'active' ? 'selected' : '' }} value="active">{{___('Show')}}</option>
                                    <option {{ $page_data->status == 'hide' ? 'selected' : '' }} value="hide">{{___('Hide')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="input-item input-with-label">
                                <label for="type_user" class="input-item-label">{{___('Type User')}}</label>
                                {{-- <select name="type_user" id="type_user" class="select select-bordered select-block">
                                    <option {{ $page_data->type_user == '0' ? 'selected' : '' }} value="1">{{___('All')}}</option>
                                    <option {{ $page_data->type_user == '12' ? 'selected' : '' }} value="1">{{___('Level 1 and 2')}}</option>
                                    <option {{ $page_data->type_user == '1' ? 'selected' : '' }} value="1">{{___('Level 1')}}</option>
                                    <option {{ $page_data->type_user == '2' ? 'selected' : '' }} value="2">{{___('Level 2')}}</option>
                                    <option {{ $page_data->type_user == '3' ? 'selected' : '' }} value="3">{{___('Level 3')}}</option>
                                </select> --}}
                                <select class="select select-bordered select-block form-control js-example-tags" multiple="multiple" name="type_user[]">
                                    <option {{ $page_data->type_user == '1' ? 'selected' : '' }} value="1">{{___('Level 1')}}</option>
                                    <option {{ $page_data->type_user == '2' ? 'selected' : '' }} value="2">{{___('Level 2')}}</option>
                                    <option {{ $page_data->type_user == '3' ? 'selected' : '' }} value="3">{{___('Level 3')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            @include('admin/includes/edit-page-icon-select-include')
                        </div>
                    </div>
                    <div class="gaps-1x"></div>
                    <button type="submit" class="btn btn-md btn-primary ucap">{{___('Update Page')}}</button>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection
@push('header')
<link preload href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

@endpush

@push('footer')
<link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css')}}?ver=1.0">
<script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>

<script type="text/javascript">
   

    $( document ).ready(function() {

        (function($) {
        if($('.editor').length > 0){
            $('.editor').trumbowyg({autogrow: true});
        }
        var $_form = $('form#update_page');
        if ($_form.length > 0) {
            ajax_form_submit($_form, false);
        }
    })(jQuery);

        $(".js-example-tags").select2({
        tags: true
    });
    console.log( "ready!" );
});

    
</script>
@endpush
