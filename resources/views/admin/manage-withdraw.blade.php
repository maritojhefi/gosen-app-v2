@extends('layouts.admin')
@section('title', ___('Withdraw Manage'))

@section('content')

<div class="page-content">
    <div class="container">
        @include('vendor.notice')
            <div class="row">
                <div class="col">
                    <div class="content-area card">
                        <div class="card-innr">
                            @include('layouts.messages')
                            <div class="card-head has-aside">
                                <h4 class="card-title">Manage Withdraw</h4>
                            </div>
                            <div class="gaps-1x"></div>
                            <table class="table table-even-odd table-page table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th>Withdraw Title</th>
                                        <th><span>Status</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cardWithdraw as $card)
                                        <tr class="page-{{ $card->id }}">
                                            <td>
                                                <h5>{{ $card->field }}</h5>
                                            </td>
                                            <td class="text-right">
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch switch" name="token_wallet_req" {{ showWithdrawCardStatus($card->value)==1 ? 'checked ' : 0 }} type="checkbox" id="{{ $card->id }}">
                                                    <label for="{{ $card->id }}"><span>Disabled</span><span
                                                            class="over">Enabled</span></label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <link preload href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css') }}?ver=1.0">
    <script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>

    <script type="text/javascript">
        $(".switch").click(function() {
            $.ajax({
                        method: "get",
                        url: "update/status/withdraw/" + $(this).attr('id')
                    })
        });

    </script>
@endpush
