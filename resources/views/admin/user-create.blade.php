@extends('layouts.admin')
@section('content')
    <div class="page-content" id="addUser" tabindex="-1">
        <div class="container">
            @include('layouts.messages')
            @include('vendor.notice')
            <div class="card content-area content-area-mh">
                <div class="popup-body popup-body-md">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{ ___('Add New User') }}</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                @if (gup('user') || gup('view_user'))
                                    <li{!! is_page('users') || is_page('users.user') || is_page('users.admin') ? ' class="active"' : '' !!}>
                                        <a href="{{ route('admin.users', 'user') }}"
                                            class="btn btn-sm btn-auto btn-primary"><em class="fas fa-list"></em>
                                            {{ ___('Users List') }}</a>
                                        </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <form class="adduser-form validate-modern" id="addUserForm" autocomplete="false">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('User Type') }}</label>
                                    <select name="role" class="select select-bordered select-block" required="required">
                                        <option value="user">
                                            {{ ___('Regular') }}
                                        </option>
                                        <option value="admin">
                                            {{ ___('Admin') }}
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="input-item-label">{{ ___('UserName') }}</label>
                            <div class="input-wrap">
                                <input name="username" class="input-bordered" minlength="3" required="required"
                                    type="text" placeholder="{{ ___('UserName') }}" value="">
                            </div>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="input-item-label">{{ ___('Full Name') }}</label>
                            <div class="input-wrap">
                                <input name="name" class="input-bordered" minlength="3" required="required"
                                    type="text" placeholder="{{ ___('User Full Name') }}" value="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Email Address') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" required="required" name="email" type="email"
                                            placeholder="{{ ___('Email address') }}" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Password') }}</label>
                                    <div class="input-wrap">
                                        <input name="password" class="input-bordered" minlength="6"
                                            placeholder="{{ ___('Automatically generated if blank') }}" type="password"
                                            autocomplete='new-password' value="" s>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-item">
                            <input checked class="input-checkbox input-checkbox-sm" name="email_req" id="send-email"
                                type="checkbox">
                            <label for="send-email">{{ ___('Required Email Verification') }}
                            </label>
                        </div>
                        <div class="gaps-1x"></div>
                        <button class="btn btn-md btn-primary" type="submit">{{ ___('Add User') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#addUserForm').submit(function(event) {
                event.preventDefault();
                var ruta = "{{ route('admin.ajax.users.add') }}";
                var datos = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: ruta,
                    data: datos,
                    success: function(data) {
                        show_toast(data.msg, data.message);
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    },
                    error: function(xhr, status, error) {
                        show_toast(data.msg, data.message);
                    }
                })
            });
        });
    </script>
@endpush
