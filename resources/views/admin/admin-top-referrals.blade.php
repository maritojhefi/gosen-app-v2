@extends('layouts.admin')
@section('gosencss')
<style>
    div.dataTables_wrapper div.dataTables_length {
        text-align: left;
    }

    div.dataTables_wrapper div.dataTables_length select {
        width: 60px;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_info {
        text-align: left;
        padding: 22px 0 0;


    }

    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        justify-content: flex-end;
    }

    div.dataTables_wrapper div.dataTables_filter label:before {
        position: relative;
        height: 36px;
        width: 30px;
        text-align: center;
        line-height: 36px;
        content: "\e610";
        color: #abbbd9;
        font-size: 15px;
    }

    .page-item.active .page-link {
        z-index: 3;
        color: black;
        background-color: #fff;
        border-color: black;
    }

</style>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
@endsection

@section('content')

    <div class="page-content">
        <div class="container">
            @include('layouts.messages')
            @include('vendor.notice')
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{___('Top Referrarls')}}</h4>

                    </div>
                    <table id="tableID" class="data-table table-responsive-lg" style="width:100%">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col data-col-wd-md filter-data dt-user">#</th>
                                <th class="data-col data-col-wd-md filter-data dt-user">Id</th>
                                <th class="data-col data-col-wd-md filter-data dt-user">User</th>
                                <th class="data-col data-col-wd-md dt-referrals">{{___('Total Referrals')}}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $Fila = 0;
                            @endphp
                            @foreach ($array as $item)
                                <tr class="data-item">
                                    <td class="data-col data-col-wd-md dt-user">
                                        {{ $Fila = $Fila + 1 }}
                                    </td>
                                    <td class="data-col data-col-wd-md dt-user">
                                        {{ $item['id'] }}
                                    </td>
                                    <td class="data-col data-col-wd-md dt-name">
                                        {{ $item['nombre'] }}
                                    </td>
                                    <td class="data-col data-col-wd-md dt-referral">
                                        {{ $item['cantidad'] }}
                                    </td>
                                    <td class="data-col text-right">
                                        <div class="card-opt data-action-list">
                                            <ul class="btn btn-auto btn-sm btn-primary">
                                                <li>
                                                    <a href="javascript:void(0)" data-uid="{{ $item['id'] }}"
                                                        data-type="referrals"
                                                        class="user-form-action user-action text-white"><em
                                                            class="fas fa-users"></em> {{___('Referrals')}}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        /* Initialization of datatable */
        $(document).ready(function() {
            $('#tableID').DataTable({});
        });
    </script>
@endsection
