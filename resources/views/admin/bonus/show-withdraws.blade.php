@extends('layouts.admin')
@php($has_sidebar = true)
@push('header')
    <style>
        @media (min-width:1190px) {
            #tabla {
                overflow-x: hidden;
            }
        }

        .rows-flex {
            display: flex;
            justify-content: center;
        }

        .align-row-divs {
            align-self: stretch;
            margin-bottom: 2px;
        }

        .dropdown-left[style] {
            right: 0 !important;
            left: auto !important;
        }
    </style>
@endpush
@section('content')
    @include('partials.messages-admin')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="token-statistics card card-token card-full-height" style="">
                        <div class="card-innr"
                            style="background-image: linear-gradient(rgb(80 78 78 / 65%), rgb(0 0 0 / 85%))  '); color:black;border-radius:20px; ">
                            <div class="token-balance token-balance-with-icon">
                                <div class="token-balance-icon">
                                    <i class="fa fa-wallet" style="font-size:25px"></i>
                                </div>
                                <div class="token-balance-text">
                                    <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">
                                        {{ ___('Users Withdrawal Bonuses') }}</h6>
                                    <span class="lead" style="font-size: 25px"> {{ $trnxAll->count() }}
                                        <span> {{ ___('ALL WITHDRAWAL BONUSES') }}
                                        </span></span>
                                </div>
                            </div>
                            <div class="token-balance token-balance-s2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row row-cols-3 rows-flex">
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('APPROVED') }}</h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonus->sum('amount'), 2, '.', '') }} (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonus->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('CANCELED') }}</h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonusCanceled->sum('amount'), 2, '.', '') }}
                                            (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonusCanceled->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('PENDING') }}
                                        </h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonusPending->sum('amount'), 2, '.', '') }} (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonusPending->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gaps-1x mgb-0-5x "></div>
                <div class="card content-area content-area-mh">
                    @livewire('admin.withdraw-bonus-list-component', ['transaction' => $trnxWith])
                </div>
            </div>
        </div>
    @endsection
    @section('modals')
        @if ($tipo == 'withdraw')
            @foreach ($trnxWith as $trnx)
                <div class="modal fade" id="modal-small{{ $trnx->id }}" tabindex="-1" style="display: none;"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
                        <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                                aria-label="Close"><em class="ti ti-close"></em></a>
                            <div class="popup-body text-center">
                                <form class="" method="post" action="{{ route('admin.withdraw.dollars') }}">
                                    @csrf
                                    <div class="col-12" style="text-align:center; font-size: 19px; letter-spacing: 3px;">
                                        <strong>
                                            {{ ___('Are you sure you want to approve this dollar withdrawal') }}?
                                        </strong>
                                    </div>
                                    <div class="content-area card stage-card mt-3">
                                        <div class="card-innr">
                                            <div class="card-head row" style="padding-bottom: 0px;">
                                                <div class="col-9">
                                                    <div class="input-item">
                                                        <input type="checkbox" class="input-checkbox checkbox col-2"
                                                            name="check" id="CheckBox" checked>
                                                        <label for="CheckBox"
                                                            class="col-10">{{ ___('Send Mail') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-item input-with-label contHash" id="contHash">
                                                <label class="input-item-label">
                                                    {{ ___('Enter the Hash of the Transaction') }} (
                                                    {{ ___('Optional') }} )</label>
                                                <input class="input-bordered" type="text" name="hash"
                                                    placeholder="{{ ___('Write the Hash') }}">
                                            </div>
                                        </div><!-- .card-innr -->
                                    </div>
                                    <input type="hidden" name="status" value="{{ $trnx->status }}">
                                    <input type="hidden" name="id" value="{{ Crypt::encrypt($trnx->id) }}">
                                    <input type="hidden" name="user_id" value="{{ Crypt::encrypt($trnx->user_id) }}">
                                    <input type="hidden" name="amount" value="{{ $trnx->amount }}">
                                    <div class="col mt-4">
                                        <button type="submit" id="confirm" class="btn btn-primary mr-4">
                                            {{ ___('Approve') }}
                                        </button>
                                        <button type="button" data-dismiss="modal" class="btn btn-danger ml-4">
                                            {{ ___('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- .modal-content -->
                    </div><!-- .modal-dialog -->
                </div>
                <div class="modal fade" id="modal-smallo{{ $trnx->id }}" tabindex="-1" style="display: none;"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
                        <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                                aria-label="Close"><em class="ti ti-close"></em></a>
                            <div class="popup-body text-center">
                                <form class="" method="post"
                                    action="{{ route('admin.withdraw.dollars.cancel') }}">
                                    @csrf
                                    <div class="col-12 " style="text-align:center;">
                                        <strong>
                                            {{ ___('Are you sure you want to decline this dollar withdrawal') }}?
                                        </strong>
                                    </div>
                                    <input type="hidden" name="status" value="{{ $trnx->status }}">
                                    <input type="hidden" name="id" value="{{ Crypt::encrypt($trnx->id) }}">
                                    <input type="hidden" name="user_id" value="{{ Crypt::encrypt($trnx->user_id) }}">
                                    <div class="input-item input-with-label"><label
                                            class="input-item-label">{{ ___('Write the reason') }}</label>
                                        <textarea name="comment" class="input-bordered input-textarea"
                                            placeholder="{{ ___('Write the reason why the transaction will be rejected') }}"></textarea>
                                    </div>
                                    <div class="col mt-4">
                                        <button type="button" data-dismiss="modal" class="btn"
                                            style="background: gray;">
                                            {{ ___('Cancel') }}
                                        </button>
                                        <button type="submit" id="decline" class="btn btn-danger mr-4">
                                            {{ ___('Decline') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- .modal-content -->
                    </div><!-- .modal-dialog -->
                </div>

                <div class="modal fade" id="modal-medium2{{ $trnx->id }}" tabindex="-1"
                    style="padding-right: 17px;">
                    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                        <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                                aria-label="Close"><em class="ti ti-close"></em></a>
                            <div class="popup-body">
                                <div class="content-area popup-content">
                                    <div class="card-head d-flex justify-content-between align-items-center">
                                        <h4 class="card-title mb-0">{{ ___('Retiro de bonos en dolares') }}
                                            ({{ $trnx->code }})
                                        </h4>
                                    </div>
                                    <div class="trans-status">
                                        <span
                                            class="badge badge-{{ $trnx->status == 'canceled' ? 'danger' : 'warning' }} ucap">
                                            {{ ___($trnx->status) }}
                                        </span>
                                    </div>
                                    <div class="p-2"></div>
                                    <h6 class="card-sub-title">{{ ___('Transaction Bonus Details') }}</h6>
                                    <ul class="data-details-list">
                                        <li>
                                            <div class="data-details-head">ID {{ ___('Bonus') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->code }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Name o Username') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->userBy->name ? $trnx->userBy->name : $trnx->userBy->username }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Email') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->userBy->email }}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Type User') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->userBy->type_user }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Aprobado por el usuario') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->approved_bonus == 1 ? 'Si' : 'No' }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Amount') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->amount }} (USDT)
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Wallet of this withdrawal') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->wallet }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Date') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->created_at }}
                                                </span>
                                                <span>{!! fechaOrden(\Carbon\Carbon::parse($trnx->created_at)) !!} </span>
                                            </div>
                                        </li>
                                        @if ($trnx->comment != null)
                                            <li>
                                                <div class="data-details-head">{{ ___('Comment') }}</div>
                                                <div class="data-details-des">
                                                    <span>{{ $trnx->comment }}</span>
                                                </div>
                                            </li>
                                        @endif
                                </div>
                            </div>
                        </div><!-- .modal-content -->
                    </div><!-- .modal-dialog -->
                </div>
            @endforeach
        @endif
    @endsection
    @push('footer')
        <script>
            $(document).ready(function() {
                $("#confirm").click(function() {
                    // disable button
                    $(this).addClass('disabled');
                    // add spinner to button
                    $(this).html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Approving...`
                    );
                });
            })
            $(document).ready(function() {
                $("#decline").click(function() {
                    // disable button
                    $(this).addClass('disabled');
                    // add spinner to button
                    $(this).html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Rejecting...`
                    );
                });
            });
            $(function() {
                $("#CheckBox").change(function() {
                    if ($(this).is(":checked")) {
                        $('.checkbox').attr('checked', true);
                        $('.contHash').removeClass('hide');
                    } else {
                        $('.checkbox').removeAttr('checked');
                        $('.contHash').addClass('hide');
                    }
                });
            });
            $(window).resize(function() {
                if (screen.width < 1219)
                    $('#tabla').addClass('table-responsive');
                else if (screen.width > 1219)
                    $('#tabla').removeClass('table-responsive');
            });
        </script>
    @endpush
