@extends('layouts.admin')
@php($has_sidebar = true)
@push('header')
    <style>
        @media (min-width:1190px) {
            #tabla {
                overflow-x: hidden;
            }
        }

        .rows-flex {
            display: flex;
            justify-content: center;
        }

        .align-row-divs {
            align-self: stretch;
            margin-bottom: 2px;
        }
    </style>
@endpush
@section('content')
    @include('partials.messages-admin')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="token-statistics card card-token card-full-height" style="">
                        <div class="card-innr"
                            style="background-image: linear-gradient(rgb(80 78 78 / 65%), rgb(0 0 0 / 85%))  '); color:black;">
                            <div class="token-balance token-balance-with-icon">
                                <div class="token-balance-icon">
                                    <i class="fa fa-coins" style="font-size:25px"></i>
                                </div>
                                <div class="token-balance-text">
                                    <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">
                                        {{ ___('Referral Bonuses') }}</h6>
                                    <span class="lead" style="font-size: 25px">{{ $bonusReferrals->count() }}<span>
                                            {{ ___('ALL REFERRAL BONUSES') }}</span></span>
                                </div>
                            </div>
                            <div class="token-balance token-balance-s2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row row-cols-2 rows-flex">
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 0 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser0 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title "
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 1 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser1 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 2 Users') }}
                                        </small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser2 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title "
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 3 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser3 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 4 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser4 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 5 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser5 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 6 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser6 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 7 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser7 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 8 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser8 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 align-row-divs">
                                        <small class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('Type 9 Users') }}</small>
                                        <h5 class="sub" style="color: white; font-weight:bold; text-align: center;">
                                            {{ $typeUser9 }} {{ ___('Users') }}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="token-statistics card card-token card-full-height" style="">
                        <div class="card-innr"
                            style="background-image: linear-gradient(rgb(80 78 78 / 65%), rgb(0 0 0 / 85%))  '); color:black;border-radius:20px; ">
                            <div class="token-balance token-balance-with-icon">
                                <div class="token-balance-icon">
                                    <i class="fa fa-wallet" style="font-size:25px"></i>
                                </div>
                                <div class="token-balance-text">
                                    <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">
                                        {{ ___('Users Withdrawal Bonuses') }}</h6>
                                    <span class="lead" style="font-size: 25px"> {{ $trnxAll->count() }}
                                        <span> {{ ___('ALL WITHDRAWAL BONUSES') }}
                                        </span></span>
                                </div>
                            </div>
                            <div class="token-balance token-balance-s2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row row-cols-3 rows-flex">
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('APPROVED') }}</h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonus->sum('amount'), 2, '.', '') }} (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonus->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('CANCELED') }}</h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonusCanceled->sum('amount'), 2, '.', '') }}
                                            (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonusCanceled->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    <div class="align-row-divs">
                                        <h6 class="card-sub-title"
                                            style="color: #1dd69f; font-weight:bold;text-align: center">
                                            {{ ___('PENDING') }}
                                        </h6>
                                        <h3 class="sub"
                                            style="color: white; font-weight:bold; text-align: center;font-size:15px">
                                            {{ number_format((float) $trnxBonusPending->sum('amount'), 2, '.', '') }} (USDT)
                                        </h3>
                                        <span class="sub sub-email text-white" style="text-align: center">(
                                            {{ $trnxBonusPending->count() }} {{ ucfirst(___('transactions')) }} )</span>
                                    </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="gaps-1x mgb-0-5x "></div>
                <div class="card content-area content-area-mh">
                    <div class="card-innr ">
                        <div class="card-head">
                            <h6 class="card-title">{{ ___('Bonus Dollars') }}
                            </h6>
                        </div>
                        <table id="tabla" class="data-table table-hover dt-filter-init">
                            <thead>
                                <tr class="data-item data-head">
                                    <th class="data-col tnx-status dt-tnxno">{{ ___('Bonus') }} ID</th>
                                    <th class=" data-col dt-token">{{ ___('Bonus to') }}</th>
                                    <th class="data-col dt-usd-amount">{{ ___('Level Bonus') }}</th>
                                    <th class="data-col dt-type tnx-typ">{{ ___('Amount') }}</th>
                                    <th class="data-col dt-usd-amount">{{ ___('Type Bonus') }}</th>
                                    <th class="data-col pm-gateway dt-account">{{ ___('Date') }}</th>
                                    <th class="data-col dt-amount">{{ ___('Status') }}</th>
                                    <th class="data-col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bonus as $bon)
                                    <tr class="data-item" id="tnx-item-102">
                                        <td class="data-col">
                                            <div class="d-flex align-items-center">
                                                @if (isset($bon->transaction->tnxUser))
                                                    <div data-toggle="tooltip" data-placement="top" title=""
                                                        class="data-state data-state-approved"
                                                        data-original-title="Approved">
                                                        <span class="d-none"></span>
                                                    </div>
                                                    <span class=" pay-with">
                                                        {{ $tipobono . $bon->id }}
                                                        <span class="sub sub-date">
                                                            {{ ___('from') }}
                                                            {{ $bon->transaction ? $bon->transaction->tnx_id : '' }}
                                                        </span>
                                                    </span>
                                                @else
                                                    <div data-toggle="tooltip" data-placement="top" title=""
                                                        class="data-state data-state-{!! $bon->status !!}"
                                                        data-original-title="{{ $bon->status }}">
                                                        <span class="d-none"></span>
                                                    </div>
                                                    <span class="pay-with">
                                                        {{ ___($bon->code) }}
                                                        <span class="sub sub-date">({{ ucfirst($bon->status) }})</span>
                                                    </span>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="data-col">
                                            <span class="sub sub-s2 pay-with">{{ $bon->userBy ? $bon->userBy->name : '' }}
                                            </span>
                                        </td>
                                        <td class="data-col dt-account">
                                            @if (isset($bon->transaction->tnxUser))
                                                @if ($bon->level_bonus == 0)
                                                    <span class="pay-with badge"
                                                        style="background: #0B132B;border-radius: 10px;">{{ ___('Level') }}
                                                        {{ $bon->level_bonus }}</span>
                                                @else
                                                    @switch($bon->level_bonus)
                                                        @case(1)
                                                            <span class="pay-with badge "
                                                                style="background: #1C2541; border-radius: 10px;">{{ ___('Level') }}
                                                                {{ $bon->level_bonus }}</span>
                                                        @break

                                                        @case(2)
                                                            <span class="pay-with badge "
                                                                style="background:#3A506B; border-radius: 10px;">{{ ___('Level') }}
                                                                {{ $bon->level_bonus }}</span>
                                                        @break

                                                        @case(3)
                                                            <span class="pay-with badge "
                                                                style="background: #41638A; border-radius: 10px;">{{ ___('Level') }}
                                                                {{ $bon->level_bonus }}</span>
                                                        @break

                                                        @case(4)
                                                            <span class="pay-with badge "
                                                                style="background: #457D9B; border-radius: 10px;">{{ ___('Level') }}
                                                                {{ $bon->level_bonus }}</span>
                                                        @break

                                                        @case(5)
                                                            <span class="pay-with badge "
                                                                style="background:#5490B0; border-radius: 10px;">{{ ___('Level') }}
                                                                {{ $bon->level_bonus }}</span>
                                                        @break

                                                        @default
                                                    @endswitch
                                                @endif
                                            @else
                                                <span class="sub sub-s2 pay-with">
                                                    N/A
                                                </span>
                                            @endif
                                        </td>
                                        <td class="data-col ">
                                            <span class="lead amount-pay">{{ $bon->amount }}</span>
                                            <span class="sub sub-symbol">(USDT)</span>
                                        </td>
                                        <td class="data-col dt-account">
                                            @if (isset($bon->transaction->tnxUser))
                                                <span class="sub sub-s2 pay-with">
                                                    {{ ucfirst($bon->type_bonus) }}
                                                    @if ($bon->type_bonus == 'percent')
                                                        <em class="fas fa-info-circle" data-toggle="tooltip"
                                                            data-placement="bottom" title=""
                                                            data-original-title="{{ $bon->type_value }}%"></em>
                                                    @else
                                                        <em class="fas fa-info-circle" data-toggle="tooltip"
                                                            data-placement="bottom" title=""
                                                            data-original-title="{{ $bon->type_value }}(USDT)"></em>
                                                    @endif
                                                </span>
                                            @else
                                                <span class="sub sub-s2 pay-with">
                                                    N/A
                                                </span>
                                            @endif
                                        </td>
                                        <td class="data-col dt-account">
                                            <span class="sub sub-s2 pay-with">
                                                {{ date('jS F, Y', strtotime($bon->created_at)) }}
                                            </span>
                                            <span class="sub sub-date"> {!! fechaOrden(\Carbon\Carbon::parse($bon->created_at)) !!}</span>
                                        </td>
                                        <td class="data-col data-type">
                                            @if (isset($bon->transaction->tnxUser))
                                                <span
                                                    class="dt-type-md badge badge-outline badge-md badge-102 badge-success">
                                                    {{ ___('Send') }}
                                                </span>
                                            @else
                                                <span class="sub sub-s2 pay-with">
                                                    N/A
                                                </span>
                                            @endif
                                        </td>
                                        @if ($bon->status == 'approved')
                                            <td class="data-col text-right">
                                            </td>
                                        @elseif($bon->status == 'canceled')
                                            <td class="data-col text-right">
                                                <div class="relative d-inline-block">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt"></em></a>
                                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                        <ul id="more-menu-102" class="dropdown-list">
                                                            <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-medium2{{ $bon->id }}">
                                                                    <em class="ti ti-eye"></em>
                                                                    {{ ___('View Details') }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        @else
                                            <td class="data-col text-right">
                                                <div class="relative d-inline-block">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt"></em></a>
                                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                        <ul id="more-menu-102" class="dropdown-list">
                                                            <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-medium{{ isset($bon->transaction->tnxUser) ? $bon->id : '2' . $bon->id }}">
                                                                    <em class="ti ti-eye"></em>
                                                                    {{ ___('View Details') }}</a>
                                                            </li>
                                                            @if (isset($bon->transaction->tnxUser))
                                                                <li><a href="#">
                                                                        <em class="ti ti-check"></em>
                                                                        {{ ___('Approved') }}</a>
                                                                </li>
                                                                <li><a href="#">
                                                                        <em class="ti ti-close"></em>
                                                                        {{ ___('Canceled') }}</a>
                                                                </li>
                                                            @else
                                                                <li><a href="#" data-toggle="modal"
                                                                        data-target="#modal-small{{ $bon->id }}">
                                                                        <em class="ti ti-check"></em>
                                                                        {{ ___('Approved') }}</a>
                                                                </li>
                                                                <li><a href="#" data-toggle="modal"
                                                                        data-target="#modal-smallo{{ $bon->id }}">
                                                                        <em class="ti ti-close"></em>
                                                                        {{ ___('Decline') }}</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('modals')
        @foreach ($bonus as $bon)
            @if (isset($bon->transaction->tnxUser))
                <div class="modal fade" id="modal-medium{{ $bon->id }}" tabindex="-1"
                    style="padding-right: 17px;">
                    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                        <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                                aria-label="Close"><em class="ti ti-close"></em></a>
                            <div class="popup-body">
                                <div class="content-area popup-content">
                                    <div class="card-head d-flex justify-content-between align-items-center">
                                        <h4 class="card-title mb-0">{{ ___('Transaction Dollars from') }}
                                            ({{ $bon->transaction ? $bon->transaction->tnx_id : '' }})
                                        </h4>
                                        <div class="trans-status">
                                            <span class="badge badge-success ucap">{{ ___('Send') }}</span>
                                        </div>
                                    </div>
                                    <div class="gaps-2-5x"></div>
                                    <h6 class="card-sub-title">{{ ___('Transaction Bonus Details') }}</h6>
                                    <ul class="data-details-list">
                                        <li>
                                            <div class="data-details-head">ID {{ ___('Bonus') }} </div>
                                            <div class="data-details-des">
                                                <span>{{ $tipobono }}{{ $bon->id }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Main Referral') }}</div>
                                            <div class="data-details-des" style="background: #e8f9ff; ">
                                                <span>{{ $bon->transaction->tnxUser->name }}
                                                </span>
                                                <span>({{ ___('User Type') }}
                                                    {{ $bon->transaction->tnxUser->type_user }})
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Referral') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->userFrom ? $bon->userFrom->name : '' }}
                                                </span>
                                                <span>({{ ___('User Type') }} {{ $bon->userFrom->type_user }})
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('User') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->userBy->name }}</span>
                                                <span>({{ ___('User Type') }} {{ $bon->userBy->type_user }}) </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Amount') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->amount }}</span>(USDT)
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('From Transaction') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->transaction ? $bon->transaction->tnx_id : '' }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ ___('Date') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->created_at }}
                                                </span>
                                                <span>{!! fechaOrden(\Carbon\Carbon::parse($bon->created_at)) !!} </span>
                                            </div>
                                        </li>
                                </div>
                            </div>
                        </div><!-- .modal-content -->
                    </div><!-- .modal-dialog -->
                </div>
            @endif
        @endforeach
    @endsection

    @push('footer')
        <script>
            $(document).ready(function() {
                $("#confirm").click(function() {
                    // disable button
                    $(this).addClass('disabled');
                    // add spinner to button
                    $(this).html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Approving...`
                    );
                });
            })
            $(document).ready(function() {
                $("#decline").click(function() {
                    // disable button
                    $(this).addClass('disabled');
                    // add spinner to button
                    $(this).html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Rejecting...`
                    );
                });
            });
            $(function() {
                $("#CheckBox").change(function() {
                    if ($(this).is(":checked")) {
                        $('.checkbox').attr('checked', true);
                        $('.contHash').removeClass('hide');
                    } else {
                        $('.checkbox').removeAttr('checked');
                        $('.contHash').addClass('hide');
                    }
                });
            });
            $(window).resize(function() {
                if (screen.width < 1219)
                    $('#tabla').addClass('table-responsive');
                else if (screen.width > 1219)
                    $('#tabla').removeClass('table-responsive');
            });
        </script>
    @endpush
