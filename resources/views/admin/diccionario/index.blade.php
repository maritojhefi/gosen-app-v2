@extends('layouts.admin')
@section('title', ___('Diccionario'))

@section('content')
@livewire('diccionario-component')
@endsection