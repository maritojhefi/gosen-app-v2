@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container">
            @include('partials.messages-admin')
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{___('Create Questions')}}</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li>
                                    <a href="{{ route('admin.frequentquestions.index') }}"
                                        class="btn btn-sm btn-auto btn-primary">
                                        <em class="fas fa-list"></em><span>{{___('List')}} <span
                                                class="d-none d-sm-inline-block">{{___('Questions')}}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active ">
                            <form method="post" action="{{ route('admin.frequentquestions.update', $question->id) }}">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">{{___('Questions')}}</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered" required="" type="text"
                                                    data-validation="required" name="titulo"
                                                    value="{{ old('titulo', __($question->titulo)) }}">
                                            </div>
                                        </div>
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">{{___('Observation')}}</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered" required="" type="text"
                                                    data-validation="required" name="observacion"
                                                    value="{{ old('observacion', __($question->observacion)) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">{{___('Content')}}</label>
                                            <div class="input-wrap">
                                                <textarea rows="10" class="input-bordered" required="" type="text" data-validation="required"
                                                    name="contenido">{{ old('required', __($question->contenido)) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gaps-1x"></div>
                                <div class="d-flex">
                                    <button href="" type="submit" class="btn btn-sm btn-auto btn-primary">
                                        <em class="ti ti-reload"></em><span>{{___('Update')}} <span
                                                class="d-none d-sm-inline-block">{{___('Question')}}</span></span>
                                    </button>
                                </div>
                                <div class="gaps-0-5x"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
