@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container">
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{___('Questions')}}</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li>
                                    <a href="{{ route('admin.frequentquestions.create') }}"
                                        class="btn btn-sm btn-auto btn-primary">
                                        <em class="fas fa-plus-circle"></em><span>Add <span
                                                class="d-none d-sm-inline-block">{{___('Questions')}}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init admin-tnx table-responsive-lg">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col dt-id">ID</th>
                                <th class="data-col dt-titulo">Title</th>
                                <th class="data-col dt-contenido">Content</th>
                                <th class="data-col dt-observacion">Observation</th>
                                <th class="data-col text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($questions as $question)
                                <tr class="data-item" id="tnx-item">
                                    <td class="">
                                        {{ $question->id }}
                                    </td>
                                    <td class="data-col dt-titulo">
                                        {{ __($question->titulo) }}
                                    </td>
                                    <td class="data-col dt-contenido">
                                        {{ Str::of(__($question->contenido))->limit(50) }}
                                    </td>
                                    <td class="data-col dt-observacion">
                                        {{ __($question->observacion) }}
                                    </td>
                                    <td class="data-col text-right">
                                        <div class="tools w-100 w-sm-auto">
                                            <ul class="btn-group guttar-8px">
                                                <li><a href="{{ route('admin.frequentquestions.edit', $question->id) }}"
                                                        class="btn btn-light btn-sm btn-icon btn-outline bg-white"
                                                        title={{___('Edit question')}}><em class="fa fa-edit"></em></a>
                                                </li>
                                                <li>
                                                    <form
                                                        action="{{ route('admin.frequentquestions.destroy', $question) }}">
                                                <li> <a href="javascript:void(0)"
                                                        onclick="sweetAlert1({{ $question->id }})"
                                                        class="btn btn-danger btn-icon  btn-sm mr-md-2"
                                                        title={{___('Delete question')}}><em class="ti ti-trash"></em></a></li>
                                                </form>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
<script type="text/javascript">
    function sweetAlert1(id) {
        var html;
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        swal({
            title: "Want to delete question?",
            icon: "warning",
            text: "Please proceed, If you really want to delete all the question permanently from database.",
            buttons: ["Cancel", "Yes"],
            dangerMode: !0,
        }).then((value) => {
            if (value == true) {
                $.ajax({
                    method: "get",
                    url: "delete/question/" + id,
                    success: function(result) {
                        if (result == "eliminado") {
                            swal("Deleted!", "Your Question has been deleted.", "success");
                            setTimeout(function() {
                                window.location.reload();
                            }, 0e1);
                        }
                    }
                })
            }
        });
    }
</script>
@endpush

