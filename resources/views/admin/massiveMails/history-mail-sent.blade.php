@extends('layouts.admin')
@push('header')
    <style>
        @media (min-width:1190px) {
            #tabla {
                overflow-x: hidden;
            }
        }

        @media only screen and (min-width: 1490px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-50 {
                width: 300px !important;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }
        }

        @media(max-width:991px) {
            .texta {
                width: calc(100% - 40px) !important;
            }

        }

        @media (max-width: 1490px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col>div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table,
        td {
            color: #000000;
        }

        a {
            color: #161a39;
            text-decoration: underline;
        }
    </style>
    <link preload href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
@endpush
@section('content')
    <div class="page-content">
        @include('partials.messages-admin')
        <div class="container">
            <div class="gaps-1x mgb-0-5x d-lg-none d-none d-sm-block"></div>
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head">
                        <h6 class="card-title">{{ ___('Sent Email History') }}
                        </h6>
                    </div>
                    <table id="tabla" class="data-table table-responsive table-hover dt-filter-init">
                        <thead>
                            <tr class="data-item">
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">{{ ___('Header') }}
                                </th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">{{ ___('Footer') }}
                                </th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;"> {{ ___('Date') }}
                                </th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">
                                    {{ ___('Type user') }}(s)</th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">
                                    {{ ___('To user') }}(s)</th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">{{ ___('Type Mail') }}
                                </th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;">
                                    {{ ___('Total Users Sent') }}
                                </th>
                                <th class="data-col tnx-status dt-tnxno " style="padding-left: 2rem;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($historys as $his)
                                <tr class="data-item" id="tnx-item-102">
                                    <td class="data-col   " style="padding-left:2rem;">
                                        @if ($his->type_mail == 'design')
                                            <span
                                                class="sub sub-s2 pay-with m-2">{{ $his->header != null ? ___($his->header) : ___('Not available') }}</span>
                                        @else
                                            <span class="sub sub-date m-2">{{ ___('N/A') }}</span>
                                        @endif
                                    </td>
                                    <td class="data-col   " style="padding-left:2rem;">
                                        @if ($his->type_mail == 'design')
                                            <span
                                                class="sub sub-s2 pay-with m-2">{{ $his->footer != null ? ___($his->footer) : ___('Not available') }}</span>
                                        @else
                                            <span class="sub sub-date m-2">{{ ___('N/A') }}</span>
                                        @endif
                                    </td>
                                    <td class="data-col" style="padding-left:2rem;">
                                        <span class="sub sub-s2 pay-with">
                                            {{ date('jS F, Y', strtotime($his->created_at)) }}
                                            <span class="sub sub-date">{!! fechaOrden(\Carbon\Carbon::parse($his->created_at)) !!}</span>
                                        </span>
                                    </td>
                                    <td class="data-col   " style="padding-left:2rem;">
                                        <span class="badge badge-sm badge-primary" type="button" data-toggle="modal"
                                            data-target="#modalDetails{{ $his->id }}"><em
                                                class="fas fa-eye mr-1"></em>
                                            {{ ___('Show') }} </span>
                                    </td>
                                    <td class="data-col" style="padding-left:2rem;">
                                        <span
                                            class="sub sub-s2 pay-with m-2">{{ ___(ucfirst($his->to_specific_users == 1 ? 'Manual' : 'Por tipo de Usuario')) }}</span>
                                    </td>
                                    <td class="data-col" style="padding-left:2rem;">
                                        <span class="sub sub-s2 pay-with m-2">{{ ___(ucfirst($his->type_mail)) }}</span>
                                    </td>
                                    <td class="data-col" style="padding-left:2rem;">
                                        <span class="sub sub-s2 pay-with m-2"> <strong>{{ $his->total }}
                                                {{ ___('Users') }}</strong>
                                        </span>
                                    </td>
                                    <td class="data-col text-right" style="padding-left:2rem;">
                                        <div class="relative d-inline-block m-2">
                                            <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                    class="ti ti-more-alt"></em></a>
                                            <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                <ul id="more-menu-102" class="dropdown-list">
                                                    <li><a href="#" data-toggle="modal"
                                                            data-target="#modal-large{{ $his->id }}">
                                                            <em class="ti ti-eye"></em> {{ ___('View Mail') }}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
    @foreach ($historys as $his)
        <div class="modal fade" id="modalDetails{{ $his->id }}" tabindex="-1" style="padding-right: 17px;">
            <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                            class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <div class="content-area popup-content">
                            <div class="card-head d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-2" style="letter-spacing:2px;">{{ ___('Email sent to') }}
                                    {{ $his->to_specific_users == 1 ? 'Users' : 'Type Users' }}
                                </h4>
                            </div>
                            <ul class="data-details-list" id="lista">
                                @if ($his->to_specific_users == 1)
                                    @foreach ($his->specificUsers() as $item)
                                        <li>
                                            <div class="data-details-head">{{ __('ID Usuario') }} </div>
                                            <div class="data-details-des">
                                                <span>{{ $item->id }}
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="data-details-head">{{ __('Usuario') }} </div>
                                            <div class="data-details-des">
                                                <span>{{ $item->name }} - ({{ ___('Type User') }}
                                                    {{ $item->type_user }})
                                                </span>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    @foreach ($his->types_user() as $item)
                                        <li>
                                            <div class="data-details-head">{{ __('Users Type') }} </div>
                                            <div class="data-details-des">
                                                <span>{{ $item->valor }}
                                                </span>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach





    @foreach ($historys as $his)
        <div class="modal fade" id="modal-large{{ $his->id }}" tabindex="-1" style="padding-right: 17px;">
            <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                        aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <div class="content-area popup-content">
                            <div class="card-head d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-2" style="letter-spacing:2px;">{{ ___('Email Sent') }}
                                    <i class="fa fa-envelope-open"></i>
                                </h4>
                            </div>
                            @if ($his->type_mail == 'design')
                                <table
                                    style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f9f9f9;width:100%"
                                    cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr style="vertical-align: top">
                                            <td
                                                style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: #f9f9f9">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table height="0px"
                                                                                            align="center" border="0"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0" width="100%"
                                                                                            style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                            <tbody>
                                                                                                <tr
                                                                                                    style="vertical-align: top">
                                                                                                    <td
                                                                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                        <span>&#160;</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">

                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">

                                                                                        <table width="100%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('images/logo-mail.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 46%;" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">

                                                                                        <div
                                                                                            style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                            <p
                                                                                                style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                                                <span id="cabecera"
                                                                                                    style="font-size: 28px; line-height: 39.2px; color: #280F53; font-family: Roboto, sans-serif;">
                                                                                                    {{ $his->header != null ? ___($his->header) : ___('Not available') }}
                                                                                                </span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent; margin-top: -4%;">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">

                                                        <div class="texta" id="viewer"
                                                            style="    height: auto;
                                                            width: 560px;
                                                            text-align: center;
                                                            margin: 0 auto; margin-top:20px;">
                                                            {!! ___($his->content) !!}
                                                        </div>
                                                        <table style="font-family:'Roboto',sans-serif;"
                                                            role="presentation" cellpadding="0" cellspacing="0"
                                                            width="100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:45px 40px 0px;font-family:'Roboto',sans-serif;"
                                                                        align="center">
                                                                        <div
                                                                            style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                            <p
                                                                                style="font-size: 14px; line-height: 140%;font-style: italic;">
                                                                                <span id="piedepagina"
                                                                                    style="color: #888888;font-style: italic; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                            class="text-primary"
                                                                                            style="font-size: 16px; font-style: italic; line-height: 22.4px;">{{ $his->footer != null ? ___($his->footer) : ___('Not available') }}</span></em></span><br /><span
                                                                                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                            style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                            </p>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg,#280f534b 100%,#280f534b 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col"
                                                                style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <div align="center">
                                                                                            <p
                                                                                                style="line-height: 140%; font-size: 14px;">
                                                                                                <span
                                                                                                    style="font-size: 14px; line-height: 19.6px;"><span
                                                                                                        style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                                            style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                                            {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table
                                                                            style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table width="70%"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0"
                                                                                            border="0">
                                                                                            <tr>
                                                                                                <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                    align="center">
                                                                                                    <img loading="lazy" align="center"
                                                                                                        border="0"
                                                                                                        src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                                        alt="Image"
                                                                                                        title="Image"
                                                                                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <div align="center">
                                                                                            <a href="https://app.disruptive.center/"
                                                                                                target="_blank"
                                                                                                style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                                                <span
                                                                                                    style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                                        style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                                    </span></span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <div
                                                                                            style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @else
                                <table
                                    style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: f9f9f900;width:100%"
                                    cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr style="vertical-align: top">
                                            <td
                                                style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: f9f9f900">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <table height="0px"
                                                                                            align="center" border="0"
                                                                                            cellpadding="0"
                                                                                            cellspacing="0" width="100%"
                                                                                            style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid f9f9f900;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                            <tbody>
                                                                                                <tr
                                                                                                    style="vertical-align: top">
                                                                                                    <td
                                                                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                        <span>&#160;</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="texta" id="viewer2"
                                                    style="    height: auto;
                                            width: 560px;
                                            text-align: center;
                                            margin: 0 auto; margin-top:20px;">
                                                    {!! $his->content !!}
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-bottom-left-radius: 20px;
                    border-bottom-right-radius: 20px;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col"
                                                                style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <div align="center">
                                                                                            <div
                                                                                                style="display: table; max-width:187px;">
                                                                                                <table align="left"
                                                                                                    border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="0"
                                                                                                    width="32"
                                                                                                    height="32"
                                                                                                    style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;">
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-row-container"
                                                    style="padding: 0px;background-color: transparent">
                                                    <div class="u-row"
                                                        style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                        <div
                                                            style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                            <div class="u-col u-col-100"
                                                                style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                <div style="width: 100% !important;">
                                                                    <div
                                                                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                        <table style="font-family:'Roboto',sans-serif;"
                                                                            role="presentation" cellpadding="0"
                                                                            cellspacing="0" width="100%"
                                                                            border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                        align="left">
                                                                                        <div
                                                                                            style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
    @endforeach
@endsection
