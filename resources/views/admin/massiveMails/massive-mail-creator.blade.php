@extends('layouts.admin')
@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css"
        integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"
        integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <style>
        @media only screen and (min-width: 1490px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-50 {
                width: 300px !important;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }
        }

        @media (max-width: 1490px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .texta {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col>div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table,
        td {
            color: #000000;
        }

        a {
            color: #161a39;
            text-decoration: underline;
        }
    </style>
    <link preload href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
@endpush
@section('content')
    <div class="page-content">
        <div class="container">
            @include('partials.messages-admin')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        <div class="card-body">
                            <h4 class="card-title" style="letter-spacing: .2rem;">{{ ___('Massive Creator Mail') }}
                                <img loading="lazy" src="{{ asset('assets/images/mail.gif') }}" alt=""
                                    style="height: 50px; margin-top: -16px;">
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                            <div class="content-area card fondo">
                                <div class="card-innr">
                                    <div class="card-head row d-flex align-items-center justify-content-center">

                                        <h6 class="card-title col-6">{{ ___('Mail Creator') }}</h6>
                                        <div class="form-group col-6">
                                            <label for=""></label>
                                            <select class="custom-select" name="" id="">
                                                <option selected value="0">
                                                    --{{ ___('Seleccione un lenguage') }}--
                                                </option>
                                                @foreach ($lenguages as $lang)
                                                    <option value="{{ $lang->code }}">{{ $lang->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: -3%; margin-bottom: 6%;">
                                        <div class="button-items d-flex justify-content-center" style="gap: 14px;">
                                            <span class="badge bg-primary" id="agregar-nombre-empresa"
                                                type="button">{{ ___('Nombre del Sistema') }}</span>
                                            <span class="badge bg-primary" id="agregar-nombre-user"
                                                type="button">{{ ___('Nombre Usuario') }}</span>
                                        </div>
                                    </div>
                                    <small>({{ ___('Fill in the fields of the mail to send') }})</small>
                                    <input type="hidden" name="" id="url" value="{{ asset2() }}">
                                    <form action="{{ route('admin.send.mails.type') }}" class="validate-modern"
                                        method="POST" id="formularioMassiveMails">
                                        <div class="input-item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <input name="personal" type="checkbox"
                                                class="input-switch input-switch-sm checkbox-personal" id="check_personal">
                                            <label for="check_personal">{{ ___('Personal Email') }}</label>
                                        </div>
                                        <div class="col-12 d-none" id="group_users">
                                            <div class="input-item input-with-label">
                                                <label for="user"
                                                    class="input-item-label">{{ ___('Seleccione usuarios para enviar el correo') }}</label>
                                                <div class="input-wrap" id="userc" style="display: grid;">
                                                    <select class="js-example-basic-user col-lg-12" id="user"
                                                        name="user[]" multiple="multiple">
                                                        @foreach ($users as $user)
                                                            <option value="{{ $user->id }}"><strong>ID :
                                                                </strong>{{ $user->id }} =>
                                                                <strong>{{ ___('Name') }}
                                                                    : <strong> {{ $user->name }}
                                                        @endforeach
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>





                                        @csrf
                                        <div class="col-12" id="type_users">
                                            <div class="input-item input-with-label">
                                                <label for="type_user"
                                                    class="input-item-label">{{ ___('Type Users') }}</label> <small
                                                    class="text-danger">({{ ___('Requerido') }})</small>
                                                <div class="input-wrap">
                                                    <select class="js-example-basic-multiple" id="type_user"
                                                        name="type_user[]" multiple="multiple">
                                                        @foreach ($tiposUsuario as $tipos)
                                                            <option value="{{ $tipos->id - 1 }}">{{ ___($tipos->valor) }}
                                                        @endforeach
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>{{-- .input-item --}}
                                        </div>{{-- .col --}}
                                        <div class="col-12" id="group_header">
                                            <label for="">{{ ___('Email Title') }} </label> <small
                                                class="text-danger">({{ ___('Aparecera junto al remitente del correo') }})</small>
                                            <input type="text" name="header" class="form-control" id="header">
                                        </div>
                                        <div class="col-12">
                                            <label for="content" class="mt-3">{{ ___('Contents') }}</label> <small
                                                class="text-danger">({{ ___('Requerido') }})</small>
                                            <textarea id="content" name="content" class="form-control input-textarea content"></textarea>
                                            <button type="button" class="btn btn-success" onclick="viewHtml();"
                                                value="" style="padding: 3px;"><i
                                                    class="fa fa-search mr-2"></i>{{ ___('View changes') }}</button>
                                        </div>
                                        <div class="col-12" id="group_footer">
                                            <label class="mt-3" for="">{{ ___('Footer') }}</label> <small
                                                class="text-danger">({{ ___('Opcional') }})</small>
                                            <input type="text" id="footer" name="footer" class="form-control"
                                                id="">
                                        </div>
                                        <div class="row mt-3 mr-1">
                                            <div class="col">
                                                <button type="button" data-target="#modal-centered" data-toggle="modal"
                                                    class="btn btn-primary float-right"><i
                                                        class="fas fa-paper-plane mr-2"></i>{{ ___('Save and Send') }}</button>
                                            </div>
                                        </div>
                                </div><!-- .card-innr -->
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                            <div class="content-area card">
                                <div class="card-innr">
                                    <div class="input-item col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <input name="type_mail" type="checkbox"
                                            class="input-switch input-switch-sm checkbox" id="CheckBox" checked>
                                        <label for="CheckBox">{{ ___('With Design') }}</label>
                                    </div>
                                    </form>
                                    <div id="mailDesign">
                                        <table
                                            style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f9f9f9;width:100%"
                                            cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr style="vertical-align: top">
                                                    <td
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: #f9f9f9">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table height="0px"
                                                                                                    align="center"
                                                                                                    border="0"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    width="100%"
                                                                                                    style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                    <tbody>
                                                                                                        <tr
                                                                                                            style="vertical-align: top">
                                                                                                            <td
                                                                                                                style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                                <span>&#160;</span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">

                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">

                                                                                                <table width="100%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('images/logo-mail.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 46%;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">

                                                                                                <div
                                                                                                    style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                    <p
                                                                                                        style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                                                        <span
                                                                                                            id="cabecera"
                                                                                                            style="font-size: 28px; line-height: 39.2px; color: #280F53; font-family: Roboto, sans-serif;">
                                                                                                            {{ ___('Header') }}
                                                                                                        </span>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent; margin-top: -4%;">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">

                                                                <div class="texta" id="viewer"
                                                                    style="height: auto; width: 80%; text-align: center; margin: 0 auto; margin-top:20px;">
                                                                </div>
                                                                <table style="font-family:'Roboto',sans-serif;"
                                                                    role="presentation" cellpadding="0" cellspacing="0"
                                                                    width="100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:45px 40px 0px;font-family:'Roboto',sans-serif;"
                                                                                align="center">
                                                                                <div
                                                                                    style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                                    <p
                                                                                        style="font-size: 14px; line-height: 140%;font-style: italic;">
                                                                                        <span id="piedepagina"
                                                                                            style="color: #888888;font-style: italic; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                    class="text-primary"
                                                                                                    style="font-size: 16px; font-style: italic; line-height: 22.4px;">{{ ___('Footer') }}</span></em></span><br /><span
                                                                                            style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                    style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                                    </p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg,#280f534b 100%,#280f534b 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col"
                                                                        style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <div align="center">
                                                                                                    <p
                                                                                                        style="line-height: 140%; font-size: 14px;">
                                                                                                        <span
                                                                                                            style="font-size: 14px; line-height: 19.6px;"><span
                                                                                                                style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                                                    style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                                                    {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table width="70%"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    border="0">
                                                                                                    <tr>
                                                                                                        <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                            align="center">
                                                                                                            <img loading="lazy" align="center"
                                                                                                                border="0"
                                                                                                                src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                                                alt="Image"
                                                                                                                title="Image"
                                                                                                                style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <div align="center">
                                                                                                    <a href="https://app.disruptive.center/"
                                                                                                        target="_blank"
                                                                                                        style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                                                        <span
                                                                                                            style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                                                style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                                            </span></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <div
                                                                                                    style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>



                                    <div id="mailClassic" class="hide">
                                        <table
                                            style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: f9f9f900;width:100%"
                                            cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr style="vertical-align: top">
                                                    <td
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: f9f9f900">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <table height="0px"
                                                                                                    align="center"
                                                                                                    border="0"
                                                                                                    cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    width="100%"
                                                                                                    style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid f9f9f900;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                    <tbody>
                                                                                                        <tr
                                                                                                            style="vertical-align: top">
                                                                                                            <td
                                                                                                                style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                                <span>&#160;</span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="texta" id="viewer2"
                                                            style="    height: auto;
                                                    width: 80%;
                                                    text-align: center;
                                                    margin: 0 auto; margin-top:20px;">
                                                            <h1>
                                                                {{ ___('Your changes will appear here') }}
                                                            </h1>
                                                            <div class="pt-3">
                                                                <i style="font-size: 47px;"
                                                                    class="ti ti-exchange-vertical"></i>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col"
                                                                        style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <div align="center">
                                                                                                    <div
                                                                                                        style="display: table; max-width:187px;">
                                                                                                        <table
                                                                                                            align="left"
                                                                                                            border="0"
                                                                                                            cellspacing="0"
                                                                                                            cellpadding="0"
                                                                                                            width="32"
                                                                                                            height="32"
                                                                                                            style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;">
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-row-container"
                                                            style="padding: 0px;background-color: transparent">
                                                            <div class="u-row"
                                                                style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                                <div
                                                                    style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                    <div class="u-col u-col-100"
                                                                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                        <div style="width: 100% !important;">
                                                                            <div
                                                                                style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                <table
                                                                                    style="font-family:'Roboto',sans-serif;"
                                                                                    role="presentation" cellpadding="0"
                                                                                    cellspacing="0" width="100%"
                                                                                    border="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                                align="left">
                                                                                                <div
                                                                                                    style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-innr -->
                            </div><!-- .card -->
                        </div><!-- .col -->
                    </div><!-- .col -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
    <div class="modal fade mod" id="modal-centered" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content" style="text-align:center;"><a href="#" class="modal-close"
                    data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <img loading="lazy" src="{{ asset('assets/images/info.gif') }}" alt="" style="height: 60px;">
                    <h3 class="popup-title mt-3">
                        {{ ___('Are you sure you want to send the emails in the selected format?') }}</h3>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button class="btn btn-primary mr-3" id="enviar">
                            <i class="fa fa-paper-plane mr-1"></i> {{ ___('Send Mails') }}
                        </button>
                        <a href="#" type="button" class="btn btn-danger" data-dismiss="modal"> <i
                                class="fa fa-ban mr-1"></i>{{ ___('Cancel') }}</a>
                    </div>

                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
@endsection
@push('footer')
    <link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css') }}?ver=1.0">
    <script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>

    <script src="{{ asset('assets/plugins/trumbowyg/plugins/fontsize/trumbowyg.fontsize.js') }}?ver=101"></script>

    <script src="https://rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/resizimg/trumbowyg.resizimg.min.js">
    </script>
    <link preload href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $('.js-example-basic-user').select2({
            theme: "bootstrap4"
        });
        $('.js-example-basic-multiple').select2({
            theme: "bootstrap4"
        });

        (function($) {
            if ($('.content').length > 0) {
                $('.content').trumbowyg({
                    autogrow: true
                });
            }
        })(jQuery);
    </script>
    <script>
        var ClipboardHelper = {
            copyElement: function($element) {
                this.copyText($element.text());
            },
            copyText: function(text) // Linebreaks with \n
            {
                var $tempInput = $("<textarea>");
                $("body").append($tempInput);
                $tempInput.val(text).select();
                document.execCommand("copy");
                $tempInput.remove();
            }
        };
        var editor = $('#content').trumbowyg({
            semantic: false,
            /* (i.e. <em> instead of <i>, <strong> instead of <b>, etc.).  */
            minimalLinks: true,
            btnsDef: {
                spec1: {
                    fn: function() {
                        ClipboardHelper.copyText('-');
                    },
                    text: '-',
                    title: '-',
                    hasIcon: false,
                },
                spec3: {
                    fn: function() {
                        ClipboardHelper.copyText('※');
                    },
                    text: '※',
                    title: '※',
                    hasIcon: false,
                },
                spec2: {
                    fn: function() {
                        ClipboardHelper.copyText('▶');
                    },
                    text: '▶',
                    title: '▶',
                    hasIcon: false,
                },
                specList: {
                    dropdown: ['spec1', 'spec2', 'spec3'],
                    text: '복사하기',
                    title: '특수문자 복사하기',
                    hasIcon: false,
                }
            },
            btns: [
                ['viewHTML'],
                ['strong', 'em'],
                ['link'],
                ['fontsize'],
                ['foreColor'], /*, 'backColor'*/
                ['unorderedList', 'orderedList'],
                ['specList'],
            ],
            plugins: {
                resizimg: {
                    minSize: 64,
                    step: 16,
                },
                colors: {
                    colorList: ['ff0000', '0000ff', '000000',
                        'E72419', '003186', '002369', '4FC7E7', '00CAD4'
                    ],
                },
            },
            tagsToRemove: ['script', 'link']
        });

        function viewHtml() {
            // Get HTML content
            $('#viewer').html(editor.trumbowyg('html'));
            $('#viewer2').html(editor.trumbowyg('html'));

        }

        var variable_user = '{user}';
        var variable_name_app = '{name_app}';
        var focusedElement;
        var currentText;

        // Manejar el cambio de foco en el input con id "header"
        $("#header").focus(function() {
            focusedElement = $(this);
        });

        // Manejar el cambio de foco en el textarea con id "editor"
        $('#content').on('tbwfocus', function() {
            focusedElement = $(this);
        });
        $('#agregar-nombre-user').click(function() {
            // Verificar si el elemento enfocado es el textarea con id "editor"
            if (focusedElement.attr('id') == 'content') {
                editor.trumbowyg('html', editor.trumbowyg('html') + ' ' + variable_user);
            }
            // Verificar si el elemento enfocado es el input con id "header"
            if (focusedElement.attr('id') == 'header') {
                currentText = focusedElement.val();
                focusedElement.val(currentText + ' ' + variable_user);
            }
        });

        $('#agregar-nombre-empresa').click(function() {
            // Verificar si el elemento enfocado es el textarea con id "editor"
            if (focusedElement.attr('id') == 'content') {
                editor.trumbowyg('html', editor.trumbowyg('html') + ' ' + variable_name_app);
            }
            // Verificar si el elemento enfocado es el input con id "header"
            if (focusedElement.attr('id') == 'header') {
                currentText = focusedElement.val();
                focusedElement.val(currentText + ' ' + variable_name_app);
            }
        });
    </script>
    <script>
        document.getElementById("header").addEventListener("keyup", testpa);
        document.getElementById("footer").addEventListener("keyup", testpa);

        function testpa() {
            //es el campo input 
            var txtInput = document.querySelector('#header');
            var txtInput2 = document.querySelector('#footer');
            //es el span
            var divCopia = document.getElementById('cabecera');
            var divCopia2 = document.getElementById('piedepagina');
            //monto total que tiene el usuario
            console.log(txtInput.val);
            txtInput.addEventListener('keyup', () => {
                divCopia.innerHTML = txtInput.value;
            });
            txtInput2.addEventListener('keyup', () => {
                divCopia2.innerHTML = txtInput2.value;
            });
        }
    </script>
    <script>
        $(function() {
            $("#CheckBox").change(function() {
                if ($(this).is(":checked")) {
                    $('.checkbox').attr('checked', true);
                    $('#mailDesign').removeClass('hide');
                    $('#mailClassic').addClass('hide');
                    $('#group_footer').removeClass('hide');
                } else {
                    $('.checkbox').removeAttr('checked');
                    $('#mailClassic').removeClass('hide');
                    $('#mailDesign').addClass('hide');
                    $('#group_footer').addClass('hide');
                }
            });
        });
        $(function() {
            $("#check_personal").change(function() {
                if ($(this).is(":checked")) {
                    $('.checkbox-personal').attr('checked', true);
                    $('#group_users').removeClass('d-none');
                    $('#type_users').addClass('hide');
                } else {
                    $('.checkbox-personal').removeAttr('checked');
                    $('#group_users').addClass('d-none');
                    $('#type_users').removeClass('hide');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#enviar").click(function(e) {
                console.log('enviar')
                e.preventDefault();
                var type_user = $('#type_user').val();
                var users = $('#user').val();
                var type_mail = ($("#CheckBox").is(':checked')) ? '1' : '0';
                var check_personal = ($("#check_personal").is(':checked')) ? '1' : '0';
                var header = $('#header').val();
                var content = $('#content').val();
                var footer = $('#footer').val();
                var ruta = $('#url').val();
                var lenguage = $('.custom-select').val();
                $.ajax({
                    type: "post",
                    url: ruta + 'admin/send/mails',
                    data: {
                        users: users,
                        type_user: type_user,
                        type_mail: type_mail,
                        check_personal: check_personal,
                        header: header,
                        content: content,
                        footer: footer,
                        lenguage: lenguage,
                        '_token': '{{ csrf_token() }}',
                    },
                    success: function(json) {
                        displaySuccessToaster(json.msg, json.message);
                    }
                });

                function displaySuccessToaster(tipo, mensaje) {
                    console.log(tipo + ' ' + mensaje);
                    if (tipo == 'success') {
                        toastr.success(mensaje);
                        setTimeout(function() {
                            window.location.reload(1);
                        }, 1000);
                    } else if (tipo == 'error') {
                        toastr.error(mensaje);
                    }
                }
            });
        });
    </script>
@endpush
