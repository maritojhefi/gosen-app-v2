@extends('layouts.admin')
@section('title', __(ucfirst($is_page)).___('Transactions'))

@section('contenttrans')
<div class="page-content">
    <div class="container">
        @include('layouts.messages')
        @include('vendor.notice')
        <div class="card content-area content-area-mh">
            <div class="card-innr">
                <div class="card-head has-aside">
                    <h4 class="card-title">{{ __(ucfirst($is_page)) }} {{___('Transactions')}}</h4>
                    <div class="card-opt">
                        <ul class="btn-grp btn-grp-block guttar-20px">
                            <li>
                                <a href="{{ route('pdf') }}" class="btn btn-sm btn-auto btn-danger" target="_blank">
                                    <em class="fa fa-file-pdf"></em><span class="d-none d-sm-inline-block">PDF</span>
                                </a>
                                <a href="{{ route('excel') }}" class="btn btn-sm btn-auto btn-success">
                                    <em class="fa fa-file-excel"></em><span class="d-none d-sm-inline-block">Excel</span>
                                </a>
                                <a href="#" class="btn btn-sm btn-auto btn-primary" data-toggle="modal" data-target="#addTnx">
                                    <em class="fas fa-plus-circle"></em><span>{{___('Add')}} <span class="d-none d-sm-inline-block">Tokens</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="page-nav-wrap">
                    <div class="page-nav-bar justify-content-between bg-lighter">
                        <div class="page-nav w-100 w-lg-auto">
                            <ul class="nav">
                                <li class="nav-item{{ (is_page('transactions.pending') ? ' active' : '') }}"><a class="nav-link" href="{{ route('admin.transactions', 'pending') }}">{{___('Pending')}}</a></li>
                                <li class="nav-item {{ (is_page('transactions.approved') ? ' active' : '') }}"><a class="nav-link" href="{{ route('admin.transactions', 'approved') }}">{{___('Approved')}}</a></li>
                                <li class="nav-item {{ (is_page('transactions.withdraw') ? ' active' : '') }}"><a class="nav-link" href="{{ route('admin.transactions', 'withdraw') }}">{{___('Withdraw')}}</a></li>
                                <li class="nav-item {{ (is_page('transactions.bonuses') ? ' active' : '') }}"><a class="nav-link" href="{{ route('admin.transactions', 'bonuses') }}">{{___('Bonuses')}}</a></li>
                                <li class="nav-item {{ (is_page('transactions') ? ' active' : '') }}"><a class="nav-link" href="{{ route('admin.transactions') }}">{{___('All')}}</a></li>
                            </ul>
                        </div>
                        <div class="search flex-grow-1 pl-lg-4 w-100 w-sm-auto">
                            <form action="{{ route('admin.transactions') }}" method="GET" autocomplete="off">
                                <div class="input-wrap">
                                    <span class="input-icon input-icon-left"><em class="ti ti-search"></em></span>
                                    <input type="search" class="input-solid input-transparent" placeholder="{{___('Tranx ID to quick search')}}" value="{{ request()->get('s', '') }}" name="s">
                                </div>
                            </form>
                        </div>
                        @if(!empty(env_file()) && nio_status() && !empty(app_key()))
                        <div class="tools w-100 w-sm-auto" align="right">
                            <ul class="btn-grp guttar-8px">
                                <li><a href="#" class="btn btn-light btn-sm btn-icon  bg-white advsearch-opt" data-toggle="tooltip" data-placement="bottom" data-original-title="Filter"> <em class="ti ti-panel"></em> </a></li>
                                {{-- @if(is_super_admin())
                                <li>
                                    <div class="relative">
                                        <a href="#" class="btn btn-light bg-white btn-sm btn-icon toggle-tigger "><em class="ti ti-server"></em> </a>
                                        <div class="toggle-class dropdown-content dropdown-content-sm dropdown-content-center shadow-soft">
                                            <ul class="dropdown-list">
                                                <li><h6 class="dropdown-title">{{___('Export')}}</h6></li>
                                                <li><a href="{{ route('admin.export', array_merge([ 'table' => 'transactions', 'format' => 'entire'], request()->all())) }}">{{___('Entire')}}</a></li>
                                                <li><a href="{{ route('admin.export', array_merge([ 'table' => 'transactions', 'format' => 'minimal'], request()->all())) }}">{{___('Minimal')}}</a></li>
                                                <li><a href="{{ route('admin.export', array_merge([ 'table' => 'transactions', 'format' => 'compact'], request()->all())) }}">{{___('Compact')}}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endif --}}
                                <li>
                                    <div class="relative">
                                        <a href="#" class="btn btn-light bg-white btn-sm btn-icon toggle-tigger " data-toggle="tooltip" data-placement="bottom" data-original-title="Settings"><em class="ti ti-settings"></em> </a>
                                        <div class="toggle-class dropdown-content dropdown-content-sm dropdown-content-center shadow-soft">
                                            <form class="update-meta" action="#" data-type="tnx_page_meta">
                                                <ul class="dropdown-list">
                                                    <li><h6 class="dropdown-title">{{___('Show')}}</h6></li>
                                                    <li{!! (gmvl('tnx_per_page', 10)==10) ? ' class="active"' : '' !!}>
                                                        <a href="#" data-meta="perpage=10">10</a></li>
                                                    <li{!! (gmvl('tnx_per_page', 10)==20) ? ' class="active"' : '' !!}>
                                                        <a href="#" data-meta="perpage=20">20</a></li>
                                                    <li{!! (gmvl('tnx_per_page', 10)==50) ? ' class="active"' : '' !!}>
                                                        <a href="#" data-meta="perpage=50">50</a></li>
                                                </ul>
                                                <ul class="dropdown-list">
                                                    <li><h6 class="dropdown-title">{{___('Order')}}</h6></li>
                                                    <li{!! (gmvl('tnx_ordered', 'DESC')=='DESC') ? ' class="active"' : '' !!}>
                                                        <a href="#" data-meta="ordered=DESC">DESC</a></li>
                                                    <li{!! (gmvl('tnx_ordered', 'DESC')=='ASC') ? ' class="active"' : '' !!}>
                                                        <a href="#" data-meta="ordered=ASC">ASC</a></li>
                                                </ul>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @endif
                    </div>
                    @if( !empty(env_file()) && nio_status() && !empty(app_key()) )
                    <div class="search-adv-wrap hide">
                        <form class="adv-search" id="adv-search" action="{{ route('admin.transactions') }}" method="GET" autocomplete="off">
                            <div class="row align-items-end guttar-20px guttar-vr-15px">
                                <div class="col-lg-6">
                                    <div class="input-grp-wrap">
                                        <span class="input-item-label input-item-label-s2 text-exlight">{{___('Advanced Search')}}</span>
                                        <div class="input-grp align-items-center bg-white">
                                            <div class="input-wrap flex-grow-1">
                                                <input value="{{ request()->get('search') }}" class="input-solid input-solid-sm input-transparent" type="text" placeholder="{{___('Search by ID')}}" name="search">
                                            </div>
                                            <ul class="search-type">
                                                <li class="input-wrap input-radio-wrap">
                                                    <input name="by" class="input-radio-select" id="advs-by-tnx" value="" type="radio"{{ (empty(request()->by) || request()->by!='usr') ? ' checked' : '' }}>
                                                    <label for="advs-by-tnx">TRANX</label>
                                                </li>
                                                <li class="input-wrap input-radio-wrap">
                                                    <input name="by" class="input-radio-select" id="advs-by-user" value="usr" type="radio"{{ (isset(request()->by) && request()->by=='usr') ? ' checked' : '' }}>
                                                    <label for="advs-by-user">{{___('User')}}</label>
                                                </li>
                                                <li class="input-wrap input-radio-wrap">
                                                    <input name="by" class="input-radio-select" id="advs-by-wallet_address" value="wallet_address" type="radio"{{ (isset(request()->by) && request()->by=='wallet_address') ? ' checked' : '' }}>
                                                    <label for="advs-by-wallet_address">{{___('Reference')}}</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Tranx Type')}}</label>
                                        <select  name="type" class="select select-sm select-block select-bordered" data-dd-class="search-off">
                                            <option value="">{{___('Any Type')}}</option>
                                            <option {{ request()->get('type') == 'purchase' ? 'selected' : '' }} value="purchase">{{___('Purchase')}}</option>
                                            <option {{ request()->get('type') == 'launch' ? 'selected' : '' }} value="launch">{{___('Launch')}}</option>
                                            <option {{ request()->get('type') == 'joined' ? 'selected' : '' }} value="joined">{{___('Joined')}}</option>
                                            <option {{ request()->get('type') == 'withdraw' ? 'selected' : '' }} value="withdraw">{{___('Withdraw')}}</option>
                                            <option {{ request()->get('type') == 'bonus' ? 'selected' : '' }} value="bonus">Bonus</option>
                                            <option {{ request()->get('type') == 'referral' ? 'selected' : '' }} value="referral">{{___('Referral')}}</option>
                                            <option {{ request()->get('type') == 'transfer' ? 'selected' : '' }} value="transfer">{{___('Transfer')}}</option>
                                            <option {{ request()->get('type') == 'refund' ? 'selected' : '' }} value="refund">{{___('Refund')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Status')}}</label>
                                        <select name="state" class="select select-sm select-block select-bordered" data-dd-class="search-off">
                                            <option value="">{{___('Show All')}}</option>
                                            <option {{ request()->get('state') == 'pending' ? 'selected' : '' }} value="pending">{{___('Pending')}}</option>
                                            <option {{ request()->get('state') == 'onhold' ? 'selected' : '' }} value="onhold">{{___('Onhold')}}</option>
                                            <option {{ request()->get('state') == 'approved' ? 'selected' : '' }} value="approved">{{___('Approved')}}</option>
                                            <option {{ request()->get('state') == 'canceled' ? 'selected' : '' }} value="canceled">{{___('Canceled')}}</option>
                                            <option {{ request()->get('state') == 'deleted' ? 'selected' : '' }} value="deleted">{{___('Deleted')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Stage')}}</label>
                                        <select name="stg" class="select select-sm select-block select-bordered" data-dd-class="search-off">
                                            <option value="">{{___('All Stage')}}</option>
                                            @forelse($stages as $stage)
                                            <option {{ request()->get('stg') == $stage->id ? 'selected' : '' }} value="{{ $stage->id }}">{{ __($stage->name) }}</option>
                                            @empty
                                            <option value="">{{___('No active stage')}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Pay Method')}}</label>
                                        <select name="pmg" class="select select-sm select-block select-bordered" data-dd-class="search-off">
                                            <option value="">{{___('All')}}</option>
                                            @foreach($gateway as $pmg)
                                            <option {{ request()->get('pmg') == $pmg ? 'selected' : '' }} value="{{ $pmg }}">{{ ucfirst($pmg) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Pay Currency')}}</label>
                                        <select name="pmc" class="select select-sm select-block select-bordered" data-dd-class="search-off">
                                            <option value="">{{___('All')}}</option>
                                            @foreach($pm_currency as $gt => $full)
                                            @if(token('purchase_'.$gt) == 1)
                                            <option {{ request()->get('pmc') == $gt ? 'selected' : '' }} value="{{ strtolower($gt) }}">{{ strtoupper($gt) }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('Date Within')}}</label>
                                        <select name="date" class="select select-sm select-block select-bordered date-opt" data-dd-class="search-off">
                                            <option value="">{{___('All Time')}}</option>
                                            <option {{ request()->get('date') == 'today' ? 'selected' : '' }} value="today">{{___('Today')}}</option>
                                            <option {{ request()->get('date') == 'this-month' ? 'selected' : '' }} value="this-month">{{___('This Month')}}</option>
                                            <option {{ request()->get('date') == 'last-month' ? 'selected' : '' }} value="last-month">{{___('Last Month')}}</option>
                                            <option {{ request()->get('date') == '90day' ? 'selected' : '' }} value="90day">{{___('Last 90 Days')}}</option>
                                            <option {{ request()->get('date') == 'custom' ? 'selected' : '' }} value="custom">{{___('Custom Range')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6 date-hide-show">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('From')}}</label>
                                        <div class="relative">
                                            <input class="input-bordered input-solid-sm date-picker bg-white" value="{{ (request()->get('date') == 'custom') ? request()->get('from') : '' }}" type="text" id="date-from" name="from" data-format="alt">
                                            <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6 date-hide-show">
                                    <div class="input-wrap input-with-label">
                                        <label class="input-item-label input-item-label-s2 text-exlight">{{___('To')}}</label>
                                        <div class="relative">
                                            <input class="input-bordered input-solid-sm date-picker bg-white" value="{{ (request()->get('date') == 'custom') ? request()->get('to') : '' }}" type="text" id="date-to" name="to" data-format="alt">
                                            <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-2 col-mb-6">
                                    <div class="input-wrap">
                                        <input type="hidden" name="filter" value="1">
                                        <button class="btn btn-sm btn-sm-s2 btn-auto btn-primary">
                                            <em class="ti ti-search width-auto"></em><span>{{___('Search')}}</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif

                    @if (request()->get('filter') || request()->s)
                    <div class="search-adv-result">
                        <div class="search-info">
                            {{___('Found')}} <span class="search-count">{{ $trnxs->total() }}</span> {{___('Transactions')}}{{ (request()->get('date') != 'custom') ? '.' : '' }}
                            @if (request()->get('date') == 'custom')
                            {{___('between')}} <span>{{ _date2sz(request()->get('from'), '', true) }}</span> {{___('to')}} <span>{{ _date2sz(request()->get('to'), '', true) }}</span>.
                            @endif
                        </div>
                        <ul class="search-opt">
                            @if(request()->get('search'))
                            <li><a href="{{ qs_url(qs_filter('search')) }}">{{___('Search')}} <span>'{{ request()->get('search') }}'</span>{{ (request()->by=='usr') ? ' (User)' : '' }}</a></li>
                            @endif

                            @if(request()->get('type'))
                                <li><a href="{{ qs_url( qs_filter('type')) }}">{{___('Type')}}: <span>{{ ucfirst(request()->get('type')) }}</span></a></li>
                            @endif

                            @if(request()->get('state'))
                                <li><a href="{{ qs_url( qs_filter('state')) }}">{{___('Status')}}: <span>{{ ucfirst(request()->get('state')) }}</span></a></li>
                            @endif

                            @if(request()->get('stg'))
                                <li><a href="{{ qs_url( qs_filter('stg')) }}">{{___('Stage')}}: <span>{{ ucfirst(request()->get('stg')) }}</span></a></li>
                            @endif

                            @if(request()->get('pmg'))
                                <li><a href="{{ qs_url( qs_filter('pmg')) }}">{{___('Pay Method')}}: <span>{{ ucfirst(request()->get('pmg')) }}</span></a></li>
                            @endif

                            @if(request()->get('pmc'))
                                <li><a href="{{ qs_url( qs_filter('pmc')) }}">{{___('Currency')}}: <span>{{ strtoupper(request()->get('pmc')) }}</span></a></li>
                            @endif

                            @if (request()->get('date') == 'today')
                                <li><a href="{{ qs_url( qs_filter('date')) }}">{{___('In today')}}</span></a></li>
                            @endif

                            @if (request()->get('date') == 'this-month')
                                <li><a href="{{ qs_url( qs_filter('date')) }}"><span>{{___('In this month')}}</span></a></li>
                            @endif

                            @if (request()->get('date') == 'last-month')
                                <li><a href="{{ qs_url( qs_filter('date')) }}"><span>{{___('In last month')}}</span></a></li>
                            @endif

                            @if (request()->get('date') == '90day')
                                <li><a href="{{ qs_url( qs_filter('date')) }}"><span>{{___('In last 90 days')}}</span></a></li>
                            @endif
                            <li><a href="{{ route('admin.transactions') }}" class="link link-underline">{{___('Clear All')}}</a></li>
                        </ul>
                    </div>
                    @endif
                </div>
                
                @if($trnxs->total() > 0) 
                <table class="data-table admin-tnx">
                    <thead>
                        <tr class="data-item data-head">
                            <th class="data-col tnx-status dt-tnxno">Tranx ID</th>
                            <th class="data-col dt-token">Tokens</th>
                            <th class="data-col dt-amount">{{___('Amount')}}</th>
                            <th class="data-col dt-usd-amount">{{___('Base Amount')}}</th>
                            <th class="data-col pm-gateway dt-account">{{___('Pay From')}}</th>
                            <th class="data-col pm-gateway dt-account">{{___('Approved By')}}</th>
                            <th class="data-col dt-type tnx-type">{{___('Type')}}</th>
                            <th class="data-col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trnxs as $trnx)
                        @php 
                            $text_danger = ( $trnx->tnx_type=='refund' || ($trnx->tnx_type=='transfer' && $trnx->extra=='sent') ) ? ' text-danger' : '';
                        @endphp

                       
                        <tr class="data-item" id="tnx-item-{{ $trnx->id }}">
                            <td class="data-col dt-tnxno">
                                <div class="d-flex align-items-center">
                                    <div id="ds-{{ $trnx->id }}" data-toggle="tooltip" data-placement="top" title="{{ __status($trnx->status, 'text') }}" class="data-state data-state-{{ __status($trnx->status, 'icon') }}">
                                        <span class="d-none">{{ ucfirst($trnx->status) }}</span>
                                    </div>
                                    <div class="fake-class">
                                        <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                        <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                    </div>
                                </div>
                            </td>
                            <td class="data-col dt-token">
                                <span class="lead token-amount{{ $text_danger }}">{{ (starts_with($trnx->total_tokens, '-') ? '' : '+').$trnx->total_tokens }}</span>
                                <span class="sub sub-symbol">{{ token('symbol') }}</span>
                            </td>
                            <td class="data-col dt-amount">
                                @if ($trnx->tnx_type=='referral'||$trnx->tnx_type=='bonus') 
                                    <span class="lead amount-pay">{{ '~' }}</span>
                                @else 
                                <span class="lead amount-pay{{ $text_danger }}">{{ $trnx->amount }}</span>
                                <span class="sub sub-symbol">{{ strtoupper($trnx->currency) }} <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="1 {{ token('symbol') }} = {{ to_num($trnx->currency_rate, 'max').' '.strtoupper($trnx->currency) }}"></em></span>
                                @endif
                            </td>
                            <td class="data-col dt-usd-amount{{ $text_danger }}">
                                @if ($trnx->tnx_type=='referral'||$trnx->tnx_type=='bonus') 
                                    <span class="lead amount-receive">{{ '~' }}</span>
                                @else 
                                <span class="lead amount-receive{{ $text_danger }}">{{ $trnx->base_amount }}</span>
                                <span class="sub sub-symbol">{{ strtoupper($trnx->base_currency) }} <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="1 {{ token('symbol') }} = {{ to_num($trnx->base_currency_rate, 'max').' '.strtoupper($trnx->base_currency) }}"></em></span>
                                @endif
                            </td>
                            <td class="data-col dt-account">
                                <span class="sub sub-s2 pay-with">
                                    @if ($trnx->tnx_type=='bonus' && $trnx->added_by!=set_added_by('0')) 
                                        {{ 'Added by '.transaction_by($trnx->added_by) }}
                                    @elseif($trnx->tnx_type == 'refund')
                                        {{ $trnx->details }}
                                    @elseif($trnx->tnx_type == 'transfer')
                                        {{ $trnx->details }}
                                    @else
                                        {{ (is_gateway($trnx->payment_method, 'internal') ? gateway_type($trnx->payment_method, 'name') : ( (is_gateway($trnx->payment_method, 'online') || $trnx->payment_method=='bank') ? 'Pay via '.ucfirst($trnx->payment_method) : 'Pay with '.strtoupper($trnx->currency) ) ) }}
                                        @if($trnx->wallet_address && $trnx->tnx_type!='bonus')
                                        <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="{{ $trnx->wallet_address }}"></em>
                                        @endif
                                    @endif
                                </span>
                                @if($trnx->tnx_type == 'refund')
                                    @php 
                                    $extra = (is_json($trnx->extra, true) ?? $trnx->extra);
                                    @endphp
                                    <span class="sub sub-email"><a href="{{ route('admin.transactions.view', ($extra->trnx ?? $trnx->id)) }}">{{ ___("View Transaction") }}</a></span>
                                @else
                                    <span class="sub sub-email">{{ set_id($trnx->user) }} <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="{{ isset($trnx->tnxUser) ? explode_user_for_demo($trnx->tnxUser->email, auth()->user()->type) : '' }}"></em></span> 
                                @endif
                            </td>
                            <td class="data-col data-approved">
                                @if($trnx->tnx_type=='refund')
                                    @php 
                                    $trnx_extra = (is_json($trnx->extra, true) ?? $trnx->extra);
                                    @endphp
                                    <span class="data-details-title">{{___('Refund Note')}}</span>
                                    <span class="data-details-info">{{ $trnx_extra->message }}</span>
                                @else
                                    @if($trnx->checked_by != NULL)
                                    <span class="data-details-info"> <strong>{{ ucfirst(approved_by($trnx->checked_by)) }}</strong> <br> at {{ _date($trnx->checked_time) }}</span>
                                    @elseif($trnx->status == 'canceled')
                                    <span class="data-details-info">{{___('Canceled by User')}}</span>
                                    @else
                                    <span class="data-details-info">{{___('Not Reviewed yet')}}.</span>
                                    @endif
                                @endif
                            </td>
                            <td class="data-col data-type">
                                <span class="dt-type-md badge badge-outline badge-md badge-{{$trnx->id}} badge-{{__status($trnx->tnx_type,'status')}}">{{ ucfirst($trnx->tnx_type) }}</span>
                                <span class="dt-type-sm badge badge-sq badge-outline badge-md badge-{{$trnx->id}} badge-{{__status($trnx->tnx_type,'status')}}">{{ ucfirst(substr($trnx->tnx_type, 0, 1)) }}</span>
                            </td>
                            <td class="data-col text-right">
                                @if($trnx->status == 'deleted')
                                <a href="{{ route('admin.transactions.view', $trnx->id) }}" target="_blank" class="btn btn-light-alt btn-xs btn-icon"><em class="ti ti-eye"></em></a>
                                @else 
                                <div class="relative d-inline-block">
                                    <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em class="ti ti-more-alt"></em></a>
                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                        <ul id="more-menu-{{ $trnx->id }}" class="dropdown-list">
                                            <li><a href="{{ route('admin.transactions.view', $trnx->id) }}">
                                                <em class="ti ti-eye"></em> {{___('View Details')}}</a></li>
                                            @if( $trnx->tnx_type == 'transfer' && $trnx->status == 'pending')
                                            <li><a href="javascript:void(0)" class="tnx-transfer-action" data-status="approved" data-tnx_id="{{ $trnx->id }}">
                                                <em class="far fa-check-square"></em> {{___('Approve')}}</a></li>
                                            <li><a href="javascript:void(0)" class="tnx-transfer-action" data-status="rejected" data-tnx_id="{{ $trnx->id }}">
                                                <em class="fas fa-ban"></em> {{___('Reject')}}</a></li>
                                            @endif
                                            @if($trnx->status == 'approved' && $trnx->tnx_type == 'purchase' && $trnx->refund == null)
                                            <li><a href="javascript:void(0)" class="tnx-action" data-type="refund" data-id="{{ $trnx->id }}">
                                                <em class="fas fa-reply"></em> {{___('Refund')}}</a></li>
                                            @endif
                                            @if($trnx->status == 'pending' || $trnx->status == 'onhold')
                                                @if($trnx->payment_method == 'bank' || $trnx->payment_method == 'manual')
                                                <li><a href="javascript:void(0)" id="token_adjust"  class="token_adjust" data-id="{{ $trnx->id }}">
                                                    <em class="far fa-check-square"></em>{{___('Approve')}}</a></li>
                                                @endif
                                                @if ($trnx->payment_method == 'coinbase' && $trnx->status != 'canceled')
                                                <li><a href="{{ route('admin.transactions.check', ['tid' => $trnx->id]) }}">
                                                    <em class="fas fa-reply"></em>{{___('Check Status')}}</a></li> 
                                                @endif
                                                @if($trnx->tnx_type != 'transfer')
                                                <li id=""><a href="#" data-target="#modal-centered{{ $trnx->id }}" data-toggle="modal" >
                                                    <em class="fas fa-ban"></em>{{___('Cancel')}}</a></li>
                                                @endif
                                            @endif
                                            @if($trnx->status == 'canceled')
                                                @if( !empty($trnx->checked_by) && ($trnx->payment_method == 'bank' || $trnx->payment_method == 'manual'))
                                                <li><a href="javascript:void(0)" id="token_adjust" class="token_adjust" data-id="{{ $trnx->id }}">
                                                    <em class="far fa-check-square"></em>{{___('Approve')}}</a></li>
                                                @endif
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>{{-- .data-item --}}
                        @endforeach
                    </tbody>
                </table>
                @else 
                    <div class="bg-light text-center rounded pdt-5x pdb-5x">
                        <p><em class="ti ti-server fs-24"></em><br>{{ ($is_page=='all') ? ___('No transaction found!') : ___('No ').$is_page.___('transaction here!') }}</p>
                        <p><a class="btn btn-primary btn-auto" href="{{ route('admin.transactions') }}">{{___('View All Transactions')}}</a></p>
                    </div>
                @endif

                @if ($pagi->hasPages())
                <div class="pagination-bar">
                    <div class="d-flex flex-wrap justify-content-between guttar-vr-20px guttar-20px">
                        <div class="fake-class">
                            <ul class="btn-grp guttar-10px pagination-btn">
                                @if($pagi->previousPageUrl())
                                <li><a href="{{ $pagi->previousPageUrl() }}" class="btn ucap btn-auto btn-sm btn-light-alt">Prev</a></li>
                                @endif 
                                @if($pagi->nextPageUrl())
                                <li><a href="{{ $pagi->nextPageUrl() }}" class="btn ucap btn-auto btn-sm btn-light-alt">Next</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="fake-class">
                            <div class="pagination-info guttar-10px justify-content-sm-end justify-content-mb-end">
                                <div class="pagination-info-text ucap">{{___('Page')}} </div>
                                <div class="input-wrap w-80px">
                                    <select class="select select-xs select-bordered goto-page" data-dd-class="search-{{ ($pagi->lastPage() > 7) ? 'on' : 'off' }}">
                                        @for ($i = 1; $i <= $pagi->lastPage(); $i++)
                                        <option value="{{ $pagi->url($i) }}"{{ ($pagi->currentPage() ==$i) ? ' selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            <div class="pagination-info-text ucap">{{___('of')}} {{ $pagi->lastPage() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>{{-- .card-innr --}}
        </div>{{-- .card --}}
    </div>{{-- .container --}}
</div>{{-- .page-content --}}
@endsection

@section('modals')

@foreach ($trnxs as $trnx)
        <div class="modal fade" id="modal-centered{{ $trnx->id }}" tabindex="-1" style="display: none;"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                        aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <strong>
                            <h3 class="popup-title">{{ ___('Are you sure you want to cancel this transaction?') }}</h3>
                        </strong>
                        <p>
                            <span class="pt-2"
                                style="font-size:12px;">{{ ___('Write the reason why it will be canceled (optional)') }}</span>
                        </p>
                        <form action="{{ route('admin.ajax.transactions.update') }}" method="POST" class="validate-modern">
                            @csrf
                            <input type="hidden" name="req_type" value="canceled">
                            <input type="hidden" name="tnx_id" value="{{$trnx->id}}">
                            <input type="hidden" name="token" value="0">
                            <input type="hidden" name="chk_adjust" value="0">
                            <input type="hidden" name="adjusted_token" value="0">
                            <input type="hidden" name="base_bonus" value="0">
                            <input type="hidden" name="token_bonus" value="0">
                            <input type="hidden" name="amount" value="0">
                            <input type="hidden" name="message" value="true">
                            <textarea name="abstract" id="abstract" cols="30" rows="10" style="width: 100%; height:auto;"></textarea>
                            <div style="display: flex; justify-content: center;">
                                <button type="submit" class="btn btn-primary mr-5">
                                    {{ ___('Yes') }}
                                </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">
                                    {{ ___('No') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
    @endforeach







<div class="modal fade" id="addTnx">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="popup-body popup-body-md">
                <h3 class="popup-title">{{___('Manually Add Tokens')}}</h3>
                <form action="{{ route('admin.ajax.transactions.add') }}" method="POST" class="validate-modern" id="add_token" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Tranx Type')}}</label>
                                <div class="input-wrap">
                                    <select name="type" class="select select-block select-bordered" required>
                                        <option value="purchase">{{___('Purchase')}}</option>
                                        <option value="bonus">{{___('Bonus')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label w-sm-60">
                                <label class="input-item-label">{{___('Tranx Date')}}</label>
                                <div class="input-wrap">
                                    <input class="input-bordered date-picker" required="" type="text" name="tnx_date" value="{{ date('m/d/Y') }}">
                                    <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">Token {{___('Added To')}}</label>
                                <div class="input-wrap">
                                    <select name="user" required="" class="select-block select-bordered" data-dd-class="search-on">
                                        @forelse($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @empty
                                        <option value="">{{___('No user found')}}</option>
                                        @endif
                                    </select>
                                    <span class="input-note">{{___('Select account to add token')}}.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">Token {{___('for Stage')}}</label>
                                <div class="input-wrap">
                                    <select name="stage" class="select select-block select-bordered" required>
                                        @forelse($stages as $stage)
                                        <option value="{{ $stage->id }}">{{ $stage->name }}</option>
                                        @empty
                                        <option value="">{{___('No active stage')}}</option>
                                        @endif
                                    </select>
                                    <span class="input-note">{{___('Select Stage where from adjust tokens')}}.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Payment Gateway')}}</label>
                                <div class="input-wrap">
                                    <select name="payment_method" class="select select-block select-bordered">
                                        @foreach($pmethods as $pmn)
                                        <option value="{{ $pmn->payment_method }}">{{ ucfirst($pmn->payment_method) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <span class="input-note">{{___('Select method for this transaction')}}.</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Payment Amount')}}</label>
                                <div class="row flex-n guttar-10px">
                                    <div class="col-7">
                                        <div class="input-wrap">
                                            <input class="input-bordered" type="number" name="amount" placeholder="{{___('Optional')}}">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="input-wrap">
                                            <select name="currency" class="select select-block select-bordered">
                                                @foreach($pm_currency as $gt => $full)
                                                @if(token('purchase_'.$gt) == 1)
                                                <option value="{{ strtoupper($gt) }}"{{ base_currency()==$gt ? ' selected=""' : '' }}>{{ strtoupper($gt) }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <span class="input-note">{{___('Amount calculate based on stage if leave blank')}}.</span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Payment Address')}}</label>
                                <div class="input-wrap">
                                    <input class="input-bordered" type="text" name="wallet_address" placeholder="{{___('Optional')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Number of')}} Token</label>
                                <div class="input-wrap">
                                    <input class="input-bordered" type="number" name="total_tokens" max="{{ active_stage()->max_purchase }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label d-none d-sm-inline-block">&nbsp;</label>
                                <div class="input-wrap input-wrap-checkbox mt-sm-2">
                                    <input id="auto-bonus" class="input-checkbox input-checkbox-md" type="checkbox" name="bonus_calc">
                                    <label for="auto-bonus"><span>{{___('Bonus Adjusted from Stage')}}</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">{{___('Add')}} Token</button>
                    <div class="gaps-3x"></div>
                    <div class="note note-plane note-light">
                        <em class="fas fa-info-circle"></em>
                        <p>{{___('If checked')}} <strong>'{{___('Bonus Adjusted')}}'</strong>, {{___('it will applied bonus based on selected stage (only for Purchase type)')}}.</p>
                    </div>
                </form>
            </div>
        </div>{{-- .modal-content --}}
    </div>{{-- .modal-dialog --}}
</div>
{{-- Modal End --}}

@endsection
@push('footer')
<script>
    $(document).ready(function() {
        var b = $("#ajax-modal")
        var access_url = "{{ route('admin.ajax.transactions.adjustement') }}";
        $('.token_adjust').click(function(c) {
            c.preventDefault();
            var d = $(this),
                e = d.data("id");
            b.html(""),
                $.ajax({
                    method: "post",
                    data: {
                        _token: csrf_token,
                        tnx_id: e
                    },
                    url: trnx_adjust_url,
                    success: function(a) {
                        a.status && "die" == a.status ?
                        show_toast(a.msg, a.message, "ti ti-lock") :
                        (b.html(a.modal),
                        $('#adjustment').modal('show'));
                    },
                    error: function(a, b, c) {
                        _log(a, b, c),
                            show_toast("error", "Sorry, something went wrong!\n" + c);
                    }
                })
        });
    });
</script>
@endpush