@extends('layouts.admin')
@section('title', ___('Gift Setting'))

@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">{{___('Revutokens Update')}}</h4>
                    <div class="d-flex guttar-15px">                      
                        <div class="fake-class">
                            <a href="{{route('admin.regalos.settings')}}" class="btn btn-sm btn-auto btn-primary d-sm-inline-block d-none"><em class="fas fa-arrow-left"></em><span>{{___('Back')}}</span></a>
                            <a href="{{route('admin.regalos.settings')}}" class="btn btn-icon btn-sm btn-primary d-sm-none"><em class="fas fa-arrow-left"></em></a>
                        </div>
                    </div>
                </div>
                <div class="gaps-1x"></div>
                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#regalo_config">{{___('Details')}}</a>
                    </li>                  
                </ul>{{-- .nav-tabs-line --}}
                <div class="tab-content" id="gift-details">
                    <div class="tab-pane fade show active" id="regalo_config">
                        <form action="{{ route('admin.ajax.regalo.update') }}" class="_reload validate-modern" method="POST" id="regalo_config" autocomplete="off">
                            @csrf
                            <input type="hidden" name="gift_id" value="{{ $gift->id }}">
                            <div id="regaloDetails" class="wide-max-md">
                                {{-- Regalo Details Form --}}
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{___('Name')}} 
										@if($gift->status == 'active') 
										<span class="badge badge-success">{{___('Active')}}</span>
										@else
										<span class="badge badge-danger">{{___('Inactive')}}</span>
										@endif 
                                    </label>
                                <div class="input-wrap">
                                    <input class="input-bordered" type="text" name="name" value="{{ $gift->name }}" required>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">{{___('Total')}} Tokens</label>
                                           <div class="input-wrap">
                                            <input class="input-bordered" type="number" min="1" name="total_tokens" value="{{ $gift->total_tokens }}" required>
                                            </div>
                                            <span class="input-note">{{___('Define how many tokens are required')}}.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">{{___('Total Referrals')}}</label>
                                           <div class="input-wrap">
                                            <input class="input-bordered" type="number" min="0" name="total_referrals" value="{{ $gift->total_guests }}" required>
                                            </div>
                                            <span class="input-note">{{___('Define how many referrals are required')}}.</span>
                                        </div>
                                    </div>
                         
                           
                                    <div class="col-12 mb-4 mt-1">
                                        <div class="sap"></div>
                                    </div>
									
									<div class="col-12">
                                        <div class="input-item input-with-label">
                                            <div class="row guttar-15px align-items-center">
                                                <div class="col-12">
                                                    <div class="input-wrap">
                                                        <input type="checkbox" class="input-switch" id="status"  name="status" {{($gift->status == 'active')?'checked ':''}}>
                                                        <label for="status">{{___('Active')}}</label>
                                                    </div>
                                                    <span class="input-note">{{___('Disable this, if you want to stop revutokens temporary')}}.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
									
									<div class="col-12 mb-4 mt-1">
                                        <div class="sap"></div>
                                    </div>
									
									<div class="col-12">
										<div class="input-item input-with-label">
											<label class="input-item-label">{{___('Comments')}}</label>
										   <div class="input-wrap">
											<input class="input-bordered" type="text" name="comment" value="{{ $gift->comment }}">
											</div>
											<span class="input-note">{{___('Additional references')}}.</span>
										</div>
                                    </div>                                    
                                    
                                </div>
                                <div class="gaps-1-5x"></div>
                                <div class="d-flex">
                                    <button class="btn btn-primary save-disabled" type="submit" disabled="">{{___('Revutokens Gift')}}</button>
                                </div>
                                {{-- .form-wrap --}}
                            </div>
                        </form>
                    </div>{{-- .tab-pane --}}
					
				{{-- .tab-pane --}}			   
				   
				  {{-- .tab-pane --}}

</div>{{-- .tab-content --}}
</div>
</div>
</div>{{-- .container --}}
</div>{{-- .page-content --}}

@endsection