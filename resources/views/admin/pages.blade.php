@extends('layouts.admin')
@section('title', ___('Page Manage'))

@section('content')

    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            @include('partials.messages-admin')
            <div class="row">
                <div class="col">
                    <div class="content-area card">
                        <div class="card-innr">
                            <div class="card-head has-aside">
                                <h4 class="card-title">Main Pages List</h4>
                            </div>
                            <div class="gaps-1x"></div>
                            <table class="table table-even-odd table-page table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th>Page Title</th>
                                        <th>Blocked Type</th>
                                        <th><span>Status</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($mainPages as $page)
                                        <tr class="page-{{ $page->id }}">
                                            <td>
                                                <h5>{{ str_limit($page->title, 25) }}</h5>
                                            </td>
                                            <td>
                                                <form class="form" action="{{ route('guardar.paginasBloqueadas') }}"
                                                    method="post">
                                                    <div class="row d-none " id="inputs{{ $page->id }}">

                                                        @csrf
                                                        <div class="col-4"><input type="text" class="form-control "
                                                                name="blocked"
                                                                value="{{ isset($page->blocked) ? $page->blocked : '[]' }}">
                                                        </div>
                                                        <input type="hidden" name="page_id" value="{{ $page->id }}">
                                                        <div class="col-4"><button class="btn btn-success"><i
                                                                    class="fa fa-save"></i></button></div>



                                                    </div>
                                                </form>
                                                <a href="#" class="showInput" data-id="{{ $page->id }}">
                                                    <h5
                                                        class="badge badge-{{ isset($page->blocked) ? 'info' : 'danger' }} badge-lg text-white">
                                                        {{ isset($page->blocked) ? $page->blocked : ___('Vacio') }}</h5>
                                                </a>
                                            </td>
                                            <td class="text-right">
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch switch" name="token_wallet_req"
                                                        {{ showPageStatus($page->status) == 1 ? 'checked ' : 0 }}
                                                        type="checkbox" id="{{ $page->id }}">
                                                    <label for="{{ $page->id }}"><span>Disabled</span><span
                                                            class="over">Enabled</span></label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> {{-- .card-innr --}}
                    </div> {{-- .content-area --}}
                </div>{{-- .col --}}
            </div>{{-- .row --}}
            <div class="row">
                <div class="col">
                    <div class="content-area card">
                        <div class="card-innr">
                           
                            <div class="card-head has-aside">
                                <h4 class="card-title">Pages List</h4>
                                <div class="card-opt">
                                    <ul class="btn-grp btn-grp-block guttar-20px">
                                        <li>
                                            <a href="{{ route('admin.add.page') }}"
                                                class="btn btn-sm btn-auto btn-primary">
                                                <em class="fas fa-plus-circle"></em><span>New <span
                                                        class="d-none d-sm-inline-block">Page</span></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="gaps-1x"></div>

                            <table class="table table-even-odd table-page table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th>Page Title</th>
                                        <th><span>Menu Name</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pages as $page)
                                        <tr class="page-{{ $page->id }}">
                                            <td>
                                                <h5><i class="fa {{ $page->icon }}"></i>
                                                    {{ str_limit($page->title, 25) }}
                                                </h5>
                                            </td>
                                            <td>
                                                <p>{{ $page->menu_title }}</p>
                                            </td>
                                            <td class="text-right">
                                                <ul class="btn-group guttar-8px">
                                                    <li><a class="btn btn-circle btn-xs btn-icon btn-lighter"
                                                            href="{{ route('admin.pages.edit', ['slug' => $page->slug]) }}"
                                                            data-slug="{{ $page->slug }}"><em
                                                                class="far fa-edit"></em></a></li>
                                                    <li>
                                                        <form action="">
                                                    <li> <a href="javascript:void(0)"
                                                            onclick="sweetAlert1({{ $page->id }})"
                                                            class="btn btn-circle btn-xs btn-icon btn-danger"
                                                            title="Delete question"><em class="ti ti-trash"></em></a></li>
                                                    </form>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div> {{-- .card-innr --}}

                    </div> {{-- .content-area --}}
                </div>{{-- .col --}}
            </div>{{-- .row --}}

            {{-- <div class="aside sidebar-right col-lg-4">
                <div class="token-sales card">
                    <div class="card-innr">
                        <div class="card no-hover-shadow">
                            <div class="card-head">
                                <h5 class="card-title-md">{{___('White Paper')}}</h5>
                            </div>
                            <div class="sap"></div>
                            <div class="pdt-3x">
                                <h6>{{___('Upload your whitepaper here')}}.</h6>
                                <div class="upload-box">
                                    <div class="wh-upload-zone whitepaper_upload">
                                        <div class="dz-message" data-dz-message>
                                            <span class="dz-message-text">{{___('Drag and drop file')}}</span>
                                            <span class="dz-message-or">or</span>
                                            <button type="button" class="btn btn-primary">SELECT</button>
                                        </div>
                                        <input type="hidden" name="whitepaper_file" accept="application/pdf" />
                                    </div>
                                    <small>Accept : pdf</small>
                                    <div class="hiddenFiles"></div>
                                    <div class="pt-3">
                                        @if (get_setting('site_white_paper') != '')
                                            <strong>White paper : </strong><a href="{{ route('public.white.paper') }}"
                                                target="_blank">{{ get_setting('site_white_paper') }}</a>
                                        @else
                                            <p class="text-light">No file uploaded yet!</p>
                                        @endif
                                    </div>
                                    <input type="hidden" name="whitepaper_file" accept="application/pdf" />
                                </div>
                                <small>}{{___('Accept')}} : pdf</small>
                                <div class="hiddenFiles"></div>
                                <div class="pt-3">
                                    @if (get_setting('site_white_paper') != '')
                                    <strong>{{___('White paper')}} : </strong><a href="{{ route('public.white.paper') }}" target="_blank" >{{get_setting('site_white_paper')}}</a>
                                    @else
                                    <p class="text-light">{{___('No file uploaded yet')}}!</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div> --}}



        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection
@push('footer')
    <link preload href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css') }}?ver=1.0">
    <script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>

    <script type="text/javascript">
        $(".switch").click(function() {
            $.ajax({
                method: "get",
                url: "update/status/" + $(this).attr('id')
            })
        });
        $('.showInput').click(function() {
            var id = $(this).attr('data-id')
            console.log($(this).attr('data-id'))
            $('#inputs' + id).removeClass('d-none')
            $(this).addClass('d-none')
        });

        function sweetAlert1(id) {
            var html;
            const wrapper = document.createElement('div');
            wrapper.innerHTML = html;
            swal({
                title: "Want to delete page?",
                icon: "warning",
                text: "Please proceed, If you really want to delete all the page permanently from database.",
                buttons: ["Cancel", "Yes"],
                dangerMode: !0,
            }).then((value) => {
                if (value == true) {
                    $.ajax({
                        method: "get",
                        url: "delete/pag/" + id,
                        success: function(result) {
                            window.location.reload();
                        }
                    })
                }
            });
        }
    </script>
@endpush
