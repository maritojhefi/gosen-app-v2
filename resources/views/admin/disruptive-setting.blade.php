@extends('layouts.admin')
@section('title', ___('Disruptive Settings'))

@section('content')
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            <div class="row">
                <div class="col">
                    <div class="content-area card">
                        <div class="card-innr">
                            @include('layouts.messages')
                            <div class="card-head has-aside">
                                <h4 class="card-title">{{ ___('Disruptive Settings') }}</h4>
                            </div>
                            <div class="gaps-1x"></div>
                            <table class="table table-even-odd table-page table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th>{{ ___('Setting Title') }}</th>
                                        <th><span>{{ ___('Status') }}</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (json_decode($cardsProgreso->value) as $key => $tipo_user)
                                        <tr>
                                            <td style="background-color: {{ asset(theme_color()) }}">
                                                <h5 class="text-white">
                                                    <strong>{{ ___('Tipo de usuarios :') . ' ' . str_replace('type_', '', $key) }}</strong>
                                                </h5>
                                            </td>
                                            <td style="background-color: {{ asset(theme_color()) }}">
                                            </td>
                                        </tr>
                                        @foreach ($tipo_user as $card => $estado_card)
                                            <tr>
                                                <td>
                                                    {{ $card }}
                                                </td>
                                                <td>
                                                    <div class="input-wrap input-wrap-switch">
                                                        <input class="input-switch switch" name="progress-card"
                                                            {{ $estado_card == 1 ? 'checked ' : 0 }} type="checkbox"
                                                            onclick="cambiarEstado('{{ $key }}','{{ $card }}', '{{ $estado_card }}')"
                                                            method="post" id="estado{{ $key.$card }}">
                                                        <label
                                                            for="estado{{ $key.$card }}"><span>{{ ___('Disabled') }}</span><span
                                                                class="over">{{ ___('Enabled') }}</span></label>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script>
        function cambiarEstado(tipo, card, estado_card) {
            $.ajax({
                method: "post",
                url: "settings/status/progress/" + tipo + '/' + card,
                data: {
                    type: 'modal',
                    '_token': '{{ csrf_token() }}'
                },
                success: function(result) {
                    if (result == 'enabled') {
                        $("#estado" + id).html("enabled");
                    } else {
                        $("#estado" + id).html("disabled");
                    }
                }
            })
        }
    </script>
@endpush
