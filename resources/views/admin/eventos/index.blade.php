@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container">
            @include('partials.messages-admin')
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">{{___('Events')}}</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li>
                                    <a href="#" class="btn btn-sm btn-auto btn-primary" data-toggle="modal"
                                        data-target="#addEvent">
                                        <em class="fas fa-plus-circle"></em><span>{{___('Add')}}<span
                                                class="d-none d-sm-inline-block">{{___('Events')}}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="gaps-1-5x"></div>
                    <div class="row guttar-vr-30px">

                        @foreach ($eventos as $eve)
                        <div class="col-xl-4 col-md-6">
                            <div class="stage-item stage-card">
                                <div class="stage-head">
                                    <div class="stage-title">
                                        <h4>{{___('Notification')}} {{ $eve->id }} <span class="badge-outline badge badge-success">{{ $eve->alcance }}</span></h4>
                                    </div>

                                    <div class="stage-action">
                                        <a href="#" class="toggle-tigger rotate"><em class="ti ti-more-alt"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list">
                                                <li><a href="" data-toggle="modal" data-target="#editEvent{{$eve->id}}">{{___('Edit')}}</a>
                                                    <form action="{{ route('admin.eventos.destroy', $eve->id) }}">
                                                <li><a href="javascript:void(0)" onclick="sweetAlert1({{ $eve->id }})">{{___('Delete')}}</a></li>
                                                    </form>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                @if ($eve->image != null)
                                <div class="stage-info" style="border-bottom: none;">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="stage-info-graph">
                                                <img loading="lazy" src="{{ asset2('imagenes/'.$eve->image) }}" alt="TokenLite">
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="stage-info-txt">
                                                <h5>{{__($eve->body) }}</h5>
                                                <p>{{__($eve->footer) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="stage-info">
                                    <div class="row">
                                        <div class="col" style="text-align: center">
                                            <div class="stage-info-txt">
                                                <h5>{{ __($eve->body) }}</h5>
                                                <p>{{ __($eve->footer) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>{{-- .stage-card --}}
                        </div>{{-- .col --}}
                        @endforeach
                    </div>
                    <div class="gaps-0-5x"></div>
                </div>
            </div>
        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection

@section('modals')
    <div class="modal fade" id="addEvent">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body popup-body-md">
                    <h3 class="popup-title">{{___('Add Events')}}</h3>
                    <form action="{{ route('admin.eventos.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{___('Body of Notify')}}</label>
                                    <div class="input-wrap">
                                        <textarea class="input-bordered input-textarea" required="" name="body"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{___('Footer of Notify')}}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" name="footer" placeholder="Optional">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{___('Image')}}</label>
                                    <div class="relative">
                                        <em class="input-file-icon fas fa-upload"></em>
                                        <input type="file" class="input-file" name="image" id="file-01">
                                        <label for="file-01">{{___('Choose a file')}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label d-none d-sm-inline-block">{{___('Event For')}}</label>
                                    {{-- <div class="input-wrap mt-sm-2"> --}}
                                        <select class="input-bordered" name="alcance" id="">
                                            <option value="all">{{___('All')}}</option>
                                            <option value="user">{{___('User')}}</option>
                                            <option value="admin">{{___('Admin')}}</option>
                                        </select>
                                    {{-- </div> --}}
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">{{___('Add Event')}}</button>
                        <div class="gaps-3x"></div>
                    </form>
                </div>
            </div>{{-- .modal-content --}}
        </div>{{-- .modal-dialog --}}
    </div>
    {{-- Modal End --}}

@foreach ($eventos as $eve)
<div class="modal fade" id="editEvent{{$eve->id}}">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                    class="ti ti-close"></em></a>
            <div class="popup-body popup-body-md">
                <h3 class="popup-title">{{___('Edit Events')}}</h3>
                <form action="{{ route('admin.eventos.update',$eve->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Body of Notify')}}</label>
                                <div class="input-wrap">
                                    <textarea class="input-bordered input-textarea" required="" name="body">{{ __($eve->body) }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Footer of Notify')}}</label>
                                <div class="input-wrap">
                                    <input class="input-bordered" type="text" name="footer" value="{{ $eve->footer }}" placeholder="{{___('Optional')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">{{___('Image')}}</label>
                                <div class="relative">
                                    <em class="input-file-icon fas fa-upload"></em>
                                    <input type="file" class="input-file" name="image" id="file-{{$eve->id}}">
                                    <label for="file-{{$eve->id}}">{{___('choose another file')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="input-item input-with-label">
                                <label class="input-item-label d-none d-sm-inline-block">&nbsp;</label>
                                <div class="input-wrap input-wrap-checkbox mt-sm-2">
                                    @if ($eve->image != null)
                                    <div class="stage-info-graph">
                                        <img loading="lazy" src="{{ asset2('imagenes/'.$eve->image) }}" alt="TokenLite">
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">{{___('Update Event')}}</button>
                    <div class="gaps-3x"></div>
                </form>
            </div>
        </div>{{-- .modal-content --}}
    </div>{{-- .modal-dialog --}}
</div>
{{-- Modal End --}}
@endforeach
    
@endsection
@push('footer')
<script type="text/javascript">
    function sweetAlert1(id) {
        var html;
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        swal({
            title: "Want to delete evento?",
            icon: "warning",
            text: "Please proceed, If you really want to delete the evento permanently from database.",
            buttons: ["Cancel", "Yes"],
            dangerMode: !0,
        }).then((value) => {
            if (value == true) {
                $.ajax({
                    method: "get",
                    url: "delete/evento/" + id,
                    success: function(result) {
                        if (result == "eliminado") {
                            swal("Deleted!", "Your evento has been deleted.", "success");
                            setTimeout(function() {
                                window.location.reload();
                            }, 0e1);
                        }
                    }
                })
            }
        });
    }
</script>
@endpush

