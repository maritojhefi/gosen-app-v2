@extends('layouts.admin')
@section('title', ___('Referral Setting'))
@section('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css"
        integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"
        integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
@section('content')
    <div class="page-content">
        <div class="container">
            @include('partials.messages-admin')
            <div class="row">
                <div class="col-md-12">
                    @include('vendor.notice')
                    <div class="content-area card">
                        <div class="card-innr">
                            <div class="card-head">
                                <h4 class="card-title">{{ ___('Referral Settings') }}</h4>
                            </div>
                            <div class="card-text">
                                <p>{{ ___('Manage your referral system to boost your token sales. Once enable referral system option, it will start tracking invitation link also user/contributor able to see invitation link on their profile. They can share any where to invite more people to join on your platform. You can specify how much bonus a user can get') }}.
                                </p>
                                <p class="text-head">
                                    <strong>{{ ___('Note') }}:
                                        {{ ___('To active referral system completly, you have set Show in Visibility by edit Referral page from') }}
                                        <a href="{{ route('admin.pages.edit', ['slug' => 'referral']) }}">{{ ___('Manage') }}>{{ ___('Page') }}</a>
                                    </strong>
                                </p>
                            </div>
                            <div class="gaps-2x"></div>
                            <div class="card-text ico-setting setting-token-referral">
                                <form action="{{ route('admin.ajax.settings.update') }}" method="POST"
                                    id="referral_setting_form" class="validate-modern">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">{{ ___('Referral System') }}</label>
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch switch-toggle"
                                                        data-switch="switch-to-referral" name="referral_system"
                                                        type="checkbox"
                                                        {{ get_setting('referral_system') == 1 ? 'checked ' : '' }}id="referral-system-enable">
                                                    <label
                                                        for="referral-system-enable"><span>{{ ___('Disable') }}</span><span
                                                            class="over">{{ ___('Enable') }}</span></label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="switch-content switch-to-referral">
                                                    <h5 class="card-title-sm text-secondary">{{ ___('Referral User') }}
                                                        <small
                                                            class="ucap text-primary">({{ ___('who referred') }})</small>
                                                        <small
                                                            class="ucap text-primary">({{ ___('Level 0 - First Referral') }})</small>
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-sm-6">
                                                            <div class="input-item input-with-label">
                                                                <label
                                                                    class="input-item-label">{{ ___('Referral Bonus Allowed') }}</label>
                                                                <div class="input-wrap">
                                                                    <select id="bonus_applicable"
                                                                        class="select select-block select-bordered "
                                                                        name="referral_allow">
                                                                        <option
                                                                            {{ get_setting('referral_allow') == 'all_time' ? 'selected' : '' }}
                                                                            value="all_time">
                                                                            {{ ___('For All Transactions') }}
                                                                        </option>
                                                                        @foreach ($general->steps_refer as $step)
                                                                            <option
                                                                                {{ get_setting('referral_allow') == $step ? 'selected ' : '' }}value="{{ $step }}">
                                                                                {{ ___('Max') }}
                                                                                {{ to_num_token($step) }}
                                                                                Tokens</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <span
                                                                    class="input-note">{{ ___('Limit with referral bonus amount, how much bonus will add into account for invite someone') }}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-sm-6">
                                                            <div class="input-item input-with-label">
                                                                <label
                                                                    class="input-item-label">{{ ___('Offering Type') }}</label>
                                                                <div class="input-wrap">
                                                                    <select class="select select-block select-bordered"
                                                                        name="referral_calc">
                                                                        <option
                                                                            {{ get_setting('referral_calc') == 'percent' ? 'selected ' : '' }}value="percent">
                                                                            {{ ___('Percentage') }}</option>
                                                                        <option
                                                                            {{ get_setting('referral_calc') == 'fixed' ? 'selected ' : '' }}value="fixed">
                                                                            {{ ___('Fixed/Flat') }}</option>
                                                                    </select>
                                                                </div>
                                                                <span
                                                                    class="input-note">{{ ___('Choose whether the referral bonus will calculated as percentage or fixed amount') }}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-lg-3">
                                                            <div class="input-item input-with-label">
                                                                <label class="input-item-label">Bonus -
                                                                    {{ ___('Offer Amount') }}</label>
                                                                <div class="input-wrap wide-15">
                                                                    <input type="number" class="input-bordered"
                                                                        min="0" name="referral_bonus"
                                                                        value="{{ get_setting('referral_bonus') }}">
                                                                    <span
                                                                        class="input-hint input-hint-lg"><span>&nbsp;&nbsp;</span></span>
                                                                </div>
                                                                <div class="input-note">
                                                                    {{ ___('Specify bonus amount for who referred') }}.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="gaps-1x"></div>
                                                    <div class="d-flex">
                                                        @csrf
                                                        <input type="hidden" name="type" value="referral">
                                                        <button class="btn btn-primary save-disabled" type="submit"
                                                            disabled><i
                                                                class="ti ti-reload"></i><span>{{ ___('Update') }}</span></button>
                                                    </div>
                                                    <div class="gaps-2x"></div>
                                                    <div class="hint"><em><strong>{{ ___('Note') }}:</strong>
                                                            {{ ___('Bonus will automatically adjust after each successfull transaction. The token balance add into contributor account who referred (and/or who join)') }}.</em>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                                <div class="card-head mt-4">
                                    <h4 class="card-title">{{ ___('Advanced Options') }}</h4>
                                </div>
                                <x-level-bonus-dollar-component />
                            </div>
                        </div>{{-- .card-innr --}}
                    </div>{{-- .card --}}

                </div>{{-- .col --}}
            </div>{{-- .container --}}
        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection
@push('footer')
    <script>
        $(document).ready(function() {
            $('.num').keyup(function() {
                if ($(".num").filter(function() {
                        return $.trim($(this).val()).length == 0
                    }).length == 0) {
                    var int = $('#int').val();
                    if (int != 'int') {

                        $('#buttons').attr('disabled', false);
                    } else {
                        $('#buttons').attr('disabled', true);
                    }
                } else {
                    $('#buttons').attr('disabled', true);
                }
            })
        });
    </script>
    <script>
        function getFromData(formulario) {
            var aid = $('#' + formulario).serializeArray()[1].value;
            var max = $('#' + formulario).serializeArray()[2].value;
            var type = $('#' + formulario).serializeArray()[3].value;
            var value = $('#' + formulario).serializeArray()[4].value;
            var ruta = $('#url').val();
            $.ajax({
                type: "GET",
                url: ruta + 'level/bonus/doll',
                data: {
                    id: aid,
                    type: type,
                    value: value,
                    max: max,
                    '_token': '{{ csrf_token() }}',
                },
                success: function(json) {
                    displaySuccessToaster(json.msg, json.message);
                }
            });

            function displaySuccessToaster(tipo, mensaje) {
                console.log(tipo + ' ' + mensaje);
                if (tipo == 'success') {
                    toastr.success(mensaje);
                } else if (tipo == 'error') {
                    toastr.error(mensaje);
                }
            }
        };
    </script>
@endpush
