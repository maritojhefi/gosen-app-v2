@extends('layouts.admin')
@section('title', ___('Revutokens Setting'))
@section('content')
<div class="page-content">
    <div class="container">
        @include('vendor.notice')
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">{{___('Gosen Tokens Bonus')}}</h4>
                </div>
                <div class="gaps-1-5x"></div>
                <div class="row guttar-vr-30px">
                    @forelse($gift as $stage)
                    <div class="col-xl-4 col-md-6">
                        <div class="stage-item stage-card">
                            <div class="stage-head">
                                <div class="stage-title">
                                    <h6>{{$stage->name}}&nbsp;</h6>
                                    @if($stage->status == 'active') 
                                    <span class="badge badge-success">{{___('Active')}}</span>
                                    @else
                                    <span class="badge badge-danger">{{___('Inactive')}}</span>
                                    @endif                                  
                                </div>
                                
                                <div class="stage-action">
                                    <a href="#" class="toggle-tigger rotate"><em class="ti ti-more-alt"></em></a>
                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                        <ul class="dropdown-list">
                                            <li><a href="javascript:void(0);" class="gift-view-ajax-action" data-action="overview" data-view="modal" data-gift="{{$stage->id}}">{{___('Overview')}}</a></li>
                                            <li><a href="{{route('admin.regalos.edit', $stage->id)}}">{{___('Update')}}</a></li>                                     
                                        </ul>
                                    </div>
                                </div>
                            </div>               
                             <div class="stage-info">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="stage-info-txt">
                                            <h6>{{___('Total')}} Tokens</h6>
                                            <div class="h2 stage-info-number">{{$stage->total_tokens}}</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="stage-info-txt">
                                            <h6>{{___('Total Referrals')}}</h6>
                                            <div class="h2 stage-info-number">{{ $stage->total_guests }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>{{-- .stage-card --}}
                    </div>{{-- .col --}}
                    @empty
                    <span>{{___('No Stage Found')}}</span>
                    @endforelse

                </div>
                <div class="gaps-0-5x"></div>
            </div>
        </div>
    </div>{{-- .container --}}
</div>{{-- .page-content --}}
@endsection