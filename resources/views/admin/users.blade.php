@extends('layouts.admin')
@section('title', ucfirst(__($is_page)) . ___(' User List'))
@section('content')
    <div class="page-content">
        <div class="container">
            @include('layouts.messages')
            @include('vendor.notice')
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{ ucfirst(__($is_page)) }} {{ ___('User List') }}</h4>

                        <div class="col-6" style="text-align:end;">
                            <a href="{{ route('admin.excel') }}" class="btn btn-auto btn-sm btn-success">
                                <em class="fas fa-file-excel"></em><span> <span
                                        class="d-none d-md-inline-block">Excel</span></span>
                            </a>
                            <a href="{{ route('admin.pdf') }}" class="btn btn-auto btn-sm btn-danger" target="_blank">
                                <em class="fas fa-file-pdf"></em><span> <span
                                        class="d-none d-md-inline-block">PDF</span></span>
                            </a>
                        </div>

                        <div class="relative d-inline-block d-md-none">
                            <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                    class="ti ti-more-alt"></em></a>
                            <div class="toggle-class dropdown-content dropdown-content-center-left pd-2x">
                                <div class="card-opt data-action-list">

                                    <ul class="btn-grp btn-grp-block guttar-20px guttar-vr-10px">
                                        <li>
                                            <a class="btn btn-auto btn-info  btn-sm"
                                                href="{{ route('admin.users.wallet.change') }}">{{ ___('Wallet Change Request') }}</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.user-create') }}"
                                                class="btn btn-auto btn-sm btn-primary">
                                                <em class="fas fa-plus-circle"> </em>
                                                <span>{{ ___('Add') }} <span
                                                        class="d-none d-md-inline-block">{{ ___('User') }}</span></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-opt data-action-list d-none d-md-inline-flex">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li><a class="btn btn-info  btn-sm"
                                        href="{{ route('admin.users.wallet.change') }}">{{ ___('Wallet
                                                                                                                                                                Change Request') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.user-create') }}" class="btn btn-auto btn-sm btn-primary">
                                        <em class="fas fa-plus-circle"> </em><span>{{ ___('Add') }} <span
                                                class="d-none d-md-inline-block">{{ ___('User') }}</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="page-nav-wrap">
                        <div class="page-nav-bar justify-content-between bg-lighter">
                            <div class="page-nav w-100 w-lg-auto">
                                <ul class="nav">
                                    <li class="nav-item{{ is_page('users.user') ? ' active' : '' }}"><a class="nav-link"
                                            href="{{ route('admin.users', 'user') }}">{{ ___('Investor') }} /
                                            {{ ___('Users') }}</a></li>
                                    <li class="nav-item {{ is_page('users.admin') ? ' active' : '' }}"><a class="nav-link"
                                            href="{{ route('admin.users', 'admin') }}">{{ ___('Admin
                                                                                                                                                                                Account') }}</a>
                                    </li>
                                    <li class="nav-item {{ is_page('users') ? ' active' : '' }}"><a class="nav-link"
                                            href="{{ route('admin.users') }}">{{ ___('All') }}</a></li>
                                </ul>
                            </div>
                            <div class="search flex-grow-1 pl-lg-4 w-100 w-sm-auto">
                                <form action="{{ route('admin.users') }}" method="GET" autocomplete="off">
                                    <div class="input-wrap">
                                        <span class="input-icon input-icon-left"><em class="ti ti-search"></em></span>
                                        <input type="search" class="input-solid input-transparent"
                                            placeholder="{{ ___('Quick search with name/email') }}/id/{{ ___('wallet address') }}"
                                            value="{{ request()->get('s', '') }}" name="s">
                                    </div>
                                </form>
                            </div>
                            @if (!empty(env_file()) && nio_status() && !empty(app_key()))
                                <div class="tools w-100 w-sm-auto" align="right">
                                    <ul class="btn-grp guttar-8px">
                                        <li>
                                            <form action="{{ route('admin.ajax.users.delete') }}" method="POST">
                                        <li><a href="javascript:void(0)" title="{{ ___('Delete all unvarified users') }}"
                                                data-toggle="tooltip"
                                                class="btn btn-danger btn-icon  btn-sm delete-unverified-user mr-md-2"> <em
                                                    class="ti ti-trash"></em> </a></li>
                                        </form>
                                        </li>

                                        <li><a href="#" class="btn btn-light btn-sm btn-icon  bg-white advsearch-opt">
                                                <em class="ti ti-panel"></em> </a></li>
                                        <li>
                                            <div class="relative">
                                                <a href="#"
                                                    class="btn btn-light bg-white btn-sm btn-icon toggle-tigger "><em
                                                        class="ti ti-server"></em> </a>
                                                <div
                                                    class="toggle-class dropdown-content dropdown-content-sm dropdown-content-center shadow-soft">
                                                    <ul class="dropdown-list">
                                                        <li>
                                                            <h6 class="dropdown-title">{{ ___('Export') }}</h6>
                                                        </li>
                                                        <li><a
                                                                href="{{ route('admin.export', array_merge(['table' => 'users', 'format' => 'entire'], request()->all())) }}">{{ ___('Entire') }}</a>
                                                        </li>
                                                        <li><a
                                                                href="{{ route('admin.export', array_merge(['table' => 'users', 'format' => 'minimal'], request()->all())) }}">{{ ___('Minimal') }}</a>
                                                        </li>
                                                        <li><a
                                                                href="{{ route('admin.export', array_merge(['table' => 'users', 'format' => 'compact'], request()->all())) }}">{{ ___('Compact') }}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="relative">
                                                <a href="#"
                                                    class="btn btn-light bg-white btn-sm btn-icon toggle-tigger "><em
                                                        class="ti ti-settings"></em> </a>
                                                <div
                                                    class="toggle-class dropdown-content dropdown-content-sm dropdown-content-center shadow-soft">
                                                    <form class="update-meta" action="#" data-type="user_page_meta">
                                                        <ul class="dropdown-list">
                                                            <li>
                                                                <h6 class="dropdown-title">{{ ___('Show') }}</h6>
                                                            </li>
                                                            <li{!! gmvl('user_per_page', 10) == 10 ? ' class="active"' : '' !!}>
                                                                <a href="#" data-meta="perpage=10">10</a>
                                        </li>
                                        <li{!! gmvl('user_per_page', 10) == 20 ? ' class="active"' : '' !!}>
                                            <a href="#" data-meta="perpage=20">20</a></li>
                                            <li{!! gmvl('user_per_page', 10) == 50 ? ' class="active"' : '' !!}>
                                                <a href="#" data-meta="perpage=50">50</a></li>
                                    </ul>
                                    <ul class="dropdown-list">
                                        <li>
                                            <h6 class="dropdown-title">{{ ___('Order By') }}</h6>
                                        </li>
                                        <li{!! gmvl('user_order_by', 'id') == 'id' ? ' class="active"' : '' !!}>
                                            <a href="#" data-meta="orderby=id">{{ ___('User') }} ID</a></li>
                                            <li{!! gmvl('user_order_by', 'id') == 'name' ? ' class="active"' : '' !!}>
                                                <a href="#" data-meta="orderby=name">{{ ___('Name') }}</a></li>
                                                <li{!! gmvl('user_order_by', 'id') == 'token' ? ' class="active"' : '' !!}>
                                                    <a href="#" data-meta="orderby=token">Token</a></li>
                                    </ul>
                                    <ul class="dropdown-list">
                                        <li>
                                            <h6 class="dropdown-title">Order</h6>
                                        </li>
                                        <li{!! gmvl('user_ordered', 'DESC') == 'DESC' ? ' class="active"' : '' !!}>
                                            <a href="#" data-meta="ordered=DESC">DESC</a></li>
                                            <li{!! gmvl('user_ordered', 'DESC') == 'ASC' ? ' class="active"' : '' !!}>
                                                <a href="#" data-meta="ordered=ASC">ASC</a></li>
                                    </ul>
                                    </form>
                                </div>
                        </div>
                        </li>
                        </ul>
                    </div>
                    @endif
                </div>
                @if (!empty(env_file()) && nio_status() && !empty(app_key()))
                    <div class="search-adv-wrap hide">
                        <form class="adv-search" id="adv-search" action="{{ route('admin.users') }}" method="GET"
                            autocomplete="off">
                            <div class="adv-search">
                                <div class="row align-items-end guttar-20px guttar-vr-15px">
                                    <div class="col-lg-6">
                                        <div class="input-grp-wrap">
                                            <span
                                                class="input-item-label input-item-label-s2 text-exlight">{{ ___('Advanced
                                                                                                                                                                                                Search') }}</span>
                                            <div class="input-grp align-items-center bg-white">
                                                <div class="input-wrap flex-grow-1">
                                                    <input value="{{ request()->get('search') }}"
                                                        class="input-solid input-solid-sm input-transparent"
                                                        type="text" placeholder="{{ ___('Search by user') }}"
                                                        name="search">
                                                </div>
                                                <ul class="search-type">
                                                    <li class="input-wrap input-radio-wrap">
                                                        <input name="by" value="" class="input-radio-select"
                                                            id="advs-by-name" type="radio" id="advs-by-name"
                                                            {{ empty(request()->by) || (request()->by != 'email' && request()->by != 'id') ? ' checked' : '' }}>
                                                        <label for="advs-by-name">{{ ___('Name') }}</label>
                                                    </li>
                                                    <li class="input-wrap input-radio-wrap">
                                                        <input name="by" value="email" class="input-radio-select"
                                                            id="advs-by-email" type="radio" id="advs-by-email"
                                                            {{ isset(request()->by) && request()->by == 'email' ? ' checked' : '' }}>
                                                        <label for="advs-by-email">{{ ___('Email') }}</label>
                                                    </li>
                                                    <li class="input-wrap input-radio-wrap">
                                                        <input name="by" value="id" class="input-radio-select"
                                                            id="advs-by-id" type="radio" id="advs-by-id"
                                                            {{ isset(request()->by) && request()->by == 'id' ? ' checked' : '' }}>
                                                        <label for="advs-by-id">ID</label>
                                                    </li>
                                                    <li class="input-wrap input-radio-wrap">
                                                        <input name="by" value="walletAddress"
                                                            class="input-radio-select" id="advs-by-walletAddress"
                                                            type="radio" id="advs-by-walletAddress"
                                                            {{ isset(request()->by) && request()->by == 'walletAddress' ? ' checked' : '' }}>
                                                        <label for="advs-by-walletAddress">{{ ___('Wallet') }}</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 col-mb-6">
                                        <div class="input-wrap input-item-middle text-left">
                                            <input {{ request()->get('wallet') == 'yes' ? 'checked' : '' }} name="wallet"
                                                value="yes" class="input-checkbox input-checkbox-md" id="has-wallet"
                                                type="checkbox">
                                            <label for="has-wallet">{{ ___('Has Wallet') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-8 col-mb-6">
                                        <div class="input-wrap input-item-middle text-left">
                                            <input {{ request()->get('adm') == 'yes' ? 'checked' : '' }} name="adm"
                                                value="yes" class="input-checkbox input-checkbox-md"
                                                id="include-admin" type="checkbox">
                                            <label for="include-admin">{{ ___('Including Admin') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap input-with-label">
                                            <label
                                                class="input-item-label input-item-label-s2 text-exlight">{{ ___('Account
                                                                                                                                                                                                Status') }}</label>
                                            <select name="state" class="select select-sm select-block select-bordered"
                                                data-dd-class="search-off">
                                                <option value="">{{ ___('Any Status') }}</option>
                                                <option{{ request()->get('state') == 'active' ? ' selected' : '' }}
                                                    value="active">{{ ___('Actived') }}</option>
                                                    <option{{ request()->get('state') == 'suspend' ? ' selected' : '' }}
                                                        value="suspend">{{ ___('Suspended') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap input-with-label">
                                            <label
                                                class="input-item-label input-item-label-s2 text-exlight">{{ ___('Reg
                                                                                                                                                                                                Method') }}</label>
                                            <select name="reg" class="select select-sm select-block select-bordered"
                                                data-dd-class="search-off">
                                                <option value="">{{ ___('Any Method') }}</option>
                                                <option{{ request()->get('reg') == 'internal' ? ' selected' : '' }}
                                                    value="internal">{{ ___('Internal') }}</option>
                                                    <option{{ request()->get('reg') == 'email' ? ' selected' : '' }}
                                                        value="email">{{ ___('Email') }}</option>
                                                        <option{{ request()->get('reg') == 'google' ? ' selected' : '' }}
                                                            value="google">Google</option>
                                                            <option{{ request()->get('reg') == 'facebook' ? ' selected' : '' }}
                                                                value="facebook">Facebook</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap input-with-label">
                                            <label
                                                class="input-item-label input-item-label-s2 text-exlight">{{ ___('Verified
                                                                                                                                                                                                Status') }}</label>
                                            <select name="valid" class="select select-sm select-block select-bordered"
                                                data-dd-class="search-off">
                                                <option value="">{{ ___('Anything') }}</option>
                                                <option{{ request()->get('valid') == 'email' ? ' selected' : '' }}
                                                    value="email">{{ ___('Email Verified') }}</option>
                                                    <option{{ request()->get('valid') == 'kyc' ? ' selected' : '' }}
                                                        value="kyc">KYC {{ ___('Verified') }}</option>
                                                        <option{{ request()->get('valid') == 'both' ? ' selected' : '' }}
                                                            value="both">{{ ___('Both Verified') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap input-with-label">
                                            <label class="input-item-label input-item-label-s2 text-exlight">Token
                                                {{ ___('Balance') }}</label>
                                            <select name="token" class="select select-sm select-block select-bordered"
                                                data-dd-class="search-off">
                                                <option value="">{{ ___('Any Amount') }}</option>
                                                <option {{ request()->get('token') == 'has' ? 'selected' : '' }}
                                                    value="has">{{ ___('Has') }} Token</option>
                                                <option {{ request()->get('token') == 'zero' ? 'selected' : '' }}
                                                    value="zero">{{ ___('Zero') }} Token</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap input-with-label">
                                            <label
                                                class="input-item-label input-item-label-s2 text-exlight">{{ ___('Is Referred
                                                                                                                                                                                                By') }}</label>
                                            <select name="refer" class="select select-sm select-block select-bordered"
                                                data-dd-class="search-off">
                                                <option value="">{{ ___('Anything') }}</option>
                                                <option {{ request()->get('refer') == 'yes' ? 'selected' : '' }}
                                                    value="yes">{{ ___('Yes') }}</option>
                                                <option {{ request()->get('refer') == 'no' ? 'selected' : '' }}
                                                    value="no">{{ ___('No') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-2 col-mb-6">
                                        <div class="input-wrap">
                                            <input type="hidden" name="filter" value="1">
                                            <button class="btn btn-sm btn-sm-s2 btn-auto btn-primary">
                                                <em class="ti ti-search width-auto"></em><span>{{ ___('Search') }}</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif

                @if (request()->get('filter') || request()->s)
                    <div class="search-adv-result">
                        <div class="search-info">{{ ___('Found') }} <span
                                class="search-count">{{ $users->total() }}</span>
                            {{ ___('Users') }}{{ isset(request()->adm) && request()->adm == 'yes' ? ' including admin user.' : '.' }}
                        </div>
                        <ul class="search-opt">
                            @if (request()->get('search'))
                                <li><a href="{{ qs_url(qs_filter('search')) }}">{{ ___('Search') }}
                                        <span>'{{ request()->get('search') }}'</span>{{ !empty(request()->by) ? ' (' . (request()->by == 'id' ? strtoupper(request()->by) : ucfirst(request()->by)) . ')' : ' (Name)' }}</a>
                                </li>
                            @endif
                            @if (request()->get('wallet'))
                                <li><a href="{{ qs_url(qs_filter('wallet')) }}">{{ ___('Has') }}
                                        <span>{{ ___('Wallet') }}</span></a></li>
                            @endif
                            @if (request()->get('token'))
                                <li><a href="{{ qs_url(qs_filter('token')) }}"><span>{{ ucfirst(request()->get('token')) }}</span>
                                        Token</a></li>
                            @endif
                            @if (request()->get('state'))
                                <li><a href="{{ qs_url(qs_filter('state')) }}">{{ ___('Status') }}:
                                        <span>{{ ucfirst(request()->get('state')) }}</span></a></li>
                            @endif
                            @if (request()->get('reg'))
                                <li><a href="{{ qs_url(qs_filter('reg')) }}">{{ ___('Reg Method') }}:
                                        <span>{{ ucfirst(request()->get('reg')) }}</span></a></li>
                            @endif
                            @if (request()->get('valid'))
                                <li><a href="{{ qs_url(qs_filter('valid')) }}">{{ ___('Verified') }}:
                                        <span>{{ request()->valid == 'kyc' ? strtoupper(request()->valid) : ucfirst(request()->valid) }}</span></a>
                                </li>
                            @endif
                            @if (request()->get('refer'))
                                <li><a href="{{ qs_url(qs_filter('refer')) }}">{{ ___('Referred') }}:
                                        <span>{{ ucfirst(request()->get('refer')) }}</span></a></li>
                            @endif
                            <li><a href="{{ route('admin.users') }}"
                                    class="link link-underline">{{ ___('Clear All') }}</a></li>
                        </ul>
                    </div>
                @endif
            </div>

            @if ($users->total() > 0)
                <table class="data-table user-list table-responsive">
                    <thead>
                        <tr class="data-item data-head">
                            <th class="data-col data-col-wd-md filter-data dt-user">{{ ___('User') }}</th>
                            <th class="data-col data-col-wd-md dt-email">{{ ___('Email') }}</th>
                            <th class="data-col dt-token">Tokens</th>
                            <th class="data-col dt-verify">{{ ___('Verified Status') }}</th>
                            <th class="data-col dt-login">{{ ___('Last Login') }}</th>
                            <th class="data-col dt-status">{{ ___('Status') }}</th>
                            <th class="data-col dt-status">{{ ___('Type Admin') }}</th>
                            <th class="data-col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr class="data-item">
                                <td class="data-col data-col-wd-md dt-user">
                                    <div class="d-flex align-items-center">
                                        <div class="fake-class">
                                            <span class="lead user-name text-wrap">{{ $user->name }}</span>
                                            <span class="sub user-id">{{ set_id($user->id, 'user') }}
                                                @if ($user->role == 'admin')
                                                    <span
                                                        class="badge badge-xs badge-dim badge-{{ $user->type != 'demo' ? 'success' : 'danger' }}">{{ ___('ADMIN') }}</span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col data-col-wd-md dt-email">
                                    <span
                                        class="sub sub-s2 sub-email text-wrap">{{ explode_user_for_demo($user->email, auth()->user()->type) }}</span>
                                </td>
                                <td class="data-col dt-token">
                                    <span class="lead lead-btoken">{{ number_format($user->tokenBalance) }}</span>
                                </td>
                                <td class="data-col dt-verify">
                                    <ul class="data-vr-list">
                                        <li>
                                            <div
                                                class="data-state data-state-sm data-state-{{ $user->email_verified_at !== null ? 'approved' : 'pending' }}">
                                            </div> {{ ___('Email') }}
                                        </li>
                                        @php
                                            if (isset($user->kyc_info->status)) {
                                                $user->kyc_info->status = str_replace('rejected', 'canceled', $user->kyc_info->status);
                                            }
                                            $kyc_a_bf = isset($user->kyc_info->id) ? '<a href="' . route('admin.kyc.view', [$user->kyc_info->id, 'kyc_details']) . '" target="_blank">' : '';
                                            $kyc_a_af = isset($user->kyc_info->id) ? '</a>' : '';
                                        @endphp
                                        @if ($user->role != 'admin')
                                            <li>{!! $kyc_a_bf !!}<div
                                                    class="data-state data-state-sm data-state-{{ !empty($user->kyc_info) ? $user->kyc_info->status : 'missing' }}">
                                                </div>KYC {!! $kyc_a_af !!}</li>
                                        @endif
                                        <li>
                                            <div class="fab fa-whatsapp mr-2 ml-2 text-{{ $user->whatsapp_status == true && $user->mobile != null ? 'success' : 'danger' }}"
                                                style="font-size: 27px;" data-toggle="tooltip" data-placement="bottom"
                                                title="" data-original-title="{{ $user->whatsapp_status == true && $user->mobile != null ? ___('Whastapp Verificado') : ___('Aún falta verificar el whatsapp')  }}">
                                            </div> {{ ___('Whatsapp') }}
                                        </li>

                                    </ul>
                                </td>
                                <td class="data-col dt-login">
                                    <span
                                        class="sub sub-s2 sub-time">{{ $user->lastLogin && $user->email_verified_at !== null ? _date($user->lastLogin) : 'Not logged yet' }}</span>
                                </td>
                                <td class="data-col dt-status">
                                    <span
                                        class="dt-status-md badge badge-outline badge-md badge-{{ __status($user->status, 'status') }}">{{ __status($user->status, 'text') }}</span>
                                    <span
                                        class="dt-status-sm badge badge-sq badge-outline badge-md badge-{{ __status($user->status, 'status') }}">{{ substr(__status($user->status, 'text'), 0, 1) }}</span>
                                </td>
                                <td class="data-col dt-type_user">
                                    <div class="row">


                                        {{-- <div class="col" style="padding:0%">
                                            <textarea readonly style="width:0px;height: 0px; resize:none; border: none; border-style:none; "
                                                id="resetPass{{$user->id}}"> {{ route('reset.password', Crypt::encrypt($user->id)) }} </textarea>
                                                
                                        </div> --}}

                                        <div class="col-9">
                                            @foreach ($menu as $item)
                                                <span
                                                    class="sub">{{ $item->id == $user->type_user + 1 ? $item->valor : '' }}
                                                </span>
                                            @endforeach
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col text-right">
                                    <div class="relative d-inline-block">
                                        <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                class="ti ti-more-alt"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list more-menu-{{ $user->id }}">


                                                <li><a href="{{ route('admin.send.mail', Crypt::encrypt($user->id)) }}"><em
                                                            class="fa fa-key"></em> {{ ___('Reset Password') }}</a></li>



                                                <li><a href="{{ route('admin.users.view', [$user->id, 'type_user']) }}"><em
                                                            class="far fa-user"></em> {{ ___('Type User') }}</a></li>
                                                <li><a
                                                        href="{{ route('admin.users.log.in', Crypt::encrypt($user->id)) }}"><em
                                                            class="fa fa-user"></em>{{ ___('Login As User') }}</a></li>
                                                <li><a href="{{ route('admin.users.view', [$user->id, 'details']) }}"><em
                                                            class="far fa-eye"></em> {{ ___('View Details') }}</a></li>
                                                <li><a class="user-email-action" href="#EmailUser"
                                                        data-uid="{{ $user->id }}" data-toggle="modal"><em
                                                            class="far fa-envelope"></em>{{ ___('Send Email') }}</a></li>
                                                @if ($user->role == 'user')
                                                    <li><a href="javascript:void(0)" data-uid="{{ $user->id }}"
                                                            data-type="transactions"
                                                            class="user-form-action user-action"><em
                                                                class="fas fa-random"></em>{{ ___('Transaction') }}</a>
                                                    </li>
                                                @endif
                                                <li><a href="javascript:void(0)" data-uid="{{ $user->id }}"
                                                        data-type="activities" class="user-form-action user-action"><em
                                                            class="fas fa-sign-out-alt"></em>{{ ___('Activities') }}</a>
                                                </li>
                                                <li><a href="javascript:void(0)" data-uid="{{ $user->id }}"
                                                        data-type="referrals" class="user-form-action user-action"><em
                                                            class="fas fa-users"></em>{{ ___('Referrals') }}</a></li>

                                                @if ($user->id != save_gmeta('site_super_admin')->value)
                                                    <li><a class="user-form-action user-action" href="#"
                                                            data-type="reset_pwd" data-uid="{{ $user->id }}"><em
                                                                class="fas fa-shield-alt"></em>{{ ___('Reset Pass') }}</a>
                                                    </li>
                                                @endif
                                                @if ($user->google2fa == 1)
                                                    <li><a class="user-form-action user-action" href="javascript:void(0)"
                                                            data-type="reset_2fa" data-uid="{{ $user->id }}"><em
                                                                class="fas fa-unlink"></em>{{ ___('Reset') }} 2FA</a>
                                                    </li>
                                                @endif
                                                @if ($user->email_verified_at === null)
                                                    <li><a class="user-form-action user-action" href="javascript:void(0)"
                                                            data-type="verify_email" data-uid="{{ $user->id }}"><em
                                                                class="fas fa-check"></em>{{ ___('Verify Email') }}</a>
                                                    </li>
                                                @endif
                                                @if (Auth::id() != $user->id && $user->id != save_gmeta('site_super_admin')->value)
                                                    @if ($user->status != 'suspend')
                                                        <li><a href="#" data-uid="{{ $user->id }}"
                                                                data-type="suspend_user" class="user-action front"><em
                                                                    class="fas fa-ban"></em>{{ ___('Suspend') }}</a></li>
                                                    @else
                                                        <li><a href="#" id="front"
                                                                data-uid="{{ $user->id }}" data-type="active_user"
                                                                class="user-action"><em
                                                                    class="fas fa-ban"></em>{{ ___('Active') }}</a></li>
                                                    @endif
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            {{-- .data-item --}}
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="bg-light text-center rounded pdt-5x pdb-5x">
                    <p><em
                            class="ti ti-server fs-24"></em><br>{{ $is_page == 'all' ? ___('No investor / user found!') : ___('No ') . $is_page . ___(' user here!') }}
                    </p>
                    <p><a class="btn btn-primary btn-auto"
                            href="{{ route('admin.users', 'user') }}">{{ ___('View All Users') }}</a>
                    </p>
                </div>
            @endif

            @if ($pagi->hasPages())
                <div class="pagination-bar">
                    <div class="d-flex flex-wrap justify-content-between guttar-vr-20px guttar-20px">
                        <div class="fake-class">
                            <ul class="btn-grp guttar-10px pagination-btn">
                                @if ($pagi->previousPageUrl())
                                    <li><a href="{{ $pagi->previousPageUrl() }}"
                                            class="btn ucap btn-auto btn-sm btn-light-alt">Prev</a></li>
                                @endif
                                @if ($pagi->nextPageUrl())
                                    <li><a href="{{ $pagi->nextPageUrl() }}"
                                            class="btn ucap btn-auto btn-sm btn-light-alt">Next</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="fake-class">
                            <div class="pagination-info guttar-10px justify-content-sm-end justify-content-mb-end">
                                <div class="pagination-info-text ucap">{{ ___('Page') }} </div>
                                <div class="input-wrap w-80px">
                                    <select class="select select-xs select-bordered goto-page"
                                        data-dd-class="search-{{ $pagi->lastPage() > 7 ? 'on' : 'off' }}">
                                        @for ($i = 1; $i <= $pagi->lastPage(); $i++)
                                            <option value="{{ $pagi->url($i) }}"
                                                {{ $pagi->currentPage() == $i ? ' selected' : '' }}>{{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="pagination-info-text ucap">{{ ___('of') }} {{ $pagi->lastPage() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        {{-- .card-innr --}}
    </div>{{-- .card --}}
    </div>{{-- .container --}}
    </div>{{-- .page-content --}}

@endsection

@section('modals')

    <div class="modal fade" id="addUser" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body popup-body-md">
                    <h3 class="popup-title">{{ ___('Add New User') }}</h3>
                    <form action="{{ route('admin.ajax.users.add') }}" method="POST"
                        class="adduser-form validate-modern" id="addUserForm" autocomplete="false">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('User Type') }}</label>
                                    <select name="role" class="select select-bordered select-block"
                                        required="required">
                                        <option value="user">
                                            {{ ___('Regular') }}
                                        </option>
                                        <option value="admin">
                                            {{ ___('Admin') }}
                                        </option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="input-item-label">{{ ___('Full Name') }}</label>
                            <div class="input-wrap">
                                <input name="name" class="input-bordered" minlength="3" required="required"
                                    type="text" placeholder="{{ ___('User Full Name') }}">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Email Address') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" required="required" name="email" type="email"
                                            placeholder="{{ ___('Email address') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Password') }}</label>
                                    <div class="input-wrap">
                                        <input name="password" class="input-bordered" minlength="6"
                                            placeholder="{{ ___('Automatically generated if blank') }}" type="password"
                                            autocomplete='new-password'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-item">
                            <input checked class="input-checkbox input-checkbox-sm" name="email_req" id="send-email"
                                type="checkbox">
                            <label for="send-email">{{ ___('Required Email Verification') }}
                            </label>
                        </div>
                        <div class="gaps-1x"></div>
                        <button class="btn btn-md btn-primary" type="submit">{{ ___('Add User') }}</button>
                    </form>
                </div>
            </div>
            {{-- .modal-content --}}
        </div>
        {{-- .modal-dialog --}}
    </div>

    <div class="modal fade" id="EmailUser" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body popup-body-md">
                    <h3 class="popup-title">{{ ___('Send Email to User') }} </h3>
                    <div class="msg-box"></div>
                    <form class="validate-modern" id="emailToUser" action="{{ route('admin.ajax.users.email') }}"
                        method="POST" autocomplete="off">
                        @csrf
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="input-item input-with-label">
                            <label class="clear input-item-label">{{ ___('Email Subject') }}</label>
                            <div class="input-wrap">
                                <input type="text" name="subject" class="input-bordered cls"
                                    placeholder="{{ ___('New Message') }}">
                            </div>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="clear input-item-label">{{ ___('Email Greeting') }}</label>
                            <div class="input-wrap">
                                <input type="text" name="greeting" class="input-bordered cls"
                                    placeholder="{{ ___('Hello User') }}">
                            </div>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="clear input-item-label">{{ ___('Your Message') }}</label>
                            <div class="input-wrap">
                                <textarea required="required" name="message" class="input-bordered cls input-textarea input-textarea-sm"
                                    type="text" placeholder="{{ ___('Write something') }}..."></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">{{ ___('Send Email') }}</button>
                    </form>
                </div>
            </div>{{-- .modal-content --}}
        </div>{{-- .modal-dialog --}}
    </div>

@endsection
@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"
        integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        @if (Session::has('message'))
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.success("{{ session('message') }}");
        @endif

        @if (Session::has('error'))
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.error("{{ session('error') }}");
        @endif

        @if (Session::has('info'))
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.info("{{ session('info') }}");
        @endif

        @if (Session::has('warning'))
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.warning("{{ session('warning') }}");
        @endif
    </script>
@endpush
