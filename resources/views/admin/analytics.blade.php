@extends('layouts.admin')
@section('title', ___('Admin Analytics'))
@section('content')
    <div class="page-content">
        <div class="container">
            @include('vendor.notice')
            @include('layouts.messages')
            <div class="row">
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-innr">
                            <h4 class="card-title">{{ ___('Países más populares') }}</h4>
                            <div data-simplebar="init" style="max-height: 550px;">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper"
                                                style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">
                                                    <ul class="list-unstyled activity-wid">
                                                        @php
                                                        $contador1 = 1;
                                                        @endphp
                                                        @foreach ($paises as $pais)
                                                            <li
                                                                class="list-group-item d-flex justify-content-between align-items-center">
                                                                <div class="col-2 p-0">
                                                                    <small> # {{ $contador1 }}</small>
                                                                </div>
                                                                <div class="col-2 p-0 text-center">
                                                                    <img loading="lazy" src="{{ $pais->flag }}"
                                                                        alt=""
                                                                        style="border-radius: 50%;width: 28px;height: 28px;">
                                                                </div>
                                                                <div class="col-5 p-0">
                                                                    <small id="nombre">
                                                                        {{ $pais->name }}
                                                                    </small>
                                                                </div>
                                                                <div class="col-3 text-end">
                                                                    <span
                                                                        id="{{ $pais->name }}">{{ $pais->users }}</span>
                                                                </div>
                                                            </li>
                                                            @php
                                                            $contador1++;
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-innr pb-0">
                            <h4 class="card-title">{{ ___('Países más populares') }}</h4>
                        </div>
                        <div class="card-innr pt-0" style="height: 541px">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ ___('Ciudades más populares') }}</h4>
                            <div data-simplebar="init" style="max-height: 565px;">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper"
                                                style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">


                                                    <div id="accordion" class="custom-accordion">
                                                        @php
                                                        $contador2 = 1;
                                                        @endphp
                                                        @foreach ($arrayRegionesPorCiudades as $key => $ciudad)
                                                            <div class="card mb-1 shadow-one">
                                                                <a href="#collapse{{ $key }}"
                                                                    class="text-dark collapsed" data-bs-toggle="collapse"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapse{{ $key }}">
                                                                    <div class="card-header list-group-item d-flex justify-content-between align-items-center"
                                                                        id="heading{{ $key }}">
                                                                        <div class="col-2 p-0">
                                                                            <small> # {{ $contador2 }}</small>
                                                                        </div>
                                                                        <div class="col-2 p-0 text-center">
                                                                            <img loading="lazy" src="{{ $ciudad['flag'] }}"
                                                                                alt=""
                                                                                style="border-radius: 50%;width: 28px;height: 28px;">
                                                                        </div>
                                                                        <div class="col-5 p-0">
                                                                            <small>
                                                                                {{ $ciudad['city'] }}
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-3 text-end">
                                                                            <span>{{ $ciudad['cantidad'] }}</span>
                                                                        </div>
                                                                    </div>
                                                                </a>

                                                                <div id="collapse{{ $key }}" class="collapse p-3"
                                                                    aria-labelledby="heading{{ $key }}"
                                                                    data-bs-parent="#accordion" style="">
                                                                    @foreach ($ciudad['region'] as $region => $usuarios)
                                                                        <div class="card-body p-0">
                                                                            <div class="row">
                                                                                <div class="col-6">
                                                                                    <small>{{ $region }}</small>
                                                                                </div>
                                                                                <div class="col-6">
                                                                                    <small>{{ $usuarios }}</small>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @php
                                                            $contador2++;
                                                            @endphp
                                                        @endforeach
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-innr pb-0">
                            <h4 class="card-title">{{ ___('Ciudades más populares') }}</h4>
                        </div>
                        <div class="card-innr pt-0" style="height: 541px">
                            <canvas id="myChartCiudad"></canvas>
                        </div>
                    </div>
                </div>


                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-innr">
                            <h4 class="card-title">{{ ___('Tipos de Usuarios') }}</h4>
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <th>{{ ___('Tipo Usuario') }}</th>
                                            <th>{{ ___('Cantidad') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $contador = 1;
                                        @endphp
                                        @foreach ($typeUsers as $user)
                                            <tr>
                                                <td>Tipo {{ $contador - 1 }}</td>
                                                <td id="Tipo{{ $contador - 1 }}">{{ $user['cantidad_type'] }}</td>
                                            </tr>
                                            @php
                                            $contador++;
                                            @endphp
                                        @endforeach
                                        <tr class="table-info">
                                            <td>{{ ___('TOTAL') }}:</td>
                                            <td id="total">{{ $total }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-innr pb-0">
                            <h4 class="card-title">{{ ___('Tipos de Usuarios') }}</h4>
                        </div>
                        <div class="card-innr pt-0" style="height: 512px">
                            <canvas id="myChart2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        const ctx = document.getElementById('myChart');
        let array = {!! json_encode($arrayNomPais) !!}
        let array2 = {!! json_encode($arrayUserPais) !!}
        Charts(array, array2)

        function Charts(array, array2) {
            Charts1 = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: array,
                    datasets: [{
                        label: '# of users',
                        data: array2,
                        borderWidth: 1,
                        borderColor: '#00000',
                        backgroundColor: '#51C1C0',
                    }]
                },
                options: {
                    indexAxis: 'y',
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
        Echo.channel('refresh-analytics').listen('.analytics.admin.event', (data) => {
            Charts1.destroy();
            Charts(data.array, data.array2);
            for (let i = 0; i < data.array.length; i++) {
                $('#' + data.array[i]).text(data.array2[i]);
            }
        });
    </script>

    <script>
        const ctx3 = document.getElementById('myChartCiudad');
        let arrayNomCiudad = {!! json_encode($arrayNomCiudad) !!}
        let arrayCantCiudad = {!! json_encode($arrayCantCiudad) !!}
        Chartsi(arrayNomCiudad, arrayCantCiudad)

        function Chartsi(arrayNomCiudad, arrayCantCiudad) {
            Charts3 = new Chart(ctx3, {
                type: 'bar',
                data: {
                    labels: arrayNomCiudad,
                    datasets: [{
                        label: '# of users',
                        data: arrayCantCiudad,
                        borderWidth: 1,
                        borderColor: '#00000',
                        backgroundColor: '#51C1C0',
                    }]
                },
                options: {
                    indexAxis: 'y',
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
        Echo.channel('refresh-ciudad').listen('.ciudades.populares', (data) => {
            Charts3.destroy();
            Chartsi(data.arrayNomCiudad, data.arrayCantCiudad);
            for (let i = 0; i < data.arrayNomCiudad.length; i++) {
                $('#' + data.arrayNomCiudad[i]).text(data.arrayCantCiudad[i]);
            }
        });
    </script>

    <script>
        const ctx2 = document.getElementById('myChart2');
        let array3 = {!! json_encode($tipos) !!}
        let array4 = {!! json_encode($cant_user_type) !!}
        Chartss(array3, array4)

        function Chartss(array3, array4) {
            Charts2 = new Chart(ctx2, {
                type: 'bar',
                data: {
                    labels: array3,
                    datasets: [{
                        label: '# of users',
                        data: array4,
                        borderWidth: 1,
                        borderColor: '#00000',
                        backgroundColor: '#51C1C0',
                    }]
                },
                options: {
                    indexAxis: 'y',
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
        Echo.channel('refresh-type').listen('.hola.event', (data) => {
            Charts2.destroy();
            Chartss(data.array3, data.array4)
            for (let j = 0; j < data.array3.length; j++) {
                $('#' + data.array3[j]).text(data.array4[j]);
            }
            suma = 0;
            data.array4.forEach(function(numero) {
                suma += numero;
            });
            $('#total').text(suma);
        });
    </script>
@endpush
