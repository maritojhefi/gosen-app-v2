@extends('layouts.auth')
@section('title', ___('Sign up'))

@push('header')
    <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
    <style>
        form-horizontal {
            background: white;
        }

        .container {
            margin-top: 10%;
        }

        .progress {
            height: 9px;
            width: 80%;

        }

        .control-label {
            text-align: left !important;
            padding-bottom: 7px;
        }

        .form-horizontal {
            padding: 25px 20px;
            border: 2px solid #e8eaed;
            border-radius: 5px;
        }

        .fa-times {
            color: red;
        }

        /*del cdn*/

        @-webkit-keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }

            to {
                background-position: 0 0;
            }
        }

        @-o-keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }

            to {
                background-position: 0 0;
            }
        }

        @keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }

            to {
                background-position: 0 0;
            }
        }

        .progress {
            height: 9px;
            margin-bottom: 0px;
            overflow: hidden;
            background-color: #f5f5f5;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        }

        .progress-bar {
            float: left;
            width: 0;
            height: 100%;
            font-size: 12px;
            line-height: 20px;
            color: #fff;
            text-align: center;
            background-color: #337ab7;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            -webkit-transition: width 0.6s ease;
            -o-transition: width 0.6s ease;
            transition: width 0.6s ease;
        }

        .progress-bar-striped,
        .progress-striped .progress-bar {
            background-image: -webkit-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: -o-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            -webkit-background-size: 40px 40px;
            background-size: 40px 40px;
        }

        .progress-bar.active,
        .progress.active .progress-bar {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
        }

        .progress-bar-success {
            background-color: #5cb85c;
        }

        .progress-striped .progress-bar-success {
            background-image: -webkit-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: -o-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
        }

        .progress-bar-info {
            background-color: #5bc0de;
        }

        .progress-striped .progress-bar-info {
            background-image: -webkit-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: -o-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
        }

        .progress-bar-warning {
            background-color: #f0ad4e;
        }

        .progress-striped .progress-bar-warning {
            background-image: -webkit-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: -o-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
        }

        .progress-bar-danger {
            background-color: #d9534f;
        }

        .progress-striped .progress-bar-danger {
            background-image: -webkit-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: -o-linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
            background-image: linear-gradient(45deg,
                    rgba(255, 255, 255, 0.15) 25%,
                    transparent 25%,
                    transparent 50%,
                    rgba(255, 255, 255, 0.15) 50%,
                    rgba(255, 255, 255, 0.15) 75%,
                    transparent 75%,
                    transparent);
        }
    </style>

    <style>
        .qs {
            background-color: #02bdda00;
            border-radius: 16px;
            color: #e3fbff;
            cursor: default;
            display: inline-block;
            font-family: &#39;
            Helvetica&#39;
            ,
            sans-serif;
            font-size: 18px;
            font-weight: bold;
            height: 20px;
            line-height: 30px;
            position: relative;
            text-align: center;
            width: 20px;
            position: absolute;
            top: -22px;
            transform: translateY(-50%);
            left: calc(100% - -323px);
        }

        .qs .popover {
            background-color: rgb(0 0 0 / 58%);
            border-radius: 5px;
            bottom: 34px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.4);
            color: #fff;
            font-size: 12px;
            font-family: &#39;
            Helvetica&#39;
            ,
            sans-serif;
            left: -179px;
            padding: 7px 10px;
            position: absolute;
            width: 200px;
            z-index: 4;
        }

        .qs .popover:before {
            border-top: 7px solid rgba(0, 0, 0, 0.85);
            border-right: 7px solid transparent;
            border-left: 7px solid transparent;
            bottom: -7px;
            content: &#39;
            &#39;
            ;
            display: block;
            left: 50%;
            margin-left: -7px;
            position: absolute;
        }

        .qs:hover .popover {
            display: block;
            -webkit-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
            -moz-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
            -ms-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
        }

        @-webkit-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-moz-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-ms-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-webkit-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @-moz-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @-ms-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @media (max-width: 575px) {
            #globo {
                left: calc(100% - -5px);
            }

        }
    </style>
@endpush

@section('content')

    @php
        $check_users = \App\Models\User::count();
        
    @endphp
    @if (recaptcha())
        @push('header')
            <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ recaptcha('site') }}', {
                        action: 'register'
                    }).then(function(token) {
                        if (token) {
                            document.getElementById('recaptcha').value = token;
                        }
                    });
                });
            </script>
        @endpush
    @endif
    <div class="page-ath-form pt-0 mt-5">
        <h2 class="page-ath-heading text-secondary"
            style="{{ isset(tenancy()->tenant) == true ? 'color: #280f53;' : 'color: #37363c;' }} margin-bottom: -10px; font-size: 30px;">
            {{ ___('Sign up') }} <small id="textoLogin" class="text-secondary"
                style="{{ isset(tenancy()->tenant) == true ? 'color: #37363c' : 'color: #37363c;' }}">{{ ___('New Account') }}
            </small></h2>
        @livewire('register-form-component', ['check_users' => $check_users])
        <input type="hidden" name="" id="url" value="{{ asset2() }}">

        @if (application_installed(true) && $check_users > 0 && Schema::hasTable('settings'))
            @if (
                (get_setting('site_api_fb_id', env('FB_CLIENT_ID', '')) != '' &&
                    get_setting('site_api_fb_secret', env('FB_CLIENT_SECRET', '')) != '') ||
                    (get_setting('site_api_google_id', env('GOOGLE_CLIENT_ID', '')) != '' &&
                        get_setting('site_api_google_secret', env('GOOGLE_CLIENT_SECRET', '')) != ''))
                <div class="sap-text"><span>{{ ___('Or Sign up with') }}</span></div>
                <ul class="row guttar-20px guttar-vr-20px">
                    <li class="col"><a href="{{ route('social.login', 'facebook') }}"
                            class="btn  btn-dark btn-facebook btn-block"><em
                                class="fab fa-facebook-f"></em><span>{{ ___('Facebook') }}</span></a></li>
                    <li class="col"><a href="{{ route('social.login', 'google') }}"
                            class="btn  btn-dark btn-google btn-block"><em
                                class="fab fa-google"></em><span>{{ ___('Google') }}</span></a></li>
                </ul>
            @endif
            <div class="gaps-4x"></div>
            <div class="form-note text-center">
                {{ ___('Already have an account ?') }} <a href="{{ route('register.login') }}">
                    <strong id="acc">{{ ___('Sign in instead') }}</strong></a>
            </div>
        @endif
    </div>


@endsection

@push('footer')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <script>
        $(document).ready(function() {
            $('#password').keyup(function() {
                var password = $('#password').val();
                if (password == 0) {
                    $('#pov').addClass('hide');
                    $('#pov').removeClass('show')
                    $('#popover-password').addClass('hide');
                } else {
                    $('#popover-password').removeClass('hide');
                    $('#pov').addClass('show');
                    $('#pov').removeClass('hide');
                    $('#pop').removeClass('d-none');
                }
                if (checkStrength(password) == false) {
                    $('#pop').addClass('d-none');
                } else if (checkStrength(password) == 'Strong') {
                    $('#pop').addClass('d-none');
                    $('#popover-password').addClass('d-none');
                } else {
                    $('#pop').removeClass('d-none');
                    $('#popover-password').removeClass('d-none');
                }
                if ($('#password').val() !== $('#password_confirmation').val()) {
                    $('#password').removeClass('is-valid');
                    $('#password_confirmation').removeClass('is-valid');
                    $("#show_hide_password i").css("color", "#191919");
                } else if (checkStrength(password) == 'Strong') {
                    $('#password').addClass('is-valid');
                    $('#password_confirmation').addClass('is-valid');
                    $("#show_hide_password i").css("color", "#1cbb8c");
                }
            });



            $('#password_confirmation').on('keyup', function() {
                if ($('#password').val() === $('#password_confirmation').val()) {
                    $('#popover-cpassword').addClass('hide');
                } else {
                    $('#popover-cpassword').removeClass('hide');
                }
            });
            $('#password').on('keyup', function() {
                if ($('#password').val() === $('#password_confirmation').val()) {
                    $('#popover-cpassword').addClass('hide');
                } else {
                    $('#popover-cpassword').removeClass('hide');
                }
            });



            $('#password_confirmation').keyup(function() {
                var password = $('#password').val();
                if ($('#password').val() !== $('#password_confirmation').val()) {
                    $('#password').removeClass('is-valid');
                    $('#password_confirmation').removeClass('is-valid');
                    $("#show_hide_password i").css("color", "#191919");
                } else if (checkStrength(password) == 'Strong') {
                    $('#password').addClass('is-valid');
                    $('#password_confirmation').addClass('is-valid');
                    $("#show_hide_password i").css("color", "#1cbb8c");
                }
            });

            function checkStrength(password) {
                var strength = 0;
                //If password contains both lower and uppercase characters, increase strength value.
                if (password.match(/([A-Z].*)|(.*[A-Z])/)) {
                    strength += 1;
                    $('.low-upper-case').addClass('text-success');
                    $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                    $('#uppercase').addClass('d-none')
                    $('#popover-password-top').addClass('hide');
                } else {

                    $('.low-upper-case').removeClass('text-success');
                    $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                    $('#uppercase').removeClass('d-none');
                    $('#popover-password-top').removeClass('hide');
                }
                if (password.match(/([a-z].*)|(.*[a-z])/)) {
                    strength += 1;

                    $('.lower-case').addClass('text-success');
                    $('.lower-case i').removeClass('fa-times').addClass('fa-check');
                    $('#lowercase').addClass('d-none')
                    $('#popover-password-top').addClass('hide');
                } else {

                    $('.lower-case').removeClass('text-success');
                    $('.lower-case i').addClass('fa-times').removeClass('fa-check');
                    $('#lowercase').removeClass('d-none');
                    $('#popover-password-top').removeClass('hide');
                }
                //If it has numbers and characters, increase strength value.
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                    strength += 1;

                    $('.one-number').addClass('text-success');
                    $('.one-number i').removeClass('fa-times').addClass('fa-check');
                    $('#number').addClass('d-none')
                    $('#popover-password-top').addClass('hide');
                } else {

                    $('.one-number').removeClass('text-success');
                    $('.one-number i').addClass('fa-times').removeClass('fa-check');
                    $('#number').removeClass('d-none');
                    $('#popover-password-top').removeClass('hide');
                }
                //If it has one special character, increase strength value.
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                    strength += 1;

                    $('.one-special-char').addClass('text-success');
                    $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                    $('#special').addClass('d-none')
                    $('#popover-password-top').addClass('hide');
                } else {

                    $('.one-special-char').removeClass('text-success');
                    $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                    $('#special').removeClass('d-none')
                    $('#popover-password-top').removeClass('hide');
                }
                if (password.length > 7) {
                    strength += 1;

                    $('.eight-character').addClass('text-success');
                    $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');
                    $('#character').addClass('d-none')
                } else {

                    $('.eight-character').removeClass('text-success');
                    $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                    $('#character').removeClass('d-none')
                }
                // If value is less than 2
                if (strength < 3) {

                    $('#uls').removeClass('d-none')
                    $('#result').removeClass()
                    $('#password-strength').addClass('progress-bar-danger');
                    $('#result').addClass('text-danger').text('{{ ___('Very Week') }}');
                    $('#password-strength').css('width', '10%');
                } else if (strength == 4) {

                    $('#result').addClass('good');
                    $('#password-strength').removeClass('progress-bar-danger');
                    $('#password-strength').addClass('progress-bar-warning');
                    $('#result').addClass('text-warning').text('{{ ___('Week') }}')
                    $('#password-strength').css('width', '60%');
                    return 'Week'
                } else if (strength == 5) {
                    $('#result').removeClass()
                    $('#result').addClass('strong');
                    $('#password-strength').removeClass('progress-bar-warning');
                    $('#password-strength').addClass('progress-bar-success');
                    $('#result').addClass('text-success').text('{{ ___('Strength') }}');
                    $('#password-strength').css('width', '100%');
                    return 'Strong'
                }
            }
        });

        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
        $("#password")
            .focusout(function() {
                $('#pop').addClass('d-none');
            })
    </script>
@endpush
