@extends('layouts.auth')
@push('header')
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap');


        :root {
            --background: #d74e68;
            --left-side: #ff9568;
            --smile: #ffe073;
            --star: #d74e68;
            --arrow: cfcfcf;
        }



        .container {
            background-color: #fff;
            width: 70%;
            height: 120px;
            position: relative;
            border-radius: 6px;
            transition: 0.3s ease-in-out;
        }

        .container {
            transform: scale(1.04);
        }

        .left-side {
            background: linear-gradient(90deg, rgb(75 98 136) 0%, #35496a 100%);
            width: 130px;
            height: 120px;
            border-radius: 6px;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: pointer;
            transition: 0.3s;
            overflow: hidden;
        }

        .right-side {
            width: calc(100% - 130px);
            display: flex;
            align-items: center;
            overflow: hidden;
            cursor: pointer;
            justify-content: space-between;
            white-space: nowrap;
            transition: 0.3s;
            border-radius: 6px;
        }

        .right-side {
            background-color: #f9f7f9;
        }

        .arrow {
            width: 20px;
            height: 20px;
            margin-right: 20px;
        }

        .refer {
            font-size: 23px;
            font-family: &#34;
            Lexend Deca&#34;
            ,
            sans-serif;
            margin-left: 20px;
        }

        .smile {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            background-color: var(--smile);
            position: absolute;
            display: flex;
            align-items: center;
            -webkit-box-shadow: 2px 5px 10px -2px rgba(0, 0, 0, .2);
        }

        .eyes {
            width: 6px;
            height: 10px;
            position: absolute;
            box-shadow: -15px 0 0 0 #000, 15px 0 0 0 #000;
            border-radius: 50%;
            left: 27px;
            top: 20px;
        }

        .eyes2 {
            width: 6px;
            height: 10px;
            position: absolute;
            box-shadow: -15px 0 0 0 #000, 15px 0 0 0 #000;
            border-radius: 50%;
            left: 20px;
            top: 17px;
        }

        .mouth {
            width: 36px;
            height: 18px;
            background-color: #000;
            position: absolute;
            border-radius: 0 0 60px 60px;
            top: 35px;
            left: 12px;
        }

        .container .smile {
            animation: slide-cross 0.6s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
        }

        .container .eyes,
        .container .eyes2,
        .container .mouth {
            animation: slide-bottom 0.6s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
        }

        .container .talk {
            animation: scale-up 0.4s cubic-bezier(0.39, 0.575, 0.565, 1) 0.3s both;
        }

        .container .star {
            animation: turning 0.5s ease-in-out 0.5s both;
        }

        .container .friend {
            animation: slide-in 0.7s cubic-bezier(0.23, 1, 0.32, 1) both;
        }

        .container .shock {
            animation: wow 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.4s both;
        }

        @keyframes wow {
            0% {
                transform: scale(0.9);
            }

            100% {
                transform: scale(1.2);
            }
        }

        @keyframes slide-in {
            100% {
                right: -45px;
                top: 35px;
            }
        }

        @keyframes turning {
            0% {
                transform: rotate(-160deg) scale(0);
                opacity: 0;
            }

            85% {
                transform: rotate(0) scale(1.5);
                opacity: 1;
            }

            100% {
                transform: rotate(0) scale(1);
                opacity: 1;
            }
        }

        @keyframes scale-up {
            0% {
                transform: scale(0.6);
                transform-origin: 100% 100%;
            }

            100% {
                transform: scale(1);
                transform-origin: 100% 100%;
                opacity: 1;
            }
        }

        @keyframes slide-cross {
            0% {
                transform: translateY(0) translateX(0);
            }

            100% {
                transform: translateY(-38px) translateX(-41px);
            }
        }

        @keyframes slide-bottom {
            0% {
                transform: translateY(0) translateX(0);
            }

            100% {
                transform: translateY(1px) translateX(5px);
            }
        }

        .talk {
            width: 56px;
            height: 40px;
            border-radius: 10px;
            background-color: #fff;
            position: absolute;
            z-index: 10;
            bottom: 34px;
            left: 33px;
            opacity: 0;
        }

        .triangle {
            position: absolute;
            background-color: #fff;
            top: -4px;
            left: 8px;
        }

        .triangle:before,
        .triangle:after {
            content: &#39;
            &#39;
            ;
            position: absolute;
            background-color: inherit;
        }

        .triangle,
        .triangle:before,
        .triangle:after {
            width: 12px;
            height: 12px;
            border-top-right-radius: 35%;
        }

        .triangle {
            transform: rotate(-85deg) skewX(-30deg) scale(1, 0.866);
        }

        .triangle:before {
            transform: rotate(135deg) skewY(-45deg) scale(0.707, 1.414) translate(50%);
        }

        .star {
            width: 18px;
            height: 18px;
            position: absolute;
            left: 20px;
            top: 10px;
        }

        .friend {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            background-color: var(--smile);
            position: relative;
            right: -85px;
            top: 85px;
            z-index: 15;
            box-shadow: 2px 5px 10px -2px rgba(0, 0, 0, .2);
        }

        .friend:before {
            content: &#39;
            &#39;
            ;
            width: 6px;
            height: 10px;
            position: absolute;
            box-shadow: -15px 0 0 0 #000, 15px 0 0 0 #000;
            border-radius: 50%;
            left: 24px;
            top: 18px;
        }

        .shock {
            width: 12px;
            height: 12px;
            background-color: rgba(0, 0, 0, 0.932);
            position: absolute;
            border-radius: 50%;
            top: 34px;
            right: 28px;
            will-change: transform;
        }

        /* @media only screen and (max-width: 480px) {
                .container {
                    transform: scale(0.7);
                }

                .container {
                    transform: scale(0.74);
                }

                .refer {
                    font-size: 18px;
                }
            } */
        @media (max-width : 490px) {
            .container {
                left: -20px;
                width: calc(100% - 80px);

                .left-side {
                    width: calc(70% - 80px);
                }

                .right-side {
                    width: calc(80% - 80px);
                }

                .refer {
                    font-size: 15px;
                    margin-left: 15px;
                }

                .img {
                    width: calc(100% - 70px);
                }
            }

        }
    </style>
    <style>

    </style>
@endpush
@section('content')
    <div class="container" style="top: -135px; left:auto;">
        <div class="row">
            <div class="left-side">
                <div class="smile">
                    <div class="eyes"></div>
                    <div class="mouth"></div>
                </div>
                <div class="talk">
                    <div class="triangle"></div>
                    <svg class="star" xmlns="http://www.w3.org/2000/svg" height="512" viewBox="0 -10 511.991 511"
                        width="512">
                        <path
                            d="M510.652 185.883a27.177 27.177 0 00-23.402-18.688l-147.797-13.418-58.41-136.75C276.73 6.98 266.918.497 255.996.497s-20.738 6.483-25.023 16.53l-58.41 136.75-147.82 13.418c-10.837 1-20.013 8.34-23.403 18.688a27.25 27.25 0 007.937 28.926L121 312.773 88.059 457.86c-2.41 10.668 1.73 21.7 10.582 28.098a27.087 27.087 0 0015.957 5.184 27.14 27.14 0 0013.953-3.86l127.445-76.203 127.422 76.203a27.197 27.197 0 0029.934-1.324c8.851-6.398 12.992-17.43 10.582-28.098l-32.942-145.086 111.723-97.964a27.246 27.246 0 007.937-28.926zM258.45 409.605"
                            data-original="#000000" class="active-path" data-old_color="#000000" fill="#D74E68" />
                    </svg>
                </div>
                <div class="friend">
                    <div class="eyes2"></div>
                    <div class="shock"></div>
                </div>
            </div>
            <div class="right-side">
                <div class="refer">
                    <h3>
                        Referral
                    </h3>
                </div>
            </div>
        </div>
        <div class="row mt-1">
            <div class="card border-info">
                <div class="card-body">
                    <h3 >
                        Hi, friend!
                    </h3>
                    <h5 >To have your own account someone must invite you</h5>
                </div>
            </div>
        </div>
        <a href=" {{ route('login') }}" type="button" class="btn btn-info" style="top: -25px">Back to Login</a>
    </div>
@endsection
