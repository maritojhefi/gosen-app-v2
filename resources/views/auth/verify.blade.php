@extends('layouts.auth')
@section('title', 'Verify Email')
@section('content')

<div class="{{ (gws('theme_auth_layout','default')=='center-dark'||gws('theme_auth_layout', 'default')=='center-light') ? 'page-ath-form' : 'page-ath-text' }}">

    @if (session('resent'))
    <div class="alert alert-success" role="alert">
        {{ ___('A fresh verification link has been sent to your email address.') }}
    </div>
    @endif

    <div class="alert alert-warning text-center">{{ ___('Please verify your email address.') }}</div>
    @include('layouts.messages')
    <div class="gaps-0-5x"></div>
    {{ ___('Before proceeding, please check your email for a verification link.') }}
    {{ ___('If you did not receive the email, click the button to resend.') }} 
    <div class="gaps-3x"></div>
    <a class="btn btn-primary" style="{{ isset(tenancy()->tenant) == true ? 'background-color: #280f53!important;' : '' }}" href="{{ route('verify.resend') }}">{{ ___('Resend Email') }}</a>
    <div class="gaps-1-5x"></div>
    <a class="link link-ucap btn btn-outline" href="{{ route('log-out') }}">{{ ___('Sign Out') }}</a>
</div>
@endsection