@extends('layouts.auth')
@section('title', ___('Welcome'))
@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css"
        integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"
        integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endpush
@section('content')
    <div
        class="{{ gws('theme_auth_layout', 'default') == 'center-dark' || gws('theme_auth_layout', 'default') == 'center-light' ? 'page-ath-form' : 'page-ath-text' }}">
        <h2 class="page-ath-heading">{!! $text !!}
            @isset($subtext)
                <small>{!! $subtext !!}</small>
            @endisset
        </h2>
        @if (session('warning'))
            <div class="alert alert-dismissible fade show alert-warning" role="alert">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&nbsp;</a>
                {!! session('warning') !!}
            </div>
        @endif
        <div class="gaps-4x"></div>
        @isset($hideButton)
        @else
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 mb-4">
                    <a class="btn btn-primary" href="{{ route('login') }}" style="background-color: #280f53!important;">{{ ___('Sign in') }}</a>
                </div>
                @if (session()->has('notifiable'))
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6" id="contenedor">
                        <div class="note note-plane note-info pdb-2x" id="contador">
                            <em class="fas fa-info-circle"></em>
                            <p>{{ ___('Email sent, check your inbox.') }}
                                <small href="#" id="timer" style="font-size: 12px;">{{ ___('Remaining time to resend') }}: &nbsp;<small id="demo"
                                    class="" style="font-size: 15px;"></small>
                            </p>
                            </small>
                            <input type="hidden" id="timervalue" value="{{ session('created') }}">
                            <input type="hidden" name="" id="url" value="{{ asset2() }}">
                            
                        </div>
                        <form action="{{ route('resend.verify.mail') }}" method="POST">
                            @csrf
                            <div class="col d-none" type="submit" id="btnResendMail">
                                <button class="btn btn-dark" style="margin-left: -16px;" type="submit">{{ ___('Resend Confirmation') }}</button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        @endisset
    </div>
@endsection
@push('footer')
    <script>
        var x = setInterval(function() {
            var deadline = document.getElementById("timervalue").value;
            var countDownDate = new Date(deadline).getTime();
            var now = new Date().getTime();
            var t = countDownDate - now;
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * 60)) / 1000);
            document.getElementById("demo").innerHTML =
                minutes + "m " + seconds + "s ";
            if (t < 0) {
                $('#btnResendMail').removeClass('d-none');
                $('#timer').addClass('d-none');
                $('#contador').slideUp();
            }
        });


        $(document).ready(function() {
            $("#btnResendMail").click(function() {

                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {{___('Loading')}}...`
                );
                var ruta = $('#url').val();
                $.ajax({
                        type: "get",
                        url: ruta + 'resend/mail',
                    })
                    .done(function(approved) {
                        location.reload();
                    });
            });
        });
    </script>
@endpush
