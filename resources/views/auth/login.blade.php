@extends('layouts.auth')
@section('title', ___('Sign-in'))
@section('content')
    @if (recaptcha())
        @push('header')
            <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ recaptcha('site') }}', {
                        action: 'login'
                    }).then(function(token) {
                        if (token) {
                            document.getElementById('recaptcha').value = token;
                        }
                    });
                });
            </script>
        @endpush
    @endif

    @push('footer')
        <script>
            function isEmail(correo) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(correo);
            }
            $("#mail")
                .focusout(function() {

                        if (isEmail($(this).val())) {
                            $.ajax({
                                    method: "GET",
                                    @env('local')
                                        url: "/tokenlite/revisar/correo/" + $(this).val()
                                    @endenv
                                    @production
                                    url: "/revisar/correo/" + $(this).val()
                                @endproduction
                            })
                        .done(function(approved) {
                            if (approved.includes("existe")) {
                                $('#existe').removeClass('d-none');
                                $('#disponible').addClass('d-none');

                            } else {
                                $('#disponible').removeClass('d-none');
                                $('#existe').addClass('d-none');

                            }
                        });
                    }

                });
        </script>
    @endpush

        @push('header')
            <style>
                .border-semi-redondeado {
                    border-radius: 12px;
                }
            </style>
        @endpush
 
    <div class="page-ath-form pt-0 mt-4">
        {{-- {{dd($random)}} --}}
        {{-- <h2 class="page-ath-heading" style="color: #37363c">{{ ___('Sign in') }}<small id="textoLogin" style="color: #16a085;">{{ ___('with your') }} {{ site_info('name') }} --}}
        <h2 class="page-ath-heading text-secondary" style="font-size: 30px;">{{ ___('Sign in') }}<small id="textoLogin" class="text-secondary">
            {{ ___('Enter your data') }} </small></h2>
        <form class="login-form validate validate-modern"
            action="{{ is_maintenance() ? route('admin.login') : route('login') }}" method="POST">
            @csrf
            @include('layouts.messages')
            <div class="input-item">
                <input type="email" placeholder="{{ ___('Your Email') }}" data-msg-required="{{ ___('Required.') }}"
                    class="input-bordered{{ $errors->has('email') ? ' input-error' : '' }} border-semi-redondeado" name="email"
                    value="{{ old('email') }}" id="mail" required autofocus>
                <small id="disponible" class="text-danger d-none">{{___('This email is not registered')}} <i
                        class="fa fa-exclamation"></i></small>
                <small id="existe" class="text-success d-none">{{___('Registered mail')}} <i class="fa fa-check"></i></small>
                
            </div>
            <div class="input-item" id="show_hide_password">
                <input type="password" placeholder="{{ ___('Password') }}" id="password"
                    class="input-bordered{{ $errors->has('password') ? ' input-error' : '' }} border-semi-redondeado" name="password" required>
                    <a href="#">
                        <i class="fa fa-eye-slash"
                            style="
                color: #191919;
                position: absolute;
                width: 20px;
                height: 20px;
                right:10px;
                left: auto;
                top:24px;
                transform: translateY(-50%);">
                        </i>
                    </a>
                </div>
            @if (!is_maintenance())
                <div class="d-flex justify-content-between align-items-center">
                    <div class="input-item text-left">
                        <input class="input-checkbox input-checkbox-md" type="checkbox" name="remember" id="remember-me"
                            {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember-me">{{ ___('Remember Me') }}</label>
                    </div>
                    <div>
                        <a id="acc" href="{{ route('password.request') }}">{{ ___('Forgot password?') }}</a>
                        <div class="gaps-2x"></div>
                    </div>
                </div>
            @endif
            @if (recaptcha())
                <input type="hidden" name="recaptcha" id="recaptcha">
            @endif
            <div class="d-grid gap-2 col-6 mx-auto">
                <button type="submit" id="btnAuth" class="btn btn-primary btn-block" style="background-color: #280f53!important;">{{ ___('Sign In') }}</button>
            </div>
        </form>
        @if (!is_maintenance())
            @if (Schema::hasTable('settings'))
                @if ((get_setting('site_api_fb_id', env('FB_CLIENT_ID', '')) != '' && get_setting('site_api_fb_secret', env('FB_CLIENT_SECRET', '')) != '') || (get_setting('site_api_google_id', env('GOOGLE_CLIENT_ID', '')) != '' && get_setting('site_api_google_secret', env('GOOGLE_CLIENT_SECRET', '')) != ''))
                    <div class="sap-text"><span>{{ ___('Or Sign in with') }}</span></div>
                    <ul class="row guttar-20px guttar-vr-20px">
                        @if (get_setting('site_api_fb_id', env('FB_CLIENT_ID', '')) != '' && get_setting('site_api_fb_secret', env('FB_CLIENT_SECRET', '')) != '')
                            <li class="col"><a href="{{ route('social.login', 'facebook') }}"
                                    class="btn  btn-dark btn-facebook btn-block"><em
                                        class="fab fa-facebook-f"></em><span>{{ ___('Facebook') }}</span></a></li>
                        @endif
                        @if (get_setting('site_api_google_id', env('GOOGLE_CLIENT_ID', '')) != '' && get_setting('site_api_google_secret', env('GOOGLE_CLIENT_SECRET', '')) != '')
                            <li class="col"><a href="{{ route('social.login', 'google') }}"
                                    class="btn  btn-dark btn-google btn-block"><em
                                        class="fab fa-google"></em><span>{{ ___('Google') }}</span></a></li>
                        @endif
                    </ul>
                @endif
            @endif

            <div class="gaps-4x"></div>
            <div class="form-note">
                {{-- {{___('Don’t have an')}} <a id="acc1" class="" href=" {{ route('user.nreferral') }} "> <strong style="font-weight:bold;">{{___('account?')}}</strong></a> --}}
            </div>
        @endif
    </div>
@endsection
@push('footer')
<script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });
    });
</script>
@endpush