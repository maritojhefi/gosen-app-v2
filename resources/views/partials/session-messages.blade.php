@if (session('error'))
<div class="alert alert-danger-alt alert-dismissible fade show">{{ session('error') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span></button>
</div>
@endif

@if (session('success'))
<div class="alert alert-success-alt alert-dismissible fade show"> {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span></button>
</div>
@endif