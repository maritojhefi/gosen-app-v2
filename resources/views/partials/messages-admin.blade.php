@if (Session::has('success'))
    @push('footer')
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'success',
                    title: '{{ Session::get('success') }}',
                    position: 'bottom-end',
                    background: '#585858e3',
                    color: 'white',
                    toast: true,
                })
            });
        </script>
    @endpush
@endif
@if (Session::has('warning'))
    @push('footer')
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'warning',
                    title: '{{ Session::get('warning') }}',
                    position: 'bottom-end',
                    background: '#585858e3',
                    color: 'white',
                    toast: true,
                })
            });
        </script>
    @endpush
@endif
@if (Session::has('info'))
    @push('footer')
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'info',
                    title: '{{ Session::get('info') }}',
                    position: 'bottom-end',
                    background: '#585858e3',
                    color: 'white',
                    toast: true,
                })
            });
        </script>
    @endpush
@endif
@if (Session::has('error'))
    @push('footer')
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'error',
                    title: '{{ Session::get('error') }}',
                    position: 'bottom-end',
                    background: '#585858e3',
                    color: 'white',
                    toast: true,
                })
            });
        </script>
    @endpush
@endif
