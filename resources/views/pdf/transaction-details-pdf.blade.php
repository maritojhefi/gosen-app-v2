<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaction Details</title>
    <style>
        /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
        * {
            margin: 0;
            padding: 0;
            font-family: "Roboto", sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }


        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        /* -------------------------------------
    BODY & CONTAINER
------------------------------------- */

        .body-wrap {
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
        .main {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 20px;
        }

        .content-wrap {
            padding: 15px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .logo{
            margin-top: -50px;
            text-align: center;
        }

        /* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
        h1,
        h2,
        h3 {
            font-family: "Roboto", sans-serif;
            color: #000;
            margin: 40px 0 0;
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p,
        ul,
        ol {
            margin-bottom: 10px;
            font-weight: normal;
        }

        p li,
        ul li,
        ol li {
            margin-left: 5px;
            list-style-position: inside;
        }



        /* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .aligncenter {
            text-align: center;
        }

        .alignright {
            text-align: right;
        }

        .alignleft {
            text-align: left;
        }

        .clear {
            clear: both;
        }


        /* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
        .invoice {
            margin: 40px auto;
            text-align: left;
            width: 80%;
            border: #eee 1px solid;
        }

        .invoice td {
            padding: 15px 20px;
        }

        .invoice .invoice-items {
            width: 100%;
        }

        .invoice .invoice-items td {
            /* border-right: #eee 1px solid; */
        }
        .bg{
            background-color: rgba(95, 158, 160, 0.150);
            border-right: #eee 1px solid;
        }

        /* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
        @media only screen and (max-width: 640px) {

            h1,
            h2,
            h3,
            h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content,
            .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }
    </style>

</head>

<body>

    <table class="body-wrap">
        <tbody>
            <tr>
                <td></td>
                <td class="container" width="600">
                    <div class="content">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td class="content-wrap aligncenter">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td class="header aligncenter">
                                                        <h2>Transaction Details</h2>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        <table class="invoice">
                                                            <tbody>
                                                                <tr>
                                                                    <td>TOKEN DETAILS</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="invoice-items" cellpadding="0"
                                                                            cellspacing="0">
                                                                            <tbody>
                                                                                <tr class="bg">
                                                                                    <td>User</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->tnxUser->username }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Aproval date</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->tnx_time }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="bg">
                                                                                    <td>Transactions</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->tnx_id }}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Types</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->tnx_type }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="bg">
                                                                                    <td>Token of Stage</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->ico_stage->name }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Token Amount (T)</td>
                                                                                    <td class="alignright">
                                                                                        {{ $transaction->tokens }}<span>
                                                                                            {{ token_symbol() }}</span>
                                                                                    </td>
                                                                                </tr>
                                                                                @if ($transaction->tnx_type == 'purchase')
                                                                                    <tr class="bg">
                                                                                        <td>Bonus Token (B)</td>
                                                                                        <td class="alignright">
                                                                                            <span>{{ $transaction->total_bonus }}
                                                                                                {{ token_symbol() }}</span>
                                                                                            <span>({{ $transaction->bonus_on_token }}
                                                                                                +
                                                                                                {{ $transaction->bonus_on_base }})</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Total Token</td>
                                                                                        <td class="alignright">
                                                                                            <span><strong>{{ $transaction->total_tokens }}
                                                                                                    {{ token_symbol() }}</strong></span>
                                                                                            <span>(T+B)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="bg">
                                                                                        <td>Total Payment</td>
                                                                                        <td class="alignright">
                                                                                            <span><strong>{{ $transaction->receive_amount }}
                                                                                                    {{ strtoupper($transaction->receive_currency) }}</strong></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Details</td>
                                                                                        <td class="alignright">
                                                                                            {{ $transaction->details }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @elseif ($transaction->tnx_type == 'joined')
                                                                                    <tr class="bg">
                                                                                        <td>Details</td>
                                                                                        <td class="alignright">
                                                                                            {{ $transaction->details }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        <div class="logo">
                                                            <img loading="lazy" src="{{ asset('images/token-symbol.png') }}" alt="">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>

</body>

</html>