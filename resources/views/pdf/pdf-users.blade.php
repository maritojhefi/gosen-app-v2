<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link preload rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
    <table class="table table-striped">
        <thead>
            <tr>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Register Method</th>
                <th scope="col">Social id</th>
                <th scope="col">Mobile</th>
                <th scope="col">Date of Birth</th>
                <th scope="col">Nationality</th>
                <th scope="col">Wallet Type</th>
                <th scope="col">Role</th>
                <th scope="col">Contributed</th>
                <th scope="col">Token Balance</th>
                <th scope="col">Referral Info</th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach ($user as $item)
            <tr class="active">
                <th scope="row">{{ $item->id }}</th>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->status }}</td>
                <td>{{ $item->registerMethod }}</td>
                <td>{{ $item->social_id }}</td>
                <td>{{ $item->mobile }}</td>
                <td>{{ $item->dateOfBirth }}</td>
                <td>{{ $item->nationality }}</td>
                <td>{{ $item->walletType }}</td>
                <td>{{ $item->role }}</td>
                <td>{{ $item->contributed }}</td>
                <td>{{ $item->tokenBalance }}</td>
                @if(isset($item->referralInfo))
                @php
                $referido=json_decode($item->referralInfo)
                @endphp
                <td>{{$referido->name}}</td>
                @else
                <td>Sin info</td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>