<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ ___('Transacciónes PDF') . ' ' . date('Y-m-d') }}</title>
    <link preload rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">TNX ID</th>
                <th scope="col">TNX TYPE</th>
                <th scope="col">TNX TIME</th>
                <th scope="col">TOKENS</th>
                <th scope="col">BONUS ON BASE</th>
                <th scope="col">BONUS ON TOKENS</th>
                <th scope="col">TOTAL BONUS</th>
                <th scope="col">TOTAL TOKENS</th>
                <th scope="col">USER</th>
                <th scope="col">AMOUNT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transaction as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->tnx_id }}</td>
                    <td>{{ $data->tnx_type }}</td>
                    <td>{{ $data->tnx_time }}</td>
                    <td>{{ $data->tokens }}</td>
                    <td>{{ $data->bonus_on_base }}</td>
                    <td>{{ $data->bonus_on_token }}</td>
                    <td>{{ $data->total_bonus }}</td>
                    <td>{{ $data->total_tokens }}</td>
                    <td>{{ $data->tnxUser != null ? $data->tnxUser->name : 'N/A' }}</td>
                    <td>{{ $data->amount }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
