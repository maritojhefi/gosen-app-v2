<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        GOSEN {{ date('d/m/y') }}
    </title>
    <link preload rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>

    @if ($tipo == 1)
        <x-table-component title="Transaction(GOS) Token" :variable="$trnxs" :campos="[
            'tnx_id' => 'Transaction ID',
            'tnx_type' => 'Type',
            'tnx_time' => 'Date',
            'tokens' => 'Tokens',
            'total_bonus' => 'Bonus Total',
            'total_tokens' => 'Total Tokens',
            'stage' => 'Stage',
            'name' => 'User',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'receive_amount' => 'Receive Amount',
            'payment_method' => 'Payment Method',
            'status' => 'Status',
        ]" :contador="$contadorBalanceGosToken"
            :totaltokens="$totalTokensBalanceGosTokens" :totalusdamount="$totalUsdAmountBalanceGosToken" />
    @elseif($tipo == 2)
        <x-table-component title="Token(GOS) " :variable="$trnxPurBonus" :campos="[
            'tnx_id' => 'Transaction ID',
            'tnx_type' => 'Type',
            'tnx_time' => 'Date',
            'tokens' => 'Tokens',
            'total_bonus' => 'Bonus Total',
            'total_tokens' => 'Total Tokens',
            'stage' => 'Stage',
            'name' => 'User',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'receive_amount' => 'Receive Amount',
            'payment_method' => 'Payment Method',
            'status' => 'Status',
        ]" :contador="$contadorGosToken" :totaltokens="$totalTokensGosToken"
            :totalusdamount="$totalUsdAmountGosToken" />
    @elseif($tipo == 3)
        <x-table-component title="Bonus Start " :variable="$trnxBonusStart" :campos="[
            'tnx_id' => 'Transaction ID',
            'tnx_type' => 'Type',
            'tnx_time' => 'Date',
            'tokens' => 'Tokens',
        ]" :contador="$contadorBonusStart"
            :totaltokens="$totalTokensBonusStart" :totalusdamount="$totalUsdAmountBonusStart" />
    @else
        <x-table-component title="Bonus Dollars(USDT) " :variable="$trnxBonusDollarsAll" :campos="[
            'code' => 'Code',
            'tnx_id' => 'Transaction Code',
            'name' => 'Bonus For',
            'level_bonus' => 'Bonus Level',
            'amount' => 'Amount (USDT)',
            'type' => 'Type Bonus',
            'created_at' => 'Date Created',
            'wallet' => 'Wallet (USDT)',
            'hash' => 'Hash',
        ]" :contador="$contadorDolares"
            :totaltokens="$totalTokensUsd" :totalusdamount="$totalUsdAmount" />
    @endif
</body>
<script>
    $(document).ready(function() {
       
        $('#totales').dataTable({
            "autoWidth": false
        });
    });
</script>

</html>
