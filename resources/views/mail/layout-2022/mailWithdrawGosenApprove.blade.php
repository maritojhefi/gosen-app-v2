@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', token_symbol().' withdrawal approved')
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h2>
                                                        {{___('Hello')}}, {{ $trnx->tnxUser->name }}
                                                    </h2>
                                                </strong>
                                                <p class="float-right" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Congratulations! We have successfully transfered')}}
                                                        <strong>{{ $trnx->tokens . ' ' . token_symbol() }}</strong>
                                                        {{___('into your wallet address as
                                                        below')}}:<strong>{{ $trnx->wallet_address }}</strong>
                                                    </span>
                                                    <br>
                                                </p>
                                                <br>
                                                @if ($trnx->payment_id != '')
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Withdraw Reference')}} ID (HASH) -
                                                        <strong>{{ $trnx->payment_id }}</strong>.
                                                    </span>
                                                    <br>
                                                @else
                                                    <br>
                                                @endif
                                                <br>
                                                <p class="float-right" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('If you have not received')}} token {{___('into your wallet
                                                        yet, please feel free to contact us')}}.
                                                    </span>
                                                    <br>
                                                </p>

                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
