<!DOCTYPE HTML
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css">
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title></title>

    <style type="text/css">
        @media only screen and (min-width: 620px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-50 {
                width: 300px !important;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }

        }

        @media (max-width: 620px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col>div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table,
        td {
            color: #000000;
        }

        a {
            color: #161a39;
            text-decoration: underline;
        }
    </style>
    <link preload href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
</head>

<body class="clean-body u_body"
    style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #f9f9f9;color: #000000">
    <table
        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f9f9f9;width:100%"
        cellpadding="0" cellspacing="0">
        <tbody>
            <tr style="vertical-align: top">
                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">

                    @hasSection('footer')
                        @if ($type_mail == 'design')
                            <div class="u-row-container" style="padding: 0px;background-color: #f9f9f9">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;" role="presentation"
                                                        cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table height="0px" align="center" border="0"
                                                                        cellpadding="0" cellspacing="0" width="100%"
                                                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                        <tbody>
                                                                            <tr style="vertical-align: top">
                                                                                <td
                                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                    <span>&#160;</span>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">

                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                    <table style="font-family:'Roboto',sans-serif;" role="presentation"
                                                        cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">

                                                                    <table width="100%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('images/logo-mail.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <table style="font-family:'Roboto',sans-serif;" role="presentation"
                                                        cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                    align="left">

                                                                    <div
                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                        <p
                                                                            style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                            <span
                                                                                style="font-size: 28px; line-height: 39.2px; color:#280f53; font-family: Roboto, sans-serif;">
                                                                                @yield('title')
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endif
                    @else
                        @hasSection('type_mail')
                        @else
                            <div class="u-row-container" style="padding: 0px;background-color: #f9f9f9">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table height="0px" align="center"
                                                                        border="0" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                        <tbody>
                                                                            <tr style="vertical-align: top">
                                                                                <td
                                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                    <span>&#160;</span>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">

                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">

                                                                    <table width="100%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">

                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('images/logo-mail.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                    align="left">

                                                                    <div
                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                        <p
                                                                            style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                            <span
                                                                                style="font-size: 28px; font-weight: 900; line-height: 39.2px; color:#280f53; font-family: Roboto, sans-serif;">
                                                                                @yield('title')
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif


                    @yield('content')



                    @hasSection('footer')
                        @if ($type_mail == 'design')
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%";>
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:45px 40px 0px;font-family:'Roboto',sans-serif;"
                                                                    align="center">
                                                                    <div
                                                                        style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                        <p
                                                                            style="font-size: 14px; line-height: 140%;font-style: italic;">
                                                                            <span id="piedepagina"
                                                                                style="color: #888888;font-style: italic; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                        class="text-primary"
                                                                                        style="font-size: 16px; font-style: italic; line-height: 22.4px;">{{ $footer }}</span></em></span><br /><span
                                                                                style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                        style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #D9D9D9 100%, #D9D9D9 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                        <div class="u-col"
                                            style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div align="center">
                                                                        <p style="line-height: 140%; font-size: 14px;">
                                                                            <span
                                                                                style="font-size: 14px; line-height: 19.6px;"><span
                                                                                    style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                        style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                        {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table
                                                        style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div align="center">
                                                                        <a href="https://app.disruptive.center/"
                                                                            target="_blank"
                                                                            style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                            <span
                                                                                style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                    style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                </span></span>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div
                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @else
                        @hasSection('type_mail')
                        @else
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #D9D9D9 100%, #D9D9D9 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                        <div class="u-col"
                                            style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div align="center">
                                                                        <p style="line-height: 140%; font-size: 14px;">
                                                                            <span
                                                                                style="font-size: 14px; line-height: 19.6px;"><span
                                                                                    style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                        style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                        {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table
                                                        style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <table width="70%" cellpadding="0"
                                                                        cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                align="center">
                                                                                <img loading="lazy" align="center" border="0"
                                                                                    src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                    alt="Image" title="Image"
                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div align="center">
                                                                        <a href="https://app.disruptive.center/"
                                                                            target="_blank"
                                                                            style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                            <span
                                                                                style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                    style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                </span></span>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                                <div class="u-row"
                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                    <div
                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                        <div class="u-col u-col-100"
                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                            <div style="width: 100% !important;">
                                                <div
                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                    <table style="font-family:'Roboto',sans-serif;"
                                                        role="presentation" cellpadding="0" cellspacing="0"
                                                        width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                    align="left">
                                                                    <div
                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
