@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Confirmation Request'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                            @if ($registro->status == 'canceled')
                                <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                    cellspacing="0" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                                align="center">
                                                <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                            {{ ___('Hello') }}, {{ $user->name }},
                                                            {{ ___('the reason for
                                                                                                                        this email is
                                                                                                                        to inform you that your withdrawal') }}
                                                            #{{ $registro->code }} {{ ___('of') }}
                                                            <strong>{{ $registro->amount - 2 }}
                                                                (USDT)</strong>
                                                            {{ ___('was rejected in the
                                                                                                                        wallet') }}:
                                                        </span>
                                                        <br>
                                                        <input class="form-control"
                                                            style="padding:10px; width:350px; margin-top: 20px; text-align:center; border-radius:40px"
                                                            type="text" value="{{ $registro->wallet }}" readonly>
                                                    </p>
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        &nbsp;
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <table
                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin: auto 43px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                        <thead style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                            <tr>
                                                <th colspan="3"
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                    {{ ___('Reason') }}:</th>
                                            </tr>
                                        </thead>
                                        <tbody
                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___($registro->comment) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                    cellspacing="0" width="100%";>
                                    <tbody>
                                        <tr>
                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                                align="center">
                                                <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                            {{ ___('Hello') }}, {{ $user->name }},
                                                            {{ ___('Congratulations! We have successfully
                                                                                                                        transfered') }}
                                                            <strong>{{ $registro->amount - 2 }}
                                                                (USDT)</strong>,
                                                            {{ ___('into your
                                                                                                                        wallet
                                                                                                                        address as
                                                                                                                        below') }}.
                                                        </span>
                                                        <br>
                                                        <input class="form-control"
                                                            style="padding:10px; margin-top: 20px; width:350px; text-align:center; border-radius:40px"
                                                            type="text" value="{{ $registro->wallet }}" readonly>
                                                    </p>
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        &nbsp;
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row d-flex justify-content-center">
                                    <table
                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                        <thead style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                            <tr>
                                                <th colspan="3"
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                    {{ ___('The details of the request are as follows') }}:</th>
                                            </tr>
                                        </thead>
                                        <tbody
                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                            <tr>
                                                <td width="150"
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Withdraw Bonus') }} ID</td>
                                                <td width="15"
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">#{{ $registro->code }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    ICO {{ ___('Stage') }}</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___('Stage') }}
                                                        {{ $registro->stage_id }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Amount') }}</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $registro->amount - 2 }}
                                                        (USDT)</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Status') }} </td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($registro->status)) }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Type') }}</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($registro->type)) }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Payment Method') }}</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___('Manual') }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Payment to Address') }}</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $registro->wallet }}</strong>
                                                </td>
                                            </tr>
                                            @if ($registro->hash != null)
                                                <tr>
                                                    <td
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                        {{ ___('Buy Code') }}(Hash)</td>
                                                    <td
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                        :</td>
                                                    <td
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                        <strong
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $registro->hash }}</strong>
                                                    </td>
                                                </tr>
                                            @else
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
