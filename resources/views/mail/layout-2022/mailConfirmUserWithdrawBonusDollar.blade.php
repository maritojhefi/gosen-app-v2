@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Confirmacion de retiro'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h2>
                                                        {{___('Hello')}}, {{ $bonus->userBy->name }}
                                                    </h2>
                                                </strong>
                                                <p align="center" style="font-size: 10px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Gracias por confirmar su solicitud de retiro de dolares, su retiro de')}}{{$bonus->amount.' '.token_symbol()}},{{___('se procesara dentro de las proximas 24 a 48 horas, se le notificara por correo cuando se haya aprobado, adjunto a los detalles del mismo')}}
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
