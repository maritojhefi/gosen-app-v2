@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Mail Change Request'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;"><span
                                                        style="font-size: 18px; line-height: 25.2px; color: #666666;">{{ ___('Hello') }}
                                                        , {{ $user->name }}</span>
                                                </p>
                                                <p style="font-size: 14px; line-height: 140%;">&nbsp;
                                                </p>
                                                <p style="font-size: 14px; line-height: 140%;"><span
                                                        style="font-size: 18px; line-height: 25.2px; color: #666666;">{{ ___('We have sent you this email in response to your request to change your email to the company name.') }}
                                                    </span></p>
                                                <p style="font-size: 14px; line-height: 140%;">&nbsp;
                                                </p>
                                                <p style="font-size: 14px; line-height: 140%;"><span
                                                        style="font-size: 18px; line-height: 25.2px; color: #666666;">{{ ___('Simply click the button below to confirm your new email address:') }}
                                                    </span></p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div align="center">
                                                <a href="{{ $ruta }}" target="_blank"
                                                    style="margin-bottom: 5%;box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-image: linear-gradient(45deg, #FF914D 0%, #FF914D 100%); border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                    <span style="display:block;padding:10px 40px;line-height:120%;"><span
                                                            style="font-size: 18px; line-height: 21.6px;">
                                                            {{ ___('Confirm Email Address') }}
                                                        </span></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;"><span
                                                        style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                style="font-size: 16px; line-height: 22.4px;"></span></em></span><br /><span
                                                        style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
