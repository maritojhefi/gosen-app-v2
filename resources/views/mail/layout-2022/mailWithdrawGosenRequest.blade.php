@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Withdrawal Request'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:15px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h2>
                                                        {{ ___('Hello') }}, {{ $user->name }}
                                                    </h2>
                                                </strong>
                                                <p align="left" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('We received your request to withdraw your dollars to your wallet. We will review your request shortly and should be processed with 24-48 hours after you have approved this withdrawal request. You will be notified by email when we have completed your withdraw') }}.
                                                    </span>
                                                    <br>
                                                </p>
                                                <small align="center"
                                                    style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                    "{{ ___('Este correo tiene una validez de 24 horas, pasando dicho tiempo y si no se confirmo este retiro, pasara a estado cancelado') }}"
                                                </small>
                                                <table
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;width:100%;margin:20px 0px 30px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                    <thead style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                        <tr>
                                                            <th colspan="3"
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                                {{ ___('Withdraw Details') }}:</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                                        <tr>
                                                            <td width="150"
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Withdrawal code') }}</td>
                                                            <td width="10"
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $bonus->code }}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150"
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Withdrawal amount') }}</td>
                                                            <td width="10"
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $bonus->amount - 2 . ' (USDT)' }}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Address Wallet') }}</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $bonus->wallet }}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Withdrawal status') }}</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($bonus->status)) }}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Withdrawal creation date') }} </td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                    {{ date('jS F, Y', strtotime($bonus->created_at)) }}</span>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                {{ ___('Retirement creation time') }} </td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                :</td>
                                                            <td
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                <strong
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                    {{ $horaRetiro }}</span>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div align="center">
                                                    <a href="{{ $ruta }}" target="_blank"
                                                        style="margin-bottom: 5%;box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-image: linear-gradient(45deg, #FF914D 0%, #FF914D 100%); border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                        <span
                                                            style="display:block;padding:10px 40px;line-height:120%;"><span
                                                                style="font-size: 18px; line-height: 21.6px;">
                                                                {{ ___('Confirm dollar withdrawal') }}
                                                            </span></span>
                                                    </a>
                                                </div>
                                                <small style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                    {{ ___('Nota: Si usted no realizo esta solicitud, por favor ignore este correo y en 24 hrs se cancelara esta solicitud') }}.
                                                </small>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
