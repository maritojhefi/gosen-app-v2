@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Refund Transaction'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h2>
                                                        Hello, {{ $transaction->tnxUser->name }}
                                                    </h2>
                                                </strong>
                                                <p class="float-right" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Thank you for purchase')}} {{ token_symbol() }}
                                                        token.
                                                    </span>
                                                    <br>
                                                </p>
                                                <p class="float-right" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('We found some problem in your payment so we have
                                                        refunded your order and readjusted your')}} token
                                                        {{('balance. Please find below your refund and
                                                        original purchase order details')}}.
                                                    </span>
                                                    <br>
                                                </p>
                                                <br>
                                                <div class="row pt-2 d-flex justify-content-center">
                                                    <table
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                        <thead
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                            <tr>
                                                                <th colspan="3"6
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                                    {{___('Refund Details')}}:</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                                            <tr>
                                                                <td width="150"
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Refund')}} ID</td>
                                                                <td width="15"
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $refund->tnx_id }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Refund')}} Token</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        {{ $refund->tokens . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Refund Amount')}}</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        {{ $refund->amount . ' ' . strtoupper($refund->currency) }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Refund Note')}} </td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        @php
                                                                            $json = json_decode($refund->extra);
                                                                            $mensaje = $json->message;
                                                                        @endphp
                                                                        {{ ___(ucfirst($mensaje)) }}
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <br>
                                                    <table
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                        <thead
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                            <tr>
                                                                <th colspan="3"
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                                    {{___('Refund against
                                                                    order')}} - ({{ $transaction->tnx_id }}) :
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                                            <tr>
                                                                <td width="150"
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Total')}} Token</td>
                                                                <td width="15"
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $transaction->tokens . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Pay Amount')}}</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        {{ $transaction->amount . ' ' . strtoupper($transaction->currency) }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Payment Method')}}</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        {{ ucfirst($transaction->payment_method) }}</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    {{___('Purchase at')}} </td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    :</td>
                                                                <td
                                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                    <strong
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                        {{ date('jS F, Y', strtotime($transaction->tnx_time)) }}</span>
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <br>
                                                <p class="float-right pt-2" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Note: Your payment is already refunded via the
                                                        same payment method that you used for payment')}}.
                                                    </span>
                                                    <br>
                                                </p>
                                                <p class="float-right" style="font-size: 12px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('If you have any questions about this refund,
                                                        please feel free to contact us')}}.
                                                    </span>
                                                    <br>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
