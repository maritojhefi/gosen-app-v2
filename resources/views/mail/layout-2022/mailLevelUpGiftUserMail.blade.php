@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('¡Subiste de Nivel!'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="display: flex;justify-content: center; overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                            align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding-right: 0px;padding-left: 0px; padding-bottom:3%;"
                                                        align="center">
                                                        <i align="center" border="0" class="fa fa-star"
                                                            style="font-size:100px;color:#280f53;position: relative; margin-top: -3%; margin-bottom: -5%; outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 90%;">
                                                        </i>
                                                        <br>
                                                        <h2 class="mt-2">
                                                            {{ $bonusReferral->regalotype->name }}
                                                        </h2>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p align="left"
                                                    style="font-size: 10px; line-height: 140%; margin-bottom: 2%;">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Muchas Felicidades') }}&nbsp;{{ $bonusReferral->user->name }}.&nbsp;{{ ___('Acabas de subir de nivel a') }}&nbsp;{{ $bonusReferral->regalotype->name }}&nbsp;{{ ___('por invitar a más dé') }}.&nbsp;{{ $bonusReferral->regalotype->total_guests }}&nbsp;{{ ___('personas') }}.&nbsp;{{ ___('Por esta razón obtuviste') }},&nbsp;{{ $bonusReferral->regalotype->total_tokens }}
                                                        {{ token_symbol() }}
                                                        {{ ___('adicionales que se abonaron a tu cuenta, además de muchos más beneficios en') }}&nbsp;Disruptive
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="font-size: 10px; line-height: 140%; margin-bottom: 2%;">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('En') }}&nbsp;Disruptive&nbsp;{{ ___('puedes conseguir aún más beneficios invitando a más personas a unirse a tu red de asociados') }}.&nbsp;{{ ___('O cumpliendo tareas dentro del sistema para acumular puntos y beneficios') }}.
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="font-size: 10px; line-height: 140%; margin-bottom: 2%;">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        ¡{{ ___('Gracias por unirte a') }}&nbsp;Disruptive!
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="font-size: 10px; line-height: 140%; margin-bottom: 2%;">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('El equipo de') }}&nbsp;Disruptive
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div align="center">
                                                <a href="https://app.disruptive.center/user" target="_blank"
                                                    style="margin-bottom: 5%;box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-image: linear-gradient(45deg, #FF914D 0%, #FF914D 100%); border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                    <span style="display:block;padding:10px 40px;line-height:120%;"><span
                                                            style="font-size: 18px; line-height: 21.6px;">
                                                            {{ ___('Ir a') }}&nbsp;Disruptive
                                                        </span></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
