@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Recibiste') . ' ' . 'Bonus Staking')
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h4>
                                                        {{ ___('Hello') }}, {{ $transaction->tnxUser->name }}
                                                    </h4>
                                                </strong>
                                                <p align="center" style="font-size: 10px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Felicidades, acumulaste') . ' ' . $contador . ' ' . ___('bonos por tus compras, y recibiste') . ' ' . $acumulado . ' ' . token_symbol() . ' ' .___('en la semana, por la compra de tu') . ' NFT' . ' ' . $transaction->tnx_id }}.
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Por tu NFT:') }}
                                                    </span>
                                                </p>
                                                <p align="center" style="font-size: 10px; line-height: 140%;">
                                                    <img loading="lazy" src="{{ asset('nft/confirmed-nft/' . $transaction->nft) }}"
                                                        style="border-radius: 10%; width: 50%;" />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
