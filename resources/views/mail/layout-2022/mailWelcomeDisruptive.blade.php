@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', $user->name . ' ' . ___('vienen cambios en el mundo, estas preparado para ello?'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                            align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                                        <img loading="lazy" align="center" border="0"
                                                            src="{{ asset('imagenes/layout-correo/bienvenida.png') }}"
                                                            alt="Image" title="Image"
                                                            style="position: relative; margin-top: -13%; margin-bottom: -9%; outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 90%;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Hola') }}&nbsp;{{ $user->name }}.
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify; font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Bienvenido a') }}&nbsp;Disruptive&nbsp;Center,&nbsp;{{ ___('he estado esperando 8 largos años por este momento') }}.
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify;font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Te aseguro que el conocimiento que estas a punto de acceder cambiara tu manera de ver los negocios para siempre, que te ayudaran a ver lo qué otros no ven, y a hacer lo que otros no hacen') }}.
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify;font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Solo en el último año se han desvanecido más de $ 13 billones en riqueza familiar, y la carnicería apenas comienza, con bancos, fondos financieros y demas') }}.
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify;font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Es por eso que decidí hace dos anos en preparar tu mente y mostrarte lo que esta pasando. Y como asegurarte de no perder oportunidades únicas en este tiempo') }}:
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Click aquí para ver el primer video y recibir tokens de regalo') }}.
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Este video esta valuado en 97 $us. Hoy lo tienes Gratis') }}!
                                                    </span>
                                                </p>
                                                <table
                                                    style="font-family:'Roboto',sans-serif;display: flex;justify-content: center;align-items: center;padding-top: 3%;padding-bottom: 3%;"
                                                    role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                                    border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px;font-family:'Roboto',sans-serif;"
                                                                align="center">
                                                                <div align="center">
                                                                    <a href="https://app.disruptive.center/user"
                                                                        target="_blank"
                                                                        style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-image: linear-gradient(45deg, #FF914D 0%, #FF914D 100%); border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                        <span
                                                                            style="display:block;padding:10px 40px;line-height:120%;"><span
                                                                                style="font-size: 18px; line-height: 21.6px;">
                                                                                {{ ___('Click Aqui') }}
                                                                            </span></span>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <p align="center" style="font-size: 10px; line-height: 140%; ">
                                                    <span
                                                        style="font-style: italic; font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        "{{ ___('No hay nada más poderoso que una idea cuyo momento ha llegado') }}"
                                                    </span>
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        - Victor Hugo
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify;font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Hoy, esto es especialmente cierto ya que las tendencias y estilos están en constante evolución') }}
                                                    </span>
                                                </p>
                                                <p align="left"
                                                    style="text-align: justify;font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('En resumen, la cita de') }}&nbsp;Victor Hugo
                                                        &nbsp;{{ ___('nos recuerda que las ideas pueden tener un gran impacto, pero solo cuando el momento es el adecuado y estamos dispuestos a trabajar duro para hacer que se hagan realidad.') }}
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Atte') }}.
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        Adrian Trigo R.
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        Team Disruptive Center
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        <a
                                                            href="https://app.disruptive.center">https://app.disruptive.center</a>
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%; ">
                                                    <span style="font-size: 14px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('El mundo jamás será igual') }}.
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            {{-- <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div align="center">
                                                <a href="https://app.disruptive.center/user" target="_blank"
                                                    style="margin-bottom: 5%;box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-image: linear-gradient(45deg, #FF914D 0%, #FF914D 100%); border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                    <span style="display:block;padding:10px 40px;line-height:120%;"><span
                                                            style="font-size: 18px; line-height: 21.6px;">
                                                            {{ ___('Ver Perfil') }}
                                                        </span></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
