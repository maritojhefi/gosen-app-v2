@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Transaction Rejected'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <strong>
                                                    <h2>
                                                        {{___('Hello')}}, {{ $trnx->tnxUser->name }}
                                                    </h2>
                                                </strong>
                                                @if ($trnx->abstract != null)
                                                    <p align="left" style="font-size: 12px; line-height: 140%;">
                                                        <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                            {{___('We have received your request to buy')}}
                                                            {{ strtoupper(token_symbol()) }} token. {{___('Unfortunately your
                                                            application was rejected.')}}
                                                        </span>
                                                        <br>
                                                    </p>
                                                    <br>
                                                    <div class="row d-flex justify-content-center">
                                                        <table
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                            <thead
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                <tr>
                                                                    <th colspan="3"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                                        {{___('Here are the details')}} : </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                                                <tr>
                                                                    <td width="150"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Transaction')}} ID</td>
                                                                    <td width="15"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">#{{ $trnx->tnx_id }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        ICO {{___('Stage')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{___('Stage')}}
                                                                            {{ $trnx->stage }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        Token {{___('Number')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->tokens . ' ' . strtoupper(token_symbol()) }}
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        Token {{___('Bonus')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->total_bonus . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Total')}} Token</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->total_tokens . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Transaction Status')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($trnx->status)) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Amount')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->amount }}
                                                                            {{ strtoupper($trnx->currency) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    @php
                                                                        $json = json_decode($trnx->abstract);
                                                                        $mensaje = $json->message;
                                                                    @endphp
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Reason')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($mensaje)) }}</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <br>
                                                @else
                                                    <p align="left" style="font-size: 12px; line-height: 140%;">
                                                        <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                            {{___('We have received your request to buy')}}
                                                            {{ strtoupper(token_symbol()) }} token. {{___('Unfortunately your
                                                            application was rejected')}}.
                                                        </span>
                                                        <br>
                                                    </p>
                                                    <br>
                                                    <div class="row d-flex justify-content-center">
                                                        <table
                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                            <thead
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                                <tr>
                                                                    <th colspan="3"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                                        {{___('Here are the details')}} : </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody
                                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                                                <tr>
                                                                    <td width="150"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Transaction')}} ID</td>
                                                                    <td width="15"
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">#{{ $trnx->tnx_id }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        ICO {{___('Stage')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{___('Stage')}}
                                                                            {{ $trnx->stage }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        Token {{___('Number')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->tokens . ' ' . strtoupper(token_symbol()) }}
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        Token {{___('Bonus')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->total_bonus . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Total')}} Token</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->total_tokens . ' ' . strtoupper(token_symbol()) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Payment Status')}}</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($trnx->status)) }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        {{___('Payment Amount')}} </td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        :</td>
                                                                    <td
                                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                                        <strong
                                                                            style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $trnx->amount }}
                                                                            {{ strtoupper($trnx->currency) }}</strong>
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <br>
                                                @endif
                                                <br>
                                                <p align="left" style="font-size: 10px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('Note: No tokens are given and your balance will return to it is
                                                        pre-purchase state')}}.
                                                    </span>
                                                </p>
                                                <p align="left" style="font-size: 10px; line-height: 140%;">
                                                    <span style="font-size: 18px; line-height: 25.2px; color: #666666;">
                                                        {{___('If you have any questions, please feel free to
                                                        contact us')}}.
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
