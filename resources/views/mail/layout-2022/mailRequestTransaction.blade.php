@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Solicitud de compra'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0"
                                cellspacing="0" width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;">
                                                    <span style="font-size: 15px; line-height: 25.2px; color: #666666;">
                                                        {{ ___('Thank you for your contribution') }}!
                                                        {{ ___('You have requested to purchase') }} {{ $sigla->value }}
                                                        token.
                                                        {{ ___('Your order has been received and is now being processed') }}.
                                                    </span>
                                                    <br>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row d-flex justify-content-center">
                                <table
                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                    <thead style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                        <tr>
                                            <th colspan="3"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                {{ ___('You order details are show below for your reference') }}.</th>
                                        </tr>
                                    </thead>
                                    <tbody
                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                        <tr>
                                            <td width="150"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Order') }} ID</td>
                                            <td width="15"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">#{{ $tnx->tnx_id }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Type User') }}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                    {{ ___('Tipo de usuario') }}{{ $user->typeUser->valor }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                Token {{ ___('Number') }}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $tnx->tokens }}
                                                    {{ $sigla->value }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Package') }}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                    {{$tnx->paquete->nombre}}
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Payment Status') }}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($tnx->status)) }}</strong>
                                            </td>
                                        </tr>
                                        @if ($tnx->payment_method == 'coinpayments' || $tnx->payment_method == 'stripe')
                                            <tr>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    {{ ___('Payment Method') }} </td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    :</td>
                                                <td
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                    <strong
                                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ucfirst($tnx->payment_method) }}</strong>
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Payment Amount') }} </td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                    {{ ( ($tnx->currency =='btc') ? round($btcPrice, max_decimal()) : to_num($precioUsd, 'max', ',') ) }}
                                                    {{ strtoupper($tnx->currency) }}
                                                </strong>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{ ___('Payment Amount in USD') }} </td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $priceInUsd }}
                                                    (USDT)</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
