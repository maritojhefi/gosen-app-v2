@extends('mail.layout-2022.layoutTransactionWithdrawMail')
@section('title', ___('Transaction Approved'))
@section('content')
    <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row"
            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                        <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;">
                                                    <span style="font-size: 15px; line-height: 25.2px; color: #666666;">
                                                        {{___('Congratulation, your order has been processed successfully, thank you for your contribution and purchase our')}} {{ $sigla->value }} Token.
                                                    </span>
                                                    <br>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row d-flex justify-content-center">
                                <table
                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box;margin:0 auto;border-bottom:1px solid rgba(0,0,0,0.15)">
                                    <thead style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                        <tr>
                                            <th colspan="3"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;padding-bottom:8px;margin:0;text-align:left;padding:0px 15px 7px 0px;border-bottom:1px solid rgba(0,0,0,0.15)">
                                                {{___('The details of the request are as follows')}}:</th>
                                        </tr>
                                    </thead>
                                    <tbody
                                        style="font-family:'Roboto',sans-serif;box-sizing:border-box;text-align:left!important">
                                        <tr>
                                            <td width="150"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{___('Order')}} ID</td>
                                            <td width="15"
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">#{{ $trnx_id }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                Token {{___('Number')}}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ $tokens }}
                                                    {{ $sigla->value }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{___('Payment Status')}}</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">{{ ___(ucfirst($status)) }}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                {{___('Payment Amount')}} </td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                :</td>
                                            <td
                                                style="font-family:'Roboto',sans-serif;box-sizing:border-box;color:#74787e;margin:0;font-size:14px;line-height:20px;padding:5px 15px 5px 0px">
                                                <strong
                                                    style="font-family:'Roboto',sans-serif;box-sizing:border-box">
                                                    {{ ( ($currency =='btc') ? round($btcPrice, max_decimal()) : to_num($precioUsd, 'max', ',') ) }}
                                                    {{ strtoupper($currency) }}
                                                </strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <table style="font-family:'Roboto',sans-serif;" role="presentation" cellpadding="0" cellspacing="0"
                                width="100%";>
                                <tbody>
                                    <tr>
                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                            align="center">
                                            <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;  font-style: italic;">
                                                    <span style="font-s ize: 15px; line-height: 25.2px; color: #666666;">
                                                        {{ ___(' Remember that you can request a refund of your transaction in the next 72 hours from when your transaction was approved, after this time you will not be able to request said refund') }}</span>
                                                    <br>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
