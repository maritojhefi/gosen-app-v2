<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" content="notranslate" translate="no">

<head>
    @yield('metas')
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1, user-scalable=0 viewport-fit=cover">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">

    @if (auth()->user()->theme_color == 'style-purple')
        <link preload rel="shortcut icon" href="{{ asset('images/token-symbol-light.png') }}">
    @else
        <link preload rel="shortcut icon" href="{{ asset('images/token-symbol-light.png') }}">
    @endif
    <title> @yield('title') | {{ ucfirst(env('APP_NAME')) }}</title>
    <meta name="description"
        content="{{ ___('Recibe información de alto valor por tiempo limitado. Proyecto Disruptivo en cuenta regresiva') }}" />


    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-534S2F6');
    </script>

    <style>
        :root {
            /* variables auxiliares para styles personalizados */
            --color-purple-inactivo-auxiliar: {{ auth()->user()->theme_color == 'style-purple' ? '#8590a5' : '' }};
            --color-purple-auxiliar: {{ auth()->user()->theme_color == 'style-purple' ? '#452c97' : '' }};
            --color-base-auxiliar: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            --color-loader-auxiliar: {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }};
            --color-style-black-ddd-auxiliar: {{ auth()->user()->theme_color == 'style-black' ? '#ddd' : asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            --color-theme-black-base-auxiliar: {{ auth()->user()->theme_color == 'style-black' ? asset(theme_color_user(auth()->user()->theme_color, 'base')) : '#ddd' }};
            --color-text-theme-auxiliar: {{ auth()->user()->theme_color == 'style-black' ? 'white' : 'black' }};
            --color-popover-auxiliar: {{ auth()->user()->theme_color == 'style-black' ? '#1b262c' : '#fff' }};
            --color-popover-text-auxiliar: {{ auth()->user()->theme_color == 'style-black' ? '#fff' : '' }};
            /* variables auxiliares para theme user */
            --color-base-theme-user: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            --color-base-sidebar-theme-user: {{ asset(theme_color_user(auth()->user()->theme_color, 'sidebar')) }};
            --color-base-a-blanco-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? 'var(--color-base-theme-user)' : '#fff' }};
            --bg-loading-theme-user: {{ auth()->user()->theme_color == 'style-black' ? 'rgba(20, 17, 17, 0.747)' : '#fff' }};
            --color-ti-close-theme-user: {{ auth()->user()->theme_color == 'style-black' ? 'black' : '#495463' }};
            --color-title-sidebar-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? '#d7e4ec' : 'var(--color-base-theme-user)' }};
            --color-sidebar-menu-ul-a-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? '#d7e4ec' : '#8590a5' }};
            --color-sedebar-menu-a-hover-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? 'white' : 'var(--color-base-theme-user)' }};
            --bg-sedebar-menu-a-hover-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? 'var(--color-base-sidebar-theme-user)' : '#fff' }};
            --color-vertical-menu-li-hover-theme-user: {{ auth()->user()->theme_color != 'style-purple' ? '#fff' : 'var(--color-base-sidebar-theme-user)' }};
            --color-base-purple-a-blanco-theme-user: {{ auth()->user()->theme_color == 'style-purple' ? 'var(--color-base-theme-user)' : '#fff' }};
            --bg-gift-card-theme-user: {{ auth()->user()->theme_color == 'style-purple' ? '#fff' : 'linear-gradient(45deg, var(--color-base-sidebar-theme-user) 0%, var(--color-base-theme-user) 100%)' }};
            --border-gift-card-theme-user: {{ auth()->user()->theme_color == 'style-purple' ? 'solid 1px #83898f' : 'none' }};
            --text-gift-card-theme-user: {{ auth()->user()->theme_color == 'style-purple' ? '#83898f' : '#fff' }};
            /* variables auxiliares theme css */
            --color-include-css: {{ auth()->user()->theme_color == 'style-black' ? '#fff' : 'var(--color-base-theme-user)' }};
            --bg-compoment-theme-user: {{ auth()->user()->theme_color == 'style-black' ? '#1B262C' : '#fff' }};
            /*variables para top referidos user*/
            --color-referidos-background: {{ auth()->user()->theme_color == 'style-black' ? '#1B262C' : 'white' }};
            --color-referidos-text: {{ auth()->user()->theme_color == 'style-black' ? 'white' : '#1B262C' }};
            /*color transparente de acuerdo al tema del usuario*/
            --color-transparente-theme-user: {{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }};
        }
    </style>

    {{-- Start VITE --}}
    @vite('resources/css/app.css')
    {{-- End VITE --}}

    <script>
        window.PUSHER_APP_KEY = '{{ config('broadcasting.connections.pusher.key') }}';
        window.APP_DEBUG = {{ config('app.debug') ? 'true' : 'false' }};
    </script>

    <script src="{{ asset('assets/websockets.js') }}"></script>

    {{-- Start CSS minificado  --}}
    @if (auth()->user()->theme_color != 'style')
        @vite('resources/css/resources-theme-user.css')
        <link preload rel="stylesheet" href="{{ asset('assets/css/' . auth()->user()->theme_color . '.css') }}">
        @vite('resources/css/theme-user-general.css')
    @else
        @vite('resources/css/theme-user-style.css')
    @endif
    @vite('resources/css/views-style.css')
    {{-- End CSS minificado  --}}

    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) window.scrollTo(0, scrollpos);
        });

        window.onbeforeunload = function(e) {
            localStorage.setItem('scrollpos', window.scrollY);
        };
    </script>

    @include('disruptive.includes.clockdown')
    @yield('gosencss')

    @stack('header')

    @if (get_setting('site_header_code', false))
        {{ html_string(get_setting('site_header_code')) }}
    @endif
    @livewireStyles
    @livewireScripts
</head>

<body data-sidebar="dark" class="no-touch {{ session('cambiarSidebar') == 'hide' ? ' vertical-collpsed' : '' }}">
    @include('disruptive.includes.svg-background-include')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-534S2F6" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="backk" class=""></div>

    <div id="logocontainer">
        <div id="pelogo">
            <div class="loader-circle"></div>
            <div class="loader-line-mask">
                <div class="loader-line"></div>
            </div>
            @switch(auth()->user()->theme_color)
                @case('style')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-azul.png') }}" width="60" />
                @break

                @case('style-blue')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-celeste.png') }}" width="60" />
                @break

                @case('style-green')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-verde.png') }}" width="60" />
                @break

                @case('style-coral')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-rojo.png') }}" width="60" />
                @break

                @case('style-black')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-negro.png') }}" width="60" />
                @break

                @case('style-purple')
                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-morado.png') }}" width="60" />
                @break

                @default
            @endswitch

        </div>
    </div>

    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    {{-- @if (auth()->user()->theme_color != 'style-purple')
                        <div class="navbar-brand-box" style="padding: 0 1.1rem;">
                            <a href="{{ route('user.home') }}" class="logo logo-light text-center">
                                <span class="logo-sm">
                                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-blanco.png') }}"
                                        alt="{{ site_whitelabel('name') }}" height="30">
                                </span>
                                <span class="logo-lg">
                                    <img loading="lazy" src="{{ asset('disruptive/imagenes/disruptive-blanco.png') }}"
                                        style="width: 100%;height: auto;">
                                </span>
                            </a>
                        </div>
                    @else
                        <div class="navbar-brand-box" style="padding: 0 1.1rem;">
                            <a href="{{ route('user.home') }}" class="logo logo-light text-center">
                                <span class="logo-sm">
                                    <img loading="lazy" src="{{ asset('disruptive/imagenes/d-morada.png') }}"
                                        alt="{{ site_whitelabel('name') }}" height="30">
                                </span>
                                <span class="logo-lg">
                                    <img loading="lazy" src="{{ asset('images/logo-mail.png') }}"
                                        style="width: 100%;height: auto;">
                                </span>
                            </a>
                        </div>
                    @endif --}}
                    <div class="navbar-brand-box d-flex align-items-center justify-content-center">
                        <a href="{{ route('user.home') }}" class="logo logo-light">
                            <span class="logo-sm text-rotate">
                                <div class="row">
                                    <div class="col pr-0" style="top: 35px">
                                        <span class="span-text">
                                            <img src="{{ auth()->user()->theme_color == 'style-purple' ? asset('images/token-symbol-morado.png') : asset('images/token-symbol-light.png') }}"
                                                alt="{{ ucfirst(env('APP_NAME')) }}" height="22">
                                        </span>
                                    </div>
                                </div>
                            </span>
                            <span class="logo-lg text-rotate">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-3 pl-3 pb-1 pr-0 d-flex align-items-center justify-content-center"
                                        style="right: 0px;">
                                        <span class="span-text"><img
                                                src="{{ auth()->user()->theme_color == 'style-purple' ? asset('images/token-symbol-morado.png') : asset('images/token-symbol-light.png') }}"
                                                height="40" width="80"></span>
                                    </div>
                                    <div class="col-sm-9 col-md-9 col-lg-9 mt-0 p-1 d-flex align-items-center justify-content-center gap-1"
                                        translate="no" style="right: 10px;">
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif; color: {{ auth()->user()->theme_color == 'style-purple' ? '#280f53' : 'white' }}">g</span>
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif; color: {{ auth()->user()->theme_color == 'style-purple' ? '#280f53' : 'white' }}">o</span>
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif; color: {{ auth()->user()->theme_color == 'style-purple' ? '#280f53' : 'white' }}">s</span>
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif; color: {{ auth()->user()->theme_color == 'style-purple' ? '#280f53' : 'white' }}">e</span>
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif; color: {{ auth()->user()->theme_color == 'style-purple' ? '#280f53' : 'white' }}">n</span>
                                    </div>
                                </div>
                                {{-- <img src="{{ site_whitelabel('logo-light') }}"
                                    srcset="{{ site_whitelabel('logo-light2x') }}"
                                    alt="{{ site_whitelabel('name') }}" height="40"> --}}
                            </span>
                        </a>
                    </div>
                    <button type="button" onclick="loadDoc()"
                        class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                        <i class="ri-menu-2-line align-middle"></i>
                    </button>

                    <!-- Url Referral -->
                    {!! UserPanel::user_url_usuario() !!}
                    <div class="" style="width: 500px; position:absolute; display:contents">
                        {!! UserPanel::user_referal_gift() !!}
                    </div>
                </div>
                {{-- <div class="container-clock d-none d-xl-block">
                    <span class="h1" id="headline"> </span>
                    <div id="countdown">
                        <ul>
                            <li class="li "><span class="days"></span>{{ ___('Days') }}</li>
                            <li class="li "><span class="hours"></span>{{ ___('Hours') }}</li>
                            <li class="li "><span class="minutes"></span>{{ ___('Minutes') }}</li>
                            <li class="li "><span class="seconds"></span>{{ ___('Seconds') }}</li>
                        </ul>
                    </div>

                </div> --}}
                @if (session('adminId'))
                    <a href="{{ route('user.back') }}" class="btn btn-info mr-3 ml-3">{{ ___('Volver a Admin') }}</a>
                @endif

                {{-- <div class="" id="clock-navbar" style="left: 736px; position: fixed;">
                    <div class="account-info card card-full-height m-0"
                        style="align-items: center; background: transparent; box-shadow: none;">
                        <div class="card-innr p-0">
                            <div class="clock2" style="padding-top: 17px;transform: scale(0.7); right: 77px;"></div>
                            <div class="row css-row" style="font-size: 0.8em;position: relative;top: -30px;right: -35px;">
                                <div class="col-2 css-dias" style="padding-left: 0px"><span
                                        style="">{{ ___('DÍAS') }}</span></div>
                                <div class="col-2 css-hrs" style="padding-left: 0px"><span style="">HRS</span>
                                </div>
                                <div class="col-2 css-min" style="padding-left: 0px"><span style="">MIN</span>
                                </div>
                                <div class="col-2 css-seg" style="padding-left: 0px"><span style="">SEG</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}


                <div class="d-flex">
                    @livewire('whatsapp-check-component')
                    {!! UserPanel::language_switcher_usuario() !!}

                    {{-- color theme --}}
                    @if (auth()->user()->type_user != 0)
                        <div class="dropdown">
                            <button type="button" class="btn header-item waves-effect pl-2 pr-2 popover-btn"
                                data-popover="popover-tema" id="page-header-user-dropdown" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <ul class="color-list">
                                    @if (auth()->user()->theme_color == 'style')
                                        <li class="color m-2" data-color="#334574">
                                            <i class="ri-paint-brush-fill"
                                                style="color: #334574;font-size: 26px; "></i>
                                        </li>
                                    @elseif (auth()->user()->theme_color == 'style-purple')
                                        <li class="color" data-color="#F56651">
                                            <i class="ri-paint-brush-fill"
                                                style="color: #452c97;font-size: 26px; "></i>
                                        </li>
                                    @elseif (auth()->user()->theme_color == 'style-black')
                                        <li class="color m-0" data-color="#F56651">
                                            <i class="ri-paint-brush-fill"
                                                style="color: #d7e4ec;font-size: 26px;"></i>
                                        </li>
                                    @else
                                        <li class="color" data-color="#F56651">
                                            <i class="ri-paint-brush-fill"
                                                style="color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};font-size: 26px;"></i>
                                        </li>
                                    @endif
                                </ul>
                            </button>
                            <div class="popover" id="popover-tema">
                                <div class="popover-content">
                                    <p>{{ ___('Cambiar tema') }}</p>
                                </div>
                            </div>
                            <div class="dropdown-menu dropdown-menu-end color-picker">
                                <!-- item-->
                                <ul class="color-list">
                                    @foreach (App\Helpers\GosenHelper::getColorTipoUsuario() as $item)
                                        <a href="{{ route('user.theme_color', $item[0]) }}">
                                            <li class="color" data-color="{{ $item[1] }}"
                                                style="background: {{ $item[2] }};display: grid; place-items:center;">
                                                <i class="ri-paint-fill text-white"
                                                    style="animation: pulse 1.5s infinite; font-size:20px;"></i>
                                            </li>
                                        </a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif



                    {{-- <div class="dropdown d-none d-lg-inline-block ms-1">
                        <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                            <i class="ri-fullscreen-line"></i>
                        </button>
                    </div> --}}

                    @livewire('notifications-component')


                    <div class="dropdown d-inline-block user-dropdown">
                        <button type="button" class="btn header-item waves-effect pl-2 pr-2"
                            id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            @if (auth()->user()->foto == null || auth()->user()->foto == '')
                                <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}"
                                    class="fotoperfil rounded-circle header-profile-user admin_picture">
                            @else
                                <img loading="lazy"
                                    class="fotoperfil rounded-circle header-profile-user admin_picture"
                                    src="{{ asset2('imagenes/perfil/') }}/{{ auth()->user()->foto }}">
                            @endif
                            <span
                                class="d-none d-xl-inline-block ms-1">{{ auth()->user()->username == null ? auth()->user()->name : auth()->user()->username }}</span>
                            <i class="mdi mdi-chevron-down d-xl-inline-block"></i><!-- d-none -->
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" style="border-radius:20px;">
                            <!-- item-->

                            <a class="dropdown-item" id="perfil" href="{{ route('disruptive.user.profile', 1) }}"
                                style="border-radius:20px;"><i class="ri-user-line align-middle me-1"></i>
                                {{ ___('My Profile') }}</a>
                            <a class="dropdown-item" href="{{ route('all.notifications') }}"
                                style="border-radius:20px;"><i class="far fa-bell align-middle me-1"></i>
                                {{ ___('Notifications') }}</a>
                            <a class="dropdown-item" href="{{ route('user.progress') }}"
                                style="border-radius:20px;"><i class="ri-medal-line align-middle me-1"></i>
                                {{ ___('Tu Progreso') }}</a>
                            {{-- <a class="dropdown-item"
                                href="{{ route('all.notifications', ['filtrar' => 'transacciones']) }}"
                                style="border-radius:20px;"><i class="fa fa-list align-middle me-1"></i>
                                {{ ___('Transacciones') }}</a> --}}

                            @php
                                $infoUser = auth()->user();
                                $avatar = asset('/imagenes/perfil/avatar/default.png');
                                if ($infoUser->foto != null && $infoUser->foto != '') {
                                    $avatar = asset('imagenes/perfil/' . $infoUser->foto);
                                }
                                $user = $infoUser->id . ',' . $infoUser->username . ',' . $infoUser->name . ',' . $infoUser->email . ',' . $infoUser->country . ',' . $infoUser->lang . ',' . $avatar . ',' . $infoUser->whatsapp_status . ',' . $infoUser->mobile . ',' . URL::to('/');
                                $user = Crypt::encrypt($user);
                            @endphp
                            @if (showPage('FaQ'))
                                {{-- <a href="{{ route('user.listado.faq') }}" class="dropdown-item" style="border-radius:20px;"> --}}
                                @env('local')
                                <a href="{{ env('RUTA_TO_FAQ', 'http://localhost/bedesk/help-center/') . $user }}"
                                    class="dropdown-item click-faq" style="border-radius:20px;">
                                    <i class="mdi mdi-frequently-asked-questions align-middle me-1"></i>
                                    <span>{{ get_page('faq', 'menu_title') }} </span>
                                </a>
                                @endenv
                                @production
                                    <a href="{{ 'https://support.disruptive.center/help-center/' . $user }}"
                                        class="dropdown-item click-faq" style="border-radius:20px;">
                                        <i class="mdi mdi-frequently-asked-questions align-middle me-1"></i>
                                        <span>{{ get_page('faq', 'menu_title') }} </span>
                                    </a>
                                @endproduction
                            @endif

                            {{-- @if (get_page('referral', 'status') == 'active' && is_active_referral_system())
                                <a class="dropdown-item d-block" href="{{ route('disruptive.referrals') }}"
                                    style="border-radius:20px;"><i class="ri-team-line align-middle me-1"></i>
                                    {{ get_page('referral', 'menu_title') }}</a>
                            @endif --}}

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger" href="{{ route('log-out') }}"
                                style="border-radius:20px;"
                                onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();">
                                <i class="ri-shut-down-line align-middle me-1 text-danger"></i> {{ ___('Logout') }}
                            </a>
                            <form id="logout-form" action=" {{ route('logout') }}" method="POST"
                                style="display: none;"> <input type="hidden" name="_token"
                                    value="{{ csrf_token() }}"></form>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- ========== Left Sidebar Start ========== -->

        <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                <div id="sidebar-menu" style="padding: 0px 0 30px 0;">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">{{ ___('Menu') }}</li>

                        @if (showPage('Dashboard'))
                            <li>
                                <a href="{{ route('user.home') }}" class="waves-effect">
                                    <i class="ri-home-2-fill"></i>
                                    <span>{{ ___('Dashboard') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Panel Fase 2'))
                            <li>
                                <a href="{{ route('user.home.test', 2) }}" class="waves-effect">
                                    <i class="fa fa-code"></i>
                                    <span>{{ ___('Panel Fase 2') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Panel Fase 3'))
                            <li>
                                <a href="{{ route('user.home.test', 3) }}" class="waves-effect">
                                    <i class="fa fa-code"></i>
                                    <span>{{ ___('Panel Fase 3') }}</span>
                                </a>
                            </li>
                        @endif
                        @if (showPage('Panel Fase 4'))
                            <li>
                                <a href="{{ route('user.home.test', 4) }}" class="waves-effect">
                                    <i class="fa fa-code"></i>
                                    <span>{{ ___('Panel Fase 4') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Buy Token'))
                            <li>
                                <a href="{{ route('user.token') }}" class=" waves-effect">
                                    <i class="ri-shopping-cart-2-fill"></i>
                                    <span>{{ ___('Buy Token') }}</span>
                                </a>
                            </li>
                        @endif


                        @if (showPage('Package'))
                            <li>
                                <a href="{{ route('disruptive.package') }}" class=" waves-effect">
                                    <i class="ri-shopping-cart-2-fill"></i>
                                    <span>{{ ___('Packages') }}</span>
                                </a>
                            </li>
                        @endif


                        @if (showPage('Referral'))
                            <li>
                                <a href="{{ route('disruptive.referrals') }}" class="waves-effect">
                                    <i class="ri-team-fill"></i>
                                    <span>{{ get_page('referral', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Bonus'))
                            <li>
                                <a href="{{ route('user.bonus') }}" class="waves-effect">
                                    <i class="ri-money-dollar-circle-fill"></i>
                                    <span>{{ ___('Bonus') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Cooperativa Publicitaria'))
                            <li>
                                <a href="{{ route('user.cooperativa') }}" class="waves-effect">
                                    <i class="ri-user-voice-fill"></i>
                                    <span>SuperAds</span>
                                </a>
                            </li>
                        @endif
                        @if (showPage('F101'))
                            <li>
                                <a href="{{ route('user.f101') }}" class="waves-effect">
                                    <i class="mdi mdi-racing-helmet"></i>
                                    <span>{{ ___('Fórmula') }} 101</span>
                                </a>
                            </li>
                        @endif
                        @if (showPage('Top Referrals'))
                            <li>
                                <a href="{{ route('user.top-referrals') }}" class="waves-effect">
                                    <i class="fa fa-trophy"></i>
                                    <span>Top {{ ___('referidos') }}</span>
                                </a>
                            </li>
                        @endif
                        @if (showPage('Maps'))
                            <li>
                                <a href="{{ route('user.maps') }}" class="waves-effect">
                                    <i class="ri-map-pin-fill"></i>
                                    <span>{{ ___('Mapas') }}</span>
                                </a>
                            </li>
                        @endif

                        <li>
                            <a href="{{ route('disruptive.user.profile', 1) }}" class="waves-effect">
                                <i class=" ri-account-circle-fill"></i>
                                <span>{{ ___('My Profile') }}</span>
                            </a>
                        </li>

                        @if (getAllPages()->count() > 0)
                            <li class="menu-title">{{ ___('More') }}</li>
                        @endif

                        @foreach (getAllPages() as $page)
                            @if ($page->status == 'active')
                                <li>
                                    <a href="{{ route('public.pages', $page->slug) }}" class="waves-effect">
                                        <i class="fa {{ $page->icon }}"></i>
                                        <span>{{ $page->menu_title }} </span>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        @php
                            $has_sidebar = isset($has_sidebar) ? $has_sidebar : false;
                            $col_side_cls = $has_sidebar ? 'col-lg-4' : 'col-lg-12';
                            $col_cont_cls = $has_sidebar ? 'col-lg-8' : 'col-lg-12';
                            $col_cont_cls2 = isset($content_class) ? css_class($content_class) : null;
                            $col_side_cls2 = isset($aside_class) ? css_class($aside_class) : null;
                        @endphp
                        <div class="{{ empty($col_cont_cls2) ? $col_cont_cls : $col_cont_cls2 }}">
                            @include('partials.messages-admin')
                            @yield('content')
                        </div>
                        @if ($has_sidebar == true)
                            <div
                                class="aside sidebar-right {{ empty($col_side_cls2) ? $col_side_cls : $col_side_cls2 }}">
                                @if (!has_wallet() && gws('token_wallet_req') == 1 && !empty(token_wallet()))
                                    <div class="d-none d-lg-block">
                                        @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                                            <a href="{{ route('user.account', 1) }}"
                                                class="btn btn-danger btn-xl btn-between w-100 mgb-1-5x"
                                                style="border-radius: 15px;">
                                                {{ __('Billetera (USDT) aún no registrada') }}
                                                <em class="ti ti-arrow-right"></em></a>
                                            <div class="gaps-1x mgb-0-5x d-lg-none d-none d-sm-block"></div>
                                        @endif
                                    </div>
                                @endif
                                <div class="account-info card">
                                    <div class="card-innr">
                                        {!! UserPanel::user_account_status_usuario() !!}
                                        @if (!empty(token_wallet()))
                                            <div class="gaps-2-5x"></div>
                                            @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                                                <a href="{{ route('user.account', 1) }}"
                                                    class="btn btn-danger btn-xl btn-between w-100 mgb-1-5x"
                                                    style="border-radius: 15px;">
                                                    {{ __('Billetera (USDT) aún no registrada') }}
                                                    <em class="ti ti-arrow-right"></em></a>
                                                <div class="gaps-1x mgb-0-5x d-lg-none d-none d-sm-block"></div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                {!! !is_page(get_slug('referral')) ? UserPanel::user_referral_info('') : '' !!}
                                @if (!is_kyc_hide())
                                    {!! UserPanel::user_kyc_info_usuario('') !!}
                                @endif
                            </div>{{-- .col --}}
                        @else
                            @stack('sidebar')
                        @endif

                    </div>

                </div> <!-- container-fluid -->


            </div>
            <!-- End Page-content -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->


            <footer class="mt-3">
                <div id="footer" class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="copyright-text mb-1 text-center">{{ ___('Síguenos en:') }}</p>
                                <div class="container-redes-footer m-0">
                                    <a href="#">
                                        <img loading="lazy" class="image-redes-footer"
                                            src="{{ asset('images/socials/facebook-footer.png') }}">
                                    </a>
                                    <a href="#">
                                        <img loading="lazy" class="image-redes-footer"
                                            src="{{ asset('images/socials/twitter-footer.png') }}">
                                    </a>
                                    <a href="#">
                                        <img loading="lazy" class="image-redes-footer"
                                            src="{{ asset('images/socials/instagram-footer.png') }}">
                                    </a>
                                    <a href="#">
                                        <img loading="lazy" class="image-redes-footer"
                                            src="{{ asset('images/socials/youtube-footer.png') }}">
                                    </a>
                                    <a href="#">
                                        <img loading="lazy" class="image-redes-footer"
                                            src="{{ asset('images/socials/telegram-footer.png') }}">
                                    </a>
                                </div>
                                <p class="copyright-text text-center mt-1">
                                    © {{ date('Y') }} {{ ucfirst(env('APP_NAME')) }}.
                                    {{ ___(ucwords('All Right Reserved')) }}.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    @yield('modals')
    @stack('modals')
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdropLabel" aria-hidden="true" style="backdrop-filter: blur(3px);">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
            <div class="modal-content p-3" style="background: #363a3f94;opacity: 1.9;">
                <div class="modal-body">
                    <div style="display: grid; place-items:center">
                        <h3 class="text-center text-white" style="font-size: 50px;" id="nombreRegalo">
                        </h3>
                    </div>
                    <div class="text-center" id="icono">
                    </div>
                    <strong>
                        <h1 class="text-center text-white" style="font-size: 50px;">
                            {{ ___('Congratulations') }}
                        </h1>
                    </strong>
                    <div class="text-center ">
                        <h5 class="text-white">
                            ¡{{ ___('Ganaste') }} <strong id="regalo-tokens"></strong>
                            {{ ___('por tus nuevos asociados') }}!
                        </h5>
                    </div>
                    <div class="text-center">
                        <h5 class="text-white">
                            {{ ___('Obten más') }} tokens {{ ___('invitando a tus amigos') }}.
                        </h5>
                    </div>
                    <div class="" style="display:flex; align-items:center; justify-content:center;">
                        <button type="button" class="btn btn-primary mt-3" data-bs-dismiss="modal"
                            id="cerrar-modal">{{ ___('Continuar') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ajax-modal"></div>
    <div id="ajax-modal-nft"></div>
    <div class="page-overlay">
        <div class="spinner"><span class="sp sp1"></span>
            <span class="sp sp2"></span><span class="sp sp3"></span>
        </div>
    </div>
    @livewire('evento-notificacion-component')
    @if (gws('theme_custom'))
        <link preload rel="stylesheet" href="{{ asset(style_theme('custom')) }}">
    @endif

    <script>
        var base_url = "{{ url('/') }}",
            {!! has_route('transfer:user.send') ? 'user_token_send = "' . route('transfer:user.send') . '",' : '' !!}
        {!! has_route('withdraw:user.request') ? 'user_token_withdraw = "' . route('withdraw:user.request') . '",' : '' !!}
        {!! has_route('user.ajax.account.wallet')
            ? 'user_wallet_address = "' . route('user.ajax.account.wallet') . '",'
            : '' !!}
        csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        var msg_wrong = "{{ ___('Something is Wrong!') }}",
            msg_cancel_order = "{{ ___('Do you really cancel your order?') }}",
            msg_unable_process = "{{ ___('Unable process request!') }}",
            msg_sure = "{{ ___('Are you sure?') }}",
            msg_unable_perform = "{{ ___('Unable to perform!') }}",
            msg_use_modern_browser = "{{ ___('Please use a modern browser to properly view this template!') }}",
            num_fmt = {{ gws('token_number_format', 0) ? 'true' : 'false' }};
    </script>

    <script src="{{ asset('assets/js/jquery.bundle.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/script.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/app.js') . css_js_ver() }}"></script>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script defer src="https://malsup.github.io/jquery.form.js"></script>

    <!-- JAVASCRIPT-->


    <script src="{{ asset('assets/app/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script defer src="{{ asset('assets/app/libs/metismenu/metisMenu.min.js') }}"></script>
    <script defer src="{{ asset('assets/app/libs/simplebar/simplebar.min.js') }}"></script>
    <script defer src="{{ asset('assets/app/libs/node-waves/waves.min.js') }}"></script>

    <!-- Plugins js -->
    <script defer src="{{ asset('assets/app/libs/dropzone/min/dropzone.min.js') }}"></script>
    <script defer src="{{ asset('assets/app/js/app.js') }}"></script>
    <script defer src="{{ asset('assets/app/js/user.js') }}"></script>



    <!-- image crop -->
    <script defer src="{{ asset('assets/app/libs/ijaboCropTool/ijaboCropTool.min.js') }}"></script>
    <!--/ JAVASCRIPT -->
    <script>
        function loadDoc() {
            const xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                document.getElementById("app").innerHTML = this.responseText;
            }
            @production
            xhttp.open("GET", "/user/cambiarSidebar");
        @endproduction
        @env('local')
            xhttp.open("GET", "/disruptive/user/cambiarSidebar");
        @endenv
        xhttp.send();
        }
    </script>
    <script>
        function show() {
            $('#svg1').removeClass("show");
            $('#svg1').addClass("hide");
            $('#svg2').addClass("show");
            $('#svg2').removeClass("hide");
            const myTimeout = setTimeout(mostrar, 2000);
        }

        function mostrar() {
            $('#svg1').addClass("show");
            $('#svg1').removeClass("hide");
            $('#svg2').removeClass("show");
            $('#svg2').addClass("hide");
        }
    </script>

    @stack('footer')
    <script type="text/javascript">
        @if (session('resent'))
            show_toast("success", "{{ ___('A fresh verification link has been sent to your email address.') }}");
        @endif
    </script>
    @if (get_setting('site_footer_code', false))
        {{ html_string(get_setting('site_footer_code')) }}
    @endif
    <script>
        $(window).on('load', function() {
            $('#backk').hide();
            $("#logocontainer").hide();
        })
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $("a").click(function() {
                if ($(this).prop('target').includes('blank') == true) {
                    $('#backk').show();
                    $("#logocontainer").show();
                    setTimeout(() => {
                        $('#backk').hide();
                        $("#logocontainer").hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('excel') == true) {
                    $('#backk').show();
                    $("#logocontainer").show();
                    setTimeout(() => {
                        $('#backk').hide();
                        $("#logocontainer").hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('export') == true) {
                    $('#backk').show();
                    $("#logocontainer").show();
                    setTimeout(() => {
                        $('#backk').hide();
                        $("#logocontainer").hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('http') == true) {
                    $('#backk').show();
                    $("#logocontainer").show();
                    setTimeout(() => {
                        $('#backk').hide();
                        $("#logocontainer").hide();
                    }, 4000)
                }
            });
        });
    </script>
    <script>
        // $(window).on('load', function() {
        //     $('#backk').hide();
        //     $('#logocontainer').hide();
        // })
        $(document).ajaxStart(function() {
            // Mostrar el loader
            $('#backk').show();
            $("#logocontainer").show();
        });
        $(document).ajaxComplete(function() {
            // Ocultar el loader
            $('#backk').hide();
            $("#logocontainer").hide();
        });
    </script>
    <script>
        $(document).ready(function() {
            if (screen.width < 1396)
                $('#clock-navbar').addClass('d-none');
            else if (screen.width > 1396)
                $('#clock-navbar').removeClass('d-none');
        });
    </script>
    <script>
        (function() {
            const second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24;

            //I'm adding this section so I don't have to keep updating this pen every year :-)
            //remove this if you don't need it
            let today = new Date(),
                dd = String(today.getDate()).padStart(2, "0"),
                mm = String(today.getMonth() + 1).padStart(2, "0"),
                yyyy = today.getFullYear(),
                nextYear = yyyy + 1,
                dayMonth = "06/27/",
                birthday = dayMonth + yyyy + " " + "12:00:00";
            today = mm + "/" + dd + "/" + yyyy;
            if (today > birthday) {
                birthday = dayMonth + nextYear;
            }
            //end

            const countDown = new Date(birthday).getTime(),
                x = setInterval(function() {
                    const now = new Date().getTime(),
                        distance = countDown - now;

                    var dia = Math.floor(distance / day)
                    var hora = Math.floor((distance % day) / hour);
                    if (hora === 0 && new Date().getHours() >= 12) {
                        hora = 12; // Establece la hora en 12 si ya ha pasado el límite del mediodía
                    }
                    var minuto = Math.floor((distance % hour) / minute)
                    var segundo = Math.floor((distance % minute) / second);

                    $('.days').text(dia);
                    $('.hours').text(hora);
                    $('.minutes').text(minuto);
                    $('.seconds').text(segundo);

                    //do something later when date is reached
                    if (distance < 0) {
                        document.getElementById("headline").innerText = "It's my birthday!";
                        document.getElementById("countdown").style.display = "none";
                        document.getElementById("content").style.display = "block";
                        clearInterval(x);
                    }
                    //seconds
                }, 1);
        })();
    </script>
    <script>
        ! function(a, b) {
            "use strict";

            function c(a) {
                a = a || {};
                for (var b = 1; b < arguments.length; b++) {
                    var c = arguments[b];
                    if (c)
                        for (var d in c) c.hasOwnProperty(d) && ("object" == typeof c[d] ? deepExtend(a[d], c[d]) : a[d] =
                            c[d])
                }
                return a
            }

            function d(d, g) {
                function h() {
                    if (y) {
                        r = b.createElement("canvas"), r.className = "pg-canvas", r.style.display = "block", d.insertBefore(
                            r, d.firstChild), s = r.getContext("2d"), i();
                        for (var c = Math.round(r.width * r.height / g.density), e = 0; c > e; e++) {
                            var f = new n;
                            f.setStackPos(e), z.push(f)
                        }
                        a.addEventListener("resize", function() {
                            k()
                        }, !1), b.addEventListener("mousemove", function(a) {
                            A = a.pageX, B = a.pageY
                        }, !1), D && !C && a.addEventListener("deviceorientation", function() {
                            F = Math.min(Math.max(-event.beta, -30), 30), E = Math.min(Math.max(-event.gamma, -30),
                                30)
                        }, !0), j(), q("onInit")
                    }
                }

                function i() {
                    r.width = d.offsetWidth, r.height = d.offsetHeight, s.fillStyle = g.dotColor, s.strokeStyle = g
                        .lineColor, s.lineWidth = g.lineWidth
                }

                function j() {
                    if (y) {
                        u = a.innerWidth, v = a.innerHeight, s.clearRect(0, 0, r.width, r.height);
                        for (var b = 0; b < z.length; b++) z[b].updatePosition();
                        for (var b = 0; b < z.length; b++) z[b].draw();
                        G || (t = requestAnimationFrame(j))
                    }
                }

                function k() {
                    i();
                    for (var a = d.offsetWidth, b = d.offsetHeight, c = z.length - 1; c >= 0; c--)(z[c].position.x > a || z[
                        c].position.y > b) && z.splice(c, 1);
                    var e = Math.round(r.width * r.height / g.density);
                    if (e > z.length)
                        for (; e > z.length;) {
                            var f = new n;
                            z.push(f)
                        } else e < z.length && z.splice(e);
                    for (c = z.length - 1; c >= 0; c--) z[c].setStackPos(c)
                }

                function l() {
                    G = !0
                }

                function m() {
                    G = !1, j()
                }

                function n() {
                    switch (this.stackPos, this.active = !0, this.layer = Math.ceil(3 * Math.random()), this
                        .parallaxOffsetX = 0, this.parallaxOffsetY = 0, this.position = {
                            x: Math.ceil(Math.random() * r.width),
                            y: Math.ceil(Math.random() * r.height)
                        }, this.speed = {}, g.directionX) {
                        case "left":
                            this.speed.x = +(-g.maxSpeedX + Math.random() * g.maxSpeedX - g.minSpeedX).toFixed(2);
                            break;
                        case "right":
                            this.speed.x = +(Math.random() * g.maxSpeedX + g.minSpeedX).toFixed(2);
                            break;
                        default:
                            this.speed.x = +(-g.maxSpeedX / 2 + Math.random() * g.maxSpeedX).toFixed(2), this.speed.x +=
                                this.speed.x > 0 ? g.minSpeedX : -g.minSpeedX
                    }
                    switch (g.directionY) {
                        case "up":
                            this.speed.y = +(-g.maxSpeedY + Math.random() * g.maxSpeedY - g.minSpeedY).toFixed(2);
                            break;
                        case "down":
                            this.speed.y = +(Math.random() * g.maxSpeedY + g.minSpeedY).toFixed(2);
                            break;
                        default:
                            this.speed.y = +(-g.maxSpeedY / 2 + Math.random() * g.maxSpeedY).toFixed(2), this.speed.x +=
                                this.speed.y > 0 ? g.minSpeedY : -g.minSpeedY
                    }
                }

                function o(a, b) {
                    return b ? void(g[a] = b) : g[a]
                }

                function p() {
                    console.log("destroy"), r.parentNode.removeChild(r), q("onDestroy"), f && f(d).removeData("plugin_" + e)
                }

                function q(a) {
                    void 0 !== g[a] && g[a].call(d)
                }
                var r, s, t, u, v, w, x, y = !!b.createElement("canvas").getContext,
                    z = [],
                    A = 0,
                    B = 0,
                    C = !navigator.userAgent.match(
                        /(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i),
                    D = !!a.DeviceOrientationEvent,
                    E = 0,
                    F = 0,
                    G = !1;
                return g = c({}, a[e].defaults, g), n.prototype.draw = function() {
                    s.beginPath(), s.arc(this.position.x + this.parallaxOffsetX, this.position.y + this.parallaxOffsetY,
                        g.particleRadius / 2, 0, 2 * Math.PI, !0), s.closePath(), s.fill(), s.beginPath();
                    for (var a = z.length - 1; a > this.stackPos; a--) {
                        var b = z[a],
                            c = this.position.x - b.position.x,
                            d = this.position.y - b.position.y,
                            e = Math.sqrt(c * c + d * d).toFixed(2);
                        e < g.proximity && (s.moveTo(this.position.x + this.parallaxOffsetX, this.position.y + this
                            .parallaxOffsetY), g.curvedLines ? s.quadraticCurveTo(Math.max(b.position.x, b
                                .position.x), Math.min(b.position.y, b.position.y), b.position.x + b
                            .parallaxOffsetX, b.position.y + b.parallaxOffsetY) : s.lineTo(b.position.x + b
                            .parallaxOffsetX, b.position.y + b.parallaxOffsetY))
                    }
                    s.stroke(), s.closePath()
                }, n.prototype.updatePosition = function() {
                    if (g.parallax) {
                        if (D && !C) {
                            var a = (u - 0) / 60;
                            w = (E - -30) * a + 0;
                            var b = (v - 0) / 60;
                            x = (F - -30) * b + 0
                        } else w = A, x = B;
                        this.parallaxTargX = (w - u / 2) / (g.parallaxMultiplier * this.layer), this.parallaxOffsetX +=
                            (this.parallaxTargX - this.parallaxOffsetX) / 10, this.parallaxTargY = (x - v / 2) / (g
                                .parallaxMultiplier * this.layer), this.parallaxOffsetY += (this.parallaxTargY - this
                                .parallaxOffsetY) / 10
                    }
                    var c = d.offsetWidth,
                        e = d.offsetHeight;
                    switch (g.directionX) {
                        case "left":
                            this.position.x + this.speed.x + this.parallaxOffsetX < 0 && (this.position.x = c - this
                                .parallaxOffsetX);
                            break;
                        case "right":
                            this.position.x + this.speed.x + this.parallaxOffsetX > c && (this.position.x = 0 - this
                                .parallaxOffsetX);
                            break;
                        default:
                            (this.position.x + this.speed.x + this.parallaxOffsetX > c || this.position.x + this.speed
                                .x + this.parallaxOffsetX < 0) && (this.speed.x = -this.speed.x)
                    }
                    switch (g.directionY) {
                        case "up":
                            this.position.y + this.speed.y + this.parallaxOffsetY < 0 && (this.position.y = e - this
                                .parallaxOffsetY);
                            break;
                        case "down":
                            this.position.y + this.speed.y + this.parallaxOffsetY > e && (this.position.y = 0 - this
                                .parallaxOffsetY);
                            break;
                        default:
                            (this.position.y + this.speed.y + this.parallaxOffsetY > e || this.position.y + this.speed
                                .y + this.parallaxOffsetY < 0) && (this.speed.y = -this.speed.y)
                    }
                    this.position.x += this.speed.x, this.position.y += this.speed.y
                }, n.prototype.setStackPos = function(a) {
                    this.stackPos = a
                }, h(), {
                    option: o,
                    destroy: p,
                    start: m,
                    pause: l
                }
            }
            var e = "particleground",
                f = a.jQuery;
            a[e] = function(a, b) {
                return new d(a, b)
            }, a[e].defaults = {
                minSpeedX: .1,
                maxSpeedX: .7,
                minSpeedY: .1,
                maxSpeedY: .7,
                directionX: "center",
                directionY: "center",
                density: 1e4,
                dotColor: "#666666",
                lineColor: "#666666",
                particleRadius: 7,
                lineWidth: 1,
                curvedLines: !1,
                proximity: 100,
                parallax: !0,
                parallaxMultiplier: 5,
                onInit: function() {},
                onDestroy: function() {}
            }, f && (f.fn[e] = function(a) {
                if ("string" == typeof arguments[0]) {
                    var b, c = arguments[0],
                        g = Array.prototype.slice.call(arguments, 1);
                    return this.each(function() {
                        f.data(this, "plugin_" + e) && "function" == typeof f.data(this, "plugin_" + e)[
                            c] && (b = f.data(this, "plugin_" + e)[c].apply(this, g))
                    }), void 0 !== b ? b : this
                }
                return "object" != typeof a && a ? void 0 : this.each(function() {
                    f.data(this, "plugin_" + e) || f.data(this, "plugin_" + e, new d(this, a))
                })
            })
        }(window, document),
        function() {
            for (var a = 0, b = ["ms", "moz", "webkit", "o"], c = 0; c < b.length && !window.requestAnimationFrame; ++c)
                window.requestAnimationFrame = window[b[c] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[
                    b[c] + "CancelAnimationFrame"] || window[b[c] + "CancelRequestAnimationFrame"];
            window.requestAnimationFrame || (window.requestAnimationFrame = function(b) {
                var c = (new Date).getTime(),
                    d = Math.max(0, 16 - (c - a)),
                    e = window.setTimeout(function() {
                        b(c + d)
                    }, d);
                return a = c + d, e
            }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function(a) {
                clearTimeout(a)
            })
        }();
    </script>
    <script>
        var userId = {{ auth()->user()->id }};
        Echo.channel('gift-user-' + userId).listen('.refresh.gift', (data) => {
            $("#regalo-tokens").html(data.cantidadRegalo + ' ' + 'tokens');
            $("#icono").html('<i class="' + data.icono + ' ' + 'text-primary" style="font-size: 150px;"></i>');
            $("#nombreRegalo").html(data.nombre);
            $('#staticBackdrop').modal({
                backdrop: 'static',
            })
            $('#staticBackdrop').modal('show');
        });
    </script>
    @include('disruptive.includes.ticket-send-user')
    <script>
        // script de copiar enlace de referido desde el layout
        $(".btn-copy-url-referrals-global").click(function() {
            var valorCopiado = $("#url-copy-referrals-global").val();
            navigator.clipboard.writeText(valorCopiado)
                .then(function() {
                    $(".copiado").removeClass('d-none');
                    $(".no-copiado").addClass('d-none');
                    toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                    setTimeout(function() {
                        $(".copiado").addClass('d-none');
                        $(".no-copiado").removeClass('d-none');
                    }, 1000)
                })
                .catch(function(err) {
                    toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                });
        });

        $(".popover-btn").click(function() {
            $('.popover').css('visibility', 'hidden');
            setTimeout(function() {
                $('.popover').css('visibility', 'visible');
            }, 2000);
        });
    </script>
</body>

</html>
