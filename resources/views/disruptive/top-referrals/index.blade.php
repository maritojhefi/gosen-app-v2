@extends('disruptive.disruptive-layout')
@section('title', ___('Top Referral'))
@section('content')
    <div class="container">
        <div class="row mb-5">
            {{-- Perfil --}}
            <div class="col-lg-4 mt-3">
                <div class="text-center">
                    @if (auth()->user()->foto == null)
                        <img loading="lazy" class="rounded-circle shadow-lg shadow-img-top" width="160" height="160"
                            src="{{ asset2('imagenes/user-default.jpg') }}">
                    @else
                        <img loading="lazy" class="rounded-circle shadow-lg shadow-img-top" width="160" height="160"
                            src="{{ asset2('imagenes/perfil') }}/{{ auth()->user()->foto }}">
                    @endif
                    <div class="card shadow mt-5 mb-1 p-2 texto-background-card">
                        <strong>
                            <h3 class="mt-4 mb-0 text-primary">
                                {{ auth()->user()->username == null ? auth()->user()->name : auth()->user()->username }}
                            </h3>
                        </strong>
                        <span class="text-decoration-underline mt-2">{{ auth()->user()->email }}</span>
                        <div class="row mt-4 mb-4">
                            <div class="col-12">
                                <span class="inf-card">
                                    <i class="{{ $ico }}"></i>
                                    {{ ___($regalo) }}
                                </span>
                            </div>
                        </div>
                        <p class="fst-italic">{{ ___('Invite more friends to be among the best') }}!</p>
                    </div>
                </div>
            </div>
            {{-- Top --}}
            <div class="col-lg-8 mt-3">
                <div class="text-center table-responsive top-max-height">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 10%;" scope="col"></th>
                                <th style="width: 80%;" class="text-left fs-4" scope="col">{{ ___('User') }}</th>
                                <th style="width: 10%;" scope="col">{{ ___('Associates') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalArray = count($array);
                                $contador = 1;
                            @endphp
                            @foreach ($array as $item)
                                <tr>
                                    <th>
                                        <div class="text-center mt-4">
                                            @if ($item['foto'] == null)
                                                <img loading="lazy" class="rounded-circle shadow-lg" width="55"
                                                    height="55" src="{{ asset2('imagenes/user-default.jpg') }}">
                                            @else
                                                <img loading="lazy" class="rounded-circle shadow-lg" width="55"
                                                    height="55"
                                                    src="{{ asset2('imagenes/perfil') }}/{{ $item['foto'] }}">
                                            @endif
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        <div class="card shadow-lg p-3 mt-4 mb-0 texto-background-card"
                                            style="border-radius: 10px;">
                                            <table class="m-0" style="width: 100%;">
                                                <tr>
                                                    <th style="width: 10%;"><span>{{ $contador }}</span></th>
                                                    <th style="width: 80%;">
                                                        <p>{{ $item['username'] == null ? $item['nombre'] : $item['username'] }}
                                                        </p>
                                                    </th>
                                                    <th style="width: 10%;"><span>{{ $item['cantidad'] }}</span></th>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                @if ($contador - 1 == $totalArray || $contador == 10)
                                @break
                            @endif
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection