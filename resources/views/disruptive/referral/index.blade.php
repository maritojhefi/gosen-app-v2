@extends('disruptive.disruptive-layout')
@section('title', ___('Referral'))
@push('header')
    <style>
        @media (max-width:580px) {
            .buto {
                width: 100% !important;
            }

            .image-card {
                margin: 5px !important;
            }
        }

        .form-control {
            color: #505258;
            line-height: 20px;
            border: 1px solid rgba(211, 224, 243, 0.5);
        }

        /* cards-del-modal */
        @media only screen and (max-width: 600px) {
            .card-body {
                display: block;
            }
        }

        .card-body {
            background: radial-gradient(circle, rgb(255 255 255) 0%, rgb(0 27 88) 100%);
            display: flex;
        }

        .image-card {
            margin: auto;
            position: relative;
            transition: transform 4s ease-in;
            overflow: hidden;
            border: 4px solid #fff;
        }

        .img-card {
            width: 100%;
            height: 436px;
            transition: transform 1s ease-in;
        }

        .img-card:hover {
            transform: scale(1.2);
        }

        .image-card:before {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            content: "Click Download!";
            transform: translateY(-100%);
            font-size: 25px;
            color: #fff;
            display: block;
            background-color: rgba(188, 143, 143, 0.137);
            z-index: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            /* font-family: sans-serif; */
            transition: transform .4s ease-in;
            border: 4px solid white;
            justify-content: center;
            box-sizing: border-box;
        }

        .image-card:after {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            content: "Click Download!";
            transform: translateY(100%);
            font-size: 25px;
            color: #fff;
            display: block;
            background-color: rgba(188, 143, 143, 0.137);
            z-index: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            /* font-family: sans-serif; */
            transition: transform .4s ease-in;
            border: 4px solid white;
            justify-content: center;
            box-sizing: border-box;
        }

        .image-card:hover::before {
            transform: translateY(0%);
            transform: translateY(0%);
            transform: translateY(0%);

        }

        .image-card:hover::after {
            transform: translateY(0%);

        }

        @media (max-width: 576px) {
            .img-card {
                height: 520px;
            }
        }
    </style>
@endpush
@section('content')
    @php
        $user = empty($data) ? auth()->user() : $data;
        $idioma = $user->lang;
    @endphp
    <div class="content-area card">
        <div class="card-innr">
            <div class="card-head">
                <h2 class="card-title card-title-lg">{{ __('Invitaciones') }}</h2>
                @if ($page->meta_description != null)
                    <p class="large">{{ replace_shortcode(__($page->meta_description)) }}</p>
                @endif
            </div>
            @if (!empty($page->description))
                <div class="card-text">
                    {{ ___('Invita a tus amigos y familiares y recibe tokens gratis. El enlace de referencia se puede utilizar durante una contribución simbólica, en la preventa y en la ICO. Imagine dar su enlace de referencia único a su amigo criptográfico y él o ella contribuye con tokens usando su enlace, el bono se enviará a su cuenta automáticamente. La estrategia es simple, cuantos más enlaces envíe a sus colegas, familiares y amigos, ¡más fichas podrá ganar') }}!
                </div>
            @endif
            <div class="gaps-1x"></div>
            <div class="referral-form">
                <h4 class="card-title card-title-sm">{{ ___('URL de invitado') }}</h4>
                <div class="copy-wrap mgb-1-5x mgt-1- buto" style="width:50%;">
                    <span class="copy-feedback"></span>
                    <em class="copy-icon fas fa-link"></em>
                    <input id="input-copy-url-view" type="text" class="copy-address"
                        value="{{ ___('Copia y comparte tu URL') }}" disabled>
                    <input type="text" id="url-copy-referrals-view"
                        value="{{ route('public.referral') . '?ref=' . substr(set_id(auth()->user()->username), 2) . '&lang=' . $idioma }}"
                        hidden="hidden">
                    <button class="copy-trigger copy-clipboard btn-url-copy-referrals-view">
                        <em class="ti ti-files"></em>
                    </button>
                </div>
                <p class="mgmt-1x">
                    <em><small>{{ ___('Use el enlace de arriba para referir a su amigo y obtener una bonificación por recomendación.') }}</small></em>
                </p>
            </div>
            @include('user.includes.landing-carrusel')
            @if ($referido != null)
                <div class="col-lg-6">
                    <div class="content-area card stage-card">
                        <div class="card-innr" style="">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="text-primary">
                                        <strong>
                                            <i class="mdi mdi-account-group"></i>
                                            {{ ___('Te invito') }} :
                                        </strong>
                                    </h2>
                                    <h5 class="my-0 pb-2 text-primary col-sm-12 col-xs-12">{{ $referido->name }} &nbsp;
                                        @if (auth()->user()->email_verified_at != null)
                                            <small><i
                                                    class="mdi mdi-check-decagram text-success">{{ ___('Verified User') }}</i>&nbsp;</small>
                                        @endif
                                    </h5>
                                    <small class="my-0 col-sm-12 col-xs-12"
                                        style="font-style:italic">({{ $referido->username }})</small>
                                </div>
                                <div class="col-lg-6">
                                    @if ($referido->foto == null)
                                        <img loading="lazy" class="example-image"
                                            src="{{ asset2('imagenes/user-default.jpg') }}"
                                            style="width:48px; height:48px; float:right; border-radius:50%; margin-right:3px;">
                                    @else
                                        <img loading="lazy" class=""
                                            src="{{ asset2('imagenes/perfil/' . $referido->foto) }}"
                                            style="width; height:50%; float:right; border-radius:50%; margin-right:3px;">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            {{-- @include('user.includes.list-levels-of-type-user') --}}
        </div>
    </div>
@endsection



@push('footer')
    <script>
        var target;
        $(function() {
            $("ul#land li").on({
                mouseenter: function() {
                    var el = $(this);
                    var img = el.attr("data-img");

                    target = $("#boxImage");
                    target.find('img').attr("src", "{{ asset2('images/landing') }}" + img);
                    target.show();
                },
                mouseleave: function() {
                    target.hide();
                    target.find("img").removeAttr("src");
                    target = $("#boxImage");
                    target.find('img').attr("src", "{{ asset2('images/landing/Landing-Page.svg') }}");
                    target.show();
                }
            })
            target = $("#boxImage");
            target.find('img').attr("src", "{{ asset2('images/landing/Landing-Page.svg') }}");
            target.show();
        });
    </script>
@endpush

{{-- banners con swiper --}}
@push('modals')
    <!-- Modal -->
    @for ($i = 1; $i <= 6; $i++)
        <div class="modal fade modal-carrucel-tarjetas" id="showBanner{{ $i }}" data-bs-backdrop="static"
            data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="background-color: rgba(255, 255, 255, 0.6);">
                    <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                            class="ti ti-close"></em></a>
                    <div class="row">
                        <div class="col-12">
                            <div class="swiper-tarjeta mySwiper-tarjetas">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-12">
                                                <img loading="lazy"
                                                    src="{{ asset('images/banners/' . auth()->user()->lang . '/1.png') }}" />
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid gap-2">
                                                    <a class="btn btn-primary btn-descargar-imagen"
                                                        href="{{ route('download.banner', ['d' . $i, 1]) }}">{{ ___('Descargar') }}
                                                        <i class="fas fa-download"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-12">
                                                <img loading="lazy"
                                                    src="{{ asset('images/banners/' . auth()->user()->lang . '/2.png') }}" />
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid gap-2">
                                                    <a class="btn btn-primary btn-descargar-imagen"
                                                        href="{{ route('download.banner', ['d' . $i, 2]) }}">{{ ___('Descargar') }}
                                                        <i class="fas fa-download"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-12">
                                                <img loading="lazy"
                                                    src="{{ asset('images/banners/' . auth()->user()->lang . '/3.png') }}" />
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid gap-2">
                                                    <a class="btn btn-primary btn-descargar-imagen"
                                                        href="{{ route('download.banner', ['d' . $i, 3]) }}">{{ ___('Descargar') }}
                                                        <i class="fas fa-download"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-12">
                                                <img loading="lazy"
                                                    src="{{ asset('images/banners/' . auth()->user()->lang . '/4.png') }}" />
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid gap-2">
                                                    <a class="btn btn-primary btn-descargar-imagen"
                                                        href="{{ route('download.banner', ['d' . $i, 4]) }}">{{ ___('Descargar') }}
                                                        <i class="fas fa-download"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-12">
                                                <img loading="lazy"
                                                    src="{{ asset('images/banners/' . auth()->user()->lang . '/5.png') }}" />
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid gap-2">
                                                    <a class="btn btn-primary btn-descargar-imagen"
                                                        href="{{ route('download.banner', ['d' . $i, 5]) }}">{{ ___('Descargar') }}
                                                        <i class="fas fa-download"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endfor
@endpush

@push('header')
    <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <style>
        .swiper-tarjeta {
            width: 100%;
            padding-top: 50px;
            padding-bottom: 50px;
            overflow: hidden;
        }

        .swiper-tarjeta .swiper-slide {
            background-position: center;
            background-size: cover;
            width: 250px;
            min-height: 200px;
            border-radius: 10px 10px 10px 10px;
        }

        .swiper-tarjeta .swiper-slide img {
            display: block;
            width: 100%;
            border-radius: 10px 10px 0px 0px;
        }

        .btn-descargar-imagen {
            border-radius: 0px 0px 10px 10px;
        }

        .swiper-pagination.swiper-pagination-progressbar.swiper-pagination-horizontal span {
            background-color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }

        .swiper-button-prev::after {
            color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }

        .swiper-button-next::after {
            color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }

        .swiper-pagination-bullet.swiper-pagination-bullet-active {
            background-color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }
    </style>
@endpush

@push('footer')
    <!-- Swiper JS -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiperTarjeta = null;

        $(document).ready(function() {
            cargarSwiper();
        });

        $('.modal-carrucel-tarjetas').on('shown.bs.modal', function() {
            swiperTarjeta = new Swiper(".mySwiper-tarjetas", {
                effect: "coverflow",
                initialSlide: 2,
                grabCursor: true,
                centeredSlides: true,
                slidesPerView: "auto",
                coverflowEffect: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true,
                }
            });
        });

        function cargarSwiper() {
            swiperTarjeta = new Swiper(".mySwiper-tarjetas", {
                effect: "coverflow",
                initialSlide: 2,
                grabCursor: true,
                centeredSlides: true,
                slidesPerView: "auto",
                coverflowEffect: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true,
                },
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true
                },
            });
        }

        $(".btn-url-copy-referrals-view").click(function() {
            var valorCopiado = $("#url-copy-referrals-view").val();
            navigator.clipboard.writeText(valorCopiado)
                .then(function() {
                    $("#input-copy-url-view").addClass('copiado');
                    $("#input-copy-url-view").val("{{ ___('Copied to Clipboard') }}");
                    toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                    setTimeout(function() {
                        $("#input-copy-url-view").removeClass('copiado');
                        $("#input-copy-url-view").val("{{ ___('Copia y comparte tu URL') }}");
                    }, 1000)
                })
                .catch(function(err) {
                    toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                });
        });
    </script>
@endpush
