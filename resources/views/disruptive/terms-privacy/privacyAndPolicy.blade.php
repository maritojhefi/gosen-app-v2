@extends('disruptive.layoutTermsAndPrivacy')
@section('content')
    @php
        $token_name = strtoupper(token('name'));
    @endphp
    <div class="page-header page-header-kyc">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-xl-7 text-center">
                <h2 class="page-title">{{ ___('Privacy and Policy') }}</h2>
            </div>
        </div>
        <div class="row justify-content-right" style="float: right">
            <div class="text-right">
                <a href="{{ route('register') }}" style="background:transparent;"
                class="btn btn-sm btn-auto btn-primary d-sm-inline-block d-none text-primary"><em
                    class="fas fa-arrow-left"></em><span>{{ ___('Back') }}</span></a>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-10 col-xl-9">
            <div class="card content-area">
                <div class="card-innr">
                    <div class="card-text">
                        <h4 class="text-center">
                            <b>
                                {{ ___(strtoupper('Privacidad y política')) }}
                            </b>
                        </h4>

                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('POLÍTICA DE COOKIES DE')) }} {{ $token_name }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El contenido de las políticas de cookies que se incorpora, viene a describir los diferentes tipos de almacenamiento de datos que pueden ser inyectados para luego ser aplicados por disruptive.center como alojador del proyecto') }}
                            {{ $token_name }}
                            {{ ___('o por organizaciones de terceros en este sitio web o en nuestra aplicación móvil requiera para su mejor funcionamiento') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Si tiene preguntas sobre ésta política de cookies, por favor comuníquese con disruptive.center') }}.
                        </p>



                        <h5 class="text-left">
                            1. ¿{{ ___('Qué debemos entender por') }} "cookie"?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('son diminutos archivos de texto que los sitios web alojan en sus servidores y que a su vez, extraen de los equipos de los usuarios que visitan dichos sitios. Es un enlazamiento o interacción de datos que ocurre entre los desarrolladores de las páginas que confieren el servicio y los equipos de sus usuarios. Se utilizan para que los sitios web y usuarios reciban retroalimentación fluida y para dirigir con mayor eficiencia la información entre ambos, según intereses puntuales') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('tienen varias funciones, como permitirle navegar de manera eficiente en un sitio web o una aplicación móvil, recordar sus elecciones y los bienes y servicios que compró. Gracias a las cookies, los sistemas informáticos pueden') }}:
                        </p>
                        <ul class="pl-4 pb-3">
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Conocer los intereses de los usuarios') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Ofrecer contenido específico y personalizado al usuario (por ejemplo, para que éstos puedan recordar la elección de idioma que el usuario hizo en el sitio, o para mostrarle anuncios que se adaptan a sus necesidades e intereses)') }}.
                                </li>
                            </p>
                        </ul>
                        <p class="text-justify">
                            {{ ___('Cuando hablamos de') }} cookies,
                            {{ ___('también nos referimos a tecnologías similares a las') }} cookies,
                            {{ ___('como secuencias de comandos, balizas web, píxeles, huellas dactilares y redireccionamientos. Los ') }}
                            scripts
                            ({{ ___('a veces llamados etiquetas') }})
                            {{ ___('son programas que se ejecutan en su navegador o aplicación móvil y realizan varias acciones, como enviar información a nuestros servidores') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('pueden ser implementadas por nosotros y también por organizaciones de terceros') }}.
                        </p>



                        <h5 class="text-left">
                            2. ¿{{ ___('Qué datos recopilamos gracias a las') }} "cookies"?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('permiten recopilar datos personales relacionados con usted y transmitirnos información técnica sobre los dispositivos que utiliza, los clics que realiza y el contenido que ve. Combinamos esta información con datos personales que ya nos ha proporcionado o que hemos obtenido de fuentes externas (consulte nuestra Política de privacidad aquí)') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('que colocamos en su terminal nos dan acceso a los siguientes datos personales relacionados con usted') }}:
                        </p>
                        <ul class="pl-4 pb-3">
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Identificadores del equipo que utiliza (dirección IP de su computadora, identificador de Android, identificador de Apple, etc)') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Tipo de sistema operativo que utiliza su terminal (Microsoft Windows, Apple Os, Linux, Unix, etc)') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Tipo y versión del software de navegación utilizado por su terminal (Microsoft Internet Explorer, Apple Safari, Mozilla Firefox, Google Chrome, etc)') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Fechas y horas de conexión a nuestros servicios') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Dirección de la página de Internet de origen') }} ("{{ ___('remitente') }}").
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Datos de navegación en nuestros servicios, contenido visto') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Información relacionada con su uso de nuestros sitios y aplicaciones') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Información relativa a la presencia de') }} cookies
                                    {{ ___('en su terminal, la hora y fecha de visualización; una página y una descripción de la página donde se coloca la baliza web') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Información sobre si lee o no los correos electrónicos que le enviamos, sobre los clics que realiza en los enlaces contenidos en estos correos electrónicos') }}.
                                </li>
                            </p>
                        </ul>



                        <h5 class="text-left">
                            3. ¿{{ ___('Son las') }} cookies
                            {{ ___('absolutamente esenciales para que nuestro sitio/aplicación móvil funcione correctamente') }}
                            ?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Algunas') }} cookies
                            {{ ___('son necesarias para que nuestro sitio funcione') }} (Cookies
                            {{ ___('esenciales') }}").
                            {{ ___('Sin embargo, para otros, su consentimiento se recopila antes de que las cookies se implementen en su terminal') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Las') }} cookies
                            {{ ___('esenciales permiten que nuestro sitio web/aplicaciones móviles funcionen. Sin ellos, su experiencia de usuario se vería disminuida y quizá no permitiría correr en sistemas. Las') }}
                            Cookies {{ ___('Esenciales que implementamos sin su consentimiento son las siguientes') }}:
                        </p>
                        <ul class="pl-4 pb-3">
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('que registran la elección que ha expresado sobre la implementación de') }}cookies.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('utilizadas con fines de autenticación y para garantizar la seguridad de nuestro mecanismo de autenticación') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('utilizadas para la personalización de nuestra interfaz de usuario (elección de idioma o presentación de nuestro servicio), cuando dicha personalización es esencial para brindarle nuestro servicio') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('que le permiten iniciar sesión automáticamente en su cuenta, utilizando identificadores o datos que nos ha confiado previamente') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('que nos permiten asegurarnos de que la visualización de nuestro contenido sea adecuada para su dispositivo') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies {{ ___('para recordar su suscripción a nuestros boletines') }}.
                                </li>
                            </p>
                        </ul>



                        <h5 class="text-left">
                            4. ¿{{ ___('Por qué usamos') }} cookies
                            {{ ___('no esenciales') }}
                            ?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Algunas') }} cookies
                            {{ ___('para las cuales recopilamos su consentimiento, se utilizan para medir nuestra audiencia, presentarle publicidad personalizada, adaptar el contenido que se muestra en nuestro sitio web/aplicación a sus preferencias y permitirle compartir el contenido de nuestro sitio web/aplicación móvil en las plataformas de redes sociales. Utilizamos') }}
                            cookies
                            {{ ___('no esenciales para lo siguiente:') }}:
                        </p>
                        <ul class="pl-4 pb-3">
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Optimización de su navegación en nuestro sitio web y aplicaciones') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Registro de información relativa a un formulario que haya rellenado en nuestro sitio (registro o acceso a su cuenta) o a productos, servicios o información que haya elegido en nuestro sitio (servicio suscrito, contenido visto, compra realizada, etc.)') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies {{ ___('de prueba A/B, es decir') }}, cookies
                                    {{ ___(' que nos permiten construir estadísticas de tráfico y pruebas destinadas a medir el rendimiento relativo de diferentes versiones de nuestro sitio web, detectar problemas de navegación en nuestro sitio o aplicación, o incluso organizar el contenido') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Gestión de nuestras plataformas y realización de operaciones técnicas internas como parte de la resolución de problemas, análisis de datos, pruebas, investigación, análisis, estudios y encuestas') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Cookies
                                    {{ ___('que nos permiten mejorar las medidas de seguridad de nuestro sitio web') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Medición de audiencia y analítica web') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Establecimiento de estadísticas y volúmenes de visitas y uso de los distintos elementos que componen nuestro sitio (secciones y contenidos visitados, recorridos) con el fin de mejorar el interés y la ergonomía de nuestros servicios') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Cálculo del número total de anuncios que mostramos en nuestros espacios publicitarios, clasificación y estadísticas') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Análisis de audiencias en base a los resultados de campañas publicitarias') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Determinación de intereses y comportamientos') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Mejora del conocimiento del cliente/usuario con fines de personalización') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Compartir sus datos personales en plataformas de redes sociales') }}.
                                </li>
                            </p>
                        </ul>
                        <p class="text-justify">
                            {{ ___('El uso de redes sociales para interactuar con nuestros sitios y aplicaciones') }} (
                            {{ ___('en particular, los botones') }} "{{ ___('me gusta') }}"
                            {{ ___('en Facebook, Twitter') }})
                            {{ ___('le permite compartir contenido de nuestro sitio web en plataformas de redes sociales. Por ejemplo, si está conectado a la red social Facebook y visita una página de nuestro sitio, es probable que Facebook recopile esta información. Asimismo, si consulta un artículo en nuestro sitio web y hace clic en el botón') }}
                            "{{ ___('twittear') }}",
                            {{ ___('Twitter recopilará esta información. Por lo tanto, lo invitamos a consultar las políticas de privacidad de cualquiera de dichas redes sociales para conocer las recopilaciones y el procesamiento que realizan sobre sus datos') }}.
                        </p>



                        <h5 class="text-left">
                            5. ¿{{ ___('Utilizamos') }} cookies
                            {{ ___('de terceros') }}
                            ?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Sí, algunas') }} cookies
                            {{ ___('son establecidas por nuestros proveedores de servicios que nos ayudan a lograr nuestros objetivos') }}
                            ({{ ___('por ejemplo, Google Tag Manager establece una') }}
                            cookies
                            {{ ___('para ayudarnos a medir nuestra audiencia') }}).
                            {{ ___('A continuación, se muestra la lista actual de terceros que implementan') }} cookies
                            {{ ___('en su terminal a través de nuestro sitio web/aplicación móvil') }}.
                        </p>
                        <ul class="pl-4 pb-3">
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Nombre de la galleta') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Objetivo') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Vencimiento') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Amperio') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Análisis de usuarios del sitio web realizados por amplitud de 24 horas') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Análisis de usuarios del sitio web realizados por') }} CrazyEgg.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Detección de bots en la web realizada por') }} CloudFlare
                                    {{ ___('cada 30 días') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    Tofycoincom-locale.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Definición y mantenimiento del idioma del sitio web preferido por el usuario 1 año') }}.
                                </li>
                            </p>
                            <p class="text-justify">
                                <li style="list-style-type: disc;">
                                    {{ ___('Identificación de usuarios únicos realizada por') }} Google Analytics.
                                </li>
                            </p>
                        </ul>
                        <p class="text-justify">
                            {{ ___('Estas son') }} cookies
                            {{ ___('de rendimiento que utilizamos para recopilar información sobre cómo nuestros visitantes usan el sitio web a través de') }}
                            Google Analytics.
                        </p>
                        <p class="text-justify">
                            {{ ___('La') }} cookie {{ $token_name }}
                            {{ ___('contiene una identificación única que Google usa para recordar sus preferencias y otra información, como su idioma preferido') }}.
                        </p>



                        <h5 class="text-left">
                            6. ¿{{ ___('Cómo recabamos su consentimiento') }}?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Se le pedirá que realice una acción afirmativa clara para que podamos asegurarnos de que está de acuerdo con el procesamiento de sus datos personales. Esta acción puede, por ejemplo, tomar la forma de una casilla de verificación en su herramienta en línea. Antes de obtener su consentimiento, le informaremos sistemáticamente de los nombres de las organizaciones que implementan las') }}
                            cookies,
                            {{ ___('las consecuencias de su consentimiento o rechazo, los propósitos del procesamiento y su derecho a retirar su consentimiento. Puede optar por aceptar ciertos usos que nosotros o nuestros socios haremos de sus datos y rechazar otros') }}.
                        </p>



                        <h5 class="text-left">
                            7. ¿{{ ___('Qué pasa si cambio de opinión') }}?
                        </h5>
                        <p class="text-justify">
                            {{ ___('Puede retirar su consentimiento en cualquier momento y de forma sencilla. Todo lo que necesita hacer es enviarnos un correo electrónico a') }}
                            <a href="#">support@dirruptive.com</a>
                            {{ ___('Si se niega a darnos su consentimiento para las') }}
                            cookies
                            {{ ___('no esenciales, o decide retirarlo, no sufrirá ninguna consecuencia negativa y podrá seguir utilizando el resto de nuestro servicio con normalidad') }}.
                        </p>


                    </div>
                </div>
            </div>{{-- .card --}}
            <div class="gaps-1x"></div>
            <div class="gaps-3x d-none d-sm-block"></div>
        </div>
    </div>
@endsection
