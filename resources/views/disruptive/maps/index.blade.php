@extends('disruptive.disruptive-layout')
@section('title', ___('Maps'))
@section('gosencss')
    @include('user.includes.dashboard-css')
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <x-dis.mapa-usuarios-component />
            </div>

            @livewire('user.contadores-component')
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">
                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Países más populares') }}</h4>
                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @php
                                                            $contador = 1;
                                                            @endphp
                                                            @foreach ($paises as $pais)
                                                                <li
                                                                    class="list-group-item d-flex justify-content-between align-items-center bg-theme-user" style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $pais->flag }}" alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $pais->name }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-4 text-end">
                                                                <span>{{$pais->users}}</span>
                                                            </div> --}}
                                                                </li>
                                                                @php
                                                                $contador++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Top Red de Asociados') }}</h4>

                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            {{-- {{dd($misasociados)}} --}}
                                                            @php
                                                            $contador2 = 1;
                                                            @endphp
                                                            @foreach ($misasociados as $referral)
                                                                <li
                                                                    class="list-group-item d-flex justify-content-between align-items-center bg-theme-user" style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador2 }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        @if ($referral['foto'] == null || $referral['foto'] == '')
                                                                            <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}"
                                                                                style="border-radius: 50%;width: 28px;height: 28px;">
                                                                        @else
                                                                            <img loading="lazy" style="border-radius: 50%;width: 28px;height: 28px;"
                                                                                src="{{ asset2('imagenes/perfil/' . $referral['foto']) }}">
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $referral['asociado'] }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-1 text-end">
                                                                <span>{{$referral['canrefer']}}</span>
                                                            </div> --}}
                                                                </li>
                                                                @php
                                                                $contador2++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">
                                    {{ ___('Países más populares de asociados') }}</h4>

                            <div data-simplebar="init" style="height: 296px;">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">
                                                    <ul class="list-unstyled activity-wid">
                                                        @php
                                                        $contador3 = 1;
                                                        @endphp
                                                        @foreach ($asociadosPorPais as $nombrePais=>$array)
                                                        <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user" style="border:1px transparent">
                                                            <div class="col-2 p-0">
                                                                <small># {{ $contador3 }}</small>
                                                            </div>
                                                            <div class="col-2 p-0 text-center">
                                                                <img loading="lazy" src="{{$banderas[$contador3 - 1]}}" alt="" style="border-radius: 50%;width: 28px;height: 28px;">
                                                            </div>
                                                            <div class="col-4 p-0">
                                                                {{$nombrePais}}
                                                            </div>
                                                            <div class="col-4 text-end">
                                                                <span>{{array_sum($array)}}</span>
                                                            </div>
                                                        </li>
                                                        @php
                                                        $contador3++;
                                                        @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card card-full-height p-2" style="align-items: center;">
                    <h4 class="card-title" style=" padding-top: 14px;">{{ ___('Red de asociados Global') }}</h4>
                    <div class="card-innr pb-0" style="width: 100%;">
                        <div class="row">
                            <div class="col-1 p-0">
                                <div class="card-title pb-2"
                                    style="transform: rotate(-90deg);position: absolute; top: 150px;">
                                    {{ ___('Asociados') }}</div>
                            </div>
                            <div class="col-10 p-0">
                                {!! $usersChart->container() !!}
                            </div>
                            <div class="col-1 p-0">
                                <div class="float-right position-relative">
                                    <a href="#"
                                        class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                        <em class="fa fa-sliders" style="font-size:20px; cursor:pointer;"
                                            data-toggle="tooltip" data-placement="left"
                                            data-original-title="Filters"></em> </a>
                                    <div
                                        class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                        <div id="chard">
                                            <ul class="dropdown-list dropdown-list-s2">
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="dia"
                                                        name="chard" type="radio" value="dia"
                                                        {{ $tipoGrafica == 'dia' ? 'checked' : '' }}>
                                                    <label for="dia">{{ ___('Última semana') }}</label>
                                                </li>
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="semana"
                                                        name="chard" type="radio" value="semana"
                                                        {{ $tipoGrafica == 'semana' ? 'checked' : '' }}>
                                                    <label for="semana">{{ ___('Últimos días del mes') }} </label>
                                                </li>
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="mes"
                                                        name="chard" type="radio" value="mes"
                                                        {{ $tipoGrafica == 'mes' ? 'checked' : '' }}>
                                                    <label for="mes">{{ ___('Últimos 6 meses') }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="card-title pb-2">{{ ___('Días') }}</h4>
                </div>
            </div>
            
        </div>
    </div>
@endsection
@push('footer')
    <script defer src="https://player.vimeo.com/api/player.js"></script>
    @if ($usersChart)
        {!! $usersChart->script() !!}
    @endif

    <script>
        $(document).ready(function() {
            $('input:radio').on('change', function() {
                var filter = $('input:radio[name="chard"]:checked').map(function() {

                    return this.value;

                }).get().join('|');
                $('#loading').show();

                if (filter == "dia") {
                    console.log("dia");
                    window.location.href = "{{ route('user.home') }}?grafica=dia";
                } else if (filter == "semana") {
                    console.log("semana");
                    window.location.href = "{{ route('user.home') }}?grafica=semana";
                } else if (filter == "mes") {
                    console.log("mes");
                    window.location.href = "{{ route('user.home') }}?grafica=mes";
                }
            });
        });
    </script>
<script>
    $(document).ready(function() {
        if (screen.width < 1396)
            $('#clock-dashboard').removeClass('d-none');
        else if (screen.width > 1396)
            $('#clock-dashboard').addClass('d-none');
            $('#diviframe').css('padding-bottom','18px');
            $('#diviframe').css('padding-top','10px');
    });
</script>
@endpush