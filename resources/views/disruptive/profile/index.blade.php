@extends('disruptive.disruptive-layout')
@section('title', ___('User Profile'))
@push('header')
    <style>
        .input-not-verified {
            border: solid 2px #E74C3C;
            animation-name: parpadeo;
            animation-duration: 1s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;

            -webkit-animation-name: parpadeo;
            -webkit-animation-duration: 2s;
            -webkit-animation-timing-function: linear;
            -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @-webkit-keyframes parpadeo {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }

        @keyframes parpadeo {
            0% {
                opacity: 1.0;
            }

            50% {
                opacity: 0.0;
            }

            100% {
                opacity: 1.0;
            }
        }


        .menu {
            bottom: 2%;
            right: 0;
            display: flex;
            flex-direction: column-reverse;
            justify-content: center;
            align-items: flex-end;
            margin: 0 1em 1em 0;
            /* z-index: 2; */
            width: auto;
            position: absolute;
        }

        .menu #help {
            position: relative;
            width: 50px;
            height: 50px;
            background: #fff;
            border-radius: 50%;
            box-shadow: 0 3px 4px rgba(0, 0, 0, 0.15);
            display: flex;
            justify-content: center;
            align-items: center;
            color: #333;
            cursor: pointer;
            font-size: 1.75em;
            transition: 1.25s;
        }

        .menu li span {
            text-transform: uppercase;
            color: #fff;
            text-align: center;
        }

        .menu.active .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            /* Fondo oscurecido semi-transparente */
            /* z-index: 999; */
            /* Asegurarse de que el overlay aparezca por encima de todo el contenido */
            display: none;
            /* Esconder inicialmente el overlay */
        }

        .menu.active #help {
            transform: rotate(360deg);
            box-shadow: 0 6px 8px rgba(0, 0, 0, 0.15),
                0 0 0 2px #333,
                0 0 0 6px #fff;
        }

        .share-container {
            display: flex;
            flex-direction: row-reverse;
            align-items: center;
            justify-content: center;
        }

        .social-container {
            display: flex;
            flex-direction: column-reverse;
            align-items: center;
            justify-content: center;
        }

        .shareMenu {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
        }

        .socialMenu {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .shareMenu li {
            padding: 0 0.75em 0 0;
        }

        .socialMenu li {
            padding: 0 0 0.75em;
        }

        .menu li,
        .menu li {
            list-style: none;
            transition: 0.5s;
            scale: 0;
            transition-delay: calc(0.05s * var(--i));
        }

        .menu.active li,
        .menu.active li {
            scale: 1;
        }

        .menu li a {
            display: flex;
            width: 45px;
            height: 45px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            font-size: 1.4em;
            color: var(--clr);
            box-shadow: 0 3px 4px rgba(0, 0, 0, 0.15);
            transition: 0.5s;
        }

        .menu li:hover a {
            font-size: 1.6em;
            box-shadow: 0 0 0 2px var(--clr),
                0 0 0 6px #fff;
        }

        @media screen and (max-width: 300px) {
            .text {
                font-size: 25px;
            }
        }

        .backdropp {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            /* Fondo oscurecido semi-transparente */
            display: none;
            /* Lo escondemos inicialmente */
        }

        .text-opciones-verificacion {
            font-size: 12px;
        }
    </style>
@endpush
@section('content')
    {{-- @include('layouts.messages') --}}
    <div class="content-area card">
        <div class="card-innr">
            <div class="card-head">
                <h4 class="card-title">{{ ucfirst(___('Profile Details')) }}</h4>
            </div>
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" id="profile"
                        href="#personal-data">{{ ___('Personal Data') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="botonsettings" href="#settings">{{ ___('Settings') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#password">{{ ___('Password') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="botonWallet" href="#wallet">{{ ___('Wallet') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#photoProfile">{{ ___('Photo Profile') }}</a>
                </li>
            </ul>{{-- .nav-tabs-line --}}
            <div class="tab-content" id="profile-details">
                <div class="tab-pane fade show active" id="personal-data">
                    <form class="validate-modern" action="{{ route('user.ajax.account.update') }}" method="POST"
                        id="nio-user-personal" autocomplete="off">
                        @csrf
                        <input type="hidden" name="action_type" value="personal_data">
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="user-name" class="input-item-label">{{ ___('User Name') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="user-name" name="username"
                                            required="required" placeholder="{{ ___('Enter User Name') }}" minlength="3"
                                            value="{{ $user->username }}" readonly>
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="full-name" class="input-item-label">{{ ___('Full Name') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="full-name" name="name"
                                            required="required" placeholder="{{ ___('Enter Full Name') }}" minlength="3"
                                            value="{{ $user->name }}">
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="email-address" class="input-item-label">{{ ___('Email Address') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="email-address" name="email"
                                            required="required" placeholder="{{ ___('Enter Email Address') }}"
                                            value="{{ $user->email }}" readonly>
                                        {{-- Cambiar corre  --}}
                                        {{-- <div class="note note-plane note-info text-opciones-verificacion">
                                            <em class="fas fa-info-circle"></em>
                                                <a class="text-info text-decoration-underline" href="{{ route('send.Changemail', Crypt::encrypt($user->id)) }}"
                                                id="btnAgainReset">{{ ___('Request change of mail.') }}</a>
                                        </div> --}}
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="mobile-number" class="input-item-label">{{ ___('Mobile Number') }}</label>
                                    <div class="input-wrap">
                                        <div class="input-group row">
                                            <div
                                                class="col-{{ $user->whatsapp_status == 1 && $user->mobile != null ? '9' : '12' }}">
                                                <input
                                                    class="input-bordered {{ $user->whatsapp_status == 0 ? 'input-not-verified' : '' }}"
                                                    type="text" id="mobile-number" readonly
                                                    placeholder="{{ ___('Enter Mobile Number') }}"
                                                    value="{{ $user->mobile }}">
                                            </div>
                                            @if ($user->whatsapp_status == 1 && $user->mobile != null)
                                                <div class="col-3">
                                                    <a type="button" style="z-index:0;"
                                                        class="btn btn-primary {{ $user->whatsapp_status == 1 && $user->mobile != null && $actual < $reset5MinVerification ? 'disabled' : '' }}"
                                                        href="javascript:void(0)" id="btnModalWhatsappCheckUpdate"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#modalWhatsappCheck">{{ ___('Actualizar') }}</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="note note-plane note-info text-opciones-verificacion">
                                        <em class="fab fa-whatsapp"></em>
                                        <input type="hidden" name="" value="{{ $reset5MinVerification }}"
                                            id="timerwhatsappvalue">
                                        @if ($user->whatsapp_status == 0 && $user->mobile == null && $user->timer_whatsapp_reset == null)
                                            <a class="text-info text-decoration-underline {{ $user->whatsapp_status == 1 ? 'd-none' : '' }}"
                                                id="aModalWhatsappCheck" href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#modalWhatsappCheck">{{ ___('Verify WhatsApp.') }}</a>
                                            <p id="whatsappVerified"
                                                class="{{ $user->whatsapp_status == 0 ? 'd-none' : '' }}">
                                                {{ ___('whatsapp verified.') }}</p>
                                        @elseif($user->whatsapp_status == 1 && $user->mobile != null && $user->timer_whatsapp_reset != null)
                                            {{-- @dd( $user->timer_whatsapp_reset, $reset5MinVerification ) --}}
                                            <p id="whatsappVerified"
                                                class="{{ $user->whatsapp_status == 0 ? 'd-none' : '' }}">
                                                {{ ___('whatsapp verified.') }}
                                            </p>
                                            @if ($actual < $reset5MinVerification)
                                                <p class="text-info" style="position: absolute;top: 16px;"
                                                    id="tiemporestantewhatsapp"></p>
                                            @endif
                                        @elseif(
                                            $user->whatsapp_status == 0 &&
                                                $user->mobile != null &&
                                                $user->mobile_aux == null &&
                                                $user->timer_whatsapp_reset == null)
                                            <a class="text-info text-decoration-underline {{ $user->whatsapp_status == 1 ? 'd-none' : '' }}"
                                                id="aModalWhatsappCheck" href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#modalWhatsappCheck">{{ ___('Verify WhatsApp.') }}</a>
                                            <p id="whatsappVerified"
                                                class="{{ $user->whatsapp_status == 0 ? 'd-none' : '' }}">
                                                {{ ___('whatsapp verified.') }}</p>
                                        @endif
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="date-of-birth"
                                        class="input-item-label">{{ ___('Date of Birth') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered date-picker-dob" type="text" id="date-of-birth"
                                            name="dateOfBirth" required="required" placeholder="mm/dd/yyyy"
                                            value="{{ $user->dateOfBirth != null ? _date2sz($user->dateOfBirth, 'm/d/Y') : '' }}">
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>{{-- .col --}}
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="nationality" class="input-item-label">{{ ___('Nationality') }}</label>
                                    <div class="input-wrap">
                                        <select class="select-bordered select-block" name="nationality" id="nationality"
                                            required="required" data-dd-class="search-on">
                                            <option value="">{{ ___('Select Country') }}</option>
                                            @foreach ($countries as $country)
                                                <option
                                                    {{ $user->nationality == $country ? 'selected ' : '' }}value="{{ $country }}">
                                                    {{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="gaps-1x"></div>{{-- 10px gap --}}
                        <div class="d-sm-flex justify-content-between align-items-center">
                            <button type="submit" class="btn btn-primary">{{ ___('Update Profile') }}</button>
                            <div class="gaps-2x d-sm-none"></div>
                        </div>
                    </form>{{-- form --}}
                    <div class="menu">
                        <div class="overlay"></div>
                        <div class="share-container">
                            <div id="help"
                                style="color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};">
                                <ion-icon name="menu-outline"></ion-icon>
                            </div>
                            <ul class="shareMenu">
                                <li
                                    style="--i:0; color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};">
                                    <a href="#" data-target="#modalTerminos" id="terminos" class="terminos"
                                        type="button" data-toggle="tooltip" data-placement="bottom" title=""
                                        data-original-title="{{ ___('Terminos y condiciones') }}">
                                        <i class="ri-newspaper-line"></i>
                                    </a>
                                </li>
                                <li
                                    style="--i:1;color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};">
                                    <a href="#" data-target="#modalPrivacidad" id="privacidad" class="privacidad"
                                        type="button" data-toggle="tooltip" data-placement="bottom" title=""
                                        data-original-title="{{ ___('Privacidad y politicas') }}">
                                        <i class="mdi mdi-clipboard-list-outline"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="social-container ">
                            <ul class="socialMenu">
                                <li
                                    style="--i:0; color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};">
                                    <a href="#" data-target="#modalTerminos" id="terminos" class="terminos"
                                        type="button" data-toggle="tooltip" data-placement="bottom" title=""
                                        data-original-title="{{ ___('Terminos y condiciones') }}">
                                        <i class="ri-newspaper-line"></i>
                                    </a>
                                </li>
                                <li
                                    style="--i:1;color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};">
                                    <a href="#" data-target="#modalPrivacidad" id="privacidad" class="privacidad"
                                        type="button" data-toggle="tooltip" data-placement="bottom" title=""
                                        data-original-title="{{ ___('Privacidad y politicas') }}">
                                        <i class="mdi mdi-clipboard-list-outline"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>{{-- .tab-pane --}}



                <div class="tab-pane fade" id="settings">
                    <h4 class="title">{{ ucfirst(___('Activa')) }}/{{ ucfirst(___('Desactiva las notificaciones')) }}
                    </h4>
                    <div class="row">
                        @push('footer')
                            <script>
                                $(document).ready(function() {
                                    $('.notification').click(function() {
                                        $.ajax({
                                            type: "get",
                                            url: "{{ route('change.notification') }}" + "?notify=" + $(this).attr('id'),
                                            success: function(response) {
                                                if (response == "ok") {
                                                    show_toast("success",
                                                        "{{ ___('Se actualizo la configuracion correctamente') }}",
                                                        "ti ti-check");
                                                } else {
                                                    show_toast('error',
                                                        "{{ ___('Algo salio mal, por favor vuelve a intentar') }}",
                                                        'ti ti-close');
                                                }
                                            }
                                        });
                                    });

                                });
                            </script>
                        @endpush
                        @php
                            $user = \App\Models\User::find(auth()->user()->id);
                            if (isset(auth()->user()->notifications_setting)) {
                                $notificacionesArray = \App\Helpers\GosenHelper::arrayNotifySettings();
                                $notificaciones = json_decode(auth()->user()->notifications_setting, true);
                                $keys_diff = array_diff_key($notificacionesArray, $notificaciones);
                                foreach ($keys_diff as $key => $value) {
                                    $notificaciones[$key] = $value;
                                }
                                $user->notifications_setting = $notificaciones;
                                $user->save();
                            } elseif (auth()->user()->notifications_setting == null) {
                                $notificacionesArray = \App\Helpers\GosenHelper::arrayNotifySettings(); 
                                $user->notifications_setting = $notificacionesArray;
                                $user->save();
                                $notificaciones = $user->notifications_setting;
                            } else {
                                $notificaciones = \App\Helpers\GosenHelper::arrayNotifySettings();
                            }
                            
                        @endphp
                        <div class="col-md-6">

                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            {{ ___('Asociados') }}
                                        </strong>
                                        <div class="notes">{{ ___('Serás notificado de tus nuevos asociados') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['asociados'] ? 'checked' : '' }} id="asociados">
                                        <label for="asociados"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            {{ ___('Ganancias') }}
                                        </strong>
                                        <div class="notes">
                                            {{ ___('Se le notificará de aumentos y disminuciones significativas de sus ganancias') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['ganancias'] ? 'checked' : '' }} id="ganancias">
                                        <label for="ganancias"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            {{ ___('Informacion') }}
                                        </strong>
                                        <div class="notes">
                                            {{ ___('Se le notificará de cualquier actividad de referencia') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['informacion'] ? 'checked' : '' }} id="informacion">
                                        <label for="informacion"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            {{ ___('Noticias') }}
                                        </strong>
                                        <div class="notes">
                                            {{ ___('Se le notificará de cualquier nueva actualización de noticias') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['noticias'] ? 'checked' : '' }} id="noticias">
                                        <label for="noticias"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            Whatsapp
                                        </strong>
                                        <div class="notes">
                                            {{ ___('Activa o Desactiva las notificaciones de Whatsapp') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['whatsapp'] ? 'checked' : '' }} id="whatsapp">
                                        <label for="whatsapp"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-10">
                                    <div class="input-item">
                                        <strong>
                                            {{ ___('Correo Electronico') }}
                                        </strong>
                                        <div class="notes">
                                            {{ ___('Se le notificará de cualquier nueva actualización de noticias por correo') }}.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 p-0">
                                    <div class="pt-2">
                                        <input class="input-switch input-switch-sm notification" type="checkbox"
                                            {{ $notificaciones['correos'] ? 'checked' : '' }} id="correos">
                                        <label for="correos"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>{{-- .tab-pane --}}
                <div class="tab-pane fade" id="password">
                    <div class="note note-plane pdb-1x pt-3 pl-0">
                        {{-- <em class="fas fa-info-circle pt-5" style="font-size: large; top:3px"></em> --}}
                        <h5 style="color: #717D7E">{{ ___('We will send a message to your current email address') }}
                        </h5>
                    </div>
                    <div class="note note-plane note-danger pdb-2x">
                        <em class="fas fa-info-circle"></em>
                        <p>{{ ___('Your password will only change after your confirmation by email.') }}</p>
                    </div>
                    <div class="gaps-1x"></div>{{-- 10px gap --}}
                    <div class="d-sm-flex align-items-center">
                        @if ($user->reset_status == 1)
                            <div class="note note-plane note-info pdb-2x">
                                <em class="fas fa-info-circle"></em>
                                <p>{{ ___('Email sent, check your inbox.') }}
                                    <a href="{{ route('send.mail', Crypt::encrypt($user->id)) }}" style="color: #5664d2;"
                                        id="btnAgainReset">{{ ___('Send again?') }}</a>
                                </p>
                            </div>
                        @else
                            <a href="{{ route('send.mail', Crypt::encrypt($user->id)) }}" id="btnReset"
                                class="btn btn-primary">
                                <em class="fa fa-key"></em> {{ ___('Reset Password') }}</a>
                        @endif
                        <div class="gaps-2x d-sm-none"></div>
                    </div>
                </div>{{-- .tab-pane --}}
                <div class="tab-pane fade wallet" id="wallet">
                    <form class="validate-modern" action="#" method="POST" id="nio-user-personal"
                        autocomplete="off">
                        @csrf
                        <input type="hidden" name="action_type" value="personal_data">
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="user-name"
                                        class="input-item-label">{{ ___('Enter your wallet address') }} (USDT)</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="walle" name="wallet"
                                            required="required" value="{{ $user->wallet_dollar }}"
                                            placeholder="{{ ___('Enter a wallet (USDT), validate to add it to your account') }}"
                                            minlength="20" value="">
                                    </div>
                                </div>{{-- .input-item --}}
                                <div class="d-sm-flex justify-content-between align-items-center">
                                    @if ($user->wallet_dollar != null && $user->wallet_dollar_status == 1)
                                        <a href="#" data-toggle="modal" data-target="#modal-centered"
                                            class="btn btn-primary butt disabled" id="butt"><i
                                                class="fas fa-wallet"></i> &nbsp; {{ ___('Change Wallet') }}</a>
                                    @elseif($user->wallet_dollar != null && $user->wallet_dollar_status == 0)
                                        <div class="note note-plane note-info pdb-2x">
                                            <em class="fas fa-info-circle"></em>
                                            <p>{{ ___('Email sent, check your inbox.') }}
                                                <a href="#" data-toggle="modal" data-target="#modal-centered"
                                                    style="color: #5664d2;" id="btnresetmail" class="d-none">
                                                    {{ ___('Send again?') }}
                                                </a>

                                            <p href="#" id="timer">{{ ___('Remaining time to resend') }}:
                                                &nbsp;<span id="demo" class=""></span>
                                            </p>
                                            <input type="hidden" id="timervalue"
                                                value="{{ $user->timer_mail_wallet }}">
                                            </p>
                                        </div>
                                    @else
                                        <a href="#" data-toggle="modal" data-target="#modal-centered"
                                            class="btn btn-primary butt disabled" id="butt"><i class="fas fa-wallet"
                                                style="transform: rotate(340deg);"></i> &nbsp;
                                            {{ ___('Change Wallet') }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>{{-- .row --}}
                        <div class="gaps-1x"></div>{{-- 10px gap --}}
                    </form>{{-- form --}}
                </div>{{-- .tab-pane --}}
                <div class="tab-pane fade wallet" id="photoProfile">
                    @include('user.includes.profilePhoto-include')
                </div>{{-- .tab-pane --}}
            </div>{{-- .tab-pane --}}
        </div>{{-- .tab-content --}}
    </div>{{-- .card-innr --}}
    {{-- <div class="row">
        <div class="col-lg-6">
            @include('user.includes.profilePhoto-include')
        </div>
        <div class="col-lg-6">
            <div class="content-area card">
                <div class="card-innr">
                    <div class="card-head">
                        <h4 class="card-title">{!! ___('Two-Factor Verification') !!}</h4>
                    </div>
                    <p>{!! __(
                        "Two-factor authentication is a method for protection of your account. When it is activated you are required to enter not only your password, but also a special code. You can receive this code in mobile app. Even if third party gets access to your password, they still won't be able to access your account without the 2FA code.",
                    ) !!}</p>
                    <div class="d-sm-flex justify-content-between align-items-center pdt-1-5x">
                        <span class="text-light ucap d-inline-flex align-items-center"><span
                                class="mb-0"><small>{{ ___('Current Status:') }}</small></span> <span
                                class="badge badge-{{ $user->google2fa == 1 ? 'info' : 'disabled' }} ml-2">{{ $user->google2fa == 1 ? ___('Enabled') : ___('Disabled') }}</span></span>
                        <div class="gaps-2x d-sm-none"></div>
                        <button type="button" data-toggle="modal" data-target="#g2fa-modal"
                            class="order-sm-first btn btn-{{ $user->google2fa == 1 ? 'warning' : 'primary' }}">{{ $user->google2fa != 1 ? ___('Enable 2FA') : ___('Disable 2FA') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

@endsection
@section('modals')
    @php
        $token_name = strtoupper(token('name'));
    @endphp
    <div class="modal fade modal-payment" id="modalTerminos" tabindex="-1" data-backdrop="static"
        data-keyboard="false" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <div class="card-innr">
                            <div class="card-text">
                                <h1 class="text-center">
                                    <u>
                                        <b>
                                            {{ $token_name }}
                                        </b>
                                    </u>
                                </h1>
                                <p class="text-justify">
                                    {{ ___('Antes de acceder al programa') }} {{ $token_name }}
                                    {{ ___('deberás obligatoriamente leer y aceptar los términos de uso y condiciones que a continuación establecemos') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Para fines del usuario deberás saber que') }} {{ $token_name }}
                                    {{ ___('es un programa que refleja una experiencia educativa que explica cómo cualquier persona puede acceder a conocimientos disruptivos y estrategias de apalancamiento en poco tiempo. El programa aloja sus derechos en la empresa denominada') }}
                                    GOGO MARCA LTD.,
                                    {{ ___('radicada en Inglaterra bajo el número de identificación 93313131') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Tal cual hemos establecido') }} {{ $token_name }}
                                    {{ ___('no es otra cosa que un sitio web que aloja un programa que comprime un cúmulo de relatos sobre experiencias financieras cuyo propósito principal es enseñar a sus visitantes y/o usuarios a generar riquezas de modo disruptivo en plazos muy breves. Dentro el programa les enseñamos a estudiar, ubicar o identificar proyectos “unicornios” o “estrella” prometedores que por sus condiciones confieran algunas seguridades que permitan invertir salubremente, bajo formas estratégicas e inteligentes') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El acceso o la compra de paquetes colgados al sitio web, solamente confieren el derecho a uso de los mismos') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El o los tokenes que') }} {{ $token_name }}
                                    {{ ___('pueda entregar a los Visitantes o Usuarios que hayan comprado los paquetes en cuestión solo se harán efectivos en forma de bonificación y/o regalo únicamente a quienes hayan revisado y superado o cumplido las formas establecidas del sitio web para su uso') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Se aclara que los tokenes que') }} {{ $token_name }}
                                    {{ ___('entregará a sus Visitantes o Usuarios son un regalo y por tanto no constituyen cargas tributarias') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El regalo como tal puede o no implicar un valor en el tiempo, de acuerdo a las tendencias y usos del mismo en el tiempo, no pudiendo por tanto los bonificados generar reclamo alguno en caso los propios no adquieran valor') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Una vez leídos o superados los puntos precedentes, previo a ingresar a ') }}
                                    {{ $token_name }}
                                    {{ ___(' deberás aceptar los términos de uso y política de privacidad de éste sitio bajo las condiciones contractuales que se expondrán dándote el derecho de acceder al sitio,hacer uso del material y servicio, interactuar en el sitio') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Las personas que no acepten los términos y condiciones tendrán denegado el acceso a nuestro portal, sitios y sus enlaces, contenido, bonificaciones u otros') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El ver, visitar, utilizar, usar el contenido de éste lugar o interactuar e interaccionar con cualquier Banner, Pop-Up, publicidad o con alguno de nuestros asesores o servidores, supone de modo tácito que usted acepta todas las disposiciones de nuestras políticas de privacidad y se adhiere a las condiciones contractuales estipuladas en nuestros sitios web') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Queda absolutamente prohibido el ingreso a nuestros sitios a personas incapacitadas legalmente declaradas judicialmente o menores a 18 años, pese ser una web o sitio cibernético destinado a fines lícitos consagrados en el marco de la libre expresión que plasman los Derechos Universales en lo referido al derecho de expresión y puntualmente en la temática de la educación y experiencia financiera del autor a sus seguidores. El sitio se encuentra dando cumplimiento a la ley de Privacidad en Línea para niños ') }}
                                    (COPA)
                                    {{ ___(' de 1998') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Éste sitio se reserva el derecho de negar el acceso a cualquier persona, visitante o usuario que no cumpla con alguna de las exigencias, requisitos o condiciones estipuladas en éste espacio regulatorio y contractual') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El acuerdo en términos de uso puede ser modificado unilateralmente por los titulares de ') }}
                                    {{ $token_name }}
                                    {{ ___(' en caso las condiciones del proyecto vayan siendo alteradas y para dicho cometido, sus visitantes o usuarios tienen obligación de adaptarse a los mismos, sin posibilidad de reclamación alguna') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    ({{ ___('Términos de Uso y Condiciones') }})
                                </h4>
                                <h4 class="text-center">
                                    <b>
                                        {{ ___(strtoupper('CAPITULO I – DE LAS PARTES')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('PARTES')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Los visitantes, espectadores, usuarios, suscriptores, miembros, afiliados o clientes, denominados colectivamente en el presente como') }}
                                    "{{ ___('Visitantes') }}".
                                </p>
                                <p class="text-justify">
                                    {{ ___('El sitio web y sus propietarios, alojadores empresariales, comercializadores y operadores, son partes de este acuerdo, en lo sucesivo denominado') }}
                                    "{{ ___('Sitio web') }}".
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('CANCELACIÓN, TERMINACIÓN O EXCLUSIÓN')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('La suscripción al servicio puede cancelarse en cualquier momento y sin causa, ya sea por el Sitio o por el Suscriptor previa notificación a la otra parte mediante correo electrónico, por llamada telefónica, comunicación por whatsapp u otros mecanismos tradicionales. Cuando un miembro solicite la terminación, las tarifas no tendrán reembolso. Los suscriptores son responsables de los cargos incurridos por ellos hasta la terminación del servicio. Los usuarios de tarjetas de crédito pueden estar sujetos a una autorización previa. La pre autorización no es un cargo a la tarjeta de crédito. Sin embargo, el cargo de suscripción aplicable en ese momento puede reservarse contra el límite de la tarjeta de crédito disponible del Miembro. El Sitio no será responsable de los cargos bancarios, tarifas o multas debido a cuentas del Suscriptor en descubierto o morosas') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('POLÍTICA DE REEMBOLSO')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Por el presente se establece taxativamente la imposibilidad de hacer reembolsos a sus visitantes o usuarios. Una vez la persona que accede a nuestros servicios contrata') }},
                                    {{ ___(strtoupper('NO HAY REEMBOLSOS')) }}
                                    {{ ___('al comprar nuestro programa, en consideración a que el usuario ha consumido el producto y tiene acceso a toda la información, capacitación, las herramientas y la comunidad. Las devoluciones de cargo tampoco están permitidas y, al realizar un pedido, acepta no iniciar ninguna devolución de cargo y, si lo hace, será responsable de los cargos en los que podamos incurrir, incluidos los honorarios de abogados') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Es decir, una vez usted accede a nuestros servicios, le entregamos la capacitación y el contenido según lo prometido, y por tanto, no devolvemos lo abonado. Si usted cree que esto puede no ser adecuado a sus fines, simplemente no acepte ni contrate nuestro servicio') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DEL PROGRAMA Y SU ÉXITO')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Se deja constancia amplia que la capacitación en cuestión es el resultado de la fusión de muchos estudios, conceptos y consejos de algunos expertos en el estudio de las finanzas, pero además, el propio ha sido aplicado y experimentado exitosamente por el') }}
                                    CEO,
                                    {{ ___('sin embargo, ello no garantiza el éxito en sus usuarios o adquirentes, por tanto, recomendamos a quienes sean escépticos del programa no aceptar, comprar o desarrollar el mismo, en razón, reiteramos a que su eficacia no puede ser garantizada') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    <b>
                                        {{ ___(strtoupper('CAPÍTULO II – USO DEL SITIO WEB')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('FORMA Y USO DE INFORMACIÓN DEL SITIO WEB')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('A menos que sea parte del programa y haya celebrado un contrato expreso por escrito con este sitio web; los visitantes, espectadores, suscriptores, miembros, afiliados o clientes sin autorización o permiso no tienen derecho a utilizar esta información en un entorno comercial o público; no tienen derecho a copiar, reproducir, transmitir, guardar, imprimir, vender o publicar parte o el total del contenido de este sitio web. Al ver el contenido de este sitio web, usted acepta esta condición de visualización y reconoce que cualquier uso no autorizado es ilegal sujeto a sanciones civiles o penales. El Visitante no tiene ningún derecho de usar el contenido o partes del mismo, incluidas sus bases de datos, páginas invisibles, páginas vinculadas, código subyacente o cualquier otra propiedad intelectual que pueda contener el sitio, por cualquier motivo y para cualquier uso. El visitante acepta daños liquidados por la cantidad de') }}
                                    U.S.$ 50.000
                                    {{ ___('además de las costas y perjuicios reales por incumplimiento de esta disposición. El visitante garantiza que entiende que aceptar esta disposición es una condición para verlo y que verlo constituye una aceptación') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('PROPIEDAD DEL SITIO WEB; DERECHO A USO, VENTAS Y DERECHOS DE PUBLICACIONES')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El sitio web y sus contenidos son propiedad o están autorizados por los titulares del espacio o sitio. Se debe tener por sentado que el contenido en el sitio web es de titularidad del programa') }}
                                    {{ $token_name }}
                                    {{ ___('y tiene derechos de autor debidamente alojados y registrados en') }} GOGO
                                    MARCA LTD.
                                    {{ ___('Los visitantes y sus usuarios no tienen ningún derecho al contenido del sitio. El uso del contenido del sitio web por cualquier motivo es ilegal a menos que el titular confiera derechos a través de contratos') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Quedan restringidos y prohibidos todos los enlaces o hipervínculos que se traten de casar o ligar a dicho sitio web, su marca, el') }}
                                    “framing”, {{ ___('la empresa alojadora de') }} {{ $token_name }}
                                    {{ ___('o lugares de referencia, a excepción de los autorizados expresamente por terceros contratados de la empresa a cargo del programa (en tanto y por lo demás) nadie puede vincular este sitio o partes del mismo (incluidos, entre otros, logotipos, marcas comerciales, marcas o material protegido por derechos de autor). Además, no se le permite hacer referencia a la URL (dirección del sitio web) de este sitio web en ningún medio comercial o no comercial sin permiso expreso, ni se le permite') }}
                                    "{{ ___('enmarcar ni usar') }}"
                                    {{ ___('el sitio. Usted acepta específicamente cooperar con el sitio web para eliminar o desactivar dichas actividades y ser responsable de todos los daños. Por la presente, acepta daños liquidados de') }}
                                    US $ 50,000.00
                                    {{ ___('más costos y daños reales por violar esta disposición, los cuales se hacen cobrables y ejecutables ante cualquier jurisdicción del mundo entero') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DESLINDAMIENTO DE RESPONSABILIDAD POR EL CONTENIDO DEL SITIO')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El sitio web, el Autor de') }} {{ $token_name }}
                                    {{ ___('y las empresas alojadoras del proyecto se deslindan, eximen y excluyen de todo tipo de responsabilidad por la exactitud del contenido de este sitio web. Los visitantes asumen todos los riesgos de ver, leer, usar o confiar en la información. El sitio web no garantiza que su contenido se haga efectivo en cuanto sus alcances e información') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DESLINDE Y DESCARGO RESPECTO POSIBLES DAÑOS CAUSADOS A SU COMPUTADORA O SOFTWARE AL INTERACCIONAR CON ESTE SITIO WEB O SUS CONTENIDOS')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El sitio web, sus titulares y/o la empresa alojadora no asume ningún tipo de responsabilidad ante posibles daños a las computadoras o el software del visitante o de cualquier persona con la que el visitante y/o usuario se comunique posteriormente debido a la deformación, distorsión, corrupción o mal manejo del código o los datos que se transmiten inadvertidamente a la computadora del visitante. Por su parte, el visitante que interactúa con este sitio, con banners, o que utiliza las ventanas emergentes o la publicidad que se muestra en él, asume todo tipo de riesgos y responsabilidades de su computadora en consideración a que el ingreso al sitio es de su mera voluntad') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('LIMITACIÓN DE RESPONSABILIDAD')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Al ver, usar o interactuar de cualquier manera con este sitio, incluidos banners, publicidad o ventanas emergentes, descargas y como condición del sitio web para permitir su visualización legal, el Visitante renuncia para siempre a todo derecho a reclamos por daños de cualquier y toda descripción basada en cualquier factor causal que resulte en cualquier posible daño, sin importar cuán atroz o extenso sea, ya sea físico o emocional, previsible o imprevisible, ya sea de naturaleza personal o comercial') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('TÉRMINOS DE INDEMNIZACIÓN')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El visitante acepta que, en caso de que cause daños, que el sitio web debe pagar, el visitante, como condición de visualización, promete reembolsar al sitio web por todos') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    <b>
                                        {{ ___(strtoupper('CAPITULO III- EN CUANTO CONTENIDO DEL PROGRAMA Y USO')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DE LAS PRESENTACIONES, CONFERENCIAS Y DEL MATERIAL')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El Visitante acepta como condición de visualización, que cualquier comunicación entre el Visitante y el sitio web se considera una presentación. Todos los envíos, incluidas partes de los mismos, los gráficos contenidos en los mismos, o cualquier parte del contenido del envío, se convertirán en propiedad exclusiva del sitio web y pueden usarse, sin permiso adicional, para uso comercial sin consideración adicional de ningún tipo. El visitante acepta comunicar solo esa información al sitio web, que desea permitir para siempre que el sitio web lo use de la manera que considere adecuada.') }}
                                    "{{ ___('Envíos') }}"
                                    {{ ___('también es una disposición de la Política de privacidad') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('AVISOS SIN NOTIFICACIÓN')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Los actos, las modificaciones, las alteraciones o problemas emergentes en el sitio no necesariamente serán notificados al Visitante, quien deberá visitar el sitio para obtener información y por tanto el visitante o usuario garantiza expresamente la comprensión de su renuncia a la misma') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DERECHOS DE AUTOR')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El contenido del sitio, su diseño, gráficos, organización, la compilación de datos, la traducción, el cuadro de diálogos y todos los asuntos relacionados en la web, su espacio y su estructura, están protegidos por derechos de autor, marcas comerciales y otros derechos de propiedad (incluidos, entre otros, los de propiedad intelectual). La copia, redistribución, uso o publicación de tales asuntos o cualquier parte del Sitio están estrictamente prohibidos a sus suscriptores y/o terceros. Los visitantes y usuarios del sitio no adquieren derechos de propiedad sobre ningún contenido, documento u otros materiales vistos a través del sitio web. La publicación de información o materiales en el Sitio no vincula sub derechos de marcas o derechos de autor a sus receptores') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('LICENCIA LIMITADA; USOS PERMITIDOS')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('A los visitantes, clientes o usuarios se les otorga una licencia intransferible, revocable; para acceder y utilizar el Sitio estrictamente de acuerdo con este Acuerdo, a fines éstos puedan: (1) utilizar el Sitio con fines internos personales de capacitación y no comerciales; y (2) para imprimir información discreta del Sitio únicamente para fines internos personales de capacitación y no comerciales y siempre que se mantengan y no se alteren todos los derechos de autor y otras políticas. Los clientes, usuarios y visitantes no pueden por ningún motivo utilizar la versión impresa o electrónica del Sitio o su contenido en ningún litigio o arbitraje bajo ninguna circunstancia') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('RESTRICCIONES Y PROHIBICIONES DE USO')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('La licencia para el acceso al uso del Sitio y cualquier información, materiales y todos los documentos del sitio están sujetos a las siguientes restricciones y prohibiciones de uso: Los visitantes no pueden (1) copiar, imprimir, volver a publicar, exhibir, distribuir, transmitir, vender, alquilar, arrendar, prestar o poner a disposición de cualquier forma o por cualquier medio todo o parte del Sitio o cualquier Contenido y Materiales recuperados de éste; (2) usar el Sitio o cualquier material obtenido del Sitio para desarrollar cualquier sistema de información, almacenamiento y recuperación de base de datos, base de información o recurso similar (en cualquier medio existente ahora o desarrollado en el futuro), que se ofrece para distribución comercial de cualquier tipo, incluso a través de venta, licencia, arrendamiento, alquiler, suscripción o cualquier otro mecanismo de distribución comercial; (3) crear compilaciones o trabajos derivados de cualquier Contenido y Materiales del Sitio; (4) usar cualquier Contenido y Materiales del Sitio de cualquier manera que pueda infringir cualquier derecho de autor, derecho de propiedad intelectual, derecho de propiedad o derecho de propiedad de nosotros o de terceros; (5) eliminar, cambiar u ocultar cualquier aviso de derechos de autor u otro aviso de propiedad o términos de uso contenidos en el Sitio; (6) poner a disposición cualquier parte del Sitio a través de cualquier sistema de tiempo compartido, oficina de servicios, Internet o cualquier otra tecnología existente ahora o desarrollada en el futuro; (7) eliminar, descompilar, desensamblar o aplicar ingeniería inversa a cualquier software del Sitio o usar cualquier software de detección o monitoreo de red para determinar la arquitectura del Sitio; (8) usar el Sitio de una manera que viole cualquier ley de las jurisdicciones del mundo entero; y (9) exportar o reexportar el Sitio o cualquier parte del mismo, o cualquier software disponible en a través del Sitio') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    <b>
                                        {{ ___(strtoupper('CAPÍTULO IV – DESCARGOS AL PROYECTO Y AUTORES')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DESCARGO DE RESPONSABILIDAD')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('El material proporcionado se ofrece como referencia, sin garantía alguna de que con él se tenga éxito financiero. El lector, cliente, usuario, visitante o contratante asume todos los riesgos derivados del uso de información contenida en este documento. Por tanto, el Sitio, la empresa alojadora y comercializadora, el Autor, se deslinda de toda responsabilidad por la información proporcionada') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('ACUERDO DE AFILIADOS')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Ninguna publicidad de Afiliados tendrá contenido engañoso, falso, sin fundamento o que incumpla con las leyes, regulaciones y pautas de protección al consumidor dentro la jurisdicción en la que la empresa alojadora del proyecto opera') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('POLÍTICA ANTISPAM')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('En el contexto de la mensajería electrónica de tipo spam, se refiere a mensajes no solicitados, masivos o indiscriminados, generalmente enviados con fines comerciales. Por tanto y para fines, la empresa alojadora del programa') }}
                                    {{ $token_name }}
                                    {{ ___('proporciona un servicio que permite a los usuarios enviar mensajes de correo electrónico/mensajes privados a otros. Los usuarios no deben utilizar esta función para enviar mensajes no solicitados, masivos o indiscriminados, ya sea con fines comerciales o no') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('La recepción de mensajes no deseados de nuestra empresa: en el improbable caso de que reciba algún mensaje nuestro o enviado a través de nuestros sistemas que pueda considerarse spam, comuníquese con nosotros utilizando los detalles a continuación y se investigará el asunto') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Se establece que todos los clientes, usuarios, afiliados y visitantes del proyecto') }}
                                    {{ $token_name }}
                                    {{ ___('deben cumplir con todas las regulaciones de leyes y prohibiciones de mal empleo de publicidad que se establecen en la jurisdicción en la que emprende') }}
                                    {{ $token_name }} {{ ___('y ente regulador') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El afiliado acepta toda responsabilidad a') }} GOGO MARCA LTD.
                                    {{ ___('de cualquier demanda, investigación, reclamo o queja que surja de cualquier violación o supuesta violación de los términos anteriores') }}.
                                    GOGO MARCA LTD.
                                    {{ ___('no será responsable de aprobar ningún anuncio de afiliado. El cumplimiento es únicamente con el Afiliado y el Afiliado declara y garantiza que tendrá una revisión legal de todos los Anuncios de Afiliados para el cumplimiento necesario y requerido. Los afiliados asumen toda la responsabilidad de su publicidad') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('DESCARGO DE RESPONSABILIDAD SOBRE GANANCIAS DEL CLIENTE, USUARIO O COMPRADOR DEL PROGRAMA')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Con éste punto se deja ampliamente establecido, aclarado y se enfatiza que todos los elementos de capacitación del programa o proyecto') }}
                                    {{ $token_name }},
                                    {{ ___('compuesto por videos, PDF informativos, y documentos en general, son aspectos inherentes a las vivencias financieras de su autor, siendo las propias simples opiniones y experiencias personales y por ello es que pedimos a todos sus usuarios, clientes, visitantes o afiliados que jamás tomen los criterios experimentales como una opinión definitiva para asumir decisiones financieras. Antes de decidir sobre asuntos financieros tenga en cuenta que existen diversidad de estrategias financieras para cada modelo o tipo de inversión y según las coyunturas y acorde las necesidades individuales y patrimonio de casa ser. Nuestros videos y capacitación probablemente no puedan ser adaptados a sus necesidades') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Todo usuario, cliente, afiliado, visitante del sitio e incluso las entidades reguladoras que ingresen a éste sitio deberán tomar en cuenta que, cualquier declaración de ingresos o ejemplo de ingresos del sitio, han sido obtenidos únicamente mediante el método de observación y no han superado el método científico para su uso e implementación, por tanto, la réplica de su implementación y el uso de las instrucciones no garantiza bajo ninguna circunstancia el resultado que se establece en los videos, textos o documentos incorporados al sitio y programa, por tanto, su uso implica riesgo al consumidor, el cual queda apartado y ajeno al programa. Es decir, todo individuo que aplique lo expuesto en el programa asume sus propios riesgos, no pudiendo responsabilizar al programa o su autor por el mismo') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Cualquiera y todas las reclamaciones o declaraciones, en cuanto a las ganancias de ingresos en este Sitio Web, no deben considerarse como ganancias promedio') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Consideramos que ningún programa ni éste puede garantizar el éxito de resultados en cuanto a su aplicación') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Si Usted no está dispuesto a invertir tiempo, a invertir dinero sin riesgos, le recomendamos no acceda al presente servicio') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    <b>
                                        {{ ___(strtoupper('CAPITULO VI – TÉRMINOS CONTRACTUALES')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('LA COMPRA DE ESTE PRODUCTO INFIERE AUTOMÁTICAMENTE LA ACEPTACIÓN EXPRESA DE SUS ADQUIRIENTES, USUARIOS, O CLIENTES, QUIENES SE SOMETEN A LOS TÉRMINOS DEL ACUERDO EN CUESTIÓN')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Para fines, se establece que toda persona natural o jurídica, robot, o adquirente de derechos del servicio brindado por este sitio web o todo comprador de productos ofertados dentro el programa') }}
                                    {{ $token_name }} {{ ___('alojado por') }} GOGO MARCA LTD
                                    {{ ___('de generales ya establecidas –ut supra-, se somete y acepta expresamente los términos y acuerdos de este programa y lo inscrito en el presente documento') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('CONTROVERSIAS ENTRE PARTES')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('En caso de controversia en mérito al contrato, agravio insurgente a consecuencia de la relación generada por la compra del producto, incluidos problemas de solicitud, problemas de privacidad y problemas de términos de uso., el Visitante, Usuario o Cliente renuncia al litigio civil, comercial y penal, sometiéndose única y exclusivamente al arbitraje comercial dentro la jurisdicción donde opera el programa') }}
                                    {{ $token_name }}, {{ ___('alojado por la empresa') }} GOGO MARCA LTD
                                    {{ ___('de generales y datos cursantes ut supra') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Si algún asunto relacionado con esta compra se lleva ante un tribunal de justicia, antes o después del arbitraje, el espectador, visitante, miembro, suscriptor o cliente acepta que la jurisdicción única y adecuada será el estado y la ciudad declarados en la información de contacto. del titular de la web salvo que aquí se especifique lo contrario. En caso de que el litigio sea en un tribunal federal, el tribunal apropiado será el tribunal federal más cercano a la dirección del vendedor') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('JURISDICCIÓN Y LEY APLICABLE')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('En caso concreto todo espectador, visitante, miembro, suscriptor, usuario o cliente acepta y se somete a la jurisdicción de la dirección del Vendedor') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Por tanto, la ley aplicable para el caso concreto ante cualquier controversia y arbitraje será la del vendedor') }}.
                                </p>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('ACUERDO DE NO DIVULGACIÓN DE INFORMACIÓN DE UNA PARTE A OTRA')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    1. {{ ___('Se tendrá a') }} {{ $token_name }}
                                    {{ ___('como ente informativo, el cual tendrá facultades plenas de transferir información del receptor con fines netamente comerciales y empresariales, que tendrán por objeto, dotar de material al receptor y almacenar sus datos para darle mejor servicio') }}.
                                </p>
                                <p class="text-justify">
                                    2.
                                    {{ ___('Se tendrá por parte al receptor (Usted), quien además tendrá condición de Visitante, Usuario, comprador del proyecto y los productos ofertados en el sitio web, quienes estarán limitados de divulgar toda información proveniente del proyecto o la página en cuestión') }}.
                                </p>
                                <h5 class="text-left">
                                    {{ ___(strtoupper('CONSIDERACIONES')) }}:
                                </h5>
                                <p class="text-justify">
                                    {{ ___('La Parte receptora o cliente entiende que') }} {{ $token_name }}
                                    {{ ___('ha divulgado o puede divulgar información relacionada con el código fuente, los diseños de productos, las artes y todos los conceptos vinculados o relacionados al producto o proyecto, y por ende acepta que su información sea utilizada con fines comerciales, desde luego sin provocar daños a su privacidad, personalidad y/o datos personales') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('La divulgación de Información Privilegiada por parte del programa') }}
                                    {{ $token_name }},
                                    {{ ___('respecto la Parte Receptora acuerda: (a) preservar la Información Privilegiada en absoluta confidencialidad y tomar todas las precauciones para proteger dicha Información Privilegiada (incluyendo, sin limitación, todos precauciones que la Parte receptora emplea con respecto a sus propios materiales confidenciales), (b) no divulgar dicha Información de propiedad exclusiva o cualquier información derivada de la misma a terceros interesados, (c) no hacer ningún uso en ningún momento de dicha Información de propiedad exclusiva excepto para evaluar internamente su relación con la Parte divulgadora, y (d) no copiar ni aplicar ingeniería inversa a dicha Información de propiedad exclusiva') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('La Parte Receptora entiende que nada en el presente requiere la divulgación de cualquier Información Privada o requiere que la Parte Reveladora proceda con cualquier transacción o relación') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('La Parte Receptora además reconoce y acepta que no se hace ni se hará ninguna representación o garantía, expresa o implícita, y no se acepta ni se aceptará ninguna responsabilidad por parte de la Parte Reveladora, o por cualquiera de sus respectivos directores, funcionarios, empleados, agentes o asesores, en cuanto a, o en relación con, la exactitud o integridad de cualquier Información de propiedad exclusiva puesta a disposición de la Parte receptora o sus asesores; es responsable de hacer su propia evaluación de dicha información de propiedad exclusiva') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('El hecho de que cualquiera de las partes no haga valer sus derechos bajo este Acuerdo en cualquier momento por cualquier período no se interpretará como una renuncia a tales derechos. Si alguna parte, término o disposición de este Acuerdo se considera ilegal o inaplicable, no se verá afectada la validez ni la aplicabilidad del resto de este Acuerdo. Ninguna de las Partes podrá ceder o transferir la totalidad o parte de sus derechos bajo este Acuerdo sin el consentimiento de la otra Parte. Este Acuerdo no puede modificarse por ningún otro motivo sin el acuerdo previo por escrito de ambas Partes. Este Acuerdo constituye el entendimiento completo entre las Partes en relación con el objeto del mismo, a menos que cualquier representación o garantía hecha sobre este Acuerdo se haya hecho de manera fraudulenta y, salvo que se haga referencia o referencia expresa en este documento') }}.
                                </p>
                                <p class="text-justify">
                                    {{ ___('Este Acuerdo se regirá por las leyes de la jurisdicción en la que se encuentra la Parte divulgadora (o si la Parte divulgadora tiene su sede en más de un país, el país en el que se encuentra su sede) (el Territorio) y las partes aceptan someter las disputas que surjan de o en relación con este Acuerdo a los tribunales no exclusivos del Territorio') }}.
                                </p>



                                <h4 class="text-center pt-2">
                                    <b>
                                        {{ ___(strtoupper('CAPÍTULO VIII- CONTACTO')) }}
                                    </b>
                                </h4>
                                <h5 class="text-left">
                                    <u>
                                        <b>
                                            {{ ___(strtoupper('INFORMACIÓN DEL CONTACTO')) }}:
                                        </b>
                                    </u>
                                </h5>
                                <p class="text-justify">
                                    {{ ___('Correo electrónico de contacto') }}:
                                </p>
                                <p class="text-justify">
                                    {{ ___('Atención al cliente') }}:
                                </p>
                                <p class="text-justify">
                                    {{ ___('Dirección del Ente Comercializador') }}:
                                </p>
                            </div>
                        </div>
                    </div>{{-- .card --}}

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-payment" id="modalPrivacidad" tabindex="-1" data-backdrop="static"
        data-keyboard="false" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">

                    <div class="card-innr">
                        <div class="card-text">
                            <h4 class="text-center">
                                <b>
                                    {{ ___(strtoupper('Privacidad y política')) }}
                                </b>
                            </h4>
                            <h5 class="text-left">
                                <u>
                                    <b>
                                        {{ ___(strtoupper('POLÍTICA DE COOKIES DE')) }}
                                        {{ $token_name }}:
                                    </b>
                                </u>
                            </h5>
                            <p class="text-justify">
                                {{ ___('El contenido de las políticas de cookies que se incorpora, viene a describir los diferentes tipos de almacenamiento de datos que pueden ser inyectados para luego ser aplicados por disruptive.center como alojador del proyecto') }}
                                {{ $token_name }}
                                {{ ___('o por organizaciones de terceros en este sitio web o en nuestra aplicación móvil requiera para su mejor funcionamiento') }}.
                            </p>
                            <p class="text-justify">
                                {{ ___('Si tiene preguntas sobre ésta política de cookies, por favor comuníquese con disruptive.center') }}.
                            </p>
                            <h5 class="text-left">
                                1. ¿{{ ___('Qué debemos entender por') }} "cookie"?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('son diminutos archivos de texto que los sitios web alojan en sus servidores y que a su vez, extraen de los equipos de los usuarios que visitan dichos sitios. Es un enlazamiento o interacción de datos que ocurre entre los desarrolladores de las páginas que confieren el servicio y los equipos de sus usuarios. Se utilizan para que los sitios web y usuarios reciban retroalimentación fluida y para dirigir con mayor eficiencia la información entre ambos, según intereses puntuales') }}.
                            </p>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('tienen varias funciones, como permitirle navegar de manera eficiente en un sitio web o una aplicación móvil, recordar sus elecciones y los bienes y servicios que compró. Gracias a las cookies, los sistemas informáticos pueden') }}:
                            </p>
                            <ul class="pl-4 pb-3">
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Conocer los intereses de los usuarios') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Ofrecer contenido específico y personalizado al usuario (por ejemplo, para que éstos puedan recordar la elección de idioma que el usuario hizo en el sitio, o para mostrarle anuncios que se adaptan a sus necesidades e intereses)') }}.
                                    </li>
                                </p>
                            </ul>
                            <p class="text-justify">
                                {{ ___('Cuando hablamos de') }} cookies,
                                {{ ___('también nos referimos a tecnologías similares a las') }} cookies,
                                {{ ___('como secuencias de comandos, balizas web, píxeles, huellas dactilares y redireccionamientos. Los ') }}
                                scripts
                                ({{ ___('a veces llamados etiquetas') }})
                                {{ ___('son programas que se ejecutan en su navegador o aplicación móvil y realizan varias acciones, como enviar información a nuestros servidores') }}.
                            </p>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('pueden ser implementadas por nosotros y también por organizaciones de terceros') }}.
                            </p>
                            <h5 class="text-left">
                                2. ¿{{ ___('Qué datos recopilamos gracias a las') }} "cookies"?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('permiten recopilar datos personales relacionados con usted y transmitirnos información técnica sobre los dispositivos que utiliza, los clics que realiza y el contenido que ve. Combinamos esta información con datos personales que ya nos ha proporcionado o que hemos obtenido de fuentes externas (consulte nuestra Política de privacidad aquí)') }}.
                            </p>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('que colocamos en su terminal nos dan acceso a los siguientes datos personales relacionados con usted') }}:
                            </p>
                            <ul class="pl-4 pb-3">
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Identificadores del equipo que utiliza (dirección IP de su computadora, identificador de Android, identificador de Apple, etc)') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Tipo de sistema operativo que utiliza su terminal (Microsoft Windows, Apple Os, Linux, Unix, etc)') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Tipo y versión del software de navegación utilizado por su terminal (Microsoft Internet Explorer, Apple Safari, Mozilla Firefox, Google Chrome, etc)') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Fechas y horas de conexión a nuestros servicios') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Dirección de la página de Internet de origen') }}
                                        ("{{ ___('remitente') }}").
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Datos de navegación en nuestros servicios, contenido visto') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Información relacionada con su uso de nuestros sitios y aplicaciones') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Información relativa a la presencia de') }} cookies
                                        {{ ___('en su terminal, la hora y fecha de visualización; una página y una descripción de la página donde se coloca la baliza web') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Información sobre si lee o no los correos electrónicos que le enviamos, sobre los clics que realiza en los enlaces contenidos en estos correos electrónicos') }}.
                                    </li>
                                </p>
                            </ul>
                            <h5 class="text-left">
                                3. ¿{{ ___('Son las') }} cookies
                                {{ ___('absolutamente esenciales para que nuestro sitio/aplicación móvil funcione correctamente') }}
                                ?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Algunas') }} cookies
                                {{ ___('son necesarias para que nuestro sitio funcione') }} (Cookies
                                {{ ___('esenciales') }}").
                                {{ ___('Sin embargo, para otros, su consentimiento se recopila antes de que las cookies se implementen en su terminal') }}.
                            </p>
                            <p class="text-justify">
                                {{ ___('Las') }} cookies
                                {{ ___('esenciales permiten que nuestro sitio web/aplicaciones móviles funcionen. Sin ellos, su experiencia de usuario se vería disminuida y quizá no permitiría correr en sistemas. Las') }}
                                Cookies
                                {{ ___('Esenciales que implementamos sin su consentimiento son las siguientes') }}:
                            </p>
                            <ul class="pl-4 pb-3">
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('que registran la elección que ha expresado sobre la implementación de') }}cookies.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('utilizadas con fines de autenticación y para garantizar la seguridad de nuestro mecanismo de autenticación') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('utilizadas para la personalización de nuestra interfaz de usuario (elección de idioma o presentación de nuestro servicio), cuando dicha personalización es esencial para brindarle nuestro servicio') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('que le permiten iniciar sesión automáticamente en su cuenta, utilizando identificadores o datos que nos ha confiado previamente') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('que nos permiten asegurarnos de que la visualización de nuestro contenido sea adecuada para su dispositivo') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('para recordar su suscripción a nuestros boletines') }}.
                                    </li>
                                </p>
                            </ul>
                            <h5 class="text-left">
                                4. ¿{{ ___('Por qué usamos') }} cookies
                                {{ ___('no esenciales') }}
                                ?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Algunas') }} cookies
                                {{ ___('para las cuales recopilamos su consentimiento, se utilizan para medir nuestra audiencia, presentarle publicidad personalizada, adaptar el contenido que se muestra en nuestro sitio web/aplicación a sus preferencias y permitirle compartir el contenido de nuestro sitio web/aplicación móvil en las plataformas de redes sociales. Utilizamos') }}
                                cookies
                                {{ ___('no esenciales para lo siguiente:') }}:
                            </p>
                            <ul class="pl-4 pb-3">
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Optimización de su navegación en nuestro sitio web y aplicaciones') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Registro de información relativa a un formulario que haya rellenado en nuestro sitio (registro o acceso a su cuenta) o a productos, servicios o información que haya elegido en nuestro sitio (servicio suscrito, contenido visto, compra realizada, etc.)') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies {{ ___('de prueba A/B, es decir') }}, cookies
                                        {{ ___(' que nos permiten construir estadísticas de tráfico y pruebas destinadas a medir el rendimiento relativo de diferentes versiones de nuestro sitio web, detectar problemas de navegación en nuestro sitio o aplicación, o incluso organizar el contenido') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Gestión de nuestras plataformas y realización de operaciones técnicas internas como parte de la resolución de problemas, análisis de datos, pruebas, investigación, análisis, estudios y encuestas') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Cookies
                                        {{ ___('que nos permiten mejorar las medidas de seguridad de nuestro sitio web') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Medición de audiencia y analítica web') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Establecimiento de estadísticas y volúmenes de visitas y uso de los distintos elementos que componen nuestro sitio (secciones y contenidos visitados, recorridos) con el fin de mejorar el interés y la ergonomía de nuestros servicios') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Cálculo del número total de anuncios que mostramos en nuestros espacios publicitarios, clasificación y estadísticas') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Análisis de audiencias en base a los resultados de campañas publicitarias') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Determinación de intereses y comportamientos') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Mejora del conocimiento del cliente/usuario con fines de personalización') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Compartir sus datos personales en plataformas de redes sociales') }}.
                                    </li>
                                </p>
                            </ul>
                            <p class="text-justify">
                                {{ ___('El uso de redes sociales para interactuar con nuestros sitios y aplicaciones') }}
                                (
                                {{ ___('en particular, los botones') }} "{{ ___('me gusta') }}"
                                {{ ___('en Facebook, Twitter') }})
                                {{ ___('le permite compartir contenido de nuestro sitio web en plataformas de redes sociales. Por ejemplo, si está conectado a la red social Facebook y visita una página de nuestro sitio, es probable que Facebook recopile esta información. Asimismo, si consulta un artículo en nuestro sitio web y hace clic en el botón') }}
                                "{{ ___('twittear') }}",
                                {{ ___('Twitter recopilará esta información. Por lo tanto, lo invitamos a consultar las políticas de privacidad de cualquiera de dichas redes sociales para conocer las recopilaciones y el procesamiento que realizan sobre sus datos') }}.
                            </p>
                            <h5 class="text-left">
                                5. ¿{{ ___('Utilizamos') }} cookies
                                {{ ___('de terceros') }}
                                ?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Sí, algunas') }} cookies
                                {{ ___('son establecidas por nuestros proveedores de servicios que nos ayudan a lograr nuestros objetivos') }}
                                ({{ ___('por ejemplo, Google Tag Manager establece una') }}
                                cookies
                                {{ ___('para ayudarnos a medir nuestra audiencia') }}).
                                {{ ___('A continuación, se muestra la lista actual de terceros que implementan') }}
                                cookies
                                {{ ___('en su terminal a través de nuestro sitio web/aplicación móvil') }}.
                            </p>
                            <ul class="pl-4 pb-3">
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Nombre de la galleta') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Objetivo') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Vencimiento') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Amperio') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Análisis de usuarios del sitio web realizados por amplitud de 24 horas') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Análisis de usuarios del sitio web realizados por') }}
                                        CrazyEgg.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Detección de bots en la web realizada por') }} CloudFlare
                                        {{ ___('cada 30 días') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        Tofycoincom-locale.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Definición y mantenimiento del idioma del sitio web preferido por el usuario 1 año') }}.
                                    </li>
                                </p>
                                <p class="text-justify">
                                    <li style="list-style-type: disc;">
                                        {{ ___('Identificación de usuarios únicos realizada por') }}
                                        Google Analytics.
                                    </li>
                                </p>
                            </ul>
                            <p class="text-justify">
                                {{ ___('Estas son') }} cookies
                                {{ ___('de rendimiento que utilizamos para recopilar información sobre cómo nuestros visitantes usan el sitio web a través de') }}
                                Google Analytics.
                            </p>
                            <p class="text-justify">
                                {{ ___('La') }} cookie {{ $token_name }}
                                {{ ___('contiene una identificación única que Google usa para recordar sus preferencias y otra información, como su idioma preferido') }}.
                            </p>
                            <h5 class="text-left">
                                6. ¿{{ ___('Cómo recabamos su consentimiento') }}?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Se le pedirá que realice una acción afirmativa clara para que podamos asegurarnos de que está de acuerdo con el procesamiento de sus datos personales. Esta acción puede, por ejemplo, tomar la forma de una casilla de verificación en su herramienta en línea. Antes de obtener su consentimiento, le informaremos sistemáticamente de los nombres de las organizaciones que implementan las') }}
                                cookies,
                                {{ ___('las consecuencias de su consentimiento o rechazo, los propósitos del procesamiento y su derecho a retirar su consentimiento. Puede optar por aceptar ciertos usos que nosotros o nuestros socios haremos de sus datos y rechazar otros') }}.
                            </p>
                            <h5 class="text-left">
                                7. ¿{{ ___('Qué pasa si cambio de opinión') }}?
                            </h5>
                            <p class="text-justify">
                                {{ ___('Puede retirar su consentimiento en cualquier momento y de forma sencilla. Todo lo que necesita hacer es enviarnos un correo electrónico a') }}
                                <a href="#">support@dirruptive.com</a>
                                {{ ___('Si se niega a darnos su consentimiento para las') }}
                                cookies
                                {{ ___('no esenciales, o decide retirarlo, no sufrirá ninguna consecuencia negativa y podrá seguir utilizando el resto de nuestro servicio con normalidad') }}.
                            </p>
                        </div>
                    </div>
                </div>{{-- .card --}}
                <div class="gaps-1x"></div>
                <div class="gaps-3x d-none d-sm-block"></div>
            </div>

        </div>
    </div>
    </div>

    <div class="modal fade" id="modal-centered" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <form action="{{ route('user.send.mail.wallet') }}" class="validate-modern" method="POST"
                    id="create_page">
                    @csrf
                    <div class="popup-body">
                        <h3 class="popup-title">{{ ___('Change wallet') }} address</h3>
                        <span>{{ ___('At the moment you decide to change your wallet address you will receive an email to confirm it, once your new wallet is completed it will be saved successfully') }}</span>
                        <div class="col" style="top: 10px">
                            <input class="input-bordered" id="wal" name="wallet_dollar" required="required"
                                readonly minlength="20" value={{ $user->wallet_dollar ? $user->wallet_dollar : '' }}>
                        </div>
                        <input type="hidden" value="{{ Crypt::encrypt(auth()->user()->id) }}" name="id">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-4 d-flex justify-content-center">
                                <button id="confirm" type="submit" class="btn btn-md btn-primary"><i
                                        class="fa fa-paper-plane"></i>
                                    {{ ___('Send Confirmation') }}</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-4 d-flex justify-content-center">
                                <a href="#" class="btn btn-danger"data-dismiss="modal">
                                    <i class="fa fa-ban"></i>
                                    {{ ___('Cancel') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    <div class="modal fade" id="g2fa-modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h3 class="popup-title">{{ $user->google2fa != 1 ? ___('Enable') : ___('Disable') }}
                        {{ ___('2FA Authentication') }}</h3>
                    <form class="validate-modern" action="{{ route('user.ajax.account.update') }}" method="POST"
                        id="nio-user-2fa">
                        @csrf
                        <input type="hidden" name="action_type" value="google2fa_setup">
                        @if ($user->google2fa != 1)
                            <div class="pdb-1-5x">
                                <p><strong>{{ ___('Step 1:') }}</strong> {{ ___('Install this app from') }} <a
                                        target="_blank"
                                        href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">{{ ___('Google Play') }}
                                    </a> {{ ___('store or') }} <a target="_blank"
                                        href="https://itunes.apple.com/us/app/google-authenticator/id388497605">{{ ___('App Store') }}</a>.
                                </p>
                                <p><strong>{{ ___('Step 2:') }}</strong>
                                    {{ ___('Scan the below QR code by your Google Authenticator app, or you can add account manually.') }}
                                </p>
                                <p><strong>{{ ___('Manually add Account:') }}</strong><br>{{ ___('Account Name:') }}
                                    <strong class="text-head">{{ site_info() }}</strong> <br> {{ ___('Key:') }}
                                    <strong class="text-head">{{ $google2fa_secret }}</strong>
                                </p>
                                <div class="row g2fa-box">
                                    <div class="col-md-4">
                                        <img loading="lazy" class="img-thumbnail"
                                            src="{{ route('public.qrgen', ['text' => $google2fa]) }}" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-item">
                                            <label
                                                for="google2fa_code">{{ ___('Enter Google Authenticator Code') }}</label>
                                            <input id="google2fa_code" type="number" class="input-bordered"
                                                name="google2fa_code"
                                                placeholder="{{ ___('Enter the Code to verify') }}">
                                        </div>
                                        <input type="hidden" name="google2fa_secret" value="{{ $google2fa_secret }}">
                                        <input name="google2fa" type="hidden" value="1">
                                        <button type="submit" class="btn btn-primary">{{ ___('Confirm 2FA') }}</button>
                                    </div>
                                </div>
                                <div class="gaps-2x"></div>
                                <p class="text-danger"><strong>{{ ___('Note:') }}</strong>
                                    {{ ___('If you lost your phone or uninstall the Google Authenticator app, then you will lost access of your account.') }}
                                </p>
                            </div>
                        @else
                            <div class="pdb-1-5x">
                                <div class="input-item">
                                    <label for="google2fa_code">{{ ___('Enter Google Authenticator Code') }}</label>
                                    <input id="google2fa_code" type="number" class="input-bordered"
                                        name="google2fa_code" placeholder="{{ ___('Enter the Code to verify') }}">
                                </div>
                                <input name="google2fa" type="hidden" value="0">
                                <button type="submit" class="btn btn-primary">{{ ___('Disable 2FA') }}</button>
                            </div>
                        @endif
                    </form>
                </div>
            </div>{{-- .modal-content --}}
        </div>{{-- .modal-dialog --}}
    </div>
@endsection



@push('footer')
    <script>
        $(document).ready(function() {
            if (screen.width < 464) {
                $('.socialMenu').removeClass('d-none');
                $('.shareMenu').addClass('d-none');
            } else if (screen.width > 464) {
                $('.shareMenu').removeClass('d-none');
                $('.socialMenu').addClass('d-none');
            }

            $("#aModalWhatsappCheck").click(function() {
                limpiarOtpFields();
                var telf = "{{ $user->mobile }}";
                if (telf == null || telf == "undefined") {
                    $("#enviarCodigo").prop("disabled", "disabled");
                } else {
                    formatearNumero($("#telf").val());
                }
            });
        });
    </script>
    <script>
        $('.terminos').click(function() {
            $("#modalTerminos").modal("show");
        });
        $('.privacidad').click(function() {
            $("#modalPrivacidad").modal("show");
        });
    </script>
    <canvas id="snowflakes"></canvas>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

    <script>
        // help button
        let help = document.getElementById('help');
        let menu = document.querySelector('.menu');

        help.onclick = function() {
            menu.classList.toggle('active');
            if (menu.classList.contains('active')) {
                help.innerHTML = `<ion-icon name="close-outline"></ion-icon>`;
            } else {
                help.innerHTML = `<ion-icon name="menu-outline"><ion-icon>`;
            }
        }
    </script>
    @if ($var == 2)
        <script>
            function clickButton() {
                document.querySelector('#botonWallet').click();
            }
            clickButton()
        </script>
    @elseif($var == 3)
        <script>
            function clickButton() {
                document.querySelector('#botonsettings').click();
            }
            clickButton()
        </script>
    @else
        <script>
            function clickButton() {
                document.querySelector('#profile').click();
            }
            clickButton()
        </script>
    @endif
    {{-- Modal End --}}
    <script type="text/javascript">
        (function($) {
            var $nio_user_2fa = $('#nio-user-2fa');
            if ($nio_user_2fa.length > 0) {
                ajax_form_submit($nio_user_2fa);
            }
        })(jQuery);

        $(document).ready(function() {

            $("#btnReset").click(function() {
                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
            });
            $("#btnAgainReset").click(function() {
                // disable button
                $(this).addClass('disabled');
                $(this).css('pointer-events', 'none');
                $(this).css('cursor', 'not-allowed');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
            });
        });
    </script>
    <script>
        document.getElementById("wallet").addEventListener("keyup", test);

        function test() {
            var cero = 0;
            var wallet = document.getElementById("walle").value;
            if (wallet.length > 19) {
                $('#butt').removeClass('disabled');
            } else if (wallet.length == 0) {
                $('#butt').addClass('disabled');
            }
        }
        if (screen.width < 1202)
            $('#btnModalWhatsappCheckUpdate').html('<i class="fa fa-edit"></i>');
        else
            $('#btnModalWhatsappCheckUpdate').html("{{ ___('Actualizar') }}");
    </script>
    <script>
        $(document).ready(function() {
            $("#walle").keyup(function() {
                var value = $(this).val();
                $("#wal").val(value);
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#confirm").click(function() {
                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
            });
        })
    </script>
    <script>
        document.getElementById("wallet").addEventListener("keyup", test2);

        function test2() {
            var cero = 0;
            var wallet = document.getElementById("walle").value;
            if (wallet.length < 19) {
                $('#btnresetmail').addClass('d-none');
            } else if (wallet.length == 0) {
                $('#btnresetmail').addClass('d-none');
            } else {
                $('#btnresetmail').removeClass('d-none');
            }
        }
    </script>
    <script>
        var x = setInterval(function() {
            var deadline = document.getElementById("timervalue").value;
            var countDownDate = new Date(deadline).getTime();
            var now = new Date().getTime();
            var t = countDownDate - now;
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * 60)) / 1000);
            document.getElementById("demo").innerHTML =
                minutes + "m " + seconds + "s ";
            if (t < 0) {
                $('#btnresetmail').removeClass('d-none');
                $('#timer').addClass('d-none');
            }
        }, 1000);
    </script>
    <script>
        var x = setInterval(function() {
            var deadline = document.getElementById("timerwhatsappvalue").value;
            var countDownDate = new Date(deadline).getTime();
            var now = new Date().getTime();
            var t = countDownDate - now;
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * 60)) / 1000);
            document.getElementById("tiemporestantewhatsapp").innerHTML = "{{ ___('Renviar Codigo') }} : " +
                minutes + "m " + seconds + "s ";
            if (t < 0) {
                $('#btnModalWhatsappCheckUpdate').removeClass('disabled');
                $('#tiemporestantewhatsapp').addClass('d-none');
            }
        }, 1000);
    </script>
@endpush
