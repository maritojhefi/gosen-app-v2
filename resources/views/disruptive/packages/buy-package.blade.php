@extends('disruptive.disruptive-layout')
@section('title', ___('Purchase Token'))
@section('content')
    @php
        $has_sidebar = false;
        $content_class = 'col-lg-12';
        
        $current_date = time();
        $upcoming = is_upcoming();
        
        $_b = 0;
        $bc = base_currency();
        $default_method = token_method();
        $symbol = token_symbol();
        $method = strtolower($default_method);
        $min_token = $minimum ? $minimum : active_stage()->min_purchase;
        
        $sold_token = active_stage()->soldout + active_stage()->soldlock;
        $have_token = active_stage()->total_tokens - $sold_token;
        $sales_ended = $sold_token >= active_stage()->total_tokens || $have_token < $min_token ? true : false;
        
        $is_method = is_method_valid();
        
        $sl_01 = $is_method ? '01 ' : '';
        $sl_02 = $sl_01 ? '02 ' : '';
        $sl_03 = $sl_02 ? '03 ' : '';
        
        $exc_rate = !empty($currencies) ? json_encode($currencies) : '{}';
        $token_price = !empty($price) ? json_encode($price) : '{}';
        $amount_bonus = !empty($bonus_amount) ? json_encode($bonus_amount) : '{1 : 0}';
        $decimal_min = token('decimal_min') ? token('decimal_min') : 0;
        $decimal_max = token('decimal_max') ? token('decimal_max') : 0;
        
    @endphp

    {{-- @include('layouts.messages') --}}
    @if ($upcoming)
        <div class="alert alert-dismissible fade show alert-info" role="alert">
            <a href="javascript:void(0)" class="close" data-bs-dismiss="alert" aria-label="close">&nbsp;</a>
            {{ ___('Sales Start at') }} - {{ _date2sz(active_stage()->start_date) }}
        </div>
    @endif
    <div class="content-area card">
        <div class="card-innr">
            <form action="javascript:void(0)" method="POST" class="token-purchase">
                @csrf
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title">
                        {{ __('Choose currency and calculate :SYMBOL token price', ['symbol' => $symbol]) }}
                    </h4>
                    <div class="d-flex guttar-15px">

                        <div class="fake-class">
                            <a href="{{ route('disruptive.package') }}"
                                class="btn btn-sm btn-auto btn-primary d-sm-inline-block d-none"><em
                                    class="fas fa-arrow-left"></em><span>Back</span></a>
                            <a href="{{ route('disruptive.package') }}"
                                class="btn btn-icon btn-sm btn-primary d-sm-none"><em class="fas fa-arrow-left"></em></a>
                        </div>
                    </div>
                </div>
                <div class="card-text">
                    <p>{{ __('You can buy our :SYMBOL token using the below currency choices to become part of our project.', ['symbol' => $symbol]) }}
                    </p>
                </div>



                @if ($is_method == true)
                    <div class="token-currency-choose payment-list">
                        <div class="row guttar-15px">
                            <h5>
                                <strong>
                                    {{ strtoupper(___('Adquiere con')) }} USD
                                </strong>
                            </h5>
                            @foreach ($pm_currency as $gt => $full)
                                @if ($gt == 'usd')
                                    @if (token('purchase_' . $gt) == 1 || $method == $gt)
                                        <div class="col-sm-12 col-lg-6 col-md-6">
                                            <div class="payment-item pay-option">
                                                <input class="pay-option-check pay-method pay paymethod options"
                                                    type="radio" id="pay{{ $gt }}" name="paymethod"
                                                    value="{{ $gt }}"
                                                    {{ $default_method == strtoupper($gt) ? 'checked' : '' }}>
                                                <label class="pay-option-label{{ $is_price_show != 1 ? ' d-block' : '' }}"
                                                    for="pay{{ $gt }}">
                                                    <span class="pay-title">
                                                        <img loading="lazy" src="{{ asset('imagenes/logos/' . strtoupper($gt) . '.png') }}"
                                                            class="rounded float-left mr-2" style="width:30px"
                                                            alt="">
                                                        <span class="pay-cur">{{ strtoupper($gt) }}</span>
                                                    </span>
                                                    @php
                                                        $CurrePrice = to_num($tc->calc_token($tokenConDescuento, 'price')->$gt / $token_prices->base, 'max',',');
                                                    @endphp
                                                    @if ($is_price_show == 1 && isset($token_prices->$gt))
                                                        <span id="pricess"
                                                            class="pay-amount">{{ $monto . ' ' . token_symbol() }}
                                                            =
                                                            {{ $CurrePrice }}
                                                            {{ strtoupper($gt) }}</span>
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @if ($gt == 'bn.usd')
                                    @if (token('purchase_' . $gt) == 1 || $method == $gt)
                                        <x-package-buy-usd-wallet :montousd="to_num(
                                            ($token_prices->$gt / $token_prices->base) * $tokenConDescuento,
                                            'max',
                                        )" :tokens="$monto" :currency="$gt" />
                                    @endif
                                @endif
                            @endforeach
                            <hr>
                            <h5>
                                <strong>
                                    {{ strtoupper(___('Adquiere con')) }} CRIPTO
                                </strong>
                            </h5>
                            @foreach ($pm_currency as $gt => $full)
                                @if ($gt != 'usd' && $gt != 'bn.usd')
                                    @if (token('purchase_' . $gt) == 1 || $method == $gt)
                                        <div class="col-sm-12 col-lg-6 col-md-6">
                                            <div class="payment-item pay-option">
                                                <input class="pay-option-check pay-method pay paymethod options"
                                                    type="radio" id="pay{{ $gt }}" name="paymethod"
                                                    value="{{ $gt }}"
                                                    {{ $default_method == strtoupper($gt) ? 'checked' : '' }}>
                                                <label class="pay-option-label{{ $is_price_show != 1 ? ' d-block' : '' }}"
                                                    for="pay{{ $gt }}">
                                                    <span class="pay-title">
                                                        <img loading="lazy" src="{{ asset('imagenes/logos/' . strtoupper($gt) . '.png') }}"
                                                            class="rounded float-left mr-2" style="width:30px"
                                                            alt="">
                                                        <span class="pay-cur">{{ strtoupper($gt) }}</span>
                                                    </span>
                                                    @php
                                                        $CurrePrice = to_num($tc->calc_token($tokenConDescuento, 'price')->$gt / $token_prices->base, 'max',',');
                                                    @endphp
                                                    @if ($is_price_show == 1 && isset($token_prices->$gt))
                                                        <span id="pricess"
                                                            class="pay-amount">{{ $monto . ' ' . token_symbol() }}
                                                            =
                                                            {{ $gt == 'btc' ? $btcPrice : $CurrePrice }}
                                                            {{ strtoupper($gt) }}</span>
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @endforeach

                        </div>
                    </div>
                @else
                    <div class="token-currency-default payment-item-default">
                        <input class="pay-method" type="hidden" id="pay{{ base_currency() }}" name="paymethod"
                            value="{{ base_currency() }}" checked>
                    </div>
                @endif




                @php
                    $calc = token('calculate');
                    $input_hidden_token = $calc == 'token' ? '<input class="pay-amount" type="hidden" id="pay-amount" value="">' : '';
                    $input_hidden_amount = $calc == 'pay' ? '<input class="token-number" type="hidden" id="token-number" value="{{ $monto }}">' : '';
                    
                    $input_token_purchase = '<div class="token-pay-amount payment-get">' . $input_hidden_token . '<input class="input-bordered input-with-hint token-number" type="text" id="token-number" value="" min="' . $min_token . '" max="' . $stage->max_purchase . '"><div class="token-pay-currency"><span class="input-hint input-hint-sap payment-get-cur payment-cal-cur ucap">' . $symbol . '</span></div></div>';
                    $input_pay_amount = '<div class="token-pay-amount payment-from">' . $input_hidden_amount . '<input class="input-bordered input-with-hint pay-amount" type="text" id="pay-amount" value=""><div class="token-pay-currency"><span class="input-hint input-hint-sap payment-from-cur payment-cal-cur pay-currency ucap">' . $method . '</span></div></div>';
                    $input_token_purchase_num = '<div class="token-received"><div class="token-eq-sign">=</div><div class="token-received-amount"><h5 class="token-amount token-number-u">0.00</h5><div class="token-symbol">' . $symbol . '</div></div></div>';
                    $input_pay_amount_num = '<div class="token-received token-received-alt"><div class="token-eq-sign">=</div><div class="token-received-amount"><h5 class="token-amount pay-amount-u">0.00</h5><div class="token-symbol pay-currency ucap">' . $method . '</div></div></div>';
                    $input_sep = '<div class="token-eq-sign"><em class="fas fa-exchange-alt"></em></div>';
                    $input_pay_amount_num_usd = '<div class="token-received token-received-alt"><div class="token-eq-sign">=</div><div class="token-received-amount"><h5 id="equi" class="token-amount">0.00</h5><div class="token-symbol">USD</div></div></div>';
                    
                @endphp
                @if (is_payment_method_exist() && !$upcoming && $stage->status != 'paused' && !$sales_ended)
                    <div class="pay-buttons">
                        <div class="pay-buttons pt-0 ">
                            <a data-type="offline" href="#payment-modal"
                                class="btn btn-primary btn-between pagar payment-modal">{{ ___('Realizar el pago') }}&nbsp;<i
                                    class="ti ti-wallet"></i></a>
                        </div>
                    </div>
                    <div class="pay-notes">
                        <div class="note note-plane note-light note-md font-italic">
                            <em class="fas fa-info-circle"></em>
                            <p>{{ __('Tokens will appear in your account after payment successfully made and approved by our team. Please note that, :SYMBOL token will be distributed after the token sales end-date.', ['symbol' => $symbol]) }}
                            </p>
                        </div>
                    </div>
                @else
                    <div class="alert alert-info alert-center">
                        {{ $sales_ended ? ___('Our token sales has been finished. Thank you very much for your contribution.') : ___('Our sale will start soon. Please check back at a later date/time or feel free to contact us.') }}
                    </div>
                @endif
                <input type="hidden" id="data_amount" value="{{ $monto }}">
                <input type="hidden" id="data_currency" value="{{ $default_method }}">
                <input type="hidden" name="token_amount" id="token_amount" value="{{ $monto }}">
                @push('footer')
                    <script>
                        var t = $(".token-payment-btn"),
                            u = $("#payment-modal"),
                            v = $("#data_amount"),
                            w = $("#data_currency"),
                            u = $("#payment-modal");
                        $('.payment-modal').click(function(b) {

                            var c = $(this),
                                d = c.data("type") ? c.data("type") : "offline",
                                e = v.val(),
                                f = w.val();
                            $.ajax({
                                method: "post",
                                data: {
                                    _token: csrf_token,
                                    req_type: d,
                                    min_token: minimum_token,
                                    token_amount: e,
                                    currency: f,
                                },
                                url: access_url,
                                success: function(b) {
                                    u.find(".modal-content").html(b.modal),
                                        init_inside_modal(),
                                        u.modal("show"),
                                        0 < $("#offline_payment").length &&
                                        purchase_form_submit($("#offline_payment")),
                                        (e = f = "");
                                }
                            })
                        });
                    </script>
                @endpush
            </form>
        </div> {{-- .card-innr --}}
    </div> {{-- .content-area --}}
@endsection
@section('modals')
    <div class="modal fade modal-payment" id="payment-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection
@push('header')
    <script>
        var access_url = "{{ route('user.ajax.token.access') }}";
        var minimum_token = {{ $min_token }},
            maximum_token = {{ $stage->max_purchase }},
            token_price = {!! $token_price !!},
            token_symbol = "{{ $symbol }}",
            base_bonus = {!! $bonus !!},
            amount_bonus = {!! $amount_bonus !!},
            decimals = {
                "min": {{ $decimal_min }},
                "max": {{ $decimal_max }}
            },
            base_currency = "{{ base_currency() }}",
            base_method = "{{ $method }}";
        var max_token_msg =
            "{{ ___('Maximum you can purchase :maximum_token token per contribution.', ['maximum_token' => to_num($stage->max_purchase, 'max', ',')]) }}",
            min_token_msg =
            "{{ ___('Enter minimum :minimum_token token and select currency!', ['minimum_token' => to_num($min_token, 'max', ',')]) }}";
    </script>
@endpush
@push('footer')
    <script>
        $("#token-number").bind("keyup keydown change", function() {
            $("#equi").html(($("#token-number").val({{ $monto }}) *
                    {{ to_num($token_prices->$bc, 'max', ',') }}).toFixed(2)
                .replace(/([0-9]+(\.[0-9]+[1-9])?)(\.?0+$)/, '$1'));
        });
    </script>
    <script>
        $("input[name='paymethod']").change(function() {
            $("input[id='" + this.id + "']").click();
        });
        $('input[type="radio"]').on('click change', function(e) {
            $('.pagar').removeClass('disabled');
        });
        // $(".pagar").click(function() {
        //     $.ajax({
        //         success: function(result) {
        //             if (typeof result.message !== 'undefined') {
        //                 toastr.error(result.message);
        //             } else {
        //                 $('#ajax-modal').html(result.modal);
        //                 $('#payment-modal').modal('show')
        //             }

        //         }
        //     });
        // });
    </script>



    <script>
        $(".paymethod").click(function() {
            $('#data_amount').val($('#token_amount').val());


            var hola = $('input[name="paymethod"]:checked').val();
            $('#data_currency').val(hola);
        });
    </script>
@endpush
