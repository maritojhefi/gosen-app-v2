@extends('disruptive.disruptive-layout')
@section('title', ___('Packages'))
@section('content')
    @php
        $has_sidebar = false;
        $content_class = 'col-lg-8';
        $array = ['200', '300', '400', '500', '600', '700', '800', '900', '1000'];
    @endphp
    <div class="row">
        <div class="content-area card contenedor-paquetes-scroll">
            <div class="card-innr">
                {{-- <div class="card">
                    <div class="">
                        <h4 class="card-title text-center m-4">
                            "{{ ___('ESTO ES ESCENCIAL PARA CUALQUIER PERSONA QUE QUIERA VENDER MAS CURSOS, ENTRETENIMIENTO, PRODUCTOS O SERVICIOS EN LINEA') }}..."
                        </h4>
                    </div>
                    <div class="text-center mb-3">
                        <img loading="lazy" src="{{ asset('disruptive/imagenes/estrellas.png') }}" alt=""
                            style="height: auto; width: 123px;">
                    </div>
                </div> --}}
                <div class="token-currency-choose payment-list">
                    <div class="row guttar-15px">
                        @php
                            $contador = 1;
                        @endphp
                        @foreach ($paquetes as $paquete)
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
                                <div class="card total {{ $contador == 1 ? 'pintar' : '' }}">
                                    <div class="text-center">
                                        <div class="row">
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 body packaz"
                                                type="button" id="{{ $paquete->id }}">
                                                <img src="{{ $paquete->foto == null ? asset('disruptive/imagenes/nft-image.jpg') : asset('/imagenes/paquetes') . '/' . $paquete->foto }}"
                                                    style="height: 100%; width: auto; border-radius: 1.25rem 1.25rem 1.25rem 1.25rem;">
                                            </div>
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 body packaz"
                                                type="button" id="{{ $paquete->id }}">
                                                <div class="mt-xl-5 mt-lg-5 mt-md-5 mt-sm-5 mt-2 p-1">
                                                    <h4 class="card-title text-center text-bold">
                                                        {{ ___($paquete->nombre) }}
                                                    </h4>
                                                    <span class="text-center">
                                                        {{ $paquete->tokens . ' ' . token_symbol() }}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 body packaz"
                                                type="button" id="{{ $paquete->id }}">
                                                <div class="mt-xl-5 mt-lg-5 mt-md-5 mt-sm-5 mt-2 p-1">
                                                    <h4 class="card-title text-center text-bold">
                                                        {{ ___('Staking') }}
                                                    </h4>
                                                    <span class="text-center">
                                                        {!! $paquete->staking_info !!}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                                <div class="mt-xl-5 mt-lg-5 mt-md-5 mt-sm-5 mt-1 mb-md-5 mb-sm-5 mb-3 p-1">
                                                    <a class="btn-primary btn-block btn-sm p-2 mt-2"
                                                        style="border-radius: 15px; width:fit-content; margin: 0 auto;"
                                                        href="{{ route('disruptive.buy.package', [Crypt::encrypt($paquete->tokens), $paquete->id]) }}">
                                                        <div class="row text-center">
                                                            <div class="col-lg-12 m-1">
                                                                <i class="mdi mdi-cart-outline"
                                                                    style="font-size: 15px;"></i>
                                                                {{ ___('Adquirir') }}&nbsp;NFT
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                            $contador++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @push('sidebar')
            <x-dis.sidebar-card-component col="col-lg-4" :paquete="$paquetes[0]" />
        @endpush
    </div>
@endsection
@section('modals')
    <div class="modal fade modal-detail" id="info-paquete" tabindex="-1" data-backdrop="static" data-keyboard="false"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <h4 class="popup-title">{{ ___('Detalles del Paquete') }}</h4>
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold name-package info-paquete">{{ $paquete->nombre }}</p>
                                    <p class="sub-titulo-paquete">{{ ___('Paquete') }}</p>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <div class="percent-package row">

                                    </div>
                                    <p class="sub-titulo-paquete">{{ ___('Porcentaje de asociados') }}</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold text-primary f101 info-paquete">{{ ___('Activado') }}</p>
                                    <p class="sub-titulo-paquete">{{ ___('Formula 101') }}</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold tokens-package info-paquete">{{ $paquete->tokens }}
                                        {{ token_symbol() }}</p>
                                    <p class="sub-titulo-paquete">Tokens</p>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold stock info-paquete">{{ $paquete->stock }}</p>
                                    <p class="sub-titulo-paquete">{{ ___('Disponible') }} (stock)</p>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold value-usd info-paquete">{{ $paquete->valor_usd }} USD</p>
                                    <p class="sub-titulo-paquete">{{ ___('Valor en USD') }}</p>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold descuento info-paquete">-{{ $paquete->descuento }} %</p>
                                    <p class="sub-titulo-paquete">{{ ___('Descuento') }}</p>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 col-personalizada-paquete">
                                <div class="contenedor-detalle-paquete border text-center">
                                    <p class="fw-bold adquirir info-paquete">
                                        {{ ($paquete->valor_usd * $paquete->descuento) / 100 == 0
                                            ? $paquete->valor_usd
                                            : $paquete->valor_usd - ($paquete->valor_usd * $paquete->descuento) / 100 }}
                                        USD
                                    </p>
                                    <p class="sub-titulo-paquete">{{ ___('Adquirirlo en') }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('header')
    <style>
        .pintar {
            @if (auth()->user()->theme_color == 'style-black')
                box-shadow: 0px 0px 30px #ffffff;
            @else
                box-shadow: 0px 0px 30px {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}
            @endif
        }

        .total {
            @if (auth()->user()->theme_color == 'style-black')
                border-style: groove;
                border-width: 1px;
                border-color: rgb(177, 173, 173);
            @endif
        }

        .contenedor-paquetes-scroll {
            height: 80vh;
            overflow-y: scroll;
        }

        @media screen and (max-width: 720px) {
            .contenedor-paquetes-scroll {
                height: auto;
                overflow-y: auto;
            }
        }
    </style>
@endpush
@push('footer')
    <script>
        $(document).ready(function() {
            if (screen.width < 992) {
                $(".card-pack").addClass('d-none');
            }
            $(".total").click(function() {
                $(".total").removeClass("pintar");
                $(this).addClass("pintar");
            });
            $(".body").click(function() {
                $(".body").css("pointer-events", "none");
                var idPaquete = $(this).prop("id");
                var url = "{{ route('disruptive.package.info') }}";
                var htmloriginal = $('#package-info').html();
                $.ajax({
                    method: "get",
                    url: url + "?package=" + idPaquete,
                    beforeSend: function() {
                        $('#package-info').html(
                            `<div class="spinner-border d-block mx-auto" style="width: 5rem; height: 5rem;" role="status"></div>`
                        );
                        if (screen.width > 992) {
                            $('#backk').hide();
                            $("#logocontainer").hide();
                            $(".card-pack").removeClass('d-none');
                        } else {
                            $('#backk').show();
                            $("#logocontainer").show();
                            $(".card-pack").addClass('d-none');
                        }
                    },
                    success: function(a) {
                        $('#package-info').html(htmloriginal);
                        $(".body").removeAttr('style');
                        nivelesHabilitados(a[0].niveles);
                        if (screen.width > 992) {
                            $("#info-paquete").modal('hide')
                            $("#package-info").removeClass('d-none');
                            $(".name-package").html(a[0].nombre);
                            $(".tokens-package").html(a[0].tokens + ' ' + a[3]);
                            $(".value-usd").html(a[0].valor_usd + ' ' + a[4]);
                            $(".descuento").html('-' + a[0].descuento + ' %');
                            $(".adquirir").html(
                                (
                                    (a[0].valor_usd * (a[0].descuento) / 100) == 0 ?
                                    a[0].valor_usd :
                                    (a[0].valor_usd - (a[0].valor_usd * (a[0].descuento) /
                                        100))
                                ) +
                                ' ' + a[4]);
                            $(".stock").html(a[0].stock);
                        } else {
                            $("#info-paquete").modal('show');
                            $("#package-info").addClass('d-none');
                            $(".name-package").html(a[0].nombre);
                            $(".tokens-package").html(a[0].tokens + ' ' + a[3]);
                            $(".value-usd").html(a[0].valor_usd + ' ' + a[4]);
                            $(".descuento").html('-' + a[0].descuento + ' %');
                            $(".adquirir").html(
                                (
                                    (a[0].valor_usd * (a[0].descuento) / 100) == 0 ?
                                    a[0].valor_usd :
                                    (a[0].valor_usd - (a[0].valor_usd * (a[0].descuento) /
                                        100))
                                ) +
                                ' ' + a[4]);
                            $(".stock").html(a[0].stock);
                        }
                    }
                })
            });
        });
    </script>
@endpush
