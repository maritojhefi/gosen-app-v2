@extends('disruptive.disruptive-layout')
@section('title', ___('Progress Token'))
@push('header')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css" />
    <style>
        .pulsos {
            animation: pulsos 2s infinite;
        }

        .pulsos:hover {
            animation: none;
        }

        @-webkit-keyframes pulsos {
            0% {
                -webkit-box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'back' : 'base')) }};
            }

            70% {
                -webkit-box-shadow: 0 0 0 10px rgba(204, 169, 44, 0);
            }

            100% {
                -webkit-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
            }
        }

        @keyframes pulsos {
            0% {
                -moz-box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'back' : 'base')) }};
                box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'back' : 'base')) }};
            }

            70% {
                -moz-box-shadow: 0 0 0 10px rgba(204, 169, 44, 0);
                box-shadow: 0 0 0 10px rgba(204, 169, 44, 0);
            }

            100% {
                -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
                box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
            }
        }

        .swiper-paginations {
            position: absolute;
            top: 0%;
            display: flex;
            justify-content: end;
            padding-right: 2%;
        }

        .swiper-button-prev {
            color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }

        .swiper-button-next {
            color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
        }

        .card-balance {
            box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }} 0px 5px 15px;
        }
    </style>
@endpush
@section('content')
    <div class="content-area user-account-dashboard">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account-info card card-full-height">
                    <div class="card-innr">
                        <h4 class="card-title">{{ ucfirst(___('Tu progreso')) }}</h4>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-3">
                                @if (!str_contains($arrayProgressUser[0], 'Master Global'))
                                    <div class="card"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <div class="col-6  p-3"
                                                style="left: 10px; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px; display: grid; place-items: center; box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 0px 14px 6px -26px, {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 11px 1px 13px -6px;">
                                                <h4 class="text-white">
                                                    {{ ___(ucfirst('Nivel de Referidos')) }}:
                                                </h4>
                                                <h1 class="text-white">
                                                    <strong>
                                                        {{ $arrayProgressUser[0] }} <i class="{{ $ico }}"></i>
                                                    </strong>
                                                </h1>
                                            </div>
                                            <div class="col-6  p-3" style="display: grid; place-items: center;">
                                                <h4>
                                                    {{ ___('Proximo nivel') }}:
                                                </h4>
                                                <h1>
                                                    <strong>
                                                        {{ $arrayProgressNext[0] }} <i
                                                            class="{{ $arrayProgressNext[1] }}"></i>
                                                    </strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                @elseif(str_contains($arrayProgressUser[0], 'Master Global'))
                                    <div class="card pulsos"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }}; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }};">
                                        <div class="row text-center" style="height: 100%;">
                                            <h4 class="text-white mt-3">
                                                {{ ___(ucfirst('Tienes el nivel de Referidos mas alto')) }}:
                                            </h4>
                                            <h1 class="text-white">
                                                <strong>
                                                    {{ $arrayProgressUser[0] }} <i class="{{ $ico }}"></i>
                                                </strong>
                                            </h1>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4 col-md-4 col-sm-12 col-xs-12 mt-3">
                                @if (!str_contains($arrayProgressNext[0], 'Master Global'))
                                    <div class="card"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <div class="col-6  p-3"
                                                style="left: 10px; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px; display: grid; place-items: center; box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 0px 14px 6px -26px, {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 11px 1px 13px -6px;">
                                                <h4 class="text-white">
                                                    {{ ___(ucfirst('Tus asociados')) }}:
                                                </h4>
                                                <h1 class="text-white">
                                                    <strong>
                                                        {{ $arrayProgressUser[1] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                            <div class="col-6  p-3" style="display: grid; place-items: center;">
                                                <h4>
                                                    {{ ___('Asociados faltantes para siguiente nivel') }}:
                                                </h4>
                                                <h1>
                                                    <strong>
                                                        {{ $arrayProgressNext[2] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                @elseif(str_contains($arrayProgressUser[0], 'Master Global') && str_contains($arrayProgressNext[0], 'Master Global'))
                                    <div class="card pulsos"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }}; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <h4 class="text-white mt-3">
                                                {{ ___(ucfirst('Tus asociados')) }}:
                                            </h4>
                                            <h1 class="text-white">
                                                <strong>
                                                    {{ $arrayProgressUser[1] }}
                                                </strong>
                                            </h1>
                                        </div>
                                    </div>
                                @else
                                    <div class="card"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <div class="col-6  p-3"
                                                style="left: 10px; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px; display: grid; place-items: center; box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 0px 14px 6px -26px, {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 11px 1px 13px -6px;">
                                                <h4 class="text-white">
                                                    {{ ___(ucfirst('Tus asociados')) }}:
                                                </h4>
                                                <h1 class="text-white">
                                                    <strong>
                                                        {{ $arrayProgressUser[1] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                            <div class="col-6  p-3" style="display: grid; place-items: center;">
                                                <h4>
                                                    {{ ___('Asociados faltantes para siguiente nivel') }}:
                                                </h4>
                                                <h1>
                                                    <strong>
                                                        {{ $arrayProgressNext[2] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4 col-md-4 col-sm-12 col-xs-12 mt-3">
                                @if (!str_contains($arrayProgressNext[0], 'Master Global'))
                                    <div class="card"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <div class="col-6  p-3"
                                                style="left: 10px;background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px; display: grid; place-items: center; box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 0px 14px 6px -26px, {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 11px 1px 13px -6px;">
                                                <h4 class="text-white">
                                                    {{ ___(ucfirst('Tokens adquiridos')) }}:
                                                </h4>
                                                <h1 class="text-white">
                                                    <strong>
                                                        {{ $arrayProgressUser[2] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                            <div class="col-6  p-3" style="display: grid; place-items: center;">
                                                <h4>
                                                    {{ ucfirst(___('Podrás adquirir ')) }}tokens:
                                                </h4>
                                                <h1>
                                                    <strong>
                                                        {{ $arrayProgressNext[3] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                @elseif(str_contains($arrayProgressUser[0], 'Master Global') && str_contains($arrayProgressNext[0], 'Master Global'))
                                    <div class="card pulsos"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }}; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <h4 class="text-white mt-3">
                                                {{ ___(ucfirst('Tokens adquiridos')) }}:
                                            </h4>
                                            <h1 class="text-white ">
                                                <strong>
                                                    {{ $arrayProgressUser[2] }}
                                                </strong>
                                            </h1>
                                        </div>
                                    </div>
                                @else
                                    <div class="card"
                                        style="height: 100%; border: 2px solid {{ asset(theme_color_user(auth()->user()->theme_color, 'loader')) }} ">
                                        <div class="row text-center" style="height: 100%;">
                                            <div class="col-6  p-3"
                                                style="left: 10px; background-color:{{ asset(theme_color_user(auth()->user()->theme_color, auth()->user()->theme_color == 'style-black' ? 'sidebar' : 'base')) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px; display: grid; place-items: center; box-shadow: {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 0px 14px 6px -26px, {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }} 11px 1px 13px -6px;">
                                                <h4 class="text-white">
                                                    {{ ___(ucfirst('Tokens adquiridos')) }}:
                                                </h4>
                                                <h1 class="text-white">
                                                    <strong>
                                                        {{ $arrayProgressUser[2] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                            <div class="col-6  p-3" style="display: grid; place-items: center;">
                                                <h4>
                                                    {{ ucfirst(___('Podrás adquirir')) }}tokens:
                                                </h4>
                                                <h1>
                                                    <strong>
                                                        {{ $arrayProgressNext[3] }}
                                                    </strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($cardsUserPorTipo->card1 == 0)
                <div class="col-lg-12 col-md-12">
                    <div class="account-info card card-full-height" style="height: 221px;">
                        <div class="card-innr">
                            <h4 class="card-title">{{ ___('Proximamente...') }}</h4>
                            <div class="row">
                                <div class="col-6">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="account-info card card-full-height" style="height: auto;">
                        <div class="">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="card-title mt-2 ml-3 mb-0">{{ ___('Airdrops') }}</h4>
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <div class="swiper mySwiper">
                                <div class="swiper-paginations"></div>
                                <div class="swiper-wrapper">
                                    @foreach ($transaction as $item)
                                        @php
                                            $periodos = \App\Helpers\GosenHelper::getDataTimeFromPackage($item->checked_time, $item->paquete->meses_info);
                                            $amountBonusPackageFromTransaction = \App\Helpers\GosenHelper::amountTokensBonusPackageFromTransaction($item->id);
                                        @endphp
                                        <div class="swiper-slide" style="">
                                            <div class="row"
                                                style="margin-right: 20px; margin-left: 20px;--bs-gutter-x: 0px !important;">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                                    <div
                                                        class="row d-flex align-items-center justify-content-center mt-4 ml-3 mr-3">
                                                        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                            <div class="account-info card card-full-height card-balance">
                                                                <div
                                                                    class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                                    <div class="col-12 mr-4" style="text-align: end;">
                                                                        <span class="text-muted">
                                                                            {{ \Carbon\Carbon::parse($item->checked_time)->format('d/m/y') }}
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <h3 class="text-primary">
                                                                            NFT
                                                                        </h3>
                                                                    </div>
                                                                    <br>
                                                                    <h3 class="text-primary">
                                                                        <strong>
                                                                            {{ ___($item->paquete->nombre) }}
                                                                        </strong>
                                                                    </h3>
                                                                    <br>
                                                                    <h2 class="text-secondary">
                                                                        <strong class="text-muted">
                                                                            {{ $item->tokens }}
                                                                        </strong>
                                                                    </h2>
                                                                    <br>
                                                                    <span>
                                                                        Tokens
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                            <div class="account-info card card-full-height card-balance">
                                                                <div
                                                                    class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                                    <div class="col-12 mr-4" style="text-align: end;">
                                                                        <span class="text-muted">
                                                                            ㅤ
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <h3 class="text-primary">
                                                                            {{ ___('Día') }}
                                                                        </h3>
                                                                    </div>
                                                                    <br>
                                                                    <h3 class="text-primary">
                                                                        <strong>
                                                                            {{ number_format($item->paquete->porcentaje_bono, 4) . ' ' . '%' }}
                                                                        </strong>
                                                                    </h3>
                                                                    <br>
                                                                    <h2 class="text-secondary">
                                                                        <strong>
                                                                            {{ $periodos['days'] }}
                                                                        </strong>
                                                                    </h2>
                                                                    <br>
                                                                    <span>
                                                                        ㅤ
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                            <div class="account-info card card-full-height card-balance">
                                                                <div
                                                                    class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                                    <div class="col-12 mr-4" style="text-align: end;">
                                                                        <span class="text-muted">
                                                                            ㅤ
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <h3 class="text-primary">
                                                                            {{ ___('Mes') }}
                                                                        </h3>
                                                                    </div>
                                                                    <br>
                                                                    <h3 class="text-primary">
                                                                        <strong>
                                                                            {{ $item->paquete->porcentaje_info . ' ' . '%' }}
                                                                        </strong>
                                                                    </h3>
                                                                    <br>
                                                                    <h2 class="text-secondary">
                                                                        <strong>
                                                                            {{ $periodos['months'] }}
                                                                        </strong>
                                                                    </h2>
                                                                    <br>
                                                                    <span>
                                                                        ㅤ
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                            <div class="account-info card card-full-height card-balance">
                                                                <div
                                                                    class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                                    <div class="col-12 mr-4" style="text-align: end;">
                                                                        <span class="text-muted">
                                                                            ㅤ
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <h3 class="text-primary">
                                                                            NFT
                                                                        </h3>
                                                                    </div>
                                                                    <br>
                                                                    <h3 class="text-primary">
                                                                        <strong>
                                                                            {{ ___('Total Liberado') }}
                                                                        </strong>
                                                                    </h3>
                                                                    <br>
                                                                    <h2 class="text-secondary">
                                                                        <strong>
                                                                            {{ $amountBonusPackageFromTransaction }}
                                                                        </strong>
                                                                    </h2>
                                                                    <br>
                                                                    <span>
                                                                        Tokens
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if ($cardsUserPorTipo->card2 == 0)
                <div class="col-lg-12 col-md-12">
                    <div class="account-info card card-full-height" style="height: 221px;">
                        <div class="card-innr">
                            <h4 class="card-title">{{ ___('Proximamente...') }}</h4>
                            <div class="row">
                                <div class="col-6">
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="account-info card card-full-height" style="height: auto;">
                        <div class="">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="card-title mt-2 ml-3 mb-0">{{ ___('Balance Total') }}</h4>
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                            <div class="">
                                <div class="row"
                                    style="margin-right: 20px; margin-left: 20px;--bs-gutter-x: 0px !important;">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                        <div class="row d-flex align-items-center justify-content-center mt-4 ml-3 mr-3">
                                            <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                <div class="account-info card card-full-height card-balance">
                                                    <div
                                                        class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                        <div class="col-12 mr-4" style="text-align: end;">
                                                            <span class="text-muted">
                                                                ㅤ
                                                            </span>
                                                        </div>
                                                        <div class="col-12">
                                                            <h3 class="text-primary">
                                                                ㅤ
                                                            </h3>
                                                        </div>
                                                        <br>
                                                        <h3 class="text-primary">
                                                            <strong>
                                                                {{ ___('Bonus Launch') }}:
                                                            </strong>
                                                        </h3>
                                                        <br>
                                                        <h2 class="text-secondary">
                                                            <strong class="text-muted">
                                                                {{ $user->gift_bonus . ' ' . token_symbol() }}
                                                            </strong>
                                                        </h2>
                                                        <br>
                                                        <span>
                                                            ㅤ
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                <div class="account-info card card-full-height card-balance">
                                                    <div
                                                        class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                        <div class="col-12 mr-4" style="text-align: end;">
                                                            <span class="text-muted">
                                                                ㅤ
                                                            </span>
                                                        </div>
                                                        <div class="col-12">
                                                            <h3 class="text-primary">
                                                                ㅤ
                                                            </h3>
                                                        </div>
                                                        <br>
                                                        <h3 class="text-primary">
                                                            <strong>
                                                                {{ ___('Bonus') . ' ' . 'USDT' }}:
                                                            </strong>
                                                        </h3>
                                                        <br>
                                                        <h2 class="text-secondary">
                                                            <strong>
                                                                {{ $user->dolarBonus . ' ' . 'USDT' }}
                                                            </strong>
                                                        </h2>
                                                        <br>
                                                        <span>
                                                            ㅤ
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                <div class="account-info card card-full-height card-balance">
                                                    <div
                                                        class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                        <div class="col-12 mr-4" style="text-align: end;">
                                                            <span class="text-muted">
                                                                ㅤ
                                                            </span>
                                                        </div>
                                                        <div class="col-12">
                                                            <h3 class="text-primary">
                                                                ㅤ
                                                            </h3>
                                                        </div>
                                                        <br>
                                                        <h3 class="text-primary">
                                                            <strong>
                                                                {{ 'NFT´S' . ' ' . ___('valorados en') }}:
                                                            </strong>
                                                        </h3>
                                                        <br>
                                                        <h2 class="text-secondary">
                                                            <strong>
                                                                {{ $user->tokenBalance . ' ' . token_symbol() }}
                                                            </strong>
                                                        </h2>
                                                        <br>
                                                        <span>
                                                            ㅤ
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                <div class="account-info card card-full-height card-balance">
                                                    <div
                                                        class="row d-flex align-items-center justify-content-center mt-2 mb-2">
                                                        <div class="col-12 mr-4" style="text-align: end;">
                                                            <span class="text-muted">
                                                                ㅤ
                                                            </span>
                                                        </div>
                                                        <div class="col-12">
                                                            <h3 class="text-primary">
                                                                ㅤ
                                                            </h3>
                                                        </div>
                                                        <br>
                                                        <h3 class="text-primary">
                                                            <strong>
                                                                {{ 'NFT' . ' ' . ___('Staking') }}:
                                                            </strong>
                                                        </h3>
                                                        <br>
                                                        <h2 class="text-secondary">
                                                            <strong>
                                                                {{ $user->token_bonus . ' ' . token_symbol() }}
                                                            </strong>
                                                        </h2>
                                                        <br>
                                                        <span>
                                                            ㅤ
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@push('footer')
    <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 1,
            spaceBetween: 30,
            loop: false,
            pagination: {
                el: ".swiper-paginations",
                type: "fraction",
                position: "top",
                clickable: true
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
@endpush
