<link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/css/intlTelInput.css"
    integrity="sha512-PpUO7vLt/N4Ebtd1CU2V5b3d74Hp4/kjeSNTMuJJimwUtQiZRzvhEOyP+aQPREzUypL+xNOwQ4f5GBghyjZypw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    /* @keyframes glowing {
        0% {

            transform: scale(0.95);
        }

        40% {
            box-shadow: 0 0 20px #ff0000;
            transform: scale(1);
        }

        60% {
            box-shadow: 0 0 20px #ff0000;
            transform: scale(1);
        }

        100% {

            transform: scale(0.95);
        }
    }

    .button-glow {
        animation: glowing 2000ms infinite;
    } */









    .button-glow {
        animation: wiggle 2s linear infinite;
    }

    /* Keyframes */
    @keyframes wiggle {

        0%,
        7% {
            transform: rotateZ(0);
        }

        15% {
            transform: rotateZ(-15deg);
        }

        20% {
            transform: rotateZ(10deg);
        }

        25% {
            transform: rotateZ(-10deg);
        }

        30% {
            transform: rotateZ(6deg);
        }

        35% {
            transform: rotateZ(-4deg);
        }

        40%,
        100% {
            transform: rotateZ(0);
        }
    }














    .otp-input-fields {
        margin: auto;

        max-width: 400px;
        width: auto;
        display: flex;
        justify-content: center;
        gap: 10px;
        padding: 40px;
    }

    .otp-input-fields input {
        height: 40px;
        width: 40px;
        background-color: transparent;
        border-radius: 4px;
        border: 1px solid #2f8f1f;
        text-align: center;
        outline: none;
        font-size: 16px;
        /* Firefox */
    }

    .otp-input-fields input::-webkit-outer-spin-button,
    .otp-input-fields input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .otp-input-fields input[type=number] {
        -moz-appearance: textfield;
    }

    .otp-input-fields input:focus {
        border-width: 2px;
        border-color: #287a1a;
        font-size: 20px;
    }

    .result {
        max-width: 400px;
        margin: auto;
        padding: 24px;
        text-align: center;
    }

    .result p {
        font-size: 24px;
        font-family: "Antonio", sans-serif;
        opacity: 1;
        transition: color 0.5s ease;
    }

    .result p._ok {
        color: green;
    }

    .result p._notok {
        color: red;
        border-radius: 3px;
    }

    .intl-tel-input,
    .iti {
        width: 100%;
    }

    .flashit {
        font-size: 8px;
        border-radius: 50%;
        padding: 3px 9px 3px 9px;
        margin-left: -20px;
        margin-top: -10px;
        -webkit-animation: flash linear 2s infinite;
        animation: flash linear 2s infinite;
    }

    @-webkit-keyframes flash {
        0% {
            opacity: 1;
        }
        50% {
            opacity: .1;
        }
        100% {
            opacity: 1;
        }
    }

    @keyframes flash {
        0% {
            opacity: 1;
        }
        50% {
            opacity: .1;
        }
        100% {
            opacity: 1;
        }
    }
</style>
