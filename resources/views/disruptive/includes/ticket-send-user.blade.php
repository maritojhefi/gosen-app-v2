<script>
    $(document).ready(function() {
        $('.click-faq').click(function() {
            var id = "{{ Crypt::encrypt(auth()->user()->id) }}",
                username = "{{ Crypt::encrypt(auth()->user()->username) }}",
                name = "{{ Crypt::encrypt(auth()->user()->name) }}",
                email = "{{ Crypt::encrypt(auth()->user()->email) }}",
                country = "{{ Crypt::encrypt(auth()->user()->country) }}"
            console.log(id, username, name, email, country)
            $.ajax({
                    method: "get",
                    url: "change/status/" + id,
                    success: function(result) {
                        if (result == 'enabled') {
                            $('#bonus' + id).prop('disabled', false);
                            $('#type' + id).prop('disabled', false);
                            $('#value' + id).prop('disabled', false);
                            $("#lab" + id).html("enabled");
                        } else {
                            $('#bonus' + id).prop('disabled', true);
                            $('#type' + id).prop('disabled', true);
                            $('#value' + id).prop('disabled', true);
                            $('#buttons' + id).prop('disabled', true);
                            $("#lab" + id).html("disabled");
                        }
                    }
                })
        });
    });
</script>
