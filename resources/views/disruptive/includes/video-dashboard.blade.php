<div class="modal fade modal-payment" id="modalVideo" tabindex="-1" data-backdrop="static" data-keyboard="false"
    style="display: none; place-items:center;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered" style="max-width: none;width: 80%;height: 80%;">
        <div class="modal-content" style="padding:0 0 0 0;">
            <div class="embed-responsive embed-responsive-16by9" style="border-radius:15px;">
                <iframe
                    src="https://player.vimeo.com/video/829901660?h=ec858ad228&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
                    frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen
                    style="position:absolute;top:0;left:0;width:100%;height:100%;"
                    title="A562A425-830B-480C-B310-25973B3C8E9D" data-ready="true"></iframe>
            </div>
        </div>
    </div>
</div>
@if (auth()->user()->first_login == 0)
    <div class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" id="modal-inicial" aria-labelledby="staticBackdropLabel" style="backdrop-filter: blur(3px);" aria-modal="true">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
            <div class="modal-content p-3" style="background: #363a3f94;opacity: 1.9;">
                <div class="modal-body">
                    <div style="display: grid; place-items:center">
                        <h3 class="text-center text-white" style="font-size: 50px;">{{ $regaloInicial->name }}</h3>
                    </div>
                    <div class="text-center"><i class="fa fa-check text-primary" style="font-size: 150px;"></i></div>
                    <strong>
                        <h1 class="text-center text-white" style="font-size: 50px;">
                            {{ ___('Congratulations') }}
                        </h1>
                    </strong>
                    <div class="text-center ">
                        <h5 class="text-white">
                            ¡{{ ___('Ganaste') }} <strong>{{ $regaloInicial->total_tokens }} tokens</strong>
                            {{ ___('por suscribirte') }}!
                        </h5>
                    </div>
                    <div class="text-center">
                        <h5 class="text-white">
                            {{ ___('Obten más tokens invitando a tus amigos') }}.
                        </h5>
                    </div>
                    <div class="" style="display:flex; align-items:center; justify-content:center;">
                        <button type="button" id="continuar-regalo" class="btn btn-primary mt-3"
                            data-bs-dismiss="modal">{{ ___('Continuar') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
