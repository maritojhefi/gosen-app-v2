@extends('disruptive.disruptive-layout')
@section('metas')
    @php
        // dd(request()->path());
        $appUrl = rtrim(env('APP_URL'), '/'); // Eliminar la barra al final del valor de APP_URL si existe
        $requestPath = trim(request()->path(), '/'); // Eliminar las barras al principio y al final del valor de request()->path()
        
        // Obtener el primer segmento de la ruta
        $segments = explode('/', $requestPath);
        $firstSegment = reset($segments);
        
        // Verificar si el primer segmento ya está contenido en el appUrl
        if (strpos($appUrl, $firstSegment) === false) {
            $appUrl .= '/' . $firstSegment; // Agregar el primer segmento al final de appUrl
        }
        
    @endphp
    <base href="{{ $appUrl }}">
@endsection
@section('title', ___('User Dashboard'))
@php
    $has_sidebar = false;
    $base_currency = base_currency();
    $image = gws('welcome_img_hide', 0) == 0 ? 'welcome.png' : '';
@endphp
@push('header')
    <style>
        @media only screen and (max-width: 600px) {
            .page-content {
                padding: calc(60px) calc(24px / 2) 60px calc(24px / 2)
            }
        }
    </style>
@endpush
@section('gosencss')
    @include('user.includes.dashboard-css')
@endsection
@section('content')
    <div class="content-area user-account-dashboard">
        @livewire('user.news-modal-component')
        <div class="row">


            <div class="col-lg-12 col-md-12">
                @include('user.includes.bonus-tokens', ['pagina' => 4])
            </div>
            <div class="col-lg-12 col-md-12" style="">
                {!! UserPanel::token_revutoken('', ['class' => 'card-full-height']) !!}
            </div>
            <div class="col-lg-12 col-md-12">
                <x-dis.mapa-usuarios-component />
            </div>
            {{-- <div class="col-lg-12 col-md-12">
                @include('user.includes.bonus-tokens')
            </div> --}}
            @livewire('user.contadores-component')

            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">
                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Países más populares') }}</h4>
                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @php
                                                            $contador = 1;
                                                            @endphp
                                                            @foreach ($paises as $pais)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $pais->flag }}" alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $pais->name }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-4 text-end">
                                                                <span>{{$pais->users}}</span>
                                                            </div> --}}
                                                                </li>
                                                                @php
                                                                $contador++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Top Red de Asociados') }}</h4>

                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            {{-- {{dd($misasociados)}} --}}
                                                            @php
                                                            $contador2 = 1;
                                                            @endphp
                                                            @foreach ($misasociados as $referral)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador2 }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        @if ($referral['foto'] == null || $referral['foto'] == '')
                                                                            <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}"
                                                                                style="border-radius: 50%;width: 28px;height: 28px;">
                                                                        @else
                                                                            <img loading="lazy" style="border-radius: 50%;width: 28px;height: 28px;"
                                                                                src="{{ asset2('imagenes/perfil/' . $referral['foto']) }}">
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $referral['asociado'] }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-1 text-end">
                                                                <span>{{$referral['canrefer']}}</span>
                                                            </div> --}}
                                                                </li>
                                                                @php
                                                                $contador2++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">
                                    {{ ___('Países más populares de asociados') }}</h4>

                                <div data-simplebar="init" style="height: 296px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @php
                                                            $contador3 = 1;
                                                            @endphp
                                                            @foreach ($asociadosPorPais as $nombrePais => $array)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador3 }}</strong>
                                                                    </div>
                                                                    <div class="col-2 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $banderas[$contador3 - 1] }}"
                                                                            alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-4 p-0">
                                                                        {{ $nombrePais }}
                                                                    </div>
                                                                    <div class="col-4 text-end">
                                                                        <span>{{ array_sum($array) }}</span>
                                                                    </div>
                                                                </li>
                                                                @php
                                                                $contador3++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script src="https://player.vimeo.com/api/player.js"></script>
    @if ($usersChart)
        {!! $usersChart->script() !!}
    @endif
    <script>
        $(document).ready(function() {
            $('input:radio').on('change', function() {
                var filter = $('input:radio[name="chard"]:checked').map(function() {
                    return this.value;
                }).get().join('|');
                $('#loading').show();
                if (filter == "dia") {
                    console.log("dia");
                    window.location.href = "{{ route('user.home') }}?grafica=dia";
                } else if (filter == "semana") {
                    console.log("semana");
                    window.location.href = "{{ route('user.home') }}?grafica=semana";
                } else if (filter == "mes") {
                    console.log("mes");
                    window.location.href = "{{ route('user.home') }}?grafica=mes";
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var defecto = $("#buttoncopied").html();
            if (screen.width < 1396)
                $('#clock-dashboard').removeClass('d-none');
            else if (screen.width > 1396)
                $('#clock-dashboard').addClass('d-none');
            $('#diviframe').css('padding-bottom', '18px');
            $('#diviframe').css('padding-top', '10px');
            if (screen.width < 1000) {
                $('#card-video-mensaje').removeClass('card-video');
            } else {
                $('#card-video-mensaje').addClass('card-video');
            }
            $("#link-copy").click(function() {
                $("#link-copy").addClass('d-none');
                $("#success-copy").removeClass('d-none');
                setTimeout(() => {
                    $("#link-copy").removeClass('d-none');
                    $("#success-copy").addClass('d-none');
                }, 4000);
            });
        });

        function copiarUrl(url) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(url).select();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
    <script>
        $(document).ready(function() {

            $('#modal-inicial').modal({
                backdrop: 'static',
            })
            $('#modal-inicial').modal('show');


            $('#continuar-regalo').click(function() {
                var ruta = "{{ route('disruptive.first.login') }}"
                var first_login = {{ auth()->user()->first_login }};
                $.ajax({
                    method: "post",
                    data: {
                        first_login: first_login,
                        '_token': '{{ csrf_token() }}'
                    },
                    url: ruta
                })
            });
        });
    </script>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        let iframe = document.querySelector('iframe');
        let player = new Vimeo.Player(iframe);
        let playing = false;
        let simulationTime = 0;

        player.on('play', function(e) {
            playing = true;
        });

        player.on('pause', function(e) {
            playing = false;
        });

        /**
         * Event fired when user want to fast forward
         */
        player.on('seeked', function(e) {
            if (e.seconds > simulationTime) {
                player.setCurrentTime(simulationTime).then(function(seconds) {}).catch(function(error) {
                    switch (error.name) {
                        case 'RangeError':
                            // The time is less than 0 or greater than the video's duration
                            break;
                        default:
                            // Some other error occurred
                            break;
                    }
                });
            } else {
                simulationTime = data.seconds;
            }
        });

        /**
         * Keep time going
         */
        window.setInterval(function() {
            if (playing) {
                simulationTime++;
            }
        }, 1000);
    </script>
@endpush
@section('modals')
    @include('disruptive.includes.video-dashboard')
@endsection
