@extends('disruptive.disruptive-layout')
@section('title', ___('User Dashboard'))
@php
    $has_sidebar = false;
    $base_currency = base_currency();
    $image = gws('welcome_img_hide', 0) == 0 ? 'welcome.png' : '';
@endphp
@push('header')
    <style>
        @media only screen and (max-width: 600px) {
            .page-content {
                padding: calc(60px) calc(24px / 2) 60px calc(24px / 2)
            }
        }
    </style>
@endpush
@section('gosencss')
    @include('user.includes.dashboard-css')
@endsection
@section('content')
    <div class="content-area user-account-dashboard">
        @livewire('user.news-modal-component')
        <div class="row">
            <div class="col-lg-12 col-md-12" id="clock-dashboard" style="padding-bottom: 10px;">
                <div class="account-info card card-full-height m-0"
                    style="align-items: center; background: transparent; box-shadow: none;">
                    <div class="card-innr p-0">

                        <div class="row css-row">
                            <div class="container-clock d-block d-sm-none">
                                <h1 class="h1" id="headline"> </h1>
                                <div id="countdown">
                                    <ul>
                                        <li class="li"><span class="days"></span>{{ ___('Days') }}</li>
                                        <li class="li"><span class="hours"></span>{{ ___('Hours') }}</li>
                                        <li class="li"><span class="minutes"></span>{{ ___('Minutes') }}</li>
                                        <li class="li"><span class="seconds"></span>{{ ___('Seconds') }}</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-video" id="card-video-mensaje">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding:0px">
                    <div class=" video-innr mb-3" style="width:auto;">
                        <div style="padding:56.25% 0 0 0;position:relative;" id="frame">
                            <iframe id="vimeo-player" frameborder="0"
                                src="https://player.vimeo.com/video/770238512?h=100fa61929&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
                                allow="autoplay; fullscreen; picture-in-picture" allowfullscreen
                                style="position:absolute;top:0;left:0;width:100%;height:100%; border-radius: 17px;"
                                title="A562A425-830B-480C-B310-25973B3C8E9D"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="mb-3 d-flex justify-content-center align-items-center flex-column">
                        <div class="">
                            <h1 class="text-center">
                                <strong>
                                    "{{ ___('Ya puedes crear tu red de asociados para el futuro Disruptivo en las finanzas') }}".
                                </strong>
                            </h1>
                        </div>
                        <div class="" id="buttoncopied">

                            <a class="btn-primary btn-block btn-sm mt-4 pr-2 pulse-button" id="link-copy"
                                onClick="copiarUrl('{{ route('public.referral') . '?ref=' . substr(set_id(auth()->user()->username), 2) . '&lang=' . auth()->user()->lang }}')"
                                style="border-radius: 15px;" href="#">
                                <div class="row text-center m-1">
                                    <h4 class="text-white">
                                        {{ ___('Obtener mi enlace') }}<i class="ml-2 mdi mdi-clipboard-multiple"></i>
                                    </h4>
                                </div>
                            </a>

                            <a class="btn-success btn-block btn-sm mt-4 pr-2 d-none" id="success-copy"
                                style="border-radius: 15px;" href="#">
                                <div class="row text-center m-1">
                                    <h4 class="text-white">
                                        {{ ___('Enlace copiado') }}!<i class="ml-2 mdi mdi-check-circle"></i>
                                    </h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12" style="">
                {!! UserPanel::token_revutoken('', ['class' => 'card-full-height']) !!}
            </div>
            <div class="col-lg-12 col-md-12">
                <x-dis.mapa-usuarios-component />
            </div>
            {{-- <div class="col-lg-12 col-md-12">
                @include('user.includes.bonus-tokens')
            </div> --}}
            @livewire('user.contadores-component')

            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">
                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Países más populares') }}</h4>
                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @foreach ($paises as $pais)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $loop->iteration }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $pais->flag }}" alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $pais->name }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-4 text-end">
                                                                <span>{{$pais->users}}</span>
                                                            </div> --}}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Top Red de Asociados') }}</h4>

                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            {{-- {{dd($misasociados)}} --}}
                                                            @foreach ($misasociados as $referral)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $loop->iteration }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        @if ($referral['foto'] == null || $referral['foto'] == '')
                                                                            <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}"
                                                                                style="border-radius: 50%;width: 28px;height: 28px;">
                                                                        @else
                                                                            <img loading="lazy" style="border-radius: 50%;width: 28px;height: 28px;"
                                                                                src="{{ asset2('imagenes/perfil/' . $referral['foto']) }}">
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $referral['asociado'] }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-1 text-end">
                                                                <span>{{$referral['canrefer']}}</span>
                                                            </div> --}}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">
                                    {{ ___('Países más populares de asociados') }}</h4>

                                <div data-simplebar="init" style="height: 296px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @foreach ($asociadosPorPais as $nombrePais => $array)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $loop->iteration }}</strong>
                                                                    </div>
                                                                    <div class="col-2 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $banderas[$loop->index] }}"
                                                                            alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-4 p-0">
                                                                        {{ $nombrePais }}
                                                                    </div>
                                                                    <div class="col-4 text-end">
                                                                        <span>{{ array_sum($array) }}</span>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-12 col-md-12">
                    <div class="card card-full-height p-2" style="align-items: center;">
                        <h4 class="card-title" style=" padding-top: 14px;">{{ ___('Red de asociados Global') }}</h4>
                        <div class="card-innr pb-0" style="width: 100%;">
                            <div class="row">
                                <div class="col-1 p-0">
                                    <div class="card-title pb-2"
                                        style="transform: rotate(-90deg);position: absolute; top: 150px;">
                                        {{ ___('Asociados') }}</div>
                                </div>
                                <div class="col-10 p-0">
                                    {!! $usersChart->container() !!}
                                </div>
                                <div class="col-1 p-0">
                                    <div class="float-right position-relative">
                                        <a href="#"
                                            class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                            <em class="fa fa-sliders" style="font-size:20px; cursor:pointer;"
                                                data-toggle="tooltip" data-placement="left"
                                                data-original-title="Filters"></em> </a>
                                        <div
                                            class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                            <div id="chard">
                                                <ul class="dropdown-list dropdown-list-s2">
                                                    <li>
                                                        <input class="input-checkbox input-checkbox-sm " id="dia"
                                                            name="chard" type="radio" value="dia"
                                                            {{ $tipoGrafica == 'dia' ? 'checked' : '' }}>
                                                        <label for="dia">{{ ___('Última semana') }}</label>
                                                    </li>
                                                    <li>
                                                        <input class="input-checkbox input-checkbox-sm " id="semana"
                                                            name="chard" type="radio" value="semana"
                                                            {{ $tipoGrafica == 'semana' ? 'checked' : '' }}>
                                                        <label for="semana">{{ ___('Últimos días del mes') }} </label>
                                                    </li>
                                                    <li>
                                                        <input class="input-checkbox input-checkbox-sm " id="mes"
                                                            name="chard" type="radio" value="mes"
                                                            {{ $tipoGrafica == 'mes' ? 'checked' : '' }}>
                                                        <label for="mes">{{ ___('Últimos 6 meses') }}</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="card-title pb-2">{{ ___('Días') }}</h4>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script src="https://player.vimeo.com/api/player.js"></script>
    @if ($usersChart)
        {!! $usersChart->script() !!}
    @endif
    <script>
        $(document).ready(function() {
            $('input:radio').on('change', function() {
                var filter = $('input:radio[name="chard"]:checked').map(function() {
                    return this.value;
                }).get().join('|');
                $('#loading').show();
                if (filter == "dia") {
                    console.log("dia");
                    window.location.href = "{{ route('user.home') }}?grafica=dia";
                } else if (filter == "semana") {
                    console.log("semana");
                    window.location.href = "{{ route('user.home') }}?grafica=semana";
                } else if (filter == "mes") {
                    console.log("mes");
                    window.location.href = "{{ route('user.home') }}?grafica=mes";
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var defecto = $("#buttoncopied").html();
            if (screen.width < 1396)
                $('#clock-dashboard').removeClass('d-none');
            else if (screen.width > 1396)
                $('#clock-dashboard').addClass('d-none');
            $('#diviframe').css('padding-bottom', '18px');
            $('#diviframe').css('padding-top', '10px');
            if (screen.width < 1000) {
                $('#card-video-mensaje').removeClass('card-video');
            } else {
                $('#card-video-mensaje').addClass('card-video');
            }
            $("#link-copy").click(function() {
                $("#link-copy").addClass('d-none');
                $("#success-copy").removeClass('d-none');
                setTimeout(() => {
                    $("#link-copy").removeClass('d-none');
                    $("#success-copy").addClass('d-none');
                }, 4000);
            });
        });

        function copiarUrl(url) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(url).select();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
    <script>
        $(document).ready(function() {

            $('#modal-inicial').modal({
                backdrop: 'static',
            })
            $('#modal-inicial').modal('show');


            $('#continuar-regalo').click(function() {
                var ruta = "{{ route('disruptive.first.login') }}"
                var first_login = {{ auth()->user()->first_login }};
                $.ajax({
                    method: "post",
                    data: {
                        first_login: first_login,
                        '_token': '{{ csrf_token() }}'
                    },
                    url: ruta
                })
            });
        });
    </script>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        let iframe = document.querySelector('iframe');
        let player = new Vimeo.Player(iframe);
        let playing = false;
        let simulationTime = 0;

        player.on('play', function(e) {
            playing = true;
        });

        player.on('pause', function(e) {
            playing = false;
        });

        /**
         * Event fired when user want to fast forward
         */
        player.on('seeked', function(e) {
            if (e.seconds > simulationTime) {
                player.setCurrentTime(simulationTime).then(function(seconds) {}).catch(function(error) {
                    switch (error.name) {
                        case 'RangeError':
                            // The time is less than 0 or greater than the video's duration
                            break;
                        default:
                            // Some other error occurred
                            break;
                    }
                });
            } else {
                simulationTime = data.seconds;
            }
        });

        /**
         * Keep time going
         */
        window.setInterval(function() {
            if (playing) {
                simulationTime++;
            }
        }, 1000);
    </script>
@endpush
@section('modals')
    @include('disruptive.includes.video-dashboard')
@endsection
