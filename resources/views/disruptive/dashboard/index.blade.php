@extends('disruptive.disruptive-layout')
@section('title', ___('User Dashboard'))
@php
    $has_sidebar = false;
    $base_currency = base_currency();
    $image = gws('welcome_img_hide', 0) == 0 ? 'welcome.png' : '';
@endphp
@push('header')
    <style>
        @media only screen and (max-width: 600px) {
            .page-content {
                padding: calc(60px) calc(24px / 2) 60px calc(24px / 2)
            }
        }
    </style>
@endpush
@section('gosencss')
    @include('user.includes.dashboard-css')
@endsection
@section('content')

    <div class="content-area user-account-dashboard">
        @livewire('user.news-modal-component')
        <div class="row">


            <div class="col-lg-4">
                {!! UserPanel::user_balance_card($contribution, ['vers' => 'side', 'class' => 'card-full-height']) !!}
            </div>
            <div class="col-lg-4 col-md-6">
                {!! UserPanel::user_token_block('', ['vers' => 'buy']) !!}
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="token-statistics card card-token card-full-height"
                    style="background-image: linear-gradient(rgb(0 0 0 / 40%), rgb(0 0 0 / 40%)), url('  {{ asset('/assets/images/billete2.png') }}   '); background-size: 120% 100%;">
                    <div class="card-innr">
                        <div class="token-balance token-balance-with-icon">
                            <div class="token-balance-icon" style="background: rgba(255, 255, 255, 0);">
                                <img src="{{ asset('/assets/images/dolar.png') }}" style="width: 100px;" alt="">

                            </div>
                            <div class="token-balance-text">
                                <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">
                                    {{ ___('Dollar Balance') }}</h6>
                                <span class="lead"> {{ $monto }} <span>USD </span></span>
                            </div>
                        </div>
                        <div class="token-balance token-balance-s2">
                            <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">
                                {{ ___('Your Bonuses In Gos') }}</h6>
                            <ul class="token-balance-list">
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead">{{ $bonus->count() }}
                                    </span>
                                    <span class="sub" style="color: white;">{{ ___('Transactions') }}
                                    </span>
                                </li>
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead">{{ $bonus->groupBy('user_from')->count() }}
                                    </span>
                                    <span class="sub" style="color: white;">{{ ___('Referrals') }}
                                    </span>
                                </li>
                                <li class="token-balance-sub">
                                    <a href="{{ route('user.bonus') }}" class="btn"
                                        style="background: #2d7f3a94;
                                    color: white;">
                                        {{ ___('View more') }}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>




            <div class="col-lg-12 col-md-12" style="">
                {!! UserPanel::token_revutoken('', ['class' => 'card-full-height']) !!}
            </div>

            <div class="col-xl-12 col-lg-12-col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-innr row">
                        {{-- temporizador movil --}}
                        {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" id="clock-dashboard" style="padding-bottom: 10px;">
                        <div class="account-info card card-full-height m-0"
                            style="align-items: center; background: transparent; box-shadow: none;">
                            <div class="card-innr p-0">
            
                                <div class="row css-row">
                                    <div class="container-clock d-block d-sm-none">
                                        <h1 class="h1" id="headline"> </h1>
                                        <div id="countdown">
                                            <ul>
                                                <li class="li"><span class="days"></span>{{ ___('Days') }}</li>
                                                <li class="li"><span class="hours"></span>{{ ___('Hours') }}</li>
                                                <li class="li"><span class="minutes"></span>{{ ___('Minutes') }}</li>
                                                <li class="li"><span class="seconds"></span>{{ ___('Seconds') }}</li>
                                            </ul>
                                        </div>
            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                        {{-- primare columna texto --}}
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-2">
                            <h1 class="fw-bold title-dashboard-1">{{ ___('“La revolución Disruptiva” ha comenzado.') }}
                            </h1>
                            <h4 class="primer-parrafo-dashboard-1">
                                {{ ___('Modelos y proyectos en finanzas, metaverso, juegos, Web3 está a punto de comenzar y tú puedes ser parte de este movimiento.') }}
                            </h4>
                            <p class="segundo-parrafo-dashboard-1">
                                {{ ___('Un entorno que tiene acceso a la información y acción proporciona oportunidades.') }}
                            </p>
                            {{-- copiar enlace --}}
                            <div class="mt-2">
                                <input type="text" id="urlReferralDashboard"
                                    value="{{ route('public.referral') . '?ref=' . substr(set_id(auth()->user()->username), 2) . '&lang=' . auth()->user()->lang }}"
                                    hidden="hidden">
                                <button type="button" id="btnUrlReferralDashboard" class="btn">
                                    {{ ___('Compartir mi enlace') }} <em class="copy-icon fas fa-link"></em>
                                </button>
                            </div>
                            <p class="subtitulo-empresas mb-0 mt-3 fst-italic">{{ ___('Información respaldada en') }}:</p>
                            <div class="row m-0 text-center">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/1.png') }}"
                                        class="img-empresas m-0">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/3.png') }}"
                                        class="img-empresas m-0">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/5.png') }}"
                                        class="img-empresas m-0">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/2.png') }}"
                                        class="img-empresas m-0">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/4.png') }}"
                                        class="img-empresas m-0">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                                    <img loading="lazy" src="{{ asset('images/dashboard/empresas/6.png') }}"
                                        class="img-empresas m-0">
                                </div>
                            </div>
                        </div>

                        {{-- segunda columna video --}}
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12 mt-2">
                            <div class="adhd-top-video">
                                <div class="adhd-top-video2">
                                    <iframe id="vimeo-player" frameborder="0"
                                        src="https://player.vimeo.com/video/770238512?h=100fa61929&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
                                        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen
                                        title="A562A425-830B-480C-B310-25973B3C8E9D"></iframe>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12">
                <x-dis.mapa-usuarios-component />
            </div>
            {{-- <div class="col-lg-12 col-md-12">
            @include('user.includes.bonus-tokens')
        </div> --}}
            {{-- Contadores de registro de usuarios en Gosen/Disruptive --}}
            {{-- @livewire('user.contadores-component') --}}

            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">
                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Países más populares') }}</h4>
                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @php
                                                                $contador = 1;
                                                            @endphp
                                                            @foreach ($paises as $pais)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        <img loading="lazy" src="{{ $pais->flag }}"
                                                                            alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $pais->name }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-4 text-end">
                                                            <span>{{$pais->users}}</span>
                                                        </div> --}}
                                                                </li>
                                                                @php
                                                                    $contador++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">{{ ___('Top programa de referidos') }}
                                </h4>

                                <div data-simplebar="init" style="max-height: 330px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            {{-- {{dd($misasociados)}} --}}
                                                            @php
                                                                $contador2 = 1;
                                                            @endphp
                                                            @foreach ($misasociados as $referral)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador2 }}</strong>
                                                                    </div>
                                                                    <div class="col-4 p-0 text-center">
                                                                        @if ($referral['foto'] == null || $referral['foto'] == '')
                                                                            <img loading="lazy"
                                                                                src="{{ asset2('imagenes/user-default.jpg') }}"
                                                                                style="border-radius: 50%;width: 28px;height: 28px;">
                                                                        @else
                                                                            <img loading="lazy"
                                                                                style="border-radius: 50%;width: 28px;height: 28px;"
                                                                                src="{{ asset2('imagenes/perfil/' . $referral['foto']) }}">
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-6 p-0">
                                                                        <strong>
                                                                            {{ $referral['asociado'] }}
                                                                        </strong>
                                                                    </div>
                                                                    {{-- <div class="col-1 text-end">
                                                            <span>{{$referral['canrefer']}}</span>
                                                        </div> --}}
                                                                </li>
                                                                @php
                                                                    $contador2++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-innr">

                                <h4 class="card-title" style="font-size: 1.2em;">
                                    {{ ___('Tus referidos por país') }}</h4>

                                <div data-simplebar="init" style="height: 296px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        <ul class="list-unstyled activity-wid">
                                                            @php
                                                                $contador3 = 1;
                                                            @endphp
                                                            @foreach ($asociadosPorPais as $nombrePais => $array)
                                                                <li class="list-group-item d-flex justify-content-between align-items-center bg-theme-user"
                                                                    style="border:1px transparent">
                                                                    <div class="col-2 p-0">
                                                                        <strong># {{ $contador3 }}</strong>
                                                                    </div>
                                                                    <div class="col-2 p-0 text-center">
                                                                        <img loading="lazy"
                                                                            src="{{ $banderas[$contador3 - 1] }}"
                                                                            alt=""
                                                                            style="border-radius: 50%;width: 28px;height: 28px;">
                                                                    </div>
                                                                    <div class="col-4 p-0">
                                                                        {{ $nombrePais }}
                                                                    </div>
                                                                    <div class="col-4 text-end">
                                                                        <span>{{ array_sum($array) }}</span>
                                                                    </div>
                                                                </li>
                                                                @php
                                                                    $contador3++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-12 col-md-12">
                <div class="card card-full-height p-2" style="align-items: center;">
                    <h4 class="card-title" style=" padding-top: 14px;">{{ ___('Red de asociados Global') }}</h4>
                    <div class="card-innr pb-0" style="width: 100%;">
                        <div class="row">
                            <div class="col-1 p-0">
                                <div class="card-title pb-2"
                                    style="transform: rotate(-90deg);position: absolute; top: 150px;">
                                    {{ ___('Asociados') }}</div>
                            </div>
                            <div class="col-10 p-0">
                                {!! $usersChart->container() !!}
                            </div>
                            <div class="col-1 p-0">
                                <div class="float-right position-relative">
                                    <a href="#"
                                        class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                        <em class="fa fa-sliders" style="font-size:20px; cursor:pointer;"
                                            data-toggle="tooltip" data-placement="left"
                                            data-original-title="Filters"></em> </a>
                                    <div
                                        class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                        <div id="chard">
                                            <ul class="dropdown-list dropdown-list-s2">
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="dia"
                                                        name="chard" type="radio" value="dia"
                                                        {{ $tipoGrafica == 'dia' ? 'checked' : '' }}>
                                                    <label for="dia">{{ ___('Última semana') }}</label>
                                                </li>
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="semana"
                                                        name="chard" type="radio" value="semana"
                                                        {{ $tipoGrafica == 'semana' ? 'checked' : '' }}>
                                                    <label for="semana">{{ ___('Últimos días del mes') }} </label>
                                                </li>
                                                <li>
                                                    <input class="input-checkbox input-checkbox-sm " id="mes"
                                                        name="chard" type="radio" value="mes"
                                                        {{ $tipoGrafica == 'mes' ? 'checked' : '' }}>
                                                    <label for="mes">{{ ___('Últimos 6 meses') }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="card-title pb-2">{{ ___('Días') }}</h4>
                </div>
            </div> --}}
            </div>



            <div class="row">
                <div class="col-12 col-lg-7">
                    {!! UserPanel::token_sales_progress('', ['class' => 'card-full-height']) !!}
                </div>
                <div class="col-12 col-lg-5">
                    {!! UserPanel::transaccion_user(
                        '',
                        ['image' => $image, 'class' => 'card-full-height'],
                        route('user.transactions'),
                    ) !!}
                </div>
            </div>

            <div class="col-12 col-lg-7">
                @if (get_page('referral', 'status') == 'active' && UserPanel::get_referrals())
                    {!! UserPanel::referidos_user('', ['image' => $image, 'class' => 'card-full-height'], route('user.referral')) !!}
                @endif
            </div>
            @if (gws('user_sales_progress', 1) == 1)
            @endif
            @if (get_page('home_top', 'status') == 'active')
            @endif


        </div>
    </div>

    {{-- nuevo diseño panel --}}
    @push('header')
        <style>
            #btnUrlReferralDashboard {
                border-radius: 10px;
                background-color: #ff914c;
                color: #ffffff;
            }

            #btnUrlReferralDashboard:hover {
                border: solid 1px #ff914c;
                background-color: #ff914c;
                color: #ffffff;
            }

            .title-dashboard-1 {
                color: #000000;
            }

            .primer-parrafo-dashboard-1 {
                color: #646464;
            }

            .segundo-parrafo-dashboard-1 {
                color: #000000;
                font-size: 14px;
            }

            .subtitulo-empresas {
                font-size: 12px;
                color: #b2b2b2;
            }

            .img-empresas {
                height: 40px;
                max-height: 40px;
            }

            /* video */
            .adhd-top-video {
                width: 100%;
                height: 100%;
            }

            .adhd-top-video .adhd-top-video2 {
                position: relative;
                margin-top: -5%;
                padding: 0% 0% 60% 0%;
                box-sizing: border-box;
                background: url("{{ asset('images/dashboard/laptop.png') }}") center center no-repeat;
                background-size: contain;
                widows: 100%;
                height: 100;
            }

            .adhd-top-video iframe {
                padding: 0% 11.5% 1.5% 11.5%;
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
        </style>
    @endpush

    @push('footer')
        <script>
            // copiar enlace de referido
            $("#btnUrlReferralDashboard").click(function() {
                var valorCopiado = $("#urlReferralDashboard").val();
                navigator.clipboard.writeText(valorCopiado)
                    .then(function() {
                        toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                    })
                    .catch(function(err) {
                        toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                    });
            });
        </script>
    @endpush


@endsection
@push('footer')
    <script src="https://player.vimeo.com/api/player.js"></script>
    @if ($usersChart)
        {!! $usersChart->script() !!}
    @endif
    <script>
        $(document).ready(function() {
            $('input:radio').on('change', function() {
                var filter = $('input:radio[name="chard"]:checked').map(function() {
                    return this.value;
                }).get().join('|');
                $('#loading').show();
                if (filter == "dia") {
                    console.log("dia");
                    window.location.href = "{{ route('user.home') }}?grafica=dia";
                } else if (filter == "semana") {
                    console.log("semana");
                    window.location.href = "{{ route('user.home') }}?grafica=semana";
                } else if (filter == "mes") {
                    console.log("mes");
                    window.location.href = "{{ route('user.home') }}?grafica=mes";
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var defecto = $("#buttoncopied").html();
            if (screen.width < 1396)
                $('#clock-dashboard').removeClass('d-none');
            else if (screen.width > 1396)
                $('#clock-dashboard').addClass('d-none');
            $('#diviframe').css('padding-bottom', '18px');
            $('#diviframe').css('padding-top', '10px');
            if (screen.width < 1000) {
                $('#card-video-mensaje').removeClass('card-video');
            } else {
                $('#card-video-mensaje').addClass('card-video');
            }
            $("#link-copy").click(function() {
                $("#link-copy").addClass('d-none');
                $("#success-copy").removeClass('d-none');
                setTimeout(() => {
                    $("#link-copy").removeClass('d-none');
                    $("#success-copy").addClass('d-none');
                }, 4000);
            });
        });

        function copiarUrl(url) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(url).select();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
    <script>
        $(document).ready(function() {

            $('#modal-inicial').modal({
                backdrop: 'static',
            })
            $('#modal-inicial').modal('show');


            $('#continuar-regalo').click(function() {
                var ruta = "{{ route('disruptive.first.login') }}"
                var first_login = {{ auth()->user()->first_login }};
                $.ajax({
                    method: "post",
                    data: {
                        first_login: first_login,
                        '_token': '{{ csrf_token() }}'
                    },
                    url: ruta
                })
            });
        });
    </script>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        let iframe = document.querySelector('iframe');
        let player = new Vimeo.Player(iframe);
        let playing = false;
        let simulationTime = 0;

        player.on('play', function(e) {
            playing = true;
        });

        player.on('pause', function(e) {
            playing = false;
        });

        /**
         * Event fired when user want to fast forward
         */
        player.on('seeked', function(e) {
            if (e.seconds > simulationTime) {
                player.setCurrentTime(simulationTime).then(function(seconds) {}).catch(function(error) {
                    switch (error.name) {
                        case 'RangeError':
                            // The time is less than 0 or greater than the video's duration
                            break;
                        default:
                            // Some other error occurred
                            break;
                    }
                });
            } else {
                simulationTime = data.seconds;
            }
        });

        /**
         * Keep time going
         */
        window.setInterval(function() {
            if (playing) {
                simulationTime++;
            }
        }, 1000);
    </script>
@endpush
@section('modals')
    @include('disruptive.includes.video-dashboard')
@endsection
