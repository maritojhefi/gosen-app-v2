<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" content="notranslate" translate="no">

<head>
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">
    <link preload rel="shortcut icon" href="{{ site_favicon() }}">
    <title>@yield('title') | {{ site_whitelabel('title') }}</title>
    <link preload rel="stylesheet" href="{{ asset(style_theme('vendor')) }}">
    <link preload rel="stylesheet" href="{{ asset(style_theme('user')) }}">
    @if (recaptcha())
        <script src="https://www.google.com/recaptcha/api.js?render={{ recaptcha('site') }}"></script>
    @endif
    @stack('header')
    @if (get_setting('site_header_code', false))
        {{ html_string(get_setting('site_header_code')) }}
    @endif
</head>

<body class="user-dashboard page-user theme-modern">
    <div class="topbar-wrap">
        <div class="topbar is-sticky" style="background-color:#280F53">
            <div class="container">
                <div class="d-flex justify-content-center">
                    <a class="topbar-logo" href="{{ url('/') }}">
                        <span class="logo-lg">
                            <img loading="lazy" src="{{ asset('disruptive/logos/LOGO-BLANCO.Disruptive.png') }}" height="28">
                        </span>
                    </a>
                </div>
            </div>{{-- .container --}}
        </div>{{-- .topbar --}}
    </div>{{-- .topbar-wrap --}}

    <div class="page-content">
        <div class="container">
            @yield('content')
        </div>
    </div>{{-- .page-content --}}


</body>

</html>
