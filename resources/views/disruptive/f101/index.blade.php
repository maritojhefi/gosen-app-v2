@extends('disruptive.disruptive-layout')
@section('title', ___('F101'))
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card bg-plomo-degradado">
            <div class="card-innr">
                <div class="row">
                    <div class="col-md-6 p-0 mb-3">
                        <div class="text-center title-f-101">
                            <h5 class="mb-0">{{ ___('FORMULA') }} <span id="span-title-f-101">101</span></h5>
                            <p>{{ ___('Aprende cómo llegar a tu audiencia') }}</p>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 mb-3">
                        <img id="img-f-101" src="{{ asset('images/f-101/formula1.png') }}" alt="formula 101">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="opciones-banner-f-101" class="col-md-12 text-center text-white">
        <div class="row">
            <div class="col-md-3">
                <div class="card bg-opciones-f-101">
                    <div class="card-innr">
                        <i class="fas fa-file-video"></i>
                        <p class="mt-2">{{___('CURSOS')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-opciones-f-101">
                    <div class="card-innr">
                        <i class="fas fa-file-pdf"></i>
                        <p class="mt-2">{{___('PDF')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-opciones-f-101">
                    <div class="card-innr">
                        <i class="fas fa-video"></i>
                        <p class="mt-2">{{___('EN VIVO')}} <span id="directo-punto"><i class="text-success fas fa-circle"></i></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
