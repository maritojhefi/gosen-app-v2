<div>
    @include('user.includes.events')
    <div id="popup" class="popup">

        {{-- <div target="_blank"> --}}
            @php
            $contador = 1;
            @endphp
            @foreach ($eventos as $eve)
                @if ($contador <= 3)    
                    @if ($eve->image != null)
                        <div class="notify align-items-end" id="{{$eve->pivot->id}}">
                            <div class="row">
                                <div class="col-9"></div>
                                &nbsp;
                                <div class="col-2 close">
                                    <em class="ti ti-close" ></em>
                                </div>
                            </div>
                            <div target="_blank" href="" class="promo-content-wrap">
                                <div class="promo-content-img">
                                    <img loading="lazy" src="{{ asset2('imagenes/'.$eve->image) }}" alt="GosenLite" class="imge">
                                </div>
                                <div class="promo-content-text">
                                    <h5>{{ ___($eve->body) }}</h5>
                                    <p>{{ ___($eve->footer) }}</p>
                                </div>
                            </div>
                        </div>
                    @else
                    <div class="notify">
                        <div target="_blank" href="" class="promo-content-wrap">
                            <div class="promo-content-text">
                                <h5>{{ ___($eve->body) }}</h5>
                                <p>{{ ___($eve->footer) }}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif
                @php
                $contador++;
                @endphp
            @endforeach
        {{-- </div> --}}
    
    </div>
</div>
@push('footer')
    <script>
        $( ".notify" ).click(function() {
            console.log(this.id);
            $.ajax({
                    method: "get",
                    url: "user/delete/event/" + this.id,
                    success: function(result) {
                    console.log(result)
                    }
                })
});
        </script>
@endpush    