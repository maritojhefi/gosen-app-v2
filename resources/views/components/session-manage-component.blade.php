 <div class="row">
     <div class="col-12">
         <div class="page-title-box d-sm-flex align-items-center justify-content-between">
             <h4 class="mb-sm-0">{{ ___('Sesiones Abiertas') }}</h4>

         </div>
     </div>
 </div>
 <div class="row">
    @php
    $contador = 0;
    @endphp
     @foreach ($sesiones as $sesion)
         <div class="col-xl-3 col-sm-6">
             <div class="card">
                 <div class="card-body" style="min-height: 130px">
                     <div class="text-center">
                        @if ($sesion->id==Session::getId())
                        <small class="badge badge-success">{{___('Sesion actual')}}</small>
                        @endif
                         <div class="flex-1">
                             <h5 class="text-truncate" id="city{{ $contador }}"><a href="#"
                                     class="text-dark"></a></h5>
                             <p class="text-muted" id="browser{{ $contador }}"></p>
                             @php
                                 $dispositivo=$sesion->user_agent;
                                 $array=explode('(',$dispositivo);
                                 $array2=explode(')',$array[1]);

                             @endphp
                             @if(strpos($array2[0], 'Windows'))
                             <i class="mdi mdi-desktop-mac  me-1"></i>
                             @elseif(strpos($array2[0], 'Android'))
                             <i class="mdi mdi-cellphone  me-1"></i>
                             @else
                             <i class="mdi mdi-desktop-mac  me-1"></i>
                             @endif
                             ({{Str::limit($array2[0],25)}})
                         </div>
                     </div>

                     

                 </div>
                 <div class="card-footer">
                    
                     <a href="{{route('user.cerrar.sesion',$sesion->id)}}" class="btn btn-block btn-danger">{{ ___('Cerrar sesion') }}</a>
                 </div>
             </div>
         </div>
         @php
         $contador++;
         @endphp
     @endforeach


     @push('footer')
     <script src="{{asset('app.js')}}"></script>
         <script>
             function getFlagEmoji(countryCode) {
                 const codePoints = countryCode
                     .toUpperCase()
                     .split('')
                     .map(char => 127397 + char.charCodeAt());
                 return String.fromCodePoint(...codePoints);
             }
            
            @php
            $contador2 = 0;
            @endphp
             @foreach ($sesiones as $sesion)
                 $('document').ready(function() {
                     $.ajax({
                         method: "get",
                         url: "https://apiip.net/api/check?ip={{ $sesion->ip_address }}&accessKey=90346c1e-6b17-4d37-911c-becee7b74773",
                         success: function(result) {
                             $('#city{{ $contador2 }}').html(result.city)
                         }
                     })
                 });
                 @php
                $contador2++;
                @endphp
             @endforeach
         </script>
     @endpush

 </div>
