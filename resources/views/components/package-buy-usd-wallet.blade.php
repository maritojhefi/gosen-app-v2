<div class="col-sm-12 col-lg-6 col-md-6">
    <div class="payment-item pay-option">
        {{-- <input class="pay-option-check pay-method pay paymethod options payout" type="radio" id="pay{{ $cur }}"
            name="paymethod" value="{{ $cur }}"> --}}
        <input class="pay-option-check pay-method" type="radio" id="pay{{ $cur }}" name="paymethod"
            value="{{ $cur }}">
        <label id="option-bn-usd" class="pay-option-label" for="pay{{ $cur }}">
            <span class="pay-title">
                <img loading="lazy" src="{{ asset('imagenes/logos/' . strtoupper('usd') . '.png') }}"
                    class="rounded float-left mr-2" style="width:30px" alt="">
                <span class="pay-cur">{{ strtoupper('Bonus USD') }}</span>
            </span>
            <span id="pricess"
                class="pay-amount">{{ number_format($user->dolarBonus, 2, '.', '') . ' ' . strtoupper('usd') }}
        </label>
    </div>
</div>
@push('modals')
    <div class="modal fade" id="modalPayUsd" tabindex="-1" data-backdrop="static" data-keyboard="false"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <form class="" action="{{ route('user.ajax.payment') }}" method="POST"
                            novalidate="novalidate">
                            @csrf
                            <input type="hidden" name="pp_token" class="buy_tokens" id="token_amount"
                                value="{{ $tokens }}">
                            <input type="hidden" name="pp_currency" id="pay_currency" value="{{ $cur }}">
                            <input type="hidden" name="pay_option" id="pay-option" value="manual">
                            <h4 class="popup-title">{{ ___('Proceso de Pago') }}</h4>
                            <p class="lead">
                                <span>{{ ___('Tú estas solicitando la compra de ') }}</span><strong
                                    class="tokens_comprados_total"></strong>,
                                <span>{{ ___('con tu billetera de dolares') }}</span>
                            </p>
                            <p>
                                <span>
                                    {{ ___('Los detalles son los siguientes') }}:
                                </span>
                            </p>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card text-center">
                                        <h5 class="text-center">
                                            <strong>{{ ___('Estas comprando') }} :</strong>
                                        </h5>
                                        <div class="row mb-3">
                                            <div class="col-6">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"
                                                    width="30" height="30"
                                                    style="background: black; border-radius:60%">
                                                    <g fill="#c2a633">
                                                        <path
                                                            d="M1024 659H881.12v281.69h224.79v117.94H881.12v281.67H1031c38.51 0 316.16 4.35 315.73-327.72S1077.44 659 1024 659z" />
                                                        <path
                                                            d="M1000 0C447.71 0 0 447.71 0 1000s447.71 1000 1000 1000 1000-447.71 1000-1000S1552.29 0 1000 0zm39.29 1540.1H677.14v-481.46H549.48V940.7h127.65V459.21h310.82c73.53 0 560.56-15.27 560.56 549.48 0 574.09-509.21 531.41-509.21 531.41z" />
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="col-6">
                                                <span class="tokens_comprados">
                                                </span>
                                            </div>
                                        </div>
                                        <h5 class="text-center">
                                            <strong>{{ ___('Equivalen a') }} :</strong>
                                        </h5>
                                        <div class="row mb-3">
                                            <div class="col-6">
                                                <img loading="lazy"
                                                    src="{{ asset('imagenes/logos/' . strtoupper('usd') . '.png') }}"
                                                    class="rounded " style="width:30px" alt="">
                                            </div>
                                            <div class="col-6">
                                                <span class="tokens_comprados_usd">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card text-center">
                                        <h5 class="text-center">
                                            <strong>{{ ___('Tu balance de dolares') }} :</strong>
                                        </h5>
                                        <div class="row mb-3">
                                            <div class="col-6 text-center">
                                                <img loading="lazy"
                                                    src="{{ asset('imagenes/logos/' . strtoupper('usd') . '.png') }}"
                                                    class="rounded" style="width:30px" alt="">
                                            </div>
                                            <div class="col-6">
                                                <span>
                                                    {{ $user->dolarBonus . ' ' . strtoupper('usd') }}
                                                </span>
                                            </div>
                                        </div>
                                        <h5 class="text-center">
                                            <strong>{{ ___('Despues de la compra') }} :</strong>
                                        </h5>
                                        <div class="row mb-3">
                                            <div class="col-6">
                                                <img loading="lazy"
                                                    src="{{ asset('imagenes/logos/' . strtoupper('usd') . '.png') }}"
                                                    class="rounded " style="width:30px" alt="">
                                            </div>
                                            <div class="col-6">
                                                <span class="despues_de_compra">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class=" font-italic mgb-1-5x"><small>*
                                    {{ ___('El pago por el paquete se descontará de tu saldo de tus bonos en dolares') }}.</small>
                            </p>
                            <div class="pdb-0-5x">
                                <div class="input-item text-left">
                                    <input type="checkbox"
                                        data-msg-required="{{ ___('You should accept our terms and policy.') }}"
                                        class="input-checkbox check input-checkbox-md" id="agree-terms" name="agree"
                                        required>
                                    <label
                                        for="agree-terms">{{ ___('I hereby agree to the token purchase agreement and token sale term.') }}</label>
                                </div>
                            </div>
                            <ul class="d-flex flex-wrap align-items-center guttar-30px">
                                <li><button type="submit" class="btn btn-alt btn-primary disabled" id="buttonpayment">
                                        {{ ___('Buy Token Now') }} <em class="ti ti-arrow-right mgl-2x"></em></button>
                                </li>
                            </ul>
                            <div class="gaps-3x"></div>
                            <div class="note note-plane note-light">
                                <em class="fas fa-info-circle"></em>
                                <p class="">
                                    {{ ___('Our payment address will appear or redirect you for payment after the order is placed.') }}
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalnotusd" tabindex="-1" data-backdrop="static" data-keyboard="false"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <div class="alert alert-danger text-center">
                            <h3 class="text-danger">
                                <strong>
                                    <i
                                        class="mdi mdi-block-helper me-3"></i>{{ ___('Actualmente no tienes saldo suficiente en tu billetera de dolares para adquirir este paquete') }}
                                </strong>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-minimo" tabindex="-1" data-backdrop="static" data-keyboard="false"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <div class="alert alert-danger text-center">
                            <h3 class="text-danger">
                                <strong>
                                    <i
                                        class="mdi mdi-block-helper me-3"></i>{{ ___('Se debe Introducir un minimo de') . ' ' . $min_token . ' ' . token_symbol() . ' ' . ___('para poder comprar Tokens') }}
                                </strong>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush
@push('footer')
    <script>
        $(document).ready(function() {
            $(".payout").click(function() {


                if (parseFloat($(
                            "#token-number")
                        .val()) != '') {
                    if (parseFloat($(
                                "#token-number")
                            .val()) >= parseFloat({{ $min_token }})) {
                        if (parseFloat($(".token-dolares")
                                .html()) <= parseFloat({{ auth()->user()->dolarBonus }})) {
                            $("#modalPayUsd").modal('show');
                        } else {
                            $("#modalnotusd").modal('show');
                        }
                    } else {
                        $("#modal-minimo").modal('show');
                    }
                }
            });
            $('.check').on('click', function() {
                if ($(this).is(':checked')) {
                    $('#buttonpayment').removeClass('disabled');
                } else {
                    $('#buttonpayment').addClass('disabled');
                }
            });
        });
    </script>
@endpush
