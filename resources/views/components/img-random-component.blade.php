<div class="page-ath-gfx"
    style="
background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.3)), url({{ asset('images/landing/' . $random . '.jpg') }}); ">
    <div class="w-100 d-flex justify-content-center d-none">
        <div class="col-md-8 col-xl-5">
            <div class="content">
                @if ($random != 11)
                    <div class="visible">
                        {{-- <span class="p-text" style="font-family: 'Montserrat', sans-serif;">
                        e are <span class="p-text" style="">W</span>
                    </span> --}}
                        <p class="p-text">
                            {{ ___('We are') }}
                        </p>
                        <ul class="ul-text">
                            <li class="li-text">&nbsp;{{___('friends') }}!</li>
                            <li class="li-text">&nbsp;{{___('family') }}!</li>
                            <li class="li-text">&nbsp;{{___('awesome') }}!</li>
                        </ul>
                    </div>
                @else
                    <div class="visible" style="width: 300px;">
                        <p class="p-text">
                            {{ ___('Coming soon') }}!!!
                        </p>
                    </div>
                @endif

                {{-- </div> --}}
            </div>
            {{-- <img loading="lazy" src="{{ asset('images/intro.png') }}" alt=""> --}}
        </div>
    </div>
</div>
@if ($random == 11)
    <style>
        .claseImportante {
            background-color: #363434 !important;
        }

        .acc {
            color: #3184eb !important;
        }

        #acc2 a {
            color: #3184eb !important;
        }

        .content:after,
        .content:before {
            color: #930e20 !important;
        }

        .content:after {
            right: -44px !important;
        }

        .claseImportante:hover {
            background: #500711 !important;
        }
    </style>
    @push('footer')
        <script>
            $(document).ready(function() {
                $("#textoLogin").css({
                    "color": "#930e20"
                });
                $("#acc").addClass('acc');
                $("#acc1").addClass('acc');
                $("#btnAuth").addClass('claseImportante');
                $("#button").addClass('claseImportante');
            });
        </script>
    @endpush
@endif
