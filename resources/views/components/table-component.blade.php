<div class="container" style="text-align:center;">
    <h2>
        <strong>
            <u>{{ $title }} {{ date('d/m/y') }}</u>
        </strong>
    </h2>
</div>
<div class="p-3">
    {{ ___('*Remember that your records are updated every time you make a transaction') }}
</div>
<table class="table table-striped">
    <thead>
        <tr class="text-white" style="background-color:#273043">
            @foreach ($campos as $llave => $titulo)
                <th scope="col">{{ $titulo }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody class="list">
        @foreach ($variable as $item)
            <tr class="active">
                @foreach ($campos as $key => $value)
                    @if ($item->where($key))
                        <th scope="row">{{ $item[$key] }}</th>
                        @php
                            $campos->flip();
                        @endphp
                    @endif
                @endforeach
            </tr>
        @endforeach

    </tbody>
    <div class="main-content" style="position: fixed; z-index: 1; width: 100%;">
        <div class="card text-white mb-3" style="background-color:#364a6a">
            <div class="row" style="height: auto; padding: 12px;">
                    <strong class="" style="text-align: left; padding-left:5%;">
                        Transactions : {{ $contador }} transaction
                    </strong>
                    <strong class="" style="text-align: center; padding-left:20%;">
                        Total Tokens : {{ $totaltokens . ' ' . token_symbol() }}
                    </strong>
                    <strong class="" style="text-align: right; padding-left:20%;">
                        Total (USDT) Amount : {{ $totalusdamount . ' ' . '(USDT)' }}
                    </strong>
            </div>
        </div>
    </div>
</table>
