<div class="card card-full-height text-center">

    <div class="card-innr">

        <h4 class="card-title" style="color: #000;">{{___('Mapa de tu programa de referidos')}}</h4>


        <div id="world-map" class="tamano-mapa">

        </div>
        </div>
        
    </div>
</div>
@push('header')
    <link preload rel="stylesheet" href="{{ asset('jquery-jvectormap.css') }}" type="text/css" media="screen" />
    <style>
        @media only screen and (max-width: 600px) {
            .tamano-mapa {
                height: 300px;
            }
        }

        /* Small devices (portrait tablets and large phones, 600px and up) */
        @media only screen and (min-width: 600px) {
            .tamano-mapa {
                height: 300px;
            }
        }

        @media only screen and (min-width: 768px) {
            .tamano-mapa {
                height: 300px;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .tamano-mapa {
                height: 550px;
            }
        }

        /* Extra large devices (large laptops and desktops, 1200px and up) */
        @media only screen and (min-width: 1200px) {
            .tamano-mapa circle {
                height: 550px;
            }
        }
    </style>
@endpush
@push('footer')
    <script src="{{ asset('jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('jquery-jvectormap-world-mill.js') }}"></script>
    <script>
        // $('#world-map').vectorMap({
        //     map: 'world_mill'
        // });
        if(screen.width < 600){
            switchBtn = true;
            } else{
                switchBtn = false;
            }
        
        mapObj = new jvm.Map({
            container: $('#world-map'),
            map: "world_mill_en",
            zoomButtons : switchBtn,
            zoomOnScroll: false,
            normalizeFunction: "polynomial",
            hoverOpacity: 0.7,
            hoverColor: !1,
            regionStyle: { initial: { fill: "#d4dadd" } },
            markerStyle: {
                initial: {
                r: 9,
                fill: "#556ee6",
                "fill-opacity": 0.9,
                stroke: "#fff",
                "stroke-width": 7,
                "stroke-opacity": 0.4,
                },
                hover: { stroke: "#fff", "fill-opacity": 1, "stroke-width": 1.5 },
            },
            backgroundColor: "transparent",


        });

        
        
        var mapMarkers = [];
        var mapMarkersValues = [];
        addPlantsMarkers()


        function addPlantsMarkers() {
            var plants = {!! $jsonPaises !!};
            mapMarkers.length = 0;
            mapMarkersValues.length = 0;
            for (var i = 0, l = plants.length; i < l; i++) {
                mapMarkers.push({
                    name: plants[i].name,
                    latLng: plants[i].coords
                });
                mapMarkersValues.push(plants[i].status);
            }
            mapObj.addMarkers(mapMarkers, []);
            mapObj.series.markers[0].setValues(mapMarkersValues);
        }
    </script>
    <script> 
        // if (screen.width < 1024) 
        //    alert ("Pequeña") 
        // else 
        //    if (screen.width < 1280) 
        //       alert ("Mediana") 
        //    else 
        //       alert ("Grande") 
        // if(screen.width < 600){
        //     alert ("Pequeña") 
        // }
        // console.log(screen.width);
        </script>
@endpush
