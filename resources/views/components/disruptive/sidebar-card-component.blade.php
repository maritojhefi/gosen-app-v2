@push('header')
    <style>
        .box {
            animation-duration: 2s;
            animation-iteration-count: infinite;
            transform-origin: bottom;
        }

        .bounce-2 {
            position: absolute;
            z-index: 1;
            top: 37%;
            left: 8%;
            animation-name: bounce-2;
            animation-timing-function: ease;
        }

        @keyframes bounce-2 {
            0% {
                transform: translateY(0);
            }

            50% {
                transform: translateY(-9px);
            }

            100% {
                transform: translateY(0);
            }
        }
    </style>
@endpush
@if (strpos(Request::url(), 'user/package'))
    <div class="aside sidebar-right {{ $col }}" style=" justify-content: center; margin: 0 auto;">
        <div class="account-info card">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
        </div>
        <div class="account-info card">
            <div class="card-innr p-0">
                @if (count($paquete) > 0)
                    <div class="user-receive-wallet ">
                        <h6 class="card-title">
                            {{ ___('Tu paquete') }}: <span>{{ ___('Adquirido') }}</span>
                        </h6>
                        <div class="gaps-1x"></div>
                        <div data-simplebar="init" style="max-height: 330px;">
                            <div class="simplebar-wrapper" style="margin: 0px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper"
                                            style="height: auto; overflow: hidden scroll;">
                                            <div class="simplebar-content" style="padding: 0px;">
                                                <div class="row guttar-15px">

                                                    @foreach ($paquete as $paq)
                                                        <div class="col-4">
                                                            <div class="card " style="">
                                                                <div class="mt-4" style="text-align: center;">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 2000 2000" width="50"
                                                                        height="50"
                                                                        style="background: black; border-radius:60%">
                                                                        <g fill="#c2a633">
                                                                            <path
                                                                                d="M1024 659H881.12v281.69h224.79v117.94H881.12v281.67H1031c38.51 0 316.16 4.35 315.73-327.72S1077.44 659 1024 659z" />
                                                                            <path
                                                                                d="M1000 0C447.71 0 0 447.71 0 1000s447.71 1000 1000 1000 1000-447.71 1000-1000S1552.29 0 1000 0zm39.29 1540.1H677.14v-481.46H549.48V940.7h127.65V459.21h310.82c73.53 0 560.56-15.27 560.56 549.48 0 574.09-509.21 531.41-509.21 531.41z" />
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <h4 class="card-title text-center text-bold">
                                                                    {{ ___('Paquete') . ' ' . $paq->package }}
                                                                </h4>
                                                                <span class="text-center">
                                                                    {{ $paq->tokens . ' ' . token_symbol() }}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endforeach


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="simplebar-placeholder" style="width: auto; height: 710px;"></div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="user-receive-wallet">
                        <h6 class="card-title">
                            {{ ___('Tu paquete') }}: <span>{{ ___('No tiene ningun paquete aún') }}</span>
                        </h6>
                        <div class="gaps-1x"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@elseif(strpos(Request::url(), 'user/bonus'))
    <div class="aside sidebar-right {{ $col }}" style=" justify-content: center; margin: 0 auto;">
        <div class="account-info card"
            style="border:{{ auth()->user()->theme_color == 'style-black' ? '1px solid #d2dde9' : '' }} ; width: 104%; left: -7px;">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
        </div>
    </div>

    {{-- <div id="container">
        <div id="success-box">
            <div class="dot"></div>
            <div class="dot two"></div>
            <div class="face">
                <div class="eye"></div>
                <div class="eye right"></div>
                <div class="mouth happy"></div>
            </div>
            <div class="shadow scale"></div>
            <div class="message">
                <h1 class="alert">Success!</h1>
                <p>yay, everything is working.</p>
            </div>
            <button class="button-box">
                <h1 class="green">continue</h1>
            </button>
        </div>
        <div id="error-box">
            <div class="dot"></div>
            <div class="dot two"></div>
            <div class="face2">
                <div class="eye"></div>
                <div class="eye right"></div>
                <div class="mouth sad"></div>
            </div>
            <div class="shadow move"></div>
            <div class="message">
                <h1 class="alert">Error!</h1>
                <p>oh no, something went wrong.
            </div>
            <button class="button-box">
                <h1 class="red">try again</h1>
            </button>
        </div>
    </div> --}}
@else
    <div class="aside sidebar-right {{ $col }}">
        <div class="token-sales card">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
            <div class="card-innr p-0" style="margin-top: -50px;">
                <div class="account-info card" style="background:linear-gradient(#301362, #190e2b);">
                    <div class="card-innr p-0 text-center">
                        <strong>
                            <h5 class="text-white">
                                {{ ___('Tus Referidos') }}
                            </h5>
                        </strong>
                        <div class="col text-center">
                            <h4 class="text-white" style="font-size: 25px;"><i class="mdi mdi-account-group text-white"
                                    style="font-size: 60px; padding-right: 42px;"></i>{{ ' ' . $referidos->count() }}{{ ___(' Usuarios') }}
                            </h4>
                        </div>
                        <div class="text-center text-white">
                            <strong>
                                {{ $tipo->regalotype->name . ' ' }}<i class="mdi mdi-check-circle text-success"></i>
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@push('footer')
    <script>
        var b = $("#ajax-modal")
        var x = $(".modal-wallet");
        x.click(function(c) {
            $.ajax({
                method: "post",
                data: {
                    _token: csrf_token
                },
                url: user_wallet_address,
                success: function(a) {
                    b.html(a)
                    init_inside_modal()
                    $('#add-wallet').modal('show')
                    console.log('asd')
                }
            })
        });
    </script>
@endpush
