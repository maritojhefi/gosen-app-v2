@push('header')
    <style>
        .box {
            animation-duration: 2s;
            animation-iteration-count: infinite;
            transform-origin: bottom;
        }

        .bounce-2 {
            position: absolute;
            z-index: 1;
            top: 37%;
            left: 8%;
            animation-name: bounce-2;
            animation-timing-function: ease;
        }

        @keyframes bounce-2 {
            0% {
                transform: translateY(0);
            }

            50% {
                transform: translateY(-9px);
            }

            100% {
                transform: translateY(0);
            }
        }
    </style>
    {{-- Nueva distribucion del detalle de paquetes --}}
    <style>
        .col-personalizada-paquete{
            padding: 4px !important;
        }
        .contenedor-detalle-paquete{
            box-shadow: 10px 10px 15px -3px rgba(0,0,0,0.1);
            border-radius: 10px;
            min-height: 100px;
        }

        .contenedor-detalle-paquete .info-paquete{
            margin-top: 30px;
            margin-bottom: 20px;
            color: #869791;
            overflow-wrap: break-word;
        }

        .contenedor-detalle-paquete .info-paquete-bonus-2{
            margin-top: 18px;
            margin-bottom: 10px;
            color: #869791;
            overflow-wrap: break-word;
        }

        .contenedor-detalle-paquete .info-paquete-bonus-3{
            margin-bottom: 0px;
            color: #869791;
            overflow-wrap: break-word;
        }

        .contenedor-detalle-paquete .sub-titulo-paquete{
            font-size: 11px;
        }
    </style>
@endpush
@if (strpos(Request::url(), 'user/package'))
    <div class="aside sidebar-right {{ $col }}" style=" justify-content: center; margin: 0 auto;">
        <div class="account-info card mb-3"
            style="border:{{ auth()->user()->theme_color == 'style-black' ? '1px solid #d2dde9' : '' }}">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
        </div>
        <div class="account-info card card-pack"
            style="border:{{ auth()->user()->theme_color == 'style-black' ? '1px solid #d2dde9' : '' }}">
            <div class="card-innr">
                <div class="popup-content" id="package-info">
                    <h4 class="popup-title">{{ ___('Detalles del Paquete') }}</h4>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold name-package info-paquete">{{ $paquete->nombre }}</p>
                                <p class="sub-titulo-paquete">{{ ___('Paquete') }}</p>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <div class="row percent-package">
                                    <div class="col-12">
                                        <p class="fw-bold info-paquete">level1</p>
                                    </div>
                                </div>
                                <p class="sub-titulo-paquete">{{ ___('Porcentaje de asociados') }}</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold text-primary f101 info-paquete">{{ ___('Activado') }}</p>
                                <p class="sub-titulo-paquete">{{ ___('Formula 101') }}</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold tokens-package info-paquete">{{ $paquete->tokens }} {{ token_symbol() }}</p>
                                <p class="sub-titulo-paquete">Tokens</p>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold stock info-paquete">{{ $paquete->stock }}</p>
                                <p class="sub-titulo-paquete">{{ ___('Disponible') }} (stock)</p>
                            </div>
                        </div>
    
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold value-usd info-paquete">{{ $paquete->valor_usd }} USD</p>
                                <p class="sub-titulo-paquete">{{ ___('Valor en USD') }}</p>
                            </div>
                        </div>
    
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold descuento info-paquete">-{{ $paquete->descuento }} %</p>
                                <p class="sub-titulo-paquete">{{ ___('Descuento') }}</p>
                            </div>
                        </div>
    
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-personalizada-paquete">
                            <div class="contenedor-detalle-paquete border text-center">
                                <p class="fw-bold adquirir info-paquete">
                                    {{ ($paquete->valor_usd * $paquete->descuento) / 100 == 0
                                    ? $paquete->valor_usd
                                    : $paquete->valor_usd - ($paquete->valor_usd * $paquete->descuento) / 100 }}
                                    USD
                                </p>
                                <p class="sub-titulo-paquete">{{ ___('Adquirirlo en') }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif(strpos(Request::url(), 'user/bonus'))
    <div class="aside sidebar-right {{ $col }}" style=" justify-content: center; margin: 0 auto;">
        <div class="account-info card"
            style="border:{{ auth()->user()->theme_color == 'style-black' ? '1px solid #d2dde9' : '' }}">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
        </div>
    </div>
@else
    <div class="aside sidebar-right {{ $col }}">
        <div class="token-sales card">
            <div class="card-innr p-0">
                @if (auth()->user()->wallet_dollar == null && auth()->user()->wallet_dollar_status == 0)
                    <i class="text-white ri-emotion-normal-line"
                        style="position: absolute; z-index: 1; top: -3%; left: 8%; font-size:33px;"></i>
                    <a href="{{ route('disruptive.user.profile', 2) }}   "
                        class="btn btn-danger btn-between w-100 d-flex align-items-center justify-content-center  p-0"
                        style="border-radius: 15px; height: 52px;">
                        {{ __('Billetera (USDT) aún no registrada') }}
                    </a>
                @else
                    <i class="text-white fa fa-thumbs-up  box bounce-2"></i>
                    <a href="#"
                        class="btn btn-success  btn-between w-100  d-flex align-items-center justify-content-center  p-0 "
                        style="border-radius: 15px; height: 52px; cursor: context-menu">
                        {{ __('Billetera (USDT) registrada con exito') }}!
                    </a>
                @endif
            </div>
            <div class="card-innr" style="margin-top: -50px;">
                <div class="account-info card" style="background:linear-gradient(#301362, #190e2b);">
                    <div class="card-innr text-center">
                        <strong>
                            <h5 class="text-white">
                                {{ ___('Tus Referidos') }}
                            </h5>
                        </strong>
                        <div class="col text-center">
                            <h4 class="text-white" style="font-size: 25px;"><i class="mdi mdi-account-group text-white"
                                    style="font-size: 60px; padding-right: 42px;"></i>{{ ' ' . $referidos->count() }}{{ ___(' Usuarios') }}
                            </h4>
                        </div>
                        <div class="text-center text-white">
                            <strong>
                                {{ $tipo->regalotype->name . ' ' }}<i class="mdi mdi-check-circle text-success"></i>
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@push('footer')
    <script>
        $(document).ready(function () {
            nivelesHabilitados({!! $paquete->niveles !!});
        });

        function nivelesHabilitados(nivelesPaquete){
            var nivelesAll = nivelesPaquete;
            var htmlBonusNiveles = "";
            var niveles = $.grep(nivelesAll, function(element) {
                return element.status === 'enabled';
            });
            var clases = "";
            var clases1 = "";
            var parrafoInfo = "";
            var parrafoInfo2 = "";
            var parrafoInfo3 = "";
            for (let index = 0; index < niveles.length; index++) {
                switch(niveles.length){
                    case 1:
                        clases = "col-12";
                        parrafoInfo += "<p class='fw-bold info-paquete'>" + niveles[index].name + " " + niveles[index].value  +"%</p>";
                        break;
                    case 2:
                        clases = "col-12 info-paquete-bonus-2";
                        parrafoInfo += "<p class='fw-bold m-0'>" + niveles[index].name + " " + niveles[index].value  +"%</p>";
                        break;
                    default:
                        if(index <= 1){
                            clases = "col-6 info-paquete-bonus-2";
                            parrafoInfo2 += "<p class='fw-bold m-0'>" + niveles[index].name + " " + niveles[index].value  +"%</p>";
                        }else{
                            if(index == 2){
                                clases1 = "col-6 info-paquete";
                            }else if(index == 3){
                                clases1 = "col-6 info-paquete-bonus-2";
                            }else{
                                clases1 = "col-6 info-paquete-bonus-3";
                            }
                            parrafoInfo3 += "<p class='fw-bold m-0'>" + niveles[index].name + " " + niveles[index].value  +"%</p>";
                        }
                        break;
                }
            }
            if(niveles.length <= 2){
                htmlBonusNiveles = "<div class='" + clases + "'>" + parrafoInfo + "</div>";
            }else{
                htmlBonusNiveles = "<div class='" + clases + " border-end'>" + parrafoInfo2 + "</div><div class='" + clases1 + "'>" + parrafoInfo3 + "</div>";
            }
            $(".percent-package").html(htmlBonusNiveles);              
        }
    </script>
@endpush