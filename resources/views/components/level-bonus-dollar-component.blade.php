<div>
    @push('footer')
        <script>
            $("form").on("keyup change", function(e) {
                $(this).find('button').prop('disabled', false);
            })

            function cambiarStatus(id) {
                $.ajax({
                    method: "get",
                    url: "change/status/" + id,
                    success: function(result) {
                        if (result == 'enabled') {
                            $('#bonus' + id).prop('disabled', false);
                            $('#type' + id).prop('disabled', false);
                            $('#value' + id).prop('disabled', false);
                            $("#lab" + id).html("enabled");
                        } else {
                            $('#bonus' + id).prop('disabled', true);
                            $('#type' + id).prop('disabled', true);
                            $('#value' + id).prop('disabled', true);
                            $('#buttons' + id).prop('disabled', true);
                            $("#lab" + id).html("disabled");
                        }
                    }
                })
            }
        </script>
    @endpush
    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
        @php
            $posicion = 0;
        @endphp
        @foreach ($tiposUser as $item)
            <li class="nav-item">
                <a class="nav-link {{ $posicion == '0' ? 'active' : '' }}" data-toggle="tab"
                    href="#type_user{{ $posicion }}">{{ ___('User Type' . ' ' . $posicion) }}</a>
            </li>
        @php
            $posicion += 1;
        @endphp
        @endforeach
    </ul>{{-- .nav-tabs-line --}}
    <input type="hidden" name="" id="url" value="{{ asset2() }}">
    <div class="tab-content" id="types_user">

        <div class="tab-pane fade show active" id="type_user0">
            <div class="row">
                @php
                $contador = 1;
                @endphp
                @foreach ($bonuslvl0 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox" class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid" value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>


                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador++;
                    @endphp
                @endforeach
            </div>
        </div>

        <div class="tab-pane fade" id="type_user1">
            <div class="row">
                @php
                $contador2 = 1;
                @endphp
                @foreach ($bonuslvl1 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador2 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox" class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid" value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }} </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador2 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador2++;
                    @endphp
                @endforeach
            </div>
        </div>



        <div class="tab-pane fade" id="type_user2">
            <div class="row">
                @php
                $contador3 = 1;
                @endphp
                @foreach ($bonuslvl2 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador3 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox" class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid" value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador3 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>


                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador3++;
                    @endphp
                @endforeach
            </div>
        </div>




        <div class="tab-pane fade" id="type_user3">
            <div class="row">
                @php
                $contador4 = 1;
                @endphp
                @foreach ($bonuslvl3 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador4 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador4 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador4++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user4">
            <div class="row">
                @php
                $contador5 = 1;
                @endphp
                @foreach ($bonuslvl4 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador5 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador5 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador5++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user5">
            <div class="row">
                @php
                $contador6 = 1;
                @endphp
                @foreach ($bonuslvl5 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador6 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador6 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador6++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user6">
            <div class="row">
                @php
                $contador7 = 1;
                @endphp
                @foreach ($bonuslvl6 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador7 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador7 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador7++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user7">
            <div class="row">
                @php
                $contador8 = 1;
                @endphp
                @foreach ($bonuslvl7 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador8 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador8 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador8++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user8">
            <div class="row">
                @php
                $contador9 = 1;
                @endphp
                @foreach ($bonuslvl8 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador9 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador9 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador9++;
                    @endphp
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="type_user9">
            <div class="row">
                @php
                $contador10 = 1;
                @endphp
                @foreach ($bonuslvl9 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador10 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador10 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador10++;
                    @endphp
                @endforeach
            </div>
        </div>

        <div class="tab-pane fade" id="type_user10">
            <div class="row">
                @php
                $contador11 = 1;
                @endphp
                @foreach ($bonuslvl10 as $bon)
                    <div class="col-lg-6">
                        <div class="content-area card stage-card">
                            <div class="card-innr">
                                <div class="card-head row" style="padding-bottom: 0px;">
                                    <div class="col-9">
                                        <h6 class="card-title">{{ ___('Level') }} {{ $contador11 }}</h6>
                                    </div>
                                    <div class="input-item col-3">
                                        <input name="status" type="checkbox"
                                            class="input-switch input-switch-sm num"
                                            onclick="cambiarStatus('{{ $bon->id }}')"
                                            id="CheckBox{{ $bon->id }}"
                                            {{ $bon->status == 'enabled' ? 'checked' : '' }}>
                                        <label id="lab{{ $bon->id }}" for="CheckBox{{ $bon->id }}">
                                            {{ $bon->status }} </label>
                                    </div>
                                </div>
                                <form action="{{ route('lvlbonus.dollars') }}" id="formulario{{ $bon->id }}"
                                    class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="id" id="aid"
                                        value="{{ $bon->id }}">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Allowed') }} </label>
                                        <div class="input-wrap">
                                            <select git id="bonus{{ $bon->id }}" name="max"
                                                {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                class="select select-block select-bordered num" name="">
                                                <option value="5000">
                                                    &#x21e7; Upto 5.000 Tokens
                                                </option>
                                                <option value="10000">
                                                    &#x21e7; {{ ___('Upto') }} 10.000 Tokens
                                                </option>
                                                <option value="25000">
                                                    &#x21e7; {{ ___('Upto') }} 25.000 Tokens
                                                </option>
                                                <option value="50000">
                                                    &#x21e7; {{ ___('Upto') }} 50.000 Tokens
                                                </option>
                                                <option value="75000">
                                                    &#x21e7; {{ ___('Upto') }} 75.000 Tokens
                                                </option>
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Limit with referral bonus amount.') }}
                                        </span>
                                    </div>
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">
                                            {{ ___('Bonus Offer') }} </label>
                                        <div class="row guttar-10px">
                                            <div class="col-7">
                                                <div class="input-wrap">
                                                    <select name="type" id="type{{ $bon->id }}"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        class="select select-block select-bordered num">
                                                        <option {{ $bon->type == 'percent' ? 'selected' : '' }}
                                                            value="percent">
                                                            {{ ___('percent') }} </option>
                                                        <option {{ $bon->type == 'fixed' ? 'selected' : '' }}
                                                            value="fixed">
                                                            {{ ___('fixed') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="input-wrap">
                                                    <input type="number"
                                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                                        id="value{{ $bon->id }}" name="value"
                                                        class="input-bordered num" min="0" name=""
                                                        value="{{ $bon->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-note">{{ ___("Set 'Level") }} {{ $contador11 }}'
                                            {{ ___('bonus amount for type ') }} {{ $bon->type_user }}
                                            {{ ___(' users each time.') }}
                                        </div>
                                    </div>
                                    <button id="buttons{{ $bon->id }}" disabled
                                        {{ $bon->status == 'disabled' ? 'disabled' : '' }}
                                        class="btn btn-xs text-white btn-primary tbn" type="button"
                                        onClick="getFromData('formulario{{ $bon->id }}');">
                                        <i class="ti ti-reload"></i>
                                        <span>{{ ___('Update') }} </span>
                                    </button>
                            </div><!-- .card-innr -->
                        </div><!-- .card -->
                        </form>
                    </div><!-- .col -->
                    @php
                    $contador11++;
                    @endphp
                @endforeach
            </div>
        </div>
    </div>{{-- .tab-pane --}}
</div>
