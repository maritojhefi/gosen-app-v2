@extends('layouts.auth')
@section('title', ___('New Mail'))
@section('content')

<div class="page-ath-form">
    <h2 class="page-ath-heading">{{ ___('Add new mail') }}<small>{{ ___('We will send you a verification email') }}</small></h2>
    <form class="login-form validate validate-modern"
        action="{{route('update.mail') }}" method="POST">
        @csrf
        @include('layouts.messages')
        <div class="input-item">
            <input type="hidden" value="{{Crypt::encrypt($user->id)}}" name="id">
            <input class="input-bordered" type="text" id="email-address" name="old-email"
                                            required="required" placeholder="{{ ___('Enter Email Address') }}"
                                            value="{{ $user->email }}" readonly>
        </div>
        <div class="input-item">
            <input type="email" placeholder="{{ ___('Your new Email') }}" data-msg-required="{{ ___('Required.') }}"
                class="input-bordered{{ $errors->has('email') ? ' input-error' : '' }}" name="email"
                value="{{ old('email') }}" required autofocus>
        </div>
    
        
        {{-- <button type="submit" class="btn btn-primary btn-block">{{___('Change Mail')}}</button> --}}
        <button  id="btnReset" class="btn btn-primary btn-block"> {{___('Change Mail')}}</button>
    </form>

    <div class="gaps-4x"></div>
    <div class="form-note">
        {{-- {{___('Don’t have an account?')}} <a href="{{ route('register') }}"> <strong>{{___('Sign up here')}}</strong></a> --}}
    </div>
</div>

@endsection
@push('footer')
<script>
    $(document).ready(function() {
            $("#btnReset").click(function() {
                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
            });
        });
</script>
@endpush