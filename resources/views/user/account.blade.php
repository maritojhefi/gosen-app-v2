@extends('layouts.user')
@section('title', ___('User Account'))
@section('content')
@include('layouts.messages')
    <div class="content-area card">
        <div class="card-innr">

            <div class="card-head">
                <h4 class="card-title">{{ ___('Profile Details') }}</h4>
            </div>
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#personal-data">{{ ___('Personal Data') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#settings">{{ ___('Settings') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#password">{{ ___('Password') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="botonWallet" href="#wallet">{{ ___('Wallet') }}</a>
                </li>
            </ul>{{-- .nav-tabs-line --}}
            <div class="tab-content" id="profile-details">
                <div class="tab-pane fade show active" id="personal-data">
                    <form class="validate-modern" action="{{ route('user.ajax.account.update') }}" method="POST"
                        id="nio-user-personal" autocomplete="off">
                        @csrf
                        <input type="hidden" name="action_type" value="personal_data">
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="user-name" class="input-item-label">{{ ___('User Name') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="user-name" name="username"
                                            required="required" placeholder="{{ ___('Enter User Name') }}" minlength="3"
                                            value="{{ $user->username }}" readonly>
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="full-name" class="input-item-label">{{ ___('Full Name') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="full-name" name="name"
                                            required="required" placeholder="{{ ___('Enter Full Name') }}" minlength="3"
                                            value="{{ $user->name }}">
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="email-address" class="input-item-label">{{ ___('Email Address') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="email-address" name="email"
                                            required="required" placeholder="{{ ___('Enter Email Address') }}"
                                            value="{{ $user->email }}" readonly>
                                        <div class="note note-plane note-info">
                                            <em class="fas fa-info-circle"></em>
                                            <p>{{ ___('Request change of mail.') }}
                                                <a href="{{ route('send.Changemail', Crypt::encrypt($user->id)) }}"
                                                    style="color: #5664d2;" id="btnAgainReset">{{___('Send!')}}</a>
                                            </p> 
                                        </div>
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="mobile-number" class="input-item-label">{{ ___('Mobile Number') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="mobile-number" name="mobile"
                                            placeholder="{{ ___('Enter Mobile Number') }}" value="{{ $user->mobile }}">
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="date-of-birth" class="input-item-label">{{ ___('Date of Birth') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered date-picker-dob" type="text" id="date-of-birth"
                                            name="dateOfBirth" required="required" placeholder="mm/dd/yyyy"
                                            value="{{ $user->dateOfBirth != null ? _date2sz($user->dateOfBirth, 'm/d/Y') : '' }}">
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>{{-- .col --}}
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="nationality" class="input-item-label">{{ ___('Nationality') }}</label>
                                    <div class="input-wrap">
                                        <select class="select-bordered select-block" name="nationality" id="nationality"
                                            required="required" data-dd-class="search-on">
                                            <option value="">{{ ___('Select Country') }}</option>
                                            @foreach ($countries as $country)
                                                <option
                                                    {{ $user->nationality == $country ? 'selected ' : '' }}value="{{ $country }}">
                                                    {{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>{{-- .input-item --}}
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="gaps-1x"></div>{{-- 10px gap --}}
                        <div class="d-sm-flex justify-content-between align-items-center">
                            <button type="submit" class="btn btn-primary">{{ ___('Update Profile') }}</button>
                            <div class="gaps-2x d-sm-none"></div>
                        </div>
                    </form>{{-- form --}}

                </div>{{-- .tab-pane --}}



                <div class="tab-pane fade" id="settings">
                    <form class="validate-modern" action="{{ route('user.ajax.account.update') }}" method="POST"
                        id="nio-user-settings">
                        @csrf
                        <input type="hidden" name="action_type" value="account_setting">
                        <div class="pdb-1-5x mt-3">
                            <h5 class="card-title card-title-sm text-dark">{{ ___('Security Settings') }}</h5>
                        </div>
                        <div class="input-item">
                            <input name="save_activity" class="input-switch input-switch-sm" type="checkbox"
                                {{ $userMeta->save_activity == 'TRUE' ? 'checked' : '' }} id="activitylog">
                            <label for="activitylog">{{ ___('Save my activities log') }}</label>
                        </div>
                        <div class="input-item">
                            <input class="input-switch input-switch-sm" type="checkbox"
                                @if ($userMeta->unusual == 1) checked="" @endif name="unusual" id="unuact">
                            <label
                                for="unuact">{{ ___('Alert me by email in case of unusual activity in my account') }}</label>
                        </div>
                        <div class="gaps-1x"></div>
                        <div class="d-sm-flex justify-content-between align-items-center">
                            <button type="submit" class="btn btn-primary">{{ ___('Update') }}</button>
                            <div class="gaps-2x d-sm-none"></div>
                        </div>
                    </form>
                </div>{{-- .tab-pane --}}

                <div class="tab-pane fade" id="password">


                    <div class="note note-plane note-info pdb-1x pt-5 pl-0">
                        {{-- <em class="fas fa-info-circle pt-5" style="font-size: large; top:3px"></em> --}}
                        <h5>{{ ___('We will send a message to your current email address') }}
                        </h5>
                    </div>
                    <div class="note note-plane note-danger pdb-2x">
                        <em class="fas fa-info-circle"></em>
                        <p>{{ ___('Your password will only change after your confirmation by email.') }}</p>
                    </div>
                    <div class="gaps-1x"></div>{{-- 10px gap --}}
                    <div class="d-sm-flex align-items-center">
                        @if ($user->reset_status == 1)
                            <div class="note note-plane note-info pdb-2x">
                                <em class="fas fa-info-circle"></em>
                                <p>{{ ___('Email sent, check your inbox.') }}
                                    <a href="{{ route('send.mail', Crypt::encrypt($user->id)) }}" style="color: #280f53;"
                                        id="btnAgainReset">{{___('Send again?')}}</a>
                                </p>
                            </div>
                        @else
                            <a href="{{ route('send.mail', Crypt::encrypt($user->id)) }}" id="btnReset"
                                class="btn btn-primary">
                                <em class="fa fa-key"></em> {{___('Reset Password')}}</a>
                        @endif
                        <div class="gaps-2x d-sm-none"></div>
                    </div>

                </div>{{-- .tab-pane --}}


                <div class="tab-pane fade wallet" id="wallet">
                    <form class="validate-modern" action="#" method="POST" id="nio-user-personal"
                        autocomplete="off">
                        @csrf
                        <input type="hidden" name="action_type" value="personal_data">
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="user-name"
                                        class="input-item-label">{{ ___('Your wallet address (USDT)') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" id="walle" name="wallet"
                                            required="required" value="{{ $user->wallet_dollar }}"
                                            placeholder="{{___('Enter a wallet (USDT), validate to add it to your account')}}"
                                            minlength="20" value="">
                                    </div>
                                </div>{{-- .input-item --}}
                                <div class="d-sm-flex justify-content-between align-items-center">
                                    @if ($user->wallet_dollar != null && $user->wallet_dollar_status == 1)
                                        <a href="#" data-toggle="modal" data-target="#modal-centered"
                                            class="btn btn-primary butt disabled" id="butt"><i class="fas fa-wallet"></i> &nbsp; {{___('Change Wallet')}}</a>
                                    @elseif($user->wallet_dollar != null && $user->wallet_dollar_status == 0)
                                        <div class="note note-plane note-info pdb-2x">
                                            <em class="fas fa-info-circle"></em>
                                            <p>{{ ___('Email sent, check your inbox.') }}
                                                <a href="#" data-toggle="modal" data-target="#modal-centered"
                                                    style="color: #5664d2;" id="btnresetmail" class="d-none">
                                                    {{___('Send again?')}}
                                                </a>
                                                
                                            <p href="#" id="timer">{{ ___('Remaining time to resend') }} : &nbsp;<span
                                                    id="demo" class=""></span>
                                            </p>
                                            <input type="hidden" id="timervalue"
                                                value="{{ $user->timer_mail_wallet }}">
                                            </p>
                                        </div>
                                    @else
                                        <a href="#" data-toggle="modal" data-target="#modal-centered"
                                            class="btn btn-primary butt disabled" id="butt"><i class="fas fa-wallet"
                                                style="transform: rotate(340deg);"></i> &nbsp;
                                                {{___('Change Wallet')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>{{-- .row --}}
                        <div class="gaps-1x"></div>{{-- 10px gap --}}
                    </form>{{-- form --}}
                </div>{{-- .tab-pane --}}
            </div>{{-- .tab-pane --}}
        </div>{{-- .tab-content --}}
    </div>{{-- .card-innr --}}
    <div class="modal fade" id="modal-centered" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <form action="{{ route('user.send.mail.wallet') }}" class="validate-modern" method="POST"
                    id="create_page">
                    @csrf
                    <div class="popup-body">
                        <h3 class="popup-title">{{___('Change wallet address')}}</h3>
                        <span>{{___('At the moment you decide to change your wallet address you will receive an email to confirm it, once your new wallet is completed it will be saved successfully')}}</span>
                        <div class="col" style="top: 10px">
                            <input class="input-bordered" id="wal" name="wallet_dollar" required="required"
                                readonly minlength="20" value={{ $user->wallet_dollar ? $user->wallet_dollar : '' }}>
                        </div>
                        <input type="hidden" value="{{ Crypt::encrypt(auth()->user()->id) }}" name="id">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-4 d-flex justify-content-center">
                                <button id="confirm" type="submit" class="btn btn-md btn-primary"><i
                                        class="fa fa-paper-plane"></i>
                                    {{___('Send Confirmation')}}</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-4 d-flex justify-content-center">
                                <a href="#" class="btn btn-danger"data-dismiss="modal">
                                    <i class="fa fa-ban"></i>
                                    {{___('Cancel')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </form>


            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    </div>{{-- .card --}}
    <div class="row">
        <div class="col-lg-6">
            @include('user.includes.profilePhoto-include')
        </div>
        <div class="col-lg-6">
            <div class="content-area card">
                <div class="card-innr">
                    <div class="card-head">
                        <h4 class="card-title">{!! ___('Two-Factor Verification') !!}</h4>
                    </div>
                    <p>{!! __(
                        "Two-factor authentication is a method for protection of your account. When it is activated you are required to enter not only your password, but also a special code. You can receive this code in mobile app. Even if third party gets access to your password, they still won't be able to access your account without the 2FA code.",
                    ) !!}</p>
                    <div class="d-sm-flex justify-content-between align-items-center pdt-1-5x">
                        <span class="text-light ucap d-inline-flex align-items-center"><span
                                class="mb-0"><small>{{ ___('Current Status:') }}</small></span> <span
                                class="badge badge-{{ $user->google2fa == 1 ? 'info' : 'disabled' }} ml-2">{{ $user->google2fa == 1 ? ___('Enabled') : ___('Disabled') }}</span></span>
                        <div class="gaps-2x d-sm-none"></div>
                        <button type="button" data-toggle="modal" data-target="#g2fa-modal"
                            class="order-sm-first btn btn-{{ $user->google2fa == 1 ? 'warning' : 'primary' }}">{{ $user->google2fa != 1 ? ___('Enable 2FA') : ___('Disable 2FA') }}</button>
                    </div>
                </div>{{-- .card-innr --}}
            </div>
        </div>
        
    </div>

@endsection
@section('modals')
    <div class="modal fade" id="g2fa-modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h3 class="popup-title">{{ $user->google2fa != 1 ? ___('Enable') : ___('Disable') }}
                        {{ ___('2FA Authentication') }}</h3>
                    <form class="validate-modern" action="{{ route('user.ajax.account.update') }}" method="POST"
                        id="nio-user-2fa">
                        @csrf
                        <input type="hidden" name="action_type" value="google2fa_setup">
                        @if ($user->google2fa != 1)
                            <div class="pdb-1-5x">
                                <p><strong>{{ ___('Step 1:') }}</strong> {{ ___('Install this app from') }} <a
                                        target="_blank"
                                        href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">{{ ___('Google Play') }}
                                    </a> {{ ___('store or') }} <a target="_blank"
                                        href="https://itunes.apple.com/us/app/google-authenticator/id388497605">{{ ___('App Store') }}</a>.
                                </p>
                                <p><strong>{{ ___('Step 2:') }}</strong>
                                    {{ ___('Scan the below QR code by your Google Authenticator app, or you can add account manually.') }}
                                </p>
                                <p><strong>{{ ___('Manually add Account:') }}</strong><br>{{ ___('Account Name:') }}
                                    <strong class="text-head">{{ site_info() }}</strong> <br> {{ ___('Key:') }}
                                    <strong class="text-head">{{ $google2fa_secret }}</strong>
                                </p>
                                <div class="row g2fa-box">
                                    <div class="col-md-4">
                                        <img loading="lazy" class="img-thumbnail"
                                            src="{{ route('public.qrgen', ['text' => $google2fa]) }}" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-item">
                                            <label
                                                for="google2fa_code">{{ ___('Enter Google Authenticator Code') }}</label>
                                            <input id="google2fa_code" type="number" class="input-bordered"
                                                name="google2fa_code" placeholder="{{ ___('Enter the Code to verify') }}">
                                        </div>
                                        <input type="hidden" name="google2fa_secret" value="{{ $google2fa_secret }}">
                                        <input name="google2fa" type="hidden" value="1">
                                        <button type="submit" class="btn btn-primary">{{ ___('Confirm 2FA') }}</button>
                                    </div>
                                </div>
                                <div class="gaps-2x"></div>
                                <p class="text-danger"><strong>{{ ___('Note:') }}</strong>
                                    {{ ___('If you lost your phone or uninstall the Google Authenticator app, then you will lost access of your account.') }}
                                </p>
                            </div>
                        @else
                            <div class="pdb-1-5x">
                                <div class="input-item">
                                    <label for="google2fa_code">{{ ___('Enter Google Authenticator Code') }}</label>
                                    <input id="google2fa_code" type="number" class="input-bordered"
                                        name="google2fa_code" placeholder="{{ ___('Enter the Code to verify') }}">
                                </div>
                                <input name="google2fa" type="hidden" value="0">
                                <button type="submit" class="btn btn-primary">{{ ___('Disable 2FA') }}</button>
                            </div>
                        @endif
                    </form>
                </div>
            </div>{{-- .modal-content --}}
        </div>{{-- .modal-dialog --}}
    </div>
@endsection
@push('footer')
    @if ($var == 1 || session('success'))
        <script>
            function clickButton() {
                document.querySelector('#botonWallet').click();
            }
            clickButton()
        </script>
    @endif
    {{-- Modal End --}}
    <script type="text/javascript">
        (function($) {
            var $nio_user_2fa = $('#nio-user-2fa');
            if ($nio_user_2fa.length > 0) {
                ajax_form_submit($nio_user_2fa);
            }
        })(jQuery);

        $(document).ready(function() {

            $("#btnReset").click(function() {
                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {{___('Loading')}}...`
                );
            });
            $("#btnAgainReset").click(function() {
                // disable button
                $(this).addClass('disabled');
                $(this).css('pointer-events', 'none');
                $(this).css('cursor', 'not-allowed');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {{___('Loading')}}...`
                );
            });
        });
    </script>
    <script>
        document.getElementById("wallet").addEventListener("keyup", test);
        function test() {
            var cero = 0;
            var wallet = document.getElementById("walle").value;
            if (wallet.length > 19) {
                $('#butt').removeClass('disabled');
            } else if (wallet.length == 0) {
                $('#butt').addClass('disabled');
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            $("#walle").keyup(function() {
                var value = $(this).val();
                $("#wal").val(value);
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#confirm").click(function() {
                // disable button
                $(this).addClass('disabled');
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {{___('Loading')}}...`
                );
            });
        })
    </script>
    <script>
        document.getElementById("wallet").addEventListener("keyup", test2);
        function test2() {
            var cero = 0;
            var wallet = document.getElementById("walle").value;
            if (wallet.length < 19) {
                $('#btnresetmail').addClass('d-none');
            } else if (wallet.length == 0) {
                $('#btnresetmail').addClass('d-none');
            } else {
                $('#btnresetmail').removeClass('d-none');
            }
        }
    </script>
    <script>
        var x = setInterval(function() {
            var deadline = document.getElementById("timervalue").value;
            var countDownDate = new Date(deadline).getTime();
            var now = new Date().getTime();
            var t = countDownDate - now;
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * 60)) / 1000);
            document.getElementById("demo").innerHTML =
                minutes + "m " + seconds + "s ";
            if (t < 0) {
                $('#btnresetmail').removeClass('d-none');
                $('#timer').addClass('d-none');
            }
        }, 1000);
    </script>
@endpush
