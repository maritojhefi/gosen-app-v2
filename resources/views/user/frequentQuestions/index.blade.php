@extends('layouts.user')
@section('title', 'FAQ')
@php($has_sidebar = true)

@section('content')
    <div class="card content-area content-area-mh">
        <div class="card-innr">
            <div class="card-head">
                <h4 class="card-title">FAQ</h4>
            </div>
            <div class="gaps-1x"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="float-right position-relative">
                        <div
                            class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                        </div>
                    </div>
                </div>
            </div>
            <table class="data-table dt-filter-init user-tnx">
                <thead>
                    <tr class="data-item data-head">
                        <th class="data-col tnx-status dt-tnxno">
                            <div class="data-col" style="margin-bottom: 20px">
                                <h6 class="large">{{ ___('Frequently Ask Questions') }}...</h6>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody id="accordion" class="accordion-s2">
                    @foreach ($questions as $question)
                        <tr class="data-item tnx-item-{{ $question->id }}">
                            <td class="dt-tnxno">
                                <div class="card">
                                    <div class="card-header accordion-heading collapsed" id="heading{{ $question->id }}" class="btn"
                                        data-toggle="collapse" data-target="#collapse{{ $question->id }}"
                                        aria-expanded="true" aria-controls="collapse{{ $question->id }}">
                                        <h5 class="color-style">
                                            {{ __($question->titulo) }}
                                        </h5>
                                    </div>
                                    <div id="collapse{{ $question->id }}" class="collapse"
                                        aria-labelledby="heading{{ $question->id }}" data-parent="#accordion">
                                        <div class="p-4">
                                            {{ __($question->contenido) }}
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
