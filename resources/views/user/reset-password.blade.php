@extends('layouts.auth')
@section('title', ___('Sign-in'))
@push('header')
    <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
    <link preload rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <style>
        form-horizontal {
            background: white;
        }

        .container {
            margin-top: 10%;
        }

        .progress {
            height: 7px;
            width: 80%;

        }

        .control-label {
            text-align: left !important;
            padding-bottom: 7px;
        }

        .form-horizontal {
            padding: 25px 20px;
            border: 2px solid #e8eaed;
            border-radius: 5px;
        }

        .fa-times {
            color: red;
        }
    </style>
    <style>
        .qs {
            background-color: #02bdda;
            border-radius: 16px;
            color: #e3fbff;
            cursor: default;
            display: inline-block;
            font-family: &#39;
            Helvetica&#39;
            ,
            sans-serif;
            font-size: 18px;
            font-weight: bold;
            height: auto;
            line-height: 30px;
            position: relative;
            text-align: center;
            width: 30px;
            position: absolute;
            top: 16px;
            transform: translateY(-50%);
            left: 86px;
        }

        .qs .popover {
            background-color: rgba(0, 0, 0, 0.85);
            border-radius: 5px;
            bottom: 42px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.4);
            color: #fff;
            display: none;
            font-size: 12px;
            font-family: &#39;
            Helvetica&#39;
            ,
            sans-serif;
            left: -164px;
            padding: 7px 10px;
            position: absolute;
            width: 200px;
            z-index: 4;
        }

        .qs .popover:before {
            border-top: 7px solid rgba(0, 0, 0, 0.85);
            border-right: 7px solid transparent;
            border-left: 7px solid transparent;
            bottom: -7px;
            content: &#39;
            &#39;
            ;
            display: block;
            left: 50%;
            margin-left: -7px;
            position: absolute;
        }

        .qs:hover .popover {
            display: block;
            -webkit-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
            -moz-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
            -ms-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
        }

        @-webkit-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-moz-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-ms-keyframes fade-in {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @-webkit-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @-moz-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @-ms-keyframes move-up {
            from {
                bottom: 30px;
            }

            to {
                bottom: 42px;
            }
        }

        @media (max-width: 576px) {
            .ico {
                left: 190px;
            }
        }
        @media(max-width:440px) {
            .cpass {
                width: calc(100% + 40px) !important;
            }

        }
        @media(max-width:440px) {
            .cpassu {
                width: calc(100% + 40px) !important;
            }

        }
        @media(max-width:440px) {
            .qs {
                left: calc(100% - 20px) !important;
            }

        }
        @media(max-width:440px) {
            .eyez {
                right: calc(100% - 247px) !important;
            }

        }
    </style>
@endpush
@section('content')
    <div class="page-ath-form" style="top: -10px">
        <h2 class="mb-4">
            <small>{{ ___('Password Reset') }}
                <img loading="lazy" src="{{ asset('assets/images/pass.gif') }}" alt="" style="width:30px">
            </small>
        </h2>
        <form class="" method="post" action="{{ route('update.password') }}">
            @csrf
            @include('layouts.messages')
            <div class="form-group" style="margin-bottom: -10px;">
                <div>
                    <input type="hidden" name="id" value="{{ $id }}">
                </div>
                <fieldset>
                    <!-- Password input-->

                    <span id="popover-password-top" class="hide block-help ml-4"><i class="fa fa-info-circle text-danger"
                            aria-hidden="true"></i> {{___('Enter a strong password')}}</span>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="input col-lg-10 col-md-10 col-sm-10 col-xs-10"
                                style="position: relative;
                            width: auto;"
                                id="show_hide_password">

                                <input id="password" name="password" type="password" placeholder="{{ ___('Enter a password') }}"
                                    class="form-control input-md cpassu" style="width:145%;" required>
                                <a href="#">
                                    <i class="fa fa-eye-slash eyez"
                                        style="
                                color: #191919;
                                position: absolute;
                                width: 20px;
                                height: 20px;
                                top: 20px;
                                right: -67px;
                                top:20px;
                                transform: translateY(-50%);">
                                    </i>
                                </a>
                            </div>
                            <div class="center d-none col-lg-2 col-md-2 col-sm-2 col-xs-2" id="pop">
                                <span class="qs">i
                                    <span id="poo" class="popover above show" data-trigger="focus" style="top:auto;">
                                        <ul class="list-unstyled d-none" id="uls">
                                            <li class="" id="uppercase"><span class="low-upper-case"><i
                                                        class="fa fa-times" aria-hidden="true"></i></span>&nbsp; {{ ___('1 lowercase
                                                &amp; 1
                                                uppercase') }}</li>
                                            <li class="" id="number"><span class="one-number"><i
                                                        class="fa fa-times" aria-hidden="true"></i></span> &nbsp;{{ ___('1 number
                                                (0-9)') }}</li>
                                            <li class="" id="special"><span class="one-special-char"><i
                                                        class="fa fa-times" aria-hidden="true"></i></span> &nbsp;{{ ___('1 Special
                                                Character') }}
                                                (!@#$%^&*).</li>
                                            <li class="" id="character"><span class="eight-character"><i
                                                        class="fa fa-times" aria-hidden="true"></i></span>&nbsp; {{ ___('At least 8
                                                Character') }}</li>
                                        </ul>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div id="popover-password" class="mt-3">
                            <p>{{ ___('Password Strength') }}: <span id="result"> </span></p>
                            <div class="progress">
                                <div id="password-strength" class="progress-bar progress-bar-success" style="top: -10px"
                                    role="progress-bar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 100%;">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- Repeat Password -->
            <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" style="left: -15px">
                <span id="popover-cpassword" class="hide block-help ml-4"><i class="fa fa-info-circle text-danger"
                        aria-hidden="true"></i> {{___('Password do not match')}}</span>
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 ">
                    <input id="password_confirmation" name="password_confirmation" type="password"
                        placeholder="{{ ___('Confirm Password') }}" class="form-control input-md cpass" style="width:100%;" required>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <input type="hidden" name="hola">
                </div>
            </div>
            <div class="gaps-4x ml-4">
                <button type="" onClick="comprobarClave()" id="button" class="btn btn-primary mt-5" disabled>
                    {{ ___('Save new password') }}
                </button>
            </div>
        </form>
    </div>
@endsection
@push('footer')
    {{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script> --}}
    <script>
        $(document).ready(function() {
            $('#password').keyup(function() {
                var password = $('#password').val();
                if (checkStrength(password) == false) {
                    $('#sign-up').attr('disabled', true);
                    $('#button').attr('disabled', true);
                    $('#pop').addClass('d-none');
                } else if (checkStrength(password) == 'Strong') {
                    $('#pop').addClass('d-none');
                } else {
                    $('#button').attr('disabled', true);
                    $('#pop').removeClass('d-none');
                }
            });
            $('#password_confirmation').keyup(function() {
                var password = $('#password').val();
                if ($('#password').val() !== $('#password_confirmation').val()) {
                    $('#popover-cpassword').removeClass('hide');
                    $('#sign-up').attr('disabled', true);
                    $('#button').attr('disabled', true);
                } else if (checkStrength(password) == 'Strong') {
                    $('#button').attr('disabled', false);
                    $('#popover-cpassword').addClass('hide');
                }
            });

            function checkStrength(password) {
                var strength = 0;
                //If password contains both lower and uppercase characters, increase strength value.
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                    strength += 1;

                    $('.low-upper-case').addClass('text-success');
                    $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                    $('#uppercase').addClass('d-none')
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.low-upper-case').removeClass('text-success');
                    $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                    $('#uppercase').removeClass('d-none');
                    $('#popover-password-top').removeClass('hide');

                }
                //If it has numbers and characters, increase strength value.
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                    strength += 1;
                    $('.one-number').addClass('text-success');
                    $('.one-number i').removeClass('fa-times').addClass('fa-check');
                    $('#number').addClass('d-none')
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.one-number').removeClass('text-success');
                    $('.one-number i').addClass('fa-times').removeClass('fa-check');
                    $('#number').removeClass('d-none');
                    $('#popover-password-top').removeClass('hide');
                }

                //If it has one special character, increase strength value.
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                    strength += 1;
                    $('.one-special-char').addClass('text-success');
                    $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                    $('#special').addClass('d-none')
                    $('#popover-password-top').addClass('hide');

                } else {
                    $('.one-special-char').removeClass('text-success');
                    $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                    $('#special').removeClass('d-none')
                    $('#popover-password-top').removeClass('hide');
                }

                if (password.length > 8) {
                    strength += 1;
                    $('.eight-character').addClass('text-success');
                    $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');
                    $('#character').addClass('d-none')

                } else {
                    $('.eight-character').removeClass('text-success');
                    $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                    $('#character').removeClass('d-none')
                }
                // If value is less than 2
                if (strength < 2) {
                    $('#uls').removeClass('d-none')
                    $('#result').removeClass()
                    $('#password-strength').addClass('progress-bar-danger');
                    $('#result').addClass('text-danger').text('Very Week');
                    $('#password-strength').css('width', '10%');
                } else if (strength == 2) {
                    $('#result').addClass('good');
                    $('#password-strength').removeClass('progress-bar-danger');
                    $('#password-strength').addClass('progress-bar-warning');
                    $('#result').addClass('text-warning').text('Week')
                    $('#password-strength').css('width', '60%');
                    return 'Week'
                } else if (strength == 4) {
                    $('#result').removeClass()
                    $('#result').addClass('strong');
                    $('#password-strength').removeClass('progress-bar-warning');
                    $('#password-strength').addClass('progress-bar-success');
                    $('#result').addClass('text-success').text('Strength');
                    $('#password-strength').css('width', '100%');
                    return 'Strong'
                }
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
    <script>
        document.getElementById("password").addEventListener("keyup", testpassword2);
        document.getElementById("password_confirmation").addEventListener("keyup", testpassword2);

        function testpassword2() {
            var cero = 0;
            var text1 = document.getElementById("password");
            var text2 = document.getElementById("password_confirmation");
            if (text1.value == text2.value) {
                text2.style.borderColor = "#2EFE2E";
                text1.style.borderColor = "#2EFE2E";
                if (text1.value == cero || text2.value == cero) {
                    text1.style.borderColor = "";
                    text2.style.borderColor = "";
                }
            } else {
                text1.style.borderColor = "";
                text2.style.borderColor = "";
            }
        }
    </script>
@endpush
