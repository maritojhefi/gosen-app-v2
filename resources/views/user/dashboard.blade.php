@extends('layouts.user')
@section('title', ___('User Dashboard'))
@php
$has_sidebar = false;
$base_currency = base_currency();
$image = gws('welcome_img_hide', 0) == 0 ? 'welcome.png' : '';
@endphp

@section('gosencss')
    @include('user.includes.dashboard-css')
@endsection

@section('content')
    <div class="content-area user-account-dashboard">
        @livewire('user.news-modal-component')
        <div class="row">
            <div class="col-lg-4">
                {!! UserPanel::user_balance_card($contribution, ['vers' => 'side', 'class' => 'card-full-height']) !!}
            </div>
            <div class="col-lg-4 col-md-6">
                {!! UserPanel::user_token_block('', ['vers' => 'buy']) !!}
            </div>
            {{-- <div class="col-lg-4 col-md-6">
                <div class="account-info card card-full-height">
                    <div class="card-innr">
                        {!! UserPanel::user_account_status() !!}
                        <div class="gaps-2x"></div>
                        {!! UserPanel::user_account_wallet() !!}
                    </div>
                </div>

            </div> --}}
            <div class="col-lg-4 col-md-6">
                <div class="token-statistics card card-token card-full-height" style="background-image: linear-gradient(rgb(0 0 0 / 40%), rgb(0 0 0 / 40%)), url('  {{ asset('/assets/images/billete2.png')}}   '); background-size: 120% 100%;">
                    <div class="card-innr">
                        <div class="token-balance token-balance-with-icon">
                            <div class="token-balance-icon" style="background: rgba(255, 255, 255, 0);">
                            <img loading="lazy" src="{{ asset('/assets/images/dolar.png')}}" style="width: 100px;" alt="">
                            
                            </div>
                            <div class="token-balance-text">
                                <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">{{___('Dollar Balance')}}</h6>
                                <span class="lead"> {{ $monto }} <span>USD </span></span>
                            </div>
                        </div>
                        <div class="token-balance token-balance-s2">
                            <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold">{{___('Your Bonuses In Gos')}}</h6>
                            <ul class="token-balance-list">
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead" >{{$bonus->count()}}
                                    </span>
                                    <span
                                        class="sub" style="color: white;">{{___('Transactions')}}
                                    </span>
                                </li>
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead">{{ $bonus->groupBy('user_from')->count()}}          
                                    </span>
                                    <span class="sub" style="color: white;">{{___('Referrals')}}
                                    </span>
                                </li>
                                <li class="token-balance-sub">
                                    <a href="{{ route('user.bonus') }}" class="btn" style="background: #2d7f3a94;
                                    color: white;">
                                        {{___('View more')}}</a> 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                {!! UserPanel::token_revutoken('', ['class' => 'card-full-height']) !!}
            </div>
            <div class="col-lg-12 col-md-12">
                @include('user.includes.bonus-tokens')
            </div>
            <div class="col-lg-12 col-md-12">
                <x-mapa-usuarios-component/>
            </div>
            <div class="col-12 col-lg-7">
                {!! UserPanel::token_sales_progress('', ['class' => 'card-full-height']) !!}
            </div>
            <div class="col-12 col-lg-5">
                {!! UserPanel::transaccion_user('', ['image' => $image, 'class' => 'card-full-height'], route('user.transactions')) !!}
            </div>


            <div class="col-12{{ gws('user_sales_progress', 1) == 1 ? ' col-lg-5' : '' }}">
                {!! UserPanel::content_block('welcome', ['image' => $image, 'class' => 'card-full-height']) !!}
            </div>

            <div class="col-12 col-lg-7">
                @if (get_page('referral', 'status') == 'active' && UserPanel::get_referrals())
                    {!! UserPanel::referidos_user('', ['image' => $image, 'class' => 'card-full-height'], route('user.referral')) !!}
                @endif
            </div>
            @if (gws('user_sales_progress', 1) == 1)
            @endif
            @if (get_page('home_top', 'status') == 'active')
            @endif

        </div>
    </div>
@endsection
