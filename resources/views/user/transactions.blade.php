@extends('layouts.user')
@section('title', ___('User Transactions'))

@push('header')
    <script type="text/javascript">
        var view_transaction_url = "{{ route('user.ajax.transactions.view') }}";
    </script>
@endpush

@section('content')
    @php
        $posicion = ['a' => 'left', 'b' => 'right', 'c' => 'top', 'd' => 'bottom'];
        $posicion = collect($posicion);
    @endphp

    @include('layouts.messages')
    <div class="card content-area content-area-mh">
        <div class="card-innr">
            <div class="card-head">
                <h4 class="card-title">{{ ___('Transactions list') }}</h4>
            </div>
            <div class="gaps-1x"></div>
            <ul class="nav nav-tabs nav-tabs-line" role="tablist" id="myTab">
                <li class="nav-item">
                    <a class="nav-link active balanceGos" data-toggle="tab" href="#balanceGos"
                        style="padding-left: 40px; padding-right:40px;">{{ ___('Balance GOS Token') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link gosToken" data-toggle="tab" href="#gosToken"
                        style="padding-left: 40px; padding-right:40px;">{{ ___('GOS Token') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bonusStart" data-toggle="tab" href="#bonusStart"
                        style="padding-left: 40px; padding-right:40px;">{{ ___('Bonus Start') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bonusUSD" data-toggle="tab" href="#bonusUSD"
                        style="color:#46d246; padding-left: 40px; padding-right:40px;">{{ ___('Bonus USD') }}</a>
                </li>
            </ul>{{-- .nav-tabs-line --}}
            <div class="tab-content mt-3">

                <div class="tab-pane fade show active" id="balanceGos">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" style="font-size:20px;" data-toggle="tooltip"
                                        data-placement="left" data-original-title="{{ ___('Filters') }}"></em> </a>
                                <div
                                    class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                    <div id="office2">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Types') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="all2"
                                                    name="ofc2" type="radio" value="" checked>
                                                <label for="all2">{{ ___('All') }}
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="credito2"
                                                    name="ofc2" type="radio" value="TNX|GIFT">
                                                <label for="credito2">{{ ___('Credit') }}
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="retiro2"
                                                    name="ofc2" type="radio" value="WTX|RTX">
                                                <label for="retiro2">{{ ___('Withdrawal') }}
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="office3">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Status') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="all3"
                                                    name="ofc3" type="radio" value="" checked>
                                                <label for="all3">{{ ___('All') }}
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="approved"
                                                    name="ofc3" type="radio" value="Approved">
                                                <label for="approved">{{ ___('Approved') }}
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="pending"
                                                    name="ofc3" type="radio" value="Pending">
                                                <label for="pending">{{ ___('Pending') }}
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="canceled"
                                                    name="ofc3" type="radio" value="Canceled">
                                                <label for="canceled">{{ ___('Canceled') }}
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init user-tnx tabla"id="miTabla2">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status">{{ ___('Tranx NO') }}</th>
                                <th class="data-col ">{{ ___('Tokens') }}</th>
                                <th class="data-col ">{{ ___('Amount') }}</th>
                                <th class="data-col ">{{ base_currency(true) }} {{ ___('Amount') }}</th>
                                <th class="data-col ">{{ ___('To') }}</th>
                                <th class="data-col tnx-type">
                                    <div class="dt-type-text">{{ ___('Type') }}</div>
                                </th>
                                <th class="data-col">{{ ___('Status') }}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($trnxs as $trnx)
                                @php
                                    $text_danger = $trnx->tnx_type == 'refund' || ($trnx->tnx_type == 'transfer' && $trnx->extra == 'sent') ? ' text-danger' : '';
                                @endphp
                                <tr class="data-item tnx-item-{{ $trnx->id }}" id="{{ $trnx->status }}">
                                    @if (strpos($trnx->tnx_id, 'GIFT') !== false)
                                        <td class="data-col">
                                            <div class="d-flex align-items-center">
                                                <div class="data-state data-state-approved" data-toggle="tooltip"
                                                    data-placement="top" title=""
                                                    data-original-title="{{ ___('Done') }}">
                                                    <span class="d-none"></span>
                                                </div>
                                                <div class="fake-class">
                                                    <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                                    <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                                </div>
                                            </div>
                                        </td>
                                    @else
                                        <td class="data-col">
                                            <div class="d-flex align-items-center">
                                                <div
                                                    class="data-state data-state-{{ str_replace(['progress', 'canceled'], ['pending', 'canceled'], __status($trnx->status, 'icon')) }}">
                                                    <span
                                                        class="d-none">{{ $trnx->status == 'onhold' ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                                </div>
                                                <div class="fake-class">
                                                    <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                                    <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                    <td class="data-col">
                                        @php
                                            $color = '';
                                            $signo = '';
                                            if (strpos($trnx, 'WTX')) {
                                                $color = 'red';
                                                $signo = '-';
                                            } elseif (strpos($trnx, 'RTX')) {
                                                $color = 'red';
                                            } else {
                                                $color = 'black';
                                                $signo = '+';
                                            }
                                        @endphp
                                        <span class="lead token-amount"
                                            style="color:{{ $color }}">{{ $signo }}
                                            {{ $trnx->total_tokens }}</span>
                                        <span class="sub sub-symbol">{{ token_symbol() }}</span>
                                    </td>
                                    <td class="data-col amount{{ $text_danger }}">
                                        @if ($trnx->tnx_type == 'referral' ||
                                            $trnx->tnx_type == 'bonus' ||
                                            $trnx->tnx_type == 'launch' ||
                                            $trnx->tnx_type == 'joined')
                                            <span class="lead amount-pay">{{ '~' }}</span>
                                        @else
                                            <span class="lead amount-pay{{ $text_danger }}"
                                                style="color:{{ $trnx->tnx_type == 'withdraw' || $trnx->tnx_type == 'refund' ? 'red' : '' }}">{{ ($trnx->tnx_type == 'withdraw' ? '-' : '') . '' . $trnx->amount }}</span>
                                            <span class="sub sub-symbol">{{ strtoupper($trnx->currency) }} <em
                                                    class="fas fa-info-circle" data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="1 {{ token('symbol') }} = {{ to_num($trnx->currency_rate, 'max') . ' ' . strtoupper($trnx->currency) }}"></em></span>
                                        @endif
                                    </td>
                                    <td class="data-col ">
                                        @if ($trnx->tnx_type == 'referral' ||
                                            $trnx->tnx_type == 'bonus' ||
                                            $trnx->tnx_type == 'launch' ||
                                            $trnx->tnx_type == 'joined')
                                            <span class="lead amount-pay">{{ '~' }}</span>
                                        @else
                                            <span class="lead amount-pay{{ $text_danger }}"
                                                style="color:{{ $trnx->tnx_type == 'withdraw' || $trnx->tnx_type == 'refund' ? 'red' : '' }}">{{ ($trnx->tnx_type == 'withdraw' ? '-' : '') . '' . $trnx->base_amount }}</span>
                                            <span class="sub sub-symbol">{{ base_currency(true) }} <em
                                                    class="fas fa-info-circle" data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="1 {{ token('symbol') }} = {{ to_num($trnx->base_currency_rate, 'max') . ' ' . base_currency(true) }}"></em></span>
                                        @endif
                                    </td>
                                    <td class="data-col">
                                        @php
                                            $pay_to = $trnx->payment_method == 'system' ? '~' : ($trnx->payment_method == 'bank' ? explode(',', $trnx->payment_to) : show_str($trnx->payment_to));
                                            $extra = $trnx->tnx_type == 'refund' ? is_json($trnx->extra, true) ?? $trnx->extra : '';
                                        @endphp
                                        @if ($trnx->tnx_type == 'refund')
                                            <span class="sub sub-info">{{ $trnx->details }}</span>
                                            @if ($extra->trnx)
                                                <span class="sub sub-view"><a href="javascript:void(0)"
                                                        class="view-transaction"
                                                        data-id="{{ $extra->trnx }}">{{ ___('View Transaction') }}</a></span>
                                            @endif
                                        @else
                                            @if ($trnx->refund != null)
                                                <span
                                                    class="sub sub-info text-danger">{{ ___('Refunded #:orderid', ['orderid' => set_id($trnx->refund, 'refund')]) }}</span>
                                            @else
                                                <span
                                                    class="lead user-info">{{ $trnx->payment_method == 'bank' ? $pay_to[0] : ($pay_to ? $pay_to : '~') }}</span>
                                            @endif
                                            <span
                                                class="sub sub-date">{{ $trnx->checked_time ? _date($trnx->checked_time) : _date($trnx->created_at) }}</span>
                                        @endif
                                    </td>
                                    <td class="data-col">
                                        <span
                                            class="dt-type-md badge p-2 badge-dim badge-md badge-{{ __(__status($trnx->tnx_type, 'status')) }}">{{ $trnx->tnx_type == 'purchase' ? __(ucfirst($trnx->tnx_type) . ' + B') : __(ucfirst($trnx->tnx_type)) }}</span>
                                        <span
                                            class="dt-type-sm badge badge-dim badge-md p-2 badge-{{ __(__status($trnx->tnx_type, 'status')) }}">{{ ucfirst(substr($trnx->tnx_type, 0, 1)) }}</span>
                                    </td>
                                    <td class="data-col">
                                        <span>{{ __(ucfirst($trnx->status)) }}</span>
                                    </td>
                                    <td class="data-col text-right">
                                        @if ($trnx->status == 'pending' || $trnx->status == 'onhold')
                                            @if ($trnx->tnx_type != 'transfer')
                                                <div class="relative d-inline-block d-md-none">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt"></em></a>
                                                    <div
                                                        class="toggle-class dropdown-content dropdown-content-center-left pd-2x">
                                                        <ul class="data-action-list">
                                                            <li><a href="javascript:void(0)"
                                                                    class="btn btn-auto btn-primary btn-xs view-transaction"
                                                                    data-id="{{ $trnx->id }}"><span>{{ ___('Pay') }}</span><em
                                                                        class="ti ti-wallet"></em></a></li>
                                                            @if ($trnx->checked_time != null)
                                                                <li><a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                                        class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                                        data-tnx_id="{{ $trnx->id }}"><em
                                                                            class="ti ti-trash"></em></a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <ul class="data-action-list d-none d-md-inline-flex">
                                                    <li><a href="javascript:void(0)"
                                                            class="btn btn-auto btn-primary btn-xs view-transaction"
                                                            data-id="{{ $trnx->id }}"><span>{{ ___('Pay') }}</span><em
                                                                class="ti ti-wallet"></em></a></li>
                                                    @if ($trnx->checked_time != null)
                                                        <li><a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                                class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                                data-tnx_id="{{ $trnx->id }}"><em
                                                                    class="ti ti-trash"></em></a></li>
                                                    @endif
                                                </ul>
                                            @else
                                                <a href="javascript:void(0)"
                                                    class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                    data-id="{{ $trnx->id }}"><em class="ti ti-eye"
                                                        data-toggle="tooltip" data-placement="{{ $posicion->random() }}"
                                                        data-original-title="View"
                                                        style="color:#46d246; font-size:20px;"></em></a>
                                            @endif
                                        @else
                                            <a href="javascript:void(0)"
                                                class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                data-id="{{ $trnx->id }}"><em class="ti ti-eye"
                                                    data-toggle="tooltip" data-placement="{{ $posicion->random() }}"
                                                    data-original-title="View"
                                                    style="color:#46d246; font-size:20px;"></em></a>
                                            {{-- @if ($trnx->checked_time == null && ($trnx->status == 'rejected' || $trnx->status == 'canceled'))
                                                <a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                    class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                    data-tnx_id="{{ $trnx->id }}"><em class="ti ti-trash"></em></a>
                                            @endif --}}
                                        @endif
                                    </td>

                                </tr>{{-- .data-item --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="gosToken">
                    <table class="data-table dt-filter-init user-tnx tabla ">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status">{{ ___('Tranx NO') }}</th>
                                <th class="data-col ">{{ ___('Tokens') }}</th>
                                <th class="data-col ">{{ ___('Amount') }}</th>
                                <th class="data-col ">{{ base_currency(true) }} {{ ___('Amount') }}</th>
                                <th class="data-col ">{{ ___('To') }}</th>
                                <th class="data-col tnx-type">
                                    <div class="dt-type-text">{{ ___('Type') }}</div>
                                </th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($trnxPurBonus as $trnx)
                                @php
                                    $text_danger = $trnx->tnx_type == 'refund' || ($trnx->tnx_type == 'transfer' && $trnx->extra == 'sent') ? ' text-danger' : '';
                                @endphp
                                <tr class="data-item tnx-item-{{ $trnx->id }}">
                                    <td class="data-col">
                                        <div class="d-flex align-items-center">
                                            <div
                                                class="data-state data-state-{{ str_replace(['progress', 'canceled'], ['pending', 'canceled'], __status($trnx->status, 'icon')) }}">
                                                <span
                                                    class="d-none">{{ $trnx->status == 'onhold' ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                            </div>
                                            <div class="fake-class">
                                                <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                                <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="data-col">
                                        @php
                                            $color = '';
                                            $signo = '';
                                            if (strpos($trnx, 'WTX')) {
                                                $color = 'red';
                                                $signo = '-';
                                            } elseif (strpos($trnx, 'RTX')) {
                                                $color = 'red';
                                            } else {
                                                $color = 'black';
                                                $signo = '+';
                                            }
                                        @endphp
                                        <span class="lead token-amount"
                                            style="color:{{ $color }}">{{ $signo . '' . $trnx->total_tokens }}</span>
                                        <span class="sub sub-symbol">{{ token_symbol() }}</span>
                                    </td>


                                    <td class="data-col amount{{ $text_danger }}">
                                        @if ($trnx->tnx_type == 'referral' ||
                                            $trnx->tnx_type == 'bonus' ||
                                            $trnx->tnx_type == 'launch' ||
                                            $trnx->tnx_type == 'joined')
                                            <span class="lead amount-pay">{{ '~' }}</span>
                                        @else
                                            <span class="lead amount-pay{{ $text_danger }}"
                                                style="color:{{ $trnx->tnx_type == 'withdraw' || $trnx->tnx_type == 'refund' ? 'red' : '' }}">{{ ($trnx->tnx_type == 'withdraw' ? '-' : '') . '' . $trnx->amount }}</span>
                                            <span class="sub sub-symbol">{{ strtoupper($trnx->currency) }} <em
                                                    class="fas fa-info-circle" data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="1 {{ token('symbol') }} = {{ to_num($trnx->currency_rate, 'max') . ' ' . strtoupper($trnx->currency) }}"></em></span>
                                        @endif
                                    </td>
                                    <td class="data-col ">
                                        @if ($trnx->tnx_type == 'referral' ||
                                            $trnx->tnx_type == 'bonus' ||
                                            $trnx->tnx_type == 'launch' ||
                                            $trnx->tnx_type == 'joined')
                                            <span class="lead amount-pay">{{ '~' }}</span>
                                        @else
                                            <span class="lead amount-pay{{ $text_danger }}"
                                                style="color:{{ $trnx->tnx_type == 'withdraw' || $trnx->tnx_type == 'refund' ? 'red' : '' }}">{{ ($trnx->tnx_type == 'withdraw' ? '-' : '') . '' . $trnx->base_amount }}</span>
                                            <span class="sub sub-symbol">{{ base_currency(true) }} <em
                                                    class="fas fa-info-circle" data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="1 {{ token('symbol') }} = {{ to_num($trnx->base_currency_rate, 'max') . ' ' . base_currency(true) }}"></em></span>
                                        @endif
                                    </td>
                                    <td class="data-col">
                                        @php
                                            $pay_to = $trnx->payment_method == 'system' ? '~' : ($trnx->payment_method == 'bank' ? explode(',', $trnx->payment_to) : show_str($trnx->payment_to));
                                            $extra = $trnx->tnx_type == 'refund' ? is_json($trnx->extra, true) ?? $trnx->extra : '';
                                        @endphp
                                        @if ($trnx->tnx_type == 'refund')
                                            <span class="sub sub-info">{{ $trnx->details }}</span>
                                            @if ($extra->trnx)
                                                <span class="sub sub-view"><a href="javascript:void(0)"
                                                        class="view-transaction"
                                                        data-id="{{ $extra->trnx }}">{{ ___('View Transaction') }}</a></span>
                                            @endif
                                        @else
                                            @if ($trnx->refund != null)
                                                <span
                                                    class="sub sub-info text-danger">{{ ___('Refunded #:orderid', ['orderid' => set_id($trnx->refund, 'refund')]) }}</span>
                                            @else
                                                <span
                                                    class="lead user-info">{{ $trnx->payment_method == 'bank' ? $pay_to[0] : ($pay_to ? $pay_to : '~') }}</span>
                                            @endif
                                            <span
                                                class="sub sub-date">{{ $trnx->checked_time ? _date($trnx->checked_time) : _date($trnx->created_at) }}</span>
                                        @endif
                                    </td>
                                    <td class="data-col">
                                        <span
                                            class="dt-type-md badge p-2 badge-dim badge-md badge-{{ __(__status($trnx->tnx_type, 'status')) }}">{{ $trnx->tnx_type == 'purchase' ? __(ucfirst($trnx->tnx_type) . ' + B') : __(ucfirst($trnx->tnx_type)) }}</span>
                                        <span
                                            class="dt-type-sm badge badge-dim badge-md p-2 badge-{{ __(__status($trnx->tnx_type, 'status')) }}">{{ ucfirst(substr($trnx->tnx_type, 0, 1)) }}</span>
                                    </td>
                                    <td class="data-col text-right">
                                        @if ($trnx->status == 'pending' || $trnx->status == 'onhold')
                                            @if ($trnx->tnx_type != 'transfer')
                                                <div class="relative d-inline-block d-md-none">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt"></em></a>
                                                    <div
                                                        class="toggle-class dropdown-content dropdown-content-center-left pd-2x">
                                                        <ul class="data-action-list">
                                                            <li><a href="javascript:void(0)"
                                                                    class="btn btn-auto btn-primary btn-xs view-transaction"
                                                                    data-id="{{ $trnx->id }}"><span>{{ ___('Pay') }}</span><em
                                                                        class="ti ti-wallet"></em></a></li>
                                                            @if ($trnx->checked_time != null)
                                                                <li><a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                                        class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                                        data-tnx_id="{{ $trnx->id }}"><em
                                                                            class="ti ti-trash"></em></a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <ul class="data-action-list d-none d-md-inline-flex">
                                                    <li><a href="javascript:void(0)"
                                                            class="btn btn-auto btn-primary btn-xs view-transaction"
                                                            data-id="{{ $trnx->id }}"><span>{{ ___('Pay') }}</span><em
                                                                class="ti ti-wallet"></em></a></li>
                                                    @if ($trnx->checked_time != null)
                                                        <li><a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                                class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                                data-tnx_id="{{ $trnx->id }}"><em
                                                                    class="ti ti-trash"></em></a></li>
                                                    @endif
                                                </ul>
                                            @else
                                                <a href="javascript:void(0)"
                                                    class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                    data-id="{{ $trnx->id }}"><em class="ti ti-eye"
                                                        data-toggle="tooltip" data-placement="{{ $posicion->random() }}"
                                                        data-original-title="View"
                                                        style="color:#46d246; font-size:20px;"></em></a>
                                            @endif
                                        @else
                                            <a href="javascript:void(0)"
                                                class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                data-id="{{ $trnx->id }}"><em class="ti ti-eye"
                                                    data-toggle="tooltip" data-placement="{{ $posicion->random() }}"
                                                    data-original-title="View"
                                                    style="color:#46d246; font-size:20px;"></em></a>
                                            {{-- @if ($trnx->checked_time == null && ($trnx->status == 'rejected' || $trnx->status == 'canceled'))
                                                <a href="{{ route('user.ajax.transactions.delete', $trnx->id) }}"
                                                    class="btn btn-danger-alt btn-xs btn-icon user_tnx_trash"
                                                    data-tnx_id="{{ $trnx->id }}"><em class="ti ti-trash"></em></a>
                                            @endif --}}
                                        @endif
                                    </td>
                                </tr>{{-- .data-item --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="bonusStart">
                    <table class="data-table dt-filter-init admin-tnx tabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col ">{{ ___('Bonus') }} ID</th>
                                <th class="data-col ">Tokens</th>
                                <th class="data-col ">{{ ___('Date') }}</th>
                                <th class="data-col ">{{ ___('Type') }}</th>
                                <th class="data-col ">{{ ___(' ') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($trnxBonusStart as $start)
                                <tr class="data-item" id="tnx-item-102">
                                    <td class="data-col">
                                        <div class="d-flex align-items-center">
                                            <div class="d-flex align-items-center">
                                                <div data-toggle="tooltip" data-placement="top" title=""
                                                    class="data-state data-state-approved"
                                                    data-original-title="{{ ___('Done') }}">
                                                    <span class="d-none"></span>
                                                </div>
                                                <span class="pay-with">
                                                    {{ $start->tnx_id }}
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="data-col">
                                        <span class="sub sub-s2 pay-with">+{{ $start->tokens }}
                                        </span>
                                        <span class="sub sub-symbol">{{ token_symbol() }}</span>
                                    </td>
                                    <td class="data-col">
                                        <span class="sub sub-s2 pay-with">
                                            {{ date('jS F, Y', strtotime($start->tnx_time)) }}
                                        </span>
                                        <span class="sub sub-date"> {!! fechaOrden(\Carbon\Carbon::parse($start->tnx_time)) !!}</span>
                                    </td>
                                    <td class="data-col">
                                        <span
                                            class="badge badge-dim badge-sm badge-light p-2">{{ ___($start->tnx_type) }}
                                        </span>
                                    </td>
                                    <td class="data-col text-right">
                                        <div class="relative d-inline-block">
                                            <a href="#" class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                data-toggle="modal" data-target="#modal-start{{ $start->tnx_id }}"
                                                id="otra">
                                                <em class="ti ti-eye" data-toggle="tooltip"
                                                    data-placement="{{ $posicion->random() }}"
                                                    data-original-title="View"
                                                    style="color:#46d246; font-size:20px;"></em>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="bonusUSD">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" data-toggle="tooltip" data-placement="left"
                                        data-original-title="Filters" style="font-size:20px;"></em> </a>
                                <div class="toggle-class toggle-datatable-filter dropdown-content  dropdown-content-top-left text-left"
                                    style="right: 40px;">
                                    <ul class="dropdown-list dropdown-list-s2">
                                        <li>
                                            <h6 class="dropdown-title">{{ ___('Types') }}</h6>
                                        </li>
                                        <div class="form-group" id="office">
                                            <div class="form-check pb-2">
                                                <input class="input-checkbox input-checkbox-sm " id="all"
                                                    name="ofc" type="radio" value="" checked>
                                                <label for="all">{{ ___('All') }}
                                                </label>
                                            </div>
                                            <div class="form-check pb-2">
                                                <input class="input-checkbox input-checkbox-sm " id="creditoo"
                                                    name="ofc" type="radio" value="BD-TNX">
                                                <label for="creditoo">{{ ___('Credit') }}
                                                </label>
                                            </div>
                                            <div class="form-check pb-2">
                                                <input class="input-checkbox input-checkbox-sm " id="retiroo"
                                                    name="ofc" type="radio" value="BD-WTD">
                                                <label for="retiroo">{{ ___('Withdrawal') }}
                                                </label>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div id="data" style="position: inherit;"> --}}
                    <table class="data-table dt-filter-init admin-tnx tabla" id="miTabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col dt-type">{{ ___('Bonus') }} ID</th>
                                <th class="data-col dt-type">{{ ___('Bonus to') }}</th>
                                <th class="data-col dt-type">{{ ___('Level Bonus') }}</th>
                                <th class="data-col dt-type">{{ ___('Amount') }}</th>
                                <th class="data-col dt-type">{{ ___('Type Bonus') }}</th>
                                <th class="data-col dt-type">{{ ___('Date') }}</th>
                                <th class="data-col dt-type">{{ ___('Wallet') }}</th>
                                <th class="data-col dt-type">{{ ___('Status') }}</th>
                                <th class="data-col dt-type">{{ ' ' }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($trnxBonusDollarsAll as $bon)
                                <tr class="data-item" id="tnx-item-102">
                                    <td class="data-col">
                                        <div class="d-flex align-items-center">
                                            @if (isset($bon->transaction->tnxUser))
                                                <div data-toggle="tooltip" data-placement="top" title=""
                                                    class="data-state data-state-{{ $bon->status == 'canceled' ? $bon->status : 'approved' }}"
                                                    data-original-title="{{ $bon->status == 'canceled' ? $bon->status : 'Send' }}">
                                                    <span class="d-none"></span>
                                                </div>
                                                <span class=" pay-with">
                                                    {{ $tipobono . $bon->id }}
                                                    <span class="sub sub-date">
                                                        {{ ___('from') }}
                                                        {{ $bon->transaction ? $bon->transaction->tnx_id : '' }}
                                                    </span>
                                                </span>
                                            @else
                                                <div class="d-flex align-items-center">
                                                    <div data-toggle="tooltip" data-placement="top" title=""
                                                        class="data-state data-state-{!! $bon->status !!}"
                                                        data-original-title="{{ ___($bon->status) }}">
                                                        <span class="d-none"></span>
                                                    </div>
                                                    <span class="pay-with">
                                                        {{ __($bon->code) }}
                                                        <span class="sub sub-date">({{ ucfirst($bon->status) }})</span>
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="data-col">
                                        <span
                                            class="sub sub-s2 pay-with">{{ $bon->user_from != '0' ? $bon->userBy->name : '' }}
                                        </span>
                                    </td>
                                    <td class="data-col">
                                        @if ($bon->type == 'bonus')
                                            @if ($bon->level_bonus == 0)
                                                <span class="pay-with badge"
                                                    style="background: #0B132B;border-radius: 10px;">{{ ___('Level') }}
                                                    {{ $bon->level_bonus }}</span>
                                            @else
                                                @switch($bon->level_bonus)
                                                    @case(1)
                                                        <span class="pay-with badge "
                                                            style="background: #1C2541; border-radius: 10px;">{{ ___('Level') }}
                                                            {{ $bon->level_bonus }}</span>
                                                    @break

                                                    @case(2)
                                                        <span class="pay-with badge "
                                                            style="background:#3A506B; border-radius: 10px;">{{ ___('Level') }}
                                                            {{ $bon->level_bonus }}</span>
                                                    @break

                                                    @case(3)
                                                        <span class="pay-with badge "
                                                            style="background: #41638A; border-radius: 10px;">{{ ___('Level') }}
                                                            {{ $bon->level_bonus }}</span>
                                                    @break

                                                    @case(4)
                                                        <span class="pay-with badge "
                                                            style="background: #457D9B; border-radius: 10px;">{{ ___('Level') }}
                                                            {{ $bon->level_bonus }}</span>
                                                    @break

                                                    @case(5)
                                                        <span class="pay-with badge "
                                                            style="background:5490B0; border-radius: 10px;">{{ ___('Level') }}
                                                            {{ $bon->level_bonus }}</span>
                                                    @break

                                                    @default
                                                @endswitch
                                            @endif
                                        @else
                                            <span class="pay-with badge " style="background:5490B0; border-radius: 10px;">

                                            </span>
                                        @endif
                                    </td>
                                    <td class="data-col">
                                        @php
                                            $color = '';
                                            $signo = '';
                                            if (strpos($bon->code, 'WTD') || $bon->status == 'canceled') {
                                                $color = 'red';
                                                $signo = '-';
                                            } else {
                                                $color = 'black';
                                                $signo = '+';
                                            }
                                        @endphp
                                        <span class="lead amount-pay"
                                            style="color:{{ $color }}">{{ $signo }}
                                            {{ $bon->amount }}</span>
                                        <span class="sub sub-symbol">(USDT)</span>
                                    </td>
                                    <td class="data-col">
                                        @if ($bon->type_bonus)
                                            <span class="sub sub-s2 pay-with">
                                                {{ ucfirst($bon->type_bonus) }}
                                                @if ($bon->type_bonus == 'percent')
                                                    <em class="fas fa-info-circle" data-toggle="tooltip"
                                                        data-placement="bottom" title=""
                                                        data-original-title="{{ $bon->type_value }}%"></em>
                                                @else
                                                    <em class="fas fa-info-circle" data-toggle="tooltip"
                                                        data-placement="bottom" title=""
                                                        data-original-title="{{ $bon->type_value }}(USDT)"></em>
                                                @endif
                                            </span>
                                        @else
                                            <span class="sub sub-s2 pay-with">

                                            </span>
                                        @endif

                                    </td>
                                    <td class="data-col">
                                        <span class="sub sub-s2 pay-with">
                                            {{ date('jS F, Y', strtotime($bon->created_at)) }}
                                        </span>
                                        <span class="sub sub-date"> {!! fechaOrden(\Carbon\Carbon::parse($bon->created_at)) !!}</span>
                                    </td>
                                    <td class="data-col">
                                        <span class="sub sub-s2 pay-with">
                                            {{ isset($bon->wallet) ? $bon->wallet : '' }}
                                        </span>
                                    </td>

                                    <td class="data-col">
                                        @if ($bon->type == 'bonus')
                                            @php
                                                $valorbon = '';
                                                if ($bon->status == 'pending') {
                                                    $valorbon = 'success';
                                                } elseif ($bon->status == 'canceled') {
                                                    $valorbon = 'danger';
                                                } else {
                                                    $valorbon = 'warning';
                                                }
                                            @endphp
                                            <span
                                                class="badge badge-dim badge-sm badge-{{ $valorbon }} p-2">{{ ___($bon->status == 'pending' ? 'Send' : 'Canceled') }}</span>
                                        @else
                                            @php
                                                $valor = '';
                                                if ($bon->status == 'approved') {
                                                    $valor = 'success';
                                                } elseif ($bon->status == 'canceled') {
                                                    $valor = 'danger';
                                                } else {
                                                    $valor = 'warning';
                                                }
                                            @endphp
                                            <span class="badge badge-dim badge-sm badge-{{ $valor }} p-2">
                                                {{ ___($bon->status) }}
                                            </span>
                                        @endif
                                    </td>
                                    <td class="data-col text-right">
                                        <div class="relative d-inline-block">
                                            <a href="#" data-toggle="modal"
                                                class="view-transaction btn btn-light-alt btn-xs btn-icon"
                                                data-target="#{{ $bon->type == 'bonus' ? 'modal-medium' . $bon->id : 'modal-medium2' . $bon->id }}">
                                                <em class="ti ti-eye" data-toggle="tooltip"
                                                    data-placement="{{ $posicion->random() }}"
                                                    data-original-title="{{ ___('View') }}"
                                                    style="color:#1cc910; font-size:20px;"></em>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- </div>
                    <input type="button" id="create_pdf" value="Generate PDF"> --}}
                </div>
            </div>
        </div>{{-- .card-innr --}}
    </div>{{-- .card --}}
    {{-- card flotante --}}
    <div class="main-content" style="position: fixed; z-index: 1; width: 73%; right: 0px; bottom: 0px; left: 35px;">
        <div class="card text-white mb-3" style="background-color:#252b3b;">

            {{-- valores para el bonus Dolar --}}
            <form action="{{ route('user.pdf') }}" id="form" method="POST">
                @csrf
                <div class="row" style="height: auto; padding: 12px;">
                    <input type="hidden" id="tipo" name="tipo" value="" />

                    {{-- Valores para el "Bonus USD" --}}
                    <input type="hidden" id="contador4" name="contadorDolares"
                        value="{{ $trnxBonusDollarsAll->count() }}">
                    <input type="hidden" id="sumatoriaMontos" name="totalUsdAmount" value="{{ $totalBonusDolars }}">

                    {{-- Valores para el bonus Start --}}
                    <input type="hidden" id="contador3" name="contadorBonusStart"
                        value="{{ $trnxBonusStart->count() }}">
                    <input type="hidden" id="total_Tokens" name="totalTokensBonusStart"
                        value="{{ $trnxBonusStart->sum('total_tokens') }}">
                    {{-- <input type="hidden" id="total_AmountUSD" name="totalUsdAmountBonusStart"
                        value="{{ token_price($trnxBonusStart->sum('total_tokens'), base_currency()) }}"> --}}

                    {{-- valores para el GOS Token  --}}
                    <input type="hidden" id="contador2" name="contadorGosToken" value="{{ $trnxPurBonus->count() }}">
                    <input type="hidden" id="total_Tokens1" name="totalTokensGosToken" value="{{ $totalTokenGos2 }}">
                    <input type="hidden" id="total_AmountUSD1" name="totalUsdAmountGosToken"
                        value="{{ $totalUsdAmountTokens }}">

                    {{-- valores para el Balance GOS Token  --}}
                    <input type="hidden" id="contador1" name="contadorBalanceGosToken" value="{{ $trnxs->count() }}">
                    <input type="hidden" id="total_Tokens2" name="toatlTokensBalanceGosTokens"
                        value="{{ number_format($totalTokenGos,2) }}">
                    <input type="hidden" id="total_AmountUSD2" name="totalUsdAmountBalanceGosToken"
                        value="{{ $totalUsdAmountTokens  }}">

                        
                    <div class="col-lg-3 col-md-6 col-sm-3 col-xs-2 col-3 " style="text-align: center;">
                        <strong class="min" data-id="{{ ___('Total Transactions') }}">
                            {{ ___('Total Transactions') }}
                        </strong>
                        <br>
                        <span id="totalTransactions"></span>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-3 col-xs-2 col-3 " style="text-align: center;">
                        <strong class="titulo min" data-id=" {{ ___('Total ') }}Tokens">
                            {{ ___('Total ') }}Tokens
                        </strong>
                        <br>
                        <small id="totalTokens">
                        </small>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-3 col-xs-2 col-3 " style="text-align: center;">
                        <strong class="min" data-id="{{ ___('Total USD Amount') }}">
                            {{ ___('Total USD Amount') }}
                        </strong>
                        <br>
                        <small id="totalUsdAmount">
                        </small>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-3 col-xs-2 col-3 " style="text-align: center;">
                        {{-- <strong class="min" data-id="{{ ___('Total Amount') }}">
                        {{ ___('Total Amount') }}
                    </strong>
                    <br>
                    <small id="totalAmount">
                    </small> --}}
                    </div>
                    <div class="col-lg-2 col-sm-12 col-xs-12" style="text-align:center;">
                        <a href="#" id="pdf" class="btn btn-auto btn-sm btn-primary pdf" type="submit"
                            style="width: 100px; border-radius: 10px; top: 2px;" target="_blank">
                            <em class="fas fa-file-pdf"></em><span> <span
                                    class="">{{ ___(' PDF (All)') }}</span></span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('modals')
    @foreach ($trnxBonusDollarsAll as $bon)
        @if ($bon->type == 'bonus')
            <div class="modal fade" id="modal-medium{{ $bon->id }}" tabindex="-1" style="padding-right: 17px;">
                <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                    <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                            aria-label="Close"><em class="ti ti-close"></em></a>
                        <div class="popup-body">
                            <div class="content-area popup-content">
                                <div class="card-head d-flex justify-content-between align-items-center">
                                    <h4 class="card-title mb-0">{{ ___('Transaction Dollars from') }}
                                        ({{ $bon->transaction ? $bon->transaction->tnx_id : '' }})
                                    </h4>
                                    <div class="trans-status">
                                        <span class="badge badge-success ucap">{{ ___('Send') }}</span>
                                    </div>
                                </div>
                                <div class="gaps-2-5x"></div>
                                <h6 class="card-sub-title">{{ ___('Transaction Bonus Details') }}</h6>
                                <ul class="data-details-list">
                                    <li>
                                        <div class="data-details-head">ID {{ ___('Bonus') }} </div>
                                        <div class="data-details-des">
                                            <span>{{ $tipobono }}{{ $bon->id }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('User') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->userBy->name }}</span>
                                            <span>({{ ___('User Type') }} {{ $bon->userBy->type_user }}) </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Amount') }}</div>
                                        <div class="data-details-des">
                                            @php
                                                $color = '';
                                                $signo = '';
                                                if ($bon->status == 'canceled') {
                                                    $color = 'red';
                                                    $signo = '-';
                                                } else {
                                                    $color = 'black';
                                                    $signo = '';
                                                }
                                            @endphp
                                            <span
                                                style="color:{{ $color }}">{{ $signo . ' ' . $bon->amount }}</span>(USDT)
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('From Transaction') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->transaction ? $bon->transaction->tnx_id : '' }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Status') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->status == 'canceled' ? ucfirst($bon->status) : 'Done' }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Date') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->created_at }}
                                            </span>
                                            <span>{!! fechaOrden(\Carbon\Carbon::parse($bon->created_at)) !!} </span>
                                        </div>
                                    </li>
                            </div>
                        </div>
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        @else
            <div class="modal fade" id="modal-medium2{{ $bon->id }}" tabindex="-1" style="padding-right: 17px;">
                <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                    <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                            aria-label="Close"><em class="ti ti-close"></em></a>
                        <div class="popup-body">
                            <div class="content-area popup-content">
                                <div class="card-head d-flex justify-content-between align-items-center">
                                    <h4 class="card-title mb-0">{{ ___('Bonus Dollars Transaction from') }}
                                        ({{ $bon->code }})
                                    </h4>
                                </div>
                                <div class="trans-status">
                                    <span
                                        class="badge badge-{{ $bon->status == 'canceled' ? 'danger' : 'warning' }} ucap">
                                        {{ ___($bon->status) }}
                                    </span>
                                </div>
                                <div class="p-2"></div>
                                <h6 class="card-sub-title">{{ __('Transaction Bonus Details') }}</h6>
                                <ul class="data-details-list">
                                    <li>
                                        <div class="data-details-head">ID {{ ___('Bonus') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->code }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Name o Username') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->userBy->name ? $bon->userBy->name : $bon->userBy->username }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Amount') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->amount }} (USDT)
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Wallet of this withdrawal') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->wallet }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Status') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->status == 'canceled' ? ucfirst($bon->status) : 'Done' }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Date') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $bon->created_at }}
                                            </span>
                                            <span>{!! fechaOrden(\Carbon\Carbon::parse($bon->created_at)) !!} </span>
                                        </div>
                                    </li>
                                    @if ($bon->status == 'canceled')
                                        <li>
                                            <div class="data-details-head">{{ ___('Comment') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $bon->comment }}</span>
                                            </div>
                                        </li>
                                    @endif
                            </div>
                        </div>
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        @endif
    @endforeach
    @foreach ($trnxBonusStart as $start)
        <div class="modal fade" id="modal-start{{ $start->tnx_id }}" tabindex="-1" style="padding-right: 17px;">
            <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                        aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <div class="content-area popup-content">
                            <div class="card-head d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-0">
                                    {{ ___('Transaction Bonus for ') }}
                                    {{ $start->tnx_type == 'join' ? 'joining' : 'launch' }}
                                    ({{ $start->tnx_id }})
                                </h4>
                            </div>
                            <div class="trans-status">
                                <span class="badge badge-success ucap">
                                    {{ ___('Done') }}
                                </span>
                            </div>
                            <div class="p-2"></div>
                            <h6 class="card-sub-title">{{ ___('Transaction Bonus Details') }}</h6>
                            <ul class="data-details-list">
                                <li>
                                    <div class="data-details-head">ID {{ ___('Bonus') }}</div>
                                    <div class="data-details-des">
                                        <span>{{ $start->tnx_id }}
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="data-details-head">Tokens</div>
                                    <div class="data-details-des">
                                        <span>
                                            {{ $start->tokens }} {{ token('symbol') }}
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="data-details-head">{{ ___('Date') }}</div>
                                    <div class="data-details-des">
                                        <span>{{ $start->tnx_time }}
                                        </span>
                                        <span>{!! fechaOrden(\Carbon\Carbon::parse($start->tnx_time)) !!} </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="data-details-head">{{ ___('Type') }}</div>
                                    <div class="data-details-des">
                                        <span>{{ $start->tnx_type }}</span>
                                    </div>
                                </li>
                        </div>
                    </div>
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
    @endforeach
@endsection
@push('footer')
    @include('user.includes.script-transactions-bonus')
@endpush
