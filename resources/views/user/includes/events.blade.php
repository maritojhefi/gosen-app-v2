<style>
    .popup {
        position: fixed;
        right: 30px;
        bottom: -50px;
        width: 380px;
        transform: translateY(100%);
        transition: all .4s;
        background: rgba(0, 0, 0, 0);
        z-index: 99;
    }
    .popup.active {
        transform: translateY(0);
        bottom: 30px;
        transition-delay: 2s;
    }

    .popup.hidde {
        transform: translateX(100%);
        right: -50px;
        transition: all .4s;
    }
    .notify {
        width: auto;
        background-color: whitesmoke;
        overflow: hidden;
        cursor: pointer;
        margin: 10px 0;
        box-shadow: 0 6px 60px 0 rgb(18 26 38 / 15%);
    }
    .promo-content-wrap {
        padding: 20px;
        padding-top: 0px;
        align-items: center;
    }
    .promo-content-img {
        width: 84px;
        height: 81px;
        flex-shrink: 0;
        margin-right: 15px;
        border: 2px solid #2b56f5;
    }
    .promo-content-text h5 {
        font-size: 12px;
        line-height: 22px;
        color: #202737;
        font-weight: 500;
        margin-bottom: 7px;
    }
    .promo-content-text h5 span {
        font-size: 14px;
        color: #2b56f5;
    }
    .promo-content-text p {
        font-size: 11px;
        line-height: 18px;
        color: #737e97;
        font-weight: 500;
    }
    .imge{
        width: 84px;
        height: 77px;
    }
    .close{
        top: 10px;
        right: -22px;
        font-size: 1rem;
    }
    /* .avatar-title {
        height: 40%;
        width: 100%;
        } */

    @media (max-width: 500px){
        .popup {
            width: calc(100% - 60px);
        }
    }
    @media (min-width: 416px){
        .popup.active {
            bottom: 40px;
        }
        .popup {
            right: 40px;
        }
        .promo-content-wrap {
            padding-top: 0px;
        }
        .promo-content-img {
            width: 124px;
            height: 116px;
            margin-right: 30px;
        }
        .imge{
            width: 106px;
            height: 102px;
        }
        .promo-content-text h5 {
            font-size: 14px;
        }
        .promo-content-text h5 span {
            font-size: 16px;
        }
        .promo-content-text p {
            font-size: 13px;
        }
        .close{
            top: 10px;
            right: -38px;
        }
    }
    @media (min-width: 375px){
        .promo-content-img {
            width: 110px;
            height: 106px ;
        }
        .imge{
            width: 106px;
            height: 102px;
        }
    }
</style>


@push("footer")
<script>
    $(document).ready(function () {
        var ob = document.getElementById("popup");
            ob.classList.add("active");
        $("#popup").fadeIn();

        $("#close").on("click", function (e) {
            e.preventDefault();
            $("#popup").fadeOut();
            ob.classList.remove("active");
        });
    });
    $('.notify').click(function () {
        var ob = document.getElementById("popup");
            ob.classList.add("hidde");
            $(this).fadeOut();
    });
</script>
@endpush