<script>
    $(document).ready(function() {
        if (screen.width < 1219)
            $('.tabla').addClass('table-responsive');
        else if (screen.width > 1219)
            $('.tabla').removeClass('table-responsive');
        if (screen.width < 530) {
            $(".min").each(function(index) {
                let result = "";
                var str = $(this).attr('data-id');
                // Traverse the string.
                let v = true;
                for (let i = 0; i < str.length; i++) {
                    // If it is space, set v as true.
                    if (str[i] == ' ') {
                        v = true;
                    } else if (str[i] != ' ' && v == true) {
                        result += (str[i]);
                        v = false;
                    }
                }
                $(this).text(result);
            });
        } else {
            $(".min").each(function(index) {
                $(this).text($(this).attr('data-id'));
            });
        }
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
        if ($(".bonusUSD").hasClass("active")) {
            $("#tipo").prop("value", '4');
            $('#totalTransactions').html($('#contador4').val() + ' Tranx');
            $('#totalTokens').html('~');
            $('#totalAmount').html($('#sumatoriaMontos').val() + ' (USDT)');
            $('#totalUsdAmount').html($('#sumatoriaMontos').val() + ' (USDT)');
        }
        if ($(".bonusStart").hasClass("active")) {
            $("#tipo").prop("value", '3');
            $('#totalTransactions').html($('#contador3').val() + ' Tranx');
            $('#totalTokens').html($('#total_Tokens').val() + ' GOS');
            $('#totalUsdAmount').html('~');
        }
        if ($(".gosToken").hasClass("active")) {
            $("#tipo").prop("value", '2');
            $('#totalTransactions').html($('#contador2').val() + ' Tranx');
            $('#totalTokens').html($('#total_Tokens1').val() + ' GOS');
            // $('#totalAmount').html($('#total_Amount1').val() + ' (USDT)');
            $('#totalUsdAmount').html($('#total_AmountUSD1').val() + ' (USDT)');
        }
        if ($(".balanceGos").hasClass("active")) {
            $("#tipo").prop("value", '1');
            $('#totalTransactions').html($('#contador1').val() + ' Tranx');
            $('#totalTokens').html($('#total_Tokens2').val() + ' GOS');
            // $('#totalAmount').html($('#total_Amount2').val() + ' (USDT)');
            $('#totalUsdAmount').html($('#total_AmountUSD1').val() + ' (USDT)');
        }
    });
    $(window).resize(function() {
        if (screen.width < 1219)
            $('.tabla').addClass('table-responsive');
        else if (screen.width > 1219)
            $('.tabla').removeClass('table-responsive');
        if (screen.width < 530) {
            $(".min").each(function(index) {
                let result = "";
                var str = $(this).attr('data-id');
                // Traverse the string.
                let v = true;
                for (let i = 0; i < str.length; i++) {
                    // If it is space, set v as true.
                    if (str[i] == ' ') {
                        v = true;
                    } else if (str[i] != ' ' && v == true) {
                        result += (str[i]);
                        v = false;
                    }
                }
                $(this).text(result);
            });
        } else {
            $(".min").each(function(index) {
                $(this).text($(this).attr('data-id'));
            });
        }
    });
    $(".bonusUSD").on('click', function(event) {
        $("#tipo").prop("value", '4');
        $('#totalTransactions').html($('#contador4').val() + ' Tranx');
        $('#totalTokens').html('~');
        $('#totalAmount').html($('#sumatoriaMontos').val() + ' (USDT)');
        $('#totalUsdAmount').html($('#sumatoriaMontos').val() + ' (USDT)');
    });
    $(".bonusStart").on('click', function(event) {
        $("#tipo").prop("value", '3');
        $('#totalTransactions').html($('#contador3').val() + ' Tranx');
        $('#totalTokens').html($('#total_Tokens').val() + ' GOS');
        // $('#totalAmount').html('~');
        $('#totalUsdAmount').html('~');
    });
    $(".gosToken").on('click', function(event) {
        $("#tipo").prop("value", '2');
        $('#totalTransactions').html($('#contador2').val() + ' Tranx');
        $('#totalTokens').html($('#total_Tokens1').val() + ' GOS');
        // $('#totalAmount').html($('#total_Amount1').val() + ' (USDT)');
        $('#totalUsdAmount').html($('#total_AmountUSD1').val() + ' (USDT)');
    });
    $(".balanceGos").on('click', function(event) {
        $("#tipo").prop("value", '1');
        $('#totalTransactions').html($('#contador1').val() + ' Tranx');
        $('#totalTokens').html($('#total_Tokens2').val() + ' GOS');
        // $('#totalAmount').html($('#total_Amount2').val() + ' (USDT)');
        $('#totalUsdAmount').html($('#total_AmountUSD1').val() + ' (USDT)');
    });
</script>
<script>
    $(document).ready(function() {
        var table = $('#miTabla').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofc"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(0).search(offices, true, false, false).draw(false);
        });
    });
    $(document).ready(function() {
        var table = $('#miTabla2').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofc2"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(0).search(offices, true, false, false).draw(false);
        });

        var table = $('#miTabla2').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofc3"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(6).search(offices, true, false, false).draw(false);
        });
    });
</script>
<script>
    $("#pdf").click(function() {
        $("#form").submit();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
<script>
    (function() {
        var
            form = $('#data'),
            cache_width = form.width(),
            a4 = [595.28, 841.89]; // for a4 size paper width and height  

        $('#create_pdf').on('click', function() {
            $('body').scrollTop(0);
            createPDF();
        });
        //create pdf  
        function createPDF() {
            getCanvas().then(function(canvas) {
                var
                    img = canvas.toDataURL("image/png"),
                    doc = new jsPDF({
                        unit: 'px',
                        format: 'a4',
                        orientation: 'landscape'
                    });
                doc.addImage(img, 'JPEG', 20, 20);
                doc.save('Report-Bonus-Dollars.pdf');
                form.width(cache_width);
            });
        }
        // create canvas object  
        function getCanvas() {
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
            return html2canvas(form, {
                imageTimeout: 2000,
                removeContainer: false
            });
        }
    }());
</script>
