@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.min.css">
    <style>
        .owl-theme .owl-dots,
        .owl-theme .owl-nav {
            text-align: center;
            -webkit-tap-highlight-color: transparent
        }

        .owl-theme .owl-nav {
            margin-top: 10px;
        }

        .owl-theme .owl-nav [class*=owl-] {
            color: #FFF;
            font-size: 14px;
            margin: 5px;
            padding: 4px 7px;
            background: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            display: inline-block;
            cursor: pointer;
            border-radius: 30px;
        }

        .owl-theme .owl-nav [class*=owl-]:hover {
            background: #869791;
            color: #FFF;
            text-decoration: none
        }

        .owl-theme .owl-nav .disabled {
            opacity: .5;
            cursor: default
        }

        .owl-theme .owl-nav.disabled+.owl-dots {
            margin-top: 10px
        }

        .owl-theme .owl-dots .owl-dot {
            display: inline-block;
            zoom: 1
        }

        .owl-theme .owl-dots .owl-dot span {
            width: 10px;
            height: 10px;
            margin: 5px 7px;
            background: #D6D6D6;
            display: block;
            -webkit-backface-visibility: visible;
            transition: opacity .2s ease;
            border-radius: 30px
        }

        .owl-theme .owl-dots .owl-dot.active span,
        .owl-theme .owl-dots .owl-dot:hover span {
            background: #869791
        }



        .animated {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-delay: 500ms;
            animation-delay: 500ms;
        }

        .animate-out {
            -webkit-animation-delay: 0ms;
            animation-delay: 0ms;
        }

        .h4-owl {
            font-size: 28px;
        }

        .p-owl {
            width: 50%;
            text-align: center;
            margin: 0 auto 20px;
        }

        .owl-item {
            display: table;
        }

        .owl-carousel .item {
            height: 0;
            padding-top: 23%;
            padding-bottom: 22%;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            border-radius: 20px;
        }

        .btn-owl {
            display: inline-block;
            line-height: 35px;
            height: 35px;
            text-align: center;
            padding: 0 20px;
            width: auto;
            background-color: #000;
            text-decoration: none;
            color: #fff;
        }

        @media (max-width: 576px) {
            .owl-carousel .item {
                height: 0;
                padding-top: 35%;
                padding-bottom: 0%;
                vertical-align: bottom;
                margin-bottom: 0px !important;
                height: 200px !important;
                gap: 1 !important;
            }
        }




        /*Owl Animation*/
        .owl-item {
            overflow: hidden;
        }

        .fxSwipe .owl-stage,
        .fxPushReveal .owl-stage,
        .fxSnapIn .owl-stage,
        .fxLetMeIn .owl-stage,
        .fxStickIt .owl-stage,
        .fxSlideBehind .owl-stage {
            transform: none !important;
            width: 100% !important;
            position: relative;
            height: 0;
            border: none;
            overflow: hidden;
            display: block;
            padding-top: 31.25%;
        }

        .fxSwipe .owl-item,
        .fxPushReveal .owl-item,
        .fxSnapIn .owl-item,
        .fxLetMeIn .owl-item,
        .fxStickIt .owl-item,
        .fxSlideBehind .owl-item {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0 !important;
            left: 0 !important;
            opacity: 0;
            z-index: 10;
        }

        .fxSwipe .owl-item.active,
        .fxPushReveal .owl-item.active,
        .fxSnapIn .owl-item.active,
        .fxLetMeIn .owl-item.active,
        .fxStickIt .owl-item.active,
        .fxSlideBehind .owl-item.active {
            z-index: 20;
            opacity: 1;
        }

        .fxSwipe .owl-item.owl-animated-out,
        .fxPushReveal .owl-item.owl-animated-out,
        .fxSnapIn .owl-item.owl-animated-out,
        .fxLetMeIn .owl-item.owl-animated-out,
        .fxStickIt .owl-item.owl-animated-out,
        .fxSlideBehind .owl-item.owl-animated-out {
            opacity: 1;
        }

        .fxSwipe .owl-item.owl-animated-in,
        .fxPushReveal .owl-item.owl-animated-in,
        .fxSnapIn .owl-item.owl-animated-in,
        .fxLetMeIn .owl-item.owl-animated-in,
        .fxStickIt .owl-item.owl-animated-in,
        .fxSlideBehind .owl-item.owl-animated-in {
            opacity: 0;
        }

        /*****************************************/
        /* Soft Scale */
        /*****************************************/
        .fxSoftScale .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxSoftScaleOutNext {
            animation-name: scaleUp;
        }

        .fxSoftScaleInNext {
            animation-name: scaleDownUp;
        }

        .fxSoftScaleOutPrev {
            animation-name: scaleDown;
        }

        .fxSoftScaleInPrev {
            animation-name: scaleUpDown;
        }

        @keyframes scaleUp {
            from {
                opacity: 1;
            }

            to {
                transform: scale(1.2);
                opacity: 0;
            }
        }

        @keyframes scaleDownUp {
            from {
                opacity: 0;
                transform: scale(0.9);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        @keyframes scaleDown {
            to {
                opacity: 0;
                transform: scale(0.9);
            }
        }

        @keyframes scaleUpDown {
            from {
                transform: scale(1.2);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Press away */
        /*****************************************/
        .fxPressAway .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxPressAwayOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxPressAwayInNext {
            animation-name: slideInFromLeft;
        }

        .fxPressAwayOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxPressAwayInPrev {
            animation-name: slideInFromRight;
        }

        @keyframes slideOutScaleRight {
            to {
                transform: translateX(100%) scale(0.9);
                opacity: 0;
            }
        }

        @keyframes slideInFromLeft {
            from {
                transform: translateX(-100%);
            }

            to {
                transform: translateX(0);
            }
        }

        @keyframes slideOutScaleLeft {
            to {
                transform: translateX(-100%) scale(0.9);
                opacity: 0;
            }
        }

        @keyframes slideInFromRight {
            from {
                transform: translateX(100%);
            }

            to {
                transform: translateX(0);
            }
        }

        /*****************************************/
        /* Slide Swing */
        /*****************************************/
        .fxSideSwing .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(1, -0.2, 0, 1);
        }

        .fxSideSwingOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxSideSwingInNext {
            animation-name: slideInFromLeft;
        }

        .fxSideSwingOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxSideSwingInPrev {
            animation-name: slideInFromRight;
        }

        /*****************************************/
        /* Fortune wheel */
        /*****************************************/
        .fxFortuneWheel .owl-stage {
            perspective: 1600px;
        }

        .fxFortuneWheel .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxFortuneWheelOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxFortuneWheelInNext {
            transform-origin: 100% 50%;
            animation-name: rotateInFromLeft;
        }

        .fxFortuneWheelOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxFortuneWheelInPrev {
            transform-origin: 0% 50%;
            animation-name: rotateInFromRight;
        }

        @keyframes rotateInFromLeft {
            from {
                transform: translateX(-100%) rotateY(-55deg);
            }

            to {
                transform: translateX(0) rotateY(0deg);
                opacity: 1;
            }
        }

        @keyframes rotateInFromRight {
            from {
                transform: translateX(100%) rotateY(55deg);
            }

            to {
                transform: translateX(0) rotateY(0deg);
                opacity: 1;
            }
        }

        /*****************************************/
        /* Swipe */
        /*****************************************/
        .fxSwipe .owl-item.fxSwipeOutNext {
            z-index: 30;
        }

        .fxSwipe .owl-item.fxSwipeInPrev {
            opacity: 1;
        }

        .fxSwipeOutNext {
            animation: decreaseHeight 0.8s forwards ease-in-out;
        }

        .fxSwipeInNext {
            animation: show 0.8s forwards ease-in-out;
        }

        .fxSwipeOutPrev {
            animation: hide 0.8s forwards ease-in-out;
        }

        .fxSwipeInPrev {
            animation: increaseHeight 0.8s forwards ease-in-out;
        }

        @keyframes increaseHeight {
            from {
                height: 0;
            }

            to {
                height: 100%;
            }
        }

        @keyframes decreaseHeight {
            to {
                height: 0;
            }
        }

        @keyframes show {
            0% {
                opacity: 0;
            }

            1%,
            100% {
                opacity: 1;
            }
        }

        @keyframes hide {

            0%,
            99% {
                opacity: 1;
            }

            100% {
                opacity: 0;
            }
        }

        /*****************************************/
        /* Push reveal */
        /*****************************************/
        .fxPushReveal .owl-item.animated {
            opacity: 1;
            animation-duration: 0.7s;
            animation-fill-mode: forwards;
            animation-timing-function: ease-in-out;
        }

        .fxPushRevealOutNext {
            animation-name: slideOutBottom;
        }

        .fxPushRevealInNext {
            animation-name: slideInHalfFromTop;
        }

        .fxPushReveal .fxPushRevealInNext.owl-item {
            z-index: 5;
        }

        .fxPushRevealOutPrev {
            animation-name: slideOutHalfTop;
        }

        .fxPushRevealInPrev {
            animation-name: slideInFromBottom;
        }

        @keyframes slideOutBottom {
            to {
                transform: translateY(100%);
            }
        }

        @keyframes slideInHalfFromTop {
            from {
                transform: translateY(-50%);
            }

            to {
                transform: translateY(0);
            }
        }

        @keyframes slideOutHalfTop {
            to {
                transform: translateY(-50%);
            }
        }

        @keyframes slideInFromBottom {
            from {
                transform: translateY(100%);
            }

            to {
                transform: translateY(0);
            }
        }

        /*****************************************/
        /* Snap in */
        /*****************************************/
        .fxSnapIn .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.8);
            transition: opacity 0.4s 0.1s ease-in;
        }

        .fxSnapIn .owl-item.owl-animated-out:after {
            opacity: 1;
        }

        .fxSnapIn .owl-item.owl-animated-in {
            opacity: 1;
        }

        .fxSnapIn .animated {
            animation-duration: 0.5s;
            animation-fill-mode: forwards;
        }

        .fxSnapIn .owl-animated-in {
            animation-timing-function: ease-in;
        }

        .fxSnapIn .owl-animated-out {
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxSnapInOutNext {
            animation-name: slideOutLeft;
        }

        .fxSnapInInNext {
            animation-name: slideFromRightFast;
        }

        .fxSnapInOutPrev {
            animation-name: slideOutRight;
        }

        .fxSnapInInPrev {
            animation-name: slideFromLeftFast;
        }

        @keyframes slideOutLeft {
            to {
                transform: translateX(-10%);
            }
        }

        @keyframes slideFromRightFast {

            0%,
            50% {
                transform: translateX(100%);
            }

            100% {
                transform: translateX(0%);
            }
        }

        @keyframes slideOutRight {
            to {
                transform: translateX(10%);
            }
        }

        @keyframes slideFromLeftFast {

            0%,
            50% {
                transform: translateX(-100%);
            }

            100% {
                transform: translateX(0%);
            }
        }

        /*****************************************/
        /* Let me in */
        /*****************************************/
        .fxLetMeIn .owl-stage {
            perspective: 1600px;
        }

        .fxLetMeIn .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.6);
            transition: opacity 0.5s ease-in-out;
        }

        .fxLetMeIn .owl-item.fxLetMeInOutNext:after,
        .fxLetMeIn .owl-item.fxLetMeInOutPrev:after {
            opacity: 1;
        }

        .fxLetMeIn .owl-item.fxLetMeInInNext,
        .fxLetMeIn .owl-item.fxLetMeInInPrev {
            z-index: 30;
            opacity: 1;
        }

        .fxLetMeInOutNext {
            transform-origin: 0% 50%;
            animation: rotateOutRight 0.5s forwards ease-in-out;
        }

        .fxLetMeInInNext {
            animation: slideFromRightFast 0.5s forwards ease;
        }

        .fxLetMeInOutPrev {
            transform-origin: 100% 0%;
            animation: rotateOutLeft 0.5s forwards ease-in-out;
        }

        .fxLetMeInInPrev {
            animation: slideFromLeftFast 0.5s forwards ease;
        }

        @keyframes rotateOutRight {
            to {
                transform: rotateY(10deg);
            }
        }

        @keyframes rotateOutLeft {
            to {
                transform: rotateY(-10deg);
            }
        }

        /*****************************************/
        /* Stick it */
        /*****************************************/
        .fxStickIt .owl-stage {
            perspective: 1600px;
        }

        .fxStickIt .owl-item.fxStickItInNext {
            opacity: 1;
        }

        .fxStickItOutNext {
            transform-origin: 50% 0%;
            animation: rotateBottomSideOut 0.8s forwards ease-in;
        }

        .fxStickItInNext {
            animation: slideInFromBottomDelayed 0.8s forwards;
        }

        .fxStickItOutPrev {
            opacity: 1;
            animation: slideOutToBottom 0.8s forwards;
        }

        .fxStickItInPrev {
            transform-origin: 50% 0%;
            animation: rotateBottomSideIn 0.8s 0.1s forwards ease-in;
        }

        @keyframes rotateBottomSideOut {
            40% {
                transform: rotateX(-15deg);
                animation-timing-function: ease-out;
            }

            100% {
                opacity: 0;
                transform: scale(0.8) translateZ(-200px);
            }
        }

        @keyframes slideInFromBottomDelayed {

            0%,
            30% {
                transform: translateY(100%);
            }

            100% {
                transform: translateY(0);
            }
        }

        @keyframes rotateBottomSideIn {
            0% {
                opacity: 0;
                transform: scale(0.8) translateZ(-200px);
            }

            60% {
                transform: scale(1) translateZ(0) rotateX(-15deg);
                animation-timing-function: ease-out;
            }

            100% {
                opacity: 1;
                transform: scale(1) translateZ(0) rotateX(0deg);
            }
        }

        /*****************************************/
        /* Archive me */
        /*****************************************/
        .fxArchiveMe .owl-item:before,
        .fxArchiveMe .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.7);
            transition: opacity 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMe .owl-item.fxArchiveMeInNext {
            z-index: 30;
            opacity: 1;
        }

        .fxArchiveMe .owl-item.fxArchiveMeInNext:after {
            transition: none;
        }

        .fxArchiveMe .owl-item:after,
        .fxArchiveMe .owl-item.fxArchiveMeOutNext:before {
            opacity: 1;
        }

        .fxArchiveMe .owl-item:before,
        .fxArchiveMe .owl-item.active:after,
        .fxArchiveMe .owl-item.fxArchiveMeInNext:after,
        .fxArchiveMe .owl-item.fxArchiveMeInPrev:after {
            opacity: 0;
        }

        .fxArchiveMeOutNext {
            animation: scaleHalfDown 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeInNext {
            animation: slideInFromBottom 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeOutPrev {
            animation: slideOutToBottom 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeInPrev {
            animation: scaleHalfUp 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        @keyframes scaleHalfDown {
            to {
                transform: scale(0.6);
                opacity: 0;
            }
        }

        @keyframes slideOutToBottom {
            to {
                transform: translateY(100%);
            }
        }

        @keyframes scaleHalfUp {
            from {
                opacity: 0;
                transform: scale(0.6);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Vertical growth */
        /*****************************************/
        .fxVGrowth .owl-item.fxVGrowthInNext,
        .fxVGrowth .owl-item.fxVGrowthInPrev {
            z-index: 30;
            opacity: 1;
        }

        .fxVGrowthOutNext {
            animation: scaleDown 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthInNext {
            transform-origin: 50% 100%;
            animation: maximize 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthOutPrev {
            animation: scaleDown 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthInPrev {
            transform-origin: 50% 0%;
            animation: maximize 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        @keyframes maximize {
            from {
                transform: scale(0);
            }

            to {
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Slide Behind */
        /* From https://github.com/hakimel/kontext by Hakim El Hattab, http://hakim.se */
        /*****************************************/
        .fxSlideBehind .owl-stage {
            perspective: 1000px;
        }

        .fxSlideBehindOutNext {
            animation: hideLeft 0.8s forwards;
        }

        .fxSlideBehindInNext {
            animation: showRight 0.8s forwards;
        }

        .fxSlideBehindOutPrev {
            animation: hideRight 0.8s forwards;
        }

        .fxSlideBehindInPrev {
            animation: showLeft 0.8s forwards;
        }

        @keyframes hideLeft {
            0% {
                transform: translateZ(0px);
            }

            40% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                z-index: 30;
            }

            100% {
                opacity: 1;
                transform: translateZ(-400px);
            }
        }

        @keyframes showRight {
            0% {
                transform: translateZ(-400px);
                opacity: 1;
            }

            40% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                opacity: 1;
            }

            41% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                opacity: 1;
                z-index: 30;
            }

            100% {
                transform: translateZ(0px);
                opacity: 1;
                z-index: 30;
            }
        }

        @keyframes hideRight {
            0% {
                transform: translateZ(0px);
            }

            40% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                z-index: 30;
            }

            100% {
                opacity: 1;
                transform: translateZ(-400px);
            }
        }

        @keyframes showLeft {
            0% {
                transform: translateZ(-400px);
                opacity: 1;
            }

            40% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                opacity: 1;
            }

            41% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                opacity: 1;
                z-index: 30;
            }

            100% {
                transform: translateZ(0px);
                opacity: 1;
                z-index: 30;
            }
        }

        /*****************************************/
        /* Soft Pulse */
        /*****************************************/
        .fxSoftPulseOutPrev,
        .fxSoftPulseOutNext {
            animation: scaleUpFadeOut 0.8s forwards ease-in;
        }

        .fxSoftPulseInPrev,
        .fxSoftPulseInNext {
            animation: scaleDownFadeIn 0.8s forwards ease-out;
        }

        @keyframes scaleUpFadeOut {
            50% {
                transform: scale(1.2);
                opacity: 1;
            }

            75% {
                transform: scale(1.1);
                opacity: 0;
            }

            100% {
                transform: scale(1);
                opacity: 0;
            }
        }

        @keyframes scaleDownFadeIn {
            50% {
                opacity: 1;
                transform: scale(1.2);
            }

            100% {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Earthquake */
        /* From https://elrumordelaluz.github.io/csshake/ by Lionel, http://t.co/thCECnx1Yg */
        /*****************************************/
        .fxEarthquake .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.3);
            transition: opacity 0.5s;
        }

        .fxEarthquake .owl-item:after {
            opacity: 1;
        }

        .fxEarthquake .owl-item.fxEarthquakeInPrev {
            opacity: 1;
        }

        .fxEarthquakeOutNext {
            animation: shakeSlideBottom 1s 0.1s forwards;
        }

        .fxEarthquakeInNext {
            animation: pushFromTop 1s 0.1s forwards;
        }

        .fxEarthquakeOutPrev {
            animation: shakeSlideTop 1s 0.1s forwards;
        }

        .fxEarthquakeInPrev {
            animation: pushFromBottom 1s 0.1s forwards;
        }

        @keyframes shakeSlideBottom {
            0% {
                transform: translate(0px, 0px) rotate(0deg);
            }

            2% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            4% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            6% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            8% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            10% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            12% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            14% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            16% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            18% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            20% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            22% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            24% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            26% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            28% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            30% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            32% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            34% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            36% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            38% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            40% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            42% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            44% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            46% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            48% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            50% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            52% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            54% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            56% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            58% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            60% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            62% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            64% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            66% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            68% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            70% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            100% {
                transform: translateY(100%);
            }
        }

        @keyframes pushFromTop {

            0%,
            70% {
                opacity: 0;
                transform: translateY(-100%);
            }

            100% {
                opacity: 1;
                transform: translateY(0);
            }
        }

        @keyframes shakeSlideTop {
            0% {
                transform: translate(0px, 0px) rotate(0deg);
            }

            2% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            4% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            6% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            8% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            10% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            12% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            14% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            16% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            18% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            20% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            22% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            24% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            26% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            28% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            30% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            32% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            34% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            36% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            38% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            40% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            42% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            44% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            46% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            48% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            50% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            52% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            54% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            56% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            58% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            60% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            62% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            64% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            66% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            68% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            70% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            100% {
                transform: translateY(-100%);
            }
        }

        @keyframes pushFromBottom {

            0%,
            70% {
                opacity: 0;
                transform: translateY(100%);
            }

            100% {
                opacity: 1;
                transform: translateY(0);
            }
        }

        /*****************************************/
        /* Cliff diving */
        /*****************************************/
        .fxCliffDiving .owl-item {
            transform-origin: 50% 400%;
        }

        .fxCliffDiving .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 1);
            transition: opacity 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDiving .owl-item.fxCliffDivingInNext,
        .fxCliffDiving .owl-item.fxCliffDivingInPrev {
            opacity: 1;
        }

        .fxCliffDivingOutNext {
            animation: rotateOutCircLeft 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingInNext {
            animation: rotateInCircRight 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingOutPrev {
            animation: rotateOutCircRight 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingInPrev {
            animation: rotateInCircLeft 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        @keyframes rotateOutCircLeft {
            to {
                transform: rotate(-20deg) translateX(-100%);
            }
        }

        @keyframes rotateInCircRight {
            from {
                transform: rotate(20deg) translateX(100%);
            }

            to {
                transform: rotate(0deg) translateX(0);
            }
        }

        @keyframes rotateOutCircRight {
            to {
                transform: rotate(20deg) translateX(100%);
            }
        }

        @keyframes rotateInCircLeft {
            from {
                transform: rotate(-20deg) translateX(-100%);
            }

            to {
                transform: rotate(0deg) translateX(0);
            }
        }

        .owl-nav {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: left !important;
            margin-top: 0;
            height: 0;
        }

        .owl-nav [class*=owl-] {
            transform: translateY(-50%);
            margin: 0 20px !important;
            padding: 8px 14px !important;
        }

        .owl-next {
            float: right;
        }

        .owl-dots {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
        }

        .owl-dots .owl-dot span {
            width: 14px !important;
            height: 14px !important;
        }




        @media only screen and (max-width: 580px) {
                {
                .tamano {
                    top: -50px;
                    position: relative;
                }
            }

            .button-container {
                flex-direction: row;
                gap: 28px !important;
                top: -135px;
                right: -63px;
                position: inherit;
            }


            .button-container {
                flex-direction: column;
            }

            .button-container button {
                margin-bottom: 10px;
            }
        }
    </style>
@endpush
<div class="referral-form">
    <div class="row" style="display: flex; align-items: center; justify-content: center;">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 pb-2">
            {{-- <h2 class="card-title card-title-sm pb-3">{{ ucfirst(___('Copie la URL de su pagina de Disruptive favorita')) }}</h2> --}}
            <div class="row mb-3">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <button id="btn-landing1" class="btn btn-opcion-landing border btn-opcion-active" type="button">Landing 1</button>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <button id="btn-landing2" class="btn btn-opcion-landing border btn-opcion-inactive" type="button">Landing 2</button>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <button id="btn-landing3" class="btn btn-opcion-landing border btn-opcion-inactive" type="button">Landing 3</button>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <button id="btn-landing4" class="btn btn-opcion-landing border btn-opcion-inactive" type="button">Landing 4</button>
                </div>
            </div>
            @php
                $user = empty($data) ? auth()->user() : $data;
                $idioma = $user->lang;
            @endphp
            <!-- Landing 1 -->
            <div id="container-landing1" class="swiper swiper-landing mySwiper-landing landing">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng1.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy1" value="{{ route('public.referral') . '/' . 'd1' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy1" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner2">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng2.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy2" value="{{ route('public.referral') . '/' . 'd2' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy2" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner2">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng3.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy3" value="{{ route('public.referral') . '/' . 'd3' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy3" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner3">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng4.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy4" value="{{ route('public.referral') . '/' . 'd4' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy4" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner4">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng5.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy5" value="{{ route('public.referral') . '/' . 'd5' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy5" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner5">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <img loading="lazy" class="rounded" src="{{ asset2('images/landing/CapturaLdng6.JPG') }}">
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2 container-btn-copy">
                                    <input type="text" id="url-btnCopy6" value="{{ route('public.referral') . '/' . 'd6' . '/' . substr(set_id(auth()->user()->username), 2) . '?lang=' . $idioma }}" hidden="hidden">
                                    <button type="button" id="btnCopy6" class="btn btn-sm btn-primary copy-clipboard">
                                        {{ ___('URL') }}
                                        {{ ___('of this landing page') }}
                                        <em class="ti ti-files"></em>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-1">
                                <div class="d-grid gap-2">
                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#showBanner6">
                                        {{ ___('View') }}
                                        {{ ___('download') }}
                                        {{ ___('banners') }}
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
                <div class="autoplay-progress d-none">
                    <svg viewBox="0 0 48 48">
                      <circle cx="24" cy="24" r="20"></circle>
                    </svg>
                    <span></span>
                </div>
            </div>
            {{-- Landing 2 --}}
            <div id="container-landing2" class="landing d-none bg-danger">
                <p class="text-center text-white pt-5">landing 2</p>
            </div>
            {{-- Landing 3 --}}
            <div id="container-landing3" class="landing d-none bg-success">
                <p class="text-center text-white pt-5">landing 3</p>
            </div>
            {{-- Landing 4 --}}
            <div id="container-landing4" class="landing d-none bg-info">
                <p class="text-center text-white pt-5">landing 4</p>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card ">
                <div class="card-innr">
                    <div class="card-head">
                        <h4 class="card-title card-title-sm">{{ ___('Lista de Referidos') }}</h4>
                    </div>
                    <table class="data-table dt-init refferal-table" data-items="5">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col refferal-name"><span>{{ ___('Nombre de Usuario') }}</span></th>
                                <th class="data-col refferal-date"><span>{{ ___('Fecha de Registro') }}</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($reffered as $refer)
                                <tr class="data-item">
                                    <td class="data-col refferal-name">{{ $refer->name }}</td>
                                    <td class="data-col refferal-date">{{ _date($refer->created_at) }}</td>
                                </tr>
                            @empty
                                <tr class="data-item">
                                    <td class="data-col">{{ ___('¡Nadie se unio aún!') }}</td>
                                    <td class="data-col"></td>
                                    <td class="data-col"></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('footer')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>

        <script>
            $(function() {
                const efectos = ["fxSoftScale", "fxPressAway", "fxFortuneWheel", "fxSwipe",
                    "fxPushReveal", "fxSnapIn", "fxLetMeIn", "fxStickIt", "fxArchiveMe", "fxVGrowth",
                    "fxSlideBehind", "fxSoftPulse", "fxEarthquake", "fxCliffDiving"
                ];

                let efecto = efectos[Math.floor(Math.random() * efectos.length)];
                var $owl = $('.owl-carousel'),

                    effect = efecto,
                    outIndex,
                    isDragged = false;
                if (screen.width < 580) {
                    var navButtons = [];
                    var navCarrusel = false;
                } else {
                    var navCarrusel = true;
                    var navButtons = ["<i class='mdi mdi-chevron-left' style='color: white; font-size: 23px;'></i>",
                        "<i class='mdi mdi-chevron-right' style='color: white; font-size: 23px;'></i>"
                    ];
                }
                $owl.owlCarousel({
                    margin: 0,
                    navSpeed: 500,
                    nav: navCarrusel,
                    items: 1,
                    animateIn: 'fake',
                    animateOut: 'fake',
                    loop: true,
                    margin: 0,
                    navSpeed: 500,
                    autoplayTimeout: 7000,
                    autoplay: true,
                    rewind: true,
                    items: 1,
                    navText: navButtons,
                });

                $owl.on('change.owl.carousel', function(event) {
                    outIndex = event.item.index;
                });

                $owl.on('changed.owl.carousel', function(event) {
                    var inIndex = event.item.index,
                        dir = outIndex <= inIndex ? 'Next' : 'Prev';

                    var animation = {
                        moveIn: {
                            item: $('.owl-item', $owl).eq(inIndex),
                            effect: effect + 'In' + dir
                        },
                        moveOut: {
                            item: $('.owl-item', $owl).eq(outIndex),
                            effect: effect + 'Out' + dir
                        },
                        run: function(type) {
                            var animationEndEvent =
                                'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                                animationObj = this[type],
                                inOut = type == 'moveIn' ? 'in' : 'out',
                                animationClass = 'animated owl-animated-' + inOut + ' ' + animationObj
                                .effect,
                                $nav = $owl.find('.owl-prev, .owl-next, .owl-dot, .owl-stage');

                            $nav.css('pointerEvents', 'none');

                            animationObj.item.stop().addClass(animationClass).one(animationEndEvent,
                                function() {
                                    // remove class at the end of the animations
                                    animationObj.item.removeClass(animationClass);
                                    $nav.css('pointerEvents', 'auto');
                                });
                        }
                    };

                    if (!isDragged) {
                        animation.run('moveOut');
                        animation.run('moveIn');
                    }
                });

                $owl.on('drag.owl.carousel', function(event) {
                    isDragged = true;
                });

                $owl.on('dragged.owl.carousel', function(event) {
                    isDragged = false;
                });

                /**
                 * Get Animation Name from the class 'owl-carousel',
                 * animation name begins with fx...
                 */
                function getAnimationName() {
                    var re = /fx[a-zA-Z0-9\-_]+/i,
                        matches = re.exec($owl.attr('class'));

                    return matches !== null ? matches[0] : matches;
                }


                /**
                 * For Demo (Selectbox)
                 * Change select options with animation name
                 */
                $('#fxselect').on('change', function(e) {
                    var $owlCarousel = $('.owl-carousel'),
                        animationName = getAnimationName();
                    effect = $(this).find('option:selected').val();

                    //remove old root class
                    if (animationName !== null) {
                        $owl.removeClass(animationName);
                    }

                    //add new root class
                    $owlCarousel.addClass(effect);
                });
            });
        </script>
    @endpush

    {{-- script y style para nuevo carrucel con swiper  --}}
    @push('footer')
        <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.container-btn-copy').on('click', '.copy-clipboard', function() {
                    var idBoton = $(this).attr('id');
                    var valorCopiado = $('#url-'+ idBoton).val();                    
                    navigator.clipboard.writeText(valorCopiado)
                    .then(function() {
                       toastr.success("{{___('URL copiada al portapapeles.')}}");
                    })
                    .catch(function(err) {
                        toastr.warning("{{___('No se pudo copiar la URL al portapapeles.')}}");
                    });
                });
            });

            const progressCircle = document.querySelector(".autoplay-progress svg");
            const progressContent = document.querySelector(".autoplay-progress span");
            var swiperLanding = new Swiper(".mySwiper-landing", {
                spaceBetween: 30,
                centeredSlides: true,
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false
                },
                pagination: {
                    el: ".swiper-landing .swiper-pagination",
                    type: "progressbar",
                    clickable: true
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev"
                },
                on: {
                    autoplayTimeLeft(s, time, progress) {
                    progressCircle.style.setProperty("--progress", 1 - progress);
                    progressContent.textContent = `${Math.ceil(time / 1000)}s`;
                    }
                }
            });

            function handleButtonClick(btnId, containerId) {
                $(".btn-opcion-landing").addClass('btn-opcion-inactive').removeClass('btn-opcion-active');
                $(".landing").addClass('d-none');
                $("#"+containerId).removeClass('d-none');
                $("#"+btnId).addClass('btn-opcion-active').removeClass('btn-opcion-inactive');
            }

            $("#btn-landing1").click(function() {
                handleButtonClick("btn-landing1", "container-landing1");
            });

            $("#btn-landing2").click(function() {
                handleButtonClick("btn-landing2", "container-landing2");
            });

            $("#btn-landing3").click(function() {
                handleButtonClick("btn-landing3", "container-landing3");
            });

            $("#btn-landing4").click(function() {
                handleButtonClick("btn-landing4", "container-landing4");
            });

        </script>
    @endpush

    @push('header')
        <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
        <style>
            .btn-opcion-landing{
                width: 100%;
                color: #869791;
            }

            .btn-opcion-landing:hover{
                box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.3) !important;
            }

            .btn-opcion-inactive{
                box-shadow: 0px -2px 10px 0px rgba(0,0,0,0.3);
            }

            .btn-opcion-active{
                box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.3);
            }

            .landing{
                width: 100%;
                min-height: 320px;
            }

            .swiper-landing {
                width: 100%;
                height: 100%;
            }

            .swiper-landing .swiper-slide {
                text-align: center;
                font-size: 18px;
                background: #fff;
                display: flex;
                justify-content: center;
                align-items: center;
                background-color: transparent;
            }

            .swiper-landing .swiper-slide img {
                display: block;
                width: 100%;
                object-fit: cover;
            }

            .autoplay-progress {
                position: absolute;
                right: 16px;
                top: 16px;
                z-index: 10;
                width: 48px;
                height: 48px;
                display: flex;
                align-items: center;
                justify-content: center;
                font-weight: bold;
                color: var(--swiper-theme-color);
            }

            .autoplay-progress svg {
                --progress: 0;
                position: absolute;
                left: 0;
                top: 0px;
                z-index: 10;
                width: 100%;
                height: 100%;
                stroke-width: 4px;
                stroke: var(--swiper-theme-color);
                fill: none;
                stroke-dashoffset: calc(125.6 * (1 - var(--progress)));
                stroke-dasharray: 125.6;
                transform: rotate(-90deg);
            }
        </style>
    @endpush