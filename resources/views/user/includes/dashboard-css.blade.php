<link preload rel="stylesheet" href="{{ asset('assets/css/gosen-main.css') }}">
<style>
    /* .gifloader {
                            width: 50px;
                            height: 50px;
                            margin-left: -25px;
                            border: 1px solid #1cbb8c;
                            border-radius: 50%;
                            position: absolute;
                            top: -29px;
                            left: 50%;
                        } */


    .post-item {
        margin: 10px;
        flex: 0 0 auto;
        font-family: "Roboto", sans-serif;
        font-size: 15px;
    }

    .post-item__inner {
        display: flex;
        flex-direction: column;
        height: 100%;
        border-radius: 4px;
        background-color: #F7F8F8;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.1);
        overflow: hidden;
        transition: box-shadow .2s;
        color: black;
        text-decoration: none;
        border-radius: 25px;
    }

    .post-item__thumbnail-wrapper {
        width: 100%;
        height: 0;
        padding-bottom: 60%;
        flex: 0 0 auto;
        position: relative;
        clip-path: polygon(0 0, 100% 0, 100% 90%, 0 100%);
        overflow: hidden;
        transition: clip-path .2s;
    }

    .post-item__thumbnail {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-size: cover;
        transition: transform .3s;
        left: 0px
    }

    .post-item__thumbnail-wrapper:after {
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 1;
        background-image: linear-gradient(to top, rgb(0 20 236 / 20%), rgba(162, 77, 211, 0.03));
    }

    .post-item__content-wrapper {
        padding: 2rem;
        position: relative;
        height: auto;
        flex: 1 1 auto;
        display: flex;
        flex-direction: column;
    }

    .post-item__title {
        color: white;
        line-height: 1.6;
        margin-top: -4rem;
        font-size: 15px;
        margin-bottom: 1rem;
    }

    .post-item__title span {
        display: inline;
        background-image: linear-gradient(to right, #384d70, #5c79a9);
        padding: .2rem .6rem;
        -webkit-box-decoration-break: clone;
        box-decoration-break: clone;
    }

    .post-item__metas {
        margin-bottom: 2rem;
    }

    .post-item__meta--date {
        color: #6d6d6d;
        letter-spacing: 0.01rem;
        font-size: 1.4rem;
    }

    .post-item__meta--category {
        display: inline-block;
        background-color: #a24dd3;
        color: white;
        font-size: 1.1rem;
        text-transform: uppercase;
        letter-spacing: 0.01rem;
        font-weight: 700;
        padding: 2px;
    }

    .post-item__excerpt {
        margin-bottom: 2rem;
    }

    .post-item__read-more-wrapper {
        margin-top: auto;
    }

    .post-item__read-more {
        padding: 3px 0;
        display: inline;
        background-image: linear-gradient(#a24dd3, #a24dd3);
        background-repeat: no-repeat;
        background-size: 100% 2px;
        background-position: left bottom;
        transition: background-size .3s;
    }

    /* hover state */

    .post-item__inner:hover {
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.3);
    }

    .post-item__inner:hover .post-item__thumbnail-wrapper {
        clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
    }

    .post-item__inner:hover .post-item__thumbnail {
        transform: scale(1.1);
    }

    .post-item__inner:hover .post-item__read-more {
        background-size: 30% 2px;
    }

    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');



    .blobs-container {
        display: flex;
    }

    .blob {
        background: black;
        border-radius: 50%;
        box-shadow: 0 0 0 0 rgba(0, 0, 0, 1);
        margin: 10px;
        height: 20px;
        width: 20px;
        transform: scale(1);
        animation: pulse-black 2s infinite;
    }

    @keyframes pulse-black {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
        }
    }

    .blob.white {
        background: white;
        box-shadow: 0 0 0 0 rgba(255, 255, 255, 1);
        animation: pulse-white 2s infinite;
    }

    @keyframes pulse-white {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 255, 255, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(255, 255, 255, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 255, 255, 0);
        }
    }

    .blob.red {
        background: rgba(255, 82, 82, 1);
        box-shadow: 0 0 0 0 rgba(255, 82, 82, 1);
        animation: pulse-red 2s infinite;
    }

    @keyframes pulse-red {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 82, 82, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(255, 82, 82, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 82, 82, 0);
        }
    }

    .blob.orange {
        background: rgba(255, 121, 63, 1);
        box-shadow: 0 0 0 0 rgba(255, 121, 63, 1);
        animation: pulse-orange 2s infinite;
    }

    @keyframes pulse-orange {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 121, 63, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(255, 121, 63, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 121, 63, 0);
        }
    }

    .blob.yellow {
        background: rgba(255, 177, 66, 1);
        box-shadow: 0 0 0 0 rgba(255, 177, 66, 1);
        animation: pulse-yellow 2s infinite;
    }

    @keyframes pulse-yellow {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 177, 66, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(255, 177, 66, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(255, 177, 66, 0);
        }
    }

    .blob.blue {
        background: rgba(52, 172, 224, 1);
        box-shadow: 0 0 0 0 rgba(52, 172, 224, 1);
        animation: pulse-blue 2s infinite;
    }

    @keyframes pulse-blue {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(52, 172, 224, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(52, 172, 224, 0);
        }

        100% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(52, 172, 224, 0);
        }
    }

    .blob.green {
        background: rgba(51, 217, 178, 1);
        box-shadow: 0 0 0 0 rgba(51, 217, 178, 1);
        animation: pulse-green 2s infinite;
    }

    @keyframes pulse-green {
        0% {
            transform: scale(0.95);
            box-shadow: 0 0 0 0 rgba(51, 217, 178, 0.7);
        }

        70% {
            transform: scale(1);
            box-shadow: 0 0 0 10px rgba(51, 217, 178, 0);
        }

        100% {
            tr
        }
    }
</style>
<style>
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    .wrappe {

        transform: translate(-50%, -50%);
    }

    e {
        display: block;
        left: 130px;
        width: 120px;
        height: 25px;
        padding: 2px;
        text-decoration: none;
        text-align: center;
        border-radius: 3px;

        color: #364969;

        font-size: 15px;
        /* font-family: arial; */
        position: relative;
        overflow: hidden;
        background: transparent;
        text-transform: uppercase;
        transition: all .35s;
    }

    e:before,
    e:after {
        position: absolute;
        content: "";
        width: 100%;
        height: 100%;
        top: -100%;
        left: 0;
        background-image: linear-gradient(to right, #384d70, #5c79a9);
        z-index: -1;
        transition: all .35s;
    }

    e:before {
        opacity: .5;
    }

    e:after {
        transition-delay: .2s;
    }

    e:hover {
        color: #fff;
    }

    e:hover:before,
    e:hover:after {
        top: 0;
    }

    .titulo-dasboard {
        margin: 0px 150px;
    }

    @media (max-width:1410px) {
        .buto {
            width: 100% !important;
        }
    }

    .card-dashboard {
        font-weight: bold;
        font-size: 1.1em;
        text-align: center;
        letter-spacing: 1px;
    }

    @media (min-width: 992px) {
        .card-dashboard {
            font-size: 1.8em !important;
        }
    }

    .lau-left {
        transform: scaleX(-1);
        height: 150px;
        width: 100%;
        margin-right: -69px;
    }

    .lau-right {
        height: 150px;
        width: 100%;
        margin-left: -69px;
    }

    @media only screen and (max-width: 600px) {
        .lau-left {
            transform: scaleX(-1);
            height: 135px;
            width: 123px;
            margin-right: -24px;
        }

        .lau-right {
            height: 135px;
            width: 123px;
            margin-left: -24px;
        }

        .titulo-dasboard {
            margin: 0px 0px;
        }

        .title-chard {
            left: -22px;
        }
    }
</style>





<style>
    @keyframes animate-top {
        0% {
            background: #3e3e3e;
            /* Old browsers */
            background: -moz-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #3e3e3e), color-stop(100%, #2b2b2b));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* IE10+ */
            background: linear-gradient(to bottom, #3e3e3e 0%, #2b2b2b 100%);
            /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3e3e3e', endColorstr='#2b2b2b', GradientType=0);
        }

        100% {
            color: #474747;

            transform:
                translateX(0px) translateY(0.35em) translateZ(5px) rotateX(-90deg) rotateY(0deg) rotateZ(0deg);
        }
    }

    @keyframes animate-bottom {
        0% {
            color: #474747;
            opacity: 1;
        }

        100% {
            transform:
                translateX(0) translateY(0) translateZ(0) rotateX(0deg) rotateY(0deg) rotateZ(0deg);

            opacity: 1;
        }
    }

    @-webkit-keyframes animate-top {
        0% {
            background: #3e3e3e;
            /* Old browsers */
            background: -moz-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #3e3e3e), color-stop(100%, #2b2b2b));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
            /* IE10+ */
            background: linear-gradient(to bottom, #3e3e3e 0%, #2b2b2b 100%);
            /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3e3e3e', endColorstr='#2b2b2b', GradientType=0);
        }

        100% {
            color: #474747;

            -webkit-transform:
                translateX(0px) translateY(0.35em) translateZ(5px) rotateX(-90deg) rotateY(0deg) rotateZ(0deg);
        }
    }

    @-webkit-keyframes animate-bottom {
        0% {
            color: #474747;
            opacity: 1;
        }

        100% {
            -webkit-transform:
                translateX(0) translateY(0) translateZ(0) rotateX(0deg) rotateY(0deg) rotateZ(0deg);

            opacity: 1;
        }
    }

    .number {
        position: relative;
        float: left;
        margin-right: 5px;

        font-size: 5em;

        width: 1em;
        height: 1.5em;
    }

    .number span {
        position: absolute;
        display: block;
        font-weight: bold;
        text-align: center;

        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;

        color: white;

        width: 1em;
        height: 1.5em;
        perspective: 7.14em;

    }

    .number span:before {
        display: block;
        height: 0.75em;
        overflow: hidden;

        line-height: 1.52em;

        border-bottom: 1px solid rgba(0, 0, 0, 0.5);

        border-radius: 20% 20% 0 0;

        background: #3e3e3e;
        /* Old browsers */
        background: -moz-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
        /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #3e3e3e), color-stop(100%, #2b2b2b));
        /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
        /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
        /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #3e3e3e 0%, #2b2b2b 100%);
        /* IE10+ */
        background: linear-gradient(to bottom, #3e3e3e 0%, #2b2b2b 100%);
        /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3e3e3e', endColorstr='#2b2b2b', GradientType=0);
        /* IE6-9 */
    }

    .number span:after {
        display: block;
        height: 0.75em;
        overflow: hidden;
        line-height: 0;

        border-top: 1px solid rgba(0, 0, 0, 0.5);

        border-radius: 0 0 20% 20%;

        transition: color 100ms, background-color 100ms;

        background: #252525;
        /* Old browsers */
        background: -moz-linear-gradient(top, #252525 0%, #070707 100%);
        /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #252525), color-stop(100%, #070707));
        /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #252525 0%, #070707 100%);
        /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #252525 0%, #070707 100%);
        /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #252525 0%, #070707 100%);
        /* IE10+ */
        background: linear-gradient(to bottom, #252525 0%, #070707 100%);
        /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#252525', endColorstr='#070707', GradientType=0);
        /* IE6-9 */
    }


    .flip .primary:before {
        animation: animate-top 250ms;
        -moz-animation-fill-mode: forwards;

        -webkit-animation: animate-top 250ms;
        -webkit-animation-fill-mode: forwards;


        background: #000000;
        /* Old browsers */
        background: -moz-linear-gradient(top, #000000 0%, #303030 100%);
        /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #000000), color-stop(100%, #303030));
        /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #000000 0%, #303030 100%);
        /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #000000 0%, #303030 100%);
        /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #000000 0%, #303030 100%);
        /* IE10+ */
        background: linear-gradient(to bottom, #000000 0%, #303030 100%);
        /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#000000', endColorstr='#303030', GradientType=0);
        /* IE6-9 */

    }

    .flip .primary:after {
        animation: animate-bottom 250ms;
        -moz-animation-fill-mode: forwards;
        animation-delay: 250ms;

        -webkit-animation: animate-bottom 250ms;
        -webkit-animation-fill-mode: forwards;
        -webkit-animation-delay: 250ms;
    }

    .primary:after {
        opacity: 0;

        transform:
            translateX(0) translateY(-0.28em) translateZ(0.35em) rotateX(91deg) rotateY(0deg) rotateZ(0deg);

        -webkit-transform:
            translateX(0) translateY(-0.28em) translateZ(0.35em) rotateX(91deg) rotateY(0deg) rotateZ(0deg);
    }

    .primary {
        z-index: 2;
    }

    .secondary {
        z-index: 1;
    }

    /* Number 0 */

    [data-number="0"] span.primary:before {
        content: "0";
    }

    [data-number="0"] span.secondary:after {
        content: "0";
    }

    [data-number="0"] span.secondary:before {
        content: attr(title);
    }

    [data-number="0"] span.primary:after {
        content: attr(title);
    }

    /* Number 9 */

    [data-number="9"] span.primary:before {
        content: "9";
    }

    [data-number="9"] span.secondary:after {
        content: "9";
    }

    [data-number="9"] span.secondary:before {
        content: attr(title);
    }

    [data-number="9"] span.primary:after {
        content: attr(title);
    }

    /* Number 8 */

    [data-number="8"] span.primary:before {
        content: "8";
    }

    [data-number="8"] span.secondary:after {
        content: "8";
    }

    [data-number="8"] span.secondary:before {
        content: attr(title);
    }

    [data-number="8"] span.primary:after {
        content: attr(title);
    }

    /* Number 7 */

    [data-number="7"] span.primary:before {
        content: "7";
    }

    [data-number="7"] span.secondary:after {
        content: "7";
    }

    [data-number="7"] span.secondary:before {
        content: attr(title);
    }

    [data-number="7"] span.primary:after {
        content: attr(title);
    }

    /* Number 6 */

    [data-number="6"] span.primary:before {
        content: "6";
    }

    [data-number="6"] span.secondary:after {
        content: "6";
    }

    [data-number="6"] span.secondary:before {
        content: attr(title);
    }

    [data-number="6"] span.primary:after {
        content: attr(title);
    }

    /* Number 5 */

    [data-number="5"] span.primary:before {
        content: "5";
    }

    [data-number="5"] span.secondary:after {
        content: "5";
    }

    [data-number="5"] span.secondary:before {
        content: attr(title);
    }

    [data-number="5"] span.primary:after {
        content: attr(title);
    }

    /* Number 4 */

    [data-number="4"] span.primary:before {
        content: "4";
    }

    [data-number="4"] span.secondary:after {
        content: "4";
    }

    [data-number="4"] span.secondary:before {
        content: attr(title);
    }

    [data-number="4"] span.primary:after {
        content: attr(title);
    }

    /* Number 3 */

    [data-number="3"] span.primary:before {
        content: "3";
    }

    [data-number="3"] span.secondary:after {
        content: "3";
    }

    [data-number="3"] span.secondary:before {
        content: attr(title);
    }

    [data-number="3"] span.primary:after {
        content: attr(title);
    }

    /* Number 2 */

    [data-number="2"] span.primary:before {
        content: "2";
    }

    [data-number="2"] span.secondary:after {
        content: "2";
    }

    [data-number="2"] span.secondary:before {
        content: attr(title);
    }

    [data-number="2"] span.primary:after {
        content: attr(title);
    }

    /* Number 1 */

    [data-number="1"] span.primary:before {
        content: "1";
    }

    [data-number="1"] span.secondary:after {
        content: "1";
    }

    [data-number="1"] span.secondary:before {
        content: attr(title);
    }

    [data-number="1"] span.primary:after {
        content: attr(title);
    }

    @media (max-width: 856px) {
        .number {
            font-size: 35px;
        }
    }

    @media (max-width: 450px) {
        .number {
            font-size: 20px;
        }
    }

    .pulse-button {
        
        text-align: center;
       
        cursor: pointer;
        box-shadow: 0 0 0 0 rgba(#5a99d4, .5);
        -webkit-animation: pulse 2.5s infinite;
    }

    .pulse-button:hover {
        -webkit-animation: none;
    }

    @-webkit-keyframes pulse {
        0% {
            transform(scale(.2));
        }

        70% {
            transform(scale(.5));
            box-shadow: 0 0 0 50px rgba(#5a99d4, 0);
        }

        100% {
            transform(scale(.5));
            box-shadow: 0 0 0 0 rgba(#5a99d4, 0);
        }
    }
</style>
