@extends('layouts.auth')
@section('title', ___('Confirm Mail'))
@section('content')
@if ($user->userReset->old_mail==true && $user->userReset->new_mail==false)
    <div class="page-ath-form">
        <h2>{{___('Your email')}} {{$user->email}}  {{___('has been verified successfully!')}}</h2>
        <img loading="lazy" src="{{ asset('assets/images/send-mail-min.gif') }}" alt="" style=" margin-left: auto;
        margin-right: auto;
        display: block;">

        
        <div class="gaps-4x"></div>
        <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
            <p style="font-size: 14px; line-height: 140%;"><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">{{___('Please check the')}} {{$user->userReset->mail}} {{___('address to finish the change of mail.')}}</span></em></span><br /><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
            </p>
        </div>
    </div>
@elseif($user->userReset->old_mail==false && $user->userReset->new_mail==true)
    <div class="page-ath-form">
        <h2>{{___('Your email')}} {{$user->userReset->mail}}  {{___('has been verified successfully!')}}</h2>
        <img loading="lazy" src="{{ asset('assets/images/send-mail-min.gif') }}" alt="" style=" margin-left: auto;
        margin-right: auto;
        display: block;">

        
        <div class="gaps-4x"></div>
        <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
            <p style="font-size: 14px; line-height: 140%;"><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">{{___('Please check the')}} {{$user->email}} {{___('address to finish the change of mail.')}}</span></em></span><br /><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
            </p>
        </div>
    </div>
@elseif($user->userReset->old_mail==true && $user->userReset->new_mail==true)
    <div class="page-ath-form">
        <h3>
            {{___('Your email address was change successfully go back to Login to start session!')}}</h3>
        <img loading="lazy" src="{{ asset('assets/images/send-mail-min.gif') }}" alt="" style=" margin-left: auto;
        margin-right: auto;
        display: block;">

        
        <div class="gaps-4x"></div>
        <a href="{{ route('home') }}" style=" text-align: center; display: inline-block; width: 100%" type="button" class="btn btn-primary align-content-center text-white">{{___('Back to Login')}}</a>
    </div>
@endif
@endsection
