@push('header')
    <style>
        #btnUrlReferralDashboard {
            border-radius: 10px;
            background-color: #ff914c;
            color: #ffffff;
        }

        #btnUrlReferralDashboard:hover {
            border: solid 1px #ff914c;
            background-color: #ff914c;
            color: #ffffff;
        }

        .title-dashboard-1 {
            color: #000000;
        }

        .primer-parrafo-dashboard-1 {
            color: #646464;
        }

        .segundo-parrafo-dashboard-1 {
            color: #000000;
            font-size: 14px;
        }

        .subtitulo-empresas {
            font-size: 12px;
            color: #b2b2b2;
        }

        .img-empresas {
            height: 40px;
            max-height: 40px;
        }

        /* video */
        .adhd-top-video {
            width: 100%;
            height: 100%;
        }

        .adhd-top-video .adhd-top-video2 {
            position: relative;
            margin-top: -22%;
            padding: 0% 0% 7% 0%;
            box-sizing: border-box;
            background: url("{{ asset('images/dashboard/laptop.png') }}") center center no-repeat;
            background-size: contain;
            widows: 100%;
            height: 100;
        }

        .adhd-top-video iframe {
            padding: 0% 11.5% 1.5% 11.5%;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }


        /*estilos de reproductor*/
        .fwdevp div iframe {
            width: 100% !important;
            height: 100% !important;
        }

        .fwdevp div #player1youtube {
            width: 100% !important;
            height: 100% !important;

        }

        .fwdevp div #player2youtube {
            width: 100% !important;
            height: 100% !important;

        }

        .fwdevp div #player4youtube {
            width: 100% !important;
            height: 100% !important;

        }

        .fwdevp div #player5youtube {
            width: 100% !important;
            height: 100% !important;

        }

        #myDiv .fwdevp {
            overflow: hidden !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
        }

        #myDiv2 .fwdevp {
            overflow: hidden !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
        }

        #myDiv4 .fwdevp {
            overflow: hidden !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
        }

        #myDiv5 .fwdevp {
            overflow: hidden !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
        }







        /*para la barra de progreso*/
        /* Mixins */
        /* Color Variables */
        /* Theme Variables */
        /* Animations */
        @-webkit-keyframes bounce {
            0% {
                transform: scale(1);
            }

            33% {
                transform: scale(0.9);
            }

            66% {
                transform: scale(1.1);
            }

            100% {
                transform: scale(1);
            }
        }

        /* Component Styles - Steps */
        .steps {
            display: flex;
            width: 100%;
            margin: 0;
            padding: 0 0 2rem 0;
            list-style: none;
        }

        .step {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            flex: 1;
            position: relative;
            pointer-events: none;
        }

        .step--active,
        .step--complete {
            cursor: pointer;
            pointer-events: all;
        }

        .step:not(:last-child):before,
        .step:not(:last-child):after {
            display: block;
            position: absolute;
            top: 65%;
            left: 50%;
            height: 0.25rem;
            content: "";
            transform: translateY(-50%);
            will-change: width;
            z-index: 0;
            pointer-events: none;
        }

        .step:before {
            width: 100%;
            /* background-color: #0984ff; */
        }

        .step:after {
            width: 0;
            background-color: #c9c9c9;
        }

        .step--complete:after {
            width: 100% !important;
            opacity: 1;
            transition: width 0.6s ease-in-out, opacity 0.6s ease-in-out;
        }

        .step__icon {
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
            width: 3rem;
            height: 3rem;
            background-color: #c9c9c9;
            border: 0.25rem solid #c9c9c9;
            border-radius: 50%;
            color: transparent;
            font-size: 2rem;
            z-index: 1;
        }

        .step__icon:before {
            display: block;
            color: #fff;
        }

        .step--complete.step--active .step__icon {
            color: {{ auth()->user()->theme_color == 'style-black' ? 'black' : '#fff' }};
            transition: background-color 0.3s ease-in-out, border-color 0.3s ease-in-out, color 0.3s ease-in-out;
        }

        .step--incomplete.step--active .step__icon {
            /* border-color: #000000; */

            transition-delay: 0.5s;
        }

        .step--complete .step__icon {
            -webkit-animation: bounce 0.5s ease-in-out;
            animation: bounce 0.5s ease-in-out;
            background-color: #c9c9c9;
            border-color: #c9c9c9;
            color: #fff;
        }

        .step__label {
            position: absolute;
            bottom: -2rem;
            left: 50%;
            margin-top: 1rem;
            transform: translateX(-50%);
            text-align: center;
            line-height: 15px;
        }

        .step--incomplete.step--inactive .step__label {
            color: #e6e7e8;
        }

        .step--incomplete.step--inactive .step__icon {
            background: white;
        }

        .step--incomplete.step--active .step__label {
            color: #010101;
        }

        .step--active .step__label {
            transition: color 0.3s ease-in-out;
            transition-delay: 0.5s;
        }



        .embed-responsive-pdf {
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 100%;
            /* Proporción para PDF de tamaño cuadrado (1:1) */
        }

        .embed-responsive-pdf embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        @media (min-width:720px) {
            #abrir-modal {
                padding-top: 20%;
                padding-bottom: 20%;
            }
        }

        @media (max-width:720px) {
            #abrir-modal {
                padding-top: 14%;
                padding-bottom: 14%;
            }
        }

        @media (min-width:720px) {
            .centroo {
                padding-top: 21%;
                padding-bottom: 22%;
            }
        }

        @media (max-width:720px) {
            .centroo {
                padding-top: 18%;
                padding-bottom: 19%;
            }
        }
    </style>
@endpush
<div class="account-info card card-full-height">
    <div class="card-innr"
        style="background-image: url('{{ asset('gifs/wallpaperhd.gif') }}');background-repeat:no-repeat;background-size: 400px;background: border-box;">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-2 d-flex">
                <div class="card align-items-center justify-content-center"
                    style=" border: none !important; box-shadow: none;">
                    <h1 class="fw-bold title-dashboard-1">{{ ___('“La revolución Disruptiva” ha comenzado.') }}</h1>
                    <h4 class="primer-parrafo-dashboard-1">
                        {{ ___('Modelos y proyectos en finanzas, metaverso, juegos, Web3 está a punto de comenzar y tú puedes ser parte de este movimiento.') }}
                    </h4>
                    <p class="segundo-parrafo-dashboard-1">
                        {{ ___('Un entorno que tiene acceso a la información y acción proporciona oportunidades.') }}
                    </p>
                    {{-- copiar enlace --}}
                    <div class="mt-2">
                        <input type="text" id="urlReferralDashboard"
                            value="{{ route('public.referral') . '?ref=' . substr(set_id(auth()->user()->username), 2) . '&lang=' . auth()->user()->lang }}"
                            hidden="hidden">
                        <button type="button" id="btnUrlReferralDashboard" class="btn">
                            {{ ___('Compartir mi enlace') }} <em class="copy-icon fas fa-link"></em>
                        </button>
                    </div>
                    <p class="subtitulo-empresas mb-0 mt-3 fst-italic">{{ ___('Información respaldada en') }}:</p>
                    <div class="row m-0 text-center">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/1.png') }}" class="img-empresas m-0">
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/3.png') }}" class="img-empresas m-0">
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/5.png') }}" class="img-empresas m-0">
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/2.png') }}" class="img-empresas m-0">
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/4.png') }}" class="img-empresas m-0">
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 m-0">
                            <img loading="lazy" src="{{ asset('images/dashboard/empresas/6.png') }}" class="img-empresas m-0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12 mt-2">
                <div class=" d-flex align-items-center justify-content-center">
                    @php
                        if ($pagina == 2) {
                            $array = [
                                'video1' => [$rewardsUser->video1, ___('Video 1'), '<i class="fa fa-video"></i>', 'active'],
                                'video2' => [$rewardsUser->video2, ___('Video 2'), '<i class="fa fa-video"></i>', 'active'],
                                'modal' => [$rewardsUser->modal, ___('PDF'), '<i class="fa fa-eye"></i>', 'inactive'],
                                'video3' => [$rewardsUser->video3, ___('Video 3'), '<i class="fa fa-video"></i>', 'inactive'],
                                'pdf' => [$rewardsUser->pdf, ___('Video 4'), '<i class="fa fa-video"></i>', 'inactive'],
                            ];
                        } elseif ($pagina == 3) {
                            $array = [
                                'video1' => [$rewardsUser->video1, ___('Video 1'), '<i class="fa fa-video"></i>', 'active'],
                                'video2' => [$rewardsUser->video2, ___('Video 2'), '<i class="fa fa-video"></i>', 'active'],
                                'modal' => [$rewardsUser->modal, ___('PDF'), '<i class="fa fa-eye"></i>', 'active'],
                                'video3' => [$rewardsUser->video3, ___('Video 3'), '<i class="fa fa-video"></i>', 'inactive'],
                                'pdf' => [$rewardsUser->pdf, ___('Video 4'), '<i class="fa fa-video"></i>', 'inactive'],
                            ];
                        } else {
                            $array = [
                                'video1' => [$rewardsUser->video1, ___('Video 1'), '<i class="fa fa-video"></i>', 'active'],
                                'video2' => [$rewardsUser->video2, ___('Video 2'), '<i class="fa fa-video"></i>', 'active'],
                                'modal' => [$rewardsUser->modal, ___('PDF'), '<i class="fa fa-eye"></i>', 'active'],
                                'video3' => [$rewardsUser->video3, ___('Video 3'), '<i class="fa fa-video"></i>', 'active'],
                                'pdf' => [$rewardsUser->pdf, ___('Video 4'), '<i class="fa fa-video"></i>', 'inactive'],
                            ];
                        }
                        $posicionShow = false;
                        $posicion = 0;
                    @endphp
                    <ul class="steps">
                        @php
                        $contador = 1;
                        @endphp
                        @foreach ($array as $nombre => $link)
                            @if ($link[0] == false)
                                <li class="step step--{{ $nombre == 'video1' ? 'complete' : 'incomplete' }} step--{{ $link[3] }}"
                                    id="{{ $contador }}">
                                    <small class="text-muted">
                                        {{ $nombre == 'video1' ? 'ㅤ' : ___('2 Tokens') }}
                                    </small>
                                    <span class="step__icon"> <small>ㅤ</small> </span>
                                    <small class="step__label">{{ __($link[1]) }}</small>
                                </li>
                                @php
                                    $posicionShow = true;
                                    $posicion = $contador;
                                @endphp
                            @elseif($link[0] == true)
                                <li class="step step--complete step--active" id="{{ $contador }}">
                                    <small class="text-muted">
                                        {{ $nombre == 'video1' ? 'ㅤ' : ___('2 Tokens') }}
                                    </small>
                                    <span class="step__icon"> <i class="fa fa-check icon-bar"></i> </span>
                                    <small class="step__label">{{ __($link[1]) }}</small>
                                </li>
                            @endif
                            @php
                            $contador++;
                            @endphp
                        @endforeach
                    </ul>
                </div>
                <input type="hidden" name="" id="pagina" value="{{ $pagina }}">
                <div class="">
                    <div class="adhd-top-video">
                        <div class="adhd-top-video2">
                            <div id="contenido" class="row d-flex align-items-center justify-content-center"
                                style="margin-top: 20%; margin-left: 32%;">
                                <div class="iframe1 " style="    margin-right: 48%; margin-top: 7%; ">
                                    <div id="myDiv" class="" style="position: inherit;">
                                    </div>
                                </div>
                                <div class="iframe2  d-none" style="    margin-right: 48%; margin-top: 7%; ">
                                    <div id="myDiv2" class="" style="position: inherit;">
                                    </div>
                                </div>
                                <div class="iframe3  d-none" style="margin-right: 48%; margin-top: 7%; ">
                                    <div id="myDiv3" class="" style="position: inherit;">
                                        <div class="row">
                                            <div class="col-12 d-flex justify-content-center align-items-center"
                                                style="margin-top:11%;">
                                                <a href="#" class="mx-auto d-block" id="abrir-modal"
                                                    data-toggle="modal" data-target=".bd-example-modal-lg"
                                                    style="">
                                                    <h4 class="btn btn-primary mx-auto d-block">
                                                        {{ ___('Read all text') }}
                                                        <i class="fa fa-newspaper"></i>
                                                    </h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="iframe4  d-none" style="    margin-right: 48%; margin-top: 7%; ">
                                    <div id="myDiv4" class="" style="position: inherit;">
                                        <div class="row">
                                            <div class="col-12 d-flex justify-content-center align-items-center"
                                                style="margin-top:11%;">
                                                <div class="centroo mx-auto d-block">
                                                    <span class="badge bg-success-subtle text-success  font-size-11"><i
                                                            class="mdi mdi-menu-up"> </i>
                                                        {{ ___('Contenido no Disponible') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="iframe5  d-none" style="    margin-right: 48%; margin-top: 7%; ">
                                    <div id="myDiv5" class="" style="position: inherit;">
                                        <div class="row">
                                            <div class="col-12 d-flex justify-content-center align-items-center"
                                                style="margin-top:11%;">
                                                <div class="centroo mx-auto d-block">
                                                    <span class="badge bg-success-subtle text-success  font-size-11"><i
                                                            class="mdi mdi-menu-up"> </i>
                                                        {{ ___('Contenido no Disponible') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="pdfModal" tabindex="-1" aria-labelledby="myLargeModalLabel"
    data-bs-backdrop="static" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" id="button-dismiss" data-dismiss="modal" aria-label="Close"><em
                    class="ti ti-close"></em></a>
            <div class="popup-body">
                <div class="popup-content">
                    <h4 class="modal-title mb-2" id="pdfModalLabel">
                        {{ ___('Lea todo el PDF y acepte los términos para continuar y obtener su recompensa') }}
                    </h4>
                    <div class="">
                        <h1 class="text-center">
                            <u>
                                <b>
                                    {{ env('APP_NAME') }}
                                </b>
                            </u>
                        </h1>
                        <p class="text-justify">
                            {{ ___('Antes de acceder al programa') }} {{ env('APP_NAME') }}
                            {{ ___('deberás obligatoriamente leer y aceptar los términos de uso y condiciones que a continuación establecemos') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Para fines del usuario deberás saber que') }} {{ env('APP_NAME') }}
                            {{ ___('es un programa que refleja una experiencia educativa que explica cómo cualquier persona puede acceder a conocimientos disruptivos y estrategias de apalancamiento en poco tiempo. El programa aloja sus derechos en la empresa denominada') }}
                            GOGO MARCA LTD.,
                            {{ ___('radicada en Inglaterra bajo el número de identificación 93313131') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Tal cual hemos establecido') }} {{ env('APP_NAME') }}
                            {{ ___('no es otra cosa que un sitio web que aloja un programa que comprime un cúmulo de relatos sobre experiencias financieras cuyo propósito principal es enseñar a sus visitantes y/o usuarios a generar riquezas de modo disruptivo en plazos muy breves. Dentro el programa les enseñamos a estudiar, ubicar o identificar proyectos “unicornios” o “estrella” prometedores que por sus condiciones confieran algunas seguridades que permitan invertir salubremente, bajo formas estratégicas e inteligentes') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El acceso o la compra de paquetes colgados al sitio web, solamente confieren el derecho a uso de los mismos') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El o los tokenes que') }} {{ env('APP_NAME') }}
                            {{ ___('pueda entregar a los Visitantes o Usuarios que hayan comprado los paquetes en cuestión solo se harán efectivos en forma de bonificación y/o regalo únicamente a quienes hayan revisado y superado o cumplido las formas establecidas del sitio web para su uso') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Se aclara que los tokenes que') }} {{ env('APP_NAME') }}
                            {{ ___('entregará a sus Visitantes o Usuarios son un regalo y por tanto no constituyen cargas tributarias') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El regalo como tal puede o no implicar un valor en el tiempo, de acuerdo a las tendencias y usos del mismo en el tiempo, no pudiendo por tanto los bonificados generar reclamo alguno en caso los propios no adquieran valor') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Una vez leídos o superados los puntos precedentes, previo a ingresar a ') }}
                            {{ env('APP_NAME') }}
                            {{ ___(' deberás aceptar los términos de uso y política de privacidad de éste sitio bajo las condiciones contractuales que se expondrán dándote el derecho de acceder al sitio,hacer uso del material y servicio, interactuar en el sitio') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Las personas que no acepten los términos y condiciones tendrán denegado el acceso a nuestro portal, sitios y sus enlaces, contenido, bonificaciones u otros') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El ver, visitar, utilizar, usar el contenido de éste lugar o interactuar e interaccionar con cualquier Banner, Pop-Up, publicidad o con alguno de nuestros asesores o servidores, supone de modo tácito que usted acepta todas las disposiciones de nuestras políticas de privacidad y se adhiere a las condiciones contractuales estipuladas en nuestros sitios web') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Queda absolutamente prohibido el ingreso a nuestros sitios a personas incapacitadas legalmente declaradas judicialmente o menores a 18 años, pese ser una web o sitio cibernético destinado a fines lícitos consagrados en el marco de la libre expresión que plasman los Derechos Universales en lo referido al derecho de expresión y puntualmente en la temática de la educación y experiencia financiera del autor a sus seguidores. El sitio se encuentra dando cumplimiento a la ley de Privacidad en Línea para niños ') }}
                            (COPA)
                            {{ ___(' de 1998') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Éste sitio se reserva el derecho de negar el acceso a cualquier persona, visitante o usuario que no cumpla con alguna de las exigencias, requisitos o condiciones estipuladas en éste espacio regulatorio y contractual') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El acuerdo en términos de uso puede ser modificado unilateralmente por los titulares de ') }}
                            {{ env('APP_NAME') }}
                            {{ ___(' en caso las condiciones del proyecto vayan siendo alteradas y para dicho cometido, sus visitantes o usuarios tienen obligación de adaptarse a los mismos, sin posibilidad de reclamación alguna') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            ({{ ___('Términos de Uso y Condiciones') }})
                        </h4>
                        <h4 class="text-center">
                            <b>
                                {{ ___(strtoupper('CAPITULO I – DE LAS PARTES')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('PARTES')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Los visitantes, espectadores, usuarios, suscriptores, miembros, afiliados o clientes, denominados colectivamente en el presente como') }}
                            "{{ ___('Visitantes') }}".
                        </p>
                        <p class="text-justify">
                            {{ ___('El sitio web y sus propietarios, alojadores empresariales, comercializadores y operadores, son partes de este acuerdo, en lo sucesivo denominado') }}
                            "{{ ___('Sitio web') }}".
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('CANCELACIÓN, TERMINACIÓN O EXCLUSIÓN')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('La suscripción al servicio puede cancelarse en cualquier momento y sin causa, ya sea por el Sitio o por el Suscriptor previa notificación a la otra parte mediante correo electrónico, por llamada telefónica, comunicación por whatsapp u otros mecanismos tradicionales. Cuando un miembro solicite la terminación, las tarifas no tendrán reembolso. Los suscriptores son responsables de los cargos incurridos por ellos hasta la terminación del servicio. Los usuarios de tarjetas de crédito pueden estar sujetos a una autorización previa. La pre autorización no es un cargo a la tarjeta de crédito. Sin embargo, el cargo de suscripción aplicable en ese momento puede reservarse contra el límite de la tarjeta de crédito disponible del Miembro. El Sitio no será responsable de los cargos bancarios, tarifas o multas debido a cuentas del Suscriptor en descubierto o morosas') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('POLÍTICA DE REEMBOLSO')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Por el presente se establece taxativamente la imposibilidad de hacer reembolsos a sus visitantes o usuarios. Una vez la persona que accede a nuestros servicios contrata') }},
                            {{ ___(strtoupper('NO HAY REEMBOLSOS')) }}
                            {{ ___('al comprar nuestro programa, en consideración a que el usuario ha consumido el producto y tiene acceso a toda la información, capacitación, las herramientas y la comunidad. Las devoluciones de cargo tampoco están permitidas y, al realizar un pedido, acepta no iniciar ninguna devolución de cargo y, si lo hace, será responsable de los cargos en los que podamos incurrir, incluidos los honorarios de abogados') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Es decir, una vez usted accede a nuestros servicios, le entregamos la capacitación y el contenido según lo prometido, y por tanto, no devolvemos lo abonado. Si usted cree que esto puede no ser adecuado a sus fines, simplemente no acepte ni contrate nuestro servicio') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DEL PROGRAMA Y SU ÉXITO')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Se deja constancia amplia que la capacitación en cuestión es el resultado de la fusión de muchos estudios, conceptos y consejos de algunos expertos en el estudio de las finanzas, pero además, el propio ha sido aplicado y experimentado exitosamente por el') }}
                            CEO,
                            {{ ___('sin embargo, ello no garantiza el éxito en sus usuarios o adquirentes, por tanto, recomendamos a quienes sean escépticos del programa no aceptar, comprar o desarrollar el mismo, en razón, reiteramos a que su eficacia no puede ser garantizada') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            <b>
                                {{ ___(strtoupper('CAPÍTULO II – USO DEL SITIO WEB')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('FORMA Y USO DE INFORMACIÓN DEL SITIO WEB')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('A menos que sea parte del programa y haya celebrado un contrato expreso por escrito con este sitio web; los visitantes, espectadores, suscriptores, miembros, afiliados o clientes sin autorización o permiso no tienen derecho a utilizar esta información en un entorno comercial o público; no tienen derecho a copiar, reproducir, transmitir, guardar, imprimir, vender o publicar parte o el total del contenido de este sitio web. Al ver el contenido de este sitio web, usted acepta esta condición de visualización y reconoce que cualquier uso no autorizado es ilegal sujeto a sanciones civiles o penales. El Visitante no tiene ningún derecho de usar el contenido o partes del mismo, incluidas sus bases de datos, páginas invisibles, páginas vinculadas, código subyacente o cualquier otra propiedad intelectual que pueda contener el sitio, por cualquier motivo y para cualquier uso. El visitante acepta daños liquidados por la cantidad de') }}
                            U.S.$ 50.000
                            {{ ___('además de las costas y perjuicios reales por incumplimiento de esta disposición. El visitante garantiza que entiende que aceptar esta disposición es una condición para verlo y que verlo constituye una aceptación') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('PROPIEDAD DEL SITIO WEB; DERECHO A USO, VENTAS Y DERECHOS DE PUBLICACIONES')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El sitio web y sus contenidos son propiedad o están autorizados por los titulares del espacio o sitio. Se debe tener por sentado que el contenido en el sitio web es de titularidad del programa') }}
                            {{ env('APP_NAME') }}
                            {{ ___('y tiene derechos de autor debidamente alojados y registrados en') }} GOGO MARCA
                            LTD.
                            {{ ___('Los visitantes y sus usuarios no tienen ningún derecho al contenido del sitio. El uso del contenido del sitio web por cualquier motivo es ilegal a menos que el titular confiera derechos a través de contratos') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Quedan restringidos y prohibidos todos los enlaces o hipervínculos que se traten de casar o ligar a dicho sitio web, su marca, el') }}
                            “framing”, {{ ___('la empresa alojadora de') }} {{ env('APP_NAME') }}
                            {{ ___('o lugares de referencia, a excepción de los autorizados expresamente por terceros contratados de la empresa a cargo del programa (en tanto y por lo demás) nadie puede vincular este sitio o partes del mismo (incluidos, entre otros, logotipos, marcas comerciales, marcas o material protegido por derechos de autor). Además, no se le permite hacer referencia a la URL (dirección del sitio web) de este sitio web en ningún medio comercial o no comercial sin permiso expreso, ni se le permite') }}
                            "{{ ___('enmarcar ni usar') }}"
                            {{ ___('el sitio. Usted acepta específicamente cooperar con el sitio web para eliminar o desactivar dichas actividades y ser responsable de todos los daños. Por la presente, acepta daños liquidados de') }}
                            US $ 50,000.00
                            {{ ___('más costos y daños reales por violar esta disposición, los cuales se hacen cobrables y ejecutables ante cualquier jurisdicción del mundo entero') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DESLINDAMIENTO DE RESPONSABILIDAD POR EL CONTENIDO DEL SITIO')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El sitio web, el Autor de') }} {{ env('APP_NAME') }}
                            {{ ___('y las empresas alojadoras del proyecto se deslindan, eximen y excluyen de todo tipo de responsabilidad por la exactitud del contenido de este sitio web. Los visitantes asumen todos los riesgos de ver, leer, usar o confiar en la información. El sitio web no garantiza que su contenido se haga efectivo en cuanto sus alcances e información') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DESLINDE Y DESCARGO RESPECTO POSIBLES DAÑOS CAUSADOS A SU COMPUTADORA O SOFTWARE AL INTERACCIONAR CON ESTE SITIO WEB O SUS CONTENIDOS')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El sitio web, sus titulares y/o la empresa alojadora no asume ningún tipo de responsabilidad ante posibles daños a las computadoras o el software del visitante o de cualquier persona con la que el visitante y/o usuario se comunique posteriormente debido a la deformación, distorsión, corrupción o mal manejo del código o los datos que se transmiten inadvertidamente a la computadora del visitante. Por su parte, el visitante que interactúa con este sitio, con banners, o que utiliza las ventanas emergentes o la publicidad que se muestra en él, asume todo tipo de riesgos y responsabilidades de su computadora en consideración a que el ingreso al sitio es de su mera voluntad') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('LIMITACIÓN DE RESPONSABILIDAD')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Al ver, usar o interactuar de cualquier manera con este sitio, incluidos banners, publicidad o ventanas emergentes, descargas y como condición del sitio web para permitir su visualización legal, el Visitante renuncia para siempre a todo derecho a reclamos por daños de cualquier y toda descripción basada en cualquier factor causal que resulte en cualquier posible daño, sin importar cuán atroz o extenso sea, ya sea físico o emocional, previsible o imprevisible, ya sea de naturaleza personal o comercial') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('TÉRMINOS DE INDEMNIZACIÓN')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El visitante acepta que, en caso de que cause daños, que el sitio web debe pagar, el visitante, como condición de visualización, promete reembolsar al sitio web por todos') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            <b>
                                {{ ___(strtoupper('CAPITULO III- EN CUANTO CONTENIDO DEL PROGRAMA Y USO')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DE LAS PRESENTACIONES, CONFERENCIAS Y DEL MATERIAL')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El Visitante acepta como condición de visualización, que cualquier comunicación entre el Visitante y el sitio web se considera una presentación. Todos los envíos, incluidas partes de los mismos, los gráficos contenidos en los mismos, o cualquier parte del contenido del envío, se convertirán en propiedad exclusiva del sitio web y pueden usarse, sin permiso adicional, para uso comercial sin consideración adicional de ningún tipo. El visitante acepta comunicar solo esa información al sitio web, que desea permitir para siempre que el sitio web lo use de la manera que considere adecuada.') }}
                            "{{ ___('Envíos') }}"
                            {{ ___('también es una disposición de la Política de privacidad') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('AVISOS SIN NOTIFICACIÓN')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Los actos, las modificaciones, las alteraciones o problemas emergentes en el sitio no necesariamente serán notificados al Visitante, quien deberá visitar el sitio para obtener información y por tanto el visitante o usuario garantiza expresamente la comprensión de su renuncia a la misma') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DERECHOS DE AUTOR')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El contenido del sitio, su diseño, gráficos, organización, la compilación de datos, la traducción, el cuadro de diálogos y todos los asuntos relacionados en la web, su espacio y su estructura, están protegidos por derechos de autor, marcas comerciales y otros derechos de propiedad (incluidos, entre otros, los de propiedad intelectual). La copia, redistribución, uso o publicación de tales asuntos o cualquier parte del Sitio están estrictamente prohibidos a sus suscriptores y/o terceros. Los visitantes y usuarios del sitio no adquieren derechos de propiedad sobre ningún contenido, documento u otros materiales vistos a través del sitio web. La publicación de información o materiales en el Sitio no vincula sub derechos de marcas o derechos de autor a sus receptores') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('LICENCIA LIMITADA; USOS PERMITIDOS')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('A los visitantes, clientes o usuarios se les otorga una licencia intransferible, revocable; para acceder y utilizar el Sitio estrictamente de acuerdo con este Acuerdo, a fines éstos puedan: (1) utilizar el Sitio con fines internos personales de capacitación y no comerciales; y (2) para imprimir información discreta del Sitio únicamente para fines internos personales de capacitación y no comerciales y siempre que se mantengan y no se alteren todos los derechos de autor y otras políticas. Los clientes, usuarios y visitantes no pueden por ningún motivo utilizar la versión impresa o electrónica del Sitio o su contenido en ningún litigio o arbitraje bajo ninguna circunstancia') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('RESTRICCIONES Y PROHIBICIONES DE USO')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('La licencia para el acceso al uso del Sitio y cualquier información, materiales y todos los documentos del sitio están sujetos a las siguientes restricciones y prohibiciones de uso: Los visitantes no pueden (1) copiar, imprimir, volver a publicar, exhibir, distribuir, transmitir, vender, alquilar, arrendar, prestar o poner a disposición de cualquier forma o por cualquier medio todo o parte del Sitio o cualquier Contenido y Materiales recuperados de éste; (2) usar el Sitio o cualquier material obtenido del Sitio para desarrollar cualquier sistema de información, almacenamiento y recuperación de base de datos, base de información o recurso similar (en cualquier medio existente ahora o desarrollado en el futuro), que se ofrece para distribución comercial de cualquier tipo, incluso a través de venta, licencia, arrendamiento, alquiler, suscripción o cualquier otro mecanismo de distribución comercial; (3) crear compilaciones o trabajos derivados de cualquier Contenido y Materiales del Sitio; (4) usar cualquier Contenido y Materiales del Sitio de cualquier manera que pueda infringir cualquier derecho de autor, derecho de propiedad intelectual, derecho de propiedad o derecho de propiedad de nosotros o de terceros; (5) eliminar, cambiar u ocultar cualquier aviso de derechos de autor u otro aviso de propiedad o términos de uso contenidos en el Sitio; (6) poner a disposición cualquier parte del Sitio a través de cualquier sistema de tiempo compartido, oficina de servicios, Internet o cualquier otra tecnología existente ahora o desarrollada en el futuro; (7) eliminar, descompilar, desensamblar o aplicar ingeniería inversa a cualquier software del Sitio o usar cualquier software de detección o monitoreo de red para determinar la arquitectura del Sitio; (8) usar el Sitio de una manera que viole cualquier ley de las jurisdicciones del mundo entero; y (9) exportar o reexportar el Sitio o cualquier parte del mismo, o cualquier software disponible en a través del Sitio') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            <b>
                                {{ ___(strtoupper('CAPÍTULO IV – DESCARGOS AL PROYECTO Y AUTORES')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DESCARGO DE RESPONSABILIDAD')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('El material proporcionado se ofrece como referencia, sin garantía alguna de que con él se tenga éxito financiero. El lector, cliente, usuario, visitante o contratante asume todos los riesgos derivados del uso de información contenida en este documento. Por tanto, el Sitio, la empresa alojadora y comercializadora, el Autor, se deslinda de toda responsabilidad por la información proporcionada') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('ACUERDO DE AFILIADOS')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Ninguna publicidad de Afiliados tendrá contenido engañoso, falso, sin fundamento o que incumpla con las leyes, regulaciones y pautas de protección al consumidor dentro la jurisdicción en la que la empresa alojadora del proyecto opera') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('POLÍTICA ANTISPAM')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('En el contexto de la mensajería electrónica de tipo spam, se refiere a mensajes no solicitados, masivos o indiscriminados, generalmente enviados con fines comerciales. Por tanto y para fines, la empresa alojadora del programa') }}
                            {{ env('APP_NAME') }}
                            {{ ___('proporciona un servicio que permite a los usuarios enviar mensajes de correo electrónico/mensajes privados a otros. Los usuarios no deben utilizar esta función para enviar mensajes no solicitados, masivos o indiscriminados, ya sea con fines comerciales o no') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('La recepción de mensajes no deseados de nuestra empresa: en el improbable caso de que reciba algún mensaje nuestro o enviado a través de nuestros sistemas que pueda considerarse spam, comuníquese con nosotros utilizando los detalles a continuación y se investigará el asunto') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Se establece que todos los clientes, usuarios, afiliados y visitantes del proyecto') }}
                            {{ env('APP_NAME') }}
                            {{ ___('deben cumplir con todas las regulaciones de leyes y prohibiciones de mal empleo de publicidad que se establecen en la jurisdicción en la que emprende') }}
                            {{ env('APP_NAME') }} {{ ___('y ente regulador') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El afiliado acepta toda responsabilidad a') }} GOGO MARCA LTD.
                            {{ ___('de cualquier demanda, investigación, reclamo o queja que surja de cualquier violación o supuesta violación de los términos anteriores') }}.
                            GOGO MARCA LTD.
                            {{ ___('no será responsable de aprobar ningún anuncio de afiliado. El cumplimiento es únicamente con el Afiliado y el Afiliado declara y garantiza que tendrá una revisión legal de todos los Anuncios de Afiliados para el cumplimiento necesario y requerido. Los afiliados asumen toda la responsabilidad de su publicidad') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('DESCARGO DE RESPONSABILIDAD SOBRE GANANCIAS DEL CLIENTE, USUARIO O COMPRADOR DEL PROGRAMA')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Con éste punto se deja ampliamente establecido, aclarado y se enfatiza que todos los elementos de capacitación del programa o proyecto') }}
                            {{ env('APP_NAME') }},
                            {{ ___('compuesto por videos, PDF informativos, y documentos en general, son aspectos inherentes a las vivencias financieras de su autor, siendo las propias simples opiniones y experiencias personales y por ello es que pedimos a todos sus usuarios, clientes, visitantes o afiliados que jamás tomen los criterios experimentales como una opinión definitiva para asumir decisiones financieras. Antes de decidir sobre asuntos financieros tenga en cuenta que existen diversidad de estrategias financieras para cada modelo o tipo de inversión y según las coyunturas y acorde las necesidades individuales y patrimonio de casa ser. Nuestros videos y capacitación probablemente no puedan ser adaptados a sus necesidades') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Todo usuario, cliente, afiliado, visitante del sitio e incluso las entidades reguladoras que ingresen a éste sitio deberán tomar en cuenta que, cualquier declaración de ingresos o ejemplo de ingresos del sitio, han sido obtenidos únicamente mediante el método de observación y no han superado el método científico para su uso e implementación, por tanto, la réplica de su implementación y el uso de las instrucciones no garantiza bajo ninguna circunstancia el resultado que se establece en los videos, textos o documentos incorporados al sitio y programa, por tanto, su uso implica riesgo al consumidor, el cual queda apartado y ajeno al programa. Es decir, todo individuo que aplique lo expuesto en el programa asume sus propios riesgos, no pudiendo responsabilizar al programa o su autor por el mismo') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Cualquiera y todas las reclamaciones o declaraciones, en cuanto a las ganancias de ingresos en este Sitio Web, no deben considerarse como ganancias promedio') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Consideramos que ningún programa ni éste puede garantizar el éxito de resultados en cuanto a su aplicación') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Si Usted no está dispuesto a invertir tiempo, a invertir dinero sin riesgos, le recomendamos no acceda al presente servicio') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            <b>
                                {{ ___(strtoupper('CAPITULO VI – TÉRMINOS CONTRACTUALES')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('LA COMPRA DE ESTE PRODUCTO INFIERE AUTOMÁTICAMENTE LA ACEPTACIÓN EXPRESA DE SUS ADQUIRIENTES, USUARIOS, O CLIENTES, QUIENES SE SOMETEN A LOS TÉRMINOS DEL ACUERDO EN CUESTIÓN')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Para fines, se establece que toda persona natural o jurídica, robot, o adquirente de derechos del servicio brindado por este sitio web o todo comprador de productos ofertados dentro el programa') }}
                            {{ env('APP_NAME') }} {{ ___('alojado por') }} GOGO MARCA LTD
                            {{ ___('de generales ya establecidas –ut supra-, se somete y acepta expresamente los términos y acuerdos de este programa y lo inscrito en el presente documento') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('CONTROVERSIAS ENTRE PARTES')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('En caso de controversia en mérito al contrato, agravio insurgente a consecuencia de la relación generada por la compra del producto, incluidos problemas de solicitud, problemas de privacidad y problemas de términos de uso., el Visitante, Usuario o Cliente renuncia al litigio civil, comercial y penal, sometiéndose única y exclusivamente al arbitraje comercial dentro la jurisdicción donde opera el programa') }}
                            {{ env('APP_NAME') }}, {{ ___('alojado por la empresa') }} GOGO MARCA LTD
                            {{ ___('de generales y datos cursantes ut supra') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Si algún asunto relacionado con esta compra se lleva ante un tribunal de justicia, antes o después del arbitraje, el espectador, visitante, miembro, suscriptor o cliente acepta que la jurisdicción única y adecuada será el estado y la ciudad declarados en la información de contacto. del titular de la web salvo que aquí se especifique lo contrario. En caso de que el litigio sea en un tribunal federal, el tribunal apropiado será el tribunal federal más cercano a la dirección del vendedor') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('JURISDICCIÓN Y LEY APLICABLE')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('En caso concreto todo espectador, visitante, miembro, suscriptor, usuario o cliente acepta y se somete a la jurisdicción de la dirección del Vendedor') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Por tanto, la ley aplicable para el caso concreto ante cualquier controversia y arbitraje será la del vendedor') }}.
                        </p>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('ACUERDO DE NO DIVULGACIÓN DE INFORMACIÓN DE UNA PARTE A OTRA')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            1. {{ ___('Se tendrá a') }} {{ env('APP_NAME') }}
                            {{ ___('como ente informativo, el cual tendrá facultades plenas de transferir información del receptor con fines netamente comerciales y empresariales, que tendrán por objeto, dotar de material al receptor y almacenar sus datos para darle mejor servicio') }}.
                        </p>
                        <p class="text-justify">
                            2.
                            {{ ___('Se tendrá por parte al receptor (Usted), quien además tendrá condición de Visitante, Usuario, comprador del proyecto y los productos ofertados en el sitio web, quienes estarán limitados de divulgar toda información proveniente del proyecto o la página en cuestión') }}.
                        </p>
                        <h5 class="text-left">
                            {{ ___(strtoupper('CONSIDERACIONES')) }}:
                        </h5>
                        <p class="text-justify">
                            {{ ___('La Parte receptora o cliente entiende que') }} {{ env('APP_NAME') }}
                            {{ ___('ha divulgado o puede divulgar información relacionada con el código fuente, los diseños de productos, las artes y todos los conceptos vinculados o relacionados al producto o proyecto, y por ende acepta que su información sea utilizada con fines comerciales, desde luego sin provocar daños a su privacidad, personalidad y/o datos personales') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('La divulgación de Información Privilegiada por parte del programa') }}
                            {{ env('APP_NAME') }},
                            {{ ___('respecto la Parte Receptora acuerda: (a) preservar la Información Privilegiada en absoluta confidencialidad y tomar todas las precauciones para proteger dicha Información Privilegiada (incluyendo, sin limitación, todos precauciones que la Parte receptora emplea con respecto a sus propios materiales confidenciales), (b) no divulgar dicha Información de propiedad exclusiva o cualquier información derivada de la misma a terceros interesados, (c) no hacer ningún uso en ningún momento de dicha Información de propiedad exclusiva excepto para evaluar internamente su relación con la Parte divulgadora, y (d) no copiar ni aplicar ingeniería inversa a dicha Información de propiedad exclusiva') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('La Parte Receptora entiende que nada en el presente requiere la divulgación de cualquier Información Privada o requiere que la Parte Reveladora proceda con cualquier transacción o relación') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('La Parte Receptora además reconoce y acepta que no se hace ni se hará ninguna representación o garantía, expresa o implícita, y no se acepta ni se aceptará ninguna responsabilidad por parte de la Parte Reveladora, o por cualquiera de sus respectivos directores, funcionarios, empleados, agentes o asesores, en cuanto a, o en relación con, la exactitud o integridad de cualquier Información de propiedad exclusiva puesta a disposición de la Parte receptora o sus asesores; es responsable de hacer su propia evaluación de dicha información de propiedad exclusiva') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('El hecho de que cualquiera de las partes no haga valer sus derechos bajo este Acuerdo en cualquier momento por cualquier período no se interpretará como una renuncia a tales derechos. Si alguna parte, término o disposición de este Acuerdo se considera ilegal o inaplicable, no se verá afectada la validez ni la aplicabilidad del resto de este Acuerdo. Ninguna de las Partes podrá ceder o transferir la totalidad o parte de sus derechos bajo este Acuerdo sin el consentimiento de la otra Parte. Este Acuerdo no puede modificarse por ningún otro motivo sin el acuerdo previo por escrito de ambas Partes. Este Acuerdo constituye el entendimiento completo entre las Partes en relación con el objeto del mismo, a menos que cualquier representación o garantía hecha sobre este Acuerdo se haya hecho de manera fraudulenta y, salvo que se haga referencia o referencia expresa en este documento') }}.
                        </p>
                        <p class="text-justify">
                            {{ ___('Este Acuerdo se regirá por las leyes de la jurisdicción en la que se encuentra la Parte divulgadora (o si la Parte divulgadora tiene su sede en más de un país, el país en el que se encuentra su sede) (el Territorio) y las partes aceptan someter las disputas que surjan de o en relación con este Acuerdo a los tribunales no exclusivos del Territorio') }}.
                        </p>



                        <h4 class="text-center pt-2">
                            <b>
                                {{ ___(strtoupper('CAPÍTULO VIII- CONTACTO')) }}
                            </b>
                        </h4>
                        <h5 class="text-left">
                            <u>
                                <b>
                                    {{ ___(strtoupper('INFORMACIÓN DEL CONTACTO')) }}:
                                </b>
                            </u>
                        </h5>
                        <p class="text-justify">
                            {{ ___('Correo electrónico de contacto') }}:
                        </p>
                        <p class="text-justify">
                            {{ ___('Atención al cliente') }}:
                        </p>
                        <p class="text-justify">
                            {{ ___('Dirección del Ente Comercializador') }}:
                        </p>
                    </div>
                    <div class="row mt-4">
                        <div
                            class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-center justify-content-center">
                            <input type="checkbox" class="" id="checkTerminos">
                            <label class="form-check-label ml-2"
                                for="checkTerminos">{{ ___('He leido los terminos y condiciones') }}
                            </label>
                        </div>
                        <div
                            class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-center justify-content-center">
                            <a href="" class="btn btn-success disabled"
                                id="aceptar">{{ ___('Aceptar y Descargar') }}<i
                                    class="ml-2 fa fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('footer')
    <script>
        $("#btnUrlReferralDashboard").click(function() {
            var valorCopiado = $("#urlReferralDashboard").val();
            navigator.clipboard.writeText(valorCopiado)
                .then(function() {
                    toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                })
                .catch(function(err) {
                    toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                });
        });
    </script>
    <script>
        const pdfUrl = "{{ asset('pdf/prueba.pdf') }}";
        const downloadButton = document.getElementById("aceptar");
        downloadButton.addEventListener("click", function() {
            var link = document.createElement("a");
            link.href = pdfUrl;
            link.download = "documento.pdf";
            link.click();
        });
        document.addEventListener("DOMContentLoaded", function() {
            var checkbox = document.getElementById("checkTerminos");
            checkbox.checked = false;
            var aceptarBtn = document.getElementById("aceptar");
            checkbox.addEventListener("change", function() {
                if (checkbox.checked) {
                    aceptarBtn.classList.remove("disabled");
                    aceptarBtn.removeAttribute("disabled");
                    aceptarBtn.href = "{{ route('user.reward.modal') }}" + '?checkbox=' + checkbox
                        .checked;
                } else {
                    aceptarBtn.classList.add("disabled");
                    aceptarBtn.setAttribute("disabled", "disabled");
                    aceptarBtn.href = "{{ route('user.reward.modal') }}" + '?checkbox=' + checkbox
                        .checked;
                }
            });
        });
    </script>
    <script>
        $(document).on('keydown', function(event) {
            if (event.key === 'ArrowRight' || event.key === 'ArrowLeft') {
                event.preventDefault();
            }
        });
    </script>
@endpush
@include('user.includes.bonus-tokens-script')
