@section('gosencss')
    <link preload rel="stylesheet" type="text/css" href="{{ asset('videoplayer/global.css') }}">
@endsection
@push('footer')
    <script type="text/javascript" src="{{ asset('videoplayer/FWDEVPlayer.js') }}"></script>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            // Obtener el primer elemento con la clase "step--complete"
            var firstCompleteStep = document.querySelector('.step--complete');
            // Obtener el elemento "step__icon" dentro del primer elemento "step--complete"
            var iconElement = firstCompleteStep.querySelector('.step__icon');
            // Cambiar el estilo del elemento "step__icon"
            if (iconElement) {
                iconElement.style.backgroundColor =
                    '{{ auth()->user()->theme_color == 'style-black' ? '#ddd' : asset(theme_color_user(auth()->user()->theme_color, 'base')) }}';
                iconElement.style.borderColor =
                    '{{ auth()->user()->theme_color == 'style-black' ? '#ddd' : asset(theme_color_user(auth()->user()->theme_color, 'base')) }}';
            }
            var pagina = {{ $pagina }};
            // console.log(pagina);
            // Obtén la lista <ul> por su clase
            const ulElement = document.querySelector('.steps');
            // Encuentra todos los elementos <li> dentro del <ul> que tienen las clases "step--incomplete" y "step--active"
            const incompleteActiveItems = ulElement.querySelectorAll('li.step--complete.step--active');
            // Verifica si hay elementos encontrados y obtén el último si existen
            if (incompleteActiveItems.length > 0 && incompleteActiveItems.length < pagina + 1) {
                const ultimoElemento = incompleteActiveItems[incompleteActiveItems.length - 1];
                // Verifica si el último elemento tiene un <span> con la clase "step__icon"
                const spanElement = ultimoElemento.querySelector('span.step__icon');
                if (spanElement && spanElement.querySelector('i.fa.icon-bar')) {
                    // Encuentra el siguiente elemento <li>
                    const siguienteElemento = ultimoElemento.nextElementSibling;
                    // Si existe el siguiente elemento y tiene la clase "step--inactive"
                    if (siguienteElemento && siguienteElemento.classList.contains('step--inactive')) {
                        siguienteElemento.classList.remove('step--inactive');
                        siguienteElemento.classList.add('step--active');
                    } else {
                        console.log('No se encontró un siguiente elemento con la clase "step--inactive".');
                    }
                } else {
                    console.log(
                        'El último elemento no tiene un <span> con la clase "step__icon" o no contiene un ícono.'
                    );
                }
            } else {
                console.log('No se encontraron elementos con las clases "step--incomplete" y "step--active".');
            }
            document.querySelectorAll('.step--active, .step--incomplete').forEach(function(element) {
                element.addEventListener('click', function(event) {
                    var clickedStep = event.target.parentElement.parentElement;
                    // Obtén una lista de todos los elementos con la clase "step__icon"
                    var iconList = document.querySelectorAll(".step__icon");
                    // Elimina el estilo existente de todos los elementos
                    iconList.forEach(function(icon) {
                        if (icon.closest(".step.step--active.step--complete")) {
                            icon.style.boxShadow = "none";
                            icon.style.backgroundColor = "#c9c9c9";
                            icon.style.borderColor = "#c9c9c9";
                        }
                    });
                    // Agrega el estilo al elemento correspondiente
                    var icon = clickedStep.querySelector('.step__icon');
                    icon.style.backgroundColor =
                        "{{ auth()->user()->theme_color == 'style-black' ? '#ddd' : asset(theme_color_user(auth()->user()->theme_color, 'base')) }}";
                    icon.style.borderColor =
                        "{{ auth()->user()->theme_color == 'style-black' ? '#ddd' : asset(theme_color_user(auth()->user()->theme_color, 'base')) }}";
                    if (clickedStep.classList.contains('step--active') && clickedStep.classList
                        .contains('step--incomplete')) {
                        switch (clickedStep.id) {
                            case '1':
                                document.querySelector('.iframe1').classList.remove('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '2':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.remove('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '3':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.remove('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '4':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.remove('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '5':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.remove('d-none');
                                break;
                        }
                        clickedStep.classList.remove('step--incomplete');
                        clickedStep.classList.add('step--complete');
                    } else if (clickedStep.classList.contains('step--active') && clickedStep
                        .classList.contains('step--complete')) {
                        switch (clickedStep.id) {
                            case '1':
                                document.querySelector('.iframe1').classList.remove('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '2':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.remove('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '3':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.remove('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '4':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.remove('d-none');
                                document.querySelector('.iframe5').classList.add('d-none');
                                break;
                            case '5':
                                document.querySelector('.iframe1').classList.add('d-none');
                                document.querySelector('.iframe2').classList.add('d-none');
                                document.querySelector('.iframe3').classList.add('d-none');
                                document.querySelector('.iframe4').classList.add('d-none');
                                document.querySelector('.iframe5').classList.remove('d-none');
                                break;
                        }
                    } else {
                        console.log('video');
                    }
                });
            });
        });
        var video;
        var videoInstances = {};
        function initializeVideoPlayer(instanceName, parentId, videoSourcePath, route) {
            FWDEVPlayer.videoStartBehaviour = "pause";
            if (videoInstances[instanceName]) {
                video = videoInstances[instanceName];
            } else {
                video = new FWDEVPlayer({
                    //main settings
                    instanceName: instanceName,
                    parentId: parentId,
                    mainFolderPath: "content",
                    skinPath: "minimal_skin_dark",
                    initializeOnlyWhenVisible: "yes",
                    displayType: "responsive",
                    autoScale: "yes",
                    fillEntireVideoScreen: "yes",
                    playsinline: "yes",
                    useWithoutVideoScreen: "no",
                    openDownloadLinkOnMobile: "no",
                    useVectorIcons: "no",
                    useResumeOnPlay: "no",
                    goFullScreenOnButtonPlay: "yes",
                    useHEXColorsForSkin: "no",
                    normalHEXButtonsColor: "#FF0000",
                    privateVideoPassword: "428c841430ea18a70f7b06525d4b748a",
                    startAtTime: "",
                    stopAtTime: "",
                    startAtVideoSource: 1,
                    videoSource: [{
                        source: videoSourcePath,
                        label: "small version"
                    }],
                    posterPath: "",
                    googleAnalyticsTrackingCode: "",
                    showErrorInfo: "no",
                    fillEntireScreenWithPoster: "yes",
                    disableDoubleClickFullscreen: "yes",
                    useChromeless: "no",
                    showPreloader: "no",
                    preloaderColors: ["#999999", "#FFFFFF"],
                    addKeyboardSupport: "no",
                    autoPlay: "no",
                    autoPlayText: "Click to Unmute",
                    loop: "no",
                    scrubAtTimeAtFirstPlay: "00:00:00",
                    volume: .8,
                    greenScreenTolerance: 200,
                    backgroundColor: "#000000",
                    posterBackgroundColor: "#000000",
                    //lightbox settings
                    closeLightBoxWhenPlayComplete: "no",
                    lightBoxBackgroundOpacity: .6,
                    lightBoxBackgroundColor: "#000000",
                    //logo settings
                    showLogo: "no",
                    hideLogoWithController: "yes",
                    logoPosition: "topRight",
                    logoLink: "https://w7.pngwing.com/pngs/38/732/png-transparent-msi-logo-horizontal.png",
                    logoMargins: 5,
                    logoText: "Disruptive",
                    //controller settings
                    showController: "no",
                    showDefaultControllerForVimeo: "yes",
                    showDefaultControllerForYoutube: "yes",
                    showScrubberWhenControllerIsHidden: "no",
                    showControllerWhenVideoIsStopped: "no",
                    showVolumeScrubber: "no",
                    showVolumeButton: "no",
                    showTime: "yes",
                    showRewindButton: "no",
                    showQualityButton: "no",
                    showShareButton: "no",
                    showEmbedButton: "no",
                    showDownloadButton: "no",
                    showMainScrubberToolTipLabel: "no",
                    showChromecastButton: "no",
                    showFullScreenButton: "no",
                    repeatBackground: "no",
                    controllerHeight: 41,
                    controllerHideDelay: 3,
                    startSpaceBetweenButtons: 7,
                    spaceBetweenButtons: 9,
                    mainScrubberOffestTop: 14,
                    scrubbersOffsetWidth: 4,
                    timeOffsetLeftWidth: 5,
                    timeOffsetRightWidth: 3,
                    volumeScrubberWidth: 80,
                    volumeScrubberOffsetRightWidth: 0,
                    timeColor: "#777777",
                    youtubeQualityButtonNormalColor: "#777777",
                    youtubeQualityButtonSelectedColor: "#FFFFFF",
                    scrubbersToolTipLabelBackgroundColor: "#FFFFFF",
                    scrubbersToolTipLabelFontColor: "#5a5a5a",

                    //redirect at video end
                    redirectURL: route,
                    //cuepoints
                    executeCuepointsOnlyOnce: "no",
                    cuepoints: [],
                    //annotations
                    annotiationsListId: "none",
                    showAnnotationsPositionTool: "no",
                    //subtitles
                    showSubtitleButton: "no",
                    subtitlesOffLabel: "Subtitle off",
                    startAtSubtitle: 1,
                    subtitlesSource: [{
                        subtitlePath: "content/subtitles/english_subtitle.txt",
                        subtileLabel: "English"
                    }, {
                        subtitlePath: "content/subtitles/romanian_subtitle.txt",
                        subtileLabel: "Romanian"
                    }, {
                        subtitlePath: "content/subtitles/spanish_subtitle.txt",
                        subtileLabel: "Spanish"
                    }],
                    //audio visualizer
                    audioVisualizerLinesColor: "#0099FF",
                    audioVisualizerCircleColor: "#FFFFFF",
                    //advertisement on pause window
                    aopwTitle: "Advertisement",
                    aopwSource: "",

                    aopwBorderSize: 6,
                    aopwTitleColor: "#FFFFFF",
                    //playback rate / speed
                    showPlaybackRateButton: "no",
                    defaultPlaybackRate: "1", //0.25, 0.5, 1, 1.25, 1.5, 2
                    //sticky on scroll
                    stickyOnScroll: "no",
                    stickyOnScrollShowOpener: "no",
                    stickyOnScrollWidth: "700",
                    stickyOnScrollHeight: "394",
                    //sticky display settings
                    showOpener: "no",
                    showOpenerPlayPauseButton: "no",
                    verticalPosition: "bottom",
                    horizontalPosition: "center",
                    showPlayerByDefault: "no",
                    animatePlayer: "no",
                    openerAlignment: "right",
                    mainBackgroundImagePath: "",
                    openerEqulizerOffsetTop: -1,
                    openerEqulizerOffsetLeft: 3,
                    offsetX: 0,
                    offsetY: 0,
                    //embed window
                    embedWindowCloseButtonMargins: 15,
                    borderColor: "#333333",
                    mainLabelsColor: "#FFFFFF",
                    secondaryLabelsColor: "#a1a1a1",
                    shareAndEmbedTextColor: "#5a5a5a",
                    inputBackgroundColor: "#000000",
                    inputColor: "#FFFFFF",
                    // Deshabilitar el adelanto y retraso del video
                    disableSeeking: true,

                    // Deshabilitar eventos de teclado para adelantar y retrasar
                    keyboard: {
                        global: function() {
                            return {
                                left: function() {},
                                right: function() {}
                            };
                        }
                    },
                    //ads
                });
                videoInstances[instanceName] = video;
            }
        }
        var pagina = document.getElementById('pagina');
        initializeVideoPlayer("player1", "myDiv",
            "https://player.vimeo.com/video/837702858",
            "{{ route('user.reward.video1') }}");

        document.getElementById('1').addEventListener('click', function presion1() {
            initializeVideoPlayer("player1", "myDiv",
                "https://player.vimeo.com/video/837702858",
                "{{ route('user.reward.video1') }}");
        });
        document.getElementById('2').addEventListener('click', function presion2() {
            initializeVideoPlayer("player2", "myDiv2",
                "https://www.youtube.com/watch?v=viZu3YPvsoo&ab_channel=Ansel%3Av",
                "{{ route('user.reward.video2') }}");
        });
        document.getElementById('4').addEventListener('click', function presion4() {
            if (pagina.value >= 3) {
                initializeVideoPlayer("player4", "myDiv4",
                    "https://www.youtube.com/watch?v=XqOUcYkTnx0&ab_channel=TheGarushia",
                    "{{ route('user.reward.video3') }}");
            }
        });
        document.getElementById('5').addEventListener('click', function presion5() {
            if (pagina.value == 4) {
                initializeVideoPlayer("player5", "myDiv5",
                    "https://www.youtube.com/watch?v=ogK4Cv08HoI&ab_channel=Pepitooficial",
                    "{{ route('user.reward.pdf') }}");
            }
        });
    </script>
@endpush
