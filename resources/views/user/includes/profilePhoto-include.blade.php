@push('header')
    <link preload rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css'>
@endpush
<style>
    .avatar-view img {
        width: 100%;
    }

    @media screen and (min-width: 800px) {
        .avatar-view {
            display: block;
            margin: 2% auto 0%;
            height: auto;
            width: 375px;
            border: 3px solid #fff;
            border-radius: 50%;
            box-shadow: 0 0 5px rgba(0, 0, 0, .15);
            cursor: pointer;
            overflow: hidden;
        }
    }

    @media screen and (max-width: 800px) {
        .avatar-view {
            display: block;
            height: auto;
            width: 220px;
            border: 3px solid #fff;
            border-radius: 50%;
            box-shadow: 0 0 5px rgba(0, 0, 0, .15);
            cursor: pointer;
            overflow: hidden;
            margin-left: -6px;
        }

        .btn-guardar-foto {
            margin-left: -15px;
        }
    }

    #cropModal .preview {
        float: left;
        margin-top: 15px;
        margin-right: 15px;
        border: 1px solid #eee;
        border-radius: 50%;
        background-color: #fff;
        overflow: hidden;
    }

    .preview-lg {
        height: 184px;
        width: 184px;
        margin-top: 15px;
    }

    .avatar-wrapper {
        height: 364px;
        width: 100%;
        margin-top: 15px;
        box-shadow: inset 0 0 5px rgba(0, 0, 0, .25);
        background-color: #fcfcfc;
        overflow: hidden;
    }

    #cropModal .modal-lg {
        max-width: 1000px !important;
    }

    #coprModal .close {
        position: absolute;
        right: 10px;
        top: 10px;
    }
</style>
<div class="row" style="">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-3">
        <div class="card-head pb-0">
            <strong>
                <h5> {{ ___('User Image') }} {{ $user->foto ? '' : '(' . ___('no photo yet') . ')' }}</h5>
            </strong>
        </div>
        <div class="avatar-view" title="{{ ___('Change the photo profile') }}">
            @if ($user->foto == null)
                <img loading="lazy" class="fotoperfil" src="{{ asset2('imagenes/user-default.jpg') }}" alt="Avatar">
            @else
                <img loading="lazy" class="fotoperfil" src="{{ asset2('imagenes/perfil/' . $user->foto) }}" alt="Avatar">
            @endif
            <div class="cropped"></div>
        </div>
        <input class="btn btn-sm btn-auto btn-primary input-file image" id="my-file" type="file">
        <label tabindex="0" for="my-file"
            class="btn btn-sm btn-auto btn-primary btn-guardar-foto input-file-trigger image"
            style="position:relative; color:#fff;">{{ ___('Change Image') }}</label>

        <div class="modal fade" id="cropModal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog"
            tabindex="-1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form class="avatar-form" action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                                    class="ti ti-close"></em></a>
                            <h4 class="modal-title" id="avatar-modal-label">{{ ___('Change image profile') }}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="img-container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper">
                                            <img loading="lazy" id="image">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="preview preview-lg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> --}}
                            <button type="button" class="btn btn-primary" id="crop">{{ ___('Save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('footer')
    {{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script> --}}
    <script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js'></script>
    <script id="rendered-js">
        var $modal = $('#cropModal');
        var image = $('#cropModal #image').get(0);
        var cropper;

        $("body").on("change", ".image", function(e) {
            var files = e.target.files;
            var done = function(url) {
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });


        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 1,
                autoCropArea: 1,
                preview: '.preview',
                ready: function() {
                    //Should set crop box data first here
                    cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
                }
            });
        }).on('hidden.bs.modal', function() {
            cropBoxData = cropper.getCropBoxData();
            canvasData = cropper.getCanvasData();
            cropper.destroy();
        });

        $("#crop").click(function() {
            canvas = cropper.getCroppedCanvas({
                width: 720,
                height: 720,
                imageSmoothingEnabled: true,
                imageSmoothingQuality: 'high'
            });

            canvas.toBlob(function(blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    $('#crop').html(
                        '{{ ___('Loading') }} <div class="spinner-border spinner-border-sm" role="status"> <span class="sr-only">{{ ___('Loading') }}...</span></div>'
                    )
                    $('#crop').addClass('disabled')
                    var base64data = reader.result;
                    $.ajax({
                        type: "POST",

                        url: '{{ route('update.imagen.perfil') }}',
                        data: {
                            'image': base64data,
                            '_token': '{{ csrf_token() }}',
                        },
                        success: function(data) {
                            console.log(data)
                            $('.fotoperfil').prop('src', data.url)
                            $modal.modal('hide');
                            $('#crop').html('{{ ___('Save') }}')
                            $('#crop').removeClass('disabled')
                            displaySuccessToaster(data.estado, data.mensaje)


                        }
                    });
                };
            });
        });
    </script>
    <script>
        document.querySelector("html").classList.add('js');

        var fileInput = document.querySelector(".input-file"),
            button = document.querySelector(".input-file-trigger"),
            the_return = document.querySelector(".file-return");

        button.addEventListener("keydown", function(event) {
            if (event.keyCode == 13 || event.keyCode == 32) {
                fileInput.focus();
            }
        });
        button.addEventListener("click", function(event) {
            fileInput.focus();
            return false;
        });
        fileInput.addEventListener("change", function(event) {
            the_return.innerHTML = this.value;
        });
    </script>
    <script>
        function displaySuccessToaster(tipo, mensaje) {
            show_toast(tipo, mensaje);
        }
    </script>
@endpush
