@section('gosencss')

<style>
    .a-card{
    background: linear-gradient(#fff, #cad4e1);
}
.b-card{
    background: #16171a;
}
    .html-referral {
        --black: #000;
        --white: #fff;
        --darkest: #101010;
        --darker: {{ asset(theme_color_user(auth()->user()->theme_color,'base')) }};
        --dark: #6c798b;
        --medium: #dfe7ef;
        --light: #cad4e1;
        --lighter: #f5f8fc;
        --lightest: var(--white);
        --primary: #7b16ff;
        --primary-light: #ddd9ff;
        --primary-trans: rgba(123, 22, 255, 0.4);
        --yellow: #F18F01;
        --orange: #2E86AB;
        --teal: #C73E1D;
        /* --bg: var(--darkest); */
        --color: var(--lightest);
        --surface: var(--darker);
    }
    .html-referral {
        font-size: 62.5%;
        box-sizing: border-box;
    }
    /* *, *:before, *:after {
        box-sizing: inherit;
    } */
    .html-referral, .body-referral {
        width: 100%;
        height: 100%;
    }
    .body-referral {
        background: var(--bg);
        color: var(--dark);
        font-size: 1.6rem;
        /* font-family: "Overpass Mono", system-ui; */
    }
    .l-wrapper {
        width: 100%;
        max-width: 960px;
        margin: auto;
        padding: 0.0rem 1.6rem 3.2rem;
    }
    .l-grid {
        grid-template-columns: 1fr 2fr;
        grid-column-gap: 1.6rem;
        grid-row-gap: 1.6rem;
        
    }
    .c-header {
        padding: 1.6rem 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 2.4rem;
        position: relative;
    }
    .c-header:before {
        content: "";
        position: absolute;
        bottom: 0;
        right: 0;
        left: 0;
        height: 0.2rem;
        background: var(--primary-trans);
    }
    .c-row {
    --bs-gutter-x: 0px;
}
    .c-card {
        border-radius: 20px;
        background: linear-gradient(180deg, {{ asset(theme_color_user(auth()->user()->theme_color,'sidebar')) }}, var(--surface));
        width: 100%;
        margin-bottom: 1.6rem;
        box-shadow: 0px 0px 0px 1px rgba(255, 255, 255, 0.12);
    }
    .c-card__body, .c-card__header {
        padding: 1.2rem;
    }
    .leaderboard__profile {
        display: grid;
        grid-template-columns: 1fr 3fr 1fr;
        align-items: center;
        overflow: hidden;
        box-shadow: 0 5px 7px -1px rgba(51, 51, 51, 0.23);
        cursor: pointer;
        transition: transform 0.25s cubic-bezier(0.7, 0.98, 0.86, 0.98), box-shadow 0.25s cubic-bezier(0.7, 0.98, 0.86, 0.98);
        justify-items: center;
    }

    .leaderboard__profile:hover {
        transform: scale(1.1);
        box-shadow: 0 9px 47px 11px rgba(51, 51, 51, 0.18);
    }
    .c-card__header {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-bottom: 0;
    }
    .c-logo {
        display: inline-block;
        width: 100%;
        max-width: 17.6rem;
        user-select: none;
    }
    .c-list {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }
    .c-list__item {
        padding: 1.0rem 0;
    }
    .c-list__item .c-flag {
        margin-top: 0.8rem;
    }
    .c-list__grid {
        grid-template-columns: 4.8rem 3fr 1fr;
        grid-column-gap: 2.4rem;
    }
    .c-media {
        display: inline-flex;
        align-items: center;
        flex-direction: column;
    }
    .c-media__title {
        font-weight: 500;
    }
    .c-avatar {
        width: 3.8rem;
        height: 3.8rem;
        box-shadow: inset 0px 0px 0px 1px currentColor;
        border-radius: 50%;
        /* border: 2px solid var(--darker); */
    }
    .c-avatar--lg {
        width: 9.6rem;
        height: 9.6rem;
    }
    .c-button {
        display: inline-block;
        background: var(--dark);
        border: 0;
        border-radius: 0.4rem;
        padding: 1.2rem 2rem;
        transition: all 120ms ease-out 0s;
    }
    .c-button--block {
        display: block;
        width: 100%;
    }
    .c-button:hover, .c-button:focus {
        filter: brightness(0.9);
    }
    .c-button:focus {
        box-shadow: 0px 0px 0px 0.4rem var(--primary-trans);
    }
    .c-button:active {
        box-shadow: 0px 0px 0px 0.4rem var(--primary-trans), inset 0px 0px 0.8rem rgba(0, 0, 0, 0.2);
        filter: brightness(0.8);
    }
    .c-select {
        background: transparent;
        padding: 1.2rem;
        appearance: none;
        font-size: 1.4rem;
        border-color: rgba(255, 255, 255, 0.2);
        transition: all 120ms ease-out 0s;
    }
    .c-select:hover {
        background: var(--darkest);
    }
    .c-flag {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100%;
        border-left: 5px solid var(--dark);
    }
    .c-button--light {
        background: var(--lightest);
    }
    .c-button--primary {
        background: var(--primary);
    }
    .c-button--dark {
        background: var(--darkest);
    }
    .c-button--transparent {
        background: transparent;
    }
    .c-button--medium {
        background: var(--medium);
    }
    .c-button--yellow {
        background: var(--yellow);
    }
    .c-button--orange {
        background: var(--orange);
    }
    .c-button--teal {
        background: var(--teal);
    }
    .c-button--light-gradient {
        background: linear-gradient(to top, var(--light), var(--lightest));
    }
    .u-text--title {
        font-family: "Roboto", sans-serif;
    }
    .u-text--left {
        text-align: left;
    }
    .u-text--center {
        text-align: center;
    }
    .u-text--right {
        text-align: right;
    }
    .u-bg--light {
        background: var(--lightest) !important;
    }
    .u-text--light {
        color: var(--lightest) !important;
    }
    .u-bg--primary {
        background: var(--primary) !important;
    }
    .u-text--primary {
        color: var(--primary) !important;
    }
    .u-bg--dark {
        background: var(--darkest) !important;
    }
    .u-text--dark {
        color: var(--darkest) !important;
    }
    .u-bg--transparent {
        background: transparent !important;
    }
    .u-text--transparent {
        color: transparent !important;
    }
    .u-bg--medium {
        background: var(--medium) !important;
    }
    .u-text--medium {
        color: var(--medium) !important;
    }
    .u-bg-1 {
        border-left: 5px solid var(--yellow) !important;
    }
    .u-text--yellow {
        color: var(--yellow) !important;
    }
    .u-bg-3 {
        border-left: 5px solid var(--orange) !important;
    }
    .u-text--orange {
        color: var(--orange) !important;
    }
    .u-bg-2 {
        border-left: 5px solid var(--teal) !important;
    }
    .u-text--teal {
        color: var(--teal) !important;
    }
    .u-text--teal-card {
        color: var(--orange);
        font-size: 1rem;
        margin-top: -9px;        
    }
    .head-title{
        background: linear-gradient(180deg, {{ asset(theme_color_user(auth()->user()->theme_color,'sidebar')) }}, var(--surface));
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }

    .l-grid__item
    {
        padding: 0px 30px 0px 40px;
    }
    .l-grid__item-1{
        padding: 0px 40px 0px 30px;
    }
    .u-bg--light-gradient {
        background: linear-gradient(to top, var(--light), var(--lightest))!important;
    }
    .u-text--light-gradient {
        color: linear-gradient(to top, var(--light), var(--lightest)) !important;
    }
    .u-display--flex {
        display: flex;
    }
    .u-align--center {
        align-items: center;
    }
    .u-justify--center {
        justify-content: center;
    }
    .u-align--flex-end {
        align-items: flex-end;
    }
    .u-justify--flex-end {
        justify-content: flex-end;
    }
    .u-align--flex-start {
        align-items: flex-start;
    }
    .u-justify--flex-start {
        justify-content: flex-start;
    }
    .u-align--space-between {
        align-items: space-between;
    }
    .u-justify--space-between {
        justify-content: space-between;
    }
    .u-text--small {
        font-size: 1.4rem;
    }
    .u-text--small-rank {
        font-size: 1rem;
        color: var(--medium);
        margin: 10px 10px 10px 10px;
        font-weight: 500;
    }
    .u-text--small-rank-1 {
        font-size: 1rem;
        color: var(--medium);
        padding: 10px 10px 10px 10px;
        font-weight: 500;
    }
    .u-pl--2 {
        padding-left: 0.2rem;
    }
    .u-ml--2 {
        margin-left: 0.2rem;
    }
    .u-pr--2 {
        padding-right: 0.2rem;
    }
    .u-mr--2 {
        margin-right: 0.2rem;
    }
    .u-pb--2 {
        padding-bottom: 0.2rem;
    }
    .u-mb--2 {
        margin-bottom: 0.2rem;
    }
    .u-pt--2 {
        padding-top: 0.2rem;
    }
    .u-mt--2 {
        margin-top: 0.2rem;
    }
    .u-pl--4 {
        padding-left: 0.4rem;
    }
    .u-ml--4 {
        margin-left: 0.4rem;
    }
    .u-pr--4 {
        padding-right: 0.4rem;
    }
    .u-mr--4 {
        margin-right: 0.4rem;
    }
    .u-pb--4 {
        padding-bottom: 0.4rem;
    }
    .u-mb--4 {
        margin-bottom: 0.4rem;
    }
    .u-pt--4 {
        padding-top: 0.4rem;
    }
    .u-mt--4 {
        margin-top: 0.4rem;
    }
    .u-pl--8 {
        padding-left: 0.8rem;
    }
    .u-ml--8 {
        margin-left: 0.8rem;
    }
    .u-pr--8 {
        padding-right: 0.8rem;
    }
    .u-mr--8 {
        margin-right: 0.8rem;
    }
    .u-pb--8 {
        padding-bottom: 0.8rem;
    }
    .u-mb--8 {
        margin-bottom: 0.8rem;
    }
    .u-pt--8 {
        padding-top: 0.8rem;
    }
    .u-mt--8 {
        margin-top: 0.8rem;
    }
    .u-pl--16 {
        padding-left: 1.6rem;
    }
    .u-ml--16 {
        margin-left: 1.6rem;
    }
    .u-pr--16 {
        padding-right: 1.6rem;
    }
    .u-mr--16 {
        margin-right: 1.6rem;
    }
    .u-pb--16 {
        padding-bottom: 1.6rem;
    }
    .u-mb--16 {
        margin-bottom: 1.6rem;
    }
    .u-pt--16 {
        padding-top: 1.6rem;
    }
    .u-mt--16 {
        margin-top: 1.6rem;
    }
    .u-mt--16-card {
        margin: 0.0rem;
        font-size: 2rem;
        color: var(--dark);
        font-weight: 500;
    }
    .u-pl--24 {
        padding-left: 2.4rem;
    }
    .u-ml--24 {
        margin-left: 2.4rem;
    }
    .u-pr--24 {
        padding-right: 2.4rem;
    }
    .u-mr--24 {
        margin-right: 2.4rem;
    }
    .u-pb--24 {
        padding-bottom: 2.4rem;
    }
    .u-mb--24 {
        margin-bottom: 2.4rem;
    }
    .u-pt--24 {
        padding-top: 2.4rem;
    }
    .u-mt--24 {
        margin-top: 2.4rem;
    }
    .u-pl--32 {
        padding-left: 3.2rem;
    }
    .u-ml--32 {
        margin-left: 3.2rem;
    }
    .u-pr--32 {
        padding-right: 3.2rem;
    }
    .u-mr--32 {
        margin-right: 3.2rem;
    }
    .u-pb--32 {
        padding-bottom: 3.2rem;
    }
    .u-mb--32 {
        margin-bottom: 3.2rem;
    }
    .u-pt--32 {
        padding-top: 3.2rem;
    }
    .u-mt--32 {
        margin-top: 3.2rem;
    }

    #confetti{
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: rgb(0, 0, 0, 0)
    }
    .inf-card{
        font-size: 1.1rem;
    }
    .inf-card i{
        color: #F18F01;
    }
    .user-css{
        background: #fff;
        width: 100%;
        height: 100%;
        border-top-left-radius: 20px;
        border-bottom-left-radius: 20px;
        display: inline-flex;
        align-items: center;
        box-shadow: 0 12px 5px rgb(0 0 0 / 8%);
    }
    .ref-css{
        background: #fff;
        color: var(--dark);
        width: 100%;
        height: 100%;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        align-items: center;
        justify-content: center;
        font-size: .93em;
        font-weight: bold;
        box-shadow: 0 12px 5px rgb(0 0 0 / 8%);
    }
    .h2-black{
        font-weight: bold;
        color: var(--dark);
    }
    .img-avatar{
        padding: 20px 20px 20px 70px;
    }

    @media screen and (max-width: 700px) {
        .l-grid__item
    {
        padding: 0px 0px 0px 0px;
    }
    .l-grid__item-1{
        padding: 0px 0px 0px 0px;
    }
        .l-grid {
            grid-template-columns: 1fr;
    }
    .c-card__body, .c-card__header {
            padding: 1.2rem;
    }
    .c-card__header {
            flex-direction: column;
    }
    .c-place {
            transform: translateY(4px);
    }
    .c-list__item .c-flag {
            margin-top: 0.4rem;
    }
    .c-list__grid {
            display: grid;
            grid-template-columns: 3.2rem 3fr 1fr;
            grid-column-gap: 0.8rem;
    }
        .c-media__title {
            font-size: 1.4rem;
    }
        .c-avatar {
            width: 3.2rem;
            height: 3.2rem;
    }
    .c-flag {
            width: 2.4rem;
            height: 2.4rem;
    }
    .img-avatar{
        padding: 20px 20px 20px 15px!important;
    }
    .ref-css{
        font-size: 1.4rem;
    }
    }
    
    </style>
    
@endsection