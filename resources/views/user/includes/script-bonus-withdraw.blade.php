@push('footer')
    <script>
        $(document).ready(function() {
            $("#amo").keyup(testretiro);
            $('#amo').on('keyup', function() {
                if (Number($('#amo').val() >= 10)) {
                    $('#amount').html($('#amo').val() - 2 + ' (USDT)');
                } else {
                    $('#amount').html('0 (USDT)');
                }
            });

            function testretiro() {
                var txtInput = $('#amo'); //input en el cual se introducira el monto a retirar
                var divCopia = $('#amount'); // div en el cual se refleja el monto el cual se retirara
                var monto = $('#monto'); //monto pendiente de aprobacion(aun no disponible)
                var disponibleRetiro = $("#disponible-retiro"); //disponible y aprobado para retiro USDT
                var status = $('#BtnEnviar').attr('data-id');
                if (Number(txtInput.val()) >= 10 && Number(txtInput.val()) <= Number(disponibleRetiro.val())) {
                    $('#advertencia').addClass('d-none');
                    $('#caution').addClass('d-none');
                    if (status === "1") {
                        $('#BtnEnviar').removeClass('disabled');
                    } else {
                        $('#BtnEnviar').addClass('disabled');
                    }
                } else {
                    $('#caution').removeClass('d-none');
                    $('#advertencia').removeClass('d-none');
                    $(
                        '#BtnEnviar').addClass('disabled');
                }
            }



            // $("#amo").keyup(testpa);

            // function testpa() {
            //     var txtInput = $('#amo'); //input en el cual se introducira el monto a retirar
            //     var divCopia = $('#amount'); // div en el cual se refleja el monto el cual se retirara
            //     var monto = $('#monto'); //monto pendiente de aprobacion(aun no disponible)
            //     if (Number(txtInput.val()) > Number(monto.val())) {
            //         $('#amo').val(monto.val());
            //     } else {
            //         txtInput.on('keyup', function() {
            //             divCopia.html(txtInput.val() + ' USDT');
            //         });
            //     }
            // }
            // function testInputAmount() {
            //     var cero = 0;
            //     var status = $('#BtnEnviar').attr('data-id');
            //     var text1 = $("#amo");
            //     var text2 = $("#wallet");
            //     var monto = $("#monto");

            //     if (Number(text1.val()) > Number(monto.val())) {
            //         $('#BtnEnviar').addClass('disabled');
            //     } else if (text1.val() > 0) {
            //         $('#advertencia').addClass('d-none');
            //         if (text2.val() !== '' && status === "1") {
            //             $('#BtnEnviar').removeClass('disabled');
            //         } else {
            //             $('#BtnEnviar').addClass('disabled');
            //         }
            //     } else {
            //         if (Number(text1.val()) === cero) {
            //             $('#BtnEnviar').addClass('disabled');
            //             $('#advertencia').removeClass('d-none');
            //         }
            //     }
            // }




        });
    </script>
    <script>
        $("#BtnEnviar").click(function(e) {
            e.preventDefault();
            var amount = $('#amo').val();
            var wallet = $('#wallet').val();
            var ruta = $('#url').val();

            $.ajax({
                type: "post",
                url: ruta + 'user/bonus/withdraw',
                data: {
                    amount: amount,
                    wallet: wallet,
                    '_token': '{{ csrf_token() }}',
                },
                success: function(json) {
                    displaySuccessToaster(json.msg, json.message)
                    location.reload();
                }
            });
        });

        function displaySuccessToaster(tipo, mensaje) {
            console.log(tipo + ' ' + mensaje);
            if (tipo == 'success') {

                toastr.success(mensaje);
            } else if (tipo == 'error') {

                toastr.error(mensaje);
            }
        }
    </script>
@endpush
