@extends('layouts.auth')
@section('title', ___('Confirm Mail'))
@section('content')

    <div class="page-ath-form">
        <h2>{{___('A verification email was sent to:')}}</h2><h3> {{$user->email}}</h3>
        <h2>{{___('A verification email was sent to:')}}</h2><h3> {{$user->userReset->mail}}</h3>
        <img loading="lazy" src="{{ asset('assets/images/send-mail-min.gif') }}" alt="" style=" margin-left: auto;
        margin-right: auto;
        display: block;">

        
        <div class="gaps-4x"></div>
        <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
            <p style="font-size: 14px; line-height: 140%;"><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">{{___('Please check your inbox of both addresses to finalize the change of mail.')}}</span></em></span><br /><span
                    style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                            style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
            </p>
        </div>
    </div>

    
@endsection
