<div class="card" style="background: #ebebee">
    <h1 class="m-2">{{ ___('Niveles de referidos') }}</h1>
    @if ($transaction < 1)
        <div class="col-lg-12 mt-3 mb-0">
            <div class="card text-center text-white-50 p-3">
                <h4>
                    <span class=""
                        style="color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}">{{ ___('Recuerda que los niveles de invitado y sus beneficios se activan con la compra de paquetes') }}.</span>
                </h4>
            </div>
        </div>
    @endif
    @php
        $numero = 1;
        $colorActive = '#c2c2c5';
        $colorDisabled = '#F3F3F3';
        $limite = 1;
        $colorLetraDisabled = '#D0CECE';
        $lvl = 0;
    @endphp
    @foreach ($usuariosPorNivelYFecha as $tiempo)
        <div class="card m-3 pl-2">
            <div class="row text-center">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-2"
                    style="background:{{ $nivelesActivos == null ? $colorDisabled : ($limite <= $nivelesActivos->count() ? $colorActive : $colorDisabled) }}; border-top-left-radius:20px; border-bottom-left-radius:20px; border-bottom-right-radius: 20px; border-top-right-radius: 20px;left: -4px;">
                    <em class="fa fa-circle text-{{ $nivelesActivos == null ? 'danger' : ($limite <= $nivelesActivos->count() ? 'success' : 'danger') }}"
                        style="right:8px; top:8px; position:absolute; z-index:1; font-size:15px; cursor:auto;"
                        data-toggle="tooltip"data-placement="bottom"
                        data-original-title="{{ $nivelesActivos == null ? ___('Disabled') : ($limite <= $nivelesActivos->count() ? ___('Enabled') : ___('Disabled')) }}"></em>
                    <h1>
                        @php
                            $cadena = $niveles[$lvl]['name'];
                            $separador = 'level';
                            $separada = explode($separador, $cadena);
                        @endphp
                        <strong class=""
                            style="color:{{ $nivelesActivos == null ? $colorLetraDisabled : ($limite <= $nivelesActivos->count() ? '' : $colorLetraDisabled) }}">
                            {{ ___(ucfirst(substr($niveles[$lvl]['name'], 0, -1))) }} {{ $separada[1] }}
                        </strong>
                    </h1>
                    <div class="col p-2"
                        style="color:{{ $nivelesActivos == null ? $colorLetraDisabled : ($limite <= $nivelesActivos->count() ? '' : $colorLetraDisabled) }}">
                        <strong> {{ ___('Usuarios totales (todo el tiempo)') }}</strong> : <strong
                            style="font-size: 20px;">{{ isset($referrals[$numero]) ? count($referrals[$numero]) : '0' }}</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 pt-4">
                    <h4
                        style="color:{{ $nivelesActivos == null ? $colorLetraDisabled : ($limite <= $nivelesActivos->count() ? '' : $colorLetraDisabled) }}">
                        {{ ___('Este día') }}
                        <br>
                        <strong>
                            +{{ $tiempo['Dia'] }}
                        </strong>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 pt-4">
                    <h4
                        style="color:{{ $nivelesActivos == null ? $colorLetraDisabled : ($limite <= $nivelesActivos->count() ? '' : $colorLetraDisabled) }}">
                        {{ ___('Esta semana') }}
                        <br>
                        <strong>
                            +{{ $tiempo['Semana'] }}
                        </strong>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pt-4">
                    <h4
                        style="color:{{ $nivelesActivos == null ? $colorLetraDisabled : ($limite <= $nivelesActivos->count() ? '' : $colorLetraDisabled) }}">
                        {{ ___('Este mes') }}
                        <br>
                        <strong>
                            +{{ $tiempo['Mes'] }}
                        </strong>
                    </h4>
                </div>

            </div>
        </div>
        @php
            $numero += 1;
            $limite += 1;
            $lvl += 1;
        @endphp
    @endforeach
</div>
