@extends('layouts.auth')
@section('title', ___('Sign-in'))
@section('content')

    <div class="page-ath-form">
        <h3>
            {{___('Your password was reset successfully go back to Login to start session')}}!</h3>
        <img loading="lazy" src="{{ asset('assets/images/resetpass.gif') }}" alt="" style=" margin-left: auto;
        margin-right: auto;
        display: block;">

        
        <div class="gaps-4x"></div>
        <a href="{{ route('home') }}" style=" text-align: center; display: inline-block; width: 100%" type="button" class="btn btn-primary align-content-center text-white">{{___('Back to Login')}}</a>
    </div>
@endsection
