@extends('layouts.auth')
@section('title', ___('Sign-in'))
@section('content')
    <div class="page-ath-form">
        <h2 class="mb-4" style="text-align:center;">
            @if ($estado)
                <small>{{ ___('The wallet was already confirmed') }}
                @else
                    <small>{{ ___('Wallet Confirmed !') }}
            @endif
            </small>
        </h2>
        <div class="form-group row justify-content-center">
            <span style="text-align:center;">
                {{ ___('The wallet (USDT), was confirmed successfully') }}<i class="fa fa-check"></i>
            </span>
            <small class="mt-3 text-center">
                <strong>
                    ({{___('Presiona')}} "{{___('Back to account')}}" {{___('para volver a tu cuenta y completar la actualizacion de tu billetera')}}).
                </strong>
            </small>
            <div>
                <span style="font-weight: bold;">
                    <input type="hidden" name="id" value="{{ $id }}">
                </span>
            </div>
            <img loading="lazy" src="{{ asset('assets/images/wallet.gif') }}"height="auto" style="width:55%">
            <input class="form-control" style="padding:17px;top:-28px; width:350px; text-align:center; border-radius:40px" type="text"
            value="{{ $user->wallet_dollar }}" readonly>
            <div class="gaps-4x ml-4" style="display: grid; place-items:center;">
                <a type="" id="button" class="btn btn-primary" href="{{ route('login') }}">
                    {{ ___('Back to account') }}
                </a>
            </div>
        </div>
    </div>
@endsection