@extends('layouts.user')
@section('title', ucfirst($page->title))
@php
($has_sidebar = true)
@endphp
@push('header')
<style>
    @media (max-width:580px)
        {
            .buto{
                width: 100%!important;
            }
            .image-card{
                margin: 5px!important;
            }
        }
        .form-control{
            color: #505258;
            line-height: 20px;
            border: 1px solid rgba(211, 224, 243, 0.5);
        }

        /* cards-del-modal */
        @media only screen and (max-width: 600px) {
            .card-body{
                display: block;
                }
        }
        .card-body{
            background: radial-gradient(circle, rgb(255 255 255) 0%, rgb(0 27 88) 100%);
            display: flex;
        }

        .image-card{
            margin: auto;
            position: relative;
            transition: transform 4s ease-in;
            overflow: hidden;
            border:4px solid #fff;
        }
        .img-card{
            width:100%;
            height: 436px;
            transition: transform 1s ease-in;
        }
        .img-card:hover{
            transform: scale(1.2);
        }
        .image-card:before {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left:0;
            content: "Click Download!";
            transform:translateY(-100%);
            font-size: 25px;
            color:#fff;
            display: block;
            background-color: rgba(188, 143, 143, 0.137);
            z-index: 1;
            display:flex;
            justify-content: center;
            align-items: center;
            /* font-family: sans-serif; */
            transition: transform .4s ease-in;
            border:4px solid white;
            justify-content: center;
            box-sizing: border-box;
        }
        .image-card:after {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left:0;
            content: "Click Download!";
            transform:translateY(100%);
            font-size: 25px;
            color:#fff;
            display: block;
            background-color: rgba(188, 143, 143, 0.137);
            z-index: 1;
            display:flex;
            justify-content: center;
            align-items: center;
            /* font-family: sans-serif; */
            transition: transform .4s ease-in;
            border:4px solid white;
            justify-content: center;
            box-sizing: border-box;
        }
        .image-card:hover::before{
        transform: translateY(0%); 
        
        }
        .image-card:hover::after{
            transform: translateY(0%); 
            
        }
        @media (max-width: 576px){
                .img-card{
                height: 520px;
            }
        }
</style>
@endpush
@section('content')
<div class="content-area content-area-mh card user-account-pages page-{{ $page->slug }}">
    <div class="card-innr">
        <div class="card-head">
            <h2 class="card-title card-title-lg">{{___(replace_shortcode($page->title))}}</h2>
            @if($page->meta_description!=null)
            <p class="large">{{ ___(replace_shortcode($page->meta_description)) }}</p>
            @endif
        </div>
        @if(!empty($page->description))
        <div class="card-text">
            {!! ___(replace_shortcode(auto_p($page->description))) !!}
        </div>
        @endif
        @php
        $user = empty($data) ? auth()->user() : $data;
        $idioma = $user->lang;
        @endphp
        <div class="gaps-1x"></div>
        <div class="referral-form">
            <h4 class="card-title card-title-sm">{{ ___('Referral URL') }}</h4>
            <div class="copy-wrap mgb-1-5x mgt-1- buto" style="width:50%;">
                <span class="copy-feedback"></span>
                <em class="copy-icon fas fa-link"></em>
                <input type="text" class="copy-address" value="{{___('Copy your referral URL')}}" disabled>
                <button class="copy-trigger copy-clipboard"
                        data-clipboard-text="{{ route('public.referral') . '?ref=' . substr(set_id(auth()->user()->username), 2). '&lang=' . $idioma }}"><em
                            class="ti ti-files"></em></button>            
            </div>
            <p class="mgmt-1x"><em><small>{{ ___('Use above link to refer your friend and get referral bonus.') }}</small></em></p>
        </div>

        @include('user.includes.landing-carrusel')
        
        <div class="sap sap-gap" style="margin-top: 70px; margin-bottom: 45px;"></div>
        <div class="card-head">
            <h4 class="card-title card-title-sm">{{ ___('Referral Lists') }}</h4>
        </div>
        <table class="data-table dt-init refferal-table" data-items="10">
            <thead>
                <tr class="data-item data-head">
                    <th class="data-col refferal-name"><span>{{ ___('User Name') }}</span></th>
                    <th class="data-col refferal-tokens"><span>{{ ___('Earn Token') }}</span></th>
                    <th class="data-col refferal-date"><span>{{ ___('Register Date') }}</span></th>
                </tr>
            </thead>
            <tbody>
                @forelse($reffered as $refer)
                <tr class="data-item">
                    <td class="data-col refferal-name">{{ $refer->name }}</td>
                    <td class="data-col refferal-tokens">{{ (referral_bonus($refer->id)) ? referral_bonus($refer->id).' '.token_symbol() : ___('~') }}</td>
                    <td class="data-col refferal-date">{{ _date($refer->created_at) }}</td>
                </tr>
                @empty
                <tr class="data-item">
                    <td class="data-col">{{ ___('No one join yet!') }}</td>
                    <td class="data-col"></td>
                    <td class="data-col"></td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('modals')
@for ($i = 1; $i <= 11; $i++)
<div class="modal fade" id="showBanner{{$i}}">
    <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
        <div class="modal-content card-body" style="border: 0px;">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                class="ti ti-close"></em></a>
            <div class="row">
                <div class="col-md-4 col-12">
                    <a href="{{ route('download.banner',['g'.$i,1]) }}" >
                    <div title="Download" class="image-card">
                        
                        <img loading="lazy" class="img-card" src="{{ asset('images/banners/banner-g1.jpg') }}" alt="">
                        
                    </div>
                    </a>
                </div>
                <div class="col-md-4 col-12">
                    <a href="{{ route('download.banner',['g'.$i,2]) }}">
                        <div title="Download" class="image-card">
                            <img loading="lazy" class="img-card" src="{{ asset('images/banners/banner-g2.jpg') }}" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-12">
                    
                    <a href="{{ route('download.banner',['g'.$i,3]) }}">
                        <div title="Download" class="image-card">
                            <img loading="lazy" class="img-card" src="{{ asset('images/banners/banner-g3.jpg') }}" alt="">
                        </div>
                    </a>
                </div>
            </div>

        </div>{{-- .modal-content --}}
    </div>{{-- .modal-dialog --}}
</div>{{-- Modal End --}}
@endfor

@endsection
@push('footer')
    <script>
        var target;
        $(function(){
                $("ul#land li").on({
                    mouseenter: function(){
                        var el = $(this);
                        var img = el.attr("data-img");

                        target = $("#boxImage");
                        target.find('img').attr("src","{{ asset2('images/landing') }}"+img);
                        target.show();
                    },
                    mouseleave: function(){
                        target.hide();
                        target.find("img").removeAttr("src");
                        target = $("#boxImage");
                        target.find('img').attr("src","{{ asset2('images/landing/Landing-Page.svg') }}");
                        target.show();
                    }
                })
                target = $("#boxImage");
                target.find('img').attr("src","{{ asset2('images/landing/Landing-Page.svg') }}");
                target.show();
            });
    </script>
@endpush    