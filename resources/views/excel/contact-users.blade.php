<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ ___('Withdraw') }}</title>
    <link preload rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
    <h3 align="center" style="font-weight: bold;">
        {{ ___('Users contact') }} {{ $date }} 
{{-- - {{ $language == 'all' ? $language : $language->name }} --}}
    </h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col" style="font-weight: bold;">ID</th>
                <th scope="col" style="font-weight: bold;">{{ ___('NOMBRE') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('TELEFONO/CELULAR') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('CORREO') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('IDIOMA') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('PAIS') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->mobile }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->language->name }}</td>
                    <td>{{ $data->country }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
