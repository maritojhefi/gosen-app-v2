<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ ___('Withdraw') }}</title>
    <link preload rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
    <h3 align="center" style="font-weight: bold;">
        {{ ___('Withdraw Transactions') }} -
        ({{ $periodo1 == null || $periodo2 == null ? $date : $periodo1 . ' ' . ___('hasta') . ' ' . $periodo2 }}) /
        {{ ___(strtoupper($estadoReporte == null ? 'Todos' : ($estadoReporte == 'aprobadosusuario' ? 'Aprobado por el Usuario' : $estadoReporte))) }}
    </h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col" style="font-weight: bold;">ID</th>
                <th scope="col" style="font-weight: bold;">{{ ___('CODIGO RETIRO') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('ESTADO') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('FECHA') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('USUARIO') }}</th>
                <th scope="col" style="font-weight: bold;">ID{{ ___('USUARIO') }}</th>
                <th scope="col" style="font-weight: bold;">{{ ___('MONTO SOLICITUD') }}(USDT)</th>
                <th scope="col" style="font-weight: bold;">{{ ___('COBRO POR RETIRO') }}(USDT)</th>
                <th scope="col" style="font-weight: bold;">{{ ___('MONTO TOTAL') }}(USDT)</th>
                <th scope="col" style="font-weight: bold;">{{ ___('BILLETERA') }}(USDT)</th>
                <th scope="col" style="font-weight: bold;">{{ ___('APROBADO POR USUARIO') }}</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($withdraw as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->code }}</td>
                    <td>{{ $data->status }}</td>
                    <td>{{ $data->created_at }}</td>
                    <td>{{ $data->userBy->name }}</td>
                    <td>{{ $data->user_id }}</td>
                    <td>{{ $data->amount }}</td>
                    <td>2</td>
                    <td>{{ $data->amount - 2 }}</td>
                    <td>{{ $data->wallet }}</td>
                    <td>{{ $data->approved_bonus == 1 ? ___('Si') : ___('No') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
