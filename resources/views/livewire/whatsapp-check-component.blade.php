<div class="d-flex">
    @if ($user->whatsapp_status)
        <div class="position-relative mx-2 mt-3">
            <a href="javascript:void(0)" class="mr-2 d-flex align-items-center justify-content-center popover-btn"
                data-popover="popover-whatsapp-verify">
                <i class="fab fa-whatsapp text-success"
                    style="font-size:2rem; margin-bottom: 0%;border-radius: 28px;height: 33px;width: 32px;"></i>
            </a>
            <div class="popover" id="popover-whatsapp-verify">
                <div class="popover-content">
                    <p>{{ ___('Tu whatsapp esta verificado') }}</p>
                </div>
            </div>
        </div>
    @else
        <div class="position-relative mx-2 mt-3">
            <a href="javascript:void(0)" id="btnModalWhatsappCheck" data-bs-toggle="modal"
                data-bs-target="#modalWhatsappCheck"
                class="mr-2 d-flex align-items-center justify-content-center popover-btn"
                data-popover="popover-whatsapp-no-verify">
                <i class="fab fa-whatsapp fa-2x text-success"></i>
            </a>
            <div class="popover" id="popover-whatsapp-no-verify">
                <div class="popover-content">
                    <p class="fw-bold m-0">{{ ___('Tu whatsapp no esta verificado') }}</p>
                    <p>{{ ___('Recibe 5 tokens al finalizar la verificacion, comienza ahora!') }}</p>
                </div>
            </div>
            <span class="position-absolute top-0 start-100 rounded-circle text-white bg-danger flashit">
                <i class="fas fa-exclamation"></i>
            </span>
        </div>
    @endif
</div>

@push('modals')
    <div class="modal fade" id="modalWhatsappCheck" tabindex="-1" data-bs-backdrop="static">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <form class="validate-modern validate-form" method="POST" id="online_payment">
                            <input type="hidden" name="_token" value="8YZPrMV4JN6SEC4WvseEC3Ieyf0JH6THbOcctFHQ"> <input
                                type="hidden" name="pp_token" id="token_amount" value="5000">
                            <input type="hidden" name="pp_currency" id="pay_currency" value="usd">
                            <h4 class="popup-title">{{ ___('Verificacion de whatsapp') }}</h4>
                            <p id="enviar-parrafo" class="lead {{ !$user->whatsapp_code ? '' : 'd-none' }}">
                                {{ ___('Verifica tu WhatsApp') }}
                                {{ $user->whatsapp_status == 1 && $user->mobile != ' ' ? ' ' : ___('y gana 5 tokens') }}</p>
                            <p id="verificar-parrafo" class="lead {{ $user->whatsapp_code ? '' : 'd-none' }}">
                                {{ ___('Ingresa') }}<strong> {{ ___('el código de verificación') }} </strong>
                                {{ ___('enviado al número') }} <strong id="numeroEnvio"></strong>
                                {{ $user->whatsapp_status == 1 && $user->mobile != ' ' ? ' ' : ___(' y gana 5 tokens') }}.
                            </p>
                            <a id="verificar-volver-atras" href="#"
                                class="{{ $user->whatsapp_code ? '' : 'd-none' }}" style="font-size: 12px;"><i
                                    class="fas fa-arrow-left"></i> <span
                                    style="border-bottom: solid 1px">{{ ___('¿Ingresar otro número?') }}</span></a>
                            <div class="{{ $user->whatsapp_code ? '' : 'd-none' }}" id="verificar-card">
                                <div class="otp-input-fields">
                                    <input type="number"
                                        class="otp__digit otp__field__1 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__1">
                                    <input type="number"
                                        class="otp__digit otp__field__2 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__2">
                                    <input type="number"
                                        class="otp__digit otp__field__3 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__3">
                                    <input type="number"
                                        class="otp__digit otp__field__4 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__4">
                                    <input type="number"
                                        class="otp__digit otp__field__5 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__5">
                                    <input type="number"
                                        class="otp__digit otp__field__6 verificatinput {{ auth()->user()->theme_color == 'style-black' ? 'text-white' : '' }}"
                                        id="otp__field__6">
                                </div>
                                <button class="btn btn-alt btn-primary" id="verificarCodigo">
                                    {{ ___('Verificar el codigo') }}</button>
                                <div class="gaps-3x"></div>
                            </div>
                            <div id="enviar-card" class="{{ !$user->whatsapp_code ? '' : 'd-none' }}">
                                <div id="cargando" class="text-center d-none">
                                    <div class="spinner-grow text-center" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <input type="text" class="form-control form-control-lg mx-auto d-block m-2 pt-2"
                                    id="telf" value="{{ $user->mobile }}" autofocus
                                    placeholder="   Numero de whatsapp">
                                <span style="font-size: 11px;" id="telf-span" class="text-danger d-none"></span>

                                <p class=" font-italic mgb-1-5x"><small>*
                                        {{ ___('El envio del mensaje no tiene ningun costo') }}.</small></p>
                                <button type="button" class="btn btn-alt btn-primary" id="enviarCodigo">
                                    {{ ___('Enviar codigo') }} <em class="ti ti-arrow-right mgl-2x"></em></button>
                                <div class="gaps-3x"></div>
                            </div>
                            <div class="note note-plane note-light">
                                <em class="fas fa-info-circle"></em>
                                <p class="">
                                    {{ ___('Se te notificara instantaneamente') }}
                                    {{ $user->whatsapp_status == 1 && $user->mobile != ' ' ? ' ' : ___('sobre el abono de 5 tokens') }}
                                    {{ ___('al finalizar el proceso de verificacion') }}
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="modal fade" id="modalWhatsapp" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="modalWhatsappLabel" aria-hidden="true" style="backdrop-filter: blur(3px);">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
            <div class="modal-content p-3" style="background: #363a3f94;opacity: 1.9;">
                <div class="modal-body">
                    <div style="display: grid; place-items:center">
                        <h3 class="text-center text-white" style="font-size: 50px;" id="nombreRegalo">
                        </h3>
                    </div>
                    <div class="text-center" id="icono">
                    </div>
                    <strong>
                        <h1 class="text-center text-white" style="font-size: 50px;">
                            {{ ___('Felicidades') }}!
                        </h1>
                    </strong>
                    <div class="row">
                        <div class="text-center d-flex align-items-center justify-content-center">
                            <i class="text-white mt-2 mb-2 fab fa-whatsapp" style="font-size: 200px"></i>
                        </div>
                    </div>

                    <div class="text-center ">
                        <h5 class="text-white">
                            {{ ___('Acabas de verificar tu número de Whatsapp') }}
                        </h5>
                    </div>
                    <div class="" style="display:flex; align-items:center; justify-content:center;">
                        <a type="button" href="{{ route('disruptive.user.profile', 1) }}" class="btn btn-primary mt-3"
                            id="cerrar-modal">{{ ___('Continuar') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/js/intlTelInput.min.js"
        integrity="sha512-IeQbZp3+T/xyhshcKd0r29GKvdRQdIQNAsyFvT7njgy4Z4zXYDyTKyJi89gC36rdV1SyaQJTlvtV60G3Y6ctcg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/libphonenumber-js/1.10.26/libphonenumber-js.min.js"
        integrity="sha512-00iW/03JLPCWMKeykay1Xh9ZauJ1P1l4HNgZMDVLkkfc2gGSStekjeSWkPef8e0eNDZtpb3GWtfunB0W1m4huQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/whatwg-fetch@3.6.2/dist/fetch.umd.min.js"></script>
    <script>
        (function(b, c) {
            var $ = b.jQuery || b.Cowboy || (b.Cowboy = {}),
                a;
            $.throttle = a = function(e, f, j, i) {
                var h, d = 0;
                if (typeof f !== "boolean") {
                    i = j;
                    j = f;
                    f = c
                }

                function g() {
                    var o = this,
                        m = +new Date() - d,
                        n = arguments;

                    function l() {
                        d = +new Date();
                        j.apply(o, n)
                    }

                    function k() {
                        h = c
                    }
                    if (i && !h) {
                        l()
                    }
                    h && clearTimeout(h);
                    if (i === c && m > e) {
                        l()
                    } else {
                        if (f !== true) {
                            h = setTimeout(i ? k : l, i === c ? e - m : e)
                        }
                    }
                }
                if ($.guid) {
                    g.guid = j.guid = j.guid || $.guid++
                }
                return g
            };
            $.debounce = function(d, e, f) {
                return f === c ? a(d, e, false) : a(d, f, e !== false)
            }
        })(this);
    </script>
    <script>
        var sessionNumber = "{{ $user->mobile_aux }}";
        var input = document.querySelector("#telf");
        var iti = window.intlTelInput(input, {
            separateDialCode: true,
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                fetch("https://ipapi.co/json")
                    .then(function(res) {
                        return res.json();
                    })
                    .then(function(data) {
                        callback(data.country_code);
                    })
                    .catch(function() {
                        callback("us");
                    });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/js/utils.js",
        });

        function numeroEsValido(numero) {
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            var valido = libphonenumber.isValidNumber(numero, codigoRegion);
            return valido;
        }

        var numeroLimpio;
        var numeroFormateado;
        input.addEventListener("keyup", function() {
            var numeroDigitado = $(this).val();
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            numeroFormateado = new libphonenumber.AsYouType(codigoRegion).input(numeroDigitado);
            $(this).focus().val('').val(numeroFormateado);
            var numeroFormateadoHelper = numeroFormateado + 1;
            var longitudPhone = libphonenumber.validatePhoneNumberLength(numeroFormateado, codigoRegion);
            var longitudPhoneHelper = libphonenumber.validatePhoneNumberLength(numeroFormateadoHelper,
                codigoRegion);
            var valido = numeroEsValido(numeroFormateado);
            if ((longitudPhone == undefined) && valido == true) {
                var longitud = numeroFormateado.length;
                $("#telf").prop("maxlength", longitud);
                numeroLimpio = libphonenumber.parse(numeroFormateado, codigoRegion).phone;
            } else if (longitudPhoneHelper == "TOO_LONG") {
                var longitudH = numeroFormateadoHelper.length - 1;
                $("#telf").prop("maxlength", longitudH);
            } else if (longitudPhone == "NOT_A_NUMBER" || longitudPhone == "INVALID_COUNTRY" || longitudPhone ==
                "TOO_SHORT" || longitudPhone == "INVALID_LENGTH") {
                $("#telf").removeAttr("maxlength");
            }
        });

        $(document).ready(function() {
            $('#telf').keyup($.debounce(750, function(e) {
                var valido = numeroEsValido($(this).val());
                if (valido == true) {
                    $("#enviarCodigo").removeAttr("disabled");
                    $("#telf-span").addClass('d-none');
                } else {
                    $("#enviarCodigo").prop("disabled", "disabled");
                    $("#telf-span").removeClass('d-none');
                    $("#telf-span").text("The number is not valid.");
                }
            }));

            $('#enviarCodigo').click(function() {
                $("#enviarCodigo").html("");
                $("#enviarCodigo").prop("disabled", "disabled");
                $("#enviarCodigo").append(
                    "{{ ___('Enviando...') }} <span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>"
                );
                Livewire.emit('enviarCodigo', '+' + iti.s.dialCode + numeroLimpio);
                limpiarOtpFields();
            });

            $('#verificarCodigo').click(function() {
                $("#verificarCodigo").html("");
                $("#verificarCodigo").prop("disabled", "disabled");
                $("#verificarCodigo").append(
                    "{{ ___('Verificando...') }} <span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>"
                );
                var a = $('#otp__field__1').val()
                var b = $('#otp__field__2').val()
                var c = $('#otp__field__3').val()
                var d = $('#otp__field__4').val()
                var e = $('#otp__field__5').val()
                var f = $('#otp__field__6').val()
                var total = "" + a + b + c + d + e + f;
                Livewire.emit('verificarCodigo', total);
            });
            Livewire.on('codigoEnviado', data => {
                if (data > 0) {
                    var codigoRegion = iti.getSelectedCountryData().iso2;
                    codigoRegion = codigoRegion.toUpperCase();
                    var sessionNumberFormatted = new libphonenumber.AsYouType(codigoRegion).input(data);
                    $("#numeroEnvio").text(sessionNumberFormatted);
                    $('#enviar-card').addClass('d-none');
                    $('#enviar-parrafo').addClass('d-none');
                    $('#verificar-card').removeClass('d-none');
                    $('#verificar-parrafo').removeClass('d-none');
                    $('#verificar-volver-atras').removeClass('d-none');
                } else {
                    $("#telf-span").removeClass('d-none');
                    $("#telf-span").text(data);
                }
                $("#enviarCodigo").html("");
                $("#enviarCodigo").removeAttr("disabled");
                $("#enviarCodigo").append(
                    "{{ ___('Enviar codigo') }} <em class='ti ti-arrow-right mgl-2x'></em>");
            })

            Livewire.on('codigoVerificado', function(data1, data2) {
                console.log(data1, data2);

                if (data1 == "valid") {
                    $('#modalWhatsappCheck').modal('hide');
                    $("#mobile-number").removeClass('input-not-verified');
                    $("#aModalWhatsappCheck").addClass('d-none');
                    $("#whatsappVerified").removeClass('d-none');
                    $("#mobile-number").val(data2);
                    setTimeout(() => {
                        $('#modalWhatsapp').modal({
                            backdrop: 'static',
                            keyboard: false
                        }).modal('show');
                    }, 1000);

                } else {
                    limpiarOtpFields();
                    $("#verificarCodigo").html("");
                    $("#verificarCodigo").removeAttr("disabled");
                    $("#verificarCodigo").append("{{ ___('Verificar el codigo') }}");
                }
            })

            $("#btnModalWhatsappCheck").click(function() {
                limpiarOtpFields();
                var telf = "{{ $user->mobile }}";
                if (telf == null || telf == "undefined") {
                    $("#enviarCodigo").prop("disabled", "disabled");
                } else {
                    formatearNumero($("#telf").val());
                }
            });

            $("#verificar-volver-atras").click(function() {
                $(".iti__flag-container").addClass('d-none');
                $("#telf").addClass('d-none');
                $("#cargando").removeClass('d-none');
                $("#enviarCodigo").prop("disabled", "disabled");
                Livewire.emit('numberSession');
                $("#telf-span").addClass('d-none');
                $('#enviar-card').removeClass('d-none');
                $('#enviar-parrafo').removeClass('d-none');
                $('#verificar-card').addClass('d-none');
                $('#verificar-parrafo').addClass('d-none');
                $('#verificar-volver-atras').addClass('d-none');
                limpiarOtpFields();
            });
        });

        Livewire.on('numeroSesionActualizado', numeroSesion => {
            $("#telf").val(numeroSesion);
            iti.destroy();
            var numeroLimpio = libphonenumber.parse(numeroSesion, codigoRegion).phone;
            iti = window.intlTelInput(input, {
                separateDialCode: true,
                initialCountry: "auto",
            });
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            var numeroFormateado = new libphonenumber.AsYouType(codigoRegion).input(numeroLimpio);
            $("#telf").val(numeroFormateado);
            $("#enviarCodigo").removeAttr("disabled");
            $("#telf").removeClass('d-none');
            $(".iti__flag-container").removeClass('d-none');
            $("#cargando").addClass('d-none');
        })

        function limpiarOtpFields() {
            $("#otp__field__1").val("");
            $("#otp__field__2").val("");
            $("#otp__field__3").val("");
            $("#otp__field__4").val("");
            $("#otp__field__5").val("");
            $("#otp__field__6").val("");
        }

        function formatearNumero(telf) {
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            numeroFormateado = new libphonenumber.AsYouType(codigoRegion).input(telf);
            sessionNumber = "{{ $user->mobile_aux }}";
            $("#telf").val(numeroFormateado);
            var numeroFormateadoSesion = new libphonenumber.AsYouType(codigoRegion).input(sessionNumber);
            $("#numeroEnvio").text(numeroFormateadoSesion);
            numeroLimpio = libphonenumber.parse(numeroFormateado, codigoRegion).phone;
        }

        input.addEventListener("countrychange", function() {
            $("#telf").val("");
            $("#enviarCodigo").prop("disabled", "disabled");
        });

        var otp_inputs = document.querySelectorAll(".otp__digit");
        var mykey = "0123456789".split("");
        otp_inputs.forEach((_) => {
            _.addEventListener("keyup", handle_next_input)
        })

        function handle_next_input(event) {
            let current = event.target
            let index = parseInt(current.classList[1].split("__")[2])
            current.value = event.key

            if (event.keyCode == 8 && index > 1) {
                current.previousElementSibling.focus()
            }
            if (index < 6 && mykey.indexOf("" + event.key + "") != -1) {
                var next = current.nextElementSibling;
                next.focus()
            }
            var _finalKey = ""
            for (let {
                    value
                }
                of otp_inputs) {
                _finalKey += value
            }
            if (_finalKey.length == 6) {
                document.querySelector("#_otp").classList.replace("_notok", "_ok")
                document.querySelector("#_otp").innerText = _finalKey
            } else {
                document.querySelector("#_otp").classList.replace("_ok", "_notok")
                document.querySelector("#_otp").innerText = _finalKey
            }
        }
    </script>
@endpush
