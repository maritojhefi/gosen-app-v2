<div class="dropdown d-inline-block">
    <button type="button" class="campana btn header-item noti-icon waves-effect pl-2 pr-2 popover-btn"
        id="page-header-notifications-dropdown" 
        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
        style="border: none;" 
        data-popover="popover-notificacion">
        
        <span id="icono-campana"
            class="material-symbols-outlined text-dark font-size-36 @if (isset($notificaciones) && $notificaciones->campana) fa-beat @endif  ri-notification-3-fill color"
            style="height:auto; width: auto;">
        </span>
        @if (isset($notificaciones) && $notificaciones->campana)
            <span id="dot" class="noti-dot"></span>
        @endif

    </button>
    <div class="popover" id="popover-notificacion">
        <div class="popover-content">
          <p>{{___('Notificaciones')}}</p>
        </div>
    </div>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
        aria-labelledby="page-header-notifications-dropdown" style="background-color: #fff;">
        <div class="p-3">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="m-0"> {{ ___('Notifications') }} </h6>
                </div>
                <div class="col-auto">
                    <a href="#" wire:ignore.self wire:click="clearAll" class="small text-danger"> {{ ___('Clear all') }}</a>
                </div>
                <div class="col-auto">
                    <a href="{{ route('all.notifications') }}" class="small"> {{ ___('View All') }}</a>
                </div>
            </div>
        </div>
        <div data-simplebar="init" style="max-height: 230px;">
            <div class="simplebar-wrapper" style="margin: 0px;">
                <div class="simplebar-height-auto-observer-wrapper">
                    <div class="simplebar-height-auto-observer"></div>
                </div>
                <div class="simplebar-mask">
                    <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                        <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden scroll;">
                            <div class="simplebar-content" style="padding: 0px;">
                                @if (isset($mensajes) && count($mensajes) > 0)

                                    @foreach ($mensajes as $notificacion)
                                        @if (isset($notificacion->estado_header) && $notificacion->estado_header && $notificacion->estado)
                                            <a href="{{$notificacion->ruta}}" wire:click="redirigir('{{ $notificacion->id }}')"
                                                class="text-reset notification-item">
                                                <div class="d-flex">
                                                    <div class="avatar-xs me-3">
                                                        <span
                                                            class="avatar-title bg-{{ $notificacion->color }} rounded-circle font-size-16">
                                                            <i
                                                                class="{{ $notificacion->icono }} @if ($notificacion->estado) fa-move @endif"></i>
                                                        </span>
                                                    </div>
                                                    <div class="flex-1">
                                                        <h6 class="mb-1">{{ ___($notificacion->mensaje) }}</h6>
                                                        <div class="font-size-12 text-muted">
                                                            @if (isset($notificacion->detalle))
                                                                <p class="mb-1">
                                                                    {{ Str::limit(___($notificacion->detalle), 40) }}
                                                                </p>
                                                            @endif

                                                            <p class="mb-0"><i class="mdi mdi-clock-outline"></i>
                                                                {{ App\Helpers\GosenHelper::timeago($notificacion->created_at) }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                @else
                                    <div  class="text-reset ">
                                        <div class="d-flex">
                                            
                                            <div class="flex-1">
                                                <h6 class="mb-1 text-center" style="color:#CACFD2">{{ ___('Sin notificaciones') }}</h6>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="simplebar-placeholder" style="width: auto; height: 359px;"></div>
            </div>
            <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;">
                </div>
            </div>
            <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                <div class="simplebar-scrollbar"
                    style="transform: translate3d(0px, 0px, 0px); display: block; height: 147px;"></div>
            </div>
        </div>
    </div>
</div>
@push('footer')
    @include('partials.livewire-sweetalert')
    <script>
        $(document).ready(function() {
            $('.campana').click(function() {
                if ($('#icono-campana').hasClass('fa-beat')) {

                    $.ajax({
                        type: "get",
                        url: "{{ route('detenerCampana') }}",
                        beforeSend: function() {
                            $('#backk').hide();
                            $("#logocontainer").hide();
                        },
                        success: function(response) {

                            if (response == 'apagado') {
                                $('#icono-campana').removeClass('fa-beat')
                                $('#dot').remove()
                            } else {
                                show_toast('error', '{{ ___('Algo salió mal') }}',
                                    'ti ti-close');
                            }

                        }
                    });
                }

            });
        });
    </script>
@endpush
