<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mt-4">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <h4>{{ ___('Ingresa una palabra o frase') }}</h4>

                                    <div class="app-search d-none d-lg-block">
                                        <div class="position-relative">
                                            <input type="text" class="form-control"
                                                placeholder="{{ ___('Search') }}..." wire:model.lazy="search">
                                            <span class="ri-search-line"></span>
                                        </div>
                                    </div>
                                    <p class="text-muted">
                                        {{ ___('Busca cualquier palabra para buscar sus traducciones') }}</p>
                                    <div>

                                        <button type="button" wire:click="buscar"
                                            class="btn btn-info mt-2 waves-effect waves-light">{{ ___('Buscar') }} <div
                                                wire:loading><span class="spinner-border spinner-border-sm"
                                                    role="status" aria-hidden="true"></span></div></button>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="row mt-5 justify-content-center">

                            <div class="col-lg-10">
                                <div>
                                    <ul class="nav nav-pills faq-nav-tabs justify-content-center" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link {{ $active == 'Spanish' ? 'active' : '' }}"
                                                role="tab" wire:click="cambiarIdioma('Spanish')">
                                                {{ ___('Spanish') }}
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link {{ $active == 'English' ? 'active' : '' }}"
                                                role="tab" wire:click="cambiarIdioma('English')">
                                                {{ ___('English') }}
                                            </a>
                                        </li>

                                        <li class="nav-item ">
                                            <a class="nav-link {{ $active == 'Portugues' ? 'active' : '' }}"
                                                role="tab" wire:click="cambiarIdioma('Portugues')">
                                                {{ ___('Portugues') }}
                                            </a>
                                        </li>
                                    </ul>
                                    @isset($resultado)
                                        <h3>Se encontraron {{ count($resultado) }} coincidencias</h3>
                                    @endisset
                                    <div id="gen-question-accordion" class="custom-accordion-arrow">
                                        <div class="tab-content pt-5">
                                            <div class="tab-pane fade show active" id="faq-gen-ques" role="tabpanel">
                                                @isset($resultado)
                                                    @php
                                                    $contador = 0;
                                                    @endphp
                                                    @foreach ($resultado as $llave => $frase)
                                                        <div class="card">
                                                            <a href="#gen-question-collapse{{ $contador }}"
                                                                class="text-dark" data-bs-toggle="collapse"
                                                                aria-expanded="true"
                                                                aria-controls="gen-question-collapse{{ $contador }}">
                                                                <div class="card-header"
                                                                    id="gen-question-heading{{ $contador }}">
                                                                    <h5 class="font-size-14 m-0">
                                                                        <i class="mdi mdi-chevron-up accor-arrow-icon"></i>
                                                                        {{ $frase }}
                                                                    </h5>
                                                                </div>
                                                            </a>

                                                            <div id="gen-question-collapse{{ $contador }}"
                                                                class="collapse"
                                                                aria-labelledby="gen-question-heading{{ $contador }}"
                                                                data-bs-parent="#gen-question-accordion">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="app-search row">
                                                                            <div class="col-4">
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="{{ ___('Replace here') }}..."
                                                                                    wire:model.lazy="newWords">
                                                                            </div>


                                                                            <div class="col-4">
                                                                                <button class="btn btn-success btn-rounded waves-effect waves-light"
                                                                                    wire:click="reemplazar('{{ $llave }}')">{{ ___('Replace') }}</button>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        @php
                                                        $contador++;
                                                        @endphp
                                                    @endforeach

                                                @endisset
                                            </div>
                                            

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('footer')
    <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        window.addEventListener('alert', ({
            detail: {
                type,
                message
            }
        }) => {
            Toast.fire({
                icon: type,
                title: message
            })
        })
    </script>
@endpush
