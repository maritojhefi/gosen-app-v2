<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card p-3">
            @php
                $token_name = strtoupper(token('name'));
            @endphp
            <div class="text-center">
                <h4>
                    <strong>
                        {{ ___('Miembros en') }} {{ $token_name }}
                    </strong>
                </h4>
            </div>
            <div class="mt-3 mb-3" style="align-self: center;">
                <div class="d-flex">
                    <div class="flex-1 overflow-hidden" style="font-size: 10px;">
                        @php
                            $array = array_map('intval', str_split(sprintf('%08s', $allUsers->count())));
                            // dd(array_reverse($array));
                            $numero = 0;
                        @endphp
                        <span class="number" data-number="">
                            <span class="primary">
                            </span>
                            <span class="secondary">
                            </span>
                        </span>
                        @for ($i = 1; $i <= strlen(sprintf('%08s', $allUsers->count())); $i++)
                            <span class="number" data-number="{{ $array[$numero] }}">
                                <span class="primary">
                                </span>
                                <span class="secondary">
                                </span>
                            </span>
                            @php
                                $numero++;
                            @endphp
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card p-3">
            @php
                $token_name = strtoupper(token('name'));
            @endphp
            <div class="text-center">
                <h4>
                    <strong>
                        {{ ___('Miembros que se unieron a ') }} {{ $token_name }} {{___('después de ti')}}
                    </strong>
                </h4>
            </div>
            <div class="mt-3 mb-3" style="align-self: center;">
                <div class="d-flex">
                    <div class="flex-1 overflow-hidden" style="font-size: 10px;">
                        @php
                            $array = array_map('intval', str_split(sprintf('%08s', $usuariosDespues->count())));
                            $numero = 0;
                        @endphp
                        <span class="number" data-number="">
                            <span class="primary">
                            </span>
                            <span class="secondary">
                            </span>
                        </span>
                        @for ($i = 1; $i <= strlen(sprintf('%08s', $usuariosDespues->count())); $i++)
                            <span class="number" data-number="{{ $array[$numero] }}">
                                <span class="primary">
                                </span>
                                <span class="secondary">
                                </span>
                            </span>
                            @php
                                $numero++;
                            @endphp
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

    

</div>
