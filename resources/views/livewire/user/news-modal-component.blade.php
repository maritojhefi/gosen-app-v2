<div>
    @if (isset($noticias) && $noticias->count() >= 1)
        @if ($noticias)
            <!-- Modal -->
            <div wire:ignore.self class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content"
                        style="box-shadow:0px 10px 55px 0px rgb(0 0 0 / 0%); background:transparent; border:none">
                        <div class="modal-body">
                            <div class="row">
                                @php
                                    //$colores=['rgb(255,193,0)','rgb(0,210,133)','rgb(27,171,254)','rgb(255,104,104)']
                                    $colores = ['rgb(145,194,200)', 'rgb(118,170,182)', 'rgb(93,143,164)', 'rgb(71,114,140)', 'rgb(51,86,113)'];
                                @endphp
                                @foreach ($noticias as $item)
                                    <div class="col-lg-6">
                                        <article class="post-item col-lg-12 col-sm-12">
                                            <span class="post-item__inner" href="#">
                                                <div class="post-item__thumbnail-wrapper col-lg-12"
                                                    style="position:initial;">
                                                    <div class="row">
                                                        <div class="col-lg-11 col-md-11 col-sm-11"></div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1">
                                                            <button class="blob red"
                                                                data-dismiss="{{ $noticias->count() == 1 ? 'modal' : '' }}"
                                                                type="button"
                                                                style="font-size: 30px; width:30px;height:30px; top:-2px; right:10px; background-color:red; border:none; cursor:pointer; line-height:21px; color:white; z-index:1;position:absolute;"
                                                                class="close"
                                                                wire:click="destroyNotice({{ $item->pivot->id }})">
                                                                <span aria-hidden="true"
                                                                    class='pulse-button'>&times;</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="post-item__thumbnail row col-sm-12">
                                                        @php
                                                            $desc = 12;
                                                        @endphp
                                                        <div class="col-12" style="position:absolute; right:-1px;left:-2px;top:-4px;
                                                        z-index:0;">
                                                            @if ($item->imagenes->count() != 0)
                                                                @if ($item->imagenes->count() > 0)
                                                                    <div id="carouselExampleControls{{ $item->id }}"
                                                                        class="carousel slide" data-ride="carousel"
                                                                        data-interval="3000"
                                                                        style="right:-11px; background-image: url( '{{ asset('assets/images/fondoGosen3.png') }}'); background-size: 300px 300px;">
                                                                        <div class="carousel-inner">
                                                                            @php
                                                                            $contador = 1;
                                                                            @endphp
                                                                            @foreach ($item->imagenes as $foto)
                                                                                <div
                                                                                    class="carousel-item {{ $contador == 1 ? 'active' : '' }}">
                                                                                    <img loading="lazy" alt="First slide" class="rounded m-1 d-block w-100" src="{{ asset2('imagenes/' . $foto->nombre) }}">
                                                                                </div>
                                                                                @php
                                                                                $contador++;
                                                                                @endphp
                                                                            @endforeach
                                                                        </div>
                                                                        @if ($item->imagenes->count() > 1)
                                                                            <a class="carousel-control-prev"
                                                                                href="#carouselExampleControls{{ $item->id }}"
                                                                                role="button" data-slide="prev">
                                                                                <span class="carousel-control-prev-icon"
                                                                                    aria-hidden="true"></span>
                                                                                <span
                                                                                    class="sr-only">Previous</span>
                                                                            </a>
                                                                            <a class="carousel-control-next"
                                                                                href="#carouselExampleControls{{ $item->id }}"
                                                                                role="button" data-slide="next">
                                                                                <span class="carousel-control-next-icon"
                                                                                    aria-hidden="true"></span>
                                                                                <span class="sr-only">Next</span>
                                                                            </a>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                                @else
                                                                    <td>
                                                                        <i class="fa fa-camera"></i>
                                                                        (Without image)
                                                                        <span class="badge badge-primary"></span>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-item__content-wrapper">
                                                    <h2 class="post-item__title heading-size-4"><span
                                                            class="text-white">{{ $item->titulo }}</span></h2>
                                                    <div class="post-item__metas">
                                                        <small style="
                                                                    opacity: 0.5;
                                                                    padding:10px;" class="text-black">Expiration
                                                            date (
                                                            {{ Carbon\Carbon::parse($item->fechaFin)->format('d-M-Y') }}
                                                            )</small>
                                                    </div>
                                                    <div class="text-more">
                                                        <div class="text-more__container" style="text-align: justify;
                                                        text-justify: inter-word; word-break:normal">
                                                            @php
                                                                if (strlen($item->descripcion) >= 150) {
                                                                    $boton = true;
                                                                    $parte2 = substr($item->descripcion, 150);
                                                                    $parte1 = substr($item->descripcion, 0, 150) . '...';
                                                                } else {
                                                                    $boton = false;
                                                                    $parte1 = $item->descripcion;
                                                                    $parte2 = '';
                                                                }
                                                                
                                                            @endphp
                                                            <span
                                                                id="parte1{{ $item->id }}">{!! convertiraLink($parte1) !!}<span
                                                                    id="more{{ $item->id }}"
                                                                    style="display: none;">{!! convertiraLink($parte2) !!}</span></span>
                                                        </div>
                                                        <br>
                                                        @if ($boton)
                                                            <div
                                                                class="wrappe col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <e type="button"
                                                                    onclick="myFunction({{ $item->id }})"
                                                                    id="myBtn{{ $item->id }}" href="#">Read more
                                                                </e>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                </a>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @push('footer')
                <script>
                    $('#exampleModal').modal('show')
                </script>
                <script>
                    window.onload = function() {
                        function myFunction(x) {
                            if (window.matchMedia("(max-width: 768px)").matches) {
                                Livewire.emit('reducir')
                                console.log('holsa')
                            } else {
                                Livewire.emit('aumentar')
                            }
                        }
                        var x = window.matchMedia("(max-width: 700px)")
                        myFunction(x) // Call listener function at run time
                        x.addListener(myFunction) // Attach listener function on state changes
                    };
                </script>
                <script>
                    $('document').ready(function() {
                        Livewire.on('cerrar', function => {
                            $("#exampleModal").modal('hide')
                            const collection = document.getElementsByClassName("modal-backdrop")
                        })
                    });
                </script>
                <script>
                    function myFunction(id) {
                        var parte1 = document.getElementById("parte1" + id);
                        var parte2 = document.getElementById("more" + id);
                        var btnText = document.getElementById("myBtn" + id);
                        if (parte2.style.display === "none") {
                            btnText.innerHTML = "Read less";
                            parte2.style.display = "inline";
                        } else {
                            btnText.innerHTML = "Read more";
                            parte2.style.display = "none";
                        }
                    }
                </script>
            @endpush
        @endif
    @endif
</div>
