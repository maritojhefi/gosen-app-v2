@if ($filtrar != 'transacciones')
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <div class="card">
                    <table class="table">
                        <div class="card-header" style="border-top-left-radius:20px; border-top-right-radius:20px">
                            <div class="button-items">
                                <input type="radio" class="btn-check" wire:model="filtrar" value="todos" id="optionAll"
                                    autocomplete="off" checked>
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="optionAll">{{ ___('All Notifications') }}</label>
                                {{-- <strong>{{ ___('All Notifications') }}</strong> --}}
                                <input type="radio" class="btn-check" wire:model="filtrar" value="asociados"
                                    id="option1" autocomplete="off">
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="option1">{{ ___('Asociados') }}</label>
                                <input type="radio" class="btn-check" wire:model="filtrar" value="ganancias"
                                    id="option2" autocomplete="off">
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="option2">{{ ___('Ganancias') }}</label>
                                <input type="radio" class="btn-check" wire:model="filtrar" value="noticias"
                                    id="option3" autocomplete="off">
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="option3">{{ ___('Noticias') }}</label>
                                <input type="radio" class="btn-check" wire:model="filtrar" value="informacion"
                                    id="option4" autocomplete="off">
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="option4">{{ ___('Información') }}</label>
                                <input type="radio" class="btn-check" wire:model="filtrar" value="transacciones"
                                    id="option5" autocomplete="off">
                                <label class="btn btn-sm btn-primary-notifications"
                                    for="option5">{{ ___('Transacciones') }}</label>
                                <div wire:loading class="spinner-border spinner-border-sm text-primary" role="status">
                                    <span class="sr-only">{{ ___('Loading') }}...</span>
                                </div>
                                @if ($notificaciones->count() > 0)
                                    <a href="javascript:void(0)" class="pull-right text-danger" onclick="showExample()">
                                        {{ ___('Delete All') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        @if (isset($notificaciones) && $notificaciones->count() > 0)
                            <ul class="">
                                @foreach ($notificaciones as $notificacion)
                                    <li class="{{ $notificacion->estado == false ? '' : 'font-weight-bold background-notificacion-no-leida' }} m-3"
                                        style="background: transparent; line-height: 50px; {{ $notificacion->estado == false ? '' : 'border: 1px solid' . asset(theme_color_user(auth()->user()->theme_color, 'base')) . ';' }} border-radius: 15px;">
                                        <a href="javascript:void:void(0)" class="">
                                            <div class="notificacion-large d-none d-lg-block">
                                                <div class="row p-2 pt-0 pb-0"
                                                    style="display: flex; align-items: center;">
                                                    <div
                                                        class="col-4 text-left d-flex justify-content-left align-items-center">
                                                        <a href="javascript:void:void(0)"
                                                            wire:click="redirigir('{{ $notificacion->id }}')"
                                                            class="title">
                                                            <div style="line-height: 14px;">
                                                                <span class="letra-noticacion-no-leida">
                                                                    {{ $notificacion->mensaje }}
                                                                </span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-2 date time-notification d-lg-none d-xl-block">
                                                        <small class="ml-2 ">
                                                            <i class="far fa-clock ml-2 mr-2 letra-noticacion-no-leida"
                                                                style="font-size: 20px">
                                                            </i>{{ App\Helpers\GosenHelper::timeago($notificacion->created_at) }}
                                                        </small>
                                                    </div>
                                                    <div
                                                        class="col-1 date time-notification d-none d-lg-block d-xl-none">
                                                        <small class="ml-2 ">
                                                            <i class="far fa-clock ml-2 mr-2 letra-noticacion-no-leida"
                                                                style="font-size: 20px" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="{{ App\Helpers\GosenHelper::timeago($notificacion->created_at) }}">
                                                            </i>
                                                        </small>
                                                    </div>
                                                    <div class="col-5">
                                                        <div class="row">
                                                            <div class="col-12 truncated-notifications"
                                                                style="line-height: 27px;">
                                                                @if (!empty($notificacion->detalle))
                                                                    <span
                                                                        class="text-truncated-notifications span-text mi-span"
                                                                        id="{{ $notificacion->id }}">
                                                                        {{ $notificacion->detalle }}
                                                                    </span>
                                                                    <button
                                                                        class="btn btn-link btn-truncated-notifications btn-primary-notifications button-mas btn-sm">{{ ___('Ver más') }}</button>
                                                                @else
                                                                    <div class="d-flex justify-content-center">
                                                                        <span class="text-center text-primary">
                                                                            ({{ ___('Sin Detalle') }})
                                                                        </span>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 action-buttons d-lg-none d-xl-block">
                                                        <a wire:click="eliminarNotificacion('{{ $notificacion->id }}')"
                                                            class="ri-delete-bin-line ml-2 mr-2 text-danger letra-blanca"
                                                            style="font-size: 20px;     margin-top: -3px;"></a>
                                                        @if ($notificacion->estado)
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-eye-line ml-2"
                                                                    style="font-size: 20px;"></span></a>
                                                        @else
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-mail-check-line ml-2"
                                                                    style="font-size: 20px;color:rgb(15, 180, 15)"></span></a>
                                                        @endif
                                                    </div>
                                                    <div class="col-2 action-buttons d-none d-lg-block d-xl-none">
                                                        <a wire:click="eliminarNotificacion('{{ $notificacion->id }}')"
                                                            class="ri-delete-bin-line ml-2 mr-2 text-danger letra-blanca"
                                                            style="font-size: 20px;     margin-top: -3px;"></a>
                                                        @if ($notificacion->estado)
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-eye-line ml-2"
                                                                    style="font-size: 20px;"></span></a>
                                                        @else
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-mail-check-line ml-2"
                                                                    style="font-size: 20px;color:rgb(15, 180, 15)"></span></a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="notificacion-small d-block d-lg-none">
                                                <div class="row p-2 pt-0 pb-0 main-{{ $notificacion->id }}">
                                                    <div
                                                        class="col-10 text-left d-flex justify-content-center align-items-center">
                                                        <a href="javascript:void(0);"
                                                            wire:click="redirigir('{{ $notificacion->id }}')"
                                                            class="title letra-blanca">
                                                            <div style="line-height: 14px;"><span
                                                                    class="letra-blanca">
                                                                    {{ $notificacion->mensaje }}
                                                                </span></div>
                                                        </a>
                                                    </div>
                                                    <div class="col-2">
                                                        <span type="button"
                                                            onclick="viewDetails('{{ $notificacion->id }}')"
                                                            class="btn btn-sm btn-primary-notifications view-details"
                                                            style="right: 10px;">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row p-2 pt-0 pb-0 details-{{ $notificacion->id }} d-none">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div
                                                                class="col-12 d-flex justify-content-center align-items-center">
                                                                @if (!empty($notificacion->detalle))
                                                                    @php
                                                                        $texto = $notificacion->detalle;
                                                                        $caracteres = str_word_count($texto);
                                                                        if ($caracteres > 10) {
                                                                            $palabras = explode(' ', $texto); // Divide la cadena en palabras
                                                                            $parte1 = implode(' ', array_slice($palabras, 0, 10)); // Primeras 10 palabras
                                                                            $parte2 = implode(' ', array_slice($palabras, 10));
                                                                        }
                                                                    @endphp
                                                                    @if ($caracteres > 10)
                                                                        <p class=" text-primary mt-3"
                                                                            style="line-height: 15px;">
                                                                            <small>
                                                                                {{ $parte1 }}
                                                                            </small>
                                                                            <small
                                                                                id="2points{{ $notificacion->id }}">...</small><small
                                                                                style="font-size: 11px;"class="text-primary moreText"id="2moreText{{ $notificacion->id }}">{{ $parte2 }}</small><button
                                                                                class=" btn btn-primary-notifications btn-sm ml-2"
                                                                                onclick="toggleTextSm('{{ $notificacion->id }}')"
                                                                                id="2textButton{{ $notificacion->id }}">
                                                                                {{ ___('Más') }}
                                                                            </button>
                                                                        </p>
                                                                    @else
                                                                        <small> {{ $notificacion->detalle }}</small>
                                                                    @endif
                                                                @else
                                                                    <div class="d-flex justify-content-center">
                                                                        <small class="text-center  ">
                                                                            ({{ ___('Sin Detalle') }})</small>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-12 action-buttons d-flex align-items-center justify-content-center gap-5">
                                                        <small class="">
                                                            <i class="far fa-clock ml-2 mr-2 letra-blanca "
                                                                style="font-size: 18px" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="{{ App\Helpers\GosenHelper::timeago($notificacion->created_at) }}">
                                                            </i>
                                                        </small>
                                                        <a wire:click="eliminarNotificacion('{{ $notificacion->id }}')"
                                                            class="ri-delete-bin-line ml-2 mr-2 text-danger letra-blanca"
                                                            style="font-size: 20px;     margin-top: -3px;"></a>
                                                        @if ($notificacion->estado)
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-eye-line ml-2"
                                                                    style="font-size: 20px;"></span></a>
                                                        @else
                                                            <a wire:click="leerNotificacion('{{ $notificacion->id }}')"
                                                                href="javascript:void(0)" class="letra-blanca"><span
                                                                    class="ri-mail-check-line ml-2"
                                                                    style="font-size: 20px; color:rgb(15, 180, 15)"></span></a>
                                                        @endif
                                                        <a href="javascript:void(0)"
                                                            onclick="viewDetails('{{ $notificacion->id }}')"
                                                            class=""><span
                                                                class="ri-arrow-go-back-line ml-2 text-primary"
                                                                style="font-size: 20px;color:rgb(15, 180, 15)"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="row mx-auto d-block m-3">
                                {{ $notificaciones->links() }}
                            </div>
                        @else
                            <div class="row mx-auto d-block m-3">
                                <small class="letra-blanca">{{ ___('No tienes ninguna notificación') }}</small>
                            </div>
                        @endif
                        <!-- end Col-9 -->
                    </table>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="border-radius:20px;">
                    <div class="button-items">
                        <input type="radio" class="btn-check" wire:model="filtrar" value="todos" id="optionAll"
                            autocomplete="off" checked>
                        <label class="btn btn-sm btn-primary-notifications"
                            for="optionAll">{{ ___('All Notifications') }}</label>
                        {{-- <strong>{{ ___('All Notifications') }}</strong> --}}
                        <input type="radio" class="btn-check" wire:model="filtrar" value="asociados"
                            id="option1" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="option1">{{ ___('Asociados') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtrar" value="ganancias"
                            id="option2" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="option2">{{ ___('Ganancias') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtrar" value="noticias"
                            id="option3" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="option3">{{ ___('Noticias') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtrar" value="informacion"
                            id="option4" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="option4">{{ ___('Información') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtrar" value="transacciones"
                            id="option5" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="option5">{{ ___('Transacciones') }}</label>
                        <div wire:loading class="spinner-border spinner-border-sm text-primary" role="status">
                            <span class="sr-only">{{ ___('Loading') }}...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12 col-md-6"></div>
        <div class="col-lg-6 col-sm-12 col-md-6">
            <div class="card" style="width: fit-content;">
                <div class="card-header" style="border-radius:20px;">
                    <div class="button-items center-block" style="text-align:center;">
                        <input type="radio" class="btn-check" wire:model="filtroTransacciones" value="todos"
                            id="transAll" autocomplete="off" checked>
                        <label class="btn btn-sm btn-primary-notifications"
                            for="transAll">{{ ___('All Transactions') }}</label>
                        {{-- <strong>{{ ___('All Notifications') }}</strong> --}}
                        <input type="radio" class="btn-check" wire:model="filtroTransacciones" value="approved"
                            id="trans1" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="trans1">{{ ___('Approved') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtroTransacciones" value="pending"
                            id="trans2" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="trans2">{{ ___('Pending') }}</label>
                        <input type="radio" class="btn-check" wire:model="filtroTransacciones" value="canceled"
                            id="trans3" autocomplete="off">
                        <label class="btn btn-sm btn-primary-notifications"
                            for="trans3">{{ ___('Canceled') }}</label>
                    </div>
                </div>
            </div>
        </div>
        @foreach ($transacciones as $transaccion)
            @if (
                $transaccion->status == 'canceled' &&
                    \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($transaccion->created_at)) >= 1)
                @continue
            @endif
            <div class="col-xl-3 col-sm-6">
                <div class="card p-3">
                    <div class="row">
                        @php
                            $json = json_decode($transaccion->extra);
                            $url = '#';
                            try {
                                if ($transaccion->payment_method == 'coinpayments') {
                                    $url = $json->result->checkout_url;
                                } elseif ($transaccion->payment_method == 'stripe') {
                                    $url = $json->info[0]->url;
                                }
                            } catch (\Throwable $th) {
                            }
                            
                        @endphp
                        <div class="dropdown ">
                            <a href="javascript:void(0)" class=" " data-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical m-0 text-muted font-size-20"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" style="">
                                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal"
                                    wire:click="selectTrans({{ $transaccion->id }})"
                                    data-target="#modalDetalleTransaccion">{{ ___('Ver detalles') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="text-center">
                            <img loading="lazy" class="moonPulse"
                                src="{{ asset('imagenes/logos/' . strtoupper($transaccion->receive_currency) . '.png') }}"
                                alt="" class="avatar-sm mt-2 mb-4">
                            <div class="media-body">
                                <h5 class="text-truncate"><a href="javascript:void(0)"
                                        class="text-dark">{{ $transaccion->receive_currency }}</a></h5>
                                <p class="text-muted">
                                    @switch($transaccion->status)
                                        @case('new')
                                            <span class="badge badge-primary font-size-15 v-align-middle p-1"><i
                                                    class="fa fa-smile "> </i> {{ ___($transaccion->status) }}</span>
                                        @break

                                        @case('canceled')
                                            <span class="badge badge-danger font-size-15 v-align-middle p-1"><i
                                                    class="fa fa-close "> </i> {{ ___($transaccion->status) }}</span>
                                        @break

                                        @case('approved')
                                            <span class="badge badge-success font-size-15 v-align-middle p-1"><i
                                                    class="fa fa-check "> </i> {{ ___($transaccion->status) }}</span>
                                        @break

                                        @case('pending')
                                            <span class="badge badge-warning font-size-15 v-align-middle p-1"><i
                                                    class="fa fa-timer "> </i> {{ ___($transaccion->status) }}</span>
                                        @break

                                        @case('onhold')
                                            <span class="badge badge-info font-size-15 v-align-middle p-1"><i
                                                    class="fa fa-timer "> </i> {{ ___($transaccion->status) }}</span>
                                        @break

                                        @default
                                    @endswitch
                                </p>
                                <small
                                    class="text-center">{{ App\Helpers\GosenHelper::timeago($transaccion->created_at) }}</small>
                            </div>
                        </div>
                        <hr class="">
                        <div class="row text-center">
                            <div class="col-6">
                                <p class="text-muted mb-2">{{ ___('Package') }} #{{ $transaccion->package }}</p>
                                <span>{{ $transaccion->tokens . ' ' . token_symbol() }} </span>
                            </div>
                            <div class="col-6">
                                <p class="text-muted mb-2">{{ ___('Payment Method') }}</p>
                                @if ($transaccion->payment_method == 'coinpayments' && $transaccion->status == 'pending')
                                    <span> <a href="{{ $url }}" data-toggle="modal"
                                            data-target="#modalPagoCoinPayment{{ $transaccion->id }}"
                                            class="badge badge-info badge-lg outline badge-outline" target="_blank">
                                            {{ ___('Pagar') }} <i class="fa fa-location-arrow"></i>
                                        </a></span>
                                @elseif($transaccion->payment_method == 'stripe' && $transaccion->status == 'pending')
                                    <span> <a href="{{ $url }}"
                                            class="badge badge-success badge-lg outline badge-outline"
                                            target="_blank">
                                            {{ ___('Pagar en') }} {{ $transaccion->payment_method }} <i
                                                class="fa fa-location-arrow"></i>
                                        </a></span>
                                @else
                                    <span>{{ $transaccion->payment_method }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        @endforeach
    </div>
@endif
@push('modals')
    <div wire:ignore.self class="modal fade" id="modalDetalleTransaccion" tabindex="-1" role="dialog"
        aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Transaccion # 34</h5>
                    <div wire:loading class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                    <th style="width: 50%">{{ ___('Numero de transaccion') }}</th>
                                    <td id="numeroTransaccion"></td>
                                </tr>
                                <tr>
                                    <th style="width: 50%">{{ ___('Fecha de creacion') }}</th>
                                    <td id="fechaTransaccion"></td>
                                </tr>
                                <tr>
                                    <th style="width: 50%">{{ ___('Monto recibido') }}</th>
                                    <td id="nivelTransaccion"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush
@push('footer')
    @php
        $variable = 0;
    @endphp
    @foreach ($transacciones as $transaccion)
        @php
            $data = json_decode($transaccion->extra);
            $cur = strtolower($transaccion->currency);
            $_CUR = strtoupper($cur);
            $_gateway = ucfirst($transaccion->payment_method);
            $variable++;
        @endphp
        <div class="modal fade" id="modalPagoCoinPayment{{ $transaccion->id }}" tabindex="-1" role="dialog"
            aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
                <div class="modal-content">
                    <a href="javascript:void(0)" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                            class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <h4 class="popup-title">{{ ___('Confirmación de pago') }}</h4>
                        <div class="content-area popup-content">
                            <p class="lead-lg text-primary">{{ ___('Tu pedido') }} #
                                <strong>{{ $transaccion->tnx_id }}</strong>
                                {{ ___('ha sido colocado y esperando el pago') }}.
                            </p>
                            <p class="lead">{{ ___('Para recibir') }} <strong><span
                                        class="token-total">{{ $transaccion->tokens }}<span></span></span></strong>
                                <strong class="text-primary">{{ token_symbol() }}</strong>,
                                {{ ___('realice su pago de') }} <strong
                                    class="text-primary">{{ $transaccion->amount }}</strong> <strong
                                    class="text-primary">{{ strtoupper($transaccion->currency) }}</strong>
                                {{ ___('a') }}
                                <strong>Coinpayments</strong>.
                                {{ ___('El saldo del token aparecerá en su cuenta una vez que recibamos su pago') }}.
                            </p>
                            <div class="gaps-0-5x"></div>
                            <div class="pay-wallet-address pay-wallet-bnb">
                                <h6 class="text-head font-bold">{{ ___('Make your payment to the below address') }}</h6>
                                <div class="row guttar-1px guttar-vr-15px">
                                    @if (isset($data->result->qrcode_url))
                                        <div class="col-sm-3">
                                            <p class="text-center text-sm-left"><img
                                                    title="{{ __('Scan QR code to payment') }}" class="img-thumbnail"
                                                    width="120" src="{{ $data->result->qrcode_url }}" alt="QR">
                                            </p>
                                        </div>
                                    @endif
                                    <div class="col-sm-9">
                                        <div class="fake-class pl-sm-3">
                                            @if (isset($data->result->address))
                                                <div class="copy-wrap mgb-0-5x">
                                                    <span class="copy-feedback"></span>
                                                    <em class="copy-icon ikon ikon-sign-{{ $cur }}"></em>
                                                    <input type="text" class="copy-address ignore"
                                                        value="{{ $data->result->address }}" disabled=""
                                                        readonly="" id="valor-direccion-{{ $variable }}">
                                                    <button type="button" class="copy-trigger copy-clipboard"
                                                        id="buttonCopiarDirecion-{{ $variable }}"><em
                                                            class="ti ti-files"></em></button>
                                                </div>
                                                <div class="gaps-2x"></div>
                                            @endif
                                            <div class="row">
                                                <div
                                                    class="col-6 text-center d-flex align-items-center justify-content-center">
                                                    <ul class="d-flex flex-wrap align-items-center guttar-30px">
                                                        @if (isset($data->result->status_url))
                                                            <li><a href="{{ $data->result->status_url }}"
                                                                    target="_blank"
                                                                    class="btn btn-primary-notifications">{{ ___('Check Status') }}</a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div
                                                    class="col-6 text-center d-flex align-items-center justify-content-center">
                                                    <p class="mt-2"><a class="link"
                                                            href="{{ route('payment.coinpayments.success', ['tnx_id' => $transaccion->id]) }}">{{ ___('Click here if you already paid') }}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <script>
        Livewire.on('modal-data', data => {
            $('#numeroTransaccion').html(data.tnx_id);
            $('#fechaTransaccion').html(data.fecha);
            $('#nivelTransaccion').html(data.receive_amount);
        })
    </script>
    <script>
        $(document).ready(function() {
            // script de copiar enlace de referido desde el layout
            $(".copy-trigger").click(function() {
                var elementId = $(this).attr("id");
                var inputId = "valor-direccion-" + elementId.split("-")[1];
                var valorCopiado = $("#" + inputId).val();
                navigator.clipboard.writeText(valorCopiado)
                    .then(function() {
                        // $(".copiado").removeClass('d-none');
                        // $(".no-copiado").addClass('d-none');
                        toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                        // setTimeout(function() {
                        //     $(".copiado").addClass('d-none');
                        //     $(".no-copiado").removeClass('d-none');
                        // }, 1000)
                    })
                    .catch(function(err) {
                        toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                    });
            });
        });
    </script>
    <script>
        function toggleTextLg(id) {

            // Get all the elements from the page
            var points =
                document.getElementById("points" + id);

            var showMoreText =
                document.getElementById("moreText" + id);

            var buttonText =
                document.getElementById("textButton" + id);

            // If the display property of the dots
            // to be displayed is already set to
            // 'none' (that is hidden) then this
            // section of code triggers
            if (points.style.display === "none") {

                // Hide the text between the span
                // elements
                showMoreText.style.display = "none";

                // Show the dots after the text
                points.style.display = "inline";

                // Change the text on button to
                // 'Show More'
                buttonText.innerHTML = "{{ ___('Más') }}";
            }

            // If the hidden portion is revealed,
            // we will change it back to be hidden
            else {

                // Show the text between the
                // span elements
                showMoreText.style.display = "inline";

                // Hide the dots after the text
                points.style.display = "none";

                // Change the text on button
                // to 'Show Less'
                buttonText.innerHTML = "{{ ___('Menos') }}";
            }
        }

        function toggleTextSm(id) {
            console.log(id)
            // Get all the elements from the page
            var points =
                document.getElementById("2points" + id);

            var showMoreText =
                document.getElementById("2moreText" + id);

            var buttonText =
                document.getElementById("2textButton" + id);

            // If the display property of the dots
            // to be displayed is already set to
            // 'none' (that is hidden) then this
            // section of code triggers
            if (points.style.display === "none") {

                // Hide the text between the span
                // elements
                showMoreText.style.display = "none";

                // Show the dots after the text
                points.style.display = "inline";

                // Change the text on button to
                // 'Show More'
                buttonText.innerHTML = "{{ ___('Más') }}";
            }

            // If the hidden portion is revealed,
            // we will change it back to be hidden
            else {

                // Show the text between the
                // span elements
                showMoreText.style.display = "inline";

                // Hide the dots after the text
                points.style.display = "none";

                // Change the text on button
                // to 'Show Less'
                buttonText.innerHTML = "{{ ___('Menos') }}";
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        function viewDetails(id) {
            var main = $('.main-' + id);
            var details = $('.details-' + id);
            if (details.hasClass('d-none')) {
                main.addClass('d-none');
                details.removeClass('d-none');
            } else {
                details.addClass('d-none');
                main.removeClass('d-none');
            }
        }

        function showExample() {
            Swal.fire({
                text: "{{ ___('Estás seguro de eliminar todas tus notificaciones') }}",
                target: '#custom-target',
                customClass: {
                    container: 'position-absolute'
                },
                toast: true,
                position: 'bottom-right',
                showCancelButton: true,
                confirmButtonColor: "{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}",
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit('borrarNotificaciones');
                }
            })
        }
    </script>
    <script>
        $('.btn-truncated-notifications').click(function(e) {
            var $trigger = $(this);
            $trigger.parent().toggleClass('truncated-notifications');
            var text = $trigger.text();
            $trigger.text(
                text == "{{ ___('Ver más') }}" ?
                "{{ ___('Ver menos') }}" :
                "{{ ___('Ver más') }}"
            );
        })
    </script>
    @include('partials.livewire-sweetalert')
@endpush
