<div>
    @include('user.includes.events')
    <div id="popup" class="popup">

        {{-- <div target="_blank"> --}}
        {{-- @foreach ($mensajes as $mensaje)
            @if ($mensaje->estado)
                <div class="notify align-items-end" id="{{ $mensaje->id }}"
                    wire:click="leerNotificacion('{{ $mensaje->id }}')">
                    <div class="row">
                        <div class="col-9"></div>
                        &nbsp;
                        <div class="col-2 close">
                            <em class="ti ti-close"></em>
                        </div>
                    </div>
                    <div target="_blank" href="#" wire:click="leerNotificacion('{{ $mensaje->id }}')"
                        class="promo-content-wrap">
                        <div class="row">
                            <div class="col-1 pr-0"><span
                                    class="avatar-title bg-{{ $mensaje->color }} rounded-circle font-size-12" style="height: 40%;
                                    width: 100%;">
                                    <i class="{{ $mensaje->icono }}  fa-move font-size-12"></i>
                                </span></div>
                            <div class="col-11">
                                <div class="promo-content-text">
                                    <h5>{{ ___($mensaje->mensaje) }}</h5>
                                    <p>{{ ___($mensaje->detalle) }}</p>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            @endif
        @endforeach --}}
        {{-- @foreach ($eventos as $eve)
            @if ($loop->iteration <= 3)
                @if ($eve->image != null)
                    <div class="notify align-items-end" id="{{ $eve->pivot->id }}"
                        wire:click="eliminarEvento('{{ $eve->pivot->id }}')">
                        <div class="row">
                            <div class="col-9"></div>
                            &nbsp;
                            <div class="col-2 close">
                                <em class="ti ti-close"></em>
                            </div>
                        </div>
                        <div target="_blank" href="" class="promo-content-wrap">
                            <div class="promo-content-img">
                                <img loading="lazy" src="{{ asset2('imagenes/' . $eve->image) }}" alt="GosenLite" class="imge">
                            </div>
                            <div class="promo-content-text">
                                <h5>{{ ___($eve->body) }}</h5>
                                <p>{{ ___($eve->footer) }}</p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="notify">
                        <div target="_blank" href="" class="promo-content-wrap">
                            <div class="promo-content-text">
                                <h5>{{ ___($eve->body) }}</h5>
                                <p>{{ ___($eve->footer) }}</p>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach --}}

        {{-- </div> --}}

    </div>
</div>

@push('footer')
    <script>
        $(document).ready(function() {
            // Livewire.on('abrir', mensaje => {
            //     var ob = document.getElementById("popup");
            //     ob.classList.add("active");
            //     $("#popup").fadeIn();
            // })
            Livewire.on('new-notification', data => {
                if(data.color=="danger")
                {
                    show_toast("error", data.mensaje+'\n <br>'+data.detalle,data.icono_ti);
                }
                else if(data.color=="primary")
                {
                    show_toast("info", data.mensaje+'\n <br>'+data.detalle,data.icono_ti);
                }
                else
                {
                    show_toast(''+data.color, data.mensaje+'\n <br>'+data.detalle,data.icono_ti);
                    
                }
                
                
            })
            // setTimeout(function() {

            //     var ob = document.getElementById("popup");
            //     ob.classList.add("hidde");
            //     $("#popup").fadeOut();

            // }, 12000);

            // Livewire.on('cerrarPopup', mensaje => {

            //     setTimeout(function() {

            //         var ob = document.getElementById("popup");
            //         ob.classList.add("hidde");
            //         $("#popup").fadeOut();

            //     }, 7000);
            // })
        });
    </script>
@endpush
