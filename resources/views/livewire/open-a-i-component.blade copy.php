@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.min.css">
    <style>
        .centro {
            display: flex;
            justify-content: center;
            align-items: center;

        }

        .inp {
            border: none;
            height: 38px;
            padding-left: 40px;
            padding-right: 20px;
            background-color: #f1f5f7;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 30px;
        }

        /*estilos del input de busqueda*/
        .search-box {
            position: absolute;
            width: 100%;
        }

        .input-search {
            height: 50px;
            width: 100%;
            border-style: none;
            padding: 10px;
            outline: none;
            border-radius: 25px;
            transition: all .5s ease-in-out;
            background-color: {{ auth()->user()->theme_color == 'style-black' ? '#fff' : asset(theme_color_user(auth()->user()->theme_color, 'back')) }};
            padding-right: 40px;
            color: {{ auth()->user()->theme_color == 'style-black' ? 'white' : 'black' }};
        }

        .input-search::placeholder {
            color: #ffffff;
            font-weight: 100;
        }

        .btn-search {
            width: 50px;
            height: 50px;
            border-style: none;
            font-size: 20px;
            font-weight: bold;
            outline: none;
            cursor: pointer;
            border-radius: 50%;
            position: absolute;
            right: 0px;
            color: #ffffff;
            background-color: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            pointer-events: painted;
            border-color: black;
        }

        .btn-search:focus~.input-search {
            width: 100%;
            border-radius: 0px;
            background-color: transparent;
            border-bottom: 1px solid rgba(255, 255, 255, .5);
            transition: all 500ms cubic-bezier(0, 0.110, 0.35, 2);
        }

        .input-search:focus {
            width: 100%;
            border-radius: 0px;
            background-color: transparent;
            border-bottom: 1px solid rgba(255, 255, 255, .5);
            transition: all 500ms cubic-bezier(0, 0.110, 0.35, 2);
        }


        /*otros estilos*/
        .blob {
            transform: scale(1);
            animation: pulse-black 1s infinite;
        }

        @keyframes pulse-black {
            0% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }};
            }

            70% {
                transform: scale(1);
                box-shadow: 0 0 0 10px {{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }};
            }

            100% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }};
            }
        }

        /*style para el carrusel de nft*/
        .owl-theme .owl-dots,
        .owl-theme .owl-nav {
            text-align: center;
            -webkit-tap-highlight-color: transparent
        }

        .owl-carousel {
            height: 180px;
            width: 100%;
            z-index: 1;
        }

        .owl-theme .owl-nav [class*=owl-] {
            color: #FFF;
            font-size: 14px;
            margin: 5px;
            padding: 4px 7px;
            background: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            display: inline-block;
            cursor: pointer;
            border-radius: 30px;
        }

        .owl-theme .owl-nav [class*=owl-]:hover {
            background: #869791;
            color: #FFF;
            text-decoration: none
        }

        .owl-theme .owl-nav .disabled {
            opacity: .5;
            cursor: default
        }



        .owl-theme .owl-dots .owl-dot {
            display: inline-block;
            zoom: 1
        }

        .owl-theme .owl-dots .owl-dot span {
            width: 10px;
            height: 10px;
            margin: 5px 7px;
            background: #D6D6D6;
            display: block;
            -webkit-backface-visibility: visible;
            transition: opacity .2s ease;
            border-radius: 30px
        }

        .owl-theme .owl-dots .owl-dot.active span,
        .owl-theme .owl-dots .owl-dot:hover span {
            background: #869791
        }



        .animated {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-delay: 500ms;
            animation-delay: 500ms;
        }

        .animate-out {
            -webkit-animation-delay: 0ms;
            animation-delay: 0ms;
        }

        .h4-owl {
            font-size: 28px;
        }

        .p-owl {
            width: 50%;
            text-align: center;
            margin: 0 auto 20px;
        }

        .owl-item {
            display: table;
        }

        .owl-carousel .item {
            height: 0;
            padding-bottom: 10%;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            border-radius: 20px;
        }

        .btn-owl {
            display: inline-block;
            line-height: 35px;
            height: 35px;
            text-align: center;
            padding: 0 20px;
            width: auto;
            background-color: #000;
            text-decoration: none;
            color: #fff;
        }

        @media (max-width: 576px) {
            .owl-carousel .item {
                height: 0;
                padding-bottom: 0%;
                vertical-align: bottom;
                margin-bottom: 0px !important;
                height: 200px !important;
                gap: 1 !important;
            }
        }




        /*Owl Animation*/
        .owl-item {
            overflow: hidden;
        }

        .fxSwipe .owl-stage,
        .fxPushReveal .owl-stage,
        .fxSnapIn .owl-stage,
        .fxLetMeIn .owl-stage,
        .fxStickIt .owl-stage,
        .fxSlideBehind .owl-stage {
            transform: none !important;
            width: 100% !important;
            position: relative;
            height: 0;
            border: none;
            overflow: hidden;
            display: block;
        }

        .fxSwipe .owl-item,
        .fxPushReveal .owl-item,
        .fxSnapIn .owl-item,
        .fxLetMeIn .owl-item,
        .fxStickIt .owl-item,
        .fxSlideBehind .owl-item {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0 !important;
            left: 0 !important;
            opacity: 0;
            z-index: 10;
        }

        .fxSwipe .owl-item.active,
        .fxPushReveal .owl-item.active,
        .fxSnapIn .owl-item.active,
        .fxLetMeIn .owl-item.active,
        .fxStickIt .owl-item.active,
        .fxSlideBehind .owl-item.active {
            z-index: 20;
            opacity: 1;
        }

        .fxSwipe .owl-item.owl-animated-out,
        .fxPushReveal .owl-item.owl-animated-out,
        .fxSnapIn .owl-item.owl-animated-out,
        .fxLetMeIn .owl-item.owl-animated-out,
        .fxStickIt .owl-item.owl-animated-out,
        .fxSlideBehind .owl-item.owl-animated-out {
            opacity: 1;
        }

        .fxSwipe .owl-item.owl-animated-in,
        .fxPushReveal .owl-item.owl-animated-in,
        .fxSnapIn .owl-item.owl-animated-in,
        .fxLetMeIn .owl-item.owl-animated-in,
        .fxStickIt .owl-item.owl-animated-in,
        .fxSlideBehind .owl-item.owl-animated-in {
            opacity: 0;
        }

        /*****************************************/
        /* Soft Scale */
        /*****************************************/
        .fxSoftScale .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxSoftScaleOutNext {
            animation-name: scaleUp;
        }

        .fxSoftScaleInNext {
            animation-name: scaleDownUp;
        }

        .fxSoftScaleOutPrev {
            animation-name: scaleDown;
        }

        .fxSoftScaleInPrev {
            animation-name: scaleUpDown;
        }

        @keyframes scaleUp {
            from {
                opacity: 1;
            }

            to {
                transform: scale(1.2);
                opacity: 0;
            }
        }

        @keyframes scaleDownUp {
            from {
                opacity: 0;
                transform: scale(0.9);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        @keyframes scaleDown {
            to {
                opacity: 0;
                transform: scale(0.9);
            }
        }

        @keyframes scaleUpDown {
            from {
                transform: scale(1.2);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Press away */
        /*****************************************/
        .fxPressAway .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxPressAwayOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxPressAwayInNext {
            animation-name: slideInFromLeft;
        }

        .fxPressAwayOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxPressAwayInPrev {
            animation-name: slideInFromRight;
        }

        @keyframes slideOutScaleRight {
            to {
                transform: translateX(100%) scale(0.9);
                opacity: 0;
            }
        }

        @keyframes slideInFromLeft {
            from {
                transform: translateX(-100%);
            }

            to {
                transform: translateX(0);
            }
        }

        @keyframes slideOutScaleLeft {
            to {
                transform: translateX(-100%) scale(0.9);
                opacity: 0;
            }
        }

        @keyframes slideInFromRight {
            from {
                transform: translateX(100%);
            }

            to {
                transform: translateX(0);
            }
        }

        /*****************************************/
        /* Slide Swing */
        /*****************************************/
        .fxSideSwing .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(1, -0.2, 0, 1);
        }

        .fxSideSwingOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxSideSwingInNext {
            animation-name: slideInFromLeft;
        }

        .fxSideSwingOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxSideSwingInPrev {
            animation-name: slideInFromRight;
        }

        /*****************************************/
        /* Fortune wheel */
        /*****************************************/
        .fxFortuneWheel .owl-stage {
            perspective: 1600px;
        }

        .fxFortuneWheel .animated {
            animation-duration: 1s;
            animation-fill-mode: forwards;
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxFortuneWheelOutNext {
            animation-name: slideOutScaleRight;
        }

        .fxFortuneWheelInNext {
            transform-origin: 100% 50%;
            animation-name: rotateInFromLeft;
        }

        .fxFortuneWheelOutPrev {
            animation-name: slideOutScaleLeft;
        }

        .fxFortuneWheelInPrev {
            transform-origin: 0% 50%;
            animation-name: rotateInFromRight;
        }

        @keyframes rotateInFromLeft {
            from {
                transform: translateX(-100%) rotateY(-55deg);
            }

            to {
                transform: translateX(0) rotateY(0deg);
                opacity: 1;
            }
        }

        @keyframes rotateInFromRight {
            from {
                transform: translateX(100%) rotateY(55deg);
            }

            to {
                transform: translateX(0) rotateY(0deg);
                opacity: 1;
            }
        }

        /*****************************************/
        /* Swipe */
        /*****************************************/
        .fxSwipe .owl-item.fxSwipeOutNext {
            z-index: 30;
        }

        .fxSwipe .owl-item.fxSwipeInPrev {
            opacity: 1;
        }

        .fxSwipeOutNext {
            animation: decreaseHeight 0.8s forwards ease-in-out;
        }

        .fxSwipeInNext {
            animation: show 0.8s forwards ease-in-out;
        }

        .fxSwipeOutPrev {
            animation: hide 0.8s forwards ease-in-out;
        }

        .fxSwipeInPrev {
            animation: increaseHeight 0.8s forwards ease-in-out;
        }

        @keyframes increaseHeight {
            from {
                height: 0;
            }

            to {
                height: 100%;
            }
        }

        @keyframes decreaseHeight {
            to {
                height: 0;
            }
        }

        @keyframes show {
            0% {
                opacity: 0;
            }

            1%,
            100% {
                opacity: 1;
            }
        }

        @keyframes hide {

            0%,
            99% {
                opacity: 1;
            }

            100% {
                opacity: 0;
            }
        }

        /*****************************************/
        /* Push reveal */
        /*****************************************/
        .fxPushReveal .owl-item.animated {
            opacity: 1;
            animation-duration: 0.7s;
            animation-fill-mode: forwards;
            animation-timing-function: ease-in-out;
        }

        .fxPushRevealOutNext {
            animation-name: slideOutBottom;
        }

        .fxPushRevealInNext {
            animation-name: slideInHalfFromTop;
        }

        .fxPushReveal .fxPushRevealInNext.owl-item {
            z-index: 5;
        }

        .fxPushRevealOutPrev {
            animation-name: slideOutHalfTop;
        }

        .fxPushRevealInPrev {
            animation-name: slideInFromBottom;
        }

        @keyframes slideOutBottom {
            to {
                transform: translateY(100%);
            }
        }

        @keyframes slideInHalfFromTop {
            from {
                transform: translateY(-50%);
            }

            to {
                transform: translateY(0);
            }
        }

        @keyframes slideOutHalfTop {
            to {
                transform: translateY(-50%);
            }
        }

        @keyframes slideInFromBottom {
            from {
                transform: translateY(100%);
            }

            to {
                transform: translateY(0);
            }
        }

        /*****************************************/
        /* Snap in */
        /*****************************************/
        .fxSnapIn .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.8);
            transition: opacity 0.4s 0.1s ease-in;
        }

        .fxSnapIn .owl-item.owl-animated-out:after {
            opacity: 1;
        }

        .fxSnapIn .owl-item.owl-animated-in {
            opacity: 1;
        }

        .fxSnapIn .animated {
            animation-duration: 0.5s;
            animation-fill-mode: forwards;
        }

        .fxSnapIn .owl-animated-in {
            animation-timing-function: ease-in;
        }

        .fxSnapIn .owl-animated-out {
            animation-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxSnapInOutNext {
            animation-name: slideOutLeft;
        }

        .fxSnapInInNext {
            animation-name: slideFromRightFast;
        }

        .fxSnapInOutPrev {
            animation-name: slideOutRight;
        }

        .fxSnapInInPrev {
            animation-name: slideFromLeftFast;
        }

        @keyframes slideOutLeft {
            to {
                transform: translateX(-10%);
            }
        }

        @keyframes slideFromRightFast {

            0%,
            50% {
                transform: translateX(100%);
            }

            100% {
                transform: translateX(0%);
            }
        }

        @keyframes slideOutRight {
            to {
                transform: translateX(10%);
            }
        }

        @keyframes slideFromLeftFast {

            0%,
            50% {
                transform: translateX(-100%);
            }

            100% {
                transform: translateX(0%);
            }
        }

        /*****************************************/
        /* Let me in */
        /*****************************************/
        .fxLetMeIn .owl-stage {
            perspective: 1600px;
        }

        .fxLetMeIn .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.6);
            transition: opacity 0.5s ease-in-out;
        }

        .fxLetMeIn .owl-item.fxLetMeInOutNext:after,
        .fxLetMeIn .owl-item.fxLetMeInOutPrev:after {
            opacity: 1;
        }

        .fxLetMeIn .owl-item.fxLetMeInInNext,
        .fxLetMeIn .owl-item.fxLetMeInInPrev {
            z-index: 30;
            opacity: 1;
        }

        .fxLetMeInOutNext {
            transform-origin: 0% 50%;
            animation: rotateOutRight 0.5s forwards ease-in-out;
        }

        .fxLetMeInInNext {
            animation: slideFromRightFast 0.5s forwards ease;
        }

        .fxLetMeInOutPrev {
            transform-origin: 100% 0%;
            animation: rotateOutLeft 0.5s forwards ease-in-out;
        }

        .fxLetMeInInPrev {
            animation: slideFromLeftFast 0.5s forwards ease;
        }

        @keyframes rotateOutRight {
            to {
                transform: rotateY(10deg);
            }
        }

        @keyframes rotateOutLeft {
            to {
                transform: rotateY(-10deg);
            }
        }

        /*****************************************/
        /* Stick it */
        /*****************************************/
        .fxStickIt .owl-stage {
            perspective: 1600px;
        }

        .fxStickIt .owl-item.fxStickItInNext {
            opacity: 1;
        }

        .fxStickItOutNext {
            transform-origin: 50% 0%;
            animation: rotateBottomSideOut 0.8s forwards ease-in;
        }

        .fxStickItInNext {
            animation: slideInFromBottomDelayed 0.8s forwards;
        }

        .fxStickItOutPrev {
            opacity: 1;
            animation: slideOutToBottom 0.8s forwards;
        }

        .fxStickItInPrev {
            transform-origin: 50% 0%;
            animation: rotateBottomSideIn 0.8s 0.1s forwards ease-in;
        }

        @keyframes rotateBottomSideOut {
            40% {
                transform: rotateX(-15deg);
                animation-timing-function: ease-out;
            }

            100% {
                opacity: 0;
                transform: scale(0.8) translateZ(-200px);
            }
        }

        @keyframes slideInFromBottomDelayed {

            0%,
            30% {
                transform: translateY(100%);
            }

            100% {
                transform: translateY(0);
            }
        }

        @keyframes rotateBottomSideIn {
            0% {
                opacity: 0;
                transform: scale(0.8) translateZ(-200px);
            }

            60% {
                transform: scale(1) translateZ(0) rotateX(-15deg);
                animation-timing-function: ease-out;
            }

            100% {
                opacity: 1;
                transform: scale(1) translateZ(0) rotateX(0deg);
            }
        }

        /*****************************************/
        /* Archive me */
        /*****************************************/
        .fxArchiveMe .owl-item:before,
        .fxArchiveMe .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.7);
            transition: opacity 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMe .owl-item.fxArchiveMeInNext {
            z-index: 30;
            opacity: 1;
        }

        .fxArchiveMe .owl-item.fxArchiveMeInNext:after {
            transition: none;
        }

        .fxArchiveMe .owl-item:after,
        .fxArchiveMe .owl-item.fxArchiveMeOutNext:before {
            opacity: 1;
        }

        .fxArchiveMe .owl-item:before,
        .fxArchiveMe .owl-item.active:after,
        .fxArchiveMe .owl-item.fxArchiveMeInNext:after,
        .fxArchiveMe .owl-item.fxArchiveMeInPrev:after {
            opacity: 0;
        }

        .fxArchiveMeOutNext {
            animation: scaleHalfDown 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeInNext {
            animation: slideInFromBottom 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeOutPrev {
            animation: slideOutToBottom 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxArchiveMeInPrev {
            animation: scaleHalfUp 0.7s cubic-bezier(0.7, 0, 0.3, 1);
        }

        @keyframes scaleHalfDown {
            to {
                transform: scale(0.6);
                opacity: 0;
            }
        }

        @keyframes slideOutToBottom {
            to {
                transform: translateY(100%);
            }
        }

        @keyframes scaleHalfUp {
            from {
                opacity: 0;
                transform: scale(0.6);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Vertical growth */
        /*****************************************/
        .fxVGrowth .owl-item.fxVGrowthInNext,
        .fxVGrowth .owl-item.fxVGrowthInPrev {
            z-index: 30;
            opacity: 1;
        }

        .fxVGrowthOutNext {
            animation: scaleDown 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthInNext {
            transform-origin: 50% 100%;
            animation: maximize 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthOutPrev {
            animation: scaleDown 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        .fxVGrowthInPrev {
            transform-origin: 50% 0%;
            animation: maximize 0.6s forwards cubic-bezier(0.6, 0, 0.4, 1);
        }

        @keyframes maximize {
            from {
                transform: scale(0);
            }

            to {
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Slide Behind */
        /* From https://github.com/hakimel/kontext by Hakim El Hattab, http://hakim.se */
        /*****************************************/
        .fxSlideBehind .owl-stage {
            perspective: 1000px;
        }

        .fxSlideBehindOutNext {
            animation: hideLeft 0.8s forwards;
        }

        .fxSlideBehindInNext {
            animation: showRight 0.8s forwards;
        }

        .fxSlideBehindOutPrev {
            animation: hideRight 0.8s forwards;
        }

        .fxSlideBehindInPrev {
            animation: showLeft 0.8s forwards;
        }

        @keyframes hideLeft {
            0% {
                transform: translateZ(0px);
            }

            40% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                z-index: 30;
            }

            100% {
                opacity: 1;
                transform: translateZ(-400px);
            }
        }

        @keyframes showRight {
            0% {
                transform: translateZ(-400px);
                opacity: 1;
            }

            40% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                opacity: 1;
            }

            41% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                opacity: 1;
                z-index: 30;
            }

            100% {
                transform: translateZ(0px);
                opacity: 1;
                z-index: 30;
            }
        }

        @keyframes hideRight {
            0% {
                transform: translateZ(0px);
            }

            40% {
                transform: translate(0, 40%) scale(0.8) rotateX(20deg);
                z-index: 30;
            }

            100% {
                opacity: 1;
                transform: translateZ(-400px);
            }
        }

        @keyframes showLeft {
            0% {
                transform: translateZ(-400px);
                opacity: 1;
            }

            40% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                opacity: 1;
            }

            41% {
                transform: translate(0, -40%) scale(0.8) rotateX(-20deg);
                opacity: 1;
                z-index: 30;
            }

            100% {
                transform: translateZ(0px);
                opacity: 1;
                z-index: 30;
            }
        }

        /*****************************************/
        /* Soft Pulse */
        /*****************************************/
        .fxSoftPulseOutPrev,
        .fxSoftPulseOutNext {
            animation: scaleUpFadeOut 0.8s forwards ease-in;
        }

        .fxSoftPulseInPrev,
        .fxSoftPulseInNext {
            animation: scaleDownFadeIn 0.8s forwards ease-out;
        }

        @keyframes scaleUpFadeOut {
            50% {
                transform: scale(1.2);
                opacity: 1;
            }

            75% {
                transform: scale(1.1);
                opacity: 0;
            }

            100% {
                transform: scale(1);
                opacity: 0;
            }
        }

        @keyframes scaleDownFadeIn {
            50% {
                opacity: 1;
                transform: scale(1.2);
            }

            100% {
                opacity: 1;
                transform: scale(1);
            }
        }

        /*****************************************/
        /* Earthquake */
        /* From https://elrumordelaluz.github.io/csshake/ by Lionel, http://t.co/thCECnx1Yg */
        /*****************************************/
        .fxEarthquake .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgba(0, 0, 0, 0.3);
            transition: opacity 0.5s;
        }

        .fxEarthquake .owl-item:after {
            opacity: 1;
        }

        .fxEarthquake .owl-item.fxEarthquakeInPrev {
            opacity: 1;
        }

        .fxEarthquakeOutNext {
            animation: shakeSlideBottom 1s 0.1s forwards;
        }

        .fxEarthquakeInNext {
            animation: pushFromTop 1s 0.1s forwards;
        }

        .fxEarthquakeOutPrev {
            animation: shakeSlideTop 1s 0.1s forwards;
        }

        .fxEarthquakeInPrev {
            animation: pushFromBottom 1s 0.1s forwards;
        }

        @keyframes shakeSlideBottom {
            0% {
                transform: translate(0px, 0px) rotate(0deg);
            }

            2% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            4% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            6% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            8% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            10% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            12% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            14% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            16% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            18% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            20% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            22% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            24% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            26% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            28% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            30% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            32% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            34% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            36% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            38% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            40% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            42% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            44% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            46% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            48% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            50% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            52% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            54% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            56% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            58% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            60% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            62% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            64% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            66% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            68% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            70% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            100% {
                transform: translateY(100%);
            }
        }

        @keyframes pushFromTop {

            0%,
            70% {
                opacity: 0;
                transform: translateY(-100%);
            }

            100% {
                opacity: 1;
                transform: translateY(0);
            }
        }

        @keyframes shakeSlideTop {
            0% {
                transform: translate(0px, 0px) rotate(0deg);
            }

            2% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            4% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            6% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            8% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            10% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            12% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            14% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            16% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            18% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            20% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            22% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            24% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            26% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            28% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            30% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            32% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            34% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            36% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            38% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            40% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            42% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            44% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            46% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            48% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            50% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            52% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            54% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            56% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            58% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            60% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            62% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            64% {
                transform: translate(0px, -1px) rotate(-0.5deg);
            }

            66% {
                transform: translate(-1px, -1px) rotate(-0.5deg);
            }

            68% {
                transform: translate(-1px, 0px) rotate(-0.5deg);
            }

            70% {
                transform: translate(0px, 0px) rotate(-0.5deg);
            }

            100% {
                transform: translateY(-100%);
            }
        }

        @keyframes pushFromBottom {

            0%,
            70% {
                opacity: 0;
                transform: translateY(100%);
            }

            100% {
                opacity: 1;
                transform: translateY(0);
            }
        }

        /*****************************************/
        /* Cliff diving */
        /*****************************************/
        .fxCliffDiving .owl-item {
            transform-origin: 50% 400%;
        }

        .fxCliffDiving .owl-item:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            pointer-events: none;
            background-color: rgb(255, 255, 255);
            transition: opacity 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDiving .owl-item.fxCliffDivingInNext,
        .fxCliffDiving .owl-item.fxCliffDivingInPrev {
            opacity: 1;
        }

        .fxCliffDivingOutNext {
            animation: rotateOutCircLeft 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingInNext {
            animation: rotateInCircRight 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingOutPrev {
            animation: rotateOutCircRight 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        .fxCliffDivingInPrev {
            animation: rotateInCircLeft 0.9s cubic-bezier(0.7, 0, 0.3, 1);
        }

        @keyframes rotateOutCircLeft {
            to {
                transform: rotate(-20deg) translateX(-100%);
            }
        }

        @keyframes rotateInCircRight {
            from {
                transform: rotate(20deg) translateX(100%);
            }

            to {
                transform: rotate(0deg) translateX(0);
            }
        }

        @keyframes rotateOutCircRight {
            to {
                transform: rotate(20deg) translateX(100%);
            }
        }

        @keyframes rotateInCircLeft {
            from {
                transform: rotate(-20deg) translateX(-100%);
            }

            to {
                transform: rotate(0deg) translateX(0);
            }
        }

        .owl-nav {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: left !important;
            margin-top: 0;
            height: 0;
        }

        .owl-nav [class*=owl-] {
            transform: translateY(-50%);
        }


        .owl-dots {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
        }

        .owl-dots .owl-dot span {
            width: 14px !important;
            height: 14px !important;
        }




        @media only screen and (max-width: 580px) {
                {
                .tamano {
                    top: -50px;
                    position: relative;
                }
            }

            .button-container {
                flex-direction: row;
                gap: 28px !important;
                position: inherit;
            }


            .button-container {
                flex-direction: column;
            }

            .button-container button {
                margin-bottom: 10px;
            }
        }

        .owl-prev {
            /* background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938188/left-arrow_rlxamy.png') left center no-repeat; */
            /* height: 54px; */
            position: absolute;
            top: 50%;
            /* width: 27px; */
            /* z-index: 1000; */
            left: 0%;
            cursor: pointer;
            /* color: transparent; */
            margin-top: -27px;
        }

        .owl-next {
            /* background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938220/right-arrow_zwe9sf.png') right center no-repeat; */
            /* height: 54px; */
            position: absolute;
            top: 50%;
            /* width: 27px; */
            /* z-index: 1000; */
            right: 0%;
            cursor: pointer;
            /* color: transparent; */
            margin-top: -27px;
        }

        .owl-prev:hover,
        .owl-next:hover {
            opacity: 0.5;
        }


        /* .img-thumbnail {
                                                                                                                                                                                                                                                                                                                                                                        padding: 0.25rem;
                                                                                                                                                                                                                                                                                                                                                                        background-color: {!! asset(theme_color_user(auth()->user()->theme_color, 'transparente')) !!};
                                                                                                                                                                                                                                                                                                                                                                        border: 1px solid rgba(255, 255, 255, 0);
                                                                                                                                                                                                                                                                                                                                                                        border-radius: 0.25rem;
                                                                                                                                                                                                                                                                                                                                                                        max-width: 100%;
                                                                                                                                                                                                                                                                                                                                                                        height: auto;
                                                                                                                                                                                                                                                                                                                                                                    } */

        .blobu {
            background: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};
            box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }};
            animation: pulse-red 2s infinite;
        }

        @keyframes pulse-red {
            0% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 {{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }};
            }

            70% {
                transform: scale(1);
                box-shadow: 0 0 0 10px rgba(255, 82, 82, 0);
            }

            100% {
                transform: scale(0.95);
                box-shadow: 0 0 0 0 rgba(255, 82, 82, 0);
            }
        }


        #id-svg {
            width: 100%;
            66 height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .item img {
            width: 50%;
        }

        .icon-enhance {
            font-size: 20px;
            transition: .3s;
        }

        .icon-enhance:hover {
            transform: scale(1.2) rotate(360deg);
            color: orange;
            background: transparent;
        }
    </style>
@endpush
<div class="card">
    <div class="card-innr">
        <div class="col-lg-12">
            <div class="row pt-3 pb-2">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 centro">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                        <span
                            class="badge badge-{{ auth()->user()->theme_color == 'style-black' ? '' : 'primary' }} p-2 pulse-button blobu">
                            <strong>
                                <h3 class="text-white">
                                    {{ ___('Numero de intentos') }}: {{ auth()->user()->nft_attempts }}
                                </h3>
                            </strong>
                        </span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                        <a class="btn btn-info btn-block btn-sm pulse-button p-2" style="border-radius: 15px;"
                            href="javascript:void(0);" data-toggle="modal" data-target="#modal-enhance">
                            <div class="row text-center">
                                <div class="col-lg-12">
                                    <i class="mr-3 fa fa-bolt icon-enhance" style="font-size: 25px;"></i>
                                    {{ ___('Mejora') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 centro busqueda">
                    <div class="search-box">
                        <button type="button" class="btn-search pulse-button blobu disabled" id="generarNftIntento">
                            <i class="fa fa-search"></i>
                        </button>
                        <input type="text" class="input-search focus-input"
                            placeholder="{{ ___('Type to Search') }}..." wire:model.defer="prompt">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mt-3" wire:ignore>
                @if (File::glob(public_path('nft/storage/' . auth()->id()) . '/*'))
                    <h5 style="margin-bottom: 0px;">
                        <strong>
                            {{ ___('Historial actual de tus NFT`s creados hasta el momento') }}
                        </strong>
                    </h5>
                    <div class="owl-carousel" id="owcarrusel">
                        @foreach (array_reverse(File::glob(public_path('nft/storage/' . auth()->id()) . '/*')) as $path)
                            <div class="item">
                                <a class="popup-text" href="javascript:return false">
                                    <img loading="lazy" class="card-img-top img-thumbnail"
                                        src="{{ str_replace(public_path(), URL::to('/'), $path) }}"
                                        data-id="{{ basename($path) }}" id="owl-img-{{ basename($path) }}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="row" style="margin-top: 2%;">
                @if (isset($array))
                    <div class="col-lg-{{ $array ? '6' : '' }}" style="padding-right: 0px" id="variado">
                        <div class="row generado" style="display: grid;place-items: center; padding-top: 7%;">
                            <div class="row" id="card-imagenes">
                                @foreach ($array as $item)
                                    <div class="col-lg-6">
                                        <div class="card card-generado" id="img-{{ $item }}">
                                            <a href="javascript:return false" class="card-style"
                                                onclick="renderimg('{{ $loop->index }}')">
                                                <img loading="lazy" class="card-img-top img-thumbnail" style="border-radius: 17px;"
                                                    src="{{ asset('nft/storage/' . auth()->id() . '/' . $item) }}"
                                                    data-id="{{ $item }}" id="imgn-{{ $loop->index }}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if (!$array)
                                <div class="" id="div-svg" style="margin: 0 auto; height: auto; width: 100%;">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        viewBox="0 0 800 500" width="800" height="500"
                                        preserveAspectRatio="xMidYMid meet"
                                        style="width: 100%; height: 100%; transform: translate3d(0px, 0px, 0px); content-visibility: visible; margin-top: -60px;">
                                        <defs>
                                            <clipPath id="__lottie_element_3">
                                                <rect width="800" height="500" x="0" y="0">
                                                </rect>
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#__lottie_element_3)">
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,240.46200561523438,477.39599609375)">
                                                    <path fill="rgb(0,0,0)" fill-opacity="1"
                                                        d=" M-7.794000148773193,-3.503999948501587 C-8.508999824523926,-0.6380000114440918 -8.098999977111816,1.0169999599456787 -7.458000183105469,2.6089999675750732 C-7.458000183105469,2.6089999675750732 -4.1539998054504395,2.6070001125335693 -4.1539998054504395,2.6070001125335693 C-4.1539998054504395,2.6070001125335693 -3.6080000400543213,1.253000020980835 -2.8970000743865967,1.524999976158142 C0.3659999966621399,2.7739999294281006 6.61299991607666,3.503000020980835 7.618000030517578,1.8969999551773071 C8.508999824523926,0.4740000069141388 3.5380001068115234,-1.5260000228881836 3.5380001068115234,-1.5260000228881836 C2.0490000247955322,0.9039999842643738 -1.590000033378601,-0.18299999833106995 -7.794000148773193,-3.503999948501587z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,387.89599609375,481.6080017089844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M240.6219940185547,1.3919999599456787 C240.6219940185547,1.3919999599456787 -240.6219940185547,1.3919999599456787 -240.6219940185547,1.3919999599456787 C-241.39100646972656,1.3919999599456787 -242.01400756835938,0.7689999938011169 -242.01400756835938,0 C-242.01400756835938,-0.7699999809265137 -241.39100646972656,-1.3930000066757202 -240.6219940185547,-1.3930000066757202 C-240.6219940185547,-1.3930000066757202 240.6219940185547,-1.3930000066757202 240.6219940185547,-1.3930000066757202 C241.39100646972656,-1.3930000066757202 242.01400756835938,-0.7699999809265137 242.01400756835938,-0.0010000000474974513 C242.01400756835938,-0.0010000000474974513 242.01400756835938,0 242.01400756835938,0 C242.01400756835938,0.7689999938011169 241.39100646972656,1.3919999599456787 240.6219940185547,1.3919999599456787z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,313.85101318359375,498.60699462890625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M-86.41300201416016,1.3930000066757202 C-86.41300201416016,1.3930000066757202 86.41200256347656,1.3930000066757202 86.41200256347656,1.3930000066757202 C87.18099975585938,1.3930000066757202 87.80500030517578,0.7699999809265137 87.80500030517578,0.0010000000474974513 C87.80500030517578,-0.7689999938011169 87.18099975585938,-1.3919999599456787 86.41200256347656,-1.3919999599456787 C86.41200256347656,-1.3919999599456787 -86.41300201416016,-1.3919999599456787 -86.41300201416016,-1.3919999599456787 C-87.18199920654297,-1.3919999599456787 -87.80500030517578,-0.7689999938011169 -87.80500030517578,0.0010000000474974513 C-87.80500030517578,0.7699999809265137 -87.18199920654297,1.3930000066757202 -86.41300201416016,1.3930000066757202z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,120.31600189208984,481.6080017089844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M12.020999908447266,1.3919999599456787 C12.020999908447266,1.3919999599456787 -12.020999908447266,1.3919999599456787 -12.020999908447266,1.3919999599456787 C-12.789999961853027,1.3919999599456787 -13.413999557495117,0.7689999938011169 -13.413999557495117,0 C-13.413999557495117,-0.7699999809265137 -12.789999961853027,-1.3930000066757202 -12.020999908447266,-1.3930000066757202 C-12.020999908447266,-1.3930000066757202 12.020999908447266,-1.3930000066757202 12.020999908447266,-1.3930000066757202 C12.789999961853027,-1.3930000066757202 13.413000106811523,-0.7699999809265137 13.413000106811523,0 C13.413000106811523,0.7689999938011169 12.789999961853027,1.3919999599456787 12.020999908447266,1.3919999599456787z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,658.5609741210938,481.6080017089844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M12.022000312805176,1.3919999599456787 C12.022000312805176,1.3919999599456787 -12.020999908447266,1.3919999599456787 -12.020999908447266,1.3919999599456787 C-12.789999961853027,1.3919999599456787 -13.413999557495117,0.7689999938011169 -13.413999557495117,0 C-13.413999557495117,-0.7699999809265137 -12.789999961853027,-1.3930000066757202 -12.020999908447266,-1.3930000066757202 C-12.020999908447266,-1.3930000066757202 12.022000312805176,-1.3930000066757202 12.022000312805176,-1.3930000066757202 C12.791000366210938,-1.3930000066757202 13.413999557495117,-0.7699999809265137 13.413999557495117,0 C13.413999557495117,0.7689999938011169 12.791000366210938,1.3919999599456787 12.022000312805176,1.3919999599456787z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,204.79800415039062,498.60699462890625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M-12.020999908447266,1.3930000066757202 C-12.020999908447266,1.3930000066757202 12.022000312805176,1.3930000066757202 12.022000312805176,1.3930000066757202 C12.791000366210938,1.3930000066757202 13.413999557495117,0.7699999809265137 13.413999557495117,0.0010000000474974513 C13.413999557495117,-0.7689999938011169 12.791000366210938,-1.3919999599456787 12.022000312805176,-1.3919999599456787 C12.022000312805176,-1.3919999599456787 -12.020999908447266,-1.3919999599456787 -12.020999908447266,-1.3919999599456787 C-12.789999961853027,-1.3919999599456787 -13.413000106811523,-0.7689999938011169 -13.413000106811523,0.0010000000474974513 C-13.413000106811523,0.7699999809265137 -12.789999961853027,1.3930000066757202 -12.020999908447266,1.3930000066757202z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,248.00399780273438,263.0880126953125)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-12.8934326171875,5.491463661193848 C-12.8934326171875,7.307463645935059 -11.415432929992676,8.7847261428833 -9.599432945251465,8.7847261428833 C-7.968432903289795,8.7847261428833 -6.6154327392578125,7.591463565826416 -6.355432987213135,6.033463478088379 C-6.355432987213135,6.033463478088379 -6.502432823181152,6.283463478088379 -6.502432823181152,6.283463478088379 C-6.502432823181152,6.283463478088379 -6.368432998657227,6.018725872039795 -6.368432998657227,6.018725872039795 C-6.277432918548584,6.082726001739502 -6.1704325675964355,6.115463733673096 -6.0594329833984375,6.115463733673096 C-6.0594329833984375,6.115463733673096 18.267000198364258,6.083000183105469 18.267000198364258,6.083000183105469 C18.40999984741211,6.083000183105469 18.54800033569336,6.026000022888184 18.649999618530273,5.923999786376953 C18.649999618530273,5.923999786376953 26.823999404907227,-2.249000072479248 26.823999404907227,-2.249000072479248 C26.823999404907227,-2.249000072479248 62.99100112915039,-2.249000072479248 62.99100112915039,-2.249000072479248 C63.290000915527344,-2.249000072479248 63.53300094604492,-2.492000102996826 63.53300094604492,-2.7909998893737793 C63.53300094604492,-3.0899999141693115 63.290000915527344,-3.3329999446868896 62.99100112915039,-3.3329999446868896 C62.99100112915039,-3.3329999446868896 26.598262786865234,-3.3329999446868896 26.598262786865234,-3.3329999446868896 C26.455263137817383,-3.3329999446868896 26.31800079345703,-3.2760000228881836 26.215999603271484,-3.1740000247955322 C26.215999603271484,-3.1740000247955322 18.04199981689453,5 18.04199981689453,5 C18.04199981689453,5 -5.890432834625244,5.031725883483887 -5.890432834625244,5.031725883483887 C-5.890432834625244,5.031725883483887 -6.02443265914917,5.296463489532471 -6.02443265914917,5.296463489532471 C-6.1154327392578125,5.233463764190674 -6.223432540893555,5.199463367462158 -6.333432674407959,5.199463367462158 C-6.333432674407959,5.199463367462158 -6.355432987213135,4.949463367462158 -6.355432987213135,4.949463367462158 C-6.6154327392578125,3.3914635181427 -7.968432903289795,2.198725700378418 -9.599432945251465,2.198725700378418 C-11.415432929992676,2.198725700378418 -12.8934326171875,3.6754636764526367 -12.8934326171875,5.491463661193848z M-11.808432579040527,5.491463661193848 C-11.808432579040527,4.273463726043701 -10.817432403564453,3.282463550567627 -9.599432945251465,3.282463550567627 C-8.38143253326416,3.282463550567627 -7.391432762145996,4.273463726043701 -7.391432762145996,5.491463661193848 C-7.391432762145996,6.709463596343994 -8.38143253326416,7.700725555419922 -9.599432945251465,7.700725555419922 C-10.817432403564453,7.700725555419922 -11.808432579040527,6.709463596343994 -11.808432579040527,5.491463661193848z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,249.16600036621094,132.6909942626953)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M74.7490005493164,-9.217000007629395 C74.7490005493164,-9.217000007629395 12.085662841796875,-9.205574989318848 12.085662841796875,-9.205574989318848 C11.942663192749023,-9.205574989318848 11.804662704467773,-9.146574974060059 11.702662467956543,-9.045575141906738 C11.702662467956543,-9.045575141906738 12.98166275024414,-8.82557487487793 12.98166275024414,-8.82557487487793 C12.98166275024414,-8.82557487487793 15.804662704467773,-8.942575454711914 15.804662704467773,-8.942575454711914 C15.545662879943848,-10.482575416564941 14.205662727355957,-11.661575317382812 12.592662811279297,-11.661575317382812 C10.794662475585938,-11.661575317382812 9.331663131713867,-10.198575019836426 9.331663131713867,-8.400575637817383 C9.331663131713867,-6.602575302124023 10.794662475585938,-5.139575481414795 12.592662811279297,-5.139575481414795 C14.205662727355957,-5.139575481414795 15.545662879943848,-6.318575382232666 15.804662704467773,-7.858575344085693 C15.804662704467773,-7.858575344085693 13.206663131713867,-7.741336822509766 13.206663131713867,-7.741336822509766 C13.349662780761719,-7.741336822509766 13.487663269042969,-7.7975754737854 13.589662551879883,-7.899575233459473 C13.589662551879883,-7.899575233459473 12.310663223266602,-8.121336936950684 12.310663223266602,-8.121336936950684 C12.310663223266602,-8.121336936950684 74.7490005493164,-8.131999969482422 74.7490005493164,-8.131999969482422 C75.0479965209961,-8.131999969482422 75.29100036621094,-8.37399959564209 75.29100036621094,-8.673999786376953 C75.29100036621094,-8.972999572753906 75.0479965209961,-9.217000007629395 74.7490005493164,-9.217000007629395z M12.592662811279297,-6.223575115203857 C11.39266300201416,-6.223575115203857 10.41566276550293,-7.200575351715088 10.41566276550293,-8.400575637817383 C10.41566276550293,-9.60057544708252 11.39266300201416,-10.57757568359375 12.592662811279297,-10.57757568359375 C13.792662620544434,-10.57757568359375 14.769662857055664,-9.60057544708252 14.769662857055664,-8.400575637817383 C14.769662857055664,-7.200575351715088 13.792662620544434,-6.223575115203857 12.592662811279297,-6.223575115203857z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,269.90863037109375,137.56199645996094)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-20.95400047302246,0.5419999957084656 C-20.95400047302246,0.5419999957084656 26.885000228881836,0.5419999957084656 26.885000228881836,0.5419999957084656 C27.184999465942383,0.5419999957084656 27.427000045776367,0.29899999499320984 27.427000045776367,0 C27.427000045776367,-0.29899999499320984 27.184999465942383,-0.5419999957084656 26.885000228881836,-0.5419999957084656 C26.885000228881836,-0.5419999957084656 -20.95400047302246,-0.5419999957084656 -20.95400047302246,-0.5419999957084656 C-21.21299934387207,-2.0820000171661377 -22.55299949645996,-3.260999917984009 -24.166000366210938,-3.260999917984009 C-25.964000701904297,-3.260999917984009 -27.427000045776367,-1.7979999780654907 -27.427000045776367,0 C-27.427000045776367,1.7979999780654907 -25.964000701904297,3.260999917984009 -24.166000366210938,3.260999917984009 C-22.55299949645996,3.260999917984009 -21.21299934387207,2.0820000171661377 -20.95400047302246,0.5419999957084656z M-26.343000411987305,0 C-26.343000411987305,-1.2000000476837158 -25.365999221801758,-2.177000045776367 -24.166000366210938,-2.177000045776367 C-22.965999603271484,-2.177000045776367 -21.98900032043457,-1.2000000476837158 -21.98900032043457,0 C-21.98900032043457,1.2000000476837158 -22.965999603271484,2.177000045776367 -24.166000366210938,2.177000045776367 C-25.365999221801758,2.177000045776367 -26.343000411987305,1.2000000476837158 -26.343000411987305,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,541.2059936523438,319.19000244140625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-74.7490005493164,9.215999603271484 C-74.7490005493164,9.215999603271484 -21.913959503173828,9.221053123474121 -21.913959503173828,9.221053123474121 C-21.770959854125977,9.221053123474121 -21.632959365844727,9.164052963256836 -21.530960083007812,9.063053131103516 C-21.530960083007812,9.063053131103516 -21.809959411621094,9.217053413391113 -21.809959411621094,9.217053413391113 C-21.809959411621094,9.217053413391113 -21.882959365844727,9.30405330657959 -21.882959365844727,9.30405330657959 C-21.623960494995117,10.844053268432617 -20.283960342407227,12.023053169250488 -18.67095947265625,12.023053169250488 C-16.87295913696289,12.023053169250488 -15.40995979309082,10.560053825378418 -15.40995979309082,8.762053489685059 C-15.40995979309082,6.964053630828857 -16.87295913696289,5.501053333282471 -18.67095947265625,5.501053333282471 C-20.283960342407227,5.501053333282471 -21.623960494995117,6.680053234100342 -21.882959365844727,8.220053672790527 C-21.882959365844727,8.220053672790527 -22.03495979309082,8.13305377960205 -22.03495979309082,8.13305377960205 C-22.177959442138672,8.13305377960205 -22.315959930419922,8.19005298614502 -22.417959213256836,8.29205322265625 C-22.417959213256836,8.29205322265625 -22.138959884643555,8.138053894042969 -22.138959884643555,8.138053894042969 C-22.138959884643555,8.138053894042969 -74.7490005493164,8.133000373840332 -74.7490005493164,8.133000373840332 C-75.0479965209961,8.133000373840332 -75.29100036621094,8.375 -75.29100036621094,8.673999786376953 C-75.29100036621094,8.973999977111816 -75.0479965209961,9.215999603271484 -74.7490005493164,9.215999603271484z M-18.67095947265625,6.585053443908691 C-17.47096061706543,6.585053443908691 -16.493959426879883,7.562053203582764 -16.493959426879883,8.762053489685059 C-16.493959426879883,9.962053298950195 -17.47096061706543,10.939053535461426 -18.67095947265625,10.939053535461426 C-19.870960235595703,10.939053535461426 -20.847959518432617,9.962053298950195 -20.847959518432617,8.762053489685059 C-20.847959518432617,7.562053203582764 -19.870960235595703,6.585053443908691 -18.67095947265625,6.585053443908691z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,519.4444580078125,314.3190002441406)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M20.95400047302246,-0.5419999957084656 C20.95400047302246,-0.5419999957084656 -26.885000228881836,-0.5419999957084656 -26.885000228881836,-0.5419999957084656 C-27.184999465942383,-0.5419999957084656 -27.427000045776367,-0.29899999499320984 -27.427000045776367,0 C-27.427000045776367,0.29899999499320984 -27.184999465942383,0.5419999957084656 -26.885000228881836,0.5419999957084656 C-26.885000228881836,0.5419999957084656 20.95400047302246,0.5419999957084656 20.95400047302246,0.5419999957084656 C21.21299934387207,2.0820000171661377 22.55299949645996,3.260999917984009 24.166000366210938,3.260999917984009 C25.964000701904297,3.260999917984009 27.427000045776367,1.7979999780654907 27.427000045776367,0 C27.427000045776367,-1.7979999780654907 25.964000701904297,-3.260999917984009 24.166000366210938,-3.260999917984009 C22.55299949645996,-3.260999917984009 21.21299934387207,-2.0820000171661377 20.95400047302246,-0.5419999957084656z M26.343000411987305,0 C26.343000411987305,1.2000000476837158 25.365999221801758,2.177000045776367 24.166000366210938,2.177000045776367 C22.965999603271484,2.177000045776367 21.98900032043457,1.2000000476837158 21.98900032043457,0 C21.98900032043457,-1.2000000476837158 22.965999603271484,-2.177000045776367 24.166000366210938,-2.177000045776367 C25.365999221801758,-2.177000045776367 26.343000411987305,-1.2000000476837158 26.343000411987305,0z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,402.2919921875,218.927001953125)">
                                                    <path stroke-linecap="round" stroke-linejoin="miter"
                                                        fill-opacity="0" stroke-miterlimit="10"
                                                        stroke-dasharray=" 16.068 16.068" stroke-dashoffset="0"
                                                        stroke="rgb(58,60,179)" stroke-opacity="1" stroke-width="2"
                                                        d=" M124.66300201416016,177.9499969482422 C141.47000122070312,172.48300170898438 162.67799377441406,162.9340057373047 171.37399291992188,147.5469970703125">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.36584848165512085,-0.9306744337081909,0.9306744337081909,0.36584848165512085,387.0560302734375,748.5579833984375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,401.60400390625,36.020999908447266)">
                                                    <path fill="rgb(30,73,143)" fill-opacity="1"
                                                        d=" M-8.652999877929688,3.0899999141693115 C-8.652999877929688,3.0899999141693115 8.652999877929688,4.065000057220459 8.652999877929688,4.065000057220459 C8.652999877929688,4.065000057220459 3.4519999027252197,-4.065000057220459 3.4519999027252197,-4.065000057220459 C3.4519999027252197,-4.065000057220459 -8.652999877929688,3.0899999141693115 -8.652999877929688,3.0899999141693115z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,417.47698974609375,38.9900016784668)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-8.572999954223633,3.6600000858306885 C-8.572999954223633,3.6600000858306885 13.972999572753906,-2.4800000190734863 13.972999572753906,-2.4800000190734863 C13.972999572753906,-2.4800000190734863 -13.973999977111816,-3.6600000858306885 -13.973999977111816,-3.6600000858306885 C-13.973999977111816,-3.6600000858306885 -8.572999954223633,3.6600000858306885 -8.572999954223633,3.6600000858306885z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,414.1109924316406,29.01799964904785)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M17.36199951171875,7.466000080108643 C17.36199951171875,7.466000080108643 -17.36199951171875,-7.466000080108643 -17.36199951171875,-7.466000080108643 C-17.36199951171875,-7.466000080108643 -12.413999557495117,-0.023000000044703484 -12.413999557495117,-0.023000000044703484 C-12.413999557495117,-0.023000000044703484 17.36199951171875,7.466000080108643 17.36199951171875,7.466000080108643z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,412.20098876953125,33.99399948120117)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M19.25,2.515000104904175 C19.25,2.515000104904175 -8.755999565124512,1.4040000438690186 -8.755999565124512,1.4040000438690186 C-8.755999565124512,1.4040000438690186 -19.25,5.118000030517578 -19.25,5.118000030517578 C-19.25,5.118000030517578 -10.503999710083008,-5.118000030517578 -10.503999710083008,-5.118000030517578 C-10.503999710083008,-5.118000030517578 19.25,2.515000104904175 19.25,2.515000104904175z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9998064041137695,0.019674723967909813,-0.019674723967909813,0.9998064041137695,5.284698486328125,-5.758056640625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,249.2270050048828,229.18800354003906)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M14.927000045776367,-19.66699981689453 C8.9399995803833,-24.327999114990234 2.428999900817871,-27.756000518798828 -4.045000076293945,-30.30900001525879 C-4.165999889373779,-30.351999282836914 -4.257999897003174,-30.39900016784668 -4.379000186920166,-30.44099998474121 C-6.500999927520752,-31.273000717163086 -8.62399959564209,-32.00199890136719 -10.720999717712402,-32.6349983215332 C-10.21399974822998,-29.136999130249023 -11.432000160217285,-26.41900062561035 -13.673999786376953,-24.165000915527344 C-12.562000274658203,-28.195999145507812 -13.800999641418457,-31.545000076293945 -17.195999145507812,-34.26499938964844 C-17.339000701904297,-34.37699890136719 -17.52400016784668,-34.46900177001953 -17.701000213623047,-34.505001068115234 C-23.09000015258789,-35.75199890136719 -28.139999389648438,-36.479000091552734 -32.50299835205078,-36.902000427246094 C-32.632999420166016,-36.900001525878906 -32.75,-36.91400146484375 -32.867000579833984,-36.928001403808594 C-40.435001373291016,-37.6150016784668 -45.821998596191406,-37.34600067138672 -46.99700164794922,-37.2869987487793 C-47.154998779296875,-37.28099822998047 -47.2400016784668,-37.270999908447266 -47.2400016784668,-37.270999908447266 C-47.2400016784668,-37.270999908447266 -47.22999954223633,-37.18600082397461 -47.19599914550781,-37.03099822998047 C-46.87099838256836,-35.34199905395508 -44.683998107910156,-24.61400032043457 -38.7130012512207,-12.093999862670898 C-38.65800094604492,-12 -38.6150016784668,-11.890000343322754 -38.56100082397461,-11.795999526977539 C-36.89099884033203,-8.342000007629395 -34.944000244140625,-4.749000072479248 -32.66899871826172,-1.1829999685287476 C-29.990999221801758,2.0299999713897705 -26.586999893188477,2.0380001068115234 -24.458999633789062,1.4129999876022339 C-25.891000747680664,2.8889999389648438 -27.511999130249023,3.753000020980835 -29.597000122070312,3.3350000381469727 C-25.583999633789062,8.819000244140625 -20.7549991607666,14.142999649047852 -14.927000045776367,18.68000030517578 C-14.767999649047852,18.804000854492188 -14.592000007629395,18.940000534057617 -14.420000076293945,19.04800033569336 C-14.420000076293945,19.04800033569336 -14.085000038146973,19.30900001525879 -14.085000038146973,19.30900001525879 C-10.99899959564209,21.635000228881836 -7.7769999504089355,23.6560001373291 -4.489999771118164,25.395000457763672 C-4.381999969482422,25.45400047302246 -4.289000034332275,25.499000549316406 -4.197000026702881,25.54599952697754 C18.71500015258789,37.61399841308594 44.277000427246094,36.46500015258789 46.99599838256836,36.29899978637695 C47.15399932861328,36.29399871826172 47.2400016784668,36.28300094604492 47.2400016784668,36.28300094604492 C47.2400016784668,36.28300094604492 47.22800064086914,36.196998596191406 47.19499969482422,36.04199981689453 C46.90999984741211,34.564998626708984 45.27399826049805,26.52199935913086 41.05699920654297,16.39299964904785 C41.01499938964844,16.283000946044922 40.97600173950195,16.201000213623047 40.933998107910156,16.090999603271484 C39.5989990234375,12.89799976348877 38.007999420166016,9.506999969482422 36.108001708984375,6.052999973297119 C36.013999938964844,5.877999782562256 35.92100143432617,5.701000213623047 35.82600021362305,5.525000095367432 C33.29399871826172,0.9390000104904175 30.26099967956543,-3.7300000190734863 26.60700035095215,-8.190999984741211 C25.291000366210938,-6.599999904632568 24.16699981689453,-5.320000171661377 24.16699981689453,-5.320000171661377 C24.16699981689453,-5.320000171661377 23.04599952697754,-12.244999885559082 23.04599952697754,-12.244999885559082 C20.58799934387207,-14.85099983215332 17.8799991607666,-17.368000030517578 14.927000045776367,-19.66699981689453z M-42.18899917602539,-27.85099983215332 C-42.29399871826172,-28.343000411987305 -41.96500015258789,-28.83099937438965 -41.486000061035156,-28.91900062561035 C-40.99399948120117,-29.02400016784668 -40.52199935913086,-28.70800018310547 -40.417999267578125,-28.215999603271484 C-40.31399917602539,-27.724000930786133 -40.62900161743164,-27.25200080871582 -41.119998931884766,-27.148000717163086 C-41.5989990234375,-27.059999465942383 -42.084999084472656,-27.360000610351562 -42.18899917602539,-27.85099983215332z M-40.61800003051758,-25.191999435424805 C-40.132999420166016,-25.815000534057617 -39.25299835205078,-25.926000595092773 -38.630001068115234,-25.44099998474121 C-38.007999420166016,-24.95599937438965 -37.88199996948242,-24.062999725341797 -38.367000579833984,-23.44099998474121 C-38.85200119018555,-22.81800079345703 -39.76100158691406,-22.70400047302246 -40.382999420166016,-23.187999725341797 C-41.00600051879883,-23.67300033569336 -41.10300064086914,-24.56999969482422 -40.61800003051758,-25.191999435424805z M-20.738000869750977,1.0800000429153442 C-20.402999877929688,0.6489999890327454 -19.790000915527344,0.5860000252723694 -19.358999252319336,0.9210000038146973 C-18.944000244140625,1.24399995803833 -18.85300064086914,1.8539999723434448 -19.18899917602539,2.2850000858306885 C-19.52400016784668,2.7160000801086426 -20.13800048828125,2.7780001163482666 -20.55299949645996,2.4549999237060547 C-20.983999252319336,2.11899995803833 -21.073999404907227,1.5110000371932983 -20.738000869750977,1.0800000429153442z M-19.996999740600586,5.757999897003174 C-19.413000106811523,5.007999897003174 -18.333999633789062,4.875 -17.599000930786133,5.446000099182129 C-16.849000930786133,6.031000137329102 -16.71500015258789,7.109000205993652 -17.298999786376953,7.860000133514404 C-17.871000289916992,8.593999862670898 -18.948999404907227,8.727999687194824 -19.700000762939453,8.144000053405762 C-20.43400001525879,7.572999954223633 -20.569000244140625,6.493000030517578 -19.996999740600586,5.757999897003174z M-9.104000091552734,-30.298999786376953 C-8.942000389099121,-30.506999969482422 -8.659000396728516,-30.54199981689453 -8.451000213623047,-30.3799991607666 C-8.244000434875488,-30.2189998626709 -8.192000389099121,-29.922000885009766 -8.354000091552734,-29.71500015258789 C-8.515000343322754,-29.506999969482422 -8.8149995803833,-29.483999252319336 -9.02299976348877,-29.645999908447266 C-9.229999542236328,-29.80699920654297 -9.265999794006348,-30.091999053955078 -9.104000091552734,-30.298999786376953z M-19.655000686645508,-30.975000381469727 C-19.281999588012695,-31.452999114990234 -18.60099983215332,-31.538000106811523 -18.121999740600586,-31.165000915527344 C-17.64299964904785,-30.792999267578125 -17.558000564575195,-30.111000061035156 -17.930999755859375,-29.631999969482422 C-18.304000854492188,-29.152999877929688 -18.985000610351562,-29.069000244140625 -19.464000701904297,-29.44099998474121 C-19.94300079345703,-29.81399917602539 -20.027999877929688,-30.4950008392334 -19.655000686645508,-30.975000381469727z M-17.3700008392334,-27.45400047302246 C-17.22100067138672,-27.645000457763672 -16.924999237060547,-27.69700050354004 -16.716999053955078,-27.53499984741211 C-16.509000778198242,-27.374000549316406 -16.47100067138672,-27.059999465942383 -16.6200008392334,-26.868999481201172 C-16.781999588012695,-26.660999298095703 -17.0939998626709,-26.62299919128418 -17.302000045776367,-26.78499984741211 C-17.510000228881836,-26.94700050354004 -17.531999588012695,-27.246000289916992 -17.3700008392334,-27.45400047302246z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,249.27699279785156,228.74899291992188)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-3.197000026702881,-16.761999130249023 C-3.197000026702881,-16.761999130249023 2.6540000438690186,-21.64299964904785 2.6540000438690186,-21.64299964904785 C2.6540000438690186,-21.64299964904785 2.8440001010894775,-21.392000198364258 2.8440001010894775,-21.392000198364258 C2.8440001010894775,-21.392000198364258 -3.1760001182556152,-16.361000061035156 -3.1760001182556152,-16.361000061035156 C-3.1760001182556152,-16.361000061035156 -2.184999942779541,-1.9229999780654907 -2.184999942779541,-1.9229999780654907 C-2.184999942779541,-1.9229999780654907 11.16100025177002,8.467000007629395 11.16100025177002,8.467000007629395 C11.16100025177002,8.467000007629395 10.220999717712402,-7.880000114440918 10.220999717712402,-7.880000114440918 C10.220999717712402,-7.880000114440918 10.520999908447266,-7.9029998779296875 10.520999908447266,-7.9029998779296875 C10.520999908447266,-7.9029998779296875 11.496000289916992,8.727999687194824 11.496000289916992,8.727999687194824 C11.496000289916992,8.727999687194824 34.62900161743164,26.738000869750977 34.62900161743164,26.738000869750977 C34.62900161743164,26.738000869750977 35.7760009765625,5.964000225067139 35.7760009765625,5.964000225067139 C35.869998931884766,6.139999866485596 35.9640007019043,6.315999984741211 36.05799865722656,6.492000102996826 C36.05799865722656,6.492000102996826 35.334999084472656,19.67300033569336 35.334999084472656,19.67300033569336 C35.334999084472656,19.67300033569336 40.88399887084961,16.530000686645508 40.88399887084961,16.530000686645508 C40.92599868774414,16.639999389648438 40.96500015258789,16.722000122070312 41.007999420166016,16.832000732421875 C41.007999420166016,16.832000732421875 35.3120002746582,20.065000534057617 35.3120002746582,20.065000534057617 C35.3120002746582,20.065000534057617 34.93299865722656,26.974000930786133 34.93299865722656,26.974000930786133 C34.93299865722656,26.974000930786133 47.28900146484375,36.59400177001953 47.28900146484375,36.59400177001953 C47.28900146484375,36.59400177001953 47.09000015258789,36.8489990234375 47.09000015258789,36.8489990234375 C47.09000015258789,36.8489990234375 29.1299991607666,22.867000579833984 29.1299991607666,22.867000579833984 C29.1299991607666,22.867000579833984 19.384000778198242,24.483999252319336 19.384000778198242,24.483999252319336 C19.384000778198242,24.483999252319336 19.32900047302246,24.15999984741211 19.32900047302246,24.15999984741211 C19.32900047302246,24.15999984741211 28.795000076293945,22.606000900268555 28.795000076293945,22.606000900268555 C28.795000076293945,22.606000900268555 16.374000549316406,12.935999870300293 16.374000549316406,12.935999870300293 C16.374000549316406,12.935999870300293 3.7780001163482666,15.744999885559082 3.7780001163482666,15.744999885559082 C1.909999966621399,18.111000061035156 -1.972000002861023,23.06399917602539 -4.245999813079834,25.986000061035156 C-4.3379998207092285,25.93899917602539 -4.431000232696533,25.89299964904785 -4.539000034332275,25.833999633789062 C-2.128000020980835,22.73699951171875 2.015000104904175,17.450000762939453 3.2730000019073486,15.864999771118164 C3.2730000019073486,15.864999771118164 -14.133999824523926,19.749000549316406 -14.133999824523926,19.749000549316406 C-14.133999824523926,19.749000549316406 -14.470000267028809,19.488000869750977 -14.470000267028809,19.488000869750977 C-14.470000267028809,19.488000869750977 16.038999557495117,12.675000190734863 16.038999557495117,12.675000190734863 C16.038999557495117,12.675000190734863 -8.73799991607666,-6.613999843597412 -8.73799991607666,-6.613999843597412 C-8.73799991607666,-6.613999843597412 -17.743999481201172,-2.9839999675750732 -17.743999481201172,-2.9839999675750732 C-17.743999481201172,-2.9839999675750732 -17.851999282836914,-3.2730000019073486 -17.851999282836914,-3.2730000019073486 C-17.851999282836914,-3.2730000019073486 -9.026000022888184,-6.8379998207092285 -9.026000022888184,-6.8379998207092285 C-9.026000022888184,-6.8379998207092285 -19.5310001373291,-15.015999794006348 -19.5310001373291,-15.015999794006348 C-19.5310001373291,-15.015999794006348 -38.61000061035156,-11.357000350952148 -38.61000061035156,-11.357000350952148 C-38.665000915527344,-11.451000213623047 -38.707000732421875,-11.560999870300293 -38.762001037597656,-11.656000137329102 C-38.762001037597656,-11.656000137329102 -29.27199935913086,-13.470999717712402 -29.27199935913086,-13.470999717712402 C-32.29100036621094,-15.48900032043457 -36.597999572753906,-18.354999542236328 -36.887001037597656,-18.476999282836914 C-36.887001037597656,-18.476999282836914 -36.832000732421875,-18.61400032043457 -36.832000732421875,-18.61400032043457 C-36.832000732421875,-18.61400032043457 -36.847999572753906,-18.625999450683594 -36.847999572753906,-18.625999450683594 C-36.847999572753906,-18.625999450683594 -36.98500061035156,-18.680999755859375 -36.98500061035156,-18.680999755859375 C-36.98500061035156,-18.680999755859375 -36.88199996948242,-18.7810001373291 -36.88199996948242,-18.7810001373291 C-36.784000396728516,-18.808000564575195 -36.6870002746582,-18.833999633789062 -28.820999145507812,-13.555999755859375 C-28.820999145507812,-13.555999755859375 -19.865999221801758,-15.277000427246094 -19.865999221801758,-15.277000427246094 C-19.865999221801758,-15.277000427246094 -47.244998931884766,-36.59299850463867 -47.244998931884766,-36.59299850463867 C-47.27899932861328,-36.74700164794922 -47.28900146484375,-36.832000732421875 -47.28900146484375,-36.832000732421875 C-47.28900146484375,-36.832000732421875 -47.20399856567383,-36.84299850463867 -47.04600143432617,-36.8489990234375 C-47.04600143432617,-36.8489990234375 -25.558000564575195,-20.118999481201172 -25.558000564575195,-20.118999481201172 C-25.558000564575195,-20.118999481201172 -32.91600036621094,-36.48899841308594 -32.91600036621094,-36.48899841308594 C-32.79899978637695,-36.4739990234375 -32.68199920654297,-36.459999084472656 -32.551998138427734,-36.46200180053711 C-32.551998138427734,-36.46200180053711 -25.014999389648438,-19.69700050354004 -25.014999389648438,-19.69700050354004 C-25.014999389648438,-19.69700050354004 -2.5209999084472656,-2.184000015258789 -2.5209999084472656,-2.184000015258789 C-2.5209999084472656,-2.184000015258789 -4.427999973297119,-30.00200080871582 -4.427999973297119,-30.00200080871582 C-4.307000160217285,-29.958999633789062 -4.215000152587891,-29.913000106811523 -4.093999862670898,-29.8700008392334 C-4.093999862670898,-29.8700008392334 -3.197000026702881,-16.761999130249023 -3.197000026702881,-16.761999130249023z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9998064041137695,0.019674723967909813,-0.019674723967909813,0.9998064041137695,5.21783447265625,-9.51751708984375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,510.54901123046875,209.3820037841797)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M23.054000854492188,11.694999694824219 C26.70599937438965,5.044000148773193 29.05699920654297,-1.9279999732971191 30.549999237060547,-8.725000381469727 C30.572999954223633,-8.85099983215332 30.604000091552734,-8.949999809265137 30.625999450683594,-9.076000213623047 C31.111000061035156,-11.303000450134277 31.4950008392334,-13.515000343322754 31.785999298095703,-15.685999870300293 C28.413000106811523,-14.630000114440918 25.53700065612793,-15.402000427246094 22.95599937438965,-17.256999969482422 C27.11199951171875,-16.798999786376953 30.222000122070312,-18.55299949645996 32.36899948120117,-22.336999893188477 C32.45600128173828,-22.496999740600586 32.518001556396484,-22.694000244140625 32.525001525878906,-22.874000549316406 C32.9010009765625,-28.392000198364258 32.816001892089844,-33.49399948120117 32.54100036621094,-37.86899948120117 C32.51900100708008,-37.99700164794922 32.513999938964844,-38.11399841308594 32.5099983215332,-38.231998443603516 C31.986000061035156,-45.8129997253418 30.867000579833984,-51.09000015258789 30.621999740600586,-52.2400016784668 C30.590999603271484,-52.395999908447266 30.56800079345703,-52.47800064086914 30.56800079345703,-52.47800064086914 C30.56800079345703,-52.47800064086914 30.485000610351562,-52.45399856567383 30.33799934387207,-52.395999908447266 C28.722000122070312,-51.80699920654297 18.476999282836914,-47.94499969482422 7.064000129699707,-40.0629997253418 C6.979000091552734,-39.99399948120117 6.876999855041504,-39.933998107910156 6.793000221252441,-39.8650016784668 C3.6480000019073486,-37.667999267578125 0.4099999964237213,-35.17599868774414 -2.75,-32.36399841308594 C-5.497000217437744,-29.208999633789062 -4.965000152587891,-25.847999572753906 -4.010000228881836,-23.84600067138672 C-5.695000171661377,-25.024999618530273 -6.806000232696533,-26.48900032043457 -6.723999977111816,-28.61400032043457 C-11.50100040435791,-23.7810001373291 -15.991000175476074,-18.167999267578125 -19.545000076293945,-11.694999694824219 C-19.64299964904785,-11.517000198364258 -19.75,-11.321999549865723 -19.82900047302246,-11.135000228881836 C-19.82900047302246,-11.135000228881836 -20.034000396728516,-10.762999534606934 -20.034000396728516,-10.762999534606934 C-21.840999603271484,-7.3460001945495605 -23.325000762939453,-3.8450000286102295 -24.518999099731445,-0.3230000138282776 C-24.559999465942383,-0.2070000022649765 -24.590999603271484,-0.1080000028014183 -24.62299919128418,-0.009999999776482582 C-32.9010009765625,24.527000427246094 -27.70800018310547,49.58300018310547 -27.113000869750977,52.24100112915039 C-27.08300018310547,52.395999908447266 -27.05900001525879,52.47800064086914 -27.05900001525879,52.47800064086914 C-27.05900001525879,52.47800064086914 -26.976999282836914,52.45399856567383 -26.82900047302246,52.39699935913086 C-25.415000915527344,51.880001068115234 -17.733999252319336,48.98899841308594 -8.402000427246094,43.21699905395508 C-8.300999641418457,43.15800094604492 -8.22599983215332,43.10599899291992 -8.12399959564209,43.047000885009766 C-5.184000015258789,41.22200012207031 -2.0869998931884766,39.11399841308594 1.0210000276565552,36.689998626708984 C1.1799999475479126,36.569000244140625 1.3389999866485596,36.448001861572266 1.496999979019165,36.32699966430664 C5.623000144958496,33.099998474121094 9.751999855041504,29.364999771118164 13.576000213623047,25.04800033569336 C11.795999526977539,24.00200080871582 10.354999542236328,23.094999313354492 10.354999542236328,23.094999313354492 C10.354999542236328,23.094999313354492 17.013999938964844,20.888999938964844 17.013999938964844,20.888999938964844 C19.19700050354004,18.04800033569336 21.253000259399414,14.97599983215332 23.054000854492188,11.694999694824219z M22.069000244140625,-45.99599838256836 C22.538000106811523,-46.176998138427734 23.07200050354004,-45.93000030517578 23.235000610351562,-45.47100067138672 C23.416000366210938,-45.00199890136719 23.179000854492188,-44.486000061035156 22.711000442504883,-44.30500030517578 C22.242000579833984,-44.124000549316406 21.725000381469727,-44.361000061035156 21.54400062561035,-44.83000183105469 C21.381000518798828,-45.28900146484375 21.600000381469727,-45.814998626708984 22.069000244140625,-45.99599838256836z M19.69300079345703,-44.02299880981445 C20.385000228881836,-43.643001556396484 20.632999420166016,-42.79100036621094 20.253000259399414,-42.0989990234375 C19.87299919128418,-41.40800094604492 19.011999130249023,-41.141998291015625 18.31999969482422,-41.52199935913086 C17.628999710083008,-41.902000427246094 17.371999740600586,-42.78099822998047 17.75200080871582,-43.472999572753906 C18.131999969482422,-44.16400146484375 19.00200080871582,-44.402000427246094 19.69300079345703,-44.02299880981445z M-3.0899999141693115,-20.225000381469727 C-2.6110000610351562,-19.961999893188477 -2.453000068664551,-19.367000579833984 -2.7160000801086426,-18.888999938964844 C-2.9690001010894775,-18.42799949645996 -3.555999994277954,-18.242000579833984 -4.034999847412109,-18.5049991607666 C-4.513999938964844,-18.76799964904785 -4.671999931335449,-19.36199951171875 -4.419000148773193,-19.822999954223633 C-4.156000137329102,-20.302000045776367 -3.569000005722046,-20.488000869750977 -3.0899999141693115,-20.225000381469727z M-7.5929999351501465,-18.75 C-6.758999824523926,-18.292999267578125 -6.455999851226807,-17.249000549316406 -6.9039998054504395,-16.433000564575195 C-7.361000061035156,-15.598999977111816 -8.404999732971191,-15.295000076293945 -9.23799991607666,-15.753000259399414 C-10.055000305175781,-16.201000213623047 -10.357999801635742,-17.2450008392334 -9.901000022888184,-18.077999114990234 C-9.45300006866455,-18.893999099731445 -8.409000396728516,-19.197999954223633 -7.5929999351501465,-18.75z M29.73699951171875,-13.718999862670898 C29.968000411987305,-13.592000007629395 30.047000885009766,-13.317000389099121 29.92099952697754,-13.086999893188477 C29.79400062561035,-12.855999946594238 29.510000228881836,-12.758000373840332 29.27899932861328,-12.885000228881836 C29.048999786376953,-13.01099967956543 28.97800064086914,-13.303999900817871 29.104999542236328,-13.53499984741211 C29.231000900268555,-13.765000343322754 29.506000518798828,-13.845000267028809 29.73699951171875,-13.718999862670898z M28.729000091552734,-24.243000030517578 C29.26099967956543,-23.951000213623047 29.45400047302246,-23.291000366210938 29.160999298095703,-22.759000778198242 C28.868999481201172,-22.226999282836914 28.209999084472656,-22.03499984741211 27.67799949645996,-22.32699966430664 C27.145999908447266,-22.6200008392334 26.95400047302246,-23.27899932861328 27.246000289916992,-23.81100082397461 C27.538000106811523,-24.343000411987305 28.19700050354004,-24.53499984741211 28.729000091552734,-24.243000030517578z M25.614999771118164,-21.429000854492188 C25.827999114990234,-21.312000274658203 25.926000595092773,-21.027999877929688 25.798999786376953,-20.797000885009766 C25.672000885009766,-20.566999435424805 25.3700008392334,-20.479000091552734 25.156999588012695,-20.59600067138672 C24.927000045776367,-20.722000122070312 24.839000701904297,-21.02400016784668 24.965999603271484,-21.2549991607666 C25.091999053955078,-21.485000610351562 25.384000778198242,-21.555999755859375 25.614999771118164,-21.429000854492188z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,513.218994140625,209.42100524902344)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M15.079999923706055,-5.797999858856201 C15.079999923706055,-5.797999858856201 20.82699966430664,-0.7960000038146973 20.82699966430664,-0.7960000038146973 C20.82699966430664,-0.7960000038146973 20.610000610351562,-0.5690000057220459 20.610000610351562,-0.5690000057220459 C20.610000610351562,-0.5690000057220459 14.687000274658203,-5.714000225067139 14.687000274658203,-5.714000225067139 C14.687000274658203,-5.714000225067139 0.5899999737739563,-2.444000005722046 0.5899999737739563,-2.444000005722046 C0.5899999737739563,-2.444000005722046 -7.550000190734863,12.383000373840332 -7.550000190734863,12.383000373840332 C-7.550000190734863,12.383000373840332 8.4399995803833,8.859999656677246 8.4399995803833,8.859999656677246 C8.4399995803833,8.859999656677246 8.510000228881836,9.152000427246094 8.510000228881836,9.152000427246094 C8.510000228881836,9.152000427246094 -7.755000114440918,12.755000114440918 -7.755000114440918,12.755000114440918 C-7.755000114440918,12.755000114440918 -21.864999771118164,38.452999114990234 -21.864999771118164,38.452999114990234 C-21.864999771118164,38.452999114990234 -1.1729999780654907,36.28799819946289 -1.1729999780654907,36.28799819946289 C-1.3309999704360962,36.409000396728516 -1.4900000095367432,36.529998779296875 -1.6480000019073486,36.650001525878906 C-1.6480000019073486,36.650001525878906 -14.777000427246094,38.02799987792969 -14.777000427246094,38.02799987792969 C-14.777000427246094,38.02799987792969 -10.793999671936035,43.007999420166016 -10.793999671936035,43.007999420166016 C-10.895999908447266,43.06700134277344 -10.970000267028809,43.11899948120117 -11.071999549865723,43.178001403808594 C-11.071999549865723,43.178001403808594 -15.168000221252441,38.06800079345703 -15.168000221252441,38.06800079345703 C-15.168000221252441,38.06800079345703 -22.049999237060547,38.790000915527344 -22.049999237060547,38.790000915527344 C-22.049999237060547,38.790000915527344 -29.586000442504883,52.516998291015625 -29.586000442504883,52.516998291015625 C-29.586000442504883,52.516998291015625 -29.871000289916992,52.361000061035156 -29.871000289916992,52.361000061035156 C-29.871000289916992,52.361000061035156 -18.916000366210938,32.409000396728516 -18.916000366210938,32.409000396728516 C-18.916000366210938,32.409000396728516 -22.05900001525879,23.042999267578125 -22.05900001525879,23.042999267578125 C-22.05900001525879,23.042999267578125 -21.74799919128418,22.937000274658203 -21.74799919128418,22.937000274658203 C-21.74799919128418,22.937000274658203 -18.711000442504883,32.0359992980957 -18.711000442504883,32.0359992980957 C-18.711000442504883,32.0359992980957 -11.13599967956543,18.23900032043457 -11.13599967956543,18.23900032043457 C-11.13599967956543,18.23900032043457 -15.907999992370605,6.248000144958496 -15.907999992370605,6.248000144958496 C-18.541000366210938,4.7789998054504395 -24.047000885009766,1.7330000400543213 -27.29199981689453,-0.04899999871850014 C-27.26099967956543,-0.14800000190734863 -27.229999542236328,-0.2460000067949295 -27.18899917602539,-0.3619999885559082 C-23.749000549316406,1.5269999504089355 -17.871000289916992,4.7769999504089355 -16.10700035095215,5.769000053405762 C-16.10700035095215,5.769000053405762 -22.70400047302246,-10.802000045776367 -22.70400047302246,-10.802000045776367 C-22.70400047302246,-10.802000045776367 -22.499000549316406,-11.173999786376953 -22.499000549316406,-11.173999786376953 C-22.499000549316406,-11.173999786376953 -10.930999755859375,17.865999221801758 -10.930999755859375,17.865999221801758 C-10.930999755859375,17.865999221801758 4.181000232696533,-9.657999992370605 4.181000232696533,-9.657999992370605 C4.181000232696533,-9.657999992370605 -0.8320000171661377,-17.974000930786133 -0.8320000171661377,-17.974000930786133 C-0.8320000171661377,-17.974000930786133 -0.5640000104904175,-18.125999450683594 -0.5640000104904175,-18.125999450683594 C-0.5640000104904175,-18.125999450683594 4.35699987411499,-9.977999687194824 4.35699987411499,-9.977999687194824 C4.35699987411499,-9.977999687194824 10.763999938964844,-21.64699935913086 10.763999938964844,-21.64699935913086 C10.763999938964844,-21.64699935913086 4.123000144958496,-39.90399932861328 4.123000144958496,-39.90399932861328 C4.206999778747559,-39.972999572753906 4.309000015258789,-40.03300094604492 4.394000053405762,-40.10200119018555 C4.394000053405762,-40.10200119018555 7.692999839782715,-31.020000457763672 7.692999839782715,-31.020000457763672 C9.204999923706055,-34.32099914550781 11.35200023651123,-39.02799987792969 11.427000045776367,-39.33300018310547 C11.427000045776367,-39.33300018310547 11.569999694824219,-39.30099868774414 11.569999694824219,-39.30099868774414 C11.569999694824219,-39.30099868774414 11.579999923706055,-39.319000244140625 11.579999923706055,-39.319000244140625 C11.579999923706055,-39.319000244140625 11.612000465393066,-39.46200180053711 11.612000465393066,-39.46200180053711 C11.612000465393066,-39.46200180053711 11.72700023651123,-39.375999450683594 11.72700023651123,-39.375999450683594 C11.769000053405762,-39.284000396728516 11.810999870300293,-39.19200134277344 7.8480000495910645,-30.58799934387207 C7.8480000495910645,-30.58799934387207 10.968000411987305,-22.020000457763672 10.968000411987305,-22.020000457763672 C10.968000411987305,-22.020000457763672 27.667999267578125,-52.435001373291016 27.667999267578125,-52.435001373291016 C27.815000534057617,-52.49300003051758 27.898000717163086,-52.516998291015625 27.898000717163086,-52.516998291015625 C27.898000717163086,-52.516998291015625 27.922000885009766,-52.435001373291016 27.95199966430664,-52.27899932861328 C27.95199966430664,-52.27899932861328 14.845999717712402,-28.408000946044922 14.845999717712402,-28.408000946044922 C14.845999717712402,-28.408000946044922 29.84000015258789,-38.270999908447266 29.84000015258789,-38.270999908447266 C29.8439998626709,-38.15399932861328 29.849000930786133,-38.0359992980957 29.871000289916992,-37.90800094604492 C29.871000289916992,-37.90800094604492 14.515000343322754,-27.80500030517578 14.515000343322754,-27.80500030517578 C14.515000343322754,-27.80500030517578 0.7950000166893005,-2.815999984741211 0.7950000166893005,-2.815999984741211 C0.7950000166893005,-2.815999984741211 27.957000732421875,-9.114999771118164 27.957000732421875,-9.114999771118164 C27.93400001525879,-8.98900032043457 27.902999877929688,-8.890000343322754 27.8799991607666,-8.763999938964844 C27.8799991607666,-8.763999938964844 15.079999923706055,-5.797999858856201 15.079999923706055,-5.797999858856201z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9998064041137695,-0.019674723967909813,0.019674723967909813,0.9998064041137695,-5.52423095703125,9.286834716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,518.2529907226562,257.15399169921875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M12.555000305175781,21.047000885009766 C19.051000595092773,17.12700080871582 24.58300018310547,12.276000022888184 29.28499984741211,7.145999908447266 C29.368000030517578,7.047999858856201 29.44499969482422,6.979000091552734 29.527999877929688,6.88100004196167 C31.06399917602539,5.197999954223633 32.50600051879883,3.4769999980926514 33.847999572753906,1.7450000047683716 C30.400999069213867,0.9660000205039978 28.299999237060547,-1.1449999809265137 26.99799919128418,-4.045000076293945 C30.363000869750977,-1.562999963760376 33.933998107910156,-1.5199999809265137 37.68899917602539,-3.7160000801086426 C37.845001220703125,-3.809999942779541 37.99800109863281,-3.9489998817443848 38.09400177001953,-4.1020002365112305 C41.1879997253418,-8.685999870300293 43.67499923706055,-13.140999794006348 45.632999420166016,-17.062999725341797 C45.678001403808594,-17.184999465942383 45.733001708984375,-17.288999557495117 45.78799819946289,-17.39299964904785 C49.13999938964844,-24.21299934387207 50.81999969482422,-29.339000701904297 51.185001373291016,-30.457000732421875 C51.236000061035156,-30.60700035095215 51.25699996948242,-30.690000534057617 51.25699996948242,-30.690000534057617 C51.25699996948242,-30.690000534057617 51.17399978637695,-30.709999084472656 51.018001556396484,-30.733999252319336 C49.32400131225586,-31.035999298095703 38.52399826049805,-32.83700180053711 24.69700050354004,-31.746000289916992 C24.589000701904297,-31.729000091552734 24.47100067138672,-31.72800064086914 24.363000869750977,-31.711000442504883 C20.540000915527344,-31.388999938964844 16.48900032043457,-30.857999801635742 12.345000267028809,-30.01099967956543 C8.385000228881836,-28.66200065612793 7.1579999923706055,-25.48699951171875 6.980000019073486,-23.275999069213867 C6.114999771118164,-25.141000747680664 5.888000011444092,-26.96500015258789 7.025000095367432,-28.761999130249023 C0.46799999475479126,-26.979999542236328 -6.23199987411499,-24.37700080871582 -12.555000305175781,-20.56100082397461 C-12.727999687194824,-20.457000732421875 -12.918999671936035,-20.341999053955078 -13.081999778747559,-20.219999313354492 C-13.081999778747559,-20.219999313354492 -13.446000099182129,-20.000999450683594 -13.446000099182129,-20.000999450683594 C-16.722999572753906,-17.95199966430664 -19.76300048828125,-15.668000221252441 -22.56399917602539,-13.222000122070312 C-22.658000946044922,-13.142000198364258 -22.733999252319336,-13.071999549865723 -22.809999465942383,-13.003000259399414 C-42.28499984741211,4.066999912261963 -50.36600112915039,28.344999313354492 -51.185001373291016,30.94300079345703 C-51.23699951171875,31.091999053955078 -51.25699996948242,31.174999237060547 -51.25699996948242,31.174999237060547 C-51.25699996948242,31.174999237060547 -51.17399978637695,31.195999145507812 -51.018001556396484,31.219999313354492 C-49.5359992980957,31.482999801635742 -41.441001892089844,32.83700180053711 -30.472999572753906,32.527000427246094 C-30.354999542236328,32.527000427246094 -30.264999389648438,32.52000045776367 -30.14699935913086,32.52000045776367 C-26.687999725341797,32.41699981689453 -22.95199966430664,32.14699935913086 -19.04599952697754,31.610000610351562 C-18.847999572753906,31.584999084472656 -18.649999618530273,31.56100082397461 -18.452999114990234,31.53499984741211 C-13.263999938964844,30.81399917602539 -7.818999767303467,29.6560001373291 -2.3450000286102295,27.840999603271484 C-3.3589999675750732,26.042999267578125 -4.151000022888184,24.53499984741211 -4.151000022888184,24.53499984741211 C-4.151000022888184,24.53499984741211 2.7170000076293945,25.968000411987305 2.7170000076293945,25.968000411987305 C6.03000020980835,24.606000900268555 9.350000381469727,22.981000900268555 12.555000305175781,21.047000885009766z M40.65299987792969,-29.347999572753906 C41.150001525878906,-29.268999099731445 41.48699951171875,-28.788000106811523 41.39799880981445,-28.30900001525879 C41.31999969482422,-27.812000274658203 40.85599899291992,-27.485000610351562 40.35900115966797,-27.562999725341797 C39.862998962402344,-27.642000198364258 39.53499984741211,-28.106000900268555 39.61399841308594,-28.60300064086914 C39.702999114990234,-29.08099937438965 40.15700149536133,-29.426000595092773 40.65299987792969,-29.347999572753906z M37.608001708984375,-28.833999633789062 C38.01599884033203,-28.158000946044922 37.803001403808594,-27.297000885009766 37.12699890136719,-26.888999938964844 C36.45199966430664,-26.481000900268555 35.573001861572266,-26.68400001525879 35.165000915527344,-27.360000610351562 C34.757999420166016,-28.03499984741211 34.97700119018555,-28.923999786376953 35.65299987792969,-29.332000732421875 C36.32899856567383,-29.739999771118164 37.20000076293945,-29.509000778198242 37.608001708984375,-28.833999633789062z M5.959000110626221,-19.683000564575195 C6.241000175476074,-19.21500015258789 6.078999996185303,-18.621000289916992 5.611999988555908,-18.339000701904297 C5.160999774932861,-18.066999435424805 4.560999870300293,-18.201000213623047 4.2779998779296875,-18.66900062561035 C3.996000051498413,-19.13599967956543 4.1579999923706055,-19.729999542236328 4.607999801635742,-20.00200080871582 C5.076000213623047,-20.284000396728516 5.676000118255615,-20.149999618530273 5.959000110626221,-19.683000564575195z M1.3240000009536743,-20.666000366210938 C1.815000057220459,-19.851999282836914 1.5540000200271606,-18.797000885009766 0.7570000290870667,-18.31599998474121 C-0.05700000002980232,-17.825000762939453 -1.1119999885559082,-18.086000442504883 -1.6039999723434448,-18.899999618530273 C-2.0850000381469727,-19.69700050354004 -1.8229999542236328,-20.75200080871582 -1.0089999437332153,-21.243000030517578 C-0.21299999952316284,-21.724000930786133 0.8429999947547913,-21.46299934387207 1.3240000009536743,-20.666000366210938z M31.08799934387207,2.4189999103546143 C31.224000930786133,2.6440000534057617 31.155000686645508,2.921999931335449 30.93000030517578,3.056999921798706 C30.704999923706055,3.193000078201294 30.40999984741211,3.134999990463257 30.27400016784668,2.9100000858306885 C30.13800048828125,2.684999942779541 30.224000930786133,2.3970000743865967 30.448999404907227,2.260999917984009 C30.673999786376953,2.125 30.95199966430664,2.194000005722046 31.08799934387207,2.4189999103546143z M35.49800109863281,-7.190000057220459 C35.8120002746582,-6.670000076293945 35.64699935913086,-6.004000186920166 35.12699890136719,-5.690000057220459 C34.608001708984375,-5.376999855041504 33.941001892089844,-5.540999889373779 33.62699890136719,-6.060999870300293 C33.3129997253418,-6.580999851226807 33.479000091552734,-7.247000217437744 33.99800109863281,-7.560999870300293 C34.518001556396484,-7.875 35.183998107910156,-7.710000038146973 35.49800109863281,-7.190000057220459z M31.392000198364258,-6.318999767303467 C31.517000198364258,-6.111000061035156 31.458999633789062,-5.815999984741211 31.233999252319336,-5.679999828338623 C31.009000778198242,-5.544000148773193 30.702999114990234,-5.619999885559082 30.577999114990234,-5.828000068664551 C30.441999435424805,-6.052999973297119 30.51799964904785,-6.357999801635742 30.743000030517578,-6.49399995803833 C30.968000411987305,-6.630000114440918 31.256000518798828,-6.545000076293945 31.392000198364258,-6.318999767303467z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,518.2109985351562,257.5450134277344)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M16.767000198364258,2.8980000019073486 C16.767000198364258,2.8980000019073486 19.22800064086914,10.109000205993652 19.22800064086914,10.109000205993652 C19.22800064086914,10.109000205993652 18.926000595092773,10.196000099182129 18.926000595092773,10.196000099182129 C18.926000595092773,10.196000099182129 16.385000228881836,2.7739999294281006 16.385000228881836,2.7739999294281006 C16.385000228881836,2.7739999294281006 2.549999952316284,-1.472000002861023 2.549999952316284,-1.472000002861023 C2.549999952316284,-1.472000002861023 -11.930999755859375,7.267000198364258 -11.930999755859375,7.267000198364258 C-11.930999755859375,7.267000198364258 3.6679999828338623,12.244000434875488 3.6679999828338623,12.244000434875488 C3.6679999828338623,12.244000434875488 3.5820000171661377,12.532999992370605 3.5820000171661377,12.532999992370605 C3.5820000171661377,12.532999992370605 -12.295000076293945,7.486000061035156 -12.295000076293945,7.486000061035156 C-12.295000076293945,7.486000061035156 -37.39500045776367,22.634000778198242 -37.39500045776367,22.634000778198242 C-37.39500045776367,22.634000778198242 -18.410999298095703,31.145000457763672 -18.410999298095703,31.145000457763672 C-18.608999252319336,31.170000076293945 -18.80699920654297,31.19499969482422 -19.003999710083008,31.219999313354492 C-19.003999710083008,31.219999313354492 -31.052000045776367,25.823999404907227 -31.052000045776367,25.823999404907227 C-31.052000045776367,25.823999404907227 -30.104999542236328,32.12900161743164 -30.104999542236328,32.12900161743164 C-30.222999572753906,32.130001068115234 -30.31399917602539,32.137001037597656 -30.430999755859375,32.137001037597656 C-30.430999755859375,32.137001037597656 -31.409000396728516,25.660999298095703 -31.409000396728516,25.660999298095703 C-31.409000396728516,25.660999298095703 -37.7239990234375,22.83300018310547 -37.7239990234375,22.83300018310547 C-37.7239990234375,22.83300018310547 -51.13199996948242,30.923999786376953 -51.13199996948242,30.923999786376953 C-51.13199996948242,30.923999786376953 -51.29899978637695,30.645999908447266 -51.29899978637695,30.645999908447266 C-51.29899978637695,30.645999908447266 -31.81100082397461,18.88599967956543 -31.81100082397461,18.88599967956543 C-31.81100082397461,18.88599967956543 -29.83099937438965,9.206999778747559 -29.83099937438965,9.206999778747559 C-29.83099937438965,9.206999778747559 -29.507999420166016,9.272000312805176 -29.507999420166016,9.272000312805176 C-29.507999420166016,9.272000312805176 -31.447999954223633,18.666000366210938 -31.447999954223633,18.666000366210938 C-31.447999954223633,18.666000366210938 -17.97100067138672,10.532999992370605 -17.97100067138672,10.532999992370605 C-17.97100067138672,10.532999992370605 -16.08099937438965,-2.2330000400543213 -16.08099937438965,-2.2330000400543213 C-17.621999740600586,-4.824999809265137 -20.856000900268555,-10.222999572753906 -22.768999099731445,-13.392999649047852 C-22.691999435424805,-13.461999893188477 -22.615999221801758,-13.531999588012695 -22.52199935913086,-13.612000465393066 C-20.493999481201172,-10.25100040435791 -17.041000366210938,-4.489999771118164 -16.01300048828125,-2.746999979019165 C-16.01300048828125,-2.746999979019165 -13.404000282287598,-20.391000747680664 -13.404000282287598,-20.391000747680664 C-13.404000282287598,-20.391000747680664 -13.039999961853027,-20.611000061035156 -13.039999961853027,-20.611000061035156 C-13.039999961853027,-20.611000061035156 -17.60700035095215,10.314000129699707 -17.60700035095215,10.314000129699707 C-17.60700035095215,10.314000129699707 9.277000427246094,-5.909999847412109 9.277000427246094,-5.909999847412109 C9.277000427246094,-5.909999847412109 9.11400032043457,-15.618000030517578 9.11400032043457,-15.618000030517578 C9.11400032043457,-15.618000030517578 9.42199993133545,-15.616000175476074 9.42199993133545,-15.616000175476074 C9.42199993133545,-15.616000175476074 9.58899974822998,-6.0980000495910645 9.58899974822998,-6.0980000495910645 C9.58899974822998,-6.0980000495910645 20.98699951171875,-12.97700023651123 20.98699951171875,-12.97700023651123 C20.98699951171875,-12.97700023651123 24.405000686645508,-32.10100173950195 24.405000686645508,-32.10100173950195 C24.511999130249023,-32.11899948120117 24.631000518798828,-32.11899948120117 24.738000869750977,-32.137001037597656 C24.738000869750977,-32.137001037597656 23.034000396728516,-22.625 23.034000396728516,-22.625 C25.999000549316406,-24.72100067138672 30.218000411987305,-27.715999603271484 30.43600082397461,-27.941999435424805 C30.43600082397461,-27.941999435424805 30.542999267578125,-27.841999053955078 30.542999267578125,-27.841999053955078 C30.542999267578125,-27.841999053955078 30.56100082397461,-27.85300064086914 30.56100082397461,-27.85300064086914 C30.56100082397461,-27.85300064086914 30.660999298095703,-27.961000442504883 30.660999298095703,-27.961000442504883 C30.660999298095703,-27.961000442504883 30.716999053955078,-27.82900047302246 30.716999053955078,-27.82900047302246 C30.707000732421875,-27.72800064086914 30.69700050354004,-27.62700080871582 22.95199966430664,-22.173999786376953 C22.95199966430664,-22.173999786376953 21.35099983215332,-13.196999549865723 21.35099983215332,-13.196999549865723 C21.35099983215332,-13.196999549865723 51.058998107910156,-31.125 51.058998107910156,-31.125 C51.21500015258789,-31.10099983215332 51.29899978637695,-31.079999923706055 51.29899978637695,-31.079999923706055 C51.29899978637695,-31.079999923706055 51.27799987792969,-30.996999740600586 51.22700119018555,-30.847000122070312 C51.22700119018555,-30.847000122070312 27.910999298095703,-16.777000427246094 27.910999298095703,-16.777000427246094 C27.910999298095703,-16.777000427246094 45.82899856567383,-17.784000396728516 45.82899856567383,-17.784000396728516 C45.775001525878906,-17.679000854492188 45.71900177001953,-17.575000762939453 45.67399978637695,-17.452999114990234 C45.67399978637695,-17.452999114990234 27.32200050354004,-16.42099952697754 27.32200050354004,-16.42099952697754 C27.32200050354004,-16.42099952697754 2.9140000343322754,-1.6920000314712524 2.9140000343322754,-1.6920000314712524 C2.9140000343322754,-1.6920000314712524 29.569000244140625,6.491000175476074 29.569000244140625,6.491000175476074 C29.486000061035156,6.5879998207092285 29.40999984741211,6.6579999923706055 29.32699966430664,6.755000114440918 C29.32699966430664,6.755000114440918 16.767000198364258,2.8980000019073486 16.767000198364258,2.8980000019073486z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,387.90399169921875,288.7019958496094)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M95.0459976196289,-190.96499633789062 C95.0459976196289,-190.96499633789062 -95.0459976196289,-190.96499633789062 -95.0459976196289,-190.96499633789062 C-103.43399810791016,-190.96499633789062 -110.23500061035156,-184.16299438476562 -110.23500061035156,-175.7760009765625 C-110.23500061035156,-175.7760009765625 -110.23500061035156,175.77699279785156 -110.23500061035156,175.77699279785156 C-110.23500061035156,184.1649932861328 -103.43399810791016,190.96600341796875 -95.0459976196289,190.96600341796875 C-95.0459976196289,190.96600341796875 95.0459976196289,190.96600341796875 95.0459976196289,190.96600341796875 C103.43399810791016,190.96600341796875 110.23500061035156,184.1649932861328 110.23500061035156,175.77699279785156 C110.23500061035156,175.77699279785156 110.23500061035156,-175.7760009765625 110.23500061035156,-175.7760009765625 C110.23500061035156,-184.16299438476562 103.43399810791016,-190.96499633789062 95.0459976196289,-190.96499633789062z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,387.6050109863281,287.1650085449219)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }}"
                                                        fill-opacity="1"
                                                        d=" M99.53099822998047,-195.58599853515625 C99.53099822998047,-195.58599853515625 -99.53199768066406,-195.58599853515625 -99.53199768066406,-195.58599853515625 C-106.88500213623047,-195.58599853515625 -112.83999633789062,-189.47799682617188 -112.83999633789062,-181.93299865722656 C-112.83999633789062,-181.93299865722656 -112.83999633789062,181.9250030517578 -112.83999633789062,181.9250030517578 C-112.83999633789062,189.468994140625 -106.88500213623047,195.58599853515625 -99.53199768066406,195.58599853515625 C-99.53199768066406,195.58599853515625 99.53099822998047,195.58599853515625 99.53099822998047,195.58599853515625 C106.88600158691406,195.58599853515625 112.84100341796875,189.468994140625 112.84100341796875,181.9250030517578 C112.84100341796875,181.9250030517578 112.84100341796875,-181.93299865722656 112.84100341796875,-181.93299865722656 C112.84100341796875,-189.47799682617188 106.88600158691406,-195.58599853515625 99.53099822998047,-195.58599853515625z M109.4540023803711,165.1230010986328 C109.4540023803711,172.01499938964844 103.41500091552734,177.5989990234375 95.96700286865234,177.5989990234375 C95.96700286865234,177.5989990234375 -95.74099731445312,177.5989990234375 -95.74099731445312,177.5989990234375 C-103.18900299072266,177.5989990234375 -109.22699737548828,172.01499938964844 -109.22699737548828,165.12399291992188 C-109.22699737548828,165.12399291992188 -109.22699737548828,-172.68800354003906 -109.22699737548828,-172.68800354003906 C-109.22699737548828,-179.58299255371094 -103.18499755859375,-185.17300415039062 -95.73200225830078,-185.17300415039062 C-95.73200225830078,-185.17300415039062 -19.863000869750977,-185.17300415039062 -19.863000869750977,-185.17300415039062 C-18.874000549316406,-185.17300415039062 -18.08799934387207,-184.44000244140625 -18.047000885009766,-183.5260009765625 C-17.648000717163086,-174.71200561523438 -9.590999603271484,-167.53199768066406 0.11400000005960464,-167.53199768066406 C9.82699966430664,-167.53199768066406 17.86400032043457,-174.69000244140625 18.263999938964844,-183.5260009765625 C18.30500030517578,-184.44000244140625 19.091999053955078,-185.17300415039062 20.08099937438965,-185.17300415039062 C20.08099937438965,-185.17300415039062 95.96099853515625,-185.17300415039062 95.96099853515625,-185.17300415039062 C103.41300201416016,-185.17300415039062 109.4540023803711,-179.58299255371094 109.4540023803711,-172.68800354003906 C109.4540023803711,-172.68800354003906 109.4540023803711,165.1230010986328 109.4540023803711,165.1230010986328z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,387.718994140625,105.39099884033203)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-10.305999755859375,-0.0010000000474974513 C-10.305999755859375,-5.691999912261963 -5.692999839782715,-10.305999755859375 -0.0010000000474974513,-10.305999755859375 C5.690999984741211,-10.305999755859375 10.305000305175781,-5.691999912261963 10.305000305175781,-0.0010000000474974513 C10.305000305175781,5.691999912261963 5.690999984741211,10.305999755859375 -0.0010000000474974513,10.305999755859375 C-5.692999839782715,10.305999755859375 -10.305999755859375,5.691999912261963 -10.305999755859375,-0.0010000000474974513z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,387.7179870605469,105.38999938964844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'back')) }}"
                                                        fill-opacity="1"
                                                        d=" M-7.6539998054504395,0 C-7.6539998054504395,-4.2270002365112305 -4.22599983215332,-7.6519999504089355 0.0010000000474974513,-7.6519999504089355 C4.22599983215332,-7.6519999504089355 7.6529998779296875,-4.2270002365112305 7.6529998779296875,0 C7.6529998779296875,4.224999904632568 4.22599983215332,7.6519999504089355 0.0010000000474974513,7.6519999504089355 C-4.22599983215332,7.6519999504089355 -7.6539998054504395,4.224999904632568 -7.6539998054504395,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,385.8599853515625,101.98500061035156)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-2.1710000038146973,0 C-2.1710000038146973,-1.2000000476837158 -1.1990000009536743,-2.171999931335449 0,-2.171999931335449 C1.1990000009536743,-2.171999931335449 2.1710000038146973,-1.2000000476837158 2.1710000038146973,0 C2.1710000038146973,1.1990000009536743 1.1990000009536743,2.171999931335449 0,2.171999931335449 C-1.1990000009536743,2.171999931335449 -2.1710000038146973,1.1990000009536743 -2.1710000038146973,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,341.43701171875,177.21800231933594)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-25.094999313354492,-26.240999221801758 C-25.094999313354492,-26.240999221801758 -9.95199966430664,-26.240999221801758 -9.95199966430664,-26.240999221801758 C-9.95199966430664,-26.240999221801758 9.809000015258789,2.7939999103546143 9.809000015258789,2.7939999103546143 C9.809000015258789,2.7939999103546143 9.809000015258789,-26.240999221801758 9.809000015258789,-26.240999221801758 C9.809000015258789,-26.240999221801758 25.094999313354492,-26.240999221801758 25.094999313354492,-26.240999221801758 C25.094999313354492,-26.240999221801758 25.094999313354492,26.240999221801758 25.094999313354492,26.240999221801758 C25.094999313354492,26.240999221801758 9.809000015258789,26.240999221801758 9.809000015258789,26.240999221801758 C9.809000015258789,26.240999221801758 -9.845000267028809,-2.5759999752044678 -9.845000267028809,-2.5759999752044678 C-9.845000267028809,-2.5759999752044678 -9.845000267028809,26.240999221801758 -9.845000267028809,26.240999221801758 C-9.845000267028809,26.240999221801758 -25.094999313354492,26.240999221801758 -25.094999313354492,26.240999221801758 C-25.094999313354492,26.240999221801758 -25.094999313354492,-26.240999221801758 -25.094999313354492,-26.240999221801758z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,397.4259948730469,177.21800231933594)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-20.04800033569336,-26.240999221801758 C-20.04800033569336,-26.240999221801758 20.047000885009766,-26.240999221801758 20.047000885009766,-26.240999221801758 C20.047000885009766,-26.240999221801758 20.047000885009766,-14.96399974822998 20.047000885009766,-14.96399974822998 C20.047000885009766,-14.96399974822998 -3.759999990463257,-14.96399974822998 -3.759999990463257,-14.96399974822998 C-3.759999990463257,-14.96399974822998 -3.759999990463257,-5.798999786376953 -3.759999990463257,-5.798999786376953 C-3.759999990463257,-5.798999786376953 16.573999404907227,-5.798999786376953 16.573999404907227,-5.798999786376953 C16.573999404907227,-5.798999786376953 16.573999404907227,4.796999931335449 16.573999404907227,4.796999931335449 C16.573999404907227,4.796999931335449 -3.759999990463257,4.796999931335449 -3.759999990463257,4.796999931335449 C-3.759999990463257,4.796999931335449 -3.759999990463257,26.240999221801758 -3.759999990463257,26.240999221801758 C-3.759999990463257,26.240999221801758 -20.04800033569336,26.240999221801758 -20.04800033569336,26.240999221801758 C-20.04800033569336,26.240999221801758 -20.04800033569336,-26.240999221801758 -20.04800033569336,-26.240999221801758z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,447.1679992675781,177.21800231933594)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-24.64699935913086,-26.240999221801758 C-24.64699935913086,-26.240999221801758 24.64699935913086,-26.240999221801758 24.64699935913086,-26.240999221801758 C24.64699935913086,-26.240999221801758 24.64699935913086,-13.281000137329102 24.64699935913086,-13.281000137329102 C24.64699935913086,-13.281000137329102 8.107999801635742,-13.281000137329102 8.107999801635742,-13.281000137329102 C8.107999801635742,-13.281000137329102 8.107999801635742,26.240999221801758 8.107999801635742,26.240999221801758 C8.107999801635742,26.240999221801758 -8.109000205993652,26.240999221801758 -8.109000205993652,26.240999221801758 C-8.109000205993652,26.240999221801758 -8.109000205993652,-13.281000137329102 -8.109000205993652,-13.281000137329102 C-8.109000205993652,-13.281000137329102 -24.64699935913086,-13.281000137329102 -24.64699935913086,-13.281000137329102 C-24.64699935913086,-13.281000137329102 -24.64699935913086,-26.240999221801758 -24.64699935913086,-26.240999221801758z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9995760321617126,-0.029116354882717133,0.029116354882717133,0.9995760321617126,-11.2158203125,16.63250732421875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,547.655029296875,402.84698486328125)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-12.293000221252441,-4.52400016784668 C-12.293000221252441,-4.52400016784668 -5.172999858856201,13.626999855041504 -0.03099999949336052,13.946999549865723 C2.296999931335449,14.092000007629395 5.038000106811523,13.029000282287598 7.564000129699707,11.567000389099121 C11.835000038146973,9.095999717712402 15.494000434875488,5.493000030517578 15.48900032043457,4.710999965667725 C15.48900032043457,4.710999965667725 17.305999755859375,-10.336000442504883 17.305999755859375,-10.336000442504883 C14.861000061035156,-8.645999908447266 1.3589999675750732,3.1059999465942383 0.2800000011920929,4.189000129699707 C-2.3450000286102295,1.565000057220459 -8.300999641418457,-5.435999870300293 -8.560999870300293,-5.986000061035156 C-8.560999870300293,-5.986000061035156 -8.468999862670898,-10.420999526977539 -9.543000221252441,-11.449000358581543 C-10.496000289916992,-12.357999801635742 -10.071999549865723,-8.835000038146973 -10.468999862670898,-9.192000389099121 C-10.902999877929688,-9.586999893188477 -13.907999992370605,-14.092000007629395 -14.96399974822998,-12.387999534606934 C-17.30500030517578,-8.592000007629395 -12.293000221252441,-4.52400016784668 -12.293000221252441,-4.52400016784668z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,557.5339965820312,403.7300109863281)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M-7.427000045776367,0.7910000085830688 C-6.3379998207092285,4.4039998054504395 -3.203000068664551,8.954999923706055 -2.5179998874664307,11.218000411987305 C1.753999948501587,8.746999740600586 7.176000118255615,7.11899995803833 7.170000076293945,6.336999893188477 C7.170000076293945,6.336999893188477 7.427000045776367,-11.218000411987305 7.427000045776367,-11.218000411987305 C4.98199987411499,-9.527999877929688 -2.740999937057495,-3.433000087738037 -7.427000045776367,0.7910000085830688z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9995760321617126,0.029116354882717133,-0.029116354882717133,0.9995760321617126,11.654052734375,-16.39007568359375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,568.6409912109375,379.24200439453125)">
                                                    <path fill="rgb(0,0,0)" fill-opacity="1"
                                                        d=" M-0.4309999942779541,6.117000102996826 C1.5410000085830688,6.206999778747559 2.5160000324249268,6.927000045776367 4.576000213623047,5.265999794006348 C7.711999893188477,2.73799991607666 9.112000465393066,-4.190000057220459 3.671999931335449,-5.744999885559082 C3.0929999351501465,-6.633999824523926 2.6670000553131104,-6.927000045776367 1.3530000448226929,-6.875 C0.03999999910593033,-6.821000099182129 -1.093999981880188,-5.948999881744385 -2.815000057220459,-5.533999919891357 C-4.535999774932861,-5.118000030517578 -7.420000076293945,-6.251999855041504 -8.265999794006348,-3.371999979019165 C-9.11299991607666,-0.492000013589859 -4.9039998054504395,0.4830000102519989 -2.7290000915527344,1.2280000448226929 C-0.5529999732971191,1.972000002861023 -0.6050000190734863,4.245999813079834 -0.4309999942779541,6.117000102996826z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,570.7100219726562,389.65399169921875)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M3.7290000915527344,2.368000030517578 C2.2190001010894775,4.938000202178955 -0.24199999868869781,5.839000225067139 -2.5859999656677246,3.8910000324249268 C-2.5859999656677246,3.8910000324249268 -3.5,-2.9579999446868896 -3.5,-2.9579999446868896 C-3.5,-2.9579999446868896 -3.7290000915527344,-4.666999816894531 -3.7290000915527344,-4.666999816894531 C-3.7290000915527344,-4.666999816894531 0.7860000133514404,-5.840000152587891 0.7860000133514404,-5.840000152587891 C0.7860000133514404,-5.840000152587891 1.1390000581741333,-4.835999965667725 1.1390000581741333,-4.835999965667725 C1.1390000581741333,-4.835999965667725 3.7290000915527344,2.368000030517578 3.7290000915527344,2.368000030517578z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,569.5430297851562,385.12799072265625)">
                                                    <path fill="rgb(240,180,180)" fill-opacity="1"
                                                        d=" M2.562000036239624,0.4059999883174896 C1.7949999570846558,2.0380001068115234 -1.1779999732971191,3.2260000705718994 -2.1610000133514404,2.867000102996826 C-2.1610000133514404,2.867000102996826 -2.562000036239624,-0.14100000262260437 -2.562000036239624,-0.14100000262260437 C-2.562000036239624,-0.14100000262260437 1.4839999675750732,-3.2269999980926514 1.4839999675750732,-3.2269999980926514 C1.4839999675750732,-3.2269999980926514 2.562000036239624,0.4059999883174896 2.562000036239624,0.4059999883174896z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,567.5419921875,382.2019958496094)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-4.925000190734863,-4.505000114440918 C-5.146999835968018,-1.9470000267028809 -4.616000175476074,3.441999912261963 -2.128999948501587,4.85099983215332 C-0.5540000200271606,5.743000030517578 3.8889999389648438,4.085000038146973 3.950000047683716,0.4909999966621399 C4.843999862670898,-0.5920000076293945 5.146999835968018,-2.746000051498413 3.9030001163482666,-3.365000009536743 C3.0169999599456787,-3.805000066757202 2.5239999294281006,-1.9040000438690186 1.190999984741211,-1.8509999513626099 C0.6859999895095825,-1.8309999704360962 1.3250000476837158,-5.410999774932861 -0.31299999356269836,-5.577000141143799 C-1.9509999752044678,-5.743000030517578 -3.2200000286102295,-4.131999969482422 -4.925000190734863,-4.505000114440918z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,541.2059936523438,455.7659912109375)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M34.48699951171875,-23.360000610351562 C34.48699951171875,-23.360000610351562 33.09000015258789,-16.19300079345703 33.09000015258789,-16.19300079345703 C33.09000015258789,-16.19300079345703 31.128000259399414,-6.136000156402588 31.128000259399414,-6.136000156402588 C21.618999481201172,-3.316999912261963 8.96500015258789,-0.675000011920929 -1.8170000314712524,-8.071999549865723 C-3.513000011444092,0.37700000405311584 -6.960000038146973,7.021999835968018 -12.26200008392334,14.223999977111816 C-13.093000411987305,15.373000144958496 -13.984999656677246,16.5310001373291 -14.92199993133545,17.71500015258789 C-14.92199993133545,17.71500015258789 -14.630000114440918,23.229000091552734 -14.630000114440918,23.229000091552734 C-14.630000114440918,23.229000091552734 -34.48699951171875,24.67799949645996 -34.48699951171875,24.67799949645996 C-34.48699951171875,24.67799949645996 -19.913999557495117,16.398000717163086 -19.913999557495117,16.398000717163086 C-19.375,15.081000328063965 -18.898000717163086,13.70300006866455 -18.474000930786133,12.288999557495117 C-14.788000106811523,0.24400000274181366 -14.364999771118164,-14.48799991607666 -5.660999774932861,-20.665000915527344 C-0.004999999888241291,-24.677000045776367 26.02899932861328,-23.360000610351562 26.02899932861328,-23.360000610351562 C26.02899932861328,-23.360000610351562 34.48699951171875,-23.360000610351562 34.48699951171875,-23.360000610351562z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,548.5150146484375,450.53900146484375)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M25.781999588012695,-10.965999603271484 C25.781999588012695,-10.965999603271484 23.81999969482422,-0.9089999794960022 23.81999969482422,-0.9089999794960022 C18.006000518798828,0.8140000104904175 11.02400016784668,2.4649999141693115 4.006999969482422,1.8109999895095825 C-0.45500001311302185,1.3869999647140503 -4.935999870300293,0.027000000700354576 -9.12399959564209,-2.8450000286102295 C-10.821000099182129,5.604000091552734 -14.267000198364258,12.24899959564209 -19.56999969482422,19.451000213623047 C-19.56999969482422,19.451000213623047 -22.336000442504883,18.5939998626709 -22.336000442504883,18.5939998626709 C-22.336000442504883,18.5939998626709 -25.781999588012695,17.516000747680664 -25.781999588012695,17.516000747680664 C-22.097000122070312,5.4710001945495605 -21.67300033569336,-9.26099967956543 -12.968999862670898,-15.437999725341797 C-7.313000202178955,-19.450000762939453 18.72100067138672,-18.132999420166016 18.72100067138672,-18.132999420166016 C18.72100067138672,-18.132999420166016 23.2810001373291,-18.132999420166016 23.2810001373291,-18.132999420166016 C23.2810001373291,-18.132999420166016 25.781999588012695,-10.965999603271484 25.781999588012695,-10.965999603271484z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,545.072021484375,454.97100830078125)">
                                                    <path fill="rgb(204,204,204)" fill-opacity="1"
                                                        d=" M18.89299964904785,-15.017999649047852 C18.89299964904785,-15.017999649047852 7.448999881744385,-2.619999885559082 7.448999881744385,-2.619999885559082 C2.9860000610351562,-3.0439999103546143 -1.49399995803833,-4.40500020980835 -5.683000087738037,-7.2769999504089355 C-7.380000114440918,1.1720000505447388 -10.826000213623047,7.815999984741211 -16.128000259399414,15.017999649047852 C-16.128000259399414,15.017999649047852 -18.893999099731445,14.16100025177002 -18.893999099731445,14.16100025177002 C-13.423999786376953,5.620999813079834 -9.586999893188477,-2.7669999599456787 -7.4770002365112305,-10.996999740600586 C-2.3519999980926514,-8.508999824523926 3.2960000038146973,-4.5279998779296875 7.3480000495910645,-5.6579999923706055 C7.3480000495910645,-5.6579999923706055 18.89299964904785,-15.017999649047852 18.89299964904785,-15.017999649047852z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,517.051025390625,476.39599609375)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M9.800000190734863,-3.634999990463257 C11.491999626159668,-1.5099999904632568 10.817999839782715,0.6800000071525574 10.3100004196167,3.9579999446868896 C10.3100004196167,3.9579999446868896 -11.491000175476074,3.9579999446868896 -11.491000175476074,3.9579999446868896 C-11.062999725341797,-2.506999969482422 -4.052999973297119,-0.765999972820282 3.0160000324249268,-3.7130000591278076 C3.555000066757202,-3.937999963760376 4.15500020980835,-3.9579999446868896 4.698999881744385,-3.746999979019165 C6.041999816894531,-3.2260000705718994 8.418000221252441,-2.3310000896453857 9.800000190734863,-3.634999990463257z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,570.802001953125,457.1000061035156)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M27.992000579833984,21.893999099731445 C27.992000579833984,21.893999099731445 14.647000312805176,21.893999099731445 14.647000312805176,21.893999099731445 C12.357999801635742,22.01799964904785 10.079000473022461,22.167999267578125 7.826000213623047,22.31800079345703 C-9.159000396728516,23.43199920654297 -24.38599967956543,24.69499969482422 -25.48200035095215,18.71299934387207 C-27.992000579833984,4.9710001945495605 -15.87600040435791,-7.4720001220703125 -4.326000213623047,-24.69499969482422 C-4.326000213623047,-24.69499969482422 13.621999740600586,-24.69499969482422 13.621999740600586,-24.69499969482422 C22.184999465942383,-0.6759999990463257 -0.04899999871850014,7.295000076293945 -12.62399959564209,12.845000267028809 C-4.502999782562256,11.14799976348877 1.7100000381469727,12.571000099182129 8.364999771118164,15.239999771118164 C9.628000259399414,15.743000030517578 10.909000396728516,16.299999237060547 12.217000007629395,16.882999420166016 C12.217000007629395,16.882999420166016 27.992000579833984,21.893999099731445 27.992000579833984,21.893999099731445z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,567.8980102539062,456.6520080566406)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-9.720000267028809,13.293000221252441 C-1.5989999771118164,11.597000122070312 4.613999843597412,13.019000053405762 11.267999649047852,15.687999725341797 C11.267999649047852,15.687999725341797 11.055000305175781,18.472000122070312 11.055000305175781,18.472000122070312 C11.055000305175781,18.472000122070312 10.729000091552734,22.766000747680664 10.729000091552734,22.766000747680664 C-6.25600004196167,23.8799991607666 -21.48200035095215,25.143999099731445 -22.577999114990234,19.160999298095703 C-25.08799934387207,5.419000148773193 -13.34000015258789,-7.921000003814697 -1.7899999618530273,-25.143999099731445 C-1.7899999618530273,-25.143999099731445 16.525999069213867,-24.246999740600586 16.525999069213867,-24.246999740600586 C25.089000701904297,-0.2280000001192093 2.8550000190734863,7.743000030517578 -9.720000267028809,13.293000221252441z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,570.176025390625,453.7640075683594)">
                                                    <path fill="rgb(204,204,204)" fill-opacity="1"
                                                        d=" M-11.99899959564209,16.180999755859375 C-3.878000020980835,14.484999656677246 2.3350000381469727,15.906999588012695 8.98900032043457,18.576000213623047 C8.98900032043457,18.576000213623047 8.777000427246094,21.360000610351562 8.777000427246094,21.360000610351562 C2.9539999961853027,17.48699951171875 -5.936999797821045,16.70199966430664 -16.160999298095703,17.259000778198242 C-10.9350004196167,10.875 11.652999877929688,2.796999931335449 12.347000122070312,-10.418000221252441 C12.647000312805176,-16.125999450683594 10.527000427246094,-21.358999252319336 10.527000427246094,-21.358999252319336 C10.527000427246094,-21.358999252319336 14.246999740600586,-21.358999252319336 14.246999740600586,-21.358999252319336 C22.809999465942383,2.6600000858306885 0.5759999752044678,10.630999565124512 -11.99899959564209,16.180999755859375z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,594.5590209960938,474.97100830078125)">
                                                    <path fill="rgb(0,0,0)" fill-opacity="1"
                                                        d=" M-9.875,-0.4580000042915344 C-10.309000015258789,-2.4579999446868896 -10.234000205993652,-4.14300012588501 -9.354999542236328,-6.013999938964844 C-8.843999862670898,-7.10099983215332 -7.514999866485596,-7.531000137329102 -6.4670000076293945,-6.941999912261963 C-6.4670000076293945,-6.941999912261963 9.27400016784668,1.8990000486373901 9.27400016784668,1.8990000486373901 C10.142000198364258,2.38700008392334 10.309000015258789,3.578000068664551 9.60099983215332,4.2769999504089355 C6.301000118255615,7.531000137329102 1.3320000171661377,3.4830000400543213 -5.451000213623047,4.02400016784668 C-9.331000328063965,4.333000183105469 -7.729000091552734,0.15299999713897705 -9.875,-0.4580000042915344z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,572.5540161132812,412.27099609375)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M13.52400016784668,-10.008000373840332 C14.380999565124512,-1.7400000095367432 14.027999877929688,12.812000274658203 13.859999656677246,18.051000595092773 C13.836000442504883,18.809999465942383 13.5,19.50200080871582 12.96399974822998,19.974000930786133 C12.67199993133545,20.233999252319336 12.312999725341797,20.43199920654297 11.916999816894531,20.540000915527344 C8.282999992370605,21.523000717163086 4.697000026702881,22.25 1.1699999570846558,22.655000686645508 C-2.005000114440918,23.027000427246094 -5.857999801635742,20.30500030517578 -8.930999755859375,20.135000228881836 C-10.130999565124512,20.066999435424805 -11.343000411987305,8.369999885559082 -12.515999794006348,0.07800000160932541 C-13.26200008392334,-5.165999889373779 -14.380999565124512,-18.242000579833984 -4.807000160217285,-21.51300048828125 C-4.807000160217285,-21.51300048828125 -3.3269999027252197,-20.208999633789062 -1.8040000200271606,-20.6560001373291 C-0.2809999883174896,-21.10300064086914 0.8700000047683716,-23.025999069213867 0.8700000047683716,-23.025999069213867 C7.519000053405762,-22.110000610351562 12.0600004196167,-17.290000915527344 12.91100025177002,-13.892999649047852 C13.168000221252441,-12.887999534606934 13.368000030517578,-11.555999755859375 13.52400016784668,-10.008000373840332z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,-1.668487548828125)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,471.81500244140625,435.156005859375)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M50.3120002746582,-46.970001220703125 C50.3120002746582,-46.970001220703125 -50.3120002746582,-46.970001220703125 -50.3120002746582,-46.970001220703125 C-60.53300094604492,-46.970001220703125 -68.81700134277344,-38.685001373291016 -68.81700134277344,-28.46500015258789 C-68.81700134277344,-28.46500015258789 -68.81700134277344,28.46500015258789 -68.81700134277344,28.46500015258789 C-68.81700134277344,38.685001373291016 -60.53200149536133,46.970001220703125 -50.3120002746582,46.970001220703125 C-50.3120002746582,46.970001220703125 50.3120002746582,46.970001220703125 50.3120002746582,46.970001220703125 C60.53200149536133,46.970001220703125 68.81700134277344,38.685001373291016 68.81700134277344,28.46500015258789 C68.81700134277344,28.46500015258789 68.81700134277344,-28.46500015258789 68.81700134277344,-28.46500015258789 C68.81700134277344,-38.685001373291016 60.53200149536133,-46.970001220703125 50.3120002746582,-46.970001220703125z">
                                                    </path>
                                                </g>
                                                <g opacity="0.916576000000001"
                                                    transform="matrix(1,0,0,1,502.2157897949219,419.1445007324219)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-8.029999732971191,0 C-8.029999732971191,-4.434999942779541 -4.435999870300293,-8.031000137329102 0,-8.031000137329102 C4.434999942779541,-8.031000137329102 8.029999732971191,-4.434999942779541 8.029999732971191,0 C8.029999732971191,4.434999942779541 4.434999942779541,8.031000137329102 0,8.031000137329102 C-4.435999870300293,8.031000137329102 -8.029999732971191,4.434999942779541 -8.029999732971191,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,471.81500244140625,443.25299072265625)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-52.47100067138672,23.31399917602539 C-52.47100067138672,23.31399917602539 -37.862998962402344,7.458000183105469 -37.862998962402344,7.458000183105469 C-37.862998962402344,7.458000183105469 -27.250999450683594,10.154999732971191 -27.250999450683594,10.154999732971191 C-27.250999450683594,10.154999732971191 -7.914999961853027,-23.31399917602539 -7.914999961853027,-23.31399917602539 C-7.914999961853027,-23.31399917602539 5.158999919891357,-3.36299991607666 5.158999919891357,-3.36299991607666 C5.158999919891357,-3.36299991607666 16.483999252319336,-9.904999732971191 16.483999252319336,-9.904999732971191 C16.483999252319336,-9.904999732971191 26.802000045776367,8.968999862670898 26.802000045776367,8.968999862670898 C26.802000045776367,8.968999862670898 35.108001708984375,0.49000000953674316 35.108001708984375,0.49000000953674316 C35.108001708984375,0.49000000953674316 52.47100067138672,23.31399917602539 52.47100067138672,23.31399917602539 C52.47100067138672,23.31399917602539 -52.47100067138672,23.31399917602539 -52.47100067138672,23.31399917602539z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9995760321617126,0.029116354882717133,-0.029116354882717133,0.9995760321617126,11.82794189453125,-16.715362548828125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,552.0469970703125,413.0169982910156)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M29.92099952697754,-2.3970000743865967 C29.92099952697754,-2.3970000743865967 23.764999389648438,5.489999771118164 18.0939998626709,9.897000312805176 C16.73699951171875,10.942000389099121 15.418000221252441,11.78600025177002 14.211000442504883,12.288000106811523 C5.85699987411499,15.75 -16.13599967956543,13.960000038146973 -16.13599967956543,13.960000038146973 C-18.427000045776367,16.389999389648438 -27.030000686645508,15.817000389099121 -28.632999420166016,13.548999786376953 C-31.469999313354492,9.491000175476074 -21.844999313354492,12.782999992370605 -20.947999954223633,10.279000282287598 C-20.947999954223633,10.279000282287598 -23.916000366210938,9.47700023651123 -22.673999786376953,7.598999977111816 C-21.44099998474121,5.710000038146973 -14.958999633789062,8.815999984741211 -14.958999633789062,8.815999984741211 C-6.558000087738037,8.253000259399414 5.52400016784668,4.611999988555908 9.005000114440918,2.3489999771118164 C9.60099983215332,1.4559999704360962 10.196999549865723,0.5630000233650208 10.812999725341797,-0.3179999887943268 C15.281000137329102,-6.639999866485596 20.509000778198242,-12.428999900817871 27.73699951171875,-16.388999938964844 C31.433000564575195,-11.843999862670898 31.47100067138672,-7.1479997634887695 29.92099952697754,-2.3970000743865967z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,575.4530029296875,407.50799560546875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M-3.496999979019165,15.553999900817871 C-3.496999979019165,15.553999900817871 -13.923999786376953,4.586999893188477 -13.923999786376953,4.586999893188477 C-9.456000328063965,-1.7350000143051147 -0.949999988079071,-15.553999900817871 5.428999900817871,-10.335000038146973 C13.92300033569336,-3.38700008392334 2.1740000247955322,11.147000312805176 -3.496999979019165,15.553999900817871z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9984192252159119,-0.05620543286204338,0.05620543286204338,0.9984192252159119,-23.566314697265625,18.8045654296875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(0.11273513734340668,0,0,0.11273513734340668,322.52099609375,428.3599853515625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M1.5640000104904175,-8.71399974822998 C-3.250999927520752,-9.57699966430664 -7.854000091552734,-6.376999855041504 -8.717000007629395,-1.562000036239624 C-9.579999923706055,3.25 -6.375,7.853000164031982 -1.5609999895095825,8.715999603271484 C3.250999927520752,9.57800006866455 7.853000164031982,6.374000072479248 8.71500015258789,1.562000036239624 C9.579000473022461,-3.253000020980835 6.375999927520752,-7.85099983215332 1.5640000104904175,-8.71399974822998z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,322.52099609375,428.3599853515625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M18.283000946044922,0.35199999809265137 C18.283000946044922,0.35199999809265137 14.685999870300293,-1.5549999475479126 14.685999870300293,-1.5549999475479126 C14.685999870300293,-1.5549999475479126 14.6850004196167,-1.5549999475479126 14.6850004196167,-1.5549999475479126 C14.571999549865723,-2.6440000534057617 14.335000038146973,-3.7109999656677246 13.98900032043457,-4.729000091552734 C13.98900032043457,-4.729000091552734 16.447999954223633,-8.003000259399414 16.447999954223633,-8.003000259399414 C16.86400032043457,-8.557000160217285 16.878000259399414,-9.315999984741211 16.48200035095215,-9.885000228881836 C16.48200035095215,-9.885000228881836 14.965999603271484,-12.060999870300293 14.965999603271484,-12.060999870300293 C14.567000389099121,-12.635000228881836 13.843999862670898,-12.885000228881836 13.175999641418457,-12.680000305175781 C13.175999641418457,-12.680000305175781 9.281000137329102,-11.482000350952148 9.281000137329102,-11.482000350952148 C9.281000137329102,-11.482000350952148 9.279000282287598,-11.482999801635742 9.279000282287598,-11.482999801635742 C8.442999839782715,-12.16100025177002 7.531000137329102,-12.74899959564209 6.548999786376953,-13.234000205993652 C6.548999786376953,-13.234000205993652 6.547999858856201,-13.234000205993652 6.547999858856201,-13.234000205993652 C6.547999858856201,-13.234000205993652 5.9710001945495605,-17.288999557495117 5.9710001945495605,-17.288999557495117 C5.874000072479248,-17.975000381469727 5.3470001220703125,-18.520999908447266 4.664999961853027,-18.64299964904785 C4.664999961853027,-18.64299964904785 2.053999900817871,-19.11199951171875 2.053999900817871,-19.11199951171875 C1.3660000562667847,-19.236000061035156 0.6779999732971191,-18.899999618530273 0.35100001096725464,-18.283000946044922 C0.35100001096725464,-18.283000946044922 -1.5570000410079956,-14.682000160217285 -1.5570000410079956,-14.682000160217285 C-1.5570000410079956,-14.682000160217285 -1.5570000410079956,-14.682000160217285 -1.5579999685287476,-14.682000160217285 C-2.6470000743865967,-14.571000099182129 -3.7070000171661377,-14.336000442504883 -4.72599983215332,-13.989999771118164 C-4.7270002365112305,-13.989999771118164 -4.7270002365112305,-13.98900032043457 -4.728000164031982,-13.98799991607666 C-4.728000164031982,-13.98799991607666 -8.003000259399414,-16.447999954223633 -8.003000259399414,-16.447999954223633 C-8.557000160217285,-16.86400032043457 -9.315999984741211,-16.878000259399414 -9.885000228881836,-16.48200035095215 C-9.885000228881836,-16.48200035095215 -12.062000274658203,-14.965999603271484 -12.062000274658203,-14.965999603271484 C-12.635000228881836,-14.567000389099121 -12.885000228881836,-13.843999862670898 -12.680000305175781,-13.177000045776367 C-12.680000305175781,-13.177000045776367 -11.484000205993652,-9.28600025177002 -11.484000205993652,-9.28600025177002 C-12.161999702453613,-8.449000358581543 -12.75100040435791,-7.5329999923706055 -13.237000465393066,-6.547999858856201 C-13.237000465393066,-6.547999858856201 -17.290000915527344,-5.9710001945495605 -17.290000915527344,-5.9710001945495605 C-17.97599983215332,-5.873000144958496 -18.52199935913086,-5.3460001945495605 -18.643999099731445,-4.664000034332275 C-18.643999099731445,-4.664000034332275 -19.11199951171875,-2.052999973297119 -19.11199951171875,-2.052999973297119 C-19.235000610351562,-1.3660000562667847 -18.899999618530273,-0.6769999861717224 -18.283000946044922,-0.3499999940395355 C-18.283000946044922,-0.3499999940395355 -14.685999870300293,1.5549999475479126 -14.685999870300293,1.5549999475479126 C-14.685999870300293,1.5549999475479126 -14.685999870300293,1.5540000200271606 -14.685999870300293,1.5549999475479126 C-14.572999954223633,2.6440000534057617 -14.335000038146973,3.7109999656677246 -13.989999771118164,4.729000091552734 C-13.989999771118164,4.729000091552734 -16.447999954223633,8.003000259399414 -16.447999954223633,8.003000259399414 C-16.86400032043457,8.557000160217285 -16.878000259399414,9.315999984741211 -16.48200035095215,9.883999824523926 C-16.48200035095215,9.883999824523926 -14.967000007629395,12.062000274658203 -14.967000007629395,12.062000274658203 C-14.567999839782715,12.635000228881836 -13.843999862670898,12.88599967956543 -13.175999641418457,12.680000305175781 C-13.175999641418457,12.680000305175781 -9.281999588012695,11.482000350952148 -9.281999588012695,11.482000350952148 C-8.446999549865723,12.15999984741211 -7.534999847412109,12.75 -6.552000045776367,13.232000350952148 C-6.552000045776367,13.232000350952148 -6.548999786376953,13.232000350952148 -6.548999786376953,13.232000350952148 C-6.548999786376953,13.232000350952148 -5.9720001220703125,17.288999557495117 -5.9720001220703125,17.288999557495117 C-5.874000072479248,17.975000381469727 -5.3470001220703125,18.52199935913086 -4.664999961853027,18.645000457763672 C-4.664999961853027,18.645000457763672 -2.053999900817871,19.11199951171875 -2.053999900817871,19.11199951171875 C-1.3660000562667847,19.235000610351562 -0.6779999732971191,18.900999069213867 -0.35100001096725464,18.284000396728516 C-0.35100001096725464,18.284000396728516 1.5540000200271606,14.6850004196167 1.5540000200271606,14.6850004196167 C1.5549999475479126,14.6850004196167 1.555999994277954,14.685999870300293 1.555999994277954,14.6850004196167 C2.6459999084472656,14.571999549865723 3.7090001106262207,14.336000442504883 4.724999904632568,13.989999771118164 C4.72599983215332,13.98900032043457 4.7270002365112305,13.989999771118164 4.728000164031982,13.989999771118164 C4.728000164031982,13.989999771118164 8.003000259399414,16.447999954223633 8.003000259399414,16.447999954223633 C8.557000160217285,16.86400032043457 9.315999984741211,16.87700080871582 9.885000228881836,16.48200035095215 C9.885000228881836,16.48200035095215 12.062000274658203,14.965999603271484 12.062000274658203,14.965999603271484 C12.635000228881836,14.567000389099121 12.885000228881836,13.843999862670898 12.680000305175781,13.175999641418457 C12.680000305175781,13.175999641418457 11.482999801635742,9.28600025177002 11.482999801635742,9.28600025177002 C12.16100025177002,8.447999954223633 12.75,7.5329999923706055 13.236000061035156,6.547999858856201 C13.236000061035156,6.547999858856201 17.288999557495117,5.9710001945495605 17.288999557495117,5.9710001945495605 C17.975000381469727,5.874000072479248 18.52199935913086,5.3470001220703125 18.643999099731445,4.664999961853027 C18.643999099731445,4.664999961853027 19.11199951171875,2.055000066757202 19.11199951171875,2.055000066757202 C19.235000610351562,1.3669999837875366 18.899999618530273,0.6790000200271606 18.283000946044922,0.35199999809265137z M-1.8619999885559082,10.392999649047852 C-7.60099983215332,9.36400032043457 -11.420999526977539,3.877000093460083 -10.392000198364258,-1.8619999885559082 C-9.36299991607666,-7.60099983215332 -3.875,-11.42199993133545 1.8639999628067017,-10.392999649047852 C7.604000091552734,-9.36400032043457 11.420000076293945,-3.877000093460083 10.390999794006348,1.8630000352859497 C9.362000465393066,7.6020002365112305 3.878000020980835,11.42199993133545 -1.8619999885559082,10.392999649047852z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9984192252159119,0.05620543286204338,-0.05620543286204338,0.9984192252159119,21.965911865234375,-19.7042236328125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(0.8872648477554321,0,0,0.8872648477554321,361.2799987792969,380.65399169921875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M0.0020000000949949026,-11.579999923706055 C-6.395999908447266,-11.579999923706055 -11.581999778747559,-6.3979997634887695 -11.581999778747559,0 C-11.581999778747559,6.394000053405762 -6.395999908447266,11.579999923706055 0.0020000000949949026,11.579999923706055 C6.395999908447266,11.579999923706055 11.581999778747559,6.394000053405762 11.581999778747559,0 C11.581999778747559,-6.3979997634887695 6.395999908447266,-11.579999923706055 0.0020000000949949026,-11.579999923706055z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,361.281005859375,380.65399169921875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M23.618000030517578,-3.7669999599456787 C23.618000030517578,-3.7669999599456787 18.547000885009766,-5.389999866485596 18.547000885009766,-5.389999866485596 C18.547000885009766,-5.389999866485596 18.547000885009766,-5.390999794006348 18.547000885009766,-5.390999794006348 C18.14900016784668,-6.767000198364258 17.597000122070312,-8.085000038146973 16.917999267578125,-9.315999984741211 C16.917999267578125,-9.315999984741211 19.32699966430664,-14.098999977111816 19.32699966430664,-14.098999977111816 C19.735000610351562,-14.907999992370605 19.577999114990234,-15.887999534606934 18.937000274658203,-16.52899932861328 C18.937000274658203,-16.52899932861328 16.482999801635742,-18.98200035095215 16.482999801635742,-18.98200035095215 C15.836999893188477,-19.628000259399414 14.847999572753906,-19.783000946044922 14.03600025177002,-19.364999771118164 C14.03600025177002,-19.364999771118164 9.29800033569336,-16.923999786376953 9.29800033569336,-16.923999786376953 C9.29800033569336,-16.923999786376953 9.295000076293945,-16.923999786376953 9.295000076293945,-16.923999786376953 C8.062999725341797,-17.604000091552734 6.751999855041504,-18.150999069213867 5.375999927520752,-18.548999786376953 C5.375999927520752,-18.548999786376953 5.375,-18.548999786376953 5.375,-18.548999786376953 C5.375,-18.548999786376953 3.697000026702881,-23.63599967956543 3.697000026702881,-23.63599967956543 C3.4130001068115234,-24.496999740600586 2.6080000400543213,-25.07900047302246 1.7020000219345093,-25.07900047302246 C1.7020000219345093,-25.07900047302246 -1.7680000066757202,-25.07900047302246 -1.7680000066757202,-25.07900047302246 C-2.680999994277954,-25.07900047302246 -3.490000009536743,-24.488000869750977 -3.7679998874664307,-23.618000030517578 C-3.7679998874664307,-23.618000030517578 -5.39300012588501,-18.541000366210938 -5.39300012588501,-18.541000366210938 C-5.39300012588501,-18.541000366210938 -5.394000053405762,-18.541000366210938 -5.394000053405762,-18.541000366210938 C-6.770999908447266,-18.14699935913086 -8.081999778747559,-17.600000381469727 -9.312999725341797,-16.920000076293945 C-9.314000129699707,-16.91900062561035 -9.3149995803833,-16.917999267578125 -9.315999984741211,-16.917999267578125 C-9.315999984741211,-16.917999267578125 -14.100000381469727,-19.327999114990234 -14.100000381469727,-19.327999114990234 C-14.909000396728516,-19.736000061035156 -15.888999938964844,-19.577999114990234 -16.530000686645508,-18.937000274658203 C-16.530000686645508,-18.937000274658203 -18.982999801635742,-16.483999252319336 -18.982999801635742,-16.483999252319336 C-19.628999710083008,-15.838000297546387 -19.784000396728516,-14.848999977111816 -19.364999771118164,-14.03600025177002 C-19.364999771118164,-14.03600025177002 -16.92799949645996,-9.305000305175781 -16.92799949645996,-9.305000305175781 C-17.60700035095215,-8.069000244140625 -18.15399932861328,-6.75600004196167 -18.552000045776367,-5.375 C-18.552000045776367,-5.375 -23.636999130249023,-3.697000026702881 -23.636999130249023,-3.697000026702881 C-24.49799919128418,-3.4130001068115234 -25.07900047302246,-2.6089999675750732 -25.07900047302246,-1.7029999494552612 C-25.07900047302246,-1.7029999494552612 -25.07900047302246,1.7669999599456787 -25.07900047302246,1.7669999599456787 C-25.07900047302246,2.680000066757202 -24.48900032043457,3.489000082015991 -23.618999481201172,3.7679998874664307 C-23.618999481201172,3.7679998874664307 -18.548999786376953,5.389999866485596 -18.548999786376953,5.389999866485596 C-18.548999786376953,5.389999866485596 -18.548999786376953,5.390999794006348 -18.548999786376953,5.390999794006348 C-18.150999069213867,6.76800012588501 -17.599000930786133,8.086000442504883 -16.920000076293945,9.317000389099121 C-16.920000076293945,9.317000389099121 -19.32900047302246,14.098999977111816 -19.32900047302246,14.098999977111816 C-19.73699951171875,14.909000396728516 -19.57900047302246,15.887999534606934 -18.937999725341797,16.52899932861328 C-18.937999725341797,16.52899932861328 -16.485000610351562,18.982999801635742 -16.485000610351562,18.982999801635742 C-15.83899974822998,19.628999710083008 -14.848999977111816,19.783000946044922 -14.036999702453613,19.364999771118164 C-14.036999702453613,19.364999771118164 -9.300000190734863,16.924999237060547 -9.300000190734863,16.924999237060547 C-8.069000244140625,17.604999542236328 -6.757999897003174,18.152000427246094 -5.381999969482422,18.54599952697754 C-5.381999969482422,18.54599952697754 -5.377999782562256,18.54599952697754 -5.377999782562256,18.54599952697754 C-5.377999782562256,18.54599952697754 -3.697999954223633,23.63599967956543 -3.697999954223633,23.63599967956543 C-3.4140000343322754,24.496999740600586 -2.609999895095825,25.07900047302246 -1.7029999494552612,25.07900047302246 C-1.7029999494552612,25.07900047302246 1.7660000324249268,25.07900047302246 1.7660000324249268,25.07900047302246 C2.678999900817871,25.07900047302246 3.48799991607666,24.488000869750977 3.7669999599456787,23.618000030517578 C3.7669999599456787,23.618000030517578 5.389999866485596,18.547000885009766 5.389999866485596,18.547000885009766 C5.390999794006348,18.54599952697754 5.390999794006348,18.54599952697754 5.392000198364258,18.54599952697754 C6.769000053405762,18.148000717163086 8.083999633789062,17.600000381469727 9.310999870300293,16.920000076293945 C9.312000274658203,16.920000076293945 9.312999725341797,16.91900062561035 9.314000129699707,16.917999267578125 C9.314000129699707,16.917999267578125 14.097999572753906,19.327999114990234 14.097999572753906,19.327999114990234 C14.907999992370605,19.736000061035156 15.887999534606934,19.577999114990234 16.52899932861328,18.937000274658203 C16.52899932861328,18.937000274658203 18.98200035095215,16.483999252319336 18.98200035095215,16.483999252319336 C19.628000259399414,15.838000297546387 19.781999588012695,14.848999977111816 19.36400032043457,14.036999702453613 C19.36400032043457,14.036999702453613 16.926000595092773,9.303999900817871 16.926000595092773,9.303999900817871 C17.606000900268555,8.069000244140625 18.152000427246094,6.75600004196167 18.549999237060547,5.375999927520752 C18.549999237060547,5.375999927520752 23.63599967956543,3.697999954223633 23.63599967956543,3.697999954223633 C24.496000289916992,3.4140000343322754 25.077999114990234,2.6089999675750732 25.077999114990234,1.7029999494552612 C25.077999114990234,1.7029999494552612 25.077999114990234,-1.7669999599456787 25.077999114990234,-1.7669999599456787 C25.077999114990234,-2.680000066757202 24.488000869750977,-3.489000082015991 23.618000030517578,-3.7669999599456787z M0.0010000000474974513,13.8100004196167 C-7.625,13.8100004196167 -13.809000015258789,7.626999855041504 -13.809000015258789,0 C-13.809000015258789,-7.625999927520752 -7.625,-13.809000015258789 0.0010000000474974513,-13.809000015258789 C7.626999855041504,-13.809000015258789 13.807000160217285,-7.625999927520752 13.807000160217285,0 C13.807000160217285,7.626999855041504 7.626999855041504,13.8100004196167 0.0010000000474974513,13.8100004196167z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,388.9490051269531,449.4330139160156)">
                                                    <path fill="rgb(255,209,128)" fill-opacity="1"
                                                        d=" M0.8090000152587891,-30.64900016784668 C-16.118000030517578,-31.100000381469727 -30.204999923706055,-17.738000869750977 -30.649999618530273,-0.8109999895095825 C-31.09600067138672,16.121000289916992 -17.740999221801758,30.20199966430664 -0.8090000152587891,30.652999877929688 C16.118999481201172,31.099000930786133 30.204999923706055,17.742000579833984 30.650999069213867,0.8100000023841858 C31.09600067138672,-16.121999740600586 17.740999221801758,-30.20400047302246 0.8090000152587891,-30.64900016784668z M26.698999404907227,0.7070000171661377 C26.30699920654297,15.449999809265137 14.036999702453613,27.086999893188477 -0.7059999704360962,26.700000762939453 C-15.447999954223633,26.308000564575195 -27.084999084472656,14.038999557495117 -26.697999954223633,-0.7039999961853027 C-26.305999755859375,-15.446000099182129 -14.036999702453613,-27.089000701904297 0.7059999704360962,-26.69700050354004 C15.449000358581543,-26.309999465942383 27.086000442504883,-14.03600025177002 26.698999404907227,0.7070000171661377z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,388.9490051269531,449.4330139160156)">
                                                    <path fill="rgb(255,188,71)" fill-opacity="1"
                                                        d=" M26.698999404907227,0.7080000042915344 C26.30699920654297,15.451000213623047 14.036999702453613,27.08799934387207 -0.7059999704360962,26.701000213623047 C-15.447999954223633,26.30900001525879 -27.084999084472656,14.039999961853027 -26.697999954223633,-0.703000009059906 C-26.305999755859375,-15.444999694824219 -14.036999702453613,-27.08799934387207 0.7059999704360962,-26.695999145507812 C15.449000358581543,-26.30900001525879 27.086000442504883,-14.03499984741211 26.698999404907227,0.7080000042915344z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,388.95001220703125,449.43499755859375)">
                                                    <path fill="rgb(230,212,177)" fill-opacity="1"
                                                        d=" M0.7049999833106995,-26.698999404907227 C-14.038000106811523,-27.090999603271484 -26.30699920654297,-15.447999954223633 -26.698999404907227,-0.7059999704360962 C-27.086000442504883,14.036999702453613 -15.447999954223633,26.305999755859375 -0.7059999704360962,26.697999954223633 C14.036999702453613,27.084999084472656 26.305999755859375,15.447999954223633 26.697999954223633,0.7049999833106995 C27.084999084472656,-14.038000106811523 15.447999954223633,-26.312000274658203 0.7049999833106995,-26.698999404907227z M24.836999893188477,0.656000018119812 C24.475000381469727,14.369999885559082 13.062000274658203,25.200000762939453 -0.6570000052452087,24.836999893188477 C-14.371000289916992,24.475000381469727 -25.200000762939453,13.057999610900879 -24.836999893188477,-0.656000018119812 C-24.475000381469727,-14.371000289916992 -13.057999610900879,-25.200000762939453 0.656000018119812,-24.836999893188477 C14.369999885559082,-24.475000381469727 25.198999404907227,-13.062999725341797 24.836999893188477,0.656000018119812z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,377.4339904785156,449.43499755859375)">
                                                    <path fill="rgb(230,212,177)" fill-opacity="1"
                                                        d=" M-6.343999862670898,-6.632999897003174 C-6.343999862670898,-6.632999897003174 -2.5160000324249268,-6.632999897003174 -2.5160000324249268,-6.632999897003174 C-2.5160000324249268,-6.632999897003174 2.4790000915527344,0.7070000171661377 2.4790000915527344,0.7070000171661377 C2.4790000915527344,0.7070000171661377 2.4790000915527344,-6.632999897003174 2.4790000915527344,-6.632999897003174 C2.4790000915527344,-6.632999897003174 6.3429999351501465,-6.632999897003174 6.3429999351501465,-6.632999897003174 C6.3429999351501465,-6.632999897003174 6.3429999351501465,6.632999897003174 6.3429999351501465,6.632999897003174 C6.3429999351501465,6.632999897003174 2.4790000915527344,6.632999897003174 2.4790000915527344,6.632999897003174 C2.4790000915527344,6.632999897003174 -2.489000082015991,-0.6510000228881836 -2.489000082015991,-0.6510000228881836 C-2.489000082015991,-0.6510000228881836 -2.489000082015991,6.632999897003174 -2.489000082015991,6.632999897003174 C-2.489000082015991,6.632999897003174 -6.343999862670898,6.632999897003174 -6.343999862670898,6.632999897003174 C-6.343999862670898,6.632999897003174 -6.343999862670898,-6.632999897003174 -6.343999862670898,-6.632999897003174z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,391.5870056152344,449.43499755859375)">
                                                    <path fill="rgb(230,212,177)" fill-opacity="1"
                                                        d=" M-5.066999912261963,-6.632999897003174 C-5.066999912261963,-6.632999897003174 5.067999839782715,-6.632999897003174 5.067999839782715,-6.632999897003174 C5.067999839782715,-6.632999897003174 5.067999839782715,-3.7820000648498535 5.067999839782715,-3.7820000648498535 C5.067999839782715,-3.7820000648498535 -0.9509999752044678,-3.7820000648498535 -0.9509999752044678,-3.7820000648498535 C-0.9509999752044678,-3.7820000648498535 -0.9509999752044678,-1.465999960899353 -0.9509999752044678,-1.465999960899353 C-0.9509999752044678,-1.465999960899353 4.190999984741211,-1.465999960899353 4.190999984741211,-1.465999960899353 C4.190999984741211,-1.465999960899353 4.190999984741211,1.2130000591278076 4.190999984741211,1.2130000591278076 C4.190999984741211,1.2130000591278076 -0.9509999752044678,1.2130000591278076 -0.9509999752044678,1.2130000591278076 C-0.9509999752044678,1.2130000591278076 -0.9509999752044678,6.632999897003174 -0.9509999752044678,6.632999897003174 C-0.9509999752044678,6.632999897003174 -5.066999912261963,6.632999897003174 -5.066999912261963,6.632999897003174 C-5.066999912261963,6.632999897003174 -5.066999912261963,-6.632999897003174 -5.066999912261963,-6.632999897003174z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,404.1610107421875,449.43499755859375)">
                                                    <path fill="rgb(230,212,177)" fill-opacity="1"
                                                        d=" M-6.230000019073486,-6.632999897003174 C-6.230000019073486,-6.632999897003174 6.230000019073486,-6.632999897003174 6.230000019073486,-6.632999897003174 C6.230000019073486,-6.632999897003174 6.230000019073486,-3.3570001125335693 6.230000019073486,-3.3570001125335693 C6.230000019073486,-3.3570001125335693 2.0490000247955322,-3.3570001125335693 2.0490000247955322,-3.3570001125335693 C2.0490000247955322,-3.3570001125335693 2.0490000247955322,6.632999897003174 2.0490000247955322,6.632999897003174 C2.0490000247955322,6.632999897003174 -2.0490000247955322,6.632999897003174 -2.0490000247955322,6.632999897003174 C-2.0490000247955322,6.632999897003174 -2.0490000247955322,-3.3570001125335693 -2.0490000247955322,-3.3570001125335693 C-2.0490000247955322,-3.3570001125335693 -6.230000019073486,-3.3570001125335693 -6.230000019073486,-3.3570001125335693 C-6.230000019073486,-3.3570001125335693 -6.230000019073486,-6.632999897003174 -6.230000019073486,-6.632999897003174z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,376.06201171875,449.43499755859375)">
                                                    <path fill="rgb(255,230,153)" fill-opacity="1"
                                                        d=" M-6.343999862670898,-6.632999897003174 C-6.343999862670898,-6.632999897003174 -2.5160000324249268,-6.632999897003174 -2.5160000324249268,-6.632999897003174 C-2.5160000324249268,-6.632999897003174 2.4790000915527344,0.7070000171661377 2.4790000915527344,0.7070000171661377 C2.4790000915527344,0.7070000171661377 2.4790000915527344,-6.632999897003174 2.4790000915527344,-6.632999897003174 C2.4790000915527344,-6.632999897003174 6.3429999351501465,-6.632999897003174 6.3429999351501465,-6.632999897003174 C6.3429999351501465,-6.632999897003174 6.3429999351501465,6.632999897003174 6.3429999351501465,6.632999897003174 C6.3429999351501465,6.632999897003174 2.4790000915527344,6.632999897003174 2.4790000915527344,6.632999897003174 C2.4790000915527344,6.632999897003174 -2.489000082015991,-0.6510000228881836 -2.489000082015991,-0.6510000228881836 C-2.489000082015991,-0.6510000228881836 -2.489000082015991,6.632999897003174 -2.489000082015991,6.632999897003174 C-2.489000082015991,6.632999897003174 -6.343999862670898,6.632999897003174 -6.343999862670898,6.632999897003174 C-6.343999862670898,6.632999897003174 -6.343999862670898,-6.632999897003174 -6.343999862670898,-6.632999897003174z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,390.21600341796875,449.43499755859375)">
                                                    <path fill="rgb(255,230,153)" fill-opacity="1"
                                                        d=" M-5.066999912261963,-6.632999897003174 C-5.066999912261963,-6.632999897003174 5.067999839782715,-6.632999897003174 5.067999839782715,-6.632999897003174 C5.067999839782715,-6.632999897003174 5.067999839782715,-3.7820000648498535 5.067999839782715,-3.7820000648498535 C5.067999839782715,-3.7820000648498535 -0.9490000009536743,-3.7820000648498535 -0.9490000009536743,-3.7820000648498535 C-0.9490000009536743,-3.7820000648498535 -0.9490000009536743,-1.465999960899353 -0.9490000009536743,-1.465999960899353 C-0.9490000009536743,-1.465999960899353 4.190999984741211,-1.465999960899353 4.190999984741211,-1.465999960899353 C4.190999984741211,-1.465999960899353 4.190999984741211,1.2130000591278076 4.190999984741211,1.2130000591278076 C4.190999984741211,1.2130000591278076 -0.9490000009536743,1.2130000591278076 -0.9490000009536743,1.2130000591278076 C-0.9490000009536743,1.2130000591278076 -0.9490000009536743,6.632999897003174 -0.9490000009536743,6.632999897003174 C-0.9490000009536743,6.632999897003174 -5.066999912261963,6.632999897003174 -5.066999912261963,6.632999897003174 C-5.066999912261963,6.632999897003174 -5.066999912261963,-6.632999897003174 -5.066999912261963,-6.632999897003174z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,402.7900085449219,449.43499755859375)">
                                                    <path fill="rgb(255,230,153)" fill-opacity="1"
                                                        d=" M-6.230000019073486,-6.632999897003174 C-6.230000019073486,-6.632999897003174 6.230000019073486,-6.632999897003174 6.230000019073486,-6.632999897003174 C6.230000019073486,-6.632999897003174 6.230000019073486,-3.3570001125335693 6.230000019073486,-3.3570001125335693 C6.230000019073486,-3.3570001125335693 2.0490000247955322,-3.3570001125335693 2.0490000247955322,-3.3570001125335693 C2.0490000247955322,-3.3570001125335693 2.0490000247955322,6.632999897003174 2.0490000247955322,6.632999897003174 C2.0490000247955322,6.632999897003174 -2.0490000247955322,6.632999897003174 -2.0490000247955322,6.632999897003174 C-2.0490000247955322,6.632999897003174 -2.0490000247955322,-3.3570001125335693 -2.0490000247955322,-3.3570001125335693 C-2.0490000247955322,-3.3570001125335693 -6.230000019073486,-3.3570001125335693 -6.230000019073486,-3.3570001125335693 C-6.230000019073486,-3.3570001125335693 -6.230000019073486,-6.632999897003174 -6.230000019073486,-6.632999897003174z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0,0,0,0,595.291015625,376.4590148925781)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,636.22802734375,374.61700439453125)">
                                                    <path fill="rgb(48,51,77)" fill-opacity="1"
                                                        d=" M0,-0.06199999898672104 C0.07999999821186066,-0.06199999898672104 0.07999999821186066,0.06199999898672104 0,0.06199999898672104 C-0.07999999821186066,0.06199999898672104 -0.07999999821186066,-0.06199999898672104 0,-0.06199999898672104z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,623.3330078125,358.989990234375)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-14.989999771118164,-11.326000213623047 C-8.9350004196167,-15.934000015258789 -1.0360000133514404,-17.81800079345703 6.546999931335449,-17.180999755859375 C11.447999954223633,-16.770000457763672 16.336999893188477,-15.314000129699707 20.33099937438965,-12.444999694824219 C24.326000213623047,-9.574999809265137 27.350000381469727,-5.203000068664551 28.04199981689453,-0.33399999141693115 C28.733999252319336,4.535999774932861 26.868999481201172,9.824999809265137 22.91900062561035,12.753999710083008 C18.742000579833984,15.850000381469727 13.071999549865723,15.97599983215332 7.928999900817871,15.215999603271484 C1.4199999570846558,14.253000259399414 -4.870999813079834,12.092000007629395 -11.390000343322754,11.20199966430664 C-14.086999893188477,10.833999633789062 -16.93600082397461,10.706000328063965 -19.427000045776367,11.802000045776367 C-21.91900062561035,12.89799976348877 -23.89699935913086,14.1899995803833 -24.70800018310547,17.81800079345703 C-28.733999252319336,2.2850000858306885 -21.04599952697754,-6.7179999351501465 -14.989999771118164,-11.326000213623047z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,642.1950073242188,360.4519958496094)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M2.3380000591278076,0 C2.3380000591278076,-1.2910000085830688 1.2910000085830688,-2.3380000591278076 0,-2.3380000591278076 C-1.2910000085830688,-2.3380000591278076 -2.3380000591278076,-1.2910000085830688 -2.3380000591278076,0 C-2.3380000591278076,1.2910000085830688 -1.2910000085830688,2.3380000591278076 0,2.3380000591278076 C1.2910000085830688,2.3380000591278076 2.3380000591278076,1.2910000085830688 2.3380000591278076,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,632.7529907226562,360.4519958496094)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M2.3380000591278076,0 C2.3380000591278076,-1.2910000085830688 1.2910000085830688,-2.3380000591278076 0,-2.3380000591278076 C-1.2910000085830688,-2.3380000591278076 -2.3380000591278076,-1.2910000085830688 -2.3380000591278076,0 C-2.3380000591278076,1.2910000085830688 -1.2910000085830688,2.3380000591278076 0,2.3380000591278076 C1.2910000085830688,2.3380000591278076 2.3380000591278076,1.2910000085830688 2.3380000591278076,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,623.3109741210938,360.4519958496094)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M2.3380000591278076,0 C2.3380000591278076,-1.2910000085830688 1.2910000085830688,-2.3380000591278076 0,-2.3380000591278076 C-1.2910000085830688,-2.3380000591278076 -2.3380000591278076,-1.2910000085830688 -2.3380000591278076,0 C-2.3380000591278076,1.2910000085830688 -1.2910000085830688,2.3380000591278076 0,2.3380000591278076 C1.2910000085830688,2.3380000591278076 2.3380000591278076,1.2910000085830688 2.3380000591278076,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,613.8690185546875,360.4519958496094)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M2.3380000591278076,0 C2.3380000591278076,-1.2910000085830688 1.2910000085830688,-2.3380000591278076 0,-2.3380000591278076 C-1.2910000085830688,-2.3380000591278076 -2.3380000591278076,-1.2910000085830688 -2.3380000591278076,0 C-2.3380000591278076,1.2910000085830688 -1.2910000085830688,2.3380000591278076 0,2.3380000591278076 C1.2910000085830688,2.3380000591278076 2.3380000591278076,1.2910000085830688 2.3380000591278076,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,635.89697265625,374.7040100097656)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-16.63800048828125,-0.04800000041723251 C-16.011999130249023,0.09200000017881393 -15.432000160217285,0.39399999380111694 -14.831999778747559,0.6179999709129333 C-14.22700023651123,0.8429999947547913 -13.61400032043457,1.0470000505447388 -13.001999855041504,1.2510000467300415 C-11.77299976348877,1.659999966621399 -10.545999526977539,2.066999912261963 -9.291000366210938,2.3919999599456787 C-6.793000221252441,3.0380001068115234 -4.223999977111816,3.4240000247955322 -1.6419999599456787,3.4649999141693115 C2.9000000953674316,3.5390000343322754 7.4629998207092285,2.5199999809265137 11.373000144958496,0.16099999845027924 C13.491999626159668,-1.1180000305175781 15.435999870300293,-2.819000005722046 16.944000244140625,-4.784999847412109 C17.016000747680664,-4.879000186920166 17.176000595092773,-4.795000076293945 17.107999801635742,-4.689000129699707 C15.89799976348877,-2.802999973297119 14.394000053405762,-1.2359999418258667 12.59000015258789,0.09200000017881393 C10.763999938964844,1.4359999895095825 8.722999572753906,2.4690001010894775 6.583000183105469,3.2070000171661377 C1.7400000095367432,4.877999782562256 -3.440000057220459,4.8520002365112305 -8.404000282287598,3.7300000190734863 C-9.871999740600586,3.3989999294281006 -11.338000297546387,2.990999937057495 -12.751999855041504,2.4700000286102295 C-13.449999809265137,2.2119998931884766 -14.135000228881836,1.9149999618530273 -14.810999870300293,1.6050000190734863 C-15.506999969482422,1.284999966621399 -16.277000427246094,1.0119999647140503 -16.889999389648438,0.5490000247955322 C-17.177000045776367,0.3330000042915344 -17.020999908447266,-0.1340000033378601 -16.63800048828125,-0.04800000041723251z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,600.2119750976562,353.19500732421875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-4.636000156402588,5.465000152587891 C-4.0329999923706055,2.9509999752044678 -2.8919999599456787,0.593999981880188 -1.312999963760376,-1.4520000219345093 C-0.5199999809265137,-2.4790000915527344 0.38600000739097595,-3.4189999103546143 1.38100004196167,-4.250999927520752 C1.847000002861023,-4.639999866485596 2.3299999237060547,-5.010000228881836 2.8320000171661377,-5.349999904632568 C3.3359999656677246,-5.690999984741211 3.934999942779541,-6.083000183105469 4.519999980926514,-6.258999824523926 C4.659999847412109,-6.301000118255615 4.7769999504089355,-6.109000205993652 4.664999961853027,-6.008999824523926 C4.230999946594238,-5.616000175476074 3.691999912261963,-5.302999973297119 3.2209999561309814,-4.951000213623047 C2.7019999027252197,-4.563000202178955 2.200000047683716,-4.1579999923706055 1.7239999771118164,-3.7200000286102295 C0.8240000009536743,-2.8929998874664307 0.009999999776482582,-1.9739999771118164 -0.7089999914169312,-0.9860000014305115 C-2.1649999618530273,1.0160000324249268 -3.203000068664551,3.296999931335449 -3.7339999675750732,5.715000152587891 C-3.86299991607666,6.301000118255615 -4.7769999504089355,6.052999973297119 -4.636000156402588,5.465000152587891z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,615.052978515625,373.73199462890625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-1.6510000228881836,-0.6700000166893005 C-1.3839999437332153,-0.8100000023841858 -1.0130000114440918,-0.7620000243186951 -0.718999981880188,-0.7350000143051147 C-0.42500001192092896,-0.7070000171661377 -0.13099999725818634,-0.6610000133514404 0.1550000011920929,-0.5879999995231628 C0.43299999833106995,-0.5170000195503235 0.7020000219345093,-0.421999990940094 0.9620000123977661,-0.3019999861717224 C1.2389999628067017,-0.17399999499320984 1.5520000457763672,-0.03500000014901161 1.7269999980926514,0.22200000286102295 C1.8730000257492065,0.43799999356269836 1.6829999685287476,0.8100000023841858 1.409000039100647,0.7770000100135803 C1.3020000457763672,0.7639999985694885 1.215000033378601,0.7540000081062317 1.1180000305175781,0.7080000042915344 C1.0579999685287476,0.6790000200271606 1,0.6460000276565552 0.9409999847412109,0.6169999837875366 C0.8360000252723694,0.5659999847412109 0.7710000276565552,0.5370000004768372 0.6779999732971191,0.5009999871253967 C0.4390000104904175,0.40799999237060547 0.19599999487400055,0.3240000009536743 -0.052000001072883606,0.26100000739097595 C-0.17299999296665192,0.23100000619888306 -0.29499998688697815,0.20499999821186066 -0.4180000126361847,0.18400000035762787 C-0.550000011920929,0.16099999845027924 -0.578000009059906,0.15800000727176666 -0.718999981880188,0.1459999978542328 C-0.8429999947547913,0.13500000536441803 -0.9670000076293945,0.1289999932050705 -1.090999960899353,0.12800000607967377 C-1.2419999837875366,0.12700000405311584 -1.409000039100647,0.15700000524520874 -1.5509999990463257,0.10199999809265137 C-1.7100000381469727,0.03999999910593033 -1.8320000171661377,-0.07000000029802322 -1.8539999723434448,-0.24899999797344208 C-1.8730000257492065,-0.4099999964237213 -1.8020000457763672,-0.5910000205039978 -1.6510000228881836,-0.6700000166893005z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0,0,0,0,217.0070037841797,337.3909912109375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,197.3560028076172,318.8590087890625)">
                                                    <path fill="rgb(247,189,90)" fill-opacity="1"
                                                        d=" M-14.977999687194824,-11.97700023651123 C-7.107999801635742,-25.700000762939453 21.54400062561035,-24.270000457763672 24.242000579833984,-6.177000045776367 C25.548999786376953,0.3779999911785126 19.93400001525879,5.933000087738037 14.805000305175781,8.972000122070312 C6.870999813079834,13.67199993133545 14.09000015258789,18.288999557495117 21.584999084472656,17.224000930786133 C5.039000034332275,25.700000762939453 -25.548999786376953,6.455999851226807 -14.977999687194824,-11.97700023651123z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,183.0500030517578,306.739990234375)">
                                                    <path fill="rgb(247,189,90)" fill-opacity="1"
                                                        d=" M-7.557000160217285,6.760000228881836 C-7.521999835968018,5.839000225067139 -7.415999889373779,4.921000003814697 -7.238999843597412,4.017000198364258 C-6.886000156402588,2.2139999866485596 -6.254000186920166,0.460999995470047 -5.381999969482422,-1.156000018119812 C-3.7249999046325684,-4.230999946594238 -1.1399999856948853,-6.754000186920166 1.9429999589920044,-8.387999534606934 C3.6480000019073486,-9.291999816894531 5.480999946594238,-9.897000312805176 7.400000095367432,-10.097999572753906 C7.610000133514404,-10.119999885559082 7.644000053405762,-9.788999557495117 7.448999881744385,-9.736000061035156 C4.295000076293945,-8.87399959564209 1.3200000524520874,-7.429999828338623 -1.0640000104904175,-5.159999847412109 C-3.5250000953674316,-2.815999984741211 -5.224999904632568,0.2070000022649765 -5.961999893188477,3.5260000228881836 C-6.175000190734863,4.482999801635742 -6.308000087738037,5.455999851226807 -6.360000133514404,6.434999942779541 C-6.413000106811523,7.410999774932861 -6.252999782562256,8.394000053405762 -6.315000057220459,9.362000465393066 C-6.354000091552734,9.965999603271484 -7.210000038146973,10.121000289916992 -7.390999794006348,9.508000373840332 C-7.64300012588501,8.652000427246094 -7.590000152587891,7.64300012588501 -7.557000160217285,6.760000228881836z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,177.17300415039062,321.0769958496094)">
                                                    <path fill="rgb(247,189,90)" fill-opacity="1"
                                                        d=" M-0.984000027179718,-1.3550000190734863 C-0.9760000109672546,-1.430999994277954 -0.9660000205039978,-1.5069999694824219 -0.9539999961853027,-1.5829999446868896 C-0.9409999847412109,-1.6670000553131104 -0.8930000066757202,-1.7109999656677246 -0.8659999966621399,-1.784999966621399 C-0.8360000252723694,-1.8680000305175781 -0.7110000252723694,-1.930999994277954 -0.6430000066757202,-1.8450000286102295 C-0.6029999852180481,-1.7929999828338623 -0.5450000166893005,-1.7660000324249268 -0.5040000081062317,-1.7120000123977661 C-0.46399998664855957,-1.659000039100647 -0.4339999854564667,-1.5950000286102295 -0.4000000059604645,-1.5379999876022339 C-0.3310000002384186,-1.4199999570846558 -0.26600000262260437,-1.2990000247955322 -0.20200000703334808,-1.1779999732971191 C-0.057999998331069946,-0.9070000052452087 0.04100000113248825,-0.6140000224113464 0.16099999845027924,-0.3319999873638153 C0.13600000739097595,-0.38999998569488525 0.20900000631809235,-0.22499999403953552 0.21699999272823334,-0.20800000429153442 C0.2460000067949295,-0.14300000667572021 0.27799999713897705,-0.07999999821186066 0.3089999854564667,-0.01600000075995922 C0.3720000088214874,0.1120000034570694 0.43799999356269836,0.23800000548362732 0.5070000290870667,0.3630000054836273 C0.5770000219345093,0.4880000054836273 0.6510000228881836,0.6110000014305115 0.7269999980926514,0.7319999933242798 C0.765999972820282,0.7919999957084656 0.8080000281333923,0.8489999771118164 0.8450000286102295,0.9100000262260437 C0.9200000166893005,1.0329999923706055 0.9399999976158142,1.1390000581741333 0.9570000171661377,1.2799999713897705 C1.00600004196167,1.690999984741211 0.47200000286102295,1.930999994277954 0.15600000321865082,1.7410000562667847 C-0.13199999928474426,1.5670000314712524 -0.2840000092983246,1.1729999780654907 -0.42399999499320984,0.878000020980835 C-0.5619999766349792,0.5879999995231628 -0.6869999766349792,0.28999999165534973 -0.7940000295639038,-0.014000000432133675 C-0.8949999809265137,-0.3019999861717224 -0.9779999852180481,-0.6039999723434448 -0.996999979019165,-0.9110000133514404 C-1.00600004196167,-1.055999994277954 -1,-1.2109999656677246 -0.984000027179718,-1.3550000190734863z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,218.30599975585938,326.9849853515625)">
                                                    <path fill="rgb(247,189,90)" fill-opacity="1"
                                                        d=" M-3.9260001182556152,3.2839999198913574 C-3.5320000648498535,3.055999994277954 -3.0510001182556152,2.9519999027252197 -2.634000062942505,2.7639999389648438 C-2.2100000381469727,2.572000026702881 -1.7970000505447388,2.3570001125335693 -1.3980000019073486,2.11899995803833 C-0.6110000014305115,1.6510000228881836 0.11900000274181366,1.097000002861023 0.7820000052452087,0.46700000762939453 C1.4500000476837158,-0.1679999977350235 2.059999942779541,-0.8560000061988831 2.5799999237060547,-1.6180000305175781 C3.0959999561309814,-2.375 3.5950000286102295,-3.184999942779541 3.9609999656677246,-4.025000095367432 C4.031000137329102,-4.184999942779541 4.285999774932861,-4.071000099182129 4.238999843597412,-3.9079999923706055 C3.9619998931884766,-2.9590001106262207 3.628999948501587,-2.0869998931884766 3.1089999675750732,-1.2419999837875366 C2.5980000495910645,-0.41100001335144043 1.9910000562667847,0.37700000405311584 1.2990000247955322,1.065999984741211 C0.6169999837875366,1.746999979019165 -0.14399999380111694,2.3550000190734863 -0.9580000042915344,2.871999979019165 C-1.3830000162124634,3.1410000324249268 -1.8240000009536743,3.384999990463257 -2.2780001163482666,3.6010000705718994 C-2.7249999046325684,3.812000036239624 -3.2070000171661377,4.060999870300293 -3.697999954223633,4.125 C-4.160999774932861,4.184999942779541 -4.284999847412109,3.492000102996826 -3.9260001182556152,3.2839999198913574z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,187.98599243164062,313.5899963378906)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-1.6929999589920044,0 C-1.6929999589920044,-0.9350000023841858 -0.9350000023841858,-1.6929999589920044 0,-1.6929999589920044 C0.9350000023841858,-1.6929999589920044 1.6929999589920044,-0.9350000023841858 1.6929999589920044,0 C1.6929999589920044,0.9350000023841858 0.9350000023841858,1.6929999589920044 0,1.6929999589920044 C-0.9350000023841858,1.6929999589920044 -1.6929999589920044,0.9350000023841858 -1.6929999589920044,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,195.6060028076172,313.5899963378906)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-1.6929999589920044,0 C-1.6929999589920044,-0.9350000023841858 -0.9350000023841858,-1.6929999589920044 0,-1.6929999589920044 C0.9350000023841858,-1.6929999589920044 1.6929999589920044,-0.9350000023841858 1.6929999589920044,0 C1.6929999589920044,0.9350000023841858 0.9350000023841858,1.6929999589920044 0,1.6929999589920044 C-0.9350000023841858,1.6929999589920044 -1.6929999589920044,0.9350000023841858 -1.6929999589920044,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,203.2270050048828,313.5899963378906)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-1.6929999589920044,0 C-1.6929999589920044,-0.9350000023841858 -0.9350000023841858,-1.6929999589920044 0,-1.6929999589920044 C0.9350000023841858,-1.6929999589920044 1.6929999589920044,-0.9350000023841858 1.6929999589920044,0 C1.6929999589920044,0.9350000023841858 0.9350000023841858,1.6929999589920044 0,1.6929999589920044 C-0.9350000023841858,1.6929999589920044 -1.6929999589920044,0.9350000023841858 -1.6929999589920044,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,211.2239990234375,313.5899963378906)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-1.6929999589920044,0 C-1.6929999589920044,-0.9350000023841858 -0.9350000023841858,-1.6929999589920044 0,-1.6929999589920044 C0.9350000023841858,-1.6929999589920044 1.6929999589920044,-0.9350000023841858 1.6929999589920044,0 C1.6929999589920044,0.9350000023841858 0.9350000023841858,1.6929999589920044 0,1.6929999589920044 C-0.9350000023841858,1.6929999589920044 -1.6929999589920044,0.9350000023841858 -1.6929999589920044,0z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g style="display: block;" transform="matrix(1,0,0,1,0,0)"
                                                opacity="1">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,417.7640075683594,265.0669860839844)">
                                                    <path fill="rgb(89,119,201)" fill-opacity="1"
                                                        d=" M21.493000030517578,44.1609992980957 C21.493000030517578,44.1609992980957 21.604999542236328,43.8390007019043 21.604999542236328,43.8390007019043 C21.604999542236328,43.8390007019043 21.395999908447266,44.21900177001953 21.395999908447266,44.21900177001953 C21.395999908447266,44.21900177001953 21.493000030517578,44.1609992980957 21.493000030517578,44.1609992980957z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,460.9809875488281,265.0669860839844)">
                                                    <path fill="rgb(71,102,186)" fill-opacity="1"
                                                        d=" M-21.13800048828125,43.96900177001953 C-21.13800048828125,43.96900177001953 -21.604000091552734,43.8390007019043 -21.604000091552734,43.8390007019043 C-21.604000091552734,43.8390007019043 -21.61199951171875,43.8390007019043 -21.61199951171875,43.8390007019043 C-21.61199951171875,43.8390007019043 -21.722999572753906,44.1609992980957 -21.722999572753906,44.1609992980957 C-21.722999572753906,44.1609992980957 -21.13800048828125,43.96900177001953 -21.13800048828125,43.96900177001953z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,417.7640075683594,335.67999267578125)">
                                                    <path fill="rgb(49,102,186)" fill-opacity="1"
                                                        d=" M21.604999542236328,-20.041000366210938 C21.604999542236328,-20.041000366210938 21.493000030517578,-19.59600067138672 21.493000030517578,-19.59600067138672 C21.493000030517578,-19.59600067138672 21.895999908447266,-19.40399932861328 21.895999908447266,-19.40399932861328 C21.895999908447266,-19.40399932861328 21.604999542236328,-20.041000366210938 21.604999542236328,-20.041000366210938z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,460.9809875488281,335.67999267578125)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-1.3248000144958496,-27.185333251953125 C-1.3248000144958496,-27.185333251953125 -21.671554565429688,6.126399993896484 -21.671554565429688,6.126399993896484 C-21.671554565429688,6.126399993896484 -21.61199951171875,-20.041000366210938 -21.61199951171875,-20.041000366210938 C-21.61199951171875,-20.041000366210938 -21.604000091552734,-20.041000366210938 -21.604000091552734,-20.041000366210938 C-21.604000091552734,-20.041000366210938 -1.3248000144958496,-27.185333251953125 -1.3248000144958496,-27.185333251953125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,487.8800048828125,69.9280014038086)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-8.880000114440918,-18.694000244140625 C-8.880000114440918,-18.694000244140625 8.878999710083008,-18.694000244140625 8.878999710083008,-18.694000244140625 C15.538999557495117,-18.694000244140625 20.937000274658203,-13.295999526977539 20.937000274658203,-6.636000156402588 C20.937000274658203,-6.636000156402588 20.937000274658203,18.694000244140625 20.937000274658203,18.694000244140625 C20.937000274658203,18.694000244140625 15.177000045776367,18.694000244140625 15.177000045776367,18.694000244140625 C15.177000045776367,18.694000244140625 15.177000045776367,-5.921000003814697 15.177000045776367,-5.921000003814697 C15.177000045776367,-9.793999671936035 12.036999702453613,-12.934000015258789 8.163999557495117,-12.934000015258789 C8.163999557495117,-12.934000015258789 -8.163999557495117,-12.934000015258789 -8.163999557495117,-12.934000015258789 C-12.036999702453613,-12.934000015258789 -15.177000045776367,-9.793999671936035 -15.177000045776367,-5.920000076293945 C-15.177000045776367,-5.920000076293945 -15.177000045776367,18.694000244140625 -15.177000045776367,18.694000244140625 C-15.177000045776367,18.694000244140625 -20.937000274658203,18.694000244140625 -20.937000274658203,18.694000244140625 C-20.937000274658203,18.694000244140625 -20.937000274658203,-6.636000156402588 -20.937000274658203,-6.636000156402588 C-20.937000274658203,-13.295999526977539 -15.538999557495117,-18.694000244140625 -8.880000114440918,-18.694000244140625z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,487.8789978027344,103.36900329589844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M34.02299880981445,-16.632999420166016 C34.02299880981445,-16.632999420166016 34.02299880981445,-7.379000186920166 34.02299880981445,-7.379000186920166 C34.02299880981445,11.411999702453613 18.790000915527344,26.645000457763672 0,26.645000457763672 C-18.790000915527344,26.645000457763672 -34.02299880981445,11.411999702453613 -34.02299880981445,-7.379000186920166 C-34.02299880981445,-7.379000186920166 -34.02299880981445,-16.632999420166016 -34.02299880981445,-16.632999420166016 C-34.02299880981445,-22.163000106811523 -29.541000366210938,-26.645000457763672 -24.01099967956543,-26.645000457763672 C-24.01099967956543,-26.645000457763672 24.01099967956543,-26.645000457763672 24.01099967956543,-26.645000457763672 C29.541000366210938,-26.645000457763672 34.02299880981445,-22.163000106811523 34.02299880981445,-16.632999420166016z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.739013671875,101.88300323486328)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M14.192999839782715,0 C14.192999839782715,-7.8379998207092285 7.839000225067139,-14.192999839782715 0,-14.192999839782715 C-7.8379998207092285,-14.192999839782715 -14.192999839782715,-7.8379998207092285 -14.192999839782715,0 C-14.192999839782715,7.839000225067139 -7.8379998207092285,14.192999839782715 0,14.192999839782715 C7.839000225067139,14.192999839782715 14.192999839782715,7.839000225067139 14.192999839782715,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.739013671875,101.88300323486328)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M12.355999946594238,0 C12.355999946594238,-6.823999881744385 6.823999881744385,-12.355999946594238 0,-12.355999946594238 C-6.823999881744385,-12.355999946594238 -12.355999946594238,-6.823999881744385 -12.355999946594238,0 C-12.355999946594238,6.823999881744385 -6.823999881744385,12.355999946594238 0,12.355999946594238 C6.823999881744385,12.355999946594238 12.355999946594238,6.823999881744385 12.355999946594238,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.760986328125,102.44400024414062)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-4.133999824523926,-4.708000183105469 C-4.133999824523926,-7.053999900817871 -2.1700000762939453,-8.940999984741211 0.2029999941587448,-8.8149995803833 C2.315999984741211,-8.70199966430664 4.0289998054504395,-6.942999839782715 4.090000152587891,-4.828000068664551 C4.133999824523926,-3.2850000858306885 3.325000047683716,-1.9299999475479126 2.1029999256134033,-1.190000057220459 C2.1029999256134033,-1.190000057220459 2.8489999771118164,8.222999572753906 2.8489999771118164,8.222999572753906 C2.880000114440918,8.609999656677246 2.575000047683716,8.9399995803833 2.187000036239624,8.9399995803833 C2.187000036239624,8.9399995803833 -2.2300000190734863,8.9399995803833 -2.2300000190734863,8.9399995803833 C-2.617000102996826,8.9399995803833 -2.921999931335449,8.609999656677246 -2.8919999599456787,8.222999572753906 C-2.8919999599456787,8.222999572753906 -2.1449999809265137,-1.190000057220459 -2.1449999809265137,-1.190000057220459 C-3.3359999656677246,-1.9110000133514404 -4.133999824523926,-3.2149999141693115 -4.133999824523926,-4.708000183105469z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,487.8800048828125,69.9280014038086)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-8.880000114440918,-18.694000244140625 C-8.880000114440918,-18.694000244140625 8.878999710083008,-18.694000244140625 8.878999710083008,-18.694000244140625 C15.538999557495117,-18.694000244140625 20.937000274658203,-13.295999526977539 20.937000274658203,-6.636000156402588 C20.937000274658203,-6.636000156402588 20.937000274658203,18.694000244140625 20.937000274658203,18.694000244140625 C20.937000274658203,18.694000244140625 15.177000045776367,18.694000244140625 15.177000045776367,18.694000244140625 C15.177000045776367,18.694000244140625 15.177000045776367,-5.921000003814697 15.177000045776367,-5.921000003814697 C15.177000045776367,-9.793999671936035 12.036999702453613,-12.934000015258789 8.163999557495117,-12.934000015258789 C8.163999557495117,-12.934000015258789 -8.163999557495117,-12.934000015258789 -8.163999557495117,-12.934000015258789 C-12.036999702453613,-12.934000015258789 -15.177000045776367,-9.793999671936035 -15.177000045776367,-5.920000076293945 C-15.177000045776367,-5.920000076293945 -15.177000045776367,18.694000244140625 -15.177000045776367,18.694000244140625 C-15.177000045776367,18.694000244140625 -20.937000274658203,18.694000244140625 -20.937000274658203,18.694000244140625 C-20.937000274658203,18.694000244140625 -20.937000274658203,-6.636000156402588 -20.937000274658203,-6.636000156402588 C-20.937000274658203,-13.295999526977539 -15.538999557495117,-18.694000244140625 -8.880000114440918,-18.694000244140625z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,487.8789978027344,103.36900329589844)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M34.02299880981445,-16.632999420166016 C34.02299880981445,-16.632999420166016 34.02299880981445,-7.379000186920166 34.02299880981445,-7.379000186920166 C34.02299880981445,11.411999702453613 18.790000915527344,26.645000457763672 0,26.645000457763672 C-18.790000915527344,26.645000457763672 -34.02299880981445,11.411999702453613 -34.02299880981445,-7.379000186920166 C-34.02299880981445,-7.379000186920166 -34.02299880981445,-16.632999420166016 -34.02299880981445,-16.632999420166016 C-34.02299880981445,-22.163000106811523 -29.541000366210938,-26.645000457763672 -24.01099967956543,-26.645000457763672 C-24.01099967956543,-26.645000457763672 24.01099967956543,-26.645000457763672 24.01099967956543,-26.645000457763672 C29.541000366210938,-26.645000457763672 34.02299880981445,-22.163000106811523 34.02299880981445,-16.632999420166016z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.739013671875,101.88300323486328)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M14.192999839782715,0 C14.192999839782715,-7.8379998207092285 7.839000225067139,-14.192999839782715 0,-14.192999839782715 C-7.8379998207092285,-14.192999839782715 -14.192999839782715,-7.8379998207092285 -14.192999839782715,0 C-14.192999839782715,7.839000225067139 -7.8379998207092285,14.192999839782715 0,14.192999839782715 C7.839000225067139,14.192999839782715 14.192999839782715,7.839000225067139 14.192999839782715,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.739013671875,101.88300323486328)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M12.355999946594238,0 C12.355999946594238,-6.823999881744385 6.823999881744385,-12.355999946594238 0,-12.355999946594238 C-6.823999881744385,-12.355999946594238 -12.355999946594238,-6.823999881744385 -12.355999946594238,0 C-12.355999946594238,6.823999881744385 -6.823999881744385,12.355999946594238 0,12.355999946594238 C6.823999881744385,12.355999946594238 12.355999946594238,6.823999881744385 12.355999946594238,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,486.760986328125,102.44400024414062)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-4.133999824523926,-4.708000183105469 C-4.133999824523926,-7.053999900817871 -2.1700000762939453,-8.940999984741211 0.2029999941587448,-8.8149995803833 C2.315999984741211,-8.70199966430664 4.0289998054504395,-6.942999839782715 4.090000152587891,-4.828000068664551 C4.133999824523926,-3.2850000858306885 3.325000047683716,-1.9299999475479126 2.1029999256134033,-1.190000057220459 C2.1029999256134033,-1.190000057220459 2.8489999771118164,8.222999572753906 2.8489999771118164,8.222999572753906 C2.880000114440918,8.609999656677246 2.575000047683716,8.9399995803833 2.187000036239624,8.9399995803833 C2.187000036239624,8.9399995803833 -2.2300000190734863,8.9399995803833 -2.2300000190734863,8.9399995803833 C-2.617000102996826,8.9399995803833 -2.921999931335449,8.609999656677246 -2.8919999599456787,8.222999572753906 C-2.8919999599456787,8.222999572753906 -2.1449999809265137,-1.190000057220459 -2.1449999809265137,-1.190000057220459 C-3.3359999656677246,-1.9110000133514404 -4.133999824523926,-3.2149999141693115 -4.133999824523926,-4.708000183105469z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9983099699020386,0.058113232254981995,-0.058113232254981995,0.9983099699020386,20.49444580078125,-13.488067626953125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,255.4929962158203,335.7820129394531)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-15.39799976348877,12.23799991607666 C-15.39799976348877,12.23799991607666 -10.086999893188477,18.868999481201172 -10.086999893188477,18.868999481201172 C-10.086999893188477,18.868999481201172 -9.72700023651123,19.31599998474121 -9.72700023651123,19.31599998474121 C-9.72700023651123,19.31599998474121 -8.826000213623047,18.704999923706055 -7.454999923706055,17.7450008392334 C-6.364999771118164,16.975000381469727 -4.97599983215332,15.986000061035156 -3.515000104904175,14.90999984741211 C0.032999999821186066,12.295999526977539 4.031000137329102,9.159000396728516 5.380000114440918,7.360000133514404 C8.527999877929688,3.1679999828338623 10.618000030517578,-9.166000366210938 10.618000030517578,-9.166000366210938 C13.67199993133545,-10.699999809265137 15.39799976348877,-13.87399959564209 14.456999778747559,-17.13800048828125 C13.831000328063965,-19.315000534057617 11.40999984741211,-13.666000366210938 9.604000091552734,-13.28499984741211 C9.604000091552734,-13.28499984741211 8.949999809265137,-18.12299919128418 7.914000034332275,-16.582000732421875 C6.729000091552734,-14.819000244140625 7.281000137329102,-10.475000381469727 7.281000137329102,-10.475000381469727 C7.281000137329102,-10.475000381469727 -0.3449999988079071,2.3540000915527344 -0.3449999988079071,2.3540000915527344 C-0.3449999988079071,2.3540000915527344 -6.876999855041504,6.64300012588501 -6.876999855041504,6.64300012588501 C-6.876999855041504,6.64300012588501 -15.39799976348877,12.23799991607666 -15.39799976348877,12.23799991607666z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,246.78599548339844,348.02899169921875)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-6.690999984741211,-0.008999999612569809 C-6.690999984741211,-0.008999999612569809 -1.38100004196167,6.622000217437744 -1.38100004196167,6.622000217437744 C-1.38100004196167,6.622000217437744 1.2510000467300415,5.497000217437744 1.2510000467300415,5.497000217437744 C1.2510000467300415,5.497000217437744 6.690999984741211,3.1459999084472656 6.443999767303467,2.5420000553131104 C5.65500020980835,0.6010000109672546 4.191999912261963,-4.618000030517578 3.378000020980835,-6.620999813079834 C3.378000020980835,-6.620999813079834 -6.690999984741211,-0.008999999612569809 -6.690999984741211,-0.008999999612569809z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,-8.724519729614258,-7.184986591339111)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,313.7239990234375,287.9620056152344)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'transparente')) }}"
                                                        fill-opacity="1"
                                                        d=" M17.4950008392334,37.612998962402344 C17.4950008392334,37.612998962402344 -17.493999481201172,37.612998962402344 -17.493999481201172,37.612998962402344 C-38.266998291015625,37.612998962402344 -55.106998443603516,20.773000717163086 -55.106998443603516,0 C-55.106998443603516,-20.773000717163086 -38.266998291015625,-37.61399841308594 -17.493999481201172,-37.61399841308594 C-17.493999481201172,-37.61399841308594 17.4950008392334,-37.61399841308594 17.4950008392334,-37.61399841308594 C38.268001556396484,-37.61399841308594 55.108001708984375,-20.773000717163086 55.108001708984375,0 C55.108001708984375,20.773000717163086 38.268001556396484,37.612998962402344 17.4950008392334,37.612998962402344z">
                                                    </path>
                                                </g>
                                                <g opacity="0.5"
                                                    transform="matrix(0.24289166927337646,0,0,0.24289166927337646,311.37200927734375,285.7980041503906)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-22.21500015258789,0 C-22.21500015258789,-12.269000053405762 -12.269000053405762,-22.21500015258789 0,-22.21500015258789 C12.269000053405762,-22.21500015258789 22.21500015258789,-12.269000053405762 22.21500015258789,0 C22.21500015258789,12.269000053405762 12.269000053405762,22.21500015258789 0,22.21500015258789 C-12.269000053405762,22.21500015258789 -22.21500015258789,12.269000053405762 -22.21500015258789,0z">
                                                    </path>
                                                </g>
                                                <g opacity="0.5"
                                                    transform="matrix(0.8792771100997925,0,0,0.8792771100997925,310.3340148925781,286.7590026855469)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-22.21500015258789,0 C-22.21500015258789,-12.269000053405762 -12.269000053405762,-22.21500015258789 0,-22.21500015258789 C12.269000053405762,-22.21500015258789 22.21500015258789,-12.269000053405762 22.21500015258789,0 C22.21500015258789,12.269000053405762 12.269000053405762,22.21500015258789 0,22.21500015258789 C-12.269000053405762,22.21500015258789 -22.21500015258789,12.269000053405762 -22.21500015258789,0z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,314.06500244140625,286.1839904785156)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M9.527000427246094,-1.0399999618530273 C9.527000427246094,-1.0399999618530273 -8.760000228881836,-9.70199966430664 -8.760000228881836,-9.70199966430664 C-9.52299976348877,-10.064000129699707 -10.404000282287598,-9.506999969482422 -10.404000282287598,-8.661999702453613 C-10.404000282287598,-8.661999702453613 -10.404000282287598,8.661999702453613 -10.404000282287598,8.661999702453613 C-10.404000282287598,9.506999969482422 -9.52299976348877,10.064000129699707 -8.760000228881836,9.70300006866455 C-8.760000228881836,9.70300006866455 9.527000427246094,1.0410000085830688 9.527000427246094,1.0410000085830688 C10.404999732971191,0.625 10.404999732971191,-0.6240000128746033 9.527000427246094,-1.0399999618530273z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9914510846138,-0.1304791271686554,0.1304791271686554,0.9914510846138,-43.61553955078125,36.211212158203125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,266.6199951171875,331.79901123046875)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-12.904000282287598,17.749000549316406 C-12.904000282287598,17.749000549316406 -9.340999603271484,28.65999984741211 -9.340999603271484,28.65999984741211 C-4.581999778747559,25.14299964904785 5.663000106811523,16.992000579833984 7.363999843597412,12.605999946594238 C9.87399959564209,6.135000228881836 9.020999908447266,-14.968999862670898 9.020999908447266,-14.968999862670898 C11.034000396728516,-17.458999633789062 12.904000282287598,-22.344999313354492 10.802000045776367,-25.538999557495117 C8.753999710083008,-28.660999298095703 8.317000389099121,-22.01099967956543 5.961999893188477,-20.018999099731445 C5.961999893188477,-20.018999099731445 2.6410000324249268,-24.724000930786133 3.249000072479248,-20.92300033569336 C3.8529999256134033,-17.121000289916992 5.193999767303467,-15.086999893188477 5.193999767303467,-15.086999893188477 C2.5339999198913574,-8.326000213623047 1.4989999532699585,-1.031999945640564 0.3400000035762787,7.079999923706055 C-3.1010000705718994,10.947999954223633 -8.979999542236328,14.977999687194824 -12.904000282287598,17.749000549316406z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,236.3090057373047,432.90301513671875)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-3.503999948501587,40.1510009765625 C-4.078999996185303,43.301998138427734 -3.5169999599456787,43.284000396728516 -2.1419999599456787,44.95199966430664 C2.625999927520752,45.43000030517578 7.079999923706055,45.72700119018555 9.168999671936035,43.650001525878906 C6.309000015258789,42.25899887084961 3.871000051498413,39.84400177001953 2.11299991607666,38.44300079345703 C2.256999969482422,31.367000579833984 7.425000190734863,14.321000099182129 8.9399995803833,10.585000038146973 C9.741999626159668,8.625 10.935999870300293,6.156000137329102 11.119999885559082,4.041999816894531 C12.548999786376953,-11.982999801635742 11.16100025177002,-30.4950008392334 8.670999526977539,-45.72800064086914 C8.670999526977539,-45.72800064086914 -5.065999984741211,-45.72800064086914 -5.065999984741211,-45.72800064086914 C-12.54800033569336,-29.316999435424805 1.024999976158142,-17.298999786376953 -0.20100000500679016,5.51200008392334 C-1.7359999418258667,10.088000297546387 -2.875,14.635000228881836 -3.569999933242798,19.14299964904785 C-4.603000164031982,25.891000747680664 -2.5820000171661377,34.625 -3.503999948501587,40.1510009765625z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,255.1510009765625,355.85699462890625)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-2.128000020980835,-5.729000091552734 C-2.128000020980835,-5.729000091552734 -0.23000000417232513,6.309000015258789 -0.23000000417232513,6.309000015258789 C-0.23000000417232513,6.309000015258789 0.6909999847412109,5.664999961853027 2.128000020980835,4.6020002365112305 C2.128000020980835,4.6020002365112305 -1.434999942779541,-6.309000015258789 -1.434999942779541,-6.309000015258789 C-1.6670000553131104,-6.117000102996826 -1.8969999551773071,-5.925000190734863 -2.128000020980835,-5.729000091552734z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,241.1999969482422,367.91400146484375)">
                                                    <path fill="rgb(62,50,121)" fill-opacity="1"
                                                        d=" M-13.722999572753906,17.974000930786133 C-8.602999687194824,0.2029999941587448 -11.505000114440918,-8.309000015258789 -2.062000036239624,-17.974000930786133 C-0.008999999612569809,-15.586999893188477 3.127000093460083,-15.425000190734863 11.821999549865723,-17.788000106811523 C11.821999549865723,-17.788000106811523 13.722999572753906,-5.75 13.722999572753906,-5.75 C9.666999816894531,0.46799999475479126 4.553999900817871,3.9130001068115234 6.61299991607666,17.974000930786133 C6.61299991607666,17.974000930786133 -13.722999572753906,17.974000930786133 -13.722999572753906,17.974000930786133z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9995760321617126,-0.029116354882717133,0.029116354882717133,0.9995760321617126,-11.45294189453125,6.973236083984375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,216.79100036621094,420.4230041503906)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-24.89900016784668,25.466999053955078 C-24.63800048828125,22.75 -23.9060001373291,18.357999801635742 -22.847999572753906,17.98200035095215 C-21.658000946044922,17.55900001525879 -13.336000442504883,20.493000030517578 -6.840000152587891,22.177000045776367 C-0.7620000243186951,23.753000259399414 7.605000019073486,26.025999069213867 11.029999732971191,23.268999099731445 C14.972999572753906,20.0939998626709 26.66699981689453,-5.050000190734863 29.08799934387207,-19.173999786376953 C24.85099983215332,-21.507999420166016 20.615999221801758,-23.766000747680664 16.35300064086914,-26.024999618530273 C-3.9059998989105225,-8.737000465393066 8.1850004196167,0.8650000095367432 2.5980000495910645,14.755000114440918 C-9.991000175476074,12.265999794006348 -17.52400016784668,12.755999565124512 -23.579999923706055,12.607999801635742 C-23.670000076293945,12.5649995803833 -25.06800079345703,12.520000457763672 -26.26099967956543,12.706000328063965 C-27.097000122070312,16.104999542236328 -29.08799934387207,23.7189998626709 -24.89900016784668,25.466999053955078z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,190.5540008544922,442.34100341796875)">
                                                    <path fill="rgb(0,0,0)" fill-opacity="1"
                                                        d=" M2.6579999923706055,-9.3100004196167 C0.972000002861023,-9.694000244140625 -0.7379999756813049,-9.571999549865723 -2.372999906539917,-9.239999771118164 C-2.6579999923706055,-9.182000160217285 -2.635999917984009,-9.08899974822998 -2.622999906539917,-8.843999862670898 C-2.5789999961853027,-8.026000022888184 -2.4489998817443848,-7.070000171661377 -2.361999988555908,-6.230000019073486 C-2.3440001010894775,-6.056000232696533 -2.3469998836517334,-6.048999786376953 -2.1610000133514404,-5.946000099182129 C-1.6369999647140503,-5.658999919891357 -1.6200000047683716,-5.15500020980835 -1.774999976158142,-4.408999919891357 C-2.2260000705718994,-2.244999885559082 -1.6319999694824219,2.4100000858306885 -1.0460000038146973,4.040999889373779 C0.984000027179718,9.694000244140625 1.8650000095367432,2.13700008392334 1.9140000343322754,-0.13300000131130219 C-1.2209999561309814,-3.688999891281128 0.11100000143051147,-5.999000072479248 2.6579999923706055,-9.3100004196167z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,232.5679931640625,409.39801025390625)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'sidebar')) }}"
                                                        fill-opacity="1"
                                                        d=" M-24.04800033569336,13.125 C-23.201000213623047,16.003999710083008 -20.961999893188477,18.631999969482422 -16.65399932861328,17.371000289916992 C-16.65399932861328,17.371000289916992 -15.399999618530273,23.6200008392334 -8.57699966430664,20.285999298095703 C-6.8429999351501465,21.711000442504883 -5.046999931335449,22.66699981689453 -3.186000108718872,23.155000686645508 C-0.3790000081062317,23.899999618530273 2.568000078201294,23.576000213623047 5.660999774932861,22.19099998474121 C11.361000061035156,27.20400047302246 16.16699981689453,21.69700050354004 16.16699981689453,21.69700050354004 C19.649999618530273,23.05299949645996 24.04800033569336,19.333999633789062 24.04800033569336,19.333999633789062 C22.82699966430664,3.3540000915527344 16.98200035095215,-9.590999603271484 17.483999252319336,-21.7549991607666 C17.506000518798828,-22.253000259399414 17.534000396728516,-22.750999450683594 17.57699966430664,-23.249000549316406 C17.57699966430664,-23.249000549316406 15.70199966430664,-23.64900016784668 15.70199966430664,-23.64900016784668 C15.70199966430664,-23.64900016784668 7.955999851226807,-25.295000076293945 7.955999851226807,-25.295000076293945 C7.955999851226807,-25.295000076293945 -1.0119999647140503,-27.20400047302246 -1.0119999647140503,-27.20400047302246 C-6.090000152587891,-22.5 -8.333000183105469,-13.029999732971191 -11.82699966430664,-4.11299991607666 C-14.52400016784668,2.7829999923706055 -17.969999313354492,9.35200023651123 -24.04800033569336,13.125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,242.81100463867188,370.3630065917969)">
                                                    <path
                                                        fill="{{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }}"
                                                        fill-opacity="1"
                                                        d=" M-17.065000534057617,19.631999969482422 C-17.079999923706055,21.07900047302246 -12.920999526977539,22.51799964904785 -7.966000080108643,23.34000015258789 C-7.966000080108643,23.34000015258789 -7.966000080108643,23.343000411987305 -7.966000080108643,23.343000411987305 C-0.4009999930858612,24.5939998626709 9.024999618530273,24.409000396728516 8.300999641418457,20.583999633789062 C6.466000080108643,10.881999969482422 7.171000003814697,0.07800000160932541 9.486000061035156,-3.059000015258789 C9.486000061035156,-3.059000015258789 9.508000373840332,-3.0810000896453857 9.508000373840332,-3.0810000896453857 C9.751999855041504,-3.4079999923706055 10.053999900817871,-3.759999990463257 10.402999877929688,-4.127999782562256 C10.420999526977539,-4.145999908447266 10.428000450134277,-4.1529998779296875 10.428000450134277,-4.1529998779296875 C12.3149995803833,-6.13100004196167 15.465999603271484,-8.526000022888184 17.079999923706055,-9.904000282287598 C15.78600025177002,-13.76099967956543 13.982999801635742,-17.253999710083008 10.741000175476074,-21.31100082397461 C7.044000148773193,-19.60300064086914 2.8559999465942383,-19.020999908447266 2.8559999465942383,-19.020999908447266 C0.5989999771118164,-18.68600082397461 -3.5169999599456787,-22.391000747680664 -2.934999942779541,-24.5939998626709 C-4.051000118255615,-23.45199966430664 -5.039999961853027,-22.253000259399414 -5.920000076293945,-20.9950008392334 C-8.210000038146973,-17.729999542236328 -9.791000366210938,-14.081000328063965 -11.067000389099121,-10.045999526977539 C-12.706000328063965,-4.855000019073486 -11.670000076293945,0.23100000619888306 -13.211999893188477,6.669000148773193 C-13.454999923706055,7.678999900817871 -13.746000289916992,8.678999900817871 -14.059000015258789,9.652999877929688 C-15.36400032043457,13.75 -17.042999267578125,17.429000854492188 -17.065000534057617,19.631999969482422z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9995760321617126,0.029116354882717133,-0.029116354882717133,0.9995760321617126,10.317153930664062,-6.9664306640625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,243.07699584960938,345.5769958496094)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M-3.200000047683716,0.19300000369548798 C-2.134999990463257,4.456999778747559 0.009999999776482582,6.419000148773193 2.8239998817443848,6.047999858856201 C2.8239998817443848,6.047999858856201 2.8269999027252197,5.935999870300293 2.8269999027252197,5.935999870300293 C2.8269999027252197,5.935999870300293 2.9579999446868896,1.559999942779541 2.9579999446868896,1.559999942779541 C2.9579999446868896,1.559999942779541 3.066999912261963,-2.0290000438690186 3.066999912261963,-2.0290000438690186 C3.066999912261963,-2.0290000438690186 3.2009999752044678,-6.419000148773193 3.2009999752044678,-6.419000148773193 C3.2009999752044678,-6.419000148773193 -2.117000102996826,-4.676000118255615 -2.117000102996826,-4.676000118255615 C-2.117000102996826,-4.676000118255615 -2.578000068664551,-2.5959999561309814 -2.578000068664551,-2.5959999561309814 C-2.578000068664551,-2.5959999561309814 -3.200000047683716,0.19300000369548798 -3.200000047683716,0.19300000369548798z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,243.00999450683594,347.489013671875)">
                                                    <path fill="rgb(240,175,168)" fill-opacity="1"
                                                        d=" M-3.132999897003174,-1.718999981880188 C-2.068000078201294,2.5450000762939453 0.07599999755620956,4.507999897003174 2.890000104904175,4.13700008392334 C2.890000104904175,4.13700008392334 2.8929998874664307,4.025000095367432 2.8929998874664307,4.025000095367432 C1.9479999542236328,3.2100000381469727 0.06499999761581421,-0.47600001096725464 0.9259999990463257,-0.26100000739097595 C1.6349999904632568,-0.08699999749660492 2.5880000591278076,-0.25 3.0239999294281006,-0.35199999809265137 C3.0239999294281006,-0.35199999809265137 3.132999897003174,-3.940000057220459 3.132999897003174,-3.940000057220459 C2.881999969482422,-3.9800000190734863 2.7039999961853027,-4.006999969482422 2.6570000648498535,-4.020999908447266 C2.4790000915527344,-4.072000026702881 -0.8109999895095825,-4.369999885559082 -2.51200008392334,-4.507999897003174 C-2.51200008392334,-4.507999897003174 -3.132999897003174,-1.718999981880188 -3.132999897003174,-1.718999981880188z">
                                                    </path>
                                                </g>
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,244.9669952392578,339.17999267578125)">
                                                    <path fill="rgb(249,203,203)" fill-opacity="1"
                                                        d=" M2.303999900817871,-8.782999992370605 C3.4790000915527344,-6.696000099182129 5.961999893188477,1.3910000324249268 4.0920000076293945,5.085000038146973 C2.2190001010894775,8.782999992370605 -5.961999893188477,6.980000019073486 -5.961999893188477,6.980000019073486 C-5.961999893188477,6.980000019073486 -5.581999778747559,-5.132999897003174 -5.581999778747559,-5.132999897003174 C-5.581999778747559,-5.132999897003174 2.303999900817871,-8.782999992370605 2.303999900817871,-8.782999992370605z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9999553561210632,0.009444335475564003,-0.009444335475564003,0.9999553561210632,3.610189199447632,-2.276973247528076)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,237.32200622558594,342.95098876953125)">
                                                    <path fill="rgb(0,0,0)" fill-opacity="1"
                                                        d=" M10.116999626159668,-14.689000129699707 C11.833999633789062,-13.621999740600586 12.71399974822998,-10.99899959564209 11.11299991607666,-8.217000007629395 C9.51099967956543,-5.434000015258789 6.915999889373779,-7.61299991607666 6.133999824523926,-5.870999813079834 C5.3520002365112305,-4.127999782562256 8.579000473022461,-1.7450000047683716 6.281000137329102,1.8869999647140503 C3.377000093460083,6.4730000495910645 11.777999877929688,9.206999778747559 7.816999912261963,14.842000007629395 C5.879000186920166,17.599000930786133 1.4889999628067017,14.329999923706055 -3.253999948501587,16.25 C-9.229000091552734,18.667999267578125 -12.71399974822998,13.190999984741211 -9.845999717712402,8.947999954223633 C-7.264999866485596,5.126999855041504 -3.4140000343322754,3.9800000190734863 -5.315000057220459,-0.14800000190734863 C-8.161999702453613,-6.328999996185303 -4.35699987411499,-10.25100040435791 -1.2979999780654907,-10.527999877929688 C1.7599999904632568,-10.805999755859375 3.7139999866485596,-18.66699981689453 10.116999626159668,-14.689000129699707z">
                                                    </path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <h4 class="text-center text-secondary">
                                        {{ ___('Aqui apareceran los NFT`s que generes') }}
                                    </h4>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="col-lg-6 d-none" id="div-previsualizador"
                    style="display: flex; justify-content: center; align-items: center;" wire:ignore>
                    <div class="text-left mt-2" id="card-img" style="display: grid;place-items: center;">
                        <h3 class="text-center">
                            <strong>
                                {{ ___('Previsualizador NFT') }}
                            </strong>
                        </h3>
                        <div style="position: relative; display: flex; justify-content: center; align-items: center;">
                            <img loading="lazy" style="height: auto; width: 100%; border-radius: 17px;" id="previsualizador"
                                class="card-img" src="" alt="">
                            <div
                                style="position: absolute; bottom: 50%; left: 0; right: 0; display: flex; justify-content: center;">
                                <div class="button-container"
                                    style="gap: 18px; display: flex; justify-content: flex-end;">
                                    <button title="{{ ___('Seleccionar') }} NFT"
                                        class="btn btn-xs btn-auto copy-clipboard btn-dark"
                                        data-animation-in="slideInDown" data-animation-out=""
                                        style="background: rgba(0%, 0%, 0%, 0.5);" id="seleccionar-nft">
                                        {{ ___('Seleccionar como NFT') }}<em class="ml-2 mdi mdi-image-plus"></em>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('modals')
    <div class="modal fade modal-enhance" id="modal-enhance" tabindex="-1" data-backdrop="static"
        data-keyboard="false" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <h4 class="popup-title">{{ ___('Mejora de NFT') }}</h4>
                        <p class="lead">
                            {{ ___('Por favor selecciona al menos una mejora para tu NFT y maximo tres para tu intento') }}.
                        </p>
                        <div class="row flex-container" style="justify-content: center;">
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="cinematic">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/cinematic.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance1"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Cinematográfico') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance1"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="modeling-compound">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/modeling-compound.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance2"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('compuesto de modelado') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance2"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="line-art">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/line-art.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance3"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Arte Lineal') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance3"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="origami">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/origami.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance4"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Origami') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance4"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="low-poly">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/low-poly.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance5"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Baja Poli') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance5"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="isometric">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/isometric.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance6"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Isometrico') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance6"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="neon-punk">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/neon-punk.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance7"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Punk Neón') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance7"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="analog-film">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/analog-film.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance8"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Película Analógica') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance8"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="fantasy-art">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/fantasy-art.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance9"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Arte de Fantasía') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance9"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="comic-book">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/comic-book.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance10"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Historieta') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance10"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="digital-art">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/digital-art.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance11"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Arte Digital') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance11"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="photographic">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/photographic.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance12"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Fotográfico') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance12"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="anime">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/anime.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance13"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Anime') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance13"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="enhance">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/enhance.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance14"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Mejorar') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance14"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="3d-model">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/3d-model.png') }}" data-holder-rendered="true">
                                <small class="text-white nombre-enhance15"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Modelo 3D') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance15"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 p-1 flex-item text-center mejora"
                                style="flex: 0 0 calc(25% - 8px); margin-bottom: 8px; position: relative;" type="button"
                                value="pixel-art">
                                <img loading="lazy" class="rounded me-2" alt="200x200" style="filter: brightness(0.7);" width="200"
                                    src="{{ asset('imagenes/nft-enhanced/pixel-art.png') }}"
                                    data-holder-rendered="true">
                                <small class="text-white nombre-enhance16"
                                    style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color: white;">{{ ___('Arte de Pixel') }}</small>
                                <i class="ri-checkbox-circle-line d-none text-success check-enhance16"
                                    style="font-size: 45px;position: absolute;top: 55%;left: 50%;transform: translate(-50%, -50%);"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer')
    <script>
        Livewire.on('errores', errores => {
            toastr.options.positionClass = 'toast-bottom-right';
            toastr.options = {
                "closeButton": true,
                "positionClass": "toast-bottom-right"
            }
            toastr.error(errores.prompt[0])
        })
    </script>
    <script>
        $(".focus-input").focusout(function() {
            $(".focus-input").css("color", "white")
        });
        $(".focus-input").focus(function() {
            $(".focus-input").css("color", "black")
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function() {
            var navButtons = [
                "<i class='ri-arrow-left-s-fill' style='color: {{ auth()->user()->theme_color == `style-black` ? `white` : asset(theme_color_user(auth()->user()->theme_color, 'back')) }}; font-size: 100px;'></i>",
                "<i class='ri-arrow-right-s-fill' style='color:{{ auth()->user()->theme_color == `style-black` ? `white` : asset(theme_color_user(auth()->user()->theme_color, 'back')) }}; font-size: 100px;'></i>"
            ];
            var navCarrusel = true;
            $('.owl-carousel').owlCarousel({
                autoplay: true,
                navSpeed: 500,
                autoplayTimeout: 2000,
                loop: true,
                margin: 50,
                responsiveClass: true,
                loop: true,
                navText: navButtons,
                stagePadding: 100,
                smartSpeed: 1500,
                responsive: {
                    0: {
                        items: 2,
                        stagePadding: 20,
                        margin: 10
                    },
                    768: {
                        items: 2,
                        stagePadding: 50,
                        margin: 20,
                        nav: navCarrusel
                    },
                    992: {
                        items: 5,
                        stagePadding: 100,
                        margin: 30,
                        nav: navCarrusel
                    }
                }
            })
            Livewire.on('imagenes', data => {
                data.forEach(function(elemento, indice) {
                    $('.owl-carousel').owlCarousel('add', elemento).owlCarousel('update');
                });
            })
        });
    </script>
    <script>
        function renderimg(item) {
            $('#div-previsualizador').removeClass('d-none');
            // Obtener el src de la imagen
            var src = $('#imgn-' + item).attr('src');
            // Establecer el src en la imagen de abajo
            $('#card-img').find('img').attr('src', src);
        }
        $(document).ready(function() {
            if (screen.width < 768) {
                $('.busqueda').css("margin-top", "15%");
                $('.busqueda').css("margin-bottom", "4%");
            } else if (screen.width > 768) {
                $('.busqueda').css("margin-top", "");
                $('.busqueda').css("margin-bottom", "");
            }
            // Controlador de clic del carrusel de Owl
            $('.owl-carousel .item').click(function() {
                $('#div-previsualizador').removeClass('d-none');
                $('#variado').removeClass('col-lg-12');
                $('#variado').addClass('col-lg-6');
                var imgId = $(this).find('img').data('id');
                var imgSrc = $(this).find('img').attr('src');
                $('#card-img').find('img').attr('src', imgSrc);
            });
            if (screen.width < 992) {
                $("#card-imagenes").css("margin-top", "18%")
            } else {
                $("#card-imagenes").css("margin-top", "0%")
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#generarNftIntento').click(function() {
                Livewire.emit('getNFT', myArray);
                $(this).prop('disabled',true);
                $(this).html(
                    `<span class="spinner-border spinner-border-sm pt-2" role="status" aria-hidden="true"></span>`
                );
            })
            $("#seleccionar-nft").click(function(e) {
                var imagen = $('#previsualizador').attr("src");
                var basePath = imagen.substring(0, imagen.lastIndexOf('/') + 1);
                var nombreImagen = imagen.split('/').pop();
                var usuario = {{ auth()->user()->id }};
                var ruta = "{{ route('disruptive.nft.seleccionar') }}"
                $.ajax({
                    type: "post",
                    url: ruta,
                    data: {
                        user: usuario,
                        imagen: nombreImagen,
                        ruta: basePath,
                        '_token': '{{ csrf_token() }}',
                    },
                    success: function(json) {
                        displaySuccessToaster(json.msg, json.message);
                        setTimeout(function() {
                            window.location.href =
                                "{{ route('disruptive.nft.generated') }}" + "?nft=" +
                                nombreImagen;
                        }, 2000);
                    },
                    error: function(json) {
                        displaySuccessToaster(json.msg, json.message);
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    }
                });

                function displaySuccessToaster(tipo, mensaje) {
                    if (tipo == 'success') {
                        toastr.success(mensaje);
                    } else if (tipo == 'error') {
                        toastr.error(mensaje);
                    }
                }
            });
            var myArray = [];
            $('.mejora').click(function() {
                var icon = $(this).find('i');
                var isHidden = icon.hasClass('d-none');

                if (isHidden) {
                    icon.removeClass('d-none');
                    if (screen.width > 992) {
                        var small = $(this).find('small');
                        small.css("top", "25%");
                    }
                    var value = $(this).attr('value');

                    if (myArray.length >= 3) {
                        var firstValue = myArray.shift(); // Obtener y eliminar el primer elemento del array
                        var firstIcon = $('.mejora[value="' + firstValue + '"]').find('i');
                        var firstSmall = $('.mejora[value="' + firstValue + '"]').find('small');
                        firstIcon.addClass(
                            'd-none'); // Agregar la clase "d-none" al icono del primer elemento eliminado
                        if (screen.width > 992) {
                            firstSmall.css("top",
                                "50%"
                            ); // Establecer top en 50% para el elemento small del primer elemento eliminado
                        }
                    }
                    myArray.push(value);
                    console.log(myArray.length, myArray);
                } else {
                    icon.addClass('d-none');
                    if (screen.width > 992) {
                        var small = $(this).find('small');
                        small.css("top", "50%");
                    }
                    var value = $(this).attr('value');
                    var index = myArray.indexOf(value);
                    if (index > -1) {
                        myArray.splice(index, 1);
                    }
                    console.log(myArray.length, myArray);
                }
                if (myArray.length > 3) {
                    show_toast('error', 'Solo puedes elegir 3 mejoras para tu NFT');
                }
            });
        });
    </script>
@endpush
