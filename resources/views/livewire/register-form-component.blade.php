<form class="register-form" method="POST" action="{{ route('register') }}" id="register" wire:submit.prevent="validar">
    @csrf
    @if (!is_maintenance() && application_installed(true) && $check_users == 0)
        <div class="alert alert-info-alt">
            Please register first your Super Admin account with adminstration privilege.
        </div>
    @endif

    <div class="input-item">
        <input type="text" id="username" placeholder="{{ ___('Your UserName') }}" wire:model.debounce.750ms="username"
            class="input-bordered form-control form-control-sm border-semi-redondeado">
        @error('username')
            <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}</span>
        @enderror
    </div>

    <div class="input-item">
        <input type="text" id="name" placeholder="{{ ___('Your Name') }}" wire:model.debounce.750ms="name"
            class="input-bordered form-control form-control-sm border-semi-redondeado">
        @error('name')
            <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}</span>
        @enderror
    </div>
    <div class="input-item">
        <input type="email" id="email" placeholder="{{ ___('Your Email') }}" wire:model.debounce.750ms="email"
            class="input-bordered form-control form-control-sm border-semi-redondeado">
        @error('email')
            <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}</span>
        @enderror
    </div>

    <small wire:ignore id="popover-password-top" class="hide text-danger"> {{ ___('Enter a strong password') }}&nbsp;
    </small>
    <div class="row">
        <div class="" style="
            width: 100%" id="show_hide_password" wire:ignore>
            <input id="password" name="password" type="password"
                class="input-bordered form pass form-control form-control-sm border-semi-redondeado"
                style="background-image: none;" wire:model.debounce.750ms="password" style="width:100%;"
                placeholder="{{ ___('Password') }}" required>
            <a href="#" tabindex="-1">
                <i class="fa fa-eye-slash"
                    style="
                color: #191919;
                position: absolute;
                width: 20px;
                height: 20px;
                right:20px;
                left: auto;
                top:20px;
                transform: translateY(-50%);">
                </i>
            </a>
        </div>
        @error('password')
            <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}
            @enderror
            <div class="center d-none col-lg-2 col-md-2 col-sm-2 col-xs-2 ml-1" id="pop" wire:ignore>
                <span class="qs" id="globo">
                    <i class="fa fa-info-circle text-info"></i>
                    <span id="pov" class="popover above show" style="top:auto;">
                        <ul class="list-unstyled d-none" id="uls">
                            <li class="" id="uppercase"><span class="low-upper-case"><i class="fa fa-times"
                                        aria-hidden="true"></i></span>&nbsp; {{ ___('At least 1 Uppercase') }}
                            </li>
                            <li class="" id="lowercase"><span class="lower-case"><i class="fa fa-times"
                                        aria-hidden="true"></i></span>&nbsp; {{ ___('At least 1 Lowercase') }}
                            </li>
                            <li class="" id="number"><span class="one-number"><i class="fa fa-times"
                                        aria-hidden="true"></i></span> &nbsp; {{ ___('At least 1 number ') }}
                                (0-9)</li>
                            <li class="" id="special"><span class="one-special-char"><i class="fa fa-times"
                                        aria-hidden="true"></i></span> &nbsp; {{ ___('At least 1 Special Character') }}

                                (!@#$%^&*+).</li>
                            <li class="" id="character"><span class="eight-character"><i class="fa fa-times"
                                        aria-hidden="true"></i></span>&nbsp; {{ ___('At least 8 Character') }}
                            </li>
                        </ul>
                    </span>
                </span>
            </div>
    </div>
    <div class="mt-3 d-none" id="popover-password" wire:ignore>
        <p>{{ ___('Password Strength') }}: <span id="result"> </span></p>
        <div class="progress">
            <div id="password-strength" wire:ignore class="progress-bar progress-bar-success" style="top: -10px"
                role="progress-bar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
            </div>
        </div>
    </div>
    <div class=" mb-3" style="" wire:ignore>

        <div class=" mt-3" style="width: 100%;">
            <input id="password_confirmation" name="password_confirmation" type="password"
                wire:model.debounce.750ms="password_confirmation" placeholder="{{ ___('Repeat Password') }}"
                class="input-bordered form pass form-control form-control-sm border-semi-redondeado"
                style="width:100%;" required>
            @error('password_confirmation')
                <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}
                @enderror
        </div>
        <small id="popover-cpassword" class="hide text-danger"> {{ ___("Password don't match") }}
            &nbsp;</small>
    </div>

    <div wire:ignore class="input-item">
        <input type="text" id="mobile" placeholder="{{ ___('Your Phone') }}"
            class="input-bordered form-control form-control-sm border-semi-redondeado">
        <span style="font-size: 11px;" id="mobile-span" class="text-danger d-none"></span>
    </div>

    @if (get_refer_id())
        <div class="container_referal mb-3">
            <div class="input-group">
                @if (get_refer_id_image() == null)
                    <img loading="lazy" class="example-image" src="{{ asset2('imagenes/user-default.jpg') }}"
                        style="width:30px; height:30px; border-radius:50%;">
                @else
                    <img loading="lazy" class="example-image" src="{{ asset2('imagenes/perfil') }}/{{ get_refer_id_image(true) }}"
                        style="width:30px; height:30px; border-radius:50%;">
                @endif
                <input type="text" id="textoo" style="margin-left: 8px;"
                    class="input-bordered form-control form-control-sm border-semi-redondeado"
                    value="{{ ___('Your were invited by') }} : {{ get_refer_id(true) }}" disabled readonly>
            </div>
        </div>
    @endif

    @if (application_installed(true) && $check_users > 0)
        @if (get_page_link('terms') || get_page_link('policy'))
            <div class="input-item text-left">
                <input name="terms" wire:model.debounce.750ms="terms"
                    class="input-checkbox input-checkbox-md form" id="agree" type="checkbox" required="required"
                    data-msg-required="{{ ___('You should accept our terms and policy.') }}">
                <label id=""
                    for="agree">{!! ___('I agree to the') . ' ' . '<a target="" href="' !!}{{ route('terms') }}{!! '">' . ___('Terms and Condition') . '</a>' . ' ' . ___('and') . ' ' . '<a target="" href="' !!}{{ route('privacy') }}{!! '">' . ___('Privacy and Policy') . '</a>' !!}.</label>
                @error('terms')
                    <span style="font-size: 11px;" class="text-danger">{{ ___($message) }}
                    @enderror
            </div>
        @else
            <div class="input-item text-left">
                <label for="agree">{{ ___('By registering you agree to the terms and conditions.') }}</label>
            </div>
        @endif
    @else
        <input name="terms" value="1" type="hidden">
    @endif
    @if (recaptcha())
        <input type="hidden" name="recaptcha" id="recaptcha">
    @endif

    <div class="d-grid gap-2 col-6 mx-auto"><button type="submit" id="bons" class="btn btn-primary btn-block"
            @if ($errors->any() || $validacion == 0) disabled @endif wire:click="enviarFormulario"
            style="background-color: #280f53!important;">{{ application_installed(true) && $check_users == 0 ? ___('Complete Installation') : ___('Create Account') }}</button>
    </div>
</form>
@push('header')
    <link preload rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/css/intlTelInput.css"
        integrity="sha512-PpUO7vLt/N4Ebtd1CU2V5b3d74Hp4/kjeSNTMuJJimwUtQiZRzvhEOyP+aQPREzUypL+xNOwQ4f5GBghyjZypw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .intl-tel-input,
        .iti {
            width: 100%;
        }

        .border-semi-redondeado {
            border-radius: 10px;
        }
    </style>
@endpush
@push('livewireScripts')
    <script>
        (function(b, c) {
            var $ = b.jQuery || b.Cowboy || (b.Cowboy = {}),
                a;
            $.throttle = a = function(e, f, j, i) {
                var h, d = 0;
                if (typeof f !== "boolean") {
                    i = j;
                    j = f;
                    f = c
                }

                function g() {
                    var o = this,
                        m = +new Date() - d,
                        n = arguments;

                    function l() {
                        d = +new Date();
                        j.apply(o, n)
                    }

                    function k() {
                        h = c
                    }
                    if (i && !h) {
                        l()
                    }
                    h && clearTimeout(h);
                    if (i === c && m > e) {
                        l()
                    } else {
                        if (f !== true) {
                            h = setTimeout(i ? k : l, i === c ? e - m : e)
                        }
                    }
                }
                if ($.guid) {
                    g.guid = j.guid = j.guid || $.guid++
                }
                return g
            };
            $.debounce = function(d, e, f) {
                return f === c ? a(d, e, false) : a(d, f, e !== false)
            }
        })(this);
    </script>
    <script>
        $(document).ready(function() {
            $('#mobile').keyup($.debounce(750, function(e) {
                var valido = numeroEsValido($(this).val());
                if (valido == true) {
                    $("#mobile-span").addClass('d-none');
                    Livewire.emit('sendNumber', ['+' + iti.s.dialCode + numeroLimpio, $(this).val()]);
                } else {
                    Livewire.emit('sendNumber', ['error', $(this).val()]);
                }
            }));
        });

        Livewire.on('get-message', message => {
            if (message != "valid") {
                $("#mobile-span").removeClass('d-none');
                $("#mobile-span").text(message);
            } else {
                $("#mobile-span").addClass('d-none');
            }
        })

        Livewire.on('enviar', variable => {
            var url = '{{ route('register') }}'
            var form = $('<form action="' + url + '" method="post">' +
                '<input type="text" name="name" value="' + variable[0] + '" />' +
                '<input type="text" name="_token" value="' + variable[4] + '" />' +
                '<input type="text" name="username" value="' + variable[1] + '" />' +
                '<input type="text" name="email" value="' + variable[2] + '" />' +
                '<input type="text" name="password" value="' + variable[3] + '" />' +
                '<input type="text" name="password_confirmation" value="' + variable[3] + '" />' +
                '<input type="text" name="mobile" value="+' + iti.s.dialCode + numeroLimpio + '" />' +
                '</form>');
            $('body').append(form);
            form.submit();
        })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/js/intlTelInput.min.js"
        integrity="sha512-IeQbZp3+T/xyhshcKd0r29GKvdRQdIQNAsyFvT7njgy4Z4zXYDyTKyJi89gC36rdV1SyaQJTlvtV60G3Y6ctcg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/libphonenumber-js/1.10.26/libphonenumber-js.min.js"
        integrity="sha512-00iW/03JLPCWMKeykay1Xh9ZauJ1P1l4HNgZMDVLkkfc2gGSStekjeSWkPef8e0eNDZtpb3GWtfunB0W1m4huQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/whatwg-fetch@3.6.2/dist/fetch.umd.min.js"></script>
    <script>
        var input = document.querySelector("#mobile");
        var iti = window.intlTelInput(input, {
            separateDialCode: true,
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                fetch("https://ipapi.co/json")
                    .then(function(res) {
                        return res.json();
                    })
                    .then(function(data) {
                        callback(data.country_code);
                    })
                    .catch(function() {
                        callback("us");
                    });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.1.0/js/utils.js",
        });

        var numeroLimpio;
        input.addEventListener("keyup", function() {
            var numeroDigitado = $(this).val();
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            var numeroFormateado = new libphonenumber.AsYouType(codigoRegion).input(numeroDigitado);
            $(this).focus().val('').val(numeroFormateado);
            var numeroFormateadoHelper = numeroFormateado + 1;
            var longitudPhone = libphonenumber.validatePhoneNumberLength(numeroFormateado, codigoRegion);
            var longitudPhoneHelper = libphonenumber.validatePhoneNumberLength(numeroFormateadoHelper,
                codigoRegion);
            var valido = numeroEsValido(numeroFormateado);
            if ((longitudPhone == undefined) && valido == true) {
                var longitud = numeroFormateado.length;
                $("#mobile").prop("maxlength", longitud);
                numeroLimpio = libphonenumber.parse(numeroFormateado, codigoRegion).phone;
            } else if (longitudPhoneHelper == "TOO_LONG") {
                var longitudH = numeroFormateadoHelper.length - 1;
                $("#mobile").prop("maxlength", longitudH);
            } else if (longitudPhone == "NOT_A_NUMBER" || longitudPhone == "INVALID_COUNTRY" || longitudPhone ==
                "TOO_SHORT" || longitudPhone == "INVALID_LENGTH") {
                $("#mobile").removeAttr("maxlength");
            }
        });

        function numeroEsValido(numero) {
            var codigoRegion = iti.getSelectedCountryData().iso2;
            codigoRegion = codigoRegion.toUpperCase();
            var valido = libphonenumber.isValidNumber(numero, codigoRegion);
            return valido;
        }

        var contador = 0;
        input.addEventListener("countrychange", function() {
            contador++;
            $("#mobile").val("");
            if (contador >= 2) {
                Livewire.emit('sendNumber', ['error', $(this).val()]);
            }
        });
    </script>
@endpush
