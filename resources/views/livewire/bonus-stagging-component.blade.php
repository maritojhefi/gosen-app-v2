<div class="page-content">
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ ___('Registros de transacciones tipo Bonus Package') }}</h4>
                        <div class="row mt-3 mb-3">
                            <div class="col-4">
                                <select name="" id="" class="form-control" wire:model="paquete">
                                    <option value="">Todos</option>
                                    @foreach ($agrupado as $package => $cant)
                                        <option value="{{ $package }}">{{ $paquetes->where('id',$package)->first()->nombre }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-4">
                                <input type="text" class="form-control" placeholder="Buscar" wire:model.debounce.750ms="search">
                            </div>
                            <div class="col-4" wire:loading>
                                <div class="spinner-border" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>


                        <div class="table-responsive">
                            <table class="table table-striped mb-0">

                                <thead>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Tokens</th>
                                        <th>Pertenece a</th>
                                        <th>Acumulado</th>
                                        <th>Transaccion de origen</th>
                                        <th>Fecha de creacion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($bonusPackageTrans as $bonus)
                                        <tr>
                                            <td scope="row">{{ $bonus->tnx_id }}</td>
                                            <td>{{ $bonus->tokens }}</td>
                                            <td>{{ $bonus->tnxUser->name }}</td>
                                            <td>{{$bonus->tnxUser->token_bonus}}</td>
                                            <td>{{$bonus->details}}</td>
                                            <td>{{ $bonus->created_at->format('d-m-Y H:i:s') }}</td>
                                        </tr>
                                    @endforeach


                                </tbody>
                            </table>
                            
                        </div>
                        <div class="m-3 row">
                            <span class="mx-auto d-block">{{$bonusPackageTrans->links()}}</span>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
