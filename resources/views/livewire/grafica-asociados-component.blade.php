<div class="col-lg-12 col-md-12">
    <div class="card card-full-height p-2" style="align-items: center;">
        <h4 class="card-title" style="color: #000; padding-top: 14px;">{{___('Red de asociados Global')}}</h4>
        <div class="card-innr pb-0" style="width: 100%;">
            <div class="row">
                <div class="col-1 p-0">
                    <h4 class="card-title title-chard pb-2" style="color: #000;transform: rotate(-90deg);position: absolute; top: 150px;">
                        {{___('Asociados')}}
                    </h4>
                </div>
                <div class="col-10 p-0">
                    {!! $usersChart->container() !!}
                </div>
                <div class="col-1 p-0">
                    <div class="float-right position-relative">
                        <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                            <em class="fa fa-sliders" style ="font-size:20px;" data-toggle="tooltip" data-placement="left" data-original-title="Filters"></em> </a>
                        <div
                            class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                            <div id="chard">
                                <ul class="dropdown-list dropdown-list-s2">
                                    <li>
                                        <input class="input-checkbox input-checkbox-sm " id="dias"
                                            name="chard" type="radio" value="dias" checked wire:model="checkbox">
                                        <label for="dias">{{___('Última semana')}}
                                        </label>
                                    </li>
                                    <li>
                                        <input class="input-checkbox input-checkbox-sm " id="semanas"
                                            name="chard" type="radio" value="semanas" wire:model="checkbox">
                                        <label for="semanas">{{___('Último mes')}}
                                        </label>
                                    </li>
                                    <li>
                                        <input class="input-checkbox input-checkbox-sm " id="mes"
                                            name="chard" type="radio" value="mes" wire:model="checkbox">
                                        <label for="mes">{{___('Últimos 6 meses')}}
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="card-title pb-2" style="color: #000;">{{___('Días')}}</h4>
    </div>
</div>
@push('footer')

    {{-- @if($usersChart) --}}
        {!! $usersChart->script() !!}
    {{-- @endif --}}
@endpush
