<div class="row">
    <div class="page-content">
        <div class="container">
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{ ___('Bloqueo de Correos Sospechosos y de spam') }}</h4>
                        <button class="btn btn-md btn-primary" type="button" data-toggle="modal"
                            data-target="#modal-bloqueo"><i class="mdi mdi-plus-circle-outline mr-2 mt-1"
                                style="font-size: 20px;"></i>{{ ___('Crear Nuevo') }}</button>
                        <div class="row m-4 d-flex align-items-center justify-content-center">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><span
                                    class=" text-justify p-3 pb-4">
                                    {{ ___('Aquí se registran los correos que se desean bloquear estos a partir de su bloqueo, se pueden habilitar nuevamente, y administrarlos para que el usuario no pueda crear cuentas falsas o de spam') }}.
                                </span></div>
                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                                <div class="app-search">
                                    <div class="position-relative">
                                        <input type="text" class="form-control"
                                            placeholder="{{ ___('Escriba el dominio de correo para buscar. ejem:"@xyz.com"') }}"
                                            wire:model.debounce.750ms="search">
                                        <span class="ri-search-line"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <span wire:loading class="spinner-border spinner-border-sm mt-1" role="status"
                                    aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-12">
                            <table id="tabla" class="data-table table-responsive table-hover ">
                                <thead>
                                    <tr class="data-item">
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            ID
                                        </th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            {{ ___('Correo') }}
                                        </th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            {{ ___('Tipo') }}
                                        </th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            {{ ___('Descripcion') }}</th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            {{ ___('Estado') }}</th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;">
                                            {{ ___('actualizacion de Bloqueo') }}
                                        </th>
                                        <th class="data-col tnx-status dt-tnxno " style="padding-left: 3rem;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($emails as $item)
                                        <tr class="data-item mt-2 m-b-2">
                                            <th class="data-col text-right" style="padding-left: 3rem;" scope="row"
                                                style="display: flex; justify-content: center; align-items: center; padding-top: 30px;">
                                                {{ $item->id }}</th>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                {{ $item->correo }}</td>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                {{ $item->tipo }}</td>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                {{ $item->descripcion }}</td>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                <div class="input-item d-flex justify-content-center"
                                                    style="padding-top: 20%;">
                                                    <input name="personal" type="checkbox"
                                                        {{ $item->estado == true ? 'checked' : ' ' }}
                                                        class="input-switch input-switch-sm checkbox-personal"
                                                        id="check_estado-{{ $item->id }}"
                                                        wire:change="$emit('actualizarEstadoCorreo', {{ $item->id }}, $event.target.checked)">
                                                    <label for="check_estado-{{ $item->id }}"></label>
                                                </div>
                                            </td>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                {{ $item->updated_at }}</td>
                                            <td class="data-col text-right" style="padding-left: 3rem;">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <button class="btn btn-danger borrar"
                                                            value="{{ $item->id }}">
                                                            <i class="mdi mdi-trash-can-outline"
                                                                style="font-size: 20px;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $emails->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('modals')
        <div class="modal fade modal-bloqueo" id="modal-bloqueo" tabindex="-1" style="padding-right: 17px;">
            <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" id="modal-close" data-dismiss="modal"
                        aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="popup-body">
                        <div class="content-area popup-content">
                            <div class="card-head d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-0">{{ ___('Create Mail Blocked for Users') }}
                                </h4>
                            </div>
                            <form id="formulario-bloqueo">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Dominio/Correo') }}</label>
                                    <div class="input-wrap">
                                        <input name="correo" id="correo" class="form-control" minlength="3"
                                            required="required" type="text"
                                            placeholder="{{ ___('Escriba el dominio/correo a bloquear') }}"
                                            value="">
                                    </div>
                                </div>
                                <div class="input-item input-with-label">
                                    {{-- <label class="input-item-label">{{ ___('Tipo') }}</label>
                                    <div class="input-wrap">
                                        <input name="tipo" id="tipo" class="form-control " minlength="3"
                                            required="required" type="text"
                                            placeholder="{{ ___('Describa en una palabra la razon por la cual bloqueara este dominio/correo') }}"
                                            value="">
                                    </div> --}}




                                    <label class="input-item-label">{{ ___('Tipo') }}</label>
                                    <select name="tipo" id="tipo" class="select select-bordered select-block"
                                        required="required">
                                        <option value="default" selected> -- {{ ___('Seleccione una opcion') }} --
                                        </option>
                                        <option value="Spam">
                                            {{ ___('Spam') }}
                                        </option>
                                        <option value="Cuentas Falsas">
                                            {{ ___('Cuentas Falsas') }}
                                        </option>
                                        <option value="Correos Inseguros">
                                            {{ ___('Correos Inseguros') }}
                                        </option>
                                        <option value="Suplantacion de identidad">
                                            {{ ___('Suplantacion de Identidad') }}
                                        </option>
                                        <option value="Correo de creacion masivo">
                                            {{ ___('Correo de creacion masivo') }}
                                        </option>
                                    </select>


                                </div>
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Descripcion') }} <small
                                            class="text-danger">({{ ___('Opcional') }})</small> </label>
                                    <div class="input-wrap">
                                        <input name="descripcion" id="descripcion" class="input-bordered" type="text"
                                            placeholder="{{ ___('Escriba una descripcion detallada acerca del porque del bloqueo de este correo') }}"
                                            value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8"></div>
                                    <div class="col-4 d-flex justify-content-end">
                                        <button type="submit" class="btn btn-xs btn-primary crear-bloqueo disabled">
                                            <i class="ri-mail-add-line mr-2"></i>{{ ___('Crear Bloqueo') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
    @endpush
    @push('footer')
        <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
            $('.borrar').click(function(event) {
                var id = $(this).attr('value');
                event.preventDefault();
                Swal.fire({
                    title: 'Estas seguro?',
                    text: "Esta acción no se puede revertir!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emit('eliminarDominio', id);
                    }
                })
            });
        </script>
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                showCloseButton: true,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.a6ddEventListener('mouseleave', Swal.resumeTimer)
                }
            });
            window.addEventListener('alert', ({
                detail: {
                    type,
                    message
                }
            }) => {
                Toast.fire({
                    icon: type,
                    title: message
                })
            })
        </script>
        <script>
            $(document).ready(function() {
                // Capturar los datos del formulario cuando se envíe
                $('#formulario-bloqueo').submit(function(event) {
                    event.preventDefault(); // Evitar el envío del formulario

                    // Obtener los valores de los campos
                    var formData = {};
                    $(this).find('input, select, textarea').each(function() {
                        formData[this.name] = $(this).val();
                    });
                    // Mostrar los datos en la consola (puedes modificar esto según tus necesidades)
                    // console.log(formData.correo);
                    // Aquí puedes realizar otras acciones con los datos, como enviarlos al servidor mediante AJAX
                    Livewire.emit('crearCorreoBloqueado',
                        formData.correo,
                        formData.tipo,
                        formData.descripcion
                    );
                    Livewire.on('cerrarModal', modal => {
                        if (modal == true) {
                            $(this).trigger('reset');
                            $('#modal-bloqueo').modal().hide();
                            $('.modal-backdrop').remove();
                        }
                    })
                });

            });
        </script>
        <script>
            $(document).ready(function() {
                var inputTimers = {}; // Objeto para almacenar los temporizadores de cada campo de entrada
                // Función para verificar las condiciones y habilitar o deshabilitar el botón
                function checkConditions() {
                    var correoVal = $('#correo').val().trim();
                    var tipoVal = $('#tipo').val().trim();
                    var descripcionVal = $('#descripcion').val().trim();
                    // Comprobar si "correo" y "tipo" tienen valores no vacíos
                    if (correoVal.length > 0 && tipoVal.length > 0) {
                        $('.crear-bloqueo').removeClass('disabled');
                    } else {
                        $('.crear-bloqueo').addClass('disabled');
                    }
                }
                // Función para manejar el evento cuando el usuario deja de escribir
                function handleInputChange(inputId) {
                    clearTimeout(inputTimers[inputId]); // Reiniciar el temporizador
                    // Establecer un nuevo temporizador
                    inputTimers[inputId] = setTimeout(function() {
                        checkConditions();
                    }, 1000);
                }
                // Capturar el evento keyup para los campos de entrada
                $('#correo, #tipo').on('keyup', function() {
                    var inputId = $(this).attr('id');
                    handleInputChange(inputId);
                });
                // Capturar los eventos change y blur para los campos de entrada
                $('#correo, #tipo').on('change blur', function() {
                    var inputId = $(this).attr('id');
                    handleInputChange(inputId);
                });
            });
        </script>
    @endpush
</div>
