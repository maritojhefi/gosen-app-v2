<div class="card-innr ">
    <h6 class="card-title">{{ ___('Withdraw Bonus Dollars') }}
        <span
            class="badge p-1 bg-{{ $estadoActual == null ? 'dark' : ($estadoActual == 'pending' ? 'warning' : ($estadoActual == 'approved' ? 'success' : ($estadoActual == 'canceled' ? 'danger' : 'info'))) }}">{{ ___(ucfirst($estadoActual == null ? 'Todos' : ($estadoActual == 'aprobadosusuario' ? 'Aprobado por el Usuario' : $estadoActual))) }}</span>
    </h6>
    <div class="row">
        <div class="col-4">
            <div class="row">
                <div class=" col-6">
                    <label class="input-group-text" for="fecha-inicio">{{ ___('Desde') }}:</label>
                    <input class="form-control" type="date" value="" wire:model="fechaInicio">
                </div>
                <div class=" col-6">
                    <label class="input-group-text" for="fecha-fin">{{ ___('Hasta') }}:</label>
                    <input class="form-control" type="date" value="" wire:model="fechaFin">
                </div>
            </div>
        </div>
        <div class="card-opt col-3">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <div class="btn-group" role="group">
                    <button class="btn btn-secondary" wire:click="exportarWithdrawExcel()">
                        <h5 class="text-white">
                            {{ 'Excel' }}<i class="ri-file-excel-2-line ml-2"></i>
                        </h5>
                    </button>
                    <div wire:loading>
                        <i class="fas fa-circle-notch fa-spin ml-5 mr-2"
                            style="font-size:20px;"></i>{{ ___('Cargando...') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="float-right col-5">
            <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                <em class="mdi mdi-filter" data-toggle="tooltip" data-placement="left" data-original-title="Filters"
                    style="font-size:20px;"></em> </a>
            <div class="toggle-class toggle-datatable-filter dropdown-content  dropdown-content-top-left text-left"
                style="right: 40px;">
                <select class="dropdown-list dropdown-list-s2" wire:model="estado">
                    <option class="text-center mt-2 mb-2" value="">{{ ___('Todos') }}</option>
                    @foreach ($opcionesFiltro as $opcion)
                        <option class="text-center mt-2 mb-2" value="{{ $opcion }}">{{ ___($opcion) }}</option>
                    @endforeach
                    <option class="text-center mt-2 mb-2" value="aprobadosusuario">{{ ___('Aprobados por Usuario') }}
                    </option>
                </select>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <table class="tabla" style="width: -webkit-fill-available;">
            <thead>
                <tr class="data-item data-head" role="row">
                    <th class="data-col tnx-status dt-tnxno sorting_disabled" rowspan="1" colspan="1">
                    </th>
                    <th class="data-col tnx-status dt-tnxno sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('Bonus Withdraw') }} ID</th>
                    <th class="data-col dt-token sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('User') }}</th>
                    <th class="data-col dt-token sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('Amount') }}</th>
                    <th class="data-col dt-amount sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('Wallet Address') }}</th>
                    <th class="data-col dt-amount sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('Aprobado') }}</th>
                    <th class="data-col dt-base-amount sorting_disabled" rowspan="1" colspan="1">
                        {{ ___('Date') }}
                    </th>
                    <th class="data-col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($registros as $trnx)
                    <tr class="data-item tnx-item-391 odd" role="row">
                        <td class="data-col dt-tnxno">
                            <div class="d-flex align-items-center">
                                <div data-toggle="tooltip" data-placement="top" title=""
                                    class="data-state data-state-{!! $trnx->status !!}"
                                    data-original-title="{{ $trnx->status }}">
                                    <span class="d-none"></span>
                                </div>
                            </div>
                        </td>
                        <td class="data-col dt-token">
                            <span class="lead token-amount">{{ ___($trnx->code) }}</span>
                            <span class="sub sub-date">({{ ucfirst($trnx->status) }})</span>
                        </td>
                        <td class="data-col dt-token">
                            <span class="sub sub-date">{{ $trnx->userBy->name }}</span>
                        </td>
                        <td class="data-col dt-token">
                            <span class="lead amount-pay">{{ $trnx->amount }}</span>
                            <span class="sub sub-symbol">(USDT)</span>
                        </td>
                        <td class="data-col dt-amount">
                            <span class="lead amount-pay">{{ $trnx->wallet }}</span>
                        </td>
                        <td class="data-col dt-amount">
                            <span class="lead amount-pay text-center"><i style="font-size: 25px;"
                                    class="{{ $trnx->approved_bonus == 1 && $trnx->status == 'pending' ? 'ri-check-line text-success' : ($trnx->approved_bonus == 1 && $trnx->status == 'approved' ? 'ri-check-double-line text-success' : ($trnx->approved_bonus == 0 && $trnx->status == 'canceled' ? 'ri-close-line text-danger' : 'mdi mdi-clock-outline text-info')) }}"></i></span>
                        </td>
                        <td class="data-col dt-account">
                            <span class="lead user-info">
                                {{ date('jS F, Y / h:i', strtotime($trnx->created_at)) }}</span>
                            <span class="sub sub-date"> {!! fechaOrden(\Carbon\Carbon::parse($trnx->created_at)) !!}</span>
                        </td>
                        @if ($trnx->status == 'approved')
                            <td class="data-col text-right">
                            </td>
                        @elseif($trnx->status == 'canceled')
                            <td class="data-col text-right">
                                <div class="relative d-inline-block">
                                    <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                            class="ti ti-more-alt"></em></a>
                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                        <ul id="more-menu-102" class="dropdown-list">
                                            <li><a href="#" data-toggle="modal"
                                                    data-target="#modal-medium2{{ $trnx->id }}">
                                                    <em class="ti ti-eye"></em>
                                                    {{ ___('View Details') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        @else
                            <td class="data-col text-right">
                                <div class="relative d-inline-block">
                                    <a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                            class="ti ti-more-alt"></em></a>
                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                        <ul id="more-menu-102" class="dropdown-list">
                                            <li><a href="#" data-toggle="modal"
                                                    data-target="#modal-small{{ $trnx->id }}">
                                                    <em class="ti ti-check"></em>
                                                    {{ ___('Approved') }}</a>
                                            </li>
                                            <li><a href="#" data-toggle="modal"
                                                    data-target="#modal-smallo{{ $trnx->id }}">
                                                    <em class="ti ti-close"></em>
                                                    {{ ___('Decline') }}</a>
                                            </li>
                                            <li><a href="#" data-toggle="modal"
                                                    data-target="#modal-medium2{{ $trnx->id }}">
                                                    <em class="ti ti-eye"></em>
                                                    {{ ___('View Details') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $registros->links() }}
    </div>
</div>
