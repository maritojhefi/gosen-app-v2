@push('header')
    <style>
        @media only screen and (min-width: 1490px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-50 {
                width: 300px !important;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }

        }

        @media (max-width: 1490px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col>div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table,
        td {
            color: #000000;
        }

        a {
            color: #161a39;
            text-decoration: underline;
        }
    </style>




    <link preload href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
@endpush
<div wire:ignore.self>
    <div class="page-content">
        <div class="container">
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title">Massive Creator Mail</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                    <div class="content-area card fondo">
                        <div class="card-innr">
                            <div class="card-head">
                                <h6 class="card-title">{{ ___('Mail Creator') }}</h6>
                                <small>({{ ___('Fill in the fields of the mail to send') }})</small>
                            </div>

                            <label for="">{{ ___('Email Title') }}</label>
                            <input wire:model.debounce.750ms="title" type="text" name="" class="form-control"
                                id="">


                            <label for="content">{{ ___('Contents') }}</label>
                            <div wire:ignore.self>
                                <textarea wire:model.debounce.750ms="content" id="editor" name="content"
                                    class="input-bordered input-textarea editor"></textarea>
                                <input type="button" class="btn btn-primary" onclick="viewHtml();"
                                    value="{{ ___('Upload the content') }}" />
                            </div>


                            <label for="">{{ ___('Footer') }}</label>
                            <input wire:model.debounce.750ms="footer" type="text" name="" class="form-control"
                                id="">

                            <div class="col mt-3"
                                style="display:flex; justify-content:flex-end; width:100%; padding:0;">
                                <button type="submit" class="btn btn-primary"><i
                                        class="fas fa-paper-plane mr-2"></i>{{ ___('Save and Send') }}</button>
                            </div>
                        </div><!-- .card-innr -->
                    </div><!-- .card -->
                </div><!-- .col -->
                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <div class="content-area card">
                        <div class="card-innr">
                            <table
                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: f9f9f900;width:100%"
                                cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr style="vertical-align: top">
                                        <td
                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">

                                            <div class="u-row-container"
                                                style="padding: 0px;background-color: f9f9f900">
                                                <div class="u-row"
                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                    <div
                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                        <div class="u-col u-col-100"
                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                            <div style="width: 100% !important;">
                                                                <div
                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">

                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <table height="0px" align="center"
                                                                                        border="0" cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid f9f9f900;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                        <tbody>
                                                                                            <tr
                                                                                                style="vertical-align: top">
                                                                                                <td
                                                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                    <span>&#160;</span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                            <div class="u-row-container"
                                                style="padding: 0px;background-color: transparent">
                                                <div class="u-row"
                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #252b3b 0%, #384e71 100%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                    <div
                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                        <div class="u-col u-col-100"
                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                            <div style="width: 100% !important;">

                                                                <div
                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <table width="100%"
                                                                                        cellpadding="0" cellspacing="0"
                                                                                        border="0">
                                                                                        <tr>
                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                align="center">

                                                                                                <img loading="lazy" align="center"
                                                                                                    border="0"
                                                                                                    src="https://app.disruptive.center/asset-correo/gosen2.png"
                                                                                                    alt="Image"
                                                                                                    title="Image"
                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <div
                                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                        <p
                                                                                            style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                                            <span
                                                                                                style="font-size: 28px; line-height: 39.2px; color: #ffffff; font-family: Roboto, sans-serif;">
                                                                                                {{ ___($title) }}
                                                                                            </span>
                                                                                        </p>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="u-row-container"
                                                style="padding: 0px;background-color: transparent">
                                                <div class="u-row"
                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;">
                                                    <div
                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                        <div class="u-col u-col-100"
                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                            <div style="width: 100% !important;">
                                                                <div
                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                    </table>
                                                                    <div>
                                                                        {{ ___($content) }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div id="viewer"
                                                style="height:auto;width:595px;border-left: 11px ; ridge #a9adad;">
                                            </div>


                                            <table style="font-family:'Roboto',sans-serif;" role="presentation"
                                                cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 40px 30px;font-family:'Roboto',sans-serif;"
                                                            align="center">
                                                            <div
                                                                style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                <p style="font-size: 14px; line-height: 140%;"><span
                                                                        style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                style="font-size: 16px; line-height: 22.4px;">{{ $footer }}</span></em></span><br /><span
                                                                        style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                </p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="u-row-container"
                                                style="padding: 0px;background-color: transparent">
                                                <div class="u-row"
                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #252b3b 0%, #384e71 100%); border-bottom-left-radius: 20px;
                        border-bottom-right-radius: 20px;">
                                                    <div
                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                        <div class="u-col"
                                                            style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                            <div style="width: 100% !important;">

                                                                <div
                                                                    style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <div align="center">
                                                                                        <div
                                                                                            style="display: table; max-width:187px;">

                                                                                            <table align="left"
                                                                                                border="0"
                                                                                                cellspacing="0"
                                                                                                cellpadding="0"
                                                                                                width="32"
                                                                                                height="32"
                                                                                                style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;">
                                                                                                <tbody>
                                                                                                    <tr
                                                                                                        style="vertical-align: top">
                                                                                                        <td align="left"
                                                                                                            valign="middle"
                                                                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                                                            <a href=" "
                                                                                                                title="Gosen"
                                                                                                                target="_blank">
                                                                                                                <img loading="lazy" src="https://app.disruptive.center/asset-correo/gosen1.png"
                                                                                                                    alt="Gosen"
                                                                                                                    title="Gosen"
                                                                                                                    width="32"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                                                            </a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>

                                                                                        </div>
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <div
                                                                                        style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                                        <p
                                                                                            style="line-height: 140%; font-size: 14px;">
                                                                                            <span
                                                                                                style="font-size: 14px; line-height: 19.6px;"><span
                                                                                                    style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;">
                                                                                                    <span
                                                                                                        class="text-white"
                                                                                                        style="line-height: 19.6px; font-size: 14px;">Company
                                                                                                        &copy;&nbsp; All
                                                                                                        Rights
                                                                                                        Reserved</span></span></span>
                                                                                        </p>
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>


                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>




                                            <div class="u-row-container"
                                                style="padding: 0px;background-color: transparent">
                                                <div class="u-row"
                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                    <div
                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                        <div class="u-col u-col-100"
                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                            <div style="width: 100% !important;">

                                                                <div
                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                    align="left">

                                                                                    <div
                                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">

                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- .card-innr -->
                    </div><!-- .card -->
                </div><!-- .col -->
            </div>
        </div>
    </div>
@push('footer')
        <link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css') }}?ver=1.0">
        <script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>


<script src="https://rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/resizimg/trumbowyg.resizimg.min.js"></script>

        <link preload href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
            (function($) {
                if ($('.editor').length > 0) {
                    $('.editor').trumbowyg({
                        autogrow: true
                    });
                }
            })(jQuery);
        </script>

        <script>
            var ClipboardHelper = {

                copyElement: function($element) {
                    this.copyText($element.text());
                },
                copyText: function(text) // Linebreaks with \n
                {
                    var $tempInput = $("<textarea>");
                    $("body").append($tempInput);
                    $tempInput.val(text).select();
                    document.execCommand("copy");
                    $tempInput.remove();
                }
            };
            $('#editor').trumbowyg({
                semantic: false,
                /* (i.e. <em> instead of <i>, <strong> instead of <b>, etc.).  */
                minimalLinks: true,
                btnsDef: {
                    spec1: {
                        fn: function() {
                            ClipboardHelper.copyText('-');
                        },
                        text: '-',
                        title: '-',
                        hasIcon: false,
                    },
                    spec3: {
                        fn: function() {
                            ClipboardHelper.copyText('※');
                        },
                        text: '※',
                        title: '※',
                        hasIcon: false,
                    },
                    spec2: {
                        fn: function() {
                            ClipboardHelper.copyText('▶');
                        },
                        text: '▶',
                        title: '▶',
                        hasIcon: false,
                    },
                    specList: {
                        dropdown: ['spec1', 'spec2', 'spec3'],
                        text: '복사하기',
                        title: '특수문자 복사하기',
                        hasIcon: false,
                    }
                },
                btns: [
                    ['viewHTML'],
                    ['strong', 'em'],
                    ['link'],
                    ['fontsize'],
                    ['foreColor'], /*, 'backColor'*/
                    ['unorderedList', 'orderedList'],
                    ['specList'],

                ],
                plugins: {
                    resizimg: {
                        minSize: 64,
                        step: 16,
                    },
                    colors: {
                        colorList: ['ff0000', '0000ff', '000000',
                            'E72419', '003186', '002369', '4FC7E7', '00CAD4'
                        ],
                    },
                    fontsize: {
                        sizeList: [
                            '14px',
                            '18px',
                            '22px'
                        ],
                        allowCustomSize: false
                    }
                },
                tagsToRemove: ['script', 'link']

            });

            function viewHtml() {
                // Get HTML content
                $('#viewer').html($('#editor').trumbowyg('html'));
            }
        </script>
    @endpush
