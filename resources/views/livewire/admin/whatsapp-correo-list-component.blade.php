@push('header')
    <link preload rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
@endpush
<div class="row">
    <div class="page-content">
        <div class="container">
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{ ___('Programar y editar correos electrónicos') }}</h4>
                    </div>
                    <div class="col-12">
                        <div class="row d-flex justify-content-center align-items-center">
                            <div class="col-8">
                                <div class="app-search d-none d-lg-block">
                                    <div class="position-relative">
                                        <input type="text" class="form-control"
                                            placeholder="{{ ___('Escriba el dia de envio o el estilo del correo para filtrar') }}"
                                            wire:model.debounce.750ms="search">
                                        <span class="ri-search-line"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <span wire:loading class="spinner-border spinner-border-sm mt-1" role="status"
                                    aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">{{ ___('Header') }}</th>
                                    <th scope="col">{{ ___('Footer') }}</th>
                                    <th scope="col">{{ ___('Type') }}</th>
                                    <th scope="col">{{ ___('shipping day') }}</th>
                                    <th scope="col">{{ ___('Time') }}</th>
                                    <th scope="col">{{ ___('Language') }}</th>
                                    <th scope="col">{{ ___('Status') }}</th>
                                    <th scope="col" style="width: 17%;">{{ ___('Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($correos->isNotEmpty())
                                    @foreach ($correos as $item)
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center justify-content-center">
                                                    <strong>
                                                        {{ $item->id }}
                                                    </strong>
                                                </div>
                                            </td>
                                            <td>{{ $item->header == null ? '---' : $item->header }}</td>
                                            <td>{{ $item->footer == null ? '---' : $item->footer }}</td>
                                            <td>{{ ucfirst($item->type_mail) }}</td>
                                            @if ($item->id == $editing)
                                                <td>
                                                    <div class="d-flex align-items-center justify-content-center"><input
                                                            type="number" step="any" style="width:20%;"
                                                            class="form-control text-center" placeholder=""
                                                            value="" wire:model.defer="diaValor" />
                                                        <button class="ml-2 btn btn-xs btn-dark"
                                                            wire:click="guardarDia('{{ $item->id }}')"
                                                            value="{{ $item->id }}">
                                                            <i class="fa fa-save"></i>
                                                        </button>
                                                        <button class="ml-2 btn btn-xs btn-danger"
                                                            wire:click="cancelarGuardarDia()" value="">
                                                            <i class="fa fa-ban"></i>
                                                        </button>
                                                    </div>
                                                @else
                                                <td wire:click="editValue('{{ $item->id }}')">
                                                    {{ $item->number_days }}
                                            @endif
                                            </td>
                                            @if ($item->id == $editingHora)
                                                <td>
                                                    <div class="d-flex align-items-center justify-content-center">
                                                        <input type="time" step="any" style="width:20%;"
                                                            class="form-control text-center" placeholder=""
                                                            value="" wire:model.defer="horaValor" />
                                                        <button class="ml-2 btn btn-xs btn-dark"
                                                            wire:click="guardarHora('{{ $item->id }}')"
                                                            value="{{ $item->id }}">
                                                            <i class="fa fa-save"></i>
                                                        </button>
                                                        <button class="ml-2 btn btn-xs btn-danger"
                                                            wire:click="cancelarGuardarHora()" value="">
                                                            <i class="fa fa-ban"></i>
                                                        </button>
                                                    </div>
                                                @else
                                                <td wire:click="editHora('{{ $item->id }}')">
                                                    {{ $item->time == null ? '--' : date('G:i a', strtotime($item->time)) }}
                                            @endif
                                            </td>
                                            <td>
                                                {{ $item->lang }}
                                            </td>
                                            <td>
                                                <div class="input-item d-flex justify-content-center"
                                                    style="padding-top: 20%;">
                                                    <input name="personal" type="checkbox"
                                                        {{ $item->status == true ? 'checked' : ' ' }}
                                                        class="input-switch input-switch-sm checkbox-personal"
                                                        id="check_personal-{{ $item->id }}"
                                                        wire:change="$emit('actualizarEstadoPersonal', {{ $item->id }}, $event.target.checked)">
                                                    <label for="check_personal-{{ $item->id }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row d-flex align-items-center justify-content-center">
                                                    <div class="col">
                                                        <button class="btn btn-xs btn-success btn-outline"
                                                            data-toggle="modal"
                                                            data-target="#modal-large{{ $item->id }}">
                                                            <i class="fa fa-eye"></i></button>
                                                        {{-- <button class="btn btn-xs btn-info btn-outline">
                                                            <i class="fa fa-edit"></i></button> --}}
                                                        <button class="btn btn-xs btn-danger btn-outline borrar"
                                                            value="{{ $item->id }}">
                                                            <i class="fa fa-trash"></i></button>
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <small>({{ ___('Sin Registros') }})</small>
                                @endif
                            </tbody>
                        </table>
                        {{ $correos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('modals')
        @foreach ($correos as $his)
            <div class="modal fade" id="modal-large{{ $his->id }}" tabindex="-1" style="padding-right: 17px;">
                <div class="modal-dialog modal-dialog-lg modal-dialog-centered">
                    <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                            aria-label="Close"><em class="ti ti-close"></em></a>
                        <div class="popup-body">
                            <div class="content-area popup-content">
                                <div class="card-head d-flex justify-content-between align-items-center">
                                    <h4 class="card-title mb-2" style="letter-spacing:2px;">
                                        <i class="fa fa-envelope-open mr-3"></i>{{ ___('Email sent on day') }}
                                        {{ $his->number_days }}
                                    </h4>
                                    <small> {{ ___('Creado el ') }} :
                                        {{ $his->created_at->format('Y-m-d g:i A') }}
                                    </small>
                                </div>
                                <ul class="data-details-list">
                                    <li>
                                        <div class="data-details-head">{{ ___('Hora de envio') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $his->time == null ? '--' : date('G:i a', strtotime($his->time)) }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Lenguaje') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $his->lang }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ ___('Slug') }}</div>
                                        <div class="data-details-des" style="display: flex; align-items: center;">
                                            <span>{{ $his->slug }}
                                            </span>
                                            <button value="{{ $his->slug }}"
                                                class="btn btn-primary copíar-slug">{{ ___('Copiar al portapapeles') }}</button>
                                        </div>
                                    </li>
                                </ul>

                                @if ($his->type_mail == 'design')
                                    <table
                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f9f9f9;width:100%"
                                        cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr style="vertical-align: top">
                                                <td
                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: #f9f9f9">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table height="0px"
                                                                                                align="center"
                                                                                                border="0"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                width="100%"
                                                                                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                <tbody>
                                                                                                    <tr
                                                                                                        style="vertical-align: top">
                                                                                                        <td
                                                                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                            <span>&#160;</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">

                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">

                                                                                            <table width="100%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('images/logo-mail.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 46%;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">

                                                                                            <div
                                                                                                style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                <p
                                                                                                    style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                                                    <span id="cabecera"
                                                                                                        style="font-size: 28px; line-height: 39.2px; color: #280F53; font-family: Roboto, sans-serif;">
                                                                                                        {{ $his->header != null ? ___($his->header) : ___('Not available') }}
                                                                                                    </span>
                                                                                                </p>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent; margin-top: -4%;">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">

                                                            <div class="texta" id="viewer"
                                                                style="    height: auto;
                                                            width: 560px;
                                                            text-align: center;
                                                            margin: 0 auto; margin-top:20px;">
                                                                {!! ___($his->content) !!}
                                                            </div>
                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                role="presentation" cellpadding="0" cellspacing="0"
                                                                width="100%" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:45px 40px 0px;font-family:'Roboto',sans-serif;"
                                                                            align="center">
                                                                            <div
                                                                                style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                                <p
                                                                                    style="font-size: 14px; line-height: 140%;font-style: italic;">
                                                                                    <span id="piedepagina"
                                                                                        style="color: #888888;font-style: italic; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                class="text-primary"
                                                                                                style="font-size: 16px; font-style: italic; line-height: 22.4px;">{{ $his->footer != null ? ___($his->footer) : ___('Not available') }}</span></em></span><br /><span
                                                                                        style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                                </p>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg,#280f534b 100%,#280f534b 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col"
                                                                    style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <div align="center">
                                                                                                <p
                                                                                                    style="line-height: 140%; font-size: 14px;">
                                                                                                    <span
                                                                                                        style="font-size: 14px; line-height: 19.6px;"><span
                                                                                                            style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                                                style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                                                {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                                                </p>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table
                                                                                style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table width="70%"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                border="0">
                                                                                                <tr>
                                                                                                    <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                        align="center">
                                                                                                        <img loading="lazy" align="center"
                                                                                                            border="0"
                                                                                                            src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                                            alt="Image"
                                                                                                            title="Image"
                                                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <div align="center">
                                                                                                <a href="https://app.disruptive.center/"
                                                                                                    target="_blank"
                                                                                                    style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                                                    <span
                                                                                                        style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                                            style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                                        </span></span>
                                                                                                </a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <div
                                                                                                style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                @else
                                    <table
                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: f9f9f900;width:100%"
                                        cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr style="vertical-align: top">
                                                <td
                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: f9f9f900">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <table height="0px"
                                                                                                align="center"
                                                                                                border="0"
                                                                                                cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                width="100%"
                                                                                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid f9f9f900;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                <tbody>
                                                                                                    <tr
                                                                                                        style="vertical-align: top">
                                                                                                        <td
                                                                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                            <span>&#160;</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="texta" id="viewer2"
                                                        style="    height: auto; width: 560px; text-align: center; margin: 0 auto; margin-top:20px;">
                                                        {!! $his->content !!}
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col"
                                                                    style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <div align="center">
                                                                                                <div
                                                                                                    style="display: table; max-width:187px;">
                                                                                                    <table align="left"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="32"
                                                                                                        height="32"
                                                                                                        style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;">
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="u-row-container"
                                                        style="padding: 0px;background-color: transparent">
                                                        <div class="u-row"
                                                            style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                            <div
                                                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                <div class="u-col u-col-100"
                                                                    style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                    <div style="width: 100% !important;">
                                                                        <div
                                                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                            <table style="font-family:'Roboto',sans-serif;"
                                                                                role="presentation" cellpadding="0"
                                                                                cellspacing="0" width="100%"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                            align="left">
                                                                                            <div
                                                                                                style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        @endforeach
    @endsection
    @push('footer')
        <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
            $('.borrar').click(function(event) {
                var id = $(this).attr('value');
                event.preventDefault();
                Swal.fire({
                    title: 'Estas seguro?',
                    text: "Esta acción no se puede revertir!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emit('borrarCorreo', id);
                    }
                })
            });
        </script>
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                showCloseButton: true,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.a6ddEventListener('mouseleave', Swal.resumeTimer)
                }
            });
            window.addEventListener('alert', ({
                detail: {
                    type,
                    message
                }
            }) => {
                Toast.fire({
                    icon: type,
                    title: message
                })
            })
            $('.copíar-slug').click(function(event) {
                var slug = $(this).attr('value');
                Livewire.emit('copíarSlug', slug);
            });
            Livewire.on('copiarSlugCorreoDisruptive', data => {
                navigator.clipboard.writeText(data)
                    .then(() => {
                        toastr.success('Copied.');
                    })
                    .catch((err) => {
                        console.error('No se pudo copiar el texto: ', err);
                    });
            });
        </script>
    @endpush
</div>
