<div>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                            <div class="content-area card">
                                <div class="card-innr">
                                    <div class="row d-flex align-items-center justify-content-center">
                                        <h5 class="text-center col-6">
                                            {{ ___('Crear Correo') }}
                                        </h5>

                                        <div class="form-group col-6" wire:ignore>
                                            <label for=""></label>
                                            <select class="custom-select" name="" id=""
                                                wire:model="lang">
                                                <option selected value="0">
                                                    --{{ ___('Seleccione un lenguage') }}--
                                                </option>
                                                @foreach ($lenguages as $lang)
                                                    <option value="{{ $lang->code }}">{{ $lang->name }}</option>
                                                @endforeach
                                            </select>
                                            <small
                                                class="ml-2 text-danger lang-span d-none">{{ ___('The footer field is required') }}</small>
                                        </div>

                                    </div>

                                    <div class="button-items d-flex justify-content-center">
                                        <button class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"
                                            id="agregar-nombre-empresa"type="button">{{ ___('Nombre del Sistema') }}</button>
                                        <button class="btn btn-primary btn-sm   btn-rounded  waves-effect waves-light"
                                            id="agregar-nombre-user" type="button">{{ ___('Nombre Usuario') }}</button>
                                    </div>


                                    @if ($design)
                                        <div class="col-12 mt-2" id="group_header">
                                            <label for="">{{ ___('Title') }} </label> <small
                                                class="ml-2 text-danger header-span d-none">{{ ___('The header field is required') }}</small>
                                            <input type="text" name="header" class="form-control" id="header"
                                                wire:model.defer="header">
                                        </div>
                                    @endif

                                    <div class="col-12 mt-2" id="group_content" wire:ignore>
                                        <label for="">{{ ___('Content') }} </label> <small
                                            class="ml-2 text-danger content-span d-none">{{ ___('The content field is required') }}</small>
                                        {{-- <div id="editor" placeholder="Escriba aqui su contenid0"
                                            wire:model.defer="content"></div>
                                        <button type="button" class="btn btn-success" id="copiar-contenido"
                                            value="" style="padding: 3px;"><i
                                                class="fa fa-search mr-2"></i>{{ ___('View changes') }}</button> --}}

                                        <textarea id="editor" name="editor" class="form-control input-textarea editor" wire:ignore></textarea>
                                        <button type="button" class="btn btn-success" onclick="viewHtml();"
                                            value="" style="padding: 3px;"><i
                                                class="fa fa-search mr-2"></i>{{ ___('View changes') }}</button>
                                    </div>

                                    @if ($design)
                                        <div class="col-12 mt-2" id="group_footer">
                                            <label for="">{{ ___('Footer') }} </label> <small
                                                class="ml-2 text-danger footer-span d-none">{{ ___('The footer field is required') }}</small>
                                            <input type="text" name="footer" class="form-control" id="footer"
                                                wire:model.defer="footer">
                                        </div>
                                    @endif

                                    <div class="row mt-3 mr-1">
                                        <div class="col-12">
                                            <button type="button" id="guardar" class="btn btn-primary float-right"><i
                                                    class="fas fa-paper-plane mr-2"></i>{{ ___('Save Email') }}</button>
                                            <small wire:loading>
                                                {{ ___('Cargando') }}...
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                            <div class="content-area card fondo">
                                <div class="card-innr">
                                    <div class="input-item col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                        <input wire:loading.attr="disabled" name="type_mail" type="checkbox"
                                            class="input-switch input-switch-sm checkbox" id="CheckBox" checked
                                            wire:model="design">
                                        <label for="CheckBox">{{ ___('With Design') }}</label>
                                    </div>
                                    @if ($design == true)
                                        <div wire:ignore id="mailDesign">
                                            <table
                                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f9f9f9;width:100%"
                                                cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr style="vertical-align: top">
                                                        <td
                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: #f9f9f9">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0" cellspacing="0"
                                                                                        width="100%" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:15px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        height="0px"
                                                                                                        align="center"
                                                                                                        border="0"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        width="100%"
                                                                                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f9f9f9;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                        <tbody>
                                                                                                            <tr
                                                                                                                style="vertical-align: top">
                                                                                                                <td
                                                                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                                    <span>&#160;</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">

                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">

                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">


                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:35px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">

                                                                                                    <table
                                                                                                        width="100%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('images/logo-mail.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 46%;" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 30px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">

                                                                                                    <div
                                                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                        <p
                                                                                                            style="font-size: 14px; line-height: 140%; text-align: center;">
                                                                                                            <span
                                                                                                                id="cabecera"
                                                                                                                style="font-size: 28px; line-height: 39.2px; color: #280F53; font-family: Roboto, sans-serif;">
                                                                                                                {{ ___('Header') }}
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent; margin-top: -4%;">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word; background-image: linear-gradient(45deg, #ffffff 100%, #ffffff 0%); border-top-left-radius: 20px; border-top-right-radius: 20px;">

                                                                    <div class="texta" id="viewer"
                                                                        style="height: auto; width: 80%; text-align: center; margin: 0 auto; margin-top:20px;">
                                                                    </div>
                                                                    <table style="font-family:'Roboto',sans-serif;"
                                                                        role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%"
                                                                        border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:45px 40px 0px;font-family:'Roboto',sans-serif;"
                                                                                    align="center">
                                                                                    <div
                                                                                        style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                                        <p
                                                                                            style="font-size: 14px; line-height: 140%;font-style: italic;">
                                                                                            <span id="piedepagina"
                                                                                                style="color: #888888;font-style: italic; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                        class="text-primary"
                                                                                                        style="font-size: 16px; font-style: italic; line-height: 22.4px;">{{ ___('Footer') }}</span></em></span><br /><span
                                                                                                style="color: #888888; font-size: 14px; line-height: 19.6px;"><em><span
                                                                                                        style="font-size: 16px; line-height: 22.4px;">&nbsp;</span></em></span>
                                                                                        </p>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg,#280f534b 100%,#280f534b 0%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col"
                                                                            style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <div
                                                                                                        align="center">
                                                                                                        <p
                                                                                                            style="line-height: 140%; font-size: 14px;">
                                                                                                            <span
                                                                                                                style="font-size: 14px; line-height: 19.6px;"><span
                                                                                                                    style="color: #ecf0f1; font-size: 14px; line-height: 19.6px;"><span
                                                                                                                        style="line-height: 19.6px; font-size: 14px; color: #280f53;">
                                                                                                                        {{ ___('Siguenos para enterarte de las últimas novedades') }}</span></span></span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;width: 85%;margin-left: 10%;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/1.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/2.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/3.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/4.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/5.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 0px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        width="70%"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        border="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                                align="center">
                                                                                                                <img loading="lazy" align="center"
                                                                                                                    border="0"
                                                                                                                    src="{{ asset('imagenes/iconos_redes/6.png') }}"
                                                                                                                    alt="Image"
                                                                                                                    title="Image"
                                                                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 40%;" />

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:5px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <div
                                                                                                        align="center">
                                                                                                        <a href="https://app.disruptive.center/"
                                                                                                            target="_blank"
                                                                                                            style="box-sizing: border-box;display: inline-block;font-family:'Roboto',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 10px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                                                                                            <span
                                                                                                                style="display: block;line-height: 120%;margin-bottom: 10%;"><span
                                                                                                                    style="font-size: 12px; line-height: 21.6px; color: #280f53;">app.disruptive.center
                                                                                                                </span></span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f9f9f9;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <div
                                                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <div id="mailClassic">
                                            <table
                                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: f9f9f900;width:100%"
                                                cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr style="vertical-align: top">
                                                        <td
                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: f9f9f900">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <table
                                                                                                        height="0px"
                                                                                                        align="center"
                                                                                                        border="0"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        width="100%"
                                                                                                        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid f9f9f900;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                        <tbody>
                                                                                                            <tr
                                                                                                                style="vertical-align: top">
                                                                                                                <td
                                                                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                                                    <span>&#160;</span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-top-left-radius: 20px; border-top-right-radius: 20px;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div wire:ignore class="texta" id="viewer2"
                                                                style="    height: auto;
                                                    width: 80%;
                                                    text-align: center;
                                                    margin: 0 auto; margin-top:20px;">
                                                                <h1>
                                                                    {{ ___('Your changes will appear here') }}
                                                                </h1>
                                                                <div class="pt-3">
                                                                    <i style="font-size: 47px;"
                                                                        class="ti ti-exchange-vertical"></i>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-image: linear-gradient(45deg, #ffffff 0%, #ffffff 100%); border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col"
                                                                            style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 10px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <div
                                                                                                        align="center">
                                                                                                        <div
                                                                                                            style="display: table; max-width:187px;">
                                                                                                            <table
                                                                                                                align="left"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                width="32"
                                                                                                                height="32"
                                                                                                                style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;">
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-row-container"
                                                                style="padding: 0px;background-color: transparent">
                                                                <div class="u-row"
                                                                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: f9f9f900;">
                                                                    <div
                                                                        style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                                                        <div class="u-col u-col-100"
                                                                            style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                                                            <div style="width: 100% !important;">
                                                                                <div
                                                                                    style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                                                                    <table
                                                                                        style="font-family:'Roboto',sans-serif;"
                                                                                        role="presentation"
                                                                                        cellpadding="0"
                                                                                        cellspacing="0" width="100%"
                                                                                        border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:0px 40px 30px 20px;font-family:'Roboto',sans-serif;"
                                                                                                    align="left">
                                                                                                    <div
                                                                                                        style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif



                                </div><!-- .card-innr -->
                            </div><!-- .card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('header')
    <link preload rel="stylesheet" href="{{ asset('assets/plugins/trumbowyg/ui/trumbowyg.min.css') }}?ver=1.0">
@endpush
@push('footer')
    <script src="{{ asset('assets/plugins/trumbowyg/trumbowyg.min.js') }}?ver=101"></script>
    <script src="{{ asset('assets/plugins/trumbowyg/plugins/fontsize/trumbowyg.fontsize.js') }}?ver=101"></script>
    <script src="https://rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/resizimg/trumbowyg.resizimg.min.js">
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                theme: "bootstrap4"
            });
        });
        (function($) {
            if ($('.editor').length > 0) {
                $('.editor').trumbowyg({
                    autogrow: true
                });
            }
        })(jQuery);
    </script>
    <script type="text/javascript">
        var ClipboardHelper = {
            copyElement: function($element) {
                this.copyText($element.text());
            },
            copyText: function(text) // Linebreaks with \n
            {
                var $tempInput = $("<textarea>");
                $("body").append($tempInput);
                $tempInput.val(text).select();
                document.execCommand("copy");
                $tempInput.remove();
            }
        };
        var editor = $('#editor').trumbowyg({
            semantic: false,
            /* (i.e. <em> instead of <i>, <strong> instead of <b>, etc.).  */
            minimalLinks: true,
            btnsDef: {
                spec1: {
                    fn: function() {
                        ClipboardHelper.copyText('-');
                    },
                    text: '-',
                    title: '-',
                    hasIcon: false,
                },
                spec3: {
                    fn: function() {
                        ClipboardHelper.copyText('※');
                    },
                    text: '※',
                    title: '※',
                    hasIcon: false,
                },
                spec2: {
                    fn: function() {
                        ClipboardHelper.copyText('▶');
                    },
                    text: '▶',
                    title: '▶',
                    hasIcon: false,
                },
                specList: {
                    dropdown: ['spec1', 'spec2', 'spec3'],
                    text: '복사하기',
                    title: '특수문자 복사하기',
                    hasIcon: false,
                }
            },
            btns: [
                ['viewHTML'],
                ['strong', 'em'],
                ['link'],
                ['fontsize'],
                ['foreColor'], /*, 'backColor'*/
                ['unorderedList', 'orderedList'],
                ['specList'],
            ],
            plugins: {
                resizimg: {
                    minSize: 64,
                    step: 16,
                },
                colors: {
                    colorList: ['ff0000', '0000ff', '000000',
                        'E72419', '003186', '002369', '4FC7E7', '00CAD4'
                    ],
                },
            },
            tagsToRemove: ['script', 'link']
        });

        function viewHtml() {
            // Get HTML content
            $('#viewer').html(editor.trumbowyg('html'));
            $('#viewer2').html(editor.trumbowyg('html'));

        }
        var variable_user = '{user}';
        var variable_name_app = '{name_app}';
        var focusedElement;
        var currentText;

        // Manejar el cambio de foco en el input con id "header"
        $("#header").focus(function() {
            focusedElement = $(this);
        });

        // Manejar el cambio de foco en el textarea con id "editor"
        editor.on('tbwfocus', function() {
            focusedElement = $(this);
        });
        $('#agregar-nombre-user').click(function() {
            // Verificar si el elemento enfocado es el textarea con id "editor"
            if (focusedElement.attr('id') == 'editor') {
                editor.trumbowyg('html', editor.trumbowyg('html') + ' ' + variable_user);
            }
            // Verificar si el elemento enfocado es el input con id "header"
            if (focusedElement.attr('id') == 'header') {
                currentText = focusedElement.val();
                focusedElement.val(currentText + ' ' + variable_user);
            }
        });

        $('#agregar-nombre-empresa').click(function() {
            // Verificar si el elemento enfocado es el textarea con id "editor"
            if (focusedElement.attr('id') == 'editor') {
                editor.trumbowyg('html', editor.trumbowyg('html') + ' ' + variable_name_app);
            }
            // Verificar si el elemento enfocado es el input con id "header"
            if (focusedElement.attr('id') == 'header') {
                currentText = focusedElement.val();
                focusedElement.val(currentText + ' ' + variable_name_app);
            }
        });
        $(document).ready(function() {
            // Manejar el click del botón
            $('#agregar-btn').click(function() {
                var variable = '{variable}';
                var inputText = $('#input-text').val();
                var textareaText = $('#textarea-text').val();

                // Agregar la variable al input
                $('#input-text').val(inputText + ' ' + variable);

                // Agregar la variable al textarea
                $('#textarea-text').val(textareaText + ' ' + variable);
            });
            $('#guardar').click(function() {
                Livewire.emit('guardarCorreo',
                    $('#header').val(),
                    editor.trumbowyg('html'),
                    $('#footer').val(),
                    $('#CheckBox').is(':checked'),
                    $('.custom-select').val(),
                );
            });
        });
    </script>

    <script>
        Livewire.on('errores', errores => {
            if ($.inArray("header", Object.keys(errores)) !== -1) {
                $('.header-span').removeClass('d-none');
            }
            if ($.inArray("content", Object.keys(errores)) !== -1) {
                $('.editor-span').removeClass('d-none');
            }
            if ($.inArray("footer", Object.keys(errores)) !== -1) {
                $('.footer-span').removeClass('d-none');
            }
            if ($.inArray("lang", Object.keys(errores)) !== -1) {
                $('.lang-span').removeClass('d-none');
            }
        })


        document.getElementById("header").addEventListener("keyup", testpa);
        document.getElementById("footer").addEventListener("keyup", testpa);

        function testpa() {
            //es el campo input 
            var txtInput = document.querySelector('#header');
            var txtInput2 = document.querySelector('#footer');
            //es el span
            var divCopia = document.getElementById('cabecera');
            var divCopia2 = document.getElementById('piedepagina');
            //monto total que tiene el usuario
            txtInput.addEventListener('keyup', () => {
                divCopia.innerHTML = txtInput.value;
            });
            txtInput2.addEventListener('keyup', () => {
                divCopia2.innerHTML = txtInput2.value;
            });
        }
    </script>
    <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        window.addEventListener('alert', ({
            detail: {
                type,
                message
            }
        }) => {
            Toast.fire({
                icon: type,
                title: message
            })
        })
    </script>
@endpush
</div>
