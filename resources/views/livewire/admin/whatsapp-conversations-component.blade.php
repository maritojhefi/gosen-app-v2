@include('user.includes.whatsappConversation-include')
<div class="page-content">
    <div class="card-innr">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="p-3 pb-0 text-center text-primary">{{ ___('Conversaciones de Whatsapp') }}</h3>
                        <div class="wrapper">
                            <div class="container">
                                <div class="left">
                                    <div class="top">
                                        <input type="text" placeholder="Search" />
                                    </div>
                                    <ul class="people">
                                        @php
                                        $contador = 1;
                                        @endphp
                                        @foreach ($conversaciones as $conversacion => $chat)
                                            <li class="person" data-chat="person{{ $contador }}">
                                                @php
                                                    $userOrigen = App\Helpers\GosenHelper::userConversationWhatsappNumberExist($chat[0]->origen);
                                                    $userDestino = App\Helpers\GosenHelper::userConversationWhatsappNumberExist($chat[0]->destino);
                                                    $data = json_decode($chat[0]->mensaje);
                                                    $texto = isset($data->text) ? $data->text : null;
                                                    $imageUrl = null;
                                                    if (isset($data->image) && is_object($data->image)) {
                                                        $imageUrl = isset($data->image->url) ? $data->image->url : null;
                                                    }
                                                @endphp
                                                <img loading="lazy" class="bg-white"
                                                    src="{{ $userOrigen == 'disruptive' ? asset('disruptive/imagenes/d-morado.png') : ($userOrigen != null ? asset2('imagenes/perfil') . '/' . $userOrigen->foto : asset2('imagenes/user-default.jpg')) }}"
                                                    alt="" style="border: solid 3px white;"
                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                    data-bs-original-title="{{ $userOrigen == 'disruptive' ? 'Disruptive' : ($userOrigen != null ? $userOrigen->name : $chat[0]->origen) }}" />

                                                <img loading="lazy" class="bg-white"
                                                    src="{{ $userDestino == 'disruptive' ? asset('disruptive/imagenes/d-morado.png') : ($userDestino != null ? asset2('imagenes/perfil') . '/' . $userDestino->foto : asset2('imagenes/user-default.jpg')) }}"
                                                    alt=""
                                                    style="margin-left: -20px !important; border: solid 3px white;"
                                                    data-bs-toggle="tooltip" data-bs-placement="top"
                                                    data-bs-original-title="{{ $userDestino == 'disruptive' ? 'Disruptive' : ($userDestino != null ? $userDestino->name : $chat[0]->destino) }}" />
                                                @php
                                                    $numeroCliente = $chat[0]->origen != env('NUMERO_WHATSAPP_BUSINESS') ? $chat[0]->origen : $chat[0]->destino;
                                                @endphp
                                                <span class="name">{{ $numeroCliente }}</span>
                                                <span
                                                    class="time">{{ App\Helpers\GosenHelper::timeago($chat[0]->updated_at) }}</span>
                                                <span class="preview">{!! $texto ? $texto : '<i class="fa fa-image mr-2"></i>' . ___('Imagen') . '' !!}</span>
                                            </li>
                                            @php
                                            $contador++;
                                            @endphp
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="right">
                                    <div class="top">
                                        <span>{{ ___('Conversing with') }}:
                                            <span class="name">
                                            </span>
                                        </span>
                                    </div>
                                    @php
                                    $contador2 = 1;
                                    @endphp
                                    @foreach ($conversaciones as $conversacion => $chats)
                                        <div class="chat" data-chat="person{{ $contador2 }}">
                                            <div class="conversation-start">
                                                {{-- <span>Today, 5:38 PM</span> --}}
                                            </div>
                                            @foreach ($chats as $chat)
                                                @if ($chat->origen == env('NUMERO_WHATSAPP_BUSINESS'))
                                                    <div class="bubble me">
                                                        {!! $chat->contenido !!}
                                                        <br>
                                                        <small class="text-muted" style="font-size: 10px;">
                                                            ({{ App\Helpers\GosenHelper::timeago($chat->updated_at) }})
                                                        </small>
                                                    </div>
                                                @else
                                                    <div class="bubble you">
                                                        {!! $chat->contenido !!}
                                                        <br>
                                                        <small class="text-muted text-white-50"
                                                            style="font-size: 10px;">
                                                            ({{ App\Helpers\GosenHelper::timeago($chat->updated_at) }})
                                                        </small>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        @php
                                        $contador2++;
                                        @endphp
                                    @endforeach
                                    <div class="write">
                                        <a href="javascript:;" class="write-link attach"></a>
                                        <input type="text" />
                                        <a href="javascript:;" class="write-link smiley"></a>
                                        <a href="javascript:;" class="write-link send"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
