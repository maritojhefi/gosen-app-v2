{{-- @push('header')
    <style>
        .switch-bonus-stag::before {
            background: {{ asset(theme_color()) }} !important;
        }
    </style>
@endpush --}}
<div class="page-content">
    <div class="card-innr card-innr-fix2">
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        @if ($editando)
                            <h4 class="card-title">{{ ___('Editando') . ' ' . $paquete->nombre }} <i class="fa fa-close">
                                </i></h4>
                        @else
                            <h4 class="card-title">{{ ___('Crear paquete') }}</h4>
                        @endif
                        <div>
                            <div class="mt-2">
                                <label class="mb-0">{{ ___('Nombre') }}:</label>
                                <input class="form-control @error('nombre') is-invalid @enderror"
                                    wire:model.defer="nombre" type="text">
                                @error('nombre')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">{{ ___('Valor') }} (USD):</label>
                                <input class="form-control @error('valor_usd') is-invalid @enderror"
                                    wire:model.debounce500ms="valor_usd" wire:change="actualizarTokens" type="number"
                                    step="0.01">
                                @error('valor')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">Tokens:</label>
                                <input class="form-control @error('tokens') is-invalid @enderror"
                                    wire:model.defer="tokens" type="number" step="0.01" readonly>
                                @error('tokens')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">{{ ___('Descuento') }} %:</label>
                                <input class="form-control @error('descuento') is-invalid @enderror"
                                    wire:model.defer="descuento" type="number" step="any">
                                @error('descuento')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">Stock {{ ___('inicial') }}:</label>
                                <input class="form-control @error('stock') is-invalid @enderror"
                                    wire:model.defer="stock" type="number">
                                @error('stock')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">{{ ___('Tipo de Usuario') }}:</label>
                                <input class="form-control @error('type_user') is-invalid @enderror"
                                    wire:model.defer="type_user" type="number">
                                @error('type_user')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="mb-0">{{ ___('Informacion Paquete') }}:</label>
                                <div class="col-12">
                                    <div class="row d-flex align-items-center justify-content-center">
                                        <div class="col-3">
                                            <input
                                                class="form-control form-control-xs @error('porcentaje_info') is-invalid @enderror"
                                                wire:model.defer="porcentaje_info">
                                        </div>
                                        <span class="col-4"
                                            style="font-size: 9px; margin-right: -15px; margin-left: -15px;">
                                            %{{ ___('mensual') }} X
                                        </span>
                                        <div class="col-4">
                                            <input
                                                class="form-control form-control-xs @error('meses_info') is-invalid @enderror"
                                                wire:model.defer="meses_info">
                                        </div>
                                        <span class="col-3" style=" font-size: 9px; margin-left: -14px;">
                                            {{ ___('meses') }}
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    @error('porcentaje_info')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                    @error('meses_info')
                                        <span class="invalid-feedback">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mt-2 mb-4">
                                <label class="mb-0">{{ ___('Descripcion (opcional)') }}:</label>
                                <textarea class="form-control @error('staking_info') is-invalid @enderror" wire:model.defer="staking_info"></textarea>
                                @error('staking_info')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-4 text-center">
                                <div style="position: relative;">
                                    <input class="btn btn-sm btn-auto btn-primary input-file foto" id="my-file"
                                        type="file" wire:model="foto" style="position: absolute;">
                                    <label tabindex="0" for="my-file"
                                        class="p-2 rounded-circle d-flex align-items-center justify-content-center text-center btn btn-sm btn-auto btn-primary btn-guardar-foto input-file-trigger image"
                                        style="position: absolute; color: #fff; width: 20%;margin-left: 78%;top: -9%;"><i
                                            class="ri-upload-2-fill p-2"></i></label>
                                    @if ($foto && !isset($paquete->foto))
                                        <img class="rounded me-2" alt="200x200" width="200"
                                            src="{{ $foto->temporaryUrl() }}" data-holder-rendered="true">
                                    @elseif ($editando && $paquete->foto && !isset($foto))
                                        <img class="rounded me-2" alt="200x200" width="200"
                                            src="{{ asset('imagenes/paquetes/' . $paquete->foto) }}"
                                            data-holder-rendered="true">
                                    @elseif ($editando && $paquete->foto && isset($foto))
                                        <img class="rounded me-2" alt="200x200" width="200"
                                            src="{{ $foto->temporaryUrl() }}" data-holder-rendered="true">
                                    @else
                                        <img class="rounded me-2" alt="200x200" width="200"
                                            src="{{ asset('disruptive/imagenes/nft-image.jpg') }}"
                                            data-holder-rendered="true">
                                    @endif
                                </div>
                                @error('foto')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        @if ($editando)
                            <button wire:click="actualizar" class="btn btn-info">{{ ___('Guardar') }}</button>
                            <button wire:click="cancelarEdicion"
                                class="btn btn-danger">{{ ___('Cancelar') }}</button>
                        @else
                            <button wire:click="guardar" class="btn btn-success">{{ ___('Crear paquete') }}</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ ___('Listado de paquetes') }}
                            <div wire:loading class="spinner-border spinner-border-sm" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h4>


                        <div class="table-responsive mt-3">
                            <table class="table table-bordered mb-0">

                                <thead>
                                    <tr>
                                        <th>Pos</th>
                                        <th>Nombre</th>
                                        <th>Tipo <br><small>(usuarios)</small></th>
                                        <th>Tokens</th>
                                        <th>Estado</th>
                                        <th>Valor(USDT)</th>
                                        <th>Desc.</th>
                                        <th>Stock</th>
                                        <th>Foto</th>
                                        <th><i class="fa fa-setting"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($paquetes as $paquete)
                                        <tr>
                                            <th>#{{ $paquete->posicion }}</th>
                                            <td>{{ $paquete->nombre }}</td>
                                            <td>{{ $paquete->type_user }}</td>
                                            <td>{{ $paquete->tokens }}</td>
                                            @if ($paquete->estado)
                                                <td><a href="javascript:void(0);"
                                                        wire:click="cambiarEstado({{ $paquete->id }})"
                                                        class="badge badge-success text-white"><i
                                                            class="fa fa-check"></i> {{ ___('Activo') }}</a>
                                                </td>
                                            @else
                                                <td><a href="javascript:void(0);"
                                                        wire:click="cambiarEstado({{ $paquete->id }})"
                                                        class="badge badge-warning text-white"><i
                                                            class="fa fa-exclamation"></i>
                                                        {{ ___('Inactivo') }}</a></td>
                                            @endif
                                            @if ($paquete->descuento != 0)
                                                <td><span class="badge badge-info">{{ $paquete->valor_usd }} <i
                                                            class="fa fa-arrow-right"></i>
                                                        {{ $paquete->valor_usd - $paquete->valor_usd * ($paquete->descuento / 100) }}</span>
                                                </td>
                                            @else
                                                <td><span class="badge badge-info">{{ $paquete->valor_usd }}</span>
                                                </td>
                                            @endif

                                            <td>{{ $paquete->descuento }} %</td>
                                            <td>{{ $paquete->stock }}</td>
                                            <td style="display: grid; place-items:center;">
                                                <div class=" rounded-circle"
                                                    style="width: 70px; height: 70px; overflow: hidden;     padding-right: 0px !important;">
                                                    <img class="img-fluid" alt="image-paquete"
                                                        src="{{ $paquete->foto == null ? asset('disruptive/imagenes/nft-image.jpg') : asset('/imagenes/paquetes') . '/' . $paquete->foto }}"
                                                        style="object-fit: cover; width: 100%; height: 100%;" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class=""
                                                    wire:click="editar({{ $paquete->id }})"><i
                                                        class="fa fa-edit text-info"></i></a>
                                                <a href="javascript:void(0);" class="" data-toggle="modal"
                                                    data-target="#modalEliminar{{ $paquete->id }}"><i
                                                        class="fa fa-trash text-danger"></i></a>
                                                <a href="javascript:void(0);" class="" data-toggle="modal"
                                                    data-target="#modalBono{{ $paquete->id }}"><i
                                                        class="fa fa-gift text-secondary"></i></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="modalEliminar{{ $paquete->id }}" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Esta
                                                            seguro de eliminar este paquete?</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary"
                                                            data-dismiss="modal"
                                                            wire:click="eliminar({{ $paquete->id }})">Confirmar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" wire:ignore.self id="modalBono{{ $paquete->id }}"
                                            tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4>{{ ___('Bonos automaticos para paquete:') }}
                                                            <strong>{{ $paquete->nombre }}</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <div class="input-wrap input-wrap-switch">
                                                                <input class="input-switch"
                                                                    {{ $paquete->estado_bono ? 'checked=""' : '' }}
                                                                    wire:change="changeStatus({{ $paquete->id }})"
                                                                    id="status{{ $paquete->id }}" type="checkbox">
                                                                <label for="status{{ $paquete->id }}">
                                                                    <span
                                                                        class="over">{{ ___('Inactivo') }}</span><span>{{ ___('Activo') }}</span>
                                                                </label>
                                                            </div>
                                                            <label
                                                                for="basic-url">{{ ___('Cantidad de dias para entrega de Bono') }}</label>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon3">#
                                                                        Dias</span>
                                                                </div>
                                                                <input type="number"
                                                                    value="{{ $paquete->dias_bono }}"
                                                                    {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                    id="dias{{ $paquete->id }}"
                                                                    class="form-control">
                                                            </div>
                                                            @error('dias_bono')
                                                                <span class="invalid-feedback">
                                                                    {{ $message }}
                                                                </span>
                                                            @enderror
                                                            <label
                                                                for="basic-url">{{ ___('Porcentaje del bono') }}</label>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">%</span>
                                                                </div>
                                                                <input type="number"
                                                                    value="{{ $paquete->porcentaje_bono }}"
                                                                    {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                    id="porcentaje{{ $paquete->id }}" step="any"
                                                                    class="form-control">
                                                            </div>
                                                            @error('porcentaje_bono')
                                                                <span class="invalid-feedback">
                                                                    {{ $message }}
                                                                </span>
                                                            @enderror
                                                            <div class="input-wrap input-wrap-switch">
                                                                <input {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                    class="input-switch" wire:loading.attr="disabled"
                                                                    {{ $paquete->estado_fecha_limite == true ? 'checked=""' : '' }}
                                                                    wire:change="cambiarEstadoDiaVigencia({{ $paquete->id }})"
                                                                    id="statu{{ $paquete->id }}" type="checkbox">
                                                                <label class="col-12 switch-bonus-stag"
                                                                    for="statu{{ $paquete->id }}">
                                                                    <span
                                                                        class="over">{{ ___('Por días desde la compra del paquete') }}</span><span>{{ ___('Por fecha limite') }}</span>
                                                                </label>
                                                            </div>
                                                            @if ($paquete->estado_fecha_limite == true)
                                                                <div class="fecha-limite mt-3">
                                                                    <label
                                                                        for="basic-addon3s">{{ ___('Fecha limite para la entrega de bonos') }}</label>
                                                                    <div class="input-group mb-3">
                                                                        <input type="date"
                                                                            value="{{ $paquete->fecha_limite_bono->format('Y-m-d') }}"
                                                                            {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                            id="fecha{{ $paquete->id }}"
                                                                            class="form-control">
                                                                    </div>
                                                                    @error('fecha_limite_bono')
                                                                        <span class="invalid-feedback">
                                                                            {{ $message }}
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            @else
                                                                <div class="dias-limite mt-3">
                                                                    <label
                                                                        for="basic-url">{{ ___('Días para la entrega de bonos desde la compra del paquete') }}</label>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"
                                                                                id="basic-addon3">#
                                                                                Dias</span>
                                                                        </div>
                                                                        <input type="number"
                                                                            value="{{ $paquete->dias_vigencia }}"
                                                                            {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                            id="diasvigencia{{ $paquete->id }}"
                                                                            class="form-control">
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-success updateBonusPackage"
                                                                {{ $paquete->estado_bono ? '' : 'disabled' }}
                                                                data-id="{{ $paquete->id }}">{{ ___('Guardar') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('footer')
    <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        window.addEventListener('alert', ({
            detail: {
                type,
                message
            }
        }) => {
            Toast.fire({
                icon: type,
                title: message
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('.updateBonusPackage').click(function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var diasvigencia = $('#diasvigencia' + id).val();
                Livewire.emit('updateBonusPackage', $('#porcentaje' + id).val(), $('#dias' + id).val(), $(
                    '#fecha' + id).val(), $('#diasvigencia' + id).val(), id);
            });
            Livewire.on('toast', info => {
                Toast.fire({
                    title: info.mensaje,
                    icon: info.icono
                })
            })
        });
    </script>
@endpush
