<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-sm-9 col-md-12 col-lg-6 col-xs-12 col-12">
                            <h4 class="card-title">Comments and ratings from our customers</h4>
                        </div>
                        <div class="col-sm-3 col-md-12 col-lg-6 col-xs-12 col-12 justify-content-end align-items-end">
                            <div class="col-auto text-center">
                                @switch($listado)
                                    @case(2)
                                        <button class="btn btn-success" wire:click="cambiarListado">
                                            Published
                                        </button>
                                    @break
                                @endswitch
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card content-area ">
                    <div class="card-innr">
                        <table class="table table-responsive-lg">
                            <thead>
                                <tr class="" role="row">
                                    <th class="" rowspan="1" colspan="1" wire:click="ordenar('id')">
                                        <a href="#">
                                            &nbsp Id
                                        </a>
                                        @if ($sortColumn == 'id')
                                            @if ($sortDirection == 'asc')
                                                <i class="fa fa-sort-up"></i>
                                            @else
                                                <i class="fa fa-sort-down"></i>
                                            @endif
                                        @endif
                                    </th>
                                    <th class="" rowspan="1" colspan="1" wire:click="ordenar('idUser')">
                                        <a href="#">
                                            &nbsp Name
                                        </a>
                                        @if ($sortColumn == 'idUser')
                                            @if ($sortDirection == 'asc')
                                                <i class="fa fa-sort-up"></i>
                                            @else
                                                <i class="fa fa-sort-down"></i>
                                            @endif
                                        @endif
                                    </th>
                                    <th class="" rowspan="1" colspan="1">&nbspComment</th>
                                    <th class="" rowspan="1" colspan="1" wire:click="ordenar('puntaje')">
                                        <a href="#">
                                            &nbsp Scores
                                        </a>
                                        @if ($sortColumn == 'puntaje')
                                            @if ($sortDirection == 'asc')
                                                <i class="fa fa-sort-up"></i>
                                            @else
                                                <i class="fa fa-sort-down"></i>
                                            @endif
                                        @endif
                                    </th>
                                    <th class="" rowspan="1" colspan="1" wire:click="ordenar('created_at')">
                                        <a href="#">
                                            &nbsp Created
                                        </a>
                                        @if ($sortColumn == 'created_at')
                                            @if ($sortDirection == 'asc')
                                                <i class="fa fa-sort-up"></i>
                                            @else
                                                <i class="fa fa-sort-down"></i>
                                            @endif
                                        @endif
                                    </th>
                                    <th class="" rowspan="1" colspan="1">
                                        <div class="dt-type-text"> &nbsp Status</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($valorations as $item)
                                    <tr>
                                        <td class="ml-2"> {{ $item->id }}</td>
                                        <td class="ml-2">{{ $item->user->name }}</td>
                                        <td class="ml-2">{{ str_limit($item->comentario, 55) }}</td>
                                        <td class="ml-2">
                                            @for ($i = 1; $i <= 5; $i++)
                                                @if ($i <= $item->puntaje)
                                                    <span class="fa fa-star fa-sm"
                                                        style="color: yellow;text-shadow: 1px 1px black;"></span>
                                                @else
                                                    <span class="fa fa-star fa-sm" style="color: grey;"></span>
                                                @endif
                                            @endfor
                                        </td>
                                        <td class="ml-2">
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d-M-Y') }} </td>
                                        <td class="ml-2">
                                            @if ($item->estado == true)
                                                <div>
                                                    <button type="button"
                                                        wire:click="cambiarEstado({{ $item->id }})"
                                                        wire:loading.attr="disabled"
                                                        class="change btn  btn-sm btn-success ">
                                                        <i id="icon" class="fa fa-thumbs-up"></i> &nbsp
                                                        Published &nbsp;<div wire:loading class="spinner-border" wire:target="cambiarEstado({{$item->id}})"
                                                            style="width: 15px; height:15px;" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                    </button>

                                              


                                                </div>
                                            @else
                                                <div>
                                                    <button type="button"
                                                        wire:click="cambiarEstado({{ $item->id }})"
                                                        wire:loading.attr="disabled"
                                                        class="change btn  btn-sm btn-danger">
                                                        <i id="icon" class=" fa fa-thumbs-down"></i> &nbsp
                                                        Not Published &nbsp;<div wire:loading class="spinner-border" wire:target="cambiarEstado({{$item->id}})"
                                                            style="width: 15px; height:15px;" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                    </button>
                                                </div>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach




                            </tbody>
                        </table>
                        <div class="row justify-content-center align-items-center">
                            <div class="col-auto text-center">
                                {{ $valorations->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('footer')
    <script src="{{ asset('assets/sweetalert.min.js') }}"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        window.addEventListener('alert', ({
            detail: {
                type,
                message
            }
        }) => {
            Toast.fire({
                icon: type,
                title: message
            })
        })
    </script>
@endpush
