<div wire:ignore.self>
    <div class="page-content">
        <div class="container">
            <div class="content-area card">
                <div class="card-innr card-innr-fix2" >
                    <div class="row ">
                        <div class="col-12">
                            <h6 class="card-title m-3">News Historial</h6>
                        </div>
                      
                        <div class="col-6">
                           
                                @switch($estadoActual)
                                    @case('ondate')
                                        <button class="btn btn-success btn-block" wire:click="cambiarListado">
                                            On date
                                        </button>
                                    @break

                                    @case('expired')
                                        <button class="btn btn-danger btn-block" wire:click="cambiarListado">
                                            Out of date
                                        </button>
                                    @break

                                    @case('comming')
                                        <button class="btn btn-info btn-block"  wire:click="cambiarListado">
                                            Comming soon
                                        </button>
                                    @break

                                    @default
                                        <button class="btn btn-primary btn-block" wire:click="cambiarListado">
                                            All (Recent)
                                        </button>
                                @endswitch

                            
                        </div>
                        <div class="col-6">
                            <a class="btn btn-success btn-block" wire:click="resetInputs" href="#" data-toggle="modal"
                                data-target='#Modalcrear'>
                                <i class="fa fa-plus"></i>
                                Add New(s)
                            </a>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            {{ $noticias->links('vendor.pagination.bootstrap-4') }}
                        </div><!-- .gaps -->
                    </div>
                    <br>
                    <div class="row ">
                        @isset($noticias)
                        @foreach ($noticias as $item)
                        @include('admin.includes.news-include')
                        @endforeach
                        @endisset
                    </div>
                </div><!-- .card -->
            </div><!-- .container -->
        </div>
    </div>

    <!-- Modal -->

    <div wire:ignore.self class="modal fade bd-example-modal-lg " id="Modalcrear" tabindex="-1" role="dialog"
        aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                            class="ti ti-close"></em>
                    </a>
                    <div class="">
                        <h3 class="popup-title">Create</h3>
                        <div class="container" align-items="left">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Title</label>
                                        <input class="form-control" type="text" wire:model.lazy="titulo">
                                    </div>
                                    @error('titulo')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Descripcion</label>
                                        <textarea class="form-control" type="text" wire:model.lazy="descripcion"></textarea>
                                    </div>
                                    @error('descripcion')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    <div class="form-group">
                                        <label for="">Image</label>
                                        <br>
                                        <div style="position:relative;
                                                                        display:inline-block;
                                                                        border-radius:8px;
                                                                        border:#ebebeb solid 1px;
                                                                        width:250px;
                                                                        padding: 4px 6px 4px 8px;
                                                                        font: normal 14px Myriad Pro, Verdana, Geneva, sans-serif;
                                                                        color: #7f7f7f;
                                                                        margin-top: 2px;
                                                                        background:white">
                                            <label for="example-text-input" class="form-control-label">
                                                Upload an image &nbsp;<i class="fa fa-image"></i></label>
                                            <br>
                                            <input type="file" style="position:absolute;
                                                                            top:0; left:0;
                                                                            opacity:0;" wire:model="image"
                                                class="file-upload__input" multiple>
                                        </div>
                                        @error('image.*')
                                            <span class="error">{{ $message }}</span>
                                        @enderror
                                        <span wire:loading class="badge badge-pill badge-primary">Updating...</span>
                                        @if ($image)
                                            <div class="row">
                                                @foreach ($image as $imagen)
                                                    <img loading="lazy" class="foto col-4" style="width:20;height:20;"
                                                        src="{{ $imagen->temporaryUrl() }}">
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="input-item input-with-label">
                                                <label for="example-text-input" class="form-control-label">Start date
                                                </label>
                                                <input class="form-control" type="date"
                                                    wire:model.defer="fechaInicio">
                                            </div>
                                            @error('fechaInicio')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-6">
                                            <div class="input-item input-with-label">
                                                <label for="example-text-input" class="form-control-label">End date
                                                </label>
                                                <input class="form-control" type="date" wire:model.defer="fechaFin">
                                            </div>
                                            @error('fechaFin')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .modal-content -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <a href="#" class="btn btn-success close-modal" wire:click.prevent="save">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>
@push('footer')
    <script type="text/javascript">
        window.addEventListener('cerrarModal1', event => {
            $("#Modalcrear").modal('hide');
        })
    </script>
    @include('partials.livewire-sweetalert')
@endpush
