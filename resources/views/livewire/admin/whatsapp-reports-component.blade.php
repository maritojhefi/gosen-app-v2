<div class="row">
    <div class="page-content">
        <div class="container">
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">{{ ___('Registro de whatsapp de Usuarios') }}</h4>
                        <div class="card-opt d-flex justify-content-center align-items-center" style="width: 50%;"  wire:ignore>
                            @foreach ($usersLenguaje as $lang)
                                <div class="form-check mr-5">
                                    <input class="form-check-input" type="checkbox" id="{{ $lang->lang }}"
                                        value="{{ $lang->language->name }}">
                                    <label class="form-check-label" for="{{ $lang->lang }}">
                                        {{ $lang->language->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="card-opt">
                            <div class="btn-group" wire:ignore>
                                <div class="btn-group dropstart">
                                    <button type="button"
                                        class="btn btn-auto btn-success waves-effect waves-light dropdown-toggle-split dropdown-toggle"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu" style="">
                                        @foreach ($usersCountry as $pais)
                                            <a class="dropdown-item" value="{{$pais->countries->name}}"
                                                href="javascript:void(0)">{{ucfirst($pais->countries->name)}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <button type="button" value="all" id="all-users"
                                    class="btn btn-auto btn-success waves-effect waves-light">
                                    <i class="mdi mdi-microsoft-excel"></i>
                                    Excel
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row d-flex justify-content-center align-items-center">
                            <div class="col-8">
                                <div class="app-search d-none d-lg-block">
                                    <div class="position-relative">
                                        <input type="text" class="form-control"
                                            placeholder="{{ ___('Escriba un numero de Telefono/ID/Nombre/Correo Electronico/Pais') }}"
                                            wire:model.debounce.750ms="search">
                                        <span class="ri-search-line"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <span wire:loading class="spinner-border spinner-border-sm mt-1" role="status"
                                    aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">{{ ___('Nombre') }}</th>
                                    <th scope="col">{{ ___('Telefono/Celular') }}</th>
                                    <th scope="col">{{ ___('Correo Electronico') }}</th>
                                    <th scope="col">{{ ___('Idioma') }}</th>
                                    <th scope="col">{{ ___('Pais') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $item)
                                    <tr>
                                        <th scope="row">{{ $item->id }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->mobile }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ ucfirst($item->language->label) }}</td>
                                        <td>{{ ucfirst($item->country) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('footer')
    <script>
        // $(document).ready(function() {
        //     $('#mobile').keyup($.debounce(750, function(e) {
        //         var valido = numeroEsValido($(this).val());
        //         if (valido == true) {
        //             $("#mobile-span").addClass('d-none');
        //             Livewire.emit('sendNumber', ['+' + iti.s.dialCode + numeroLimpio, $(this).val()]);
        //         } else {
        //             Livewire.emit('sendNumber', ['error', $(this).val()]);
        //         }
        //     }));
        // });


        $(document).ready(function() {
            $('.dropdown-menu a').click(function() {
                var valorSeleccionado = $(this).attr('value');
                var selectedValues = [];
                $('input[type=checkbox]:checked').each(function() {
                    selectedValues.push($(this).attr('id'));
                });
                Livewire.emit('excelUsers',   selectedValues, $(this).attr('value') );
            });
            $('#all-users').click(function() {
                var valorSeleccionado = $(this).attr('value');
                var selectedValues = [];
                $('input[type=checkbox]:checked').each(function() {
                    selectedValues.push($(this).attr('id'));
                });
                Livewire.emit('excelUsers',  selectedValues, $(this).attr('value') );
            });
        });
    </script>
@endpush
