@if ($details==true) 
    <div class="card-head d-flex justify-content-between align-items-center">
        <h4 class="card-title mb-0">{{___('Transaction Details')}}</h4>
        <div class="trans-status">
            @if($transaction->status == 'approved' || $transaction->status == '')
            <span class="badge badge-success ucap">{{___('Approved')}}</span>
            @elseif($transaction->status == 'pending')
            <span class="badge badge-warning ucap">{{___('Pending')}}</span>
            @elseif($transaction->status == 'onhold')
            <span class="badge badge-info ucap">{{___('Progress')}}</span>
            @else
            <span class="badge badge-danger ucap">{{___('Rejected')}}</span>
            @endif
        </div>
    </div>

    @if($transaction->tnx_type=='purchase')
    <div class="trans-details">
        <div class="gaps-1x"></div>
        @if($transaction->status == 'approved')
        <p class="lead-lg text-primary"><strong>{{ ___('You have successfully paid this transaction') }}</strong> ({{ ucfirst($transaction->payment_method) }} <small>- {{ gateway_type($transaction->payment_method) }}</small>).</p>
        @endif
        <p>{!! ___('The order no. :orderid was placed on :datetime.', ['orderid' => '<strong class="text-primary">'.$transaction->tnx_id.'</strong>', 'datetime' => _date($transaction->tnx_time)]) !!}</p>
        @if($transaction->checked_time != NUll && ($transaction->status == 'rejected' || $transaction->status == 'canceled'))
        <p class="text-danger fs-14">{!! ___('Sorry! Your order has been :status due to payment.', ['status' => '<strong>'.$transaction->status.'</strong>']) !!}</p>
        @endif
        <div class="gaps-0-5x"></div>
    </div>
    @endif

@endif

<div class="gaps-1x"></div>
<h6 class="card-sub-title">{{ ___('Token Details') }}</h6>
<ul class="data-details-list">
    <li>
        <div class="data-details-head">{{___('User')}}</div>
        <div class="data-details-des">{{ __(ucfirst($transaction->tnxUser->username)) }}</div>
    </li>
    <li>
        <div class="data-details-head">{{___('Approval date')}}</div>
        <div class="data-details-des">{{ __(ucfirst($transaction->tnx_time)) }}</div>
    </li>
    <li>
        <div class="data-details-head">{{___('Transactions')}}</div>
        <div class="data-details-des">{{ __(ucfirst($transaction->tnx_id)) }}</div>
    </li>
    <li>
        <div class="data-details-head">{{___('Types')}}</div>
        <div class="data-details-des">{{ __(ucfirst($transaction->tnx_type)) }}</div>
    </li>
    @if(!empty($transaction->ico_stage))
    <li>
        <div class="data-details-head">{{___('Token of Stage')}}</div>
        <div class="data-details-des"><strong>{{ $transaction->ico_stage->name }}</strong></div>
    </li>
    @endif
    @if($transaction->tnx_type=='purchase'||$transaction->tnx_type=='bonus')
    <li>
        <div class="data-details-head">{{___('Token Amount (T)')}}</div>
        <div class="data-details-des">
            <span> {{ to_num($transaction->tokens, 'zero', '', false) }} {{ token_symbol() }}</span>
        </div>
    </li>
    @endif
    @if($transaction->tnx_type=='withdraw')
    <li>
        <div class="data-details-head">{{___('Token Amount (T)')}}</div>
        <div class="data-details-des">
            <span style="color:red;"> - {{ to_num($transaction->tokens, 'zero', '', false) }} {{ token_symbol() }}</span>
        </div>
    </li>
    @endif
    @if($transaction->tnx_type=='purchase')
    <li>
        <div class="data-details-head">{{___('Bonus Token (B)')}}</div>
        <div class="data-details-des">
            <span>{{ to_num($transaction->total_bonus, 'zero', '', false) }} {{ token_symbol() }}</span>
            <span>({{ to_num($transaction->bonus_on_token) }} + {{ to_num($transaction->bonus_on_base) }})</span>
        </div>
    </li>
    <li>
        <div class="data-details-head">{{___('Total Token')}}</div>
        <div class="data-details-des">
            <span><strong>{{ to_num($transaction->total_tokens, 'zero', '', false) }} {{ token_symbol() }}</strong></span>
            <span>(Purchase + B)</span>
        </div>
    </li>
    <li>
        <div class="data-details-head">{{___('Total Payment')}}</div>
        <div class="data-details-des">
            <span><strong>{{ to_num($transaction->receive_amount, 'max') }} {{ strtoupper($transaction->receive_currency) }}</strong></span>
            <span><em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="1 {{ token('symbol') }} = {{ to_num($transaction->base_currency_rate, 'max').' '.strtoupper($transaction->base_currency) }}"></em> {{ to_num($transaction->base_amount, 'auto') }} {{ strtoupper($transaction->base_currency) }}</span>
        </div>
    </li>
    <li>
        <div class="data-details-head">{{___('Details')}}</div>
        <div class="data-details-des">{{ __(ucfirst($transaction->details)) }}</div>
    </li>
    @endif
    @if($transaction->tnx_type=='refund')
    <li>
        <div class="data-details-head">{{___('Refunded Token')}}</div>
        <div class="data-details-des">
            <span><strong class="text-danger" style="color:red" >{{ '-'.to_num(abs($transaction->total_tokens), 'max') }} {{ token_symbol() }}</strong></span>
        </div>
    </li>
    <li>
        <div class="data-details-head">{{___('Refunded Amount')}}</div>
        <div class="data-details-des">
            <span><strong class="text-danger" style="color:red">{{ '-'.to_num(abs($transaction->receive_amount), 'max') }} {{ strtoupper($transaction->receive_currency) }}</strong></span>
            <span><em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="1 {{ token('symbol') }} = {{ to_num($transaction->base_currency_rate, 'max').' '.strtoupper($transaction->base_currency) }}"></em> {{ to_num(abs($transaction->base_amount), 'max') }} {{ base_currency(true) }}</span>
        </div>
    </li>
    @endif
    @if($transaction->tnx_type=='transfer')
    <li>
        <div class="data-details-head">{{ ($transaction->extra=='sent') ? ___('Token Send To') : ___('Token Receive From') }}</div>
        <div class="data-details-des">
            <span>{{ $transaction->payment_to }}
        </div>
    </li>
    @endif
    @if($transaction->details && ($transaction->tnx_type!='purchase')&& ($transaction->tnx_type!='bonus')) 
    <li>
        <div class="data-details-head">{{ ($transaction->tnx_type=='refund' || $transaction->tnx_type=='transfer') ? ___('Notes') : ___('Details') }}</div>
        <div class="data-details-des">
            <span>{{ __($transaction->details) }}
        </div>
    </li>
    @endif

    @if($transaction->details && $transaction->tnx_type == 'bonus')
    <li>
        <div class="data-details-head">{{ ($transaction->tnx_type=='refund' || $transaction->tnx_type=='transfer') ? ___('Notes') : ___('Details') }}</div>
        <div class="data-details-des">
            <span>{{__('Purchase Bonus ')}}Tokens
        </div>
    </li>
    @endif 

    @if($transaction->tnx_type=='referral')
    <li>
        <div class="data-details-head">{{___('Referral Bonus For')}}</div>
        <div class="data-details-des">
            @php 
            $referral = (get_meta($transaction->extra, 'who')) ? $transaction->user(get_meta($transaction->extra, 'who')) : '';
            @endphp
            <span>{{ $referral->name }}
        </div>
    </li>
    @endif
    @php 
        $trnx_extra = (is_json($transaction->extra, true) ?? $transaction->extra);
    @endphp
    @if(!empty($trnx_extra->message))
    <li>
        <div class="data-details-head">{{___('Refund Note')}}</div>
        <div class="data-details-des">
            <span>{{ $trnx_extra->message }}</span>
        </div>
    </li>
    @endif
@isset($transaction->wallet_address)
    <li>
        <div class="data-details-head">Wallet</div>
        <div class="data-details-des">
        <span>{{ $transaction->wallet_address }}
        </div>
    </li>
    
    @endisset

</ul>

@if ($transaction->tnx_type=='purchase' && $transaction->status == 'approved') 
<span class="lead-lg text-secondary font-italic"><i class="fa fa-exclamation font-italic"></i>{{ ___(' Remember that you can request a refund of your transaction in the next 72 hours from when your transaction was approved, after this time you will not be able to request said refund') }}</span>
@endif
<p class="text-primary fs-12 pt-3 text-right">
<a href="{{ route('pdf-transaction-details',$transaction->id) }}" class="btn btn-sm btn-auto btn-primary text-right" target="_blank">
    <em class="fa fa-file-pdf"></em><span class="d-none d-sm-inline-block text-white">PDF</span>
</a>
</p>
{{-- @if($transaction->checked_time != NUll && $transaction->status == 'approved')
    <p class="text-primary fs-12 pt-3"><em>{!! ___('Transaction has been approved at :time.', ['time'=>_date($transaction->checked_time)])  !!}</em></p>
@endif
@if($transaction->status == 'pending')
    <p class="text-primary fs-12 pt-3">{{ ___('The transaction is currently under review. We will send you an email once our review is complete.') }}</p>
@elseif($transaction->status == 'rejected' || $transaction->status == 'canceled')
    @if($transaction->tnx_type=='purchase')
        @if($transaction->checked_time != NUll)
            <p class="text-danger fs-12 pt-3">{!! ___('The transaction was canceled by Administrator at :time.', ['time'=>_date($transaction->checked_time)])  !!}</p>
        @elseif($transaction->status == 'canceled')
            <p class="text-danger fs-13 pt-3"><em>{{ ___('You have canceled this transaction.') }}</em></p>
        @endif
    @elseif($transaction->tnx_type=='transfer')
        <p class="text-danger fs-13 pt-3">{!! ___('The transfer request was canceled at :time.', ['time'=>_date($transaction->checked_time)])  !!}</p>
    @endif
@endif --}}