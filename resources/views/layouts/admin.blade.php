<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" content="notranslate" translate="no">

<head>
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">
    <link preload rel="shortcut icon" href="{{ asset2('favicon.png') }}">
    <title>@yield('title') | Gosen</title>
    <link preload href="{{ asset('assets/css/vendorapp.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link preload href="{{ asset('assets/app/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- Bootstrap Css id="bootstrap-style" -->
    <link preload href="{{ asset('assets/app/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link preload href="{{ asset('assets/app/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css  id="app-style" -->
    <link preload href="{{ asset('assets/app/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- image crop -->
    <link preload href="{{ asset('assets/app/libs/ijaboCropTool/ijaboCropTool.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link preload href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@600&display=swap"
        rel="stylesheet">
    <script>
        window.PUSHER_APP_KEY = '{{ config('broadcasting.connections.pusher.key') }}';
        window.APP_DEBUG = {{ config('app.debug') ? 'true' : 'false' }};
    </script>
    <script src="{{ asset('assets/websockets.js') }}"></script>
    @include('layouts.color-theme')
    @yield('gosencss')
    @include('layouts.include-css-admin')
    @stack('header')
    @if (get_setting('site_header_code', false))
        {{ html_string(get_setting('site_header_code')) }}
    @endif
    @livewireStyles
    @livewireScripts
</head>

<body data-sidebar="dark" class="no-touch {{ session('cambiarSidebar') == 'hide' ? ' vertical-collpsed' : '' }}">
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="{{ route('admin.home') }}" class="logo logo-light">
                            <span class="logo-sm text-rotate">
                                <div class="row">
                                    <div class="col pl-4 pr-0" style="top: 35px">
                                        <span class="span-text">
                                            <img src="{{ asset('images/token-symbol-light.png') }}"
                                                alt="{{ site_whitelabel('name') }}" height="22">
                                        </span>
                                    </div>
                                </div>
                            </span>
                            <span class="logo-lg text-rotate">
                                <div class="row">
                                    <div class="col-sm-3 pl-3 pr-0">
                                        <span class="span-text"><img src="{{ asset('images/token-symbol-light.png') }}"
                                                height="40"></span>
                                    </div>
                                    <div class="col-sm-9 mt-0 p-1" translate="no">
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif;">g</span>
                                        <span class="span-text mr-1">o</span>
                                        <span class="span-text mr-1">s</span>
                                        <span class="span-text mr-1">e</span>
                                        <span class="span-text mr-1">n</span>
                                    </div>
                                </div>
                            </span>
                        </a>
                    </div>
                    <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect"
                        id="vertical-menu-btn" onclick="loadDoc()">
                        <i class="ri-menu-2-line align-middle"></i>
                    </button>
                </div>
                <div class="d-flex">
                    {!! UserPanel::language_switcher_usuario() !!}
                    <div class="dropdown d-none d-lg-inline-block ms-1">
                        <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                            <i class="ri-fullscreen-line"></i>
                        </button>
                    </div>
                    <div class="dropdown d-inline-block user-dropdown">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if (auth()->user()->foto == null || auth()->user()->foto == '')
                                <img loading="lazy" src="{{ asset2('imagenes/user-default.jpg') }}"
                                    class="rounded-circle header-profile-user admin_picture">
                            @else
                                <img loading="lazy" class="rounded-circle header-profile-user admin_picture"
                                    src="{{ asset2('imagenes/perfil/') }}/{{ auth()->user()->foto }}">
                            @endif
                            <span
                                class="d-none d-xl-inline-block ms-1">{{ auth()->user()->username == null ? auth()->user()->name : auth()->user()->username }}</span>
                            <i class="mdi mdi-chevron-down d-xl-inline-block"></i><!-- d-none -->
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" style="border-radius:20px;">
                            <!-- item-->
                            <a class="dropdown-item" href="{{ route('admin.profile') }}"
                                style="border-radius:20px;"><i class="ri-user-line align-middle me-1"></i>
                                {{ ___('My Profile') }}</a>
                            @if (get_page('referral', 'status') == 'active' && is_active_referral_system())
                                <a class="dropdown-item d-block" href="#" style="border-radius:20px;"><i
                                        class="ri-team-line align-middle me-1"></i>
                                    {{ get_page('referral', 'menu_title') }}</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('admin.profile.activity') }}"
                                style="border-radius:20px;"><i class="ri-eye-line align-middle me-1"></i>
                                {{ ___('Activity') }}</a>
                            @if (is_super_admin())
                                <a class="dropdown-item" id="clear-cache" style="border-radius:20px;"
                                    href="{{ route('admin.clear.cache') }}"><em
                                        class="ti ti-trash align-middle me-1"></em><span>Clear Cache</span></a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger" href="{{ route('log-out') }}"
                                style="border-radius:20px;"
                                onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();">
                                <i class="ri-shut-down-line align-middle me-1 text-danger"></i> {{ ___('Logout') }}
                            </a>
                            <form id="logout-form" action=" {{ route('logout') }}" method="POST"
                                style="display: none;"> <input type="hidden" name="_token"
                                    value="{{ csrf_token() }}"></form>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- ========== Left Sidebar Start ========== -->
        <div class=" vertical-menu">
            <div data-simplebar class="h-100">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    @if (gup('translator') && !is_super_admin(auth()->user()->id, true))
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">Menu</li>
                            <li>
                                <a href="{{ route('admin.diccionario') }}"
                                    class="waves-effect {!! is_page('users') || is_page('users.user') || is_page('users.admin') ? 'active' : '' !!}">
                                    <i class="fa fa-building"></i>
                                    <span>{{ ___('Diccionario') }}</span>
                                </a>
                            </li>
                        </ul>
                    @else
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">Menu</li>
                            <li>
                                <a href="{{ route('admin.home') }}" class="waves-effect">
                                    <i class="ikon ikon-dashboard"></i>
                                    <span>{{ ___('Dashboard') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    {{-- <i class="ri-questionnaire-line"></i> --}}
                                    <i class="fa fa-envelope-open-text" style="font-size: 13px;"></i>
                                    <span>{{ ___('Mail Creator') }}</span>
                                </a>
                                <ul class="styledrop-faq sub-menu" aria-expanded="false">
                                    <li><a href="{{ route('admin.send.massive.mail') }}">Create and Send</a></li>
                                    <li><a href="{{ route('admin.history.mail.sent') }}">Sent History</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    {{-- <i class="ri-questionnaire-line"></i> --}}
                                    <i class="fab fa-whatsapp" style="font-size: 13px;"></i>
                                    <span>{{ ___('Whatsapp') }}</span>
                                </a>
                                <ul class="styledrop-faq sub-menu" aria-expanded="false">
                                    <li>
                                        <a
                                            href="{{ route('admin.whatsapp.user.reports') }}">{{ ___('Contacto de Usuarios') }}</a>
                                    </li>
                                    <li>
                                        <a
                                            href="{{ route('admin.conversations.whatsapp') }}">{{ ___('Conversaciones') }}</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    {{-- <i class="ri-questionnaire-line"></i> --}}
                                    <i class="fab fa-whatsapp" style="font-size: 13px;"></i>+<i
                                        class="ti ti-email ml-3" style="font-size: 13px;"></i>
                                    <span>{{ ___('Notificaciones') }}</span>
                                </a>
                                <ul class="styledrop-faq sub-menu" aria-expanded="false">
                                    <li><a
                                            href="{{ route('admin.mail.whatsapp.user') }}">{{ ___('Create Mails') }}</a>
                                    </li>
                                    <li><a
                                            href="{{ route('admin.mail.whatsapp.scheduled') }}">{{ ___('Scheduled Emails') }}</a>
                                    </li>
                                </ul>
                            </li>
                            @if (gup('setting'))
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                                        <i class="ikon ikon-settings"></i>
                                        <span>{{ ___('Settings') }}</span>
                                    </a>
                                    <ul class="sub-menu styledrop-set" aria-expanded="false">
                                        <li><a href="{{ route('admin.stages.settings') }}">ICO/STO Setting</a></li>
                                        <li><a href="{{ route('admin.settings') }}">Website Setting</a></li>
                                        <li><a href="{{ route('admin.packages') }}">Manage Packages</a></li>
                                        <li><a href="{{ route('admin.bonus.stagging.list') }}">Bonus Staking
                                                Transactions</a></li>
                                        <li><a href="{{ route('admin.settings.referral') }}">Referral Setting</a></li>
                                        <li><a href="{{ route('admin.settings.email') }}">Mailing Setting</a></li>
                                        <li><a href="{{ route('admin.payments.setup') }}">Payment Methods</a></li>
                                        <li><a href="{{ route('admin.pages') }}">Manage Pages</a></li>
                                        <li><a href="{{ route('admin.settings.api') }}">Application API</a></li>
                                        <li><a href="{{ route('admin.lang.manage') }}">Manage Languages</a></li>
                                        <li><a href="{{ route('admin.system') }}">System Status</a></li>
                                        @if (has_route('manage_access:admin.index'))
                                            <li><a href="{{ route('manage_access:admin.index') }}">Manage Admin</a>
                                            </li>
                                        @endif
                                        <li><a href="{{ route('admin.regalos.settings') }}">Gosen Tokens Bonus</a>
                                        </li>
                                        <li><a href="{{ route('admin.withdraw.settings') }}">Manage Withdraw</a></li>
                                        <li><a href="{{ route('admin.disruptive.settings') }}">Disruptive Settings</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            @if (gup('tranx') || gup('view_tranx') || gup('view_all'))
                                <li>
                                    <a href="{{ route('admin.transactions', 'pending') }}" class="waves-effect">
                                        <i class="ikon ikon-transactions"></i>
                                        <span>{{ ___('Transactions') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if (gup('manage_bonusdollar') || gup('view_all'))
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                                        <i class="fa fa-dollar-sign" style="font-size: 15px;"></i>
                                        <span>{{ ___('Bonus Dolars') }}</span>
                                    </a>
                                    <ul class="styledrop-busd sub-menu" aria-expanded="false">
                                        <li>
                                            <a href="{{ route('admin.bonus', 1) }}" class="waves-effect">
                                                <i class="ri-file-list-3-line" style="font-size: 15px;"></i>
                                                <span>{{ ___('Records') }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.withdraw', 2) }}" class="waves-effect">
                                                <i class="ri-money-dollar-circle-line" style="font-size: 15px;"></i>
                                                <span>{{ ___('Withdrawals') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            @if (gup('withdraw') || gup('view_all'))
                                <li>
                                    <a href="{{ route('withdraw:admin.index') }}" class="waves-effect">
                                        <i class="ikon ikon-wallet"></i>
                                        <span>{{ ___('Withdraw') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if (gup('kyc') || gup('view_kyc') || gup('view_all'))
                                <li>
                                    <a href="{{ route('admin.kycs', 'pending') }}" class="waves-effect">
                                        <i class="ikon ikon-docs"></i>
                                        <span>{{ ___('KYC List') }}</span>
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class=" ri-account-circle-line"></i>
                                    <span>{{ ___('Users') }}</span>
                                </a>
                                <ul class="styledrop-user sub-menu" aria-expanded="false">
                                    @if (gup('user') || gup('view_user') || gup('view_all'))
                                        <li {!! is_page('users') || is_page('users.user') || is_page('users.admin') ? ' class="active"' : '' !!}>
                                            <a href="{{ route('admin.users', 'user') }}"><em class=""></em>
                                                {{ ___('Users List') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.user-create') }}">
                                                {{ ___('Create User') }}
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{ route('admin.mails-blocked') }}">
                                            {{ ___('Blocked Mails') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @if (gup('stage') || gup('view_all'))
                                <li>
                                    <a href="{{ route('admin.stages') }}" class="waves-effect">
                                        <i class="ikon ikon-coins"></i>
                                        <span>{{ ___('ICO/STO Stage') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if (gup('translator'))
                                <li>
                                    <a href="{{ route('admin.diccionario') }}"
                                        class="waves-effect {!! is_page('users') || is_page('users.user') || is_page('users.admin') ? 'active' : '' !!}">
                                        <i class="fa fa-building"></i>
                                        <span>{{ ___('Diccionario') }}</span>
                                    </a>
                                </li>
                            @endif
                            <li>
                                {{-- <a href="{{ route('admin.analytics') }}" class="waves-effect">
                                <i class="mdi mdi-chart-line"></i>
                                <span>{{ ___('Analytics') }}</span>
                            </a> --}}
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="mdi mdi-chart-line"></i>
                                    <span>{{ ___('Analytics') }}</span>
                                </a>
                                <ul class="styledrop-faq sub-menu" aria-expanded="false">
                                    <li><a href="{{ route('admin.analytics') }}">{{ ___('Registros') }}</a></li>
                                    {{-- <li><a href="#">{{ ___('Tipos de usuarios') }}</a></li> --}}
                                    <li><a href="{{ route('admin.package') }}">{{ ___('Venta de paquetes') }}</a>
                                    </li>
                                    <li><a href="{{ route('admin.ingresos') }}">{{ ___('Ingresos') }}</a></li>
                                    <li><a href="{{ route('admin.tipoPagos') }}">{{ ___('Tipos de pagos') }}</a></li>
                                    <li><a href="{{ route('admin.bonusUsd') }}">{{ ___('Bonus USD') }}</a></li>
                                    <li><a href="#">{{ ___('SuperAds') }}</a></li>
                                </ul>
                            </li>
                            <li class="menu-title">More</li>
                            <li>
                                <a href="{{ route('admin.valorations') }}" class="waves-effect">
                                    <i class="fa fa-comments"></i>
                                    <span>{{ ___('Comments') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.diccionario') }}" class="waves-effect">
                                    <i class="fa fa-check"></i>
                                    <span>{{ ___('Diccionario') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="ri-questionnaire-line"></i>
                                    <span>{{ ___('FAQ') }}</span>
                                </a>
                                <ul class="styledrop-faq sub-menu" aria-expanded="false">
                                    <li><a href="{{ route('admin.frequentquestions.index') }}">All</a></li>
                                    <li><a href="{{ route('admin.frequentquestions.create') }}">Create Question</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ route('admin.news') }}" class="waves-effect">
                                    <i class="fa fa-newspaper"></i>
                                    <span>{{ ___('News') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.eventos.index') }}" class="waves-effect">
                                    <i class="fa fa-calendar-check"></i>
                                    <span>{{ ___('Events') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.top-referrals') }}" class="waves-effect">
                                    <i class="fa fa-trophy"></i>
                                    <span>{{ ___('Top Referrals') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.statusServices') }}" target="_blank" class="waves-effect">
                                    <i class="fas fa-chart-line"></i>
                                    <span>{{ ___('Estado de los servicios') }}</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                    <!-- Left Menu Start -->
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <div class="main-content mt-3">
            <div class="page-content" style="padding: 10px">
                <div class="container-fluid">
                    <div class="row">
                        @hasSection('content')
                            @yield('content')
                            @push('footer')
                                <script src="https://code.jquery.com/jquery-3.6.3.min.js"
                                    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
                            @endpush
                        @else
                            @yield('contenttrans')
                        @endif
                    </div>
                </div> <!-- container-fluid -->
            </div>
        </div>
    </div>
    <div id="ajax-modal"></div>
    @yield('modals')
    @stack('modals')
    <div class="page-overlay">
        <div class="spinner"><span class="sp sp1"></span>
            <span class="sp sp2"></span><span class="sp sp3"></span>
        </div>
    </div>
    <x-evento-vista-component />
    @php
        $admin_routes = '';
        $route_urls = [
            'get_trnx_url' => 'admin.ajax.transactions.view',
            'view_user_url' => 'admin.ajax.users.view',
            'show_user_info' => 'admin.ajax.users.show',
            'pm_manage_url' => 'admin.ajax.payments.view',
            'get_kyc_url' => 'admin.ajax.kyc.ajax_show',
            'update_kyc_url' => 'admin.ajax.kyc.update',
            'trnx_action_url' => 'admin.ajax.transactions.update',
            'trnx_adjust_url' => 'admin.ajax.transactions.adjustement',
            'get_et_url' => 'admin.ajax.settings.email.template.view',
            'clear_cache_url' => 'admin.clear.cache',
            'whitepaper_uploads' => 'admin.ajax.pages.upload',
            'view_page_url' => 'admin.ajax.pages.view',
            'unverified_delete_url' => 'admin.ajax.users.delete',
            'stage_action_url' => 'admin.ajax.stages.actions',
            'stage_active_url' => 'admin.ajax.stages.active',
            'stage_pause_url' => 'admin.ajax.stages.pause',
            'quick_update_url' => 'admin.ajax.payments.qupdate',
            'transfer_action_url' => 'transfer:admin.update',
            'meta_update_url' => 'admin.ajax.settings.meta.update',
        ];
        foreach ($route_urls as $var => $route) {
            $admin_routes .= has_route($route) ? $var . ' = "' . route($route) . '", ' : '';
        }
    @endphp
    <script type="text/javascript">
        var base_url = "{{ url('/') }}",
            {!! $admin_routes !!} csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            sts = "{{ ___('Type in to Search') }}",
            snr = "{{ ___('No records') }}",
            sfs = "{{ ___('First') }}",
            sls = "{{ ___('Last') }}",
            snx = "{{ ___('Next') }}",
            spv = "{{ ___('Prev') }}",
            stl = "{{ ___('Total') }}";
    </script>
    {{-- <script src="{{ asset('assets/app/libs/jquery/jquery.min.js') }}"></script> --}}
    <script src="{{ asset('assets/js/jquery.bundle.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/script.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/admin.app.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/gosen.app.js') }}"></script>
    <script src="{{ asset('assets/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/app/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/node-waves/waves.min.js') }}"></script>
    <!-- Plugins js -->
    <script src="{{ asset('assets/app/libs/dropzone/min/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/app/js/app.js') }}"></script>
    <script src="{{ asset('assets/app/js/user.js') }}"></script>
    <!-- image crop -->
    <script src="{{ asset('assets/app/libs/ijaboCropTool/ijaboCropTool.min.js') }}"></script>
    @env('production')
    <script>
        function loadDoc() {
            const xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                document.getElementById("app").innerHTML = this.responseText;
            }
            @production
            xhttp.open("GET", "/admin/cambiarSidebar");
        @endproduction
        @env('local')
            xhttp.open("GET", "/tokenlite/admin/cambiarSidebar");
        @endenv
        xhttp.send();
        }
    </script>
    @endenv
    @env('local')
    <script>
        function loadDoc() {
            const xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                document.getElementById("app").innerHTML = this.responseText;
            }
            xhttp.open("GET", "/tokenlite/admin/cambiarSidebar");
            xhttp.send();
        }
    </script>
    @endenv
    <!--/ JAVASCRIPT -->
    @stack('footer')
    @if (session()->has('global'))
        <script type="text/javascript">
            show_toast("info", "{{ session('global') }}");
        </script>
    @endif
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(window).on('load', function() {
            $('#loading').hide();
        })
        $(document).ready(function() {
            $("a").click(function() {
                if ($(this).prop('target').includes('blank') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('excel') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('export') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('http') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 4000)
                }
            });
        });
    </script>
    <script>
        Livewire.on('toastDispatch', data => {
            console.log('toast')
            Toast.fire({
                icon: data.icon,
                title: data.title,
                text: data.body,
            });
        });
        $(document).on("load", function() {
            console.log('asd')
        });
    </script>
</body>

</html>
