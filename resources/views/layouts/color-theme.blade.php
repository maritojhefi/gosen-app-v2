@if (theme_color() != '#7668fe')
    <link preload rel="stylesheet" href="{{ asset(style_theme('admin')) }}">
    <style>
        body[data-sidebar=dark] .navbar-brand-box {
            background: {{ asset(theme_color()) }};
        }

        body[data-sidebar=dark] .vertical-menu {
            background: {{ asset(theme_color()) }};
        }

        body[data-sidebar=dark] .menu-title {
            color: #d7e4ec;
        }

        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid {{ asset(theme_color()) }};
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        #loading {
            position: fixed;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            opacity: 0.7;
            background-color: rgba(255, 255, 255, 0.767);
            z-index: 99;
        }

        #loading-image {
            z-index: 100;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        body[data-sidebar=dark] #sidebar-menu ul li a {
            color: #d7e4ec;
        }

        body[data-sidebar=dark] #sidebar-menu a:hover:not(.active) {
            background-color: {{ asset(theme_color('sidebar')) }};
            color: white;
        }

        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu>ul>li:hover>a {
            background: {{ asset(theme_color('sidebar')) }};
        }

        body[data-sidebar=dark] #sidebar-menu ul li a i {
            color: #d7e4ec;
        }

        body[data-sidebar=dark] #sidebar-menu ul li ul.sub-menu li a {
            color: #d7e4ec;
        }

        body[data-sidebar=dark] .mm-active .active {
            background-color: {{ asset(theme_color('sidebar')) }};
            color: white;
        }

        body[data-sidebar=dark] #vertical-menu-btn {
            color: {{ asset(theme_color('sidebar')) }};
            border-color: transparent;
        }

        body[data-sidebar=dark] #page-header-user-dropdown {
            color: {{ asset(theme_color('sidebar')) }};
            border-color: #fff;
        }

        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu ul li li a.active,
        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu ul li li a.mm-active {
            color: #d7e4ec !important;
        }

        .btn {
            min-width: 0px;
        }

        .btn-light {
            color: black;
        }

        .vertical-collpsed .vertical-menu {
            position: fixed;
        }

        @media (max-width: 420px) and (min-width: 375px) {
            .phase-block {
                display: block;
                align-items: flex-start;
            }
        }
    </style>
@else
    <link preload rel="stylesheet" href="{{ asset('assets/css/style-app.css') }}">
    <style>
        .page-ath-gfx {
            background: #101a2d;
            background-size: cover;
            background-position: 50% 50%;
        }

        @media (max-width: 420px) and (min-width: 375px) {
            .phase-block {
                display: block;
                align-items: flex-start;
            }
        }

        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid #1f355e;
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        #loading {
            position: fixed;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            opacity: 0.7;
            background-color: rgba(255, 255, 255, 0.767);
            z-index: 99;
        }

        #loading-image {
            z-index: 100;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endif
