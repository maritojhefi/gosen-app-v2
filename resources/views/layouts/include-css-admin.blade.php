<style>
    .main-content {
        transition: all 350ms cubic-bezier(0.6, 0.05, 0.28, 0.91);
    }

    .navbar-brand-box {
        transition: all 350ms cubic-bezier(0.6, 0.05, 0.28, 0.91);
        height: 70px;
    }

    .vertical-menu {
        transition: all 350ms cubic-bezier(0.6, 0.05, 0.28, 0.91);
    }

    .text-rotate {
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .text-rotate .span-text {
        display: inline-block;
        font-family: "Montserrat", sans-serif;
        text-transform: uppercase;
        font-size: 20px;
        font-weight: 700;
        color: #fff;
    }

    .text-rotate:hover .span-text {
        animation: text-rotate 2s ease forwards 1;
    }

    .text-rotate:hover .span-text:nth-child(2) {
        animation-delay: 0.1s;
    }

    .text-rotate:hover .span-text:nth-child(3) {
        animation-delay: 0.2s;
    }

    .text-rotate:hover .span-text:nth-child(4) {
        animation-delay: 0.3s;
    }

    .text-rotate:hover .span-text:nth-child(5) {
        animation-delay: 0.4s;
    }

    .text-rotate:hover .span-text:nth-child(6) {
        animation-delay: 0.5s;
    }

    .text-rotate:hover .span-text:nth-child(7) {
        animation-delay: 0.6s;
    }

    @keyframes text-rotate {
        from {
            transform: rotateY(0deg);
        }

        to {
            transform: rotateY(360deg);
        }
    }

    .card {
        border-radius: 20px;
    }

    .card-bordered {
        border-radius: 20px;
    }

    .stage-card {
        border-radius: 20px;
    }

    .modal-content {
        border-radius: 20px;
    }

    @media (max-width: 991px) {
        .navbar-brand-box {
            border-bottom-left-radius: 20px;
        }

        body.sidebar-enable .navbar-brand-box {
            border-bottom-left-radius: 0px;
            transition: none;
        }

        #page-topbar {
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        body.sidebar-enable #page-topbar {
            border-bottom-left-radius: 0px;
        }
    }

    @media (max-width: 992px) {
        body.sidebar-enable .vertical-menu {
            border-top-right-radius: 20px;
            border-bottom-right-radius: 20px;
        }


    }

    body.vertical-collpsed .styledrop-set {
        position: fixed !important;
        right: calc(5% + 100%) !important;
        top: 135px;
    }

    body.vertical-collpsed .styledrop-user {
        position: fixed !important;
        right: calc(5% + 100%) !important;
        top: 22.2rem;
        ;
    }

    body.vertical-collpsed .styledrop-faq {
        position: fixed !important;
        right: calc(5% + 100%) !important;
        top: 32.5rem;
    }
</style>
