<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" content="notranslate" translate="no">

<head>
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">
    <link preload rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <title>@yield('title') | {{ ucfirst(env('APP_NAME')) }} </title>

    <link preload rel="stylesheet" href="{{ asset(style_theme('vendor')) }}">
    <link preload rel="stylesheet" href="{{ asset(style_theme('user')) }}">

    <!-- Bootstrap Css id="bootstrap-style" -->
    <link preload href="{{ asset('assets/app/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link preload href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet">
    <link preload href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@600&display=swap"
        rel="stylesheet">
    @yield('gosencss')
    {{-- @include('layouts.color-theme') --}}
    <style>
        .page-ath-gfx {
            background: #101a2d;
            background-size: cover;
            background-position: 50% 50%;
        }

        .btn-primary {
            color: #fff;
            background-color: #35496a !important;
            border-color: #606069 !important;
        }

        .content {
            width: 308px;
            font-size: 36px;
            line-height: 40px;
            font-family: "Roboto", sans-serif;
            color: #ecf0f1;
            height: 40px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -15px;
            margin-left: -118px;
        }

        .content:before {
            content: '[';
            position: absolute;
            left: -30px;
            line-height: 40px;
        }

        .content:after {
            content: ']';
            position: absolute;
            right: -75px;
            line-height: 40px;
        }

        .content:after,
        .content:before {
            color: #be90d4;
            font-size: 42px;
            animation: 2s linear 0s normal none infinite opacity;
            -webkit-animation: 2s ease-out 0s normal none infinite opacity;
            -moz-animation: 2s ease-out 0s normal none infinite opacity;
            -o-animation: 2s ease-out 0s normal none infinite opacity;
        }

        .visible {
            float: left;
            font-weight: 600;
            overflow: hidden;
            height: 40px;
            width: 400px;
        }

        .p-text {
            display: inline;
            float: left;
            margin: 0;
        }

        .ul-text {
            margin-top: 0;
            padding-left: 124px;
            text-align: left;
            list-style: none;
            animation: 6s linear 0s normal none infinite change;
            -webkit-animation: 6s linear 0s normal none infinite change;
            -moz-animation: 6s linear 0s normal none infinite change;
            -o-animation: 6s linear 0s normal none infinite change;
        }

        .ul-text .li-text {
            line-height: 40px;
            margin: 0;
        }

        @keyframes opacity {
            0% {
                opacity: 0;
            }

            50% {
                opacity: 1;
            }

            100% {
                opacity: 0;
            }
        }

        @keyframes change {
            0% {
                margin-top: 0;
            }

            15% {
                margin-top: 0;
            }

            25% {
                margin-top: -40px;
            }

            40% {
                margin-top: -40px;
            }

            50% {
                margin-top: -80px;
            }

            65% {
                margin-top: -80px;
            }

            75% {
                margin-top: -40px;
            }

            85% {
                margin-top: -40px;
            }

            100% {
                margin-top: 0;
            }
        }
    </style>
    @if (strpos(Request::url(), env('TENANCY_DOMAIN')))
        @if (recaptcha())
            <script src="https://www.google.com/recaptcha/api.js?render={{ recaptcha('site') }}"></script>
        @endif
    @endif

    @stack('header')
    @if (get_setting('site_header_code', false))
        {{ html_string(get_setting('site_header_code')) }}
    @endif
    @livewireStyles
</head>
@php
    $auth_layout = gws('theme_auth_layout', 'default');
    $logo_light = $auth_layout == 'center-dark' ? 'logo-light' : 'logo';
    $body_class = $auth_layout == 'center-dark' || $auth_layout == 'center-light' ? ' page-ath-alt' : '';
    $body_bgc = $auth_layout == 'center-dark' ? ' bg-secondary' : '';
    $wrap_class = $auth_layout == 'default' ? ' flex-row-reverse' : '';
    $header_logo = '<div class="page-ath-header"><a href="' . url('/') . '" class="page-ath-logo"><center><img loading="lazy" style="max-width: 70%!important;max-height: 70%!important" class="page-ath-logo-img" src=" ' . asset('images/logo-negro.png') . '" srcset="' . asset('images/logo-negro.png') . '" alt="gosen"></center></a></div>';
    
@endphp
@if (!Cookie::get('cookiePrueba'))
    @include('user.includes.cookies')
@endif

<body class="page-ath theme-modern page-ath-modern{{ $body_class . $body_bgc }}">

    <div class="page-ath-wrap{{ $wrap_class }}">
        <div class="page-ath-content">
            {!! UserPanel::language_switcher_login() !!}
            {!! $header_logo !!}
            @yield('content')

            <div class="page-ath-footer">
                @if (is_show_social('login'))
                    {!! UserPanel::social_links('', ['class' => 'mb-3']) !!}
                    {!! UserPanel::footer_links(['lang' => true], ['class' => 'guttar-20px align-items-center']) !!}
                    {!! UserPanel::copyrights('div') !!}
                @else
                    {!! UserPanel::footer_links(
                        ['lang' => true, 'copyright' => true],
                        ['class' => 'guttar-20px align-items-center'],
                    ) !!}
                @endif
            </div>
        </div>
        @if ($auth_layout == 'default' || $auth_layout == 'alter')
            @isset($landing)
                <x-img-random-component :landing="$landing" />
            @endisset
        @empty($landing)
            <x-img-random-component landing="" />
        @endempty
    @endif
</div>

{{-- @if (gws('theme_custom')) --}}
<link preload rel="stylesheet" href="{{ asset(style_theme('custom')) }}">
{{-- @endif --}}
<script>
    var base_url = "{{ url('/') }}",
        csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        layouts_style = "modern";
</script>
<script src="{{ asset('assets/js/jquery.bundle.js') . css_js_ver() }}"></script>
<script src="{{ asset('assets/js/script.js') . css_js_ver() }}"></script>
<script src="{{ asset('assets/app/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.3.min.js"
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.redirect@1.1.4/jquery.redirect.min.js"></script>
<script type="text/javascript">
    jQuery(function() {
        var $frv = jQuery('.validate');
        if ($frv.length > 0) {
            $frv.validate({
                errorClass: "input-bordered-error error"
            });
        }
    });
    $(document).ready(function() {
        $("#btnAuth, #button").click(function() {
            // disable button
            $(this).addClass('disabled');
            // add spinner to button
            $(this).html(
                `{{ ___('Loading') }}...`
            );
        });
    })
</script>
@stack('footer')
@if (get_setting('site_footer_auth_code', false))
    {{ html_string(get_setting('site_footer_auth_code')) }}
@endif
@livewireScripts
@stack('livewireScripts')
</body>

</html>
