    <style>

        body {
            position: initial;
        }
        
        body[data-sidebar=dark] .navbar-brand-box {
            background: var(--color-base-a-blanco-theme-user);
        }
        
        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid var(--color-base-theme-user);
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        #loading {
            position: fixed;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            opacity: 0.7;
            background-color: var(--bg-loading-theme-user);
            z-index: 99;
        }

        .ti-close {
            color: var(--color-ti-close-theme-user) !important;
        }

        #loading-image {
            z-index: 100;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        body[data-sidebar=dark] .vertical-menu {
            background: var(--color-base-a-blanco-theme-user);
        }

        body[data-sidebar=dark] .menu-title {
            color: var(--color-title-sidebar-theme-user);
        }

        body[data-sidebar=dark] #sidebar-menu ul li a {
            color: var(--color-sidebar-menu-ul-a-theme-user);
        }

        body[data-sidebar=dark] #sidebar-menu ul li a i {
            color: var(--color-sidebar-menu-ul-a-theme-user);
        }

        body[data-sidebar=dark] #sidebar-menu a:hover:not(.active) {
            background-color: var(--bg-sedebar-menu-a-hover-theme-user);
            color: var(--color-sedebar-menu-a-hover-theme-user);
        }

        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu>ul>li:hover>a {
            background: var(--bg-sedebar-menu-a-hover-theme-user);
            color: var(--color-vertical-menu-li-hover-theme-user) !important;
        }

        body[data-sidebar=dark] #sidebar-menu ul li ul.sub-menu li a {
            color: #d7e4ec;
        }

        body[data-sidebar=dark] .mm-active .active {
            color: var(--color-sedebar-menu-a-hover-theme-user) !important;
            background-color: var(--bg-sedebar-menu-a-hover-theme-user);
        }

        body[data-sidebar=dark] .mm-active .active i {
            color: var(--color-base-theme-user) !important;
        }

        body[data-sidebar=dark] .mm-active .active i {
            color: var(--color-base-purple-a-blanco-theme-user) !important;
        }

        body[data-sidebar=dark] #vertical-menu-btn {
            color: var(--color-base-sidebar-theme-user);
            border-color: transparent;
        }

        body[data-sidebar=dark] #page-header-user-dropdown {
            color: var(--color-base-sidebar-theme-user);
            border-color: #fff;
        }

        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu ul li li a.active,
        body[data-sidebar=dark].vertical-collpsed .vertical-menu #sidebar-menu ul li li a.mm-active {
            color: #d7e4ec !important;
        }

        .btn {
            min-width: 0px;
        }

        .btn-light {
            color: black;
        }

        .vertical-collpsed .vertical-menu {
            position: fixed;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            background-color: var(--color-base-theme-user);
        }

        .cssbuttons-io-button {
            background: linear-gradient(45deg, var(--color-base-sidebar-theme-user) 0%, var(--color-base-theme-user) 100%) !important;
        }

        .gift-card {
            background: var(--bg-gift-card-theme-user) !important;
            border: var(--border-gift-card-theme-user);
        }

        .gift-card p {
            color: var(--text-gift-card-theme-user) !important;
        }
        
        .car {
            background-image: repeating-radial-gradient(circle at var(--start), transparent 0%, transparent 10%, var(--color-base-sidebar-theme-user) 10%, var(--color-base-theme-user) 17%), linear-gradient(to right, var(--color-base-sidebar-theme-user), var(--color-base-theme-user)) !important;
        }

        .leaderboard header {
            background-image: repeating-radial-gradient(circle at var(--start), transparent 0%, transparent 10%, var(--color-base-sidebar-theme-user) 10%, var(--color-base-theme-user) 17%), linear-gradient(to right, var(--color-base-sidebar-theme-user), var(--color-base-theme-user)) !important;
        }

        .form-control {
            margin-top: 0px !important;
            font-size: .9rem !important;
        }

        .card-body {
            background: radial-gradient(circle, rgb(255 255 255) 0%, var(--color-base-theme-user) 100%) !important;
        }

        .accordion-heading {
            background: var(--color-base-theme-user);
        }

        .accordion-s2 .accordion-heading {
            padding-left: 20px;
        }

        .color-style {
            color: #ffffff;
        }

        .accordion-heading:before {
            top: 20px;
            background: #ffffff;
            margin-right: 10px;
        }

        .accordion-heading:after {
            top: 15px;
            background: #ffffff;
            margin-right: 10px;
        }

        @media (max-width: 420px) and (min-width: 375px) {
            .phase-block {
                display: block;
                align-items: flex-start;
            }
        }

        @media (min-width: 480px) {
            .btn-xs {
                padding: 8px 20px;
                font-size: 14px;
            }
        }
    </style>
