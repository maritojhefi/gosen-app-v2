<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js" content="notranslate" translate="no">

<head>
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">
    <link preload rel="shortcut icon" href="{{ asset2('favicon.png') }}">
    <title>@yield('title') | {{ ucfirst(env('APP_NAME')) }}</title>
    {{-- <link preload rel="stylesheet" href="{{ asset(style_theme('vendor')) }}"> --}}
    {{-- <link preload rel="stylesheet" href="{{ asset(style_theme('user')) }}"> --}}

    <link preload href="{{ asset('assets/css/vendorapp.bundle.css') }}" rel="stylesheet" type="text/css" />

    {{-- <link preload rel="stylesheet" href="{{ asset('assets/css/dropzone.min.css') }}"> --}}
    @include('layouts.color-theme-user')



    <link preload href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link preload href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@600&display=swap" rel="stylesheet">
    <script>
		window.PUSHER_APP_KEY = '{{ config('broadcasting.connections.pusher.key') }}';
		window.APP_DEBUG = {{ config('app.debug') ? 'true' : 'false' }};
	</script>
    <script src="{{asset('assets/websockets.js')}}"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) { 
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) window.scrollTo(0, scrollpos);
        });

        window.onbeforeunload = function(e) {
            localStorage.setItem('scrollpos', window.scrollY);
        };
    </script>
    @include('layouts.include-css')

    @yield('gosencss')

    @stack('header')
    @if (get_setting('site_header_code', false))
        {{ html_string(get_setting('site_header_code')) }}
    @endif
    @livewireStyles
    @livewireScripts
</head>

<body data-sidebar="dark" class="no-touch {{ session('cambiarSidebar') == 'hide' ? ' vertical-collpsed' : '' }}">
    <div id="loading">
        <div class="loader"></div>
    </div>
    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">

                        <a href="{{ route('user.home') }}" class="logo logo-light">
                            <span class="logo-sm text-rotate">
                                <div class="row">
                                    <div class="col pr-0" style="top: 35px">
                                        <span class="span-text">
                                            <img loading="lazy" src="{{ asset('images/token-symbol-light.png') }}"
                                                alt="{{ ucfirst(env('APP_NAME')) }}" height="22">
                                        </span>
                                    </div>
                                </div>
                            </span>
                            <span class="logo-lg text-rotate">
                                <div class="row">
                                    <div class="col-sm-3 pl-3 pr-0">
                                        <span class="span-text"><img loading="lazy" src="{{ asset('images/token-symbol-light.png') }}"
                                                height="40"></span>
                                    </div>
                                    <div class="col-sm-9 mt-0 p-1" translate="no">
                                        <span class="span-text mr-1"
                                            style="font-family: 'Montserrat Alternates', sans-serif;">g</span>
                                        <span class="span-text mr-1">o</span>
                                        <span class="span-text mr-1">s</span>
                                        <span class="span-text mr-1">e</span>
                                        <span class="span-text mr-1">n</span>
                                    </div>
                                </div>
                                {{-- <img loading="lazy" src="{{ site_whitelabel('logo-light') }}"
                                    srcset="{{ site_whitelabel('logo-light2x') }}"
                                    alt="{{ site_whitelabel('name') }}" height="40"> --}}
                            </span>
                        </a>
                    </div>
                    <button type="button" onclick="loadDoc()"
                        class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                        <i class="ri-menu-2-line align-middle"></i>
                    </button>

                    <!-- Url Referral -->
                    {!! UserPanel::user_url_usuario() !!}
                </div>

                <div class="d-flex">
                    {{-- color theme --}}
                    @if (auth()->user()->type_user != 1)
                        <div class="dropdown">
                            {{-- <button type="button" class="btn header-item" id="page-header-user-dropdown"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <input type="color" value="{{ asset(theme_color_user(auth()->user()->theme_color,'base')) }}"
                                    style="width: 33px;
                                        height: 21px;
                                        border: none;
                                        margin-top: 8px;">
                            </button> --}}

                            <button type="button" class="btn header-item" id="page-header-user-dropdown"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <ul class="color-list">
                                    {{-- <input type="color" value="{{ asset(theme_color_user(auth()->user()->theme_color,'base')) }}"
                                    style="width: 33px;
                                        height: 21px;
                                        border: none;
                                        margin-top: 8px;"> --}}
                                    @if (auth()->user()->theme_color == 'style')
                                        <li class="color" data-color="#334574"
                                            style="background: #334574;height: 32px;
                                            width: 32px;margin-top: 11px;">
                                        </li>
                                    @else
                                        @if (auth()->user()->theme_color == 'style-purple')
                                        <li class="color" data-color="#F56651"
                                        style="background: #452c97;height: 32px;
                                        width: 32px;margin-top: 11px;">
                                        </li>
                                        @else
                                        <li class="color" data-color="#F56651"
                                            style="background: {{ asset(theme_color_user(auth()->user()->theme_color, 'base')) }};height: 32px;
                                            width: 32px;margin-top: 11px;">
                                        </li>
                                        @endif
                                        
                                    @endif
                                </ul>
                            </button>


                            <div class="dropdown-menu dropdown-menu-end color-picker"
                                style="border-radius:20px; min-width: 0px;">
                                <!-- item-->
                                <ul class="color-list">
                                    <a href="{{ route('user.theme_color', 'style') }}">
                                        <li class="color" data-color="#334574" style="background: #334574;"></li>
                                    </a>
                                    <a href="{{ route('user.theme_color', 'style-blue') }}">
                                        <li class="color" data-color="#2c80ff" style="background: #2c80ff;"></li>
                                    </a>
                                    @if (auth()->user()->type_user == 3)
                                        <a href="{{ route('user.theme_color', 'style-coral') }}">
                                            <li class="color" data-color="#ce2e2e" style="background: #ce2e2e;"></li>
                                        </a>
                                        <a href="{{ route('user.theme_color', 'style-green') }}">
                                            <li class="color" data-color="#21a184" style="background: #21a184;"></li>
                                        </a>
                                        <a href="{{ route('user.theme_color', 'style-black') }}">
                                            <li class="color" data-color="#000" style="background: #000;"></li>
                                        </a>
                                        <a href="{{ route('user.theme_color', 'style-purple') }}">
                                            <li class="color" data-color="#452c97" style="background: #452c97;"></li>
                                        </a>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endif
                    {!! UserPanel::language_switcher_usuario() !!}

                    <div class="dropdown d-none d-lg-inline-block ms-1">
                        <button type="button" class="btn header-item noti-icon waves-effect"
                            data-toggle="fullscreen">
                            <i class="ri-fullscreen-line"></i>
                        </button>
                    </div>

                    <div class="dropdown d-inline-block user-dropdown">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if (auth()->user()->foto == null || auth()->user()->foto == '')
                                <img loading="lazy"  src="{{ asset2('imagenes/user-default.jpg') }}"
                                    class="fotoperfil rounded-circle header-profile-user admin_picture">
                            @else
                                <img loading="lazy"  class="fotoperfil rounded-circle header-profile-user admin_picture"
                                    src="{{ asset2('imagenes/perfil/') }}/{{ auth()->user()->foto }}">
                            @endif
                            <span
                                class="d-none d-xl-inline-block ms-1">{{ auth()->user()->username == null ? auth()->user()->name : auth()->user()->username }}</span>
                            <i class="mdi mdi-chevron-down d-xl-inline-block"></i><!-- d-none -->
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" style="border-radius:20px;">
                            <!-- item-->
                            <a class="dropdown-item" id="perfil" href="{{ route('user.account', 2) }}"
                                style="border-radius:20px;"><i class="ri-user-line align-middle me-1"></i>
                                {{ ___('My Profile') }}</a>

                            @if (get_page('referral', 'status') == 'active' && is_active_referral_system())
                                <a class="dropdown-item d-block" href="{{ route('user.referral') }}"
                                    style="border-radius:20px;"><i class="ri-team-line align-middle me-1"></i>
                                    {{ get_page('referral', 'menu_title') }}</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('user.account.activity.asd') }}"
                                style="border-radius:20px;"><i class="ri-eye-line align-middle me-1"></i>
                                {{ ___('Activity') }}</a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger" href="{{ route('log-out') }}"
                                style="border-radius:20px;"
                                onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();">
                                <i class="ri-shut-down-line align-middle me-1 text-danger"></i> {{ ___('Logout') }}
                            </a>
                            <form id="logout-form" action=" {{ route('logout') }}" method="POST"
                                style="display: none;"> <input type="hidden" name="_token"
                                    value="{{ csrf_token() }}"></form>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- ========== Left Sidebar Start ========== -->

        <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">{{ ___('Menu') }}</li>

                        @if (showPage('Dashboard'))
                            <li>
                                <a href="{{ route('user.home') }}" class="waves-effect">
                                    <i class="ri-dashboard-line"></i>
                                    <span>{{ ___('Dashboard') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Buy Token'))
                            <li>
                                <a href="{{ route('user.token') }}" class=" waves-effect">
                                    <i class="ri-shopping-cart-2-fill"></i>
                                    <span>{{ ___('Buy Token') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('ICO Distribution'))
                            <li>
                                <a href="{{ route('public.pages', 'distribution') }}" class=" waves-effect">
                                    <i class="ri-share-line"></i>
                                    <span>{{ get_page('distribution', 'title') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Transactions'))
                            <li>
                                <a href="{{ route('user.transactions') }}" class=" waves-effect">
                                    <i class="ri-share-forward-2-line"></i>
                                    <span>{{ ___('Transactions') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (/*nio_module()->has('Withdraw')&&*/ has_route('withdraw:user.index') && showPage('Withdraw'))
                            <li>
                                <a href="{{ route('withdraw:user.index') }}" class=" waves-effect">
                                    <i class="ri-newspaper-line"></i>
                                    <span>{{ ___('Withdraw') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (gws('user_mytoken_page') == 1 && showPage('My Token'))
                            <li>
                                <a href="{{ route('user.token.balance.token') }}" class="waves-effect">
                                    <i class="fas fa-coins"></i>
                                    <span>{{ ___('My Token') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Referral'))
                            <li>
                                <a href="{{ route('user.referral') }}" class="waves-effect">
                                    <i class="ri-team-line"></i>
                                    <span>{{ get_page('referral', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Bonus'))
                            <li>
                                <a href="{{ route('user.bonus') }}" class="waves-effect">
                                    <i class="fa fa-dollar-sign"></i>
                                    <span>{{ ___('Bonus') }}</span>
                                </a>
                            </li>
                        @endif

                        @if (showPage('Marketing'))
                            <li>
                                <a href="{{ route('marketing') }}" class="waves-effect">
                                    <i class="fa fa-lightbulb"></i>
                                    <span>{{ ___('Marketing') }}</span>
                                </a>
                            </li>
                        @endif

                        @foreach (getAllPages() as $page)
                            @if ($page->status == 'active')
                                <li>
                                    <a href="{{ route('public.pages', $page->slug) }}" class="waves-effect">
                                        <i class="fa {{ $page->icon }}"></i>
                                        <span>{{ $page->menu_title }} </span>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                        {{-- @if (get_page('how_buy', 'status') == 'active')
                            <li>
                                <a href="{{ route('public.pages', get_slug('how_buy')) }}" class="waves-effect">
                                    <i class="ri-slideshow-2-line"></i>
                                    <span>{{ get_page('how_buy', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif

                        @if (get_page('custom_page', 'status') == 'active')
                            <li>
                                <a href="{{ route('public.pages', get_slug('custom_page')) }}"
                                    class="waves-effect">
                                    <i class="ri-file-info-line"></i>
                                    <span>{{ get_page('custom_page', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif
                        @if (get_page('privacy', 'status') == 'active')
                            <li>
                                <a href="{{ route('public.pages', get_slug('privacy')) }}"
                                    class="waves-effect">
                                    <i class="ri-file-info-line"></i>
                                    <span>{{ get_page('privacy', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif --}}


                        <li class="menu-title">More</li>

                        <li>
                            <a href="{{ route('top-referrals') }}" class="waves-effect">
                                <i class="fa fa-trophy"></i>
                                <span>{{ ___('Top Referrals') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('user.createvalo') }}" class="waves-effect">
                                <i class="fa fa-comments"></i>
                                <span>{{ ___('Valorations') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('listado.faq') }}" class="waves-effect">
                                <i class="ri-questionnaire-line"></i>
                                <span>{{ get_page('faq', 'menu_title') }} </span>
                            </a>
                        </li>


                        @if (get_page('privacy', 'status') == 'active')
                            <li>
                                <a href="{{ route('public.pages', get_slug('privacy')) }}" class="waves-effect">
                                    <i class="ri-pages-line"></i>
                                    <span>{{ get_page('privacy', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif


                        @if (get_page('terms', 'status') == 'active')
                            <li>
                                <a href="{{ route('public.pages', get_slug('terms')) }}" class="waves-effect">
                                    <i class="ri-pages-line"></i>
                                    <span>{{ get_page('terms', 'menu_title') }} </span>
                                </a>
                            </li>
                        @endif


                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class=" ri-account-circle-line"></i>
                                <span>{{ ___('Profile') }}</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li><a href="{{ route('user.account', 2) }}">{{ ___('My Profile') }}</a></li>

                                @if (get_page('referral', 'status') == 'active' && UserPanel::get_referrals())
                                    <li><a
                                            href="{{ route('user.referral') }}">{{ get_page('referral', 'menu_title') }}</a>
                                    </li>
                                @endif
                                <li><a href="{{ route('user.account.activity.asd') }}">{{ ___('Activity') }}</a></li>
                            </ul>
                        </li>



                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content mt-3">

            <div class="page-content">
                <div class="container-fluid">

                    <div class="row">
                        @php
                            $has_sidebar = isset($has_sidebar) ? $has_sidebar : false;
                            $col_side_cls = $has_sidebar ? 'col-lg-4' : 'col-lg-12';
                            $col_cont_cls = $has_sidebar ? 'col-lg-8' : 'col-lg-12';
                            $col_cont_cls2 = isset($content_class) ? css_class($content_class) : null;
                            $col_side_cls2 = isset($aside_class) ? css_class($aside_class) : null;
                        @endphp

                        <div class="{{ empty($col_cont_cls2) ? $col_cont_cls : $col_cont_cls2 }}">
                            @if (!has_wallet() && gws('token_wallet_req') == 1 && !empty(token_wallet()))
                                <div class="d-lg-none">
                                    {!! UserPanel::add_wallet_alert() !!}
                                </div>
                            @endif
                            @include('partials.messages-admin')
                            @yield('content')
                        </div>

                        @if ($has_sidebar == true)
                            <div
                                class="aside sidebar-right {{ empty($col_side_cls2) ? $col_side_cls : $col_side_cls2 }}">
                                @if (!has_wallet() && gws('token_wallet_req') == 1 && !empty(token_wallet()))
                                    <div class="d-none d-lg-block">
                                        {!! UserPanel::add_wallet_alert() !!}
                                    </div>
                                @endif
                                <div class="account-info card">
                                    <div class="card-innr">
                                        {!! UserPanel::user_account_status_usuario() !!}
                                        @if (!empty(token_wallet()))
                                            <div class="gaps-2-5x"></div>
                                            {!! UserPanel::user_account_wallet() !!}
                                        @endif
                                    </div>
                                </div>
                                {!! !is_page(get_slug('referral')) ? UserPanel::user_referral_info('') : '' !!}
                                @if (!is_kyc_hide())
                                    {!! UserPanel::user_kyc_info_usuario('') !!}
                                @endif
                            </div>{{-- .col --}}
                        @else
                            @stack('sidebar')
                        @endif

                    </div>

                </div> <!-- container-fluid -->


            </div>
            <!-- End Page-content -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->


            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! UserPanel::copyrights('div') !!}
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end  d-none   d-sm-block">
                                {!! UserPanel::social_links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </footer>


        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <x-evento-vista-component />

    @yield('modals')
    <div id="ajax-modal"></div>
    <div class="page-overlay">
        <div class="spinner"><span class="sp sp1"></span>
            <span class="sp sp2"></span><span class="sp sp3"></span>
        </div>
    </div>

    @if (gws('theme_custom'))
        <link preload rel="stylesheet" href="{{ asset(style_theme('custom')) }}">
    @endif

    <script>
        var base_url = "{{ url('/') }}",
            {!! has_route('transfer:user.send') ? 'user_token_send = "' . route('transfer:user.send') . '",' : '' !!}
        {!! has_route('withdraw:user.request') ? 'user_token_withdraw = "' . route('withdraw:user.request') . '",' : '' !!}
        {!! has_route('user.ajax.account.wallet')
            ? 'user_wallet_address = "' . route('user.ajax.account.wallet') . '",'
            : '' !!}
        csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        var msg_wrong = "{{ ___('Something is Wrong!') }}",
            msg_cancel_order = "{{ ___('Do you really cancel your order?') }}",
            msg_unable_process = "{{ ___('Unable process request!') }}",
            msg_sure = "{{ ___('Are you sure?') }}",
            msg_unable_perform = "{{ ___('Unable to perform!') }}",
            msg_use_modern_browser = "{{ ___('Please use a modern browser to properly view this template!') }}",
            num_fmt = {{ gws('token_number_format', 0) ? 'true' : 'false' }};
    </script>
    <script src="{{ asset('assets/js/jquery.bundle.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/script.js') . css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/app.js') . css_js_ver() }}"></script>


    <!-- JAVASCRIPT
        <script src="{{ asset('assets/app/libs/jquery/jquery.min.js') }}"></script>-->
       
    <script src="{{ asset('assets/app/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/app/libs/node-waves/waves.min.js') }}"></script>

    <!-- Plugins js -->
    <script src="{{ asset('assets/app/libs/dropzone/min/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/app/js/app.js') }}"></script>
    <script src="{{ asset('assets/app/js/user.js') }}"></script>


    {{-- <script src="{{ asset('assets/app/js/dropzone.min.js') }}"></script> --}}
    {{-- <script src="https://unpkg.com/cropperjs"></script> --}}
    <!-- image crop -->
    <script src="{{ asset('assets/app/libs/ijaboCropTool/ijaboCropTool.min.js') }}"></script>
    <!--/ JAVASCRIPT -->
    <script>
        function loadDoc() {
            const xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                document.getElementById("app").innerHTML = this.responseText;
            }
            @production
            xhttp.open("GET", "/user/cambiarSidebar");
        @endproduction
        @env('local')
            xhttp.open("GET", "/tokenlite/user/cambiarSidebar");
        @endenv
        xhttp.send();
        }
    </script>
    <script>
        function show() {
            $('#svg1').removeClass("show");
            $('#svg1').addClass("hide");
            $('#svg2').addClass("show");
            $('#svg2').removeClass("hide");
            const myTimeout = setTimeout(mostrar, 2000);
        }

        function mostrar() {
            $('#svg1').addClass("show");
            $('#svg1').removeClass("hide");
            $('#svg2').removeClass("show");
            $('#svg2').addClass("hide");
        }
    </script>

    @stack('footer')
    <script type="text/javascript">
        @if (session('resent'))
            show_toast("success", "{{ ___('A fresh verification link has been sent to your email address.') }}");
        @endif
        show_toast(response.msg, response.message);
    </script>
    @if (get_setting('site_footer_code', false))
        {{ html_string(get_setting('site_footer_code')) }}
    @endif
    <script>
        $(window).on('load', function() {
            $('#loading').hide();
        })

        $(document).ajaxComplete(function() {

            setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)

        });
        $(document).ready(function() {
            $("a").click(function() {
                if ($(this).prop('target').includes('blank') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('excel') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('export') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 1000)
                } else if ($(this).attr('href').includes('http') == true) {
                    $('#loading').show();
                    setTimeout(() => {
                        $('#loading').hide();
                    }, 4000)

                }
            });
        });
    </script>
</body>

</html>
