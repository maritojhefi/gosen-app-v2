<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imagene extends Model
{
    protected $table = 'images_news';

    use HasFactory;

    protected $fillable = ['nombre', 'new_id'];

    public function noticia()
    {
        return $this->belongsTo(Noticia::class);
    }
}
