<?php
/**
 * Bitacora Model
 *
 *  Manage the Bitacora data
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    /*
     * Table Name Specified
     */
    protected $table = 'bitacoras';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'host', 'usuario', 'operacion', 'tabla', 'description',
    ];
}
