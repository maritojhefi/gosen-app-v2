<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatabaseSize extends Model
{
    use HasFactory;

    protected $fillable = ['name_backup', 'weight', 'top_three_heavier', 'top_three_lightest'];
}
