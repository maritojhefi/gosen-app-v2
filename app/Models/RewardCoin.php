<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RewardCoin extends Model
{
    use HasFactory;

    protected $fillable = ['video1', 'video2', 'video3', 'pdf', 'modal', 'idUser'];
}
