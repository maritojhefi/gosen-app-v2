<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorySendedMail extends Model
{
    use HasFactory;

    protected $fillable = [
        'header', 'content', 'footer', 'type_user', 'type_mail', 'total', 'to_specific_users',
    ];

    public function specificUsers()
    {
        $usuarios = json_decode($this->type_user);
        $coleccion = collect();
        foreach ($usuarios as $idUsuario) {
            $coleccion->push(User::findOrFail($idUsuario));
        }

        return $coleccion;
    }

    public function types_user()
    {
        $types = json_decode($this->type_user);
        $coleccion = collect();
        foreach ($types as $idType) {
            $coleccion->push(Menu::findOrFail($idType));
        }

        return $coleccion;
    }
}
