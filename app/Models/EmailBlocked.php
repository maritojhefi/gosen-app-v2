<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailBlocked extends Model
{
    use HasFactory;

    protected $fillable = ['correo', 'tipo', 'descripcion', 'estado'];
}
