<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusDolar extends Model
{
    use HasFactory;

    const TYPE1 = 'withdraw';

    const TYPE2 = 'bonus';

    const STATUS1 = 'pending';

    const STATUS2 = 'approved';

    const STATUS3 = 'canceled';

    const BONUSCODETNX = 'BD-TNX';

    const BONUSCODEWTD = 'BD-WTD';

    const TYPEUSER1 = 'essential';

    const TYPEUSER2 = 'intermediate';

    const TYPEUSER3 = 'advanced';

    protected $fillable = [
        'stage_id', 'checked_by', 'user_id', 'amount', 'transaction_id', 'status', 'type_bonus', 'type_value', 'code', 'user_from', 'type', 'wallet', 'comment', 'level_bonus', 'hash',
    ];

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from');
    }

    public function userBy()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function checkedBy()
    {
        return $this->belongsTo(User::class, 'checked_by');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function stage()
    {
        return $this->belongsTo(IcoStage::class, 'stage_id');
    }
}
