<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappConversacione extends Model
{
    use HasFactory;

    protected $fillable = ['conversacion_id', 'origen', 'destino', 'mensaje'];

    protected $appends = ['contenido'];

    public function getContenidoAttribute()
    {
        $json = json_decode($this->mensaje);
        if (isset($json->text)) {
            return $json->text;
        } elseif (isset($json->image)) {
            return '<img loading="lazy" width="300" src="'.$json->image->url.'" />';
        }
    }
}
