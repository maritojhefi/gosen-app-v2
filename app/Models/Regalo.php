<?php
/**
 * Regalo Model
 *
 *  Manage the Regalo data
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regalo extends Model
{
    /*
     * Table Name Specified
     */
    protected $table = 'regalos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'referral', 'gift_id', 'gift_referral', 'gift_token', 'total_referral', 'status', 'tnx'];
}
