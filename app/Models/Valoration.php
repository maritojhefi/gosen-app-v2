<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Valoration extends Model
{
    use HasFactory;

    protected $fillable = ['comentario', 'puntaje', 'estado', 'idUser'];

    public function user()
    {
        return $this->belongsTo(User::class, 'idUser');
    }

    public function setComentarioAttribute($value)
    {
        $value = preg_replace('([^A-Za-z0-9 ])', '', $value);
        $this->attributes['comentario'] = ucfirst(strtolower(($value)));
    }

    public function getComentarioAttribute($value)
    {
        return ucfirst(strtolower($value));
    }
}
