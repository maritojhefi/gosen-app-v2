<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FrequentQuestion extends Model
{
    /*
     * Table Name Specified
     */
    protected $table = 'preguntas_frecuentes';

    protected $fillable = ['titulo', 'contenido', 'observacion'];

    // public function getContenidoAttribute($value)
    // {
    //     return ucfirst(strtolower($value));
    // }

    // public function getTituloAttribute($value)
    // {
    //     return ucfirst(strtolower($value));
    // }
}
