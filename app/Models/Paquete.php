<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use HasFactory;

    protected $fillable = [
        'tokens', 'nombre', 'valor_usd', 'estado', 'descuento', 'stock', 'estado_bono', 'porcentaje_bono', 'dias_bono', 'fecha_limite_bono', 'foto', 'type_user', 'dias_vigencia', 'estado_fecha_limite', 'meses_info', 'porcentaje_info'
    ];

    public function getNombreAttribute($value)
    {
        return ___($value);
    }

    public function getFechaLimiteBonoAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function niveles()
    {
        return $this->hasMany(LevelBonusDollar::class, 'type_user');
    }
}
