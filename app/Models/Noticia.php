<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $table = 'news';

    protected $fillable = ['titulo', 'descripcion', 'fechaInicio', 'fechaFin', 'image'];

    public function getTituloAttribute($value)
    {
        //Reemplazamos la A y a
        /* $value = str_replace(
             array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
             array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
             $value
         );

         //Reemplazamos la E y e
         $value = str_replace(
             array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
             array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
             $value
         );

         //Reemplazamos la I y i
         $value = str_replace(
             array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
             array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
             $value
         );

         //Reemplazamos la O y o
         $value = str_replace(
             array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
             array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
             $value
         );

         //Reemplazamos la U y u
         $value = str_replace(
             array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
             array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
             $value
         );

         //Reemplazamos la N, n, C y c
         $value = str_replace(
             array('Ñ', 'ñ', 'Ç', 'ç'),
             array('N', 'n', 'C', 'c'),
             $value
         );*/

        $primerFiltro = mb_strtoupper($value, 'UTF-8');

        return  preg_replace('/[0-9\@\.\;\""´`]+/', '', $primerFiltro);
    }

    public function getDescripcionAttribute($value)
    {
        return ucfirst(strtolower($value));
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'new_user', 'new_id', 'user_id');
    }

    public function imagenes()
    {
        return $this->hasMany(Imagene::class, 'new_id');
    }
}
