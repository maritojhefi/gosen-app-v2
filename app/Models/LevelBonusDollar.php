<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LevelBonusDollar extends Model
{
    use HasFactory;

    const TYPE1 = 'fixed';

    const TYPE2 = 'percent';

    const STATUS1 = 'disabled';

    const STATUS2 = 'enabled';

    protected $fillable = ['name', 'type', 'value', 'status', 'max', 'type_user'];
}
