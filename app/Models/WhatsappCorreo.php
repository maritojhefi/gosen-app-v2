<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappCorreo extends Model
{
    use HasFactory;

    protected $fillable = ['header', 'content', 'footer', 'type_mail', 'number_days', 'status', 'slug', 'time', 'lang'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_whatsapp_correo', 'whatsapp_correo_id', 'user_id')->withPivot('id')->withTimestamps();
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'lang');
    }
}
