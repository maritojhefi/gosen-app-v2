<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserReset extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'old_mail', 'new_mail', 'mail'];
}
