<?php
/**
 * RegaloType Model
 *
 *  Manage the RegaloType data
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegaloType extends Model
{
    /*
     * Table Name Specified
     */
    protected $table = 'regalo_tipos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'total_tokens', 'total_guests', 'status', 'comment'];

    public function bonusreferral()
    {
        return $this->hasMany(BonusReferral::class, 'id');
    }
}
