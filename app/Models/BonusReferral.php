<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusReferral extends Model
{
    use HasFactory;

    protected $fillable = [
        'regalo_tipo_id',
        'user_id',
        'tnx_id',
        'total_referral',
    ];

    public function regalotype()
    {
        return $this->belongsTo(RegaloType::class, 'regalo_tipo_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
