<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusService extends Model
{
    use HasFactory;

    protected $fillable = [
        'service', 'status', 'log', 'url_base', 'uri', 'description', 'color',
    ];

    const SERVICIOS = [
        'whatsapp(Disruptive)' => ['https://rest.messagebird.com', '/balance'],
        'whatsapp(Soporte)' => ['https://rest.messagebird.com', '/balance'],
        // 'dall-e'=>['https://',''],
        'stability' => ['https://api.stability.ai/v1/user', '/balance'],
        'apigps' => ['https://ipwho.is', ''],
        'ratecoin' => ['https://data.exratesapi.com', '/rates'],
        // 'aws'=>['https://',''],
    ];
}
