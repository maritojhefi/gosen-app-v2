<?php

namespace App\Http\Middleware;

use App\Models\Transaction;
use Closure;
use Illuminate\Http\Request;

class CheckPageBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $namePage)
    {
        $usuario = auth()->user()->id;
        $transaction = Transaction::select('id')->where([['user', $usuario], ['status', 'approved'], ['tnx_type', 'purchase'], ['nft', null]])->count();
        if ($transaction > 0) {
            return $next($request);
        } else {
            abort(404);
        }
    }
}
