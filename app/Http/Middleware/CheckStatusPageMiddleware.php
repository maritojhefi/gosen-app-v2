<?php

namespace App\Http\Middleware;

use App\Models\MainPage;
use Closure;
use Illuminate\Http\Request;

class CheckStatusPageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $name)
    {
        $page = MainPage::where('title', $name)->firstOrFail();
        if (! $page->status) {
            abort(404);
        } else {
            if (isset($page->blocked)) {
                $bloqueados = json_decode($page->blocked);
                foreach ($bloqueados as $blocked) {
                    if ($blocked == auth()->user()->type_user) {
                        abort(404);
                    }
                }
            }
        }

        return $next($request);
    }
}
