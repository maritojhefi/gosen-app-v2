<?php

namespace App\Http\Middleware;

use App\Jobs\TranslateJob;
use Closure;
use Illuminate\Http\Request;

class TranslateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->collect()->count() > 0) {
            //dd($request->all());
            TranslateJob::dispatch($request->all());

            return $next($request);
        } else {
            return $next($request);
        }
    }
}
