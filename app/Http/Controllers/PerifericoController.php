<?php

namespace App\Http\Controllers;

use App\Helpers\GosenHelper;
use App\Mail\changeMail;
use App\Mail\ConfirmResetMail;
use App\Mail\ResetearContraMail;
use App\Mail\VerifyAccountMail;
use App\Mail\WalletConfirmationMail;
use App\Models\LevelBonusDollar;
use App\Models\LogsGosen;
use App\Models\MainPage;
use App\Models\Notification;
use App\Models\Paquete;
use App\Models\StatusService;
use App\Models\User;
use App\Models\UserReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\Password;
use Intervention\Image\Facades\Image;
use Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Stichoza\GoogleTranslate\GoogleTranslate;

class PerifericoController extends Controller
{
    public function cerrarSesion($sesion)
    {
        if (Auth::check()) {
            $sesion = DB::table('sessions')->where('id', $sesion)->delete();
        }

        return back();
    }

    public function revisarCorreo($correo)
    {
        $buscarMail = User::where('email', $correo)->first();
        if ($buscarMail) {
            return 'existe';
        } else {
            return 'disponible';
        }
    }

    public function revisarUsername($username)
    {
        $buscarUsername = User::where('username', $username)->first();
        if ($buscarUsername) {
            return 'existe';
        } else {
            return 'disponible';
        }
    }

    public function cambiarSidebar(Request $request)
    {
        if (session('cambiarSidebar') == 'hide') {
            $request->session()->put('cambiarSidebar', 'show');

            return 'si';
        } else {
            $request->session()->put('cambiarSidebar', 'hide');

            return 'no';
        }
    }

    public function traduccionGlobal($texto)
    {
        $texto = str_replace('-', ' ', $texto);
        //dd($texto);
        try {
            $espanol = GoogleTranslate::trans($texto, 'es');
            $ingles = GoogleTranslate::trans($texto, 'en');
            $portugues = GoogleTranslate::trans($texto, 'pt');
            $ruso = GoogleTranslate::trans($texto, 'ru');
            $italiano = GoogleTranslate::trans($texto, 'it');
            $frances = GoogleTranslate::trans($texto, 'fr');
            $hindu = GoogleTranslate::trans($texto, 'hi');
            // $arabe = GoogleTranslate::trans($texto, 'ar');
            $turkish = GoogleTranslate::trans($texto, 'tr');
            $german = GoogleTranslate::trans($texto, 'de');
            $chinese = GoogleTranslate::trans($texto, 'zh');
            //dd($espanol.' '.$ingles.' '.$portugues);
            $idiomas = [
                'es' => $espanol,
                'en' => $ingles,
                'pr' => $portugues,
                'ru' => $ruso,
                'it' => $italiano,
                'fr' => $frances,
                'hi' => $hindu,
                // 'ar' => $arabe,
                'tr' => $turkish,
                'de' => $german,
                'zh' => $chinese,

            ];
            foreach ($idiomas as $idioma => $traduccion) {
                $jsonString = file_get_contents(base_path('resources/lang/'.$idioma.'.json'));
                $data = json_decode($jsonString, true);
                // Update Key
                $data[$texto] = $traduccion;
                // Write File
                $newJsonString = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                file_put_contents(base_path('resources/lang/'.$idioma.'.json'), stripslashes($newJsonString));
                // Get Key Value
            }

            return 'Traducciones creadas para el texto: '.$texto.'!';
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function rutaReturnVideo()
    {
        return back()->with('success', ___('Felicidades, usted gano 5 tokens'));
    }

    public function ResetearContraMail($id)
    {
        if (Auth::check()) {
            abort(401);
        }
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);
        if ($user->reset_status == true) {
            return view('user.reset-password', compact('id'));
        // return back()->with('success','✔️ The password was changed successfully!');
        } else {
            abort(401);
        }
    }

    public function changeStatus($id)
    {
        $encontrar = LevelBonusDollar::findOrFail($id);
        if ($encontrar) {
            if ($encontrar->status == LevelBonusDollar::STATUS1) {
                $encontrar->status = LevelBonusDollar::STATUS2;
                $encontrar->save();

                return response()->json('enabled');
            } else {
                $encontrar->status = LevelBonusDollar::STATUS1;
                $encontrar->save();

                return response()->json('disabled');
            }
        } else {
            abort(500);
        }
    }

    public function ChangeMail($id)
    {
        if (Auth::check()) {
            abort(401);
        }
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);
        if ($user->reset_mail == true) {
            return view('user.change-mail', compact('id', 'user'));
        } else {
            abort(401);
        }
    }

    public function changeNotification(Request $request)
    {
        try {
            $notificaciones = GosenHelper::arrayNotifySettings();
            if (Auth::check()) {
                $user = User::find(auth()->user()->id);
                $notify_settings = $user->notifications_setting;
                if (isset($notify_settings)) {
                    $notificaciones = json_decode($notify_settings, true);
                    if ($notificaciones[$request->notify]) {
                        $notificaciones[$request->notify] = false;
                    } else {
                        $notificaciones[$request->notify] = true;
                    }
                }
                $user->notifications_setting = json_encode($notificaciones);
                $user->save();
            }

            return 'ok';
        } catch (\Throwable $th) {
            LogsGosen::create([
                'titulo' => 'Notificacion ajuste error usuario : '.$user->id,
                'log' => $th->getMessage(),
            ]);

            return $th->getMessage();
        }
    }

    public function detenerCampana()
    {
        $notifications = Notification::where('user_id', auth()->user()->id)->first();
        if ($notifications) {
            $notifications->campana = false;
            $notifications->save();

            return 'apagado';
        } else {
            return 'nada';
        }
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => [
                'required',
                'max:50',
                Password::min(8)
                    ->mixedCase()
                    ->letters()
                    ->numbers()
                    ->symbols()
                    ->uncompromised(),
            ],
            'password_confirmation' => 'required|min:8|max:50|same:password',

        ]);
        $idUser = Crypt::decrypt($request->id);
        $user = User::findOrFail($idUser);
        $user->reset_status = false;
        $user->password = Hash::make($request->password);
        $user->save();
        GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('cambio_de_password'));

        return view('user.includes.confirmResetPass-include');
    }

    public function UpdateMail(Request $request)
    {
        // dd($request);
        $request->validate([
            'email' => ['required', 'different:old-email', 'string', 'email', 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix', 'max:255', 'unique:users'],
        ]);

        $idUser = Crypt::decrypt($request->id);
        $user = User::findOrFail($idUser);

        $user->reset_mail = false;
        $user->userReset->mail = $request->email;
        $user->userReset->save();
        $user->save();
        //dd($user->userReset);
        for ($i = 0; $i < 2; $i++) {
            $i == 0 ? $ruta = route('confirm.mail', [$request->id, $i]) : $ruta = route('confirm.mail', [$request->id, $i]);
            $i == 0 ? $receptor = $user->userReset->mail : $receptor = $user->email;
            //dd($ruta);
            Mail::to($receptor)
                ->send(new ConfirmResetMail($user, $ruta));
        }

        return view('user.includes.verificationResetMail', compact('user'));
    }

    public function confirmationMail($id, $receptor)
    {
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);
        if ($receptor == 1) {
            $user->userReset->old_mail = true;
        } else {
            $user->userReset->new_mail = true;
        }
        $user->userReset->save();
        if ($user->userReset->old_mail == true && $user->userReset->new_mail == true) {
            $user->email = $user->userReset->mail;
            $user->save();
            GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('email_verificado', []));
        }
        // $info=$user->userReset;

        // dd($user);
        return view('user.includes.confirmResetMail-include', compact('user'));
    }

    public function sendMail($id)
    {
        $ruta = route('reset.password', $id);
        $idUser = Crypt::decrypt($id);
        $user = User::find($idUser);
        if ($user) {
            $user->reset_status = true;
            $user->save();
        }
        Mail::to($user->email)
            ->send(new ResetearContraMail($user, $ruta));

        return back()->with('success', ___('The email was sent to reset the password'));
    }

    public function sendChangeMail($id)
    {
        $change = route('change.mail', $id);
        $idUser = Crypt::decrypt($id);
        $user = User::find($idUser);
        if ($user) {
            $user->reset_mail = true;
            $user->save();
        }
        Mail::to($user->email)

            ->send(new changeMail($user, $change));
        //aqui crear registro de user_reset
        if ($user->userReset) {
            $user->userReset->delete();
        }
        $userReset = UserReset::create([
            'user_id' => $user->id,
        ]);
        $userReset->save();

        return back()->with('success', ___('The email was sent to change email'));
    }

    public function sendMailWalletConfirm(Request $request)
    {
        $timer = Carbon::now()->modify('+5 minutes');
        $ruta = route('update.wallet', $request->id);
        $idUser = Crypt::decrypt($request->id);
        $request->validate([
            'wallet_dollar' => 'required|string|min:20|unique:users,wallet_dollar,'.$idUser,
        ]);
        $user = User::findOrFail($idUser);
        $wallet = $request->wallet_dollar;
        if ($user) {
            $user->timer_mail_wallet = $timer;
            $user->wallet_dollar_status = false;
            $user->wallet_dollar = $request->wallet_dollar;
            $user->save();
        }
        Mail::to($user->email)
            ->send(new WalletConfirmationMail($user, $ruta, $wallet, $timer));

        return back()->with('success', ___('A message was sent to your email to confirm the wallet'));
    }

    public function actualizarBilletera($id)
    {
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);
        $estado = $user->wallet_dollar_status;
        $user->wallet_dollar_status = true;
        $user->save();
        GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('cambio_billetera', [0, 0, 0]));

        return view('user.update-wallet', compact('id', 'estado', 'user'));
    }

    public function withdrawDollarsConfirm($id)
    {
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);

        return view('user.withdraw-dollars-confirm', compact('id', 'user'));
    }

    public function downloadBanner($bnr, $num)
    {
        $user = auth()->user();
        $idioma = $user->lang;
        switch ($num) {
            case '1':
                $numeroBanner = 'banners/'.$idioma.'/1.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
            case '2':
                $numeroBanner = 'banners/'.$idioma.'/2.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
            case '3':
                $numeroBanner = 'banners/'.$idioma.'/3.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
            case '4':
                $numeroBanner = 'banners/'.$idioma.'/4.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
            case '5':
                $numeroBanner = 'banners/'.$idioma.'/5.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
            default:
                $numeroBanner = 'banners/'.$idioma.'/1.png';
                $position = 'center';
                $padd1 = -6;
                $padd2 = 275;
                break;
        }
        $ruta = route('public.referral').'/'.$bnr.'/'.substr(set_id(auth()->user()->username), 2).'?lang='.$idioma;
        //dd($bnr);
        $img = Image::make(Storage::disk('rutaPublic')->get($numeroBanner))->resize(648, 1152);

        QrCode::format('png')->backgroundColor(255, 0, 0, 0.5)->generate($ruta, public_path().'/qrcode.png');
        $filename = 'banner-'.$bnr;

        $qr = Image::make(Storage::disk('rutaPublic')->get('qrcode.png'))->resize(235, 235);
        $img->insert($qr, $position, $padd1, $padd2);
        $img->save(public_path().'/banners/'.$filename.'.jpg');
        $file = public_path().'/banners/'.$filename.'.jpg';
        $headers = [
            'Content-Type: image',
        ];

        $return = Response::download($file, 'banner.jpg', $headers);
        ob_end_clean();

        return $return;
        //Storage::disk('rutaPublic')->delete("/banners/".$filename.".jpg");
        //return Response::download();

        //return Storage::download('app/public/'.$filename.'.jpg');
    }

    public function ResendEmail()
    {
        $notifiable = session('notifiable');
        $ruta = session('ruta');
        $tiempo = Carbon::now()->addMinutes(2)->format('Y-m-d H:i:s');
        session(['created' => $tiempo]);
        Mail::to(session('notifiable')->email)
            ->send(new VerifyAccountMail($notifiable, $ruta));
    }

    public function login()
    {
        if (Cookie::has('retargeting')) {
            Cookie::queue(Cookie::forget('retargeting'));
        }

        return redirect('/login');
    }

    public function termCondition()
    {
        return view('disruptive.terms-privacy.termsAndCondition');
    }

    public function privacyPolicy()
    {
        return view('disruptive.terms-privacy.privacyAndPolicy');
    }

    public function packageInfo(Request $request)
    {
        $paquete = Paquete::find($request->package);
        $paquetesVendidos = GosenHelper::cantidadPaquetesVendidos();
        $simboloToken = token_symbol();
        $curren = base_currency(true);
        $stockTotal = $paquete->stock;

        if (isset($paquetesVendidos[$request->package])) {
            $stockPaquete = $stockTotal - $paquetesVendidos[$request->package];
        } else {
            $stockPaquete = 0;
        }

        return [$paquete, $paquetesVendidos, $stockPaquete, $simboloToken, $curren, $stockTotal, $paquete->niveles];
    }

    public function crearCookies()
    {
        Cookie::queue('cookiePrueba', true, 60 * 24 * 7);
    }

    public function autoLogInBedesk($userId)
    {
        $id = Crypt::decrypt($userId);
        $usuario = User::find($id);
        if ($usuario != null) {
            Auth::loginUsingId($usuario->id);

            return redirect(route('home'));
        } else {
            return redirect(route('log-out'));
        }
    }

    public function guardarBloqueados(Request $request)
    {
        $page = MainPage::findOrFail($request->page_id);
        $page->blocked = $request->blocked;
        $page->save();

        return back()->with('success', ___('Ajustes guardados'));
    }

    public function loginAsAdmin()
    {
        if (session('adminId')) {
            Auth::loginUsingId(session('adminId'));

            return redirect('/admin/users/user');
        } else {
            return back();
        }
    }

    public function statusServices()
    {
        $servicios = StatusService::all();

        return view('admin.statusServices.index', compact('servicios'));
    }

    public function statusServicesPublic(){
        $servicios = StatusService::select('service', 'status', 'color', 'updated_at')->get();
        return view('admin.statusServices.public',compact('servicios'));
    }
}
