<?php

namespace App\Http\Controllers\Livewire;

use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class RegisterFormComponent extends Component
{
    public $check_users;

    public $name;

    public $username;

    public $email;

    public $mobile;

    public $mobileHelper;

    public $password;

    public $password_confirmation;

    public $terms;

    public $editando = false;

    public $validacion = 0;

    protected $listeners = ['sendNumber' => 'setNumber'];

    protected $rules = [
        'username' => 'required|min:3|alpha_dash|max:20|unique:users,username',
        'name' => 'required|string|min:3|max:100|regex:/^[a-zA-Z ]*$/',
        'email' => 'required|string|max:255|unique:users,email|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix|email_banned|not_regex:/\+/',
        'mobile' => 'required|min:8|unique:users,mobile',
        'password' => 'required|min:8|max:50|not_regex: /"`,:;<>=/|not_regex:/\s/|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@#$%^&*+]).*$/',
        'password_confirmation' => 'required|same:password',
        'terms' => 'accepted',
    ];

    protected $messages = [
        'user.alpha_dash' => 'username must not contain blank spaces or strange characters.',
        'user.name' => 'Your name must not contain foreign characters or numbers.',
        'email.email' => 'The Email Address format is not valid.',
        'email.email_banned' => 'El dominio del correo que ingresaste está bloqueado.',
        'email.not_regex' => 'El signo "+" no esta permitido.',
        'password.regex' => 'Your password must have all the security parameters complete and not have blank spaces.',
        'terms.accepted' => 'You must accept the terms and conditions to continue.',
    ];

    protected $validationAttributes = [
        'username' => 'username',
        'name' => 'name of user',
        'email' => 'email address',
        'password' => 'password of user',
    ];

    public function mount($check_users)
    {
        $this->check_users = $check_users;
    }

    public function resetInputs()
    {
        $this->editando = false;
        $this->reset('username', 'name', 'email', 'mobile', 'password');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function setNumber($number)
    {
        $this->mobile = $number[0];
        $this->mobileHelper = $number[1];
        $validator = Validator::make(
            ['mobile' => $number[0]],
            [
                'mobile' => 'required|min:8|unique:users,mobile',
            ],
            [
                'mobile.min' => ___('The number is not valid.'),
                'mobile.required' => ___('The mobile field is required.'),
                'mobile.unique' => ___('The mobile field value is already in use.'),
            ]
        );
        if ($validator->fails()) {
            $this->emit('get-message', $validator->errors()->messages()['mobile'][0]);
        } else {
            $this->emit('get-message', 'valid');
        }
    }

    public function validar()
    {
        $validateData = $this->validate();
    }

    public function enviarFormulario()
    {
        $this->emit('enviar', [$this->name, $this->username, $this->email, $this->password, csrf_token()]);
    }

    public function render()
    {
        $validator = Validator::make(
            [
                'username' => $this->username,
                'name' => $this->name,
                'email' => $this->email,
                'mobile' => $this->mobile,
                'password' => $this->password,
                'password_confirmation' => $this->password_confirmation,
                'terms' => $this->terms,
            ],
            [
                'username' => 'required|min:3|alpha_dash|max:20|unique:users,username',
                'name' => 'required|string|min:3|max:100|regex:/^[a-zA-Z ]*$/',
                'email' => 'required|string|max:255|unique:users,email|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix|email_banned|not_regex:/\+/',
                'mobile' => 'required|min:8|unique:users,mobile',
                'password' => 'required|min:8|max:50|not_regex: /"`,:;<>=/|not_regex:/\s/|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@#$%^&*+]).*$/',
                'password_confirmation' => 'required|same:password',
                'terms' => 'accepted',
            ],
        );
        if ($validator->fails()) {
            $this->validacion = 0;
        } else {
            $this->validacion = 1;
        }

        return view('livewire.register-form-component');
    }
}
