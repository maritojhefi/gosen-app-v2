<?php

namespace App\Http\Controllers\Livewire\User;

use App\Helpers\GosenHelper;
use App\Models\Notification;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class NotificationsListComponent extends Component
{
    use WithPagination;

    public $todos;

    public $filtrar = 'todos';

    public $filtroTransacciones = 'todos';

    public $trans;

    protected $queryString = ['filtrar'];

    protected $paginationTheme = 'bootstrap';

    protected $listeners = ['echo:actualizar-notificacion,NotificationEvent' => 'render', 'borrarNotificaciones' => 'borrarNotificaciones'];

    public function updatingFiltrar()
    {
        $this->resetPage();
    }

    public function selectTrans(Transaction $trans)
    {
        $trans->fecha = Carbon::parse($trans->created_at)->format('d-m-Y H:i:s');
        $this->emit('modal-data', $trans);
    }

    public function leerNotificacion($idNotificacion)
    {
        GosenHelper::leerNotificacion($idNotificacion, $this->todos);
    }

    public function redirigir($idNotificacion)
    {
        $respuesta = GosenHelper::leerNotificacion($idNotificacion, $this->todos);
        // redirect($respuesta[1]);
    }

    public function eliminarNotificacion($idNotificacion)
    {
        $respuesta = GosenHelper::eliminarNotificacion($idNotificacion, $this->todos);
        if ($respuesta == false) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal, intenta de nuevo'),
            ]);
        } else {
            $this->todos = $respuesta[0];
            $this->dispatchBrowserEvent('alert', [
                'type' => 'success',
                'message' => ___('Eliminaste la notificación'),
            ]);
        }
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function render()
    {
        $transacciones = [];
        $notificaciones = collect();
        $this->todos = Notification::where('user_id', auth()->user()->id)->first();
        if ($this->todos) {
            $notificaciones = json_decode($this->todos->messages);
            $notificaciones = collect($notificaciones);
            $notificaciones = $notificaciones->sortByDesc('created_at');
            if ($this->filtrar != 'todos') {
                if ($this->filtrar == 'transacciones') {
                    if ($this->filtroTransacciones == 'todos') {
                        $notificaciones = Transaction::where('user', auth()->user()->id)->where('tnx_type', 'purchase')->orderBy('created_at', 'desc')->get();
                    } else {
                        $notificaciones = Transaction::where('user', auth()->user()->id)->where('tnx_type', 'purchase')->where('status', $this->filtroTransacciones)->orderBy('created_at', 'desc')->get();
                    }
                } else {
                    $notificaciones = collect($notificaciones);
                    $notificaciones = $this->paginate($notificaciones->where('tag', $this->filtrar));
                }
            } else {
                $notificaciones = $this->paginate(collect($notificaciones));
            }
        }
        if ($this->filtroTransacciones == 'todos') {
            $transacciones = Transaction::where('user', auth()->user()->id)->where('tnx_type', 'purchase')->orderBy('created_at', 'desc')->get();
        } else {
            $transacciones = Transaction::where('user', auth()->user()->id)->where('tnx_type', 'purchase')->where('status', $this->filtroTransacciones)->orderBy('created_at', 'desc')->get();
        }

        return view('livewire.user.notifications-list-component', compact('notificaciones', 'transacciones'))
            ->extends('disruptive.disruptive-layout')
            ->section('content');
    }

    public function borrarNotificaciones()
    {
        if ($this->todos) {
            $notificaciones = json_decode($this->todos->messages);
            $error = false;
            DB::beginTransaction();
            foreach ($notificaciones as $not) {
                $respuesta = GosenHelper::eliminarNotificacion($not->id, $this->todos);
                if (! $respuesta) {
                    $error = true;
                    break;
                }
            }
            if ($error) {
                DB::rollBack();
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'warning',
                    'message' => ___('Algo salio mal'),
                ]);
            } else {
                DB::commit();
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'success',
                    'message' => ___('Todas tus notificaciones fueron eliminadas'),
                ]);
            }
        } else {
            $notificaciones = collect();
        }
    }
}
