<?php

namespace App\Http\Controllers\Livewire\User;

use App\Models\User;
use Livewire\Component;

class ContadoresComponent extends Component
{
    protected $listeners = ['echo:refresh-users,RefreshUsersQuantityEvent' => 'render'];

    public function render()
    {
        $userActual = User::find(auth()->user()->id);
        $usuariosDespues = User::where('email_verified_at', '>', $userActual->email_verified_at)->get();
        $allUsers = User::where('email_verified_at', '!=', null)->get();

        return view('livewire.user.contadores-component', compact('allUsers', 'usuariosDespues'));
    }
}
