<?php

namespace App\Http\Controllers\Livewire\User;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class NewsModalComponent extends Component
{
    public $mostrar;

    public $noticias;

    public $cantidadTexto = 100;

    protected $listeners = ['reducir' => 'reducir', 'aumentar' => 'aumentar'];

    public function mostrarTodo($id)
    {
        $this->mostrar = $id;
    }

    public function reducir()
    {
        $this->cantidadTexto = 100;
    }

    public function aumentar()
    {
        $this->cantidadTexto = 500;
    }

    public function render()
    {
        $user = User::find(auth()->user()->id);
        $resultado = $user->noticias->where('fechaFin', '>', date('Y-m-d'))->where('fechaInicio', '<', date('Y-m-d'));
        if ($resultado->count() > 0) {
            $this->noticias = $resultado;
        } else {
            $this->noticias = null;
            $this->emit('cerrar');
        }

        return view('livewire.user.news-modal-component', compact('user'));
    }

    public function destroyNotice($id)
    {
        DB::table('new_user')->where('id', '=', $id)->delete();
    }
}
