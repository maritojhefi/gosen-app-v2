<?php

namespace App\Http\Controllers\Livewire;

use App\Charts\UserChart;
use App\Helpers\GosenHelper;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class GraficaAsociadosComponent extends Component
{
    public $checkbox = 'dias';

    public function render()
    {
        $user = Auth::user();
        $referidos = GosenHelper::allReferral([$user->id]);
        $usersChart = new UserChart;
        $arrayColores = ['rgb(67, 35, 124)', 'rgb(46, 134, 171)', 'rgb(241, 143, 1)', 'rgb(199, 62, 29)', 'rgb(232, 100, 234)', 'rgb(255, 195, 0)', 'rgb(27, 235, 210)'];
        switch ($this->checkbox) {
            case 'dias':
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $array = [];
                    for ($i = 6; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($array, $usuarios);
                    }

                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ').$nivel + 1, 'bar', $array)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                }
                break;
            case 'semanas':
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $array = [];
                    for ($i = 6; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($array, $usuarios);
                    }

                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ').$nivel + 1, 'bar', $array)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                }
                break;
            case 'mes':
                // code...
                break;
            default:
                // code...
                break;
        }

        return view('livewire.grafica-asociados-component', compact('usersChart'));
    }
}
