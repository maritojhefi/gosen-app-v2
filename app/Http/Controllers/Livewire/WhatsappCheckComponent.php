<?php

namespace App\Http\Controllers\Livewire;

use App\Helpers\GosenHelper;
use App\Helpers\TokenCalculate as TC;
use App\Helpers\WhatsappAPIHelper;
use App\Models\BonusReferral;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class WhatsappCheckComponent extends Component
{
    public $user;

    protected $listeners = ['enviarCodigo' => 'sendCode', 'verificarCodigo' => 'verifyCode', 'numberSession' => 'numberSession'];

    public function sendCode($numero)
    {
        if (auth()->user()->timer_whatsapp_reset != null && auth()->user()->whatsapp_status == 1) {
            $hace5Min = Carbon::parse(auth()->user()->timer_whatsapp_reset)->addMinutes(5)->format('Y-m-d H:i:s');
            if ($hace5Min < Carbon::now()) {
                $validator = Validator::make(
                    ['mobile' => $numero],
                    ['mobile' => 'required|min:8|unique:users,mobile,'.$this->user->id],
                    [
                        'mobile.min' => ___('This field must contain at least 8 characters.'),
                        'mobile.required' => ___('This field is required.'),
                        'mobile.unique' => ___('The mobile is already busy.'),
                    ]
                );
                if ($validator->fails()) {
                    $this->emit('codigoEnviado', $validator->errors()->messages()['mobile'][0]);
                } else {
                    $this->user->mobile_aux = $numero;
                    $random = rand(111111, 999999);
                    WhatsappAPIHelper::enviarTemplate(
                        'disruptive_codigo_verificacion',
                        [$random],
                        $numero,
                        'es'
                    );
                    $this->user->whatsapp_code = $random;
                    $this->user->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => ___('Codigo enviado exitosamente'),
                    ]);
                    $this->emit('codigoEnviado', $numero);
                }
            } else {
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'error',
                    'message' => ___('Debes esperar 5 minutos para actualizar tu número de whatsapp'),
                ]);
            }
        } else {
            $validator = Validator::make(
                ['mobile' => $numero],
                ['mobile' => 'required|min:8|unique:users,mobile,'.$this->user->id],
                [
                    'mobile.min' => ___('This field must contain at least 8 characters.'),
                    'mobile.required' => ___('This field is required.'),
                    'mobile.unique' => ___('The mobile is already busy.'),
                ]
            );
            if ($validator->fails()) {
                $this->emit('codigoEnviado', $validator->errors()->messages()['mobile'][0]);
            } else {
                $this->user->mobile_aux = $numero;
                $random = rand(111111, 999999);
                WhatsappAPIHelper::enviarTemplate(
                    'disruptive_codigo_verificacion',
                    [$random],
                    $numero,
                    'es'
                );
                $this->user->whatsapp_code = $random;
                $this->user->save();
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'success',
                    'message' => ___('Codigo enviado exitosamente'),
                ]);
                $this->emit('codigoEnviado', $numero);
            }
        }
    }

    public function verifyCode($codigo)
    {
        $transactionWhatsapp = Transaction::where([['tnx_type', 'gift'], ['user', $this->user->id], ['details', 'Whatsapp verification finished']])->first();

        try {
            DB::beginTransaction();
            if ($this->user->whatsapp_code == $codigo) {
                if (! $transactionWhatsapp) {
                    $tc = new TC();
                    $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
                    // $bonusReferral = BonusReferral::where('user_id', $this->user->referral)->first();
                    $trans = Transaction::create([
                        'tokens' => 5,
                        'total_tokens' => 5,
                        'user' => $this->user->id,
                        'stage' => active_stage()->id,
                        'tnx_type' => 'gift',
                        'payment_method' => 'system',
                        'base_currency' => base_currency(),
                        'base_currency_rate' => $base_currency_rate,
                        'checked_by' => '{"name":"System","id":"0"}',
                        'added_by' => 'SYS-00000',
                        'checked_time' => now(),
                        'tnx_time' => now(),
                        'details' => 'Whatsapp verification finished',
                        'tnx_id' => 1,
                        // 'regalo_tipo_id' => $bonusReferral->regalo_tipo_id
                    ]);
                    $trans->tnx_id = 'GIFT'.sprintf('%06s', $trans->id);
                    $trans->save();
                    User::find($this->user->id)->increment('gift_bonus', 5);
                }
                $destinatario = $this->user->mobile_aux;
                $this->user->whatsapp_status = true;
                $this->user->whatsapp_code = null;
                $this->user->mobile = $this->user->mobile_aux;
                $this->user->timer_whatsapp_reset = Carbon::now();
                $this->user->mobile_aux = null;
                $this->user->whatsapp_verified_at = Carbon::now();
                $this->user->save();

                if (! $transactionWhatsapp) {
                    // WhatsappAPIHelper::enviarTemplateButton(
                    //     'disruptive_verificacion_finalizada',
                    //     ['user/profile/1'],
                    //     $destinatario,
                    //     'es'
                    // );
                    switch ($this->user->lang) {
                        case 'pr':
                            $lang = 'pt_BR';
                            break;
                        default:
                            $lang = $this->user->lang;
                            break;
                    }
                    WhatsappAPIHelper::enviarTemplate('dis_verifie', [$this->user->name], $destinatario, $lang);
                    GosenHelper::createNotification(
                        $this->user->id,
                        GosenHelper::contenidoNotificacion('whatsapp_verificacion_finalizada')
                    );
                } else {
                    GosenHelper::createNotification(
                        $this->user->id,
                        GosenHelper::contenidoNotificacion('whatsapp_actualizado_verificado')
                    );
                }

                $this->dispatchBrowserEvent('alert', [
                    'type' => 'success',
                    'message' => ___('Codigo verificado exitosamente'),
                ]);
                $this->emit('codigoVerificado', 'valid', $this->user->mobile);
            } else {
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'error',
                    'message' => ___('El codigo no es correcto'),
                ]);
                $this->emit('codigoVerificado', 'invalid');
            }
            DB::commit();
        } catch (\Throwable $th) {
            dd($th->getMessage());
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal, intente nuevamente'),
            ]);
        }
    }

    public function numberSession()
    {
        $numero = $this->user->mobile_aux;
        $this->emit('numeroSesionActualizado', $numero);
    }

    public function render()
    {
        $this->user = User::find(auth()->id());

        return view('livewire.whatsapp-check-component');
    }
}
