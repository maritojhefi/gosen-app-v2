<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Models\WhatsappCorreo;
use Livewire\Component;
use Livewire\WithPagination;

class WhatsappCorreoListComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $editing;

    public $diaValor;

    public $search;

    public $estadoPersonal = [];

    public $horaValor;

    public $editingHora;

    protected $listeners = ['asignarHora' => 'asignarHora', 'asignarDia' => 'asignarDia', 'borrarCorreo' => 'borrarCorreo', 'actualizarEstadoPersonal' => 'actualizarEstadoPersonal', 'copíarSlug' => 'copíarSlug'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function editValue($correo)
    {
        $this->editing = $correo;
    }

    public function editHora($hora)
    {
        $this->editingHora = $hora;
    }

    public function resetInputs()
    {
        $this->reset('search');
    }

    public function saveValue()
    {
        $this->editing = false;
    }

    public function copíarSlug($slug)
    {
        $this->emit('copiarSlugCorreoDisruptive', $slug);
    }

    public function actualizarEstadoPersonal(WhatsappCorreo $itemId, $checked)
    {
        try {
            if ($itemId) {
                if ($checked == true) {
                    $itemId->status = 1;
                    $itemId->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'info',
                        'message' => ___('Se cambio el estado del correo a activo con exito'),
                    ]);
                } else {
                    $itemId->status = 0;
                    $itemId->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'info',
                        'message' => ___('Se cambio el estado del correo a inactivo'),
                    ]);
                }
            } else {
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'error',
                    'message' => ___('No se pudo cambiar el estado del correo intente nuevamente'),
                ]);
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal intenta nuevamente'),
            ]);
        }
    }

    public function guardarDia(WhatsappCorreo $correo)
    {
        $valor = (int) $this->diaValor;
        try {
            $correo = WhatsappCorreo::find($correo->id);
            if ($correo) {
                try {
                    $correo->number_days = $valor;
                    $correo->save();
                    $this->editing = ' ';
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => ___('Se asigno el valor dia al correo con exito'),
                    ]);
                } catch (\Throwable $th) {
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'info',
                        'message' => ___('No se pudo asignar el valor de dias al correo intente nuevamente'),
                    ]);
                }
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('El correo no esta disponible'),
            ]);
        }
    }

    public function cancelarGuardarDia()
    {
        $this->editing = false;
    }

    public function asignarDia(WhatsappCorreo $correo)
    {
        try {
            if ($correo) {
                $this->emit('abrirModal', $correo);
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal intentelo nuevamente'),
            ]);
        }
    }

    public function guardarHora(WhatsappCorreo $correo)
    {
        $valor = $this->horaValor;
        try {
            $correo = WhatsappCorreo::find($correo->id);
            if ($correo) {
                try {
                    $correo->time = $valor;
                    $correo->save();
                    $this->editingHora = ' ';
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => ___('Se asigno la hora de entrega al correo con exito'),
                    ]);
                } catch (\Throwable $th) {
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'info',
                        'message' => ___('No se pudo asignar la hora al correo intente nuevamente'),
                    ]);
                }
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('El correo no esta disponible'),
            ]);
        }
    }

    public function cancelarGuardarHora()
    {
        $this->editingHora = false;
    }

    public function borrarCorreo(WhatsappCorreo $correo)
    {
        try {
            if ($correo) {
                $correo->delete();
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'info',
                    'message' => ___('El correo se borro exitosamente'),
                ]);
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal intentelo nuevamente'),
            ]);
        }
    }

    public function render()
    {
        $query = WhatsappCorreo::orderBy('created_at', 'desc');
        if (isset($this->search)) {
            $query = $query->where(function ($q) {
                $q->where('number_days', 'like', '%'.$this->search.'%')
                    ->orWhere('type_mail', 'like', '%'.$this->search.'%');
            });
        }
        $correos = $query->paginate(10);

        return view('livewire.admin.whatsapp-correo-list-component', compact('correos'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
