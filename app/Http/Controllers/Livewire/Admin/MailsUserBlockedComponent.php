<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Helpers\GosenHelper;
use App\Models\EmailBlocked;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class MailsUserBlockedComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;

    public $correo;

    public $tipo;

    public $descripcion;

    public $estado = true;

    protected $listeners = ['eliminarDominio' => 'eliminarDominio', 'actualizarEstadoCorreo' => 'actualizarEstadoCorreo', 'crearCorreoBloqueado' => 'crearCorreoBloqueado'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetInputs()
    {
        $this->reset('search', 'correo', 'tipo', 'descripcion');
    }

    public function crearCorreoBloqueado($correo, $tipo, $descripcion)
    {
        $error = GosenHelper::validar([
            'correo' => [
                $correo,
                'required|string|min:3|regex:/@/|unique:email_blockeds,correo',
                ['required' => ___('El campo de dominio/correo debe tener un valor'), 'min' => ___('El campo de dominio/correo debe tener un tamaño minimo de 3 caracteres'), 'regex' => ___('El campo de dominio/correo debe contener un @'), 'unique' => ___('El dominio/correo "'.$correo.'" a bloquear ya se encuentra registrado')],
            ],
            'tipo' => [
                $tipo,
                'required|string|min:3',
                ['required' => ___('El campo de tipo debe tener un valor'), 'min' => ___('El campo de tipo debe tener un tamaño minimo de 3 caracteres')],
            ],
            'descripcion' => [
                $descripcion,
                'string',
                [],
            ],
        ]);
        if (! $error) {
            if ($tipo == 'default') {
                $modal = false;
                $toast = [
                    'icon' => 'error',
                    'title' => ___('Se debe seleccionar un tipo para poder continuar'),
                ];
            } else {
                EmailBlocked::create([
                    'correo' => $correo,
                    'tipo' => $tipo,
                    'descripcion' => $descripcion,
                    'estado' => true,
                ]);
                //actualizacion de la cache para los registros de en la cache de correos bloqueados
                actualizarCacheCorreosBloqueados();
                $modal = true;
                $toast = [
                    'icon' => 'success',
                    'title' => ___('El dominio/correo bloqueado fue creado y bloqueado'),
                ];
            }
        } else {
            $modal = false;
            $toast = [
                'icon' => 'error',
                'title' => $error,
            ];
        }
        $this->emit('cerrarModal', $modal);
        $this->emit('toastDispatch', $toast);
    }

    public function actualizarEstadoCorreo(EmailBlocked $itemId, $checked)
    {
        try {
            if ($itemId) {
                if ($checked == true) {
                    $itemId->estado = 1;
                    $itemId->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => ___('El dominio'.' "'.$itemId->correo.'" '.' se agrego a la lista de bloqueados para el registro'),
                    ]);
                } else {
                    $itemId->estado = 0;
                    $itemId->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'warning',
                        'message' => ___('El dominio'.' "'.$itemId->correo.'" '.' se quito de la lista de bloqueados para el registro'),
                    ]);
                }
                //actualizacion de la cache para los registros de en la cache de correos bloqueados
                actualizarCacheCorreosBloqueados();
            // dd(Cache::get('email_bloqueados'));
            } else {
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'error',
                    'message' => ___('No se pudo cambiar el estado intente nuevamente'),
                ]);
            }
        } catch (\Throwable $th) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Algo salio mal intenta nuevamente'),
            ]);
        }
    }

    public function eliminarDominio(EmailBlocked $email)
    {
        $email->delete();
        //actualizacion de la cache para los registros de en la cache de correos bloqueados
        actualizarCacheCorreosBloqueados();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'info',
            'message' => ___('Se elimino el dominio/correo con exito'),
        ]);
    }

    public function render()
    {
        // dd(Cache::get('emails_cache'));
        $query = EmailBlocked::orderBy('created_at', 'desc');
        if (isset($this->search)) {
            $query = $query->where(function ($q) {
                $q->where('correo', 'like', '%'.$this->search.'%')
                    ->orWhere('id', 'like', '%'.$this->search.'%')
                    ->orWhere('tipo', 'like', '%'.$this->search.'%')
                    ->orWhere('descripcion', 'like', '%'.$this->search.'%');
            });
        }
        $emails = $query->paginate(6);

        return view('livewire.admin.mails-user-blocked-component', compact('emails'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
