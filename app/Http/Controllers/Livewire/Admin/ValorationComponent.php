<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Models\Valoration;
use Livewire\Component;
use Livewire\WithPagination;

class ValorationComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $comentario;

    public $puntaje;

    public $estado = false;

    public $idUser;

    public $listado = 0;

    public $columns = [
        'id',
        'idUser',
        'puntaje',
        'created_at',
    ];

    public $sortColumn = 'puntaje';

    public $sortDirection = 'desc';

    protected $rules = [
        'comentario' => 'required|max:20',
        'puntaje' => 'required|numeric',
        'estado' => 'required|numeric',
        'idUser' => 'required',
    ];

    public function resetInputs()
    {
        $this->reset('comentario', 'puntaje', 'estado', 'idUser');
    }

    public function render()
    {
        $valorations = Valoration::orderBy($this->sortColumn, $this->sortDirection);
        if ($this->listado == 1) {
            $valorations = $valorations->where('estado', false);
        }
        if ($this->listado == 2) {
            $valorations = $valorations->where('estado', true);
        }

        $valorations = $valorations->paginate(6);
        // dd($valorations);
        return view('livewire.admin.valoration-component', compact('valorations'))
            ->extends('layouts.admin')
            ->section('content');
    }

    public function cambiarListado()
    {
        switch ($this->listado) {
            case 0:
                $this->listado = 1;
                $this->resetPage();
                break;
            case 1:
                $this->listado = 2;
                $this->resetPage();
                break;
            case 2:
                $this->listado = 0;
                $this->resetPage();
                break;
        }
    }

    public function updatingListado()
    {
        $this->resetPage();
    }

    public function cambiarEstado(Valoration $valoration)
    {
        if ($valoration->estado == false) {
            $valoration->estado = true;
            $valoration->save();
            $this->dispatchBrowserEvent('alert', [
                'type' => 'success',
                'message' => 'Feedback status was changed to published',
            ]);
        } else {
            $valoration->estado = false;
            $valoration->save();
            $this->dispatchBrowserEvent('alert', [
                'type' => 'warning',
                'message' => 'Feedback status was changed to not published',
            ]);
        }
    }

    public function ordenar($column)
    {
        $this->sortColumn = $column;
        $this->sortDirection = $this->sortDirection == 'asc' ? 'desc' : 'asc';

        if ($this->sortDirection == 'asc') {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'info',
                'message' => 'Columns sorted in ascending order',
            ]);
        } else {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'info',
                'message' => 'Columns sorted in descending order',
            ]);
        }
    }
}
