<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Models\Paquete;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PackagesCrudComponent extends Component
{
    use WithFileUploads;

    public $nombre, $tokens, $valor_usd, $descuento, $stock, $type_user, $foto, $estado_fecha_limite,  $staking_info, $porcentaje_info, $meses_info;


    public $editando = false, $paquete;
    protected $listeners = ['updateBonusPackage' => 'updateBonusPackage'];



    public function cambiarEstado(Paquete $paquete)
    {
        if ($paquete->estado) {
            $paquete->estado = false;
        } else {
            $paquete->estado = true;
        }
        $paquete->save();

        $this->dispatchBrowserEvent('alert', [
            'type' => 'success',
            'message' => 'Estado cambiado!',
        ]);
    }

    public function validar()
    {
        $reglas = $this->validate([
            'nombre' => 'required|string',
            'tokens' => 'required|numeric',
            'valor_usd' => 'required|numeric',
            'descuento' => 'required|numeric|min:0|max:100',
            'stock' => 'required|integer',
            'type_user' => 'required|integer',
            'foto' => 'nullable|mimes:jpg|max:10240',
            'staking_info' => 'nullable|string',
            'porcentaje_info' => 'required|numeric',
            'meses_info' => 'required|numeric|min:1',
        ]);

        return $reglas;
    }

    public function changeStatus(Paquete $paquete)
    {
        if ($paquete->estado_bono) {
            $paquete->estado_bono = false;
        } else {
            $paquete->estado_bono = true;
        }
        $paquete->save();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'success',
            'message' => 'El estado de este paquete fue actualizado correctamente',
        ]);
    }

    public function cambiarEstadoDiaVigencia(Paquete $paquete)
    {
        if ($paquete->estado_fecha_limite) {
            $paquete->estado_fecha_limite = false;
        } else {
            $paquete->estado_fecha_limite = true;
        }
        $paquete->save();
    }
    public function updateBonusPackage($porcentaje, $dias, $fecha, $diasvigencia, Paquete $paquete)

    {
        // dd($this->isChecked, $porcentaje, $dias, $fecha, $diasvigencia, $paquete);
        $validator = Validator::make(['porcentaje' => $porcentaje, 'dias' => $dias, 'fecha' => $fecha, 'diasvigencia' => $diasvigencia], [
            'porcentaje' => 'required|numeric',
            'dias' => 'required|integer',
            'fecha' => 'nullable|date',
            'diasvigencia' => 'nullable|integer'
        ]);

        if ($validator->fails()) {
            $this->emit('toast', ['mensaje' => ___('Existen errores en la validacion') . ': ' . ___($validator->errors()->first()), 'icono' => 'error']);
        } else {
            $paquete->fecha_limite_bono = $paquete->estado_fecha_limite == true ? $fecha : null;
            $paquete->dias_vigencia = $paquete->estado_fecha_limite == false ? $diasvigencia : null;
            $paquete->dias_bono = $dias;
            $paquete->porcentaje_bono = $porcentaje;
            $paquete->save();
            $this->emit('toast', ['mensaje' => ___('Paquete actualizado con exito'), 'icono' => 'success']);
        }
    }

    public function guardar()
    {
        $reglas = $this->validar();
        $nuevoPaquete = Paquete::create($reglas);
        // Si se ha subido una imagen, la guardamos y actualizamos el campo "foto"
        if ($this->foto) {
            $filename = $this->foto->hashName();
            $this->foto->storeAs('/imagenes/paquetes', $filename, 'gosendisk');

            // Abrir la imagen desde Storage
            $imagen = Image::make(Storage::disk('gosendisk')->get('imagenes/paquetes/' . $filename));
            // Ajustar la calidad y tamaño de la imagen
            $imagen->resize(1000, 1000, function ($constraint) {
                $constraint->upsize();
            })
                ->encode('jpg', 80);
            // Guardar la imagen optimizada en Storage
            Storage::disk('gosendisk')->put('imagenes/paquetes/' . $filename, $imagen);
            $nuevoPaquete->foto = $filename;
        } else {
            $nuevoPaquete->foto = null;
        }
        $total = Paquete::count();
        $nuevoPaquete->posicion = $total;
        $nuevoPaquete->staking_info = nl2br($this->staking_info);
        $nuevoPaquete->save();
        $this->reset();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'success',
            'message' => 'El paquete fue creado exitosamente!',
        ]);
    }

    public function actualizarTokens()
    {
        $this->tokens = round($this->valor_usd / (float) current_price('usd'), 3);
    }

    public function cancelarEdicion()
    {
        $this->editando = false;
        $this->reset();
    }

    public function editar(Paquete $paquete)
    {
        $this->editando = true;
        $this->paquete = $paquete;
        $this->paquete->staking_info = str_replace('<br />', '', $paquete->staking_info);
        $this->fill($paquete);
        $this->reset('foto');
    }

    public function actualizar()
    {
        $reglas = $this->validar();
        // Si se ha subido una imagen al editar, la guardamos y actualizamos el campo "foto"
        if ($this->foto) {
            $filename = $this->foto->hashName();
            // Si ya existe una foto, la borramos antes de guardar la nueva imagen
            if ($this->paquete->foto && Storage::disk('gosendisk')->exists('imagenes/paquetes/' . $this->paquete->foto)) {
                Storage::disk('gosendisk')->delete('imagenes/paquetes/' . $this->paquete->foto);
            }
            $this->foto->storeAs('/imagenes/paquetes', $filename, 'gosendisk');
            // Abrir la imagen desde Storage
            $imagen = Image::make(Storage::disk('gosendisk')->get('imagenes/paquetes/' . $filename));
            // Ajustar la calidad y tamaño de la imagen
            $imagen->resize(1000, 1000, function ($constraint) {
                $constraint->upsize();
            })
                ->encode('jpg', 80);
            // Guardar la imagen optimizada en Storage
            Storage::disk('gosendisk')->put('imagenes/paquetes/' . $filename, $imagen);
            $reglas['foto'] = $filename;
        } elseif ($this->paquete->foto) {
            // Si no se ha subido una imagen al editar pero ya existe una foto en la base de datos, mantenemos la foto actual en el campo "foto"
            $reglas['foto'] = $this->paquete->foto;
        } else {
            $reglas['foto'] = null;
        }
        $this->paquete->fill($reglas);
        $this->paquete->staking_info = nl2br($this->staking_info);
        $this->paquete->save();
        $this->reset();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'success',
            'message' => 'El paquete fue actualizado correctamente!',
        ]);
    }

    public function eliminar(Paquete $paquete)
    {
        $paquete->delete();
        $this->reset();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'success',
            'message' => 'El paquete fue eliminado exitosamente!',
        ]);
    }

    public function render()
    {
        $paquetes = Paquete::orderBy('posicion', 'asc')->get();
        return view('livewire.admin.packages-crud-component', compact('paquetes'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
