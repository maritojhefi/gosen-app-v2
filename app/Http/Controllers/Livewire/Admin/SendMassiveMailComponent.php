<?php

namespace App\Http\Controllers\Livewire\Admin;

use Livewire\Component;

class SendMassiveMailComponent extends Component
{
    public $title;

    public $footer;

    public $content;

    public function render()
    {
        return view('livewire.admin.send-massive-mail-component')
        ->extends('layouts.admin')
        ->section('content');
    }
}
