<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Exports\ContactUsersExport;
use App\Models\Language;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class WhatsappReportsComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    protected $listeners = ['excelUsers' => 'excelUsers'];

    public $search;

    public $usersExport;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function excelUsers($shortIdioma, $pais)
    {
        $users = User::where([['email_verified_at', '!=', null], ['whatsapp_status', 1]]);
        if ($pais == 'all') {
            if ($shortIdioma != []) {
                $usuarios = $users->whereIn('lang', $shortIdioma);
                $lang = Language::whereIn('code', $shortIdioma)->get();
            } else {
                $usuarios = $users;
                $lang = 'all';
            }
        } else {
            if ($shortIdioma != []) {
                $usuarios = $users->whereIn('lang', $shortIdioma);
                $lang = Language::whereIn('code', $shortIdioma)->get();
            } else {
            }
            $usuarios = $users->where('country', $pais);
            $lang = 'all';
        }
        $this->usersExport = $usuarios->get();
        ob_end_clean();
        ob_start();
        $date = Carbon::parse(Carbon::now())->format('Y-m-d');

        return Excel::download(new ContactUsersExport($this->usersExport, $lang), 'users-contact-'.(string) $date.'.xls');
    }

    public function render()
    {
        $query = User::where([['email_verified_at', '!=', null], ['whatsapp_status', 1]])->with('language');
        if (isset($this->search)) {
            $query = $query->where(function ($q) {
                $q->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('lang', 'like', '%'.$this->search.'%')
                    ->orWhere('id', 'like', '%'.$this->search.'%')
                    ->orWhere('email', 'like', '%'.$this->search.'%')
                    ->orWhere('country', 'like', '%'.$this->search.'%')
                    ->orWhereHas('language', function ($q) {
                        $q->where('label', 'like', '%'.$this->search.'%');
                    });
            });
        }
        $users = $query->paginate(9);

        $usersLenguaje = $query->select('lang')->groupBy('lang')->get();
        $usersCountry = User::where([['email_verified_at', '!=', null], ['whatsapp_status', 1]])->select('country')->groupBy('country')->get();

        return view('livewire.admin.whatsapp-reports-component', compact('users', 'usersLenguaje', 'usersCountry'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
