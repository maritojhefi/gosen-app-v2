<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Models\WhatsappConversacione;
use Livewire\Component;

class WhatsappConversationsComponent extends Component
{
    public function render()
    {
        $registros = WhatsappConversacione::all();
        $registrosOrdenadosDesc = $registros->sortByDesc('created_at');
        $conversaciones = $registrosOrdenadosDesc->groupBy('conversacion_id');

        return view('livewire.admin.whatsapp-conversations-component', compact('conversaciones'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
