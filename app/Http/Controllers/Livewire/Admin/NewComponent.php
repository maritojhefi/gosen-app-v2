<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Jobs\AddNewsToUserJob;
use App\Jobs\TranslateJob;
use App\Models\Imagene;
use App\Models\Noticia;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class NewComponent extends Component
{
    use WithPagination;
    use WithFileUploads;

    protected $paginationTheme = 'bootstrap';

    public $titulo;

    public $descripcion;

    public $fechaInicio;

    public $fechaFin;

    public $image = [];

    public $editando = false;

    public $listado = 0;

    public $mostrar;

    public $cantidadTexto = 100;

    public $noticia;

    // public $estados = ['all', 'ondate', 'expired', 'comming'];
    public $estadoActual = 'all';

    protected $listeners = ['reducir' => 'reducir', 'aumentar' => 'aumentar'];

    protected $rules = [
        'titulo' => 'required|max:50|min:5',
        'descripcion' => 'required|min:20',
        'fechaInicio' => 'required|before:fechaFin',
        'fechaFin' => 'required|date|after:fechaInicio',
        'image.*' => 'required|mimes:png,jpg,bmp,webp,gif,jpeg|max:10240',
    ];

    public function resetInputs()
    {
        $this->editando = false;
        $this->reset('titulo', 'descripcion', 'fechaInicio', 'fechaFin', 'image');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render()
    {
        $noticias = Noticia::orderBy('created_at', 'desc');

        if ($this->estadoActual == 'all') {
            // dd($this->estadoActual);
        } elseif ($this->estadoActual == 'ondate') {
            $noticias = $noticias->where('fechaInicio', '<=', date('Y-m-d'))->where('fechaFin', '>=', date('Y-m-d'));
        } elseif ($this->estadoActual == 'expired') {
            $noticias = $noticias->where('fechaFin', '<', date('Y-m-d'));
        //dd($this->estadoActual);
        } elseif ($this->estadoActual == 'comming') {
            $noticias = $noticias->where('fechaInicio', '>', date('Y-m-d'));
            //dd($this->estadoActual);
        }

        $noticias = $noticias->paginate(6);

        return view('livewire.admin.new-component', compact('noticias'))
            ->extends('layouts.admin')
            ->section('content');
    }

    public function cambiarListado()
    {
        switch ($this->estadoActual) {
            case 'all':
                $this->estadoActual = 'ondate';
                $this->resetPage();
                break;
            case 'ondate':
                $this->estadoActual = 'expired';
                $this->resetPage();
                break;
            case 'expired':
                $this->estadoActual = 'comming';
                $this->resetPage();
                break;

            case 'comming':
                $this->estadoActual = 'all';
                $this->resetPage();
                break;
        }
    }

    public function updatingListado()
    {
        $this->resetPage();
    }

    public function addNewsToUsers(Noticia $noticia)
    {
        $users = User::all()->where('role', '=', 'user');

        AddNewsToUserJob::dispatch($users, $noticia);
    }

    public function save()
    {
        if ($this->editando == true) {
            //dd($this->noticia);
            $this->update($this->noticia);
            if ($this->image && count($this->image) > 0) {
                $contador = 0;
                foreach ($this->image as $imagen) {
                    $filename = $contador.'-'.time().'.'.$imagen->extension();
                    //para blade
                    //$this->image->move(public_path('images'), $filename);
                    $imagen->storeAs('imagenes', $filename, 'gosendisk');
                    Imagene::create([
                        'new_id' => $this->noticia->id,
                        'nombre' => $filename,
                    ]);
                    $contador++;
                    //$producto->imagen = $filename;
                }
                $this->reset('image');
                $this->dispatchBrowserEvent('closeModal1');
            }
            $this->dispatchBrowserEvent('alert', [
                'type' => 'success',
                'message' => 'The news was updated successfully ',
            ]);
            $this->editando = false;
        } else {
            $this->validate();
            $array = $this->validate([
                'titulo' => 'required|max:50|min:5',
                'descripcion' => 'required|min:20',
                'fechaInicio' => 'required|before:fechaFin',
                'fechaFin' => 'required|date|after:fechaInicio',
            ]);
            // TranslateJob::dispatch($array);
            $noticia = Noticia::create([
                'titulo' => $this->titulo,
                'descripcion' => $this->descripcion,
                'fechaInicio' => $this->fechaInicio,
                'fechaFin' => $this->fechaFin,
            ]);
            $this->addNewsToUsers($noticia);
            $contador = 0;
            foreach ($this->image as $imagen) {
                $filename = $contador.'-'.time().'.'.$imagen->extension();
                //para blade
                //$this->image->move(public_path('images'), $filename);
                $imagen->storeAs('imagenes', $filename, 'gosendisk');
                Imagene::create([
                    'new_id' => $noticia->id,
                    'nombre' => $filename,
                ]);
                $contador++;
                //$producto->imagen = $filename;
            }
            $this->reset('image');
            $this->dispatchBrowserEvent('closeModal1');
            $this->dispatchBrowserEvent('alert', [
                'type' => 'success',
                'message' => 'The news was created successfully',
            ]);
            $this->reset();
        }
    }

    public function edit(Noticia $noticia)
    {
        $this->noticia = $noticia;
        $this->editando = true;
        $this->titulo = $noticia->titulo;
        $this->descripcion = $noticia->descripcion;
        $this->fechaInicio = $noticia->fechaInicio;
        $this->fechaFin = $noticia->fechaFin;
    }

    public function update(Noticia $noticia)
    {
        $noticia->titulo = $this->titulo;
        $noticia->descripcion = $this->descripcion;
        $noticia->fechaInicio = $this->fechaInicio;
        $noticia->fechaFin = $this->fechaFin;
        $noticia->image = $this->image;
        $noticia->save();
    }

    public function destroy(Noticia $noticia)
    {
        $fotos = $noticia->imagenes;
        if ($fotos->count() > 0) {
            foreach ($fotos as $imagen) {
                Storage::disk('gosendisk')->delete('imagenes/'.$imagen->nombre);
                $imagen->delete();
            }
        }

        $noticia->delete();
        $this->dispatchBrowserEvent('alert', [
            'type' => 'info',
            'message' => 'The news was successfully removed',
        ]);
    }

    public function destroyimage(Imagene $imagen)
    {
        Storage::disk('gosendisk')->delete('imagenes/'.$imagen->nombre);
        $imagen->delete();

        $this->dispatchBrowserEvent('alert', [
            'type' => 'info',
            'message' => 'The image was successfully deleted!',
        ]);
    }

    public function mostrarTodo($id)
    {
        $this->mostrar = $id;
    }

    public function reducir()
    {
        $this->cantidadTexto = 10;
    }

    public function aumentar()
    {
        $this->cantidadTexto = 70;
    }
}
