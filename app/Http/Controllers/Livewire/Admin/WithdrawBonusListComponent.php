<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Exports\WithdrawBonusDollarExport;
use App\Models\BonusDolar;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class WithdrawBonusListComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $transaction;

    public $opcionesFiltro;

    public $estado = null;

    public $fechaInicio = null;

    public $fechaFin = null;

    public $reporte;

    public function mount($transaction)
    {
        $this->transaction = $transaction;
        $this->opcionesFiltro = BonusDolar::select('status')->distinct()->pluck('status');
    }

    public function updatingEstado()
    {
        $this->resetPage();
    }

    public function updatingFechaFin()
    {
        $this->resetPage();
    }

    public function updatingFechaInicio()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = BonusDolar::query()->where('type', BonusDolar::TYPE1);
        $this->reporte = $query->orderBy('created_at', 'desc')->get();
        if ($this->fechaInicio) {
            $query->where('type', BonusDolar::TYPE1)->whereDate('created_at', '>=', $this->fechaInicio);
            $this->reporte = $query->orderBy('created_at', 'desc')->get();
        }
        if ($this->fechaFin) {
            $query->where('type', BonusDolar::TYPE1)->whereDate('created_at', '<=', $this->fechaFin);
            $this->reporte = $query->orderBy('created_at', 'desc')->get();
        }
        if ($this->estado) {
            if ($this->estado == 'aprobadosusuario') {
                $query->where('type', BonusDolar::TYPE1)->where([['status', BonusDolar::STATUS1], ['approved_bonus', 1]]);
                $this->reporte = $query->orderBy('created_at', 'desc')->get();
            } else {
                $query->where('type', BonusDolar::TYPE1)->where('status', $this->estado);
                $this->reporte = $query->orderBy('created_at', 'desc')->get();
            }
        }
        $estadoActual = $this->estado;
        $registros = $query->orderBy('created_at', 'desc')->paginate(6);

        return view('livewire.admin.withdraw-bonus-list-component', compact('registros', 'query', 'estadoActual'))
            ->extends('layouts.admin')
            ->section('content');
    }

    public function exportarWithdrawExcel()
    {
        $data = $this->reporte;
        $periodo1 = $this->fechaInicio;
        $periodo2 = $this->fechaFin;
        $estadoReporte = $this->estado;
        ob_end_clean();
        ob_start();

        return Excel::download(new WithdrawBonusDollarExport($data, $periodo1, $periodo2, $estadoReporte), 'withdraw-bonus-dollars.xls');
    }
}
