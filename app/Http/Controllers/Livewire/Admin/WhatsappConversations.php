<?php

namespace App\Http\Controllers\Livewire\Admin;

use Livewire\Component;

class WhatsappConversations extends Component
{
    public function render()
    {
        return view('livewire.admin.whatsapp-conversations');
    }
}
