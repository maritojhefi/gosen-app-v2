<?php

namespace App\Http\Controllers\Livewire\Admin;

use App\Helpers\GosenHelper;
use App\Models\Language;
use App\Models\Menu;
use App\Models\User;
use App\Models\WhatsappCorreo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class WhatsappCorreoComponent extends Component
{
    public $header;

    public $content;

    public $footer;

    public $lang;

    public $design = true;

    protected $listeners = ['guardarCorreo' => 'guardarCorreo'];

    public function guardarCorreo($header, $content, $footer, $design, $lang)
    {
        // dd($header, $content, $footer, $design, $lang);
        if ($lang == 0) {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => ___('Debe seleccionar un lenguage para el correo'),
            ]);
        } else {
            if ($design == true) {
                $validator = Validator::make(
                    ['header' => $header, 'content' => $content, 'footer' => $footer, 'lang' => $lang],
                    ['header' => 'required|string', 'content' => 'required|string', 'footer' => 'required|string', 'lang' => 'required'],
                    ['header.required' => ___('This field title is required'), 'content.required' => ___('This field content is required'), 'footer.required' => ___('This field footer is required'), 'lang.required' => ___('Debe escoger un lenguage para registrar el correo')]
                );
            } else {
                $validator = Validator::make(
                    ['content' => $content, 'lang' => $lang],
                    ['content' => 'required|string', 'lang' => 'required'],
                    ['content.required' => ___('This field content is required.'), 'lang.required' => ___('Debe escoger un lenguage para registrar el correo')]
                );
            }
            if ($validator->fails()) {
                $this->emit('errores', $validator->errors()->messages());
            } else {
                DB::beginTransaction();
                $correo = WhatsappCorreo::create([
                    'header' => $header == null ? null : $header,
                    'content' => GosenHelper::verifyImgTrumbowyg($content),
                    'footer' => $footer == null ? null : $footer,
                    'type_mail' => $design == true ? 'design' : 'simple',
                ]);
                $correo->lang = $lang;
                $palabras = GosenHelper::filtrarCaracteres(strtolower($header));
                $slug = strtolower(site_info('name')).implode('_', array_slice($palabras, 0, 2)).'_'.$correo->id;
                $cantidadMinima = strlen(site_info('name')) + 8;
                $validatedData = Validator::make(
                    ['slug' => $slug],
                    ['slug' => 'required|string|min:'.$cantidadMinima.'|unique:whatsapp_correos,slug']
                );
                if ($validatedData->fails()) {
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'error',
                        'message' => ___($validatedData->errors()->first()),
                    ]);
                    DB::rollBack();
                } else {
                    $correo->slug = $slug;
                    $correo->save();
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => ___('Se creo el correo con exito, vaya a los registros de los correos para organizar la ubicacion del mismo'),
                    ]);
                    DB::commit();
                }
            }
        }
    }

    public function render()
    {
        $users = User::where('email_verified_at', '!=', null)->get();
        $tiposUsuario = Menu::all();
        $lenguages = Language::where('status', true)->get();

        return view('livewire.admin.whatsapp-correo-component', compact('users', 'tiposUsuario', 'lenguages'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
