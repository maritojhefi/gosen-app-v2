<?php

namespace App\Http\Controllers\Livewire;

use App\Helpers\GosenHelper;
use App\Models\Notification;
use Livewire\Component;

class NotificationsComponent extends Component
{
    public $notificaciones;

    protected $listeners = ['echo:actualizar-notificacion,NotificationEvent' => 'render'];

    public function clearAll()
    {
        if (isset($this->notificaciones->messages)) {
            $mensajes = (array) json_decode($this->notificaciones->messages);
            foreach ($mensajes as $notify) {
                if (isset($notify->estado_header)) {
                    $notify->estado_header = false;
                }
            }
            $this->notificaciones->messages = $mensajes;
            $this->notificaciones->save();
        }
    }

    public function redirigir($idNotificacion)
    {
        $respuesta = GosenHelper::leerNotificacion($idNotificacion, $this->notificaciones);
        redirect($respuesta[1]);
    }

    public function render()
    {
        $this->notificaciones = Notification::where('user_id', auth()->user()->id)->first();
        if ($this->notificaciones) {
            $mensajes = array_reverse((array) json_decode($this->notificaciones->messages));
        } else {
            $mensajes = [];
        }

        return view('livewire.notifications-component', compact('mensajes'));
    }
}
