<?php

namespace App\Http\Controllers\Livewire;

use App\Models\Paquete;
use App\Models\Transaction;
use Livewire\Component;
use Livewire\WithPagination;

class BonusStaggingComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $paquete;

    public $search = '';

    public $agrupado;

    public $paquetes;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->agrupado = Transaction::where('package', '!=', null)->pluck('id', 'package');
        $this->paquetes = Paquete::all();
    }

    public function render()
    {
        $bonusPackageTrans = Transaction::where('tnx_type', 'bonus_package');
        if ($this->paquete != '') {
            $bonusPackageTrans = $bonusPackageTrans->where('package', $this->paquete);
        }
        if ($this->search != '') {
            $bonusPackageTrans = $bonusPackageTrans->where(function ($q) {
                $q->where('tnx_id', 'like', '%'.$this->search.'%')
                    ->orWhere('tnx_type', 'like', '%'.$this->search.'%')
                    ->orWhere('details', 'like', '%'.$this->search.'%')
                    ->orWhere('id', 'like', '%'.$this->search.'%')
                    ->orWhere('tokens', 'like', '%'.$this->search.'%')
                    ->orWhereHas('tnxUser', function ($q) {
                        $q->where('name', 'like', '%'.$this->search.'%');
                    });
            });
        }
        $bonusPackageTrans = $bonusPackageTrans->paginate(10);

        return view('livewire.bonus-stagging-component', compact('bonusPackageTrans'))
            ->extends('layouts.admin')
            ->section('content');
    }
}
