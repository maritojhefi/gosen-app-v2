<?php

namespace App\Http\Controllers\Livewire;

use App\Helpers\GosenHelper;
use App\Models\Notification;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class EventoNotificacionComponent extends Component
{
    public $mensajes;

    public $notificaciones;

    protected $listeners = [
        'echo:actualizar-notificacion,NotificationEvent' => 'abrir',
        'echo:nueva-notificacion,NotificationDataEvent' => 'mostrarNuevaNotificacion',
    ];

    public function mostrarNuevaNotificacion($data)
    {
        if (auth()->user()->id == $data['usuario']) {
            $this->emit('new-notification', $data['data']);
        }
    }

    public function abrir()
    {
        $this->emit('abrir');
        $this->emit('cerrarPopup');
    }

    public function leerNotificacion($idNotificacion)
    {
        GosenHelper::leerNotificacion($idNotificacion, $this->notificaciones);
    }

    public function eliminarEvento($id)
    {
        DB::table('evento_user')->where('id', $id)->delete();
    }

    public function render()
    {
        $this->notificaciones = Notification::where('user_id', auth()->user()->id)->first();
        if ($this->notificaciones) {
            $this->mensajes = json_decode($this->notificaciones->messages);
        } else {
            $this->mensajes = [];
        }
        $eventos = auth()->user()->eventos;

        return view('livewire.evento-notificacion-component', compact('eventos'));
    }
}
