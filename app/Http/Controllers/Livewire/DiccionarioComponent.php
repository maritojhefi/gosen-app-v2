<?php

namespace App\Http\Controllers\Livewire;

use Livewire\Component;

class DiccionarioComponent extends Component
{
    public $active = 'Spanish';

    public $search;

    public $resultado;

    public $newWords;

    public $path;

    public function cambiarIdioma($idioma)
    {
        $this->active = $idioma;
    }

    public function buscar()
    {
        if ($this->search != null && $this->search != '') {
            switch ($this->active) {
                case 'Spanish':
                    $this->path = base_path('resources/lang/es.json');
                    $jsonString = file_get_contents($this->path);
                    break;
                case 'English':
                    $this->path = base_path('resources/lang/en.json');
                    $jsonString = file_get_contents($this->path);
                    break;
                case 'Portugues':
                    $this->path = base_path('resources/lang/pr.json');
                    $jsonString = file_get_contents($this->path);
                    break;
                default:

                    break;
            }
            $data = json_decode($jsonString, true);
            //$data = array('orange', 'blue', 'green', 'red', 'pink', 'brown', 'black');

            $input = preg_quote('Búsqueda', '~');

            $result = preg_grep('~'.$this->search.'~', $data);
            $this->resultado = $result;
        } else {
            $this->dispatchBrowserEvent('alert', [
                'type' => 'error',
                'message' => 'Ingrese una palabra o texto para buscar',
            ]);
        }
    }

    public function reemplazar($input)
    {
        $jsonString = file_get_contents($this->path);
        $data = json_decode($jsonString, true);
        $cantidadOriginal = count($data);

        // Update Key
        if (isset($data[$input])) {
            $data[$input] = str_replace('"', '´´', $this->newWords);
            $newJsonString = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            $cantidadNueva = count(json_decode($newJsonString, true));

            try {
                if ($cantidadNueva >= $cantidadOriginal) {
                    file_put_contents($this->path, stripslashes($newJsonString));
                    $this->dispatchBrowserEvent('alert', [
                        'type' => 'success',
                        'message' => 'Se guardo correctamente la traduccion',
                    ]);
                }
            } catch (\Throwable $th) {
                $this->dispatchBrowserEvent('alert', [
                    'type' => 'error',
                    'message' => 'Algo salio mal al guardar la traduccion',
                ]);
            }
        }
    }

    public function render()
    {
        return view('livewire.diccionario-component')
            ->extends('layouts.admin')
            ->section('content');
    }
}
