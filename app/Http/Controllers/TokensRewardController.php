<?php

namespace App\Http\Controllers;

use App\Helpers\TokenCalculate as TC;
use App\Models\RewardCoin;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class TokensRewardController extends Controller
{
    public function video1()
    {
        return back()->with('success', ___('Ya viste el video de presentación'));
        // $tc = new TC();
        // $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
        // $user = User::find(auth()->user()->id);
        // if ($user->reward_coin->video1 == false) {
        //     $reward = RewardCoin::where('idUser', $user->id)->first();
        //     $reward->video1 = true;
        //     $reward->save();
        //     $transaction = Transaction::create([
        //         'tokens' => 2,
        //         'total_tokens' => 2,
        //         'user' => $user->id,
        //         'stage' => active_stage()->id,
        //         'tnx_type' => 'gift',
        //         'tnx_time' => now(),
        //         'payment_method' => 'system',
        //         'base_currency' => base_currency(),
        //         'base_currency_rate' => $base_currency_rate,
        //         'checked_by' => '{"name":"System","id":"0"}',
        //         'added_by' => 'SYS-00000',
        //         'checked_time' => now(),
        //         'details' => 'Bonus Token for watch video #1',
        //         'tnx_id' => 1
        //     ]);
        //     $transaction->tnx_id = 'GIFT' . sprintf('%05s', $transaction->id);
        //     $transaction->save();
        //     $antiguo = $user->gift_bonus;
        //     $user->gift_bonus = $antiguo + 2;
        //     $user->save();
        //     return back()->with('success', ___('Ganaste 2 tokens!'));
        // } else {
        //     return back()->with('info', ___('Ya recibiste los tokens por ver el video 1'));
        // }
    }

    public function video2()
    {
        $tc = new TC();
        $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
        $user = User::find(auth()->user()->id);
        if ($user->reward_coin->video2 == false) {
            $reward = RewardCoin::where('idUser', $user->id)->first();
            $reward->video2 = true;
            $reward->save();
            $transaction = Transaction::create([
                'tokens' => 2,
                'total_tokens' => 2,
                'user' => $user->id,
                'stage' => active_stage()->id,
                'tnx_type' => 'gift',
                'tnx_time' => now(),
                'payment_method' => 'system',
                'base_currency' => base_currency(),
                'base_currency_rate' => $base_currency_rate,
                'checked_by' => '{"name":"System","id":"0"}',
                'added_by' => 'SYS-00000',
                'checked_time' => now(),
                'details' => 'Bonus Token for watch video #2',
                'tnx_id' => 1,
            ]);
            $transaction->tnx_id = 'GIFT'.sprintf('%05s', $transaction->id);
            $transaction->save();
            $antiguo = $user->gift_bonus;
            $user->gift_bonus = $antiguo + 2;
            $user->save();

            return back()->with('success', ___('Ganaste 2 tokens!'));
        } else {
            return back()->with('info', ___('Ya recibiste los tokens por ver el video 2'));
        }
    }

    public function video3()
    {
        $tc = new TC();
        $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
        $user = User::find(auth()->user()->id);
        if ($user->reward_coin->video3 == false) {
            $reward = RewardCoin::where('idUser', $user->id)->first();
            $reward->video3 = true;
            $reward->save();
            $transaction = Transaction::create([
                'tokens' => 2,
                'total_tokens' => 2,
                'user' => $user->id,
                'stage' => active_stage()->id,
                'tnx_type' => 'gift',
                'tnx_time' => now(),
                'payment_method' => 'system',
                'base_currency' => base_currency(),
                'base_currency_rate' => $base_currency_rate,
                'checked_by' => '{"name":"System","id":"0"}',
                'added_by' => 'SYS-00000',
                'checked_time' => now(),
                'details' => 'Bonus Token for watch video #3',
                'tnx_id' => 1,
            ]);
            $transaction->tnx_id = 'GIFT'.sprintf('%05s', $transaction->id);
            $transaction->save();
            $antiguo = $user->gift_bonus;
            $user->gift_bonus = $antiguo + 2;
            $user->save();

            return back()->with('success', ___('Ganaste 2 tokens!'));
        } else {
            return back()->with('info', ___('Ya recibiste los tokens por ver el video 3'));
        }
    }

    public function pdf()
    {
        $tc = new TC();
        $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
        $user = User::find(auth()->user()->id);
        if ($user->reward_coin->pdf == false) {
            $reward = RewardCoin::where('idUser', $user->id)->first();
            $reward->pdf = true;
            $reward->save();
            $transaction = Transaction::create([
                'tokens' => 2,
                'total_tokens' => 2,
                'user' => $user->id,
                'stage' => active_stage()->id,
                'tnx_type' => 'gift',
                'tnx_time' => now(),
                'payment_method' => 'system',
                'base_currency' => base_currency(),
                'base_currency_rate' => $base_currency_rate,
                'checked_by' => '{"name":"System","id":"0"}',
                'added_by' => 'SYS-00000',
                'checked_time' => now(),
                'details' => 'Bonus Token for watch PDF',
                'tnx_id' => 1,
            ]);
            $transaction->tnx_id = 'GIFT'.sprintf('%05s', $transaction->id);
            $transaction->save();
            $antiguo = $user->gift_bonus;
            $user->gift_bonus = $antiguo + 2;
            $user->save();

            return back()->with('success', ___('Ganaste 2 tokens!'));
        } else {
            return back()->with('info', ___('Ya recibiste los tokens por ver el video 4'));
        }
    }

    public function modal(Request $request)
    {
        if ($request->checkbox == 'true') {
            $tc = new TC();
            $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
            $user = User::find(auth()->user()->id);
            if ($user->reward_coin->modal == false) {
                $reward = RewardCoin::where('idUser', $user->id)->first();
                $reward->modal = true;
                $reward->save();
                $transaction = Transaction::create([
                    'tokens' => 2,
                    'total_tokens' => 2,
                    'user' => $user->id,
                    'stage' => active_stage()->id,
                    'tnx_type' => 'gift',
                    'tnx_time' => now(),
                    'payment_method' => 'system',
                    'base_currency' => base_currency(),
                    'base_currency_rate' => $base_currency_rate,
                    'checked_by' => '{"name":"System","id":"0"}',
                    'added_by' => 'SYS-00000',
                    'checked_time' => now(),
                    'details' => 'Bonus Token for read terms & politics',
                    'tnx_id' => 1,
                ]);
                $transaction->tnx_id = 'GIFT'.sprintf('%05s', $transaction->id);
                $transaction->save();
                $antiguo = $user->gift_bonus;
                $user->gift_bonus = $antiguo + 2;
                $user->save();

                return back()->with('success', ___('Ganaste 2 tokens!'));
            } else {
                return back()->with('info', ___('Ya recibiste los tokens por ver el PDF informativo'));
            }
        } else {
            return back()->with('error', ___('Debes darle al check y aceptar los terminos y condiciones para poder continuar'));
        }
    }
}
