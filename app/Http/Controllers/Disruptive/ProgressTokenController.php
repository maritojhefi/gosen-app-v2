<?php

namespace App\Http\Controllers\Disruptive;

use Auth;
use App\Models\User;
use App\Models\Setting;
use App\Models\RegaloType;
use App\Models\Transaction;
use App\Helpers\GosenHelper;
use App\Models\BonusReferral;
use App\Http\Controllers\Controller;

class ProgressTokenController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $tipo = BonusReferral::where('user_id', $user->id)->firstOrFail();
        $nomTipo = RegaloType::where('id', $tipo->regalo_tipo_id)->firstOrFail();
        switch ($nomTipo->name) {
            case 'Beginner':
                $ico = 'fas fa-check';
                break;
            case 'Basic':
                $ico = 'fas fa-award';
                break;
            case 'Advanced':
                $ico = 'fas fa-star';
                break;
            case 'Pro':
                $ico = 'fas fa-medal';
                break;
            case 'Master Pro':
                $ico = 'fas fa-trophy';
                break;
            case 'Master Global':
                $ico = 'fas fa-crown';
                break;
        }
        $regalo = $nomTipo->name;
        $referidos = User::where('referral', $user->id)->count();
        $token = $user->gift_bonus;

        $next = $tipo->regalo_tipo_id;
        if ($next < 6) {
            $next = $next + 1;
        }
        $nomTipoNext = RegaloType::where('id', $next)->first();
        $guests = RegaloType::where('id', $next)->first();
        switch ($nomTipoNext->name) {
            case 'Beginner':
                $icoNext = 'fas fa-check';
                break;
            case 'Basic':
                $icoNext = 'fas fa-award';
                break;
            case 'Advanced':
                $icoNext = 'fas fa-star';
                break;
            case 'Pro':
                $icoNext = 'fas fa-medal';
                break;
            case 'Master Pro':
                $icoNext = 'fas fa-trophy';
                break;
            case 'Master Global':
                $icoNext = 'fas fa-crown';
                break;
        }
        $regaloNext = $nomTipoNext->name;
        $referidosFaltan = $guests->total_guests - $referidos;
        $tokenNext = $guests->total_tokens;
        $arrayProgressUser = [];
        $arrayProgressNext = [];
        array_push($arrayProgressUser, $regalo, $referidos, $token);
        array_push($arrayProgressNext, $regaloNext, $icoNext, $referidosFaltan, $tokenNext);
        // $progresoCardActivo = Setting::whereIn('field', ['perfil_progreso_card_2', 'perfil_progreso_card_3'])->get();
        $transaction = Transaction::where([['user', auth()->user()->id], ['status', 'approved'], ['tnx_type', 'purchase'], ['checked_time', '!=', null]])->get();
        $cardsActivosPorTipos = Setting::select('field', 'value')->where('field', 'perfil_progreso_users_type')->first();
        $cardsUserPorTipo = '';
        foreach (json_decode($cardsActivosPorTipos->value) as $key => $value) {
            $parts = explode("_", $key);
            $valor_entero = (int)end($parts);
            if (auth()->user()->type_user == $valor_entero) {
                $cardsUserPorTipo = $value;
            }
        }
        return view('disruptive.progress-token.index', compact('user', 'arrayProgressUser', 'ico', 'arrayProgressNext', /*'progresoCardActivo'*/ 'cardsUserPorTipo', 'transaction'));
    }
}
