<?php

namespace App\Http\Controllers\disruptive;

use App\Http\Controllers\Controller;
use App\Models\FrequentQuestion;

class FAQController extends Controller
{
    public function index()
    {
        $questions = FrequentQuestion::all();

        return view('disruptive.faq.index', compact('questions'));
    }
}
