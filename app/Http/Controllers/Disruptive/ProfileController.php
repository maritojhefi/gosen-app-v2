<?php

namespace App\Http\Controllers\disruptive;

use App\Http\Controllers\Controller;
use App\Models\UserMeta;
use Auth;
use Carbon\Carbon;
use IcoHandler;
use PragmaRX\Google2FA\Google2FA;

class ProfileController extends Controller
{
    protected $handler;

    public function __construct(IcoHandler $handler)
    {
        $this->middleware('auth')->except('crearCookie');
        $this->handler = $handler;
    }

    public function account($var = 2)
    {
        $countries = $this->handler->getCountries();
        $user = Auth::user();
        $userMeta = UserMeta::getMeta($user->id);
        $g2fa = new Google2FA();
        $google2fa_secret = $g2fa->generateSecretKey();
        $google2fa = $g2fa->getQRCodeUrl(
            site_info().'-'.$user->name,
            $user->email,
            $google2fa_secret
        );
        $actual = Carbon::now();
        $reset5MinVerification = Carbon::parse($user->timer_whatsapp_reset)->addMinutes(5)->format('Y-m-d H:i:s');

        return view('disruptive.profile.index', compact('reset5MinVerification', 'actual', 'user', 'userMeta', 'countries', 'google2fa', 'google2fa_secret', 'var'));
    }
}
