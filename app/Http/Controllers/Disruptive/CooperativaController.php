<?php

namespace App\Http\Controllers\Disruptive;

use App\Http\Controllers\Controller;

class CooperativaController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Cooperativa Publicitaria');
    }

    public function index()
    {
        return view('disruptive.cooperativa.index');
    }
}
