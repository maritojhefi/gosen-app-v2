<?php

namespace App\Http\Controllers\disruptive;

use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Models\LevelBonusDollar;
use App\Models\Page;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ReferralController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Referral');
    }

    public function index()
    {
        $user = User::where('id', auth()->user()->id);
        $page = Page::where('slug', 'referral')->first();
        //trae los referidos directos de acuerdo al campo "referrral" de la tabla user
        $reffered = User::where('referral', auth()->id())->where('email', '!=', null)->orderBy('created_at', 'desc')->get();

        $niveles = LevelBonusDollar::select('name')->groupBy('name')->get();
        $tiposUsuario = LevelBonusDollar::get();
        $lvls = $tiposUsuario->groupBy('type_user');
        foreach ($lvls as $key => $value) {
            foreach ($value as $item) {
                $nivelesAc = $item->where('status', 'enabled')->get();
            }
        }
        $usuario = $nivelesAc->groupBy('type_user');
        $Activoss = isset($usuario[auth()->user()->type_user]);
        $nivelesActivos = $Activoss == false ? null : $usuario[auth()->user()->type_user];

        $user = Auth::user();
        $transaction = Transaction::where('user', $user->id)->count();
        $tipoUsuario = $user->type_user;

        $referido = User::where('id', $user->referral)->first();
        //traer todos los id de los referidos por nivel
        $referrals = GosenHelper::allReferral([$user->id], $niveles->count());
        unset($referrals[0]);
        // dd($referrals);
        $usuariosPorNivelYFecha = [
            1 => ['Dia' => 0, 'Semana' => 0, 'Mes' => 0],
            2 => ['Dia' => 0, 'Semana' => 0, 'Mes' => 0],
            3 => ['Dia' => 0, 'Semana' => 0, 'Mes' => 0],
            4 => ['Dia' => 0, 'Semana' => 0, 'Mes' => 0],
            5 => ['Dia' => 0, 'Semana' => 0, 'Mes' => 0],
        ];
        foreach ($referrals as $nivel => $array) {
            foreach ($array as $idUsuario) {
                $user = User::find($idUsuario);
                if ($user->email_verified_at) {
                    $verifiedDate = new Carbon($user->email_verified_at);
                    $now = Carbon::now();
                    $diferencia = $verifiedDate->diffInDays($now);
                    if ($diferencia == 0) {
                        $etapa = 'Dia';
                        $usuariosPorNivelYFecha[$nivel][$etapa] = $usuariosPorNivelYFecha[$nivel][$etapa] + 1;
                    }
                    if ($diferencia <= 7 && date('m', strtotime($user->email_verified_at)) == date('m')) {
                        $etapa = 'Semana';
                        $usuariosPorNivelYFecha[$nivel][$etapa] = $usuariosPorNivelYFecha[$nivel][$etapa] + 1;
                    }
                    if (date('m', strtotime($user->email_verified_at)) == date('m')) {
                        $etapa = 'Mes';
                        $usuariosPorNivelYFecha[$nivel][$etapa] = $usuariosPorNivelYFecha[$nivel][$etapa] + 1;
                    }
                }
            }
        }

        return view('disruptive.referral.index', compact('page', 'reffered', 'user', 'niveles', 'referrals', 'tipoUsuario', 'transaction', 'nivelesActivos', 'usuariosPorNivelYFecha', 'referido'));
    }
}
