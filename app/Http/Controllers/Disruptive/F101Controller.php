<?php

namespace App\Http\Controllers\Disruptive;

use App\Http\Controllers\Controller;

class F101Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:F101');
    }

    public function index()
    {
        return view('disruptive.f101.index');
    }
}
