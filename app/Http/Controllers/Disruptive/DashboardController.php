<?php

namespace App\Http\Controllers\disruptive;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Country;
use App\Charts\UserChart;
use App\Models\BonusDolar;
use App\Models\RegaloType;
use App\Models\RewardCoin;
use App\Models\Transaction;
use App\Helpers\GosenHelper;
use Illuminate\Http\Request;
use App\Models\LevelBonusDollar;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Panel Fase 2')->only('panelesTest');
    }

    public function panelesTest(Request $request, $panel)
    {
        $rewardsUser = auth()->user()->reward_coin;
        if (!$rewardsUser) {
            $rewardsUser = RewardCoin::create([
                'idUser' => auth()->user()->id,
                'video1' => true,
            ]);
        } else {
            $rewardsUser->video1 = true;
            $rewardsUser->save();
        }
        $regaloInicial = RegaloType::find(1);
        $user = Auth::user();
        $niveles = LevelBonusDollar::select('name')->groupBy('name')->get();
        $niveles = $niveles->count() - 1;
        $referidos = GosenHelper::allReferral([$user->id], $niveles);
        $usersChart = new UserChart;
        $arrayColores = ['rgb(67, 35, 124)', 'rgb(46, 134, 171)', 'rgb(241, 143, 1)', 'rgb(199, 62, 29)', 'rgb(84, 158, 86)', 'rgb(255, 195, 0)', 'rgb(27, 235, 210)'];
        if (isset($request->grafica)) {
            $tipoGrafica = $request->grafica;
        } else {
            $tipoGrafica = 'dia';
        }
        switch ($tipoGrafica) {
            case 'dia':
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $array = [];
                    for ($i = 6; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($array, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $array)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $array)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }

                break;
            case 'semana':
                $fechaActual = Carbon::now();
                $fechaComoEntero = strtotime($fechaActual);
                $dia = date('d', $fechaComoEntero);
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $arraySemanas = [];
                    for ($i = $dia - 1; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($arraySemanas, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays($dia, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arraySemanas)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNDays($dia, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arraySemanas)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }
                break;
            case 'mes':
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $arrayMeses = [];
                    for ($i = 5; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subMonths($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereMonth('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereMonth('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($arrayMeses, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNMonth(6, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arrayMeses)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNMonth(6, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arrayMeses)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }
                break;
            default:
                break;
        }
        $porPais = User::where('referral', $user->id)->where('latitude', '!=', null)->get();
        $asociadosPorPais = GosenHelper::referidosPorPaisYNivel($user->id, 5, false);
        $banderas = [];
        foreach ($asociadosPorPais as $pais => $valores) {
            $bandera = Country::where('name', $pais)->first();
            if ($bandera) {
                array_push($banderas, $bandera->flag);
            }
        }

        $paises = Country::orderBy('users', 'DESC')->get();
        $asociados = User::where('role', 'user')->get();
        $misreferidos = collect();
        foreach ($asociados as $referral) {
            $cant_refer = User::where('referral', $referral->id)->where('role', 'user')->count();
            if ($cant_refer > 0) {
                $misreferidos->push([
                    'foto' => $referral->foto,
                    'asociado' => $referral->username,
                    'canrefer' => $cant_refer,
                ]);
            }
        }
        $misasociados = $misreferidos->sortByDesc('canrefer');
        switch ($panel) {
            case 2:
                $vista = 'disruptive.dashboard.indexPanel2';
                break;
            case 3:
                $vista = 'disruptive.dashboard.indexPanel3';
                break;
            case 4:
                $vista = 'disruptive.dashboard.indexPanel4';
                break;
            default:
                $vista = 'disruptive.dashboard.index';
                break;
        }

        return view($vista, ['usersChart' => $usersChart], compact('paises', 'rewardsUser', 'user', 'misasociados', 'asociadosPorPais', 'tipoGrafica', 'banderas', 'regaloInicial'));
    }

    public function index(Request $request)
    {
        $regaloInicial = RegaloType::find(1);
        $user = Auth::user();
        $niveles = LevelBonusDollar::select('name')->groupBy('name')->get();
        $niveles = $niveles->count() - 1;
        $referidos = GosenHelper::allReferral([$user->id], $niveles);
        $usersChart = new UserChart;
        $arrayColores = ['rgb(67, 35, 124)', 'rgb(46, 134, 171)', 'rgb(241, 143, 1)', 'rgb(199, 62, 29)', 'rgb(84, 158, 86)', 'rgb(255, 195, 0)', 'rgb(27, 235, 210)'];
        if (isset($request->grafica)) {
            $tipoGrafica = $request->grafica;
        } else {
            $tipoGrafica = 'dia';
        }
        switch ($tipoGrafica) {
            case 'dia':
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $array = [];
                    for ($i = 6; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($array, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $array)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNDays(7, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $array)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }

                break;
            case 'semana':
                $fechaActual = Carbon::now();
                $fechaComoEntero = strtotime($fechaActual);
                $dia = date('d', $fechaComoEntero);
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $arraySemanas = [];
                    for ($i = $dia - 1; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subDays($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereDate('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($arraySemanas, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNDays($dia, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arraySemanas)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNDays($dia, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arraySemanas)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }
                break;
            case 'mes':
                $cantNivel = 0;
                foreach ($referidos as $nivel => $arrayReferidos) {
                    $arrayMeses = [];
                    for ($i = 5; $i >= 0; $i--) {
                        $fecha = Carbon::now()->subMonths($i);
                        $usuarios = User::select('email_verified_at')->where('referral', $arrayReferidos[0]);
                        for ($j = 1; $j < count($arrayReferidos); $j++) {
                            $usuarios = $usuarios->whereMonth('email_verified_at', $fecha)->orWhere('referral', $arrayReferidos[$j]);
                        }
                        $usuarios = $usuarios->whereMonth('email_verified_at', $fecha)->orderBy('email_verified_at', 'desc')->count();
                        array_push($arrayMeses, $usuarios);
                    }
                    $usersChart->options([
                        'scales' => [
                            'xAxes' => [],
                            'yAxes' => [
                                [
                                    'ticks' => [
                                        'stepSize' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ]);
                    $usersChart->displaylegend(true);
                    $usersChart->labels(GosenHelper::getLastNMonth(6, 'D-m'));
                    $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arrayMeses)
                        ->color($arrayColores[$nivel])
                        ->backgroundcolor($arrayColores[$nivel])
                        ->fill(true);
                    $cantNivel++;
                }
                if ($cantNivel <= 4) {
                    $nivel = $cantNivel;
                    for ($i = $cantNivel; $i <= 4; $i++) {
                        $usersChart->displaylegend(true);
                        $usersChart->labels(GosenHelper::getLastNMonth(6, 'D-m'));
                        $usersChart->dataset(___('Asociados de nivel ') . ' ' . $nivel + 1, 'bar', $arrayMeses)
                            ->color($arrayColores[$nivel])
                            ->backgroundcolor($arrayColores[$nivel])
                            ->fill(true);
                        $nivel++;
                    }
                }
                break;
            default:
                break;
        }
        $porPais = User::where('referral', $user->id)->where('latitude', '!=', null)->get();
        $asociadosPorPais = GosenHelper::referidosPorPaisYNivel($user->id, 5, false);
        $banderas = [];
        foreach ($asociadosPorPais as $pais => $valores) {
            $bandera = Country::where('name', $pais)->first();
            if ($bandera) {
                array_push($banderas, $bandera->flag);
            }
        }

        $paises = Country::orderBy('users', 'DESC')->get();
        $asociados = User::where('role', 'user')->get();
        $misreferidos = collect();
        foreach ($asociados as $referral) {
            $cant_refer = User::where('referral', $referral->id)->where('role', 'user')->count();
            if ($cant_refer > 0) {
                $misreferidos->push([
                    'foto' => $referral->foto,
                    'asociado' => $referral->username,
                    'canrefer' => $cant_refer,
                ]);
            }
        }
        $misasociados = $misreferidos->sortByDesc('canrefer');
        $contribution = Transaction::user_contribution();
        $monto = auth()->user()->dolarBonus;
        $bonus = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', BonusDolar::TYPE1)->get();
        return view('disruptive.dashboard.index', ['usersChart' => $usersChart], compact('paises', 'user', 'misasociados', 'asociadosPorPais', 'tipoGrafica', 'banderas', 'regaloInicial', 'contribution', 'monto','bonus'));
    }

    public function firstLogin(Request $request)
    {
        if ($request->first_login == 0) {
            $usuario = User::find(auth()->user()->id);
            $usuario->first_login = 1;
            $usuario->save();
        }
    }
}
