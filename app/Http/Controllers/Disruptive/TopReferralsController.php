<?php

namespace App\Http\Controllers\disruptive;

use App\Http\Controllers\Controller;
use App\Models\BonusReferral;
use App\Models\Menu;
use App\Models\RegaloType;
use App\Models\User;
use Auth;

class TopReferralsController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Top Referrals');
    }

    public function index()
    {
        $user = Auth::user();
        $tipo = BonusReferral::where('user_id', $user->id)->first();
        $nomTipo = RegaloType::where('id', $tipo->regalo_tipo_id)->first();

        switch ($nomTipo->name) {
            case 'Beginner':
                $ico = 'fas fa-check';
                break;
            case 'Basic':
                $ico = 'fas fa-award';
                break;
            case 'Advanced':
                $ico = 'fas fa-star';
                break;
            case 'Pro':
                $ico = 'fas fa-medal';
                break;
            case 'Master Pro':
                $ico = 'fas fa-trophy';
                break;
            case 'Master Global':
                $ico = 'fas fa-crown';
                break;
        }
        $nivel = Menu::where('id', $user->type_user + 1)->first();
        $regalo = $nomTipo->name;
        $nivelUser = $nivel->valor;
        $referrals = User::where([['referral', '!=', 'null']])->pluck('referral');
        $totalReferidos = $referrals->countBy(); //usuarios con el total de asociados
        $array = collect();
        $totalUserLoged = User::where('referral', auth()->user()->id)->count(); //cantidad de asociados del usuario logeado

        $cont = 0;
        $win = 0;
        foreach ($totalReferidos as $idUser => $cantidad) {
            $user = User::find($idUser);
            if ($user) {
                $array->push([
                    'id' => $user->id,
                    'username' => $user->username,
                    'nombre' => $user->name,
                    'foto' => $user->foto,
                    'cantidad' => $cantidad,

                ]);
            }
        }

        $array = $array->sortBy([
            ['cantidad', 'desc'],

        ]);  //array con los usuarios en orden descendente con el numero de asociados
        // dd($array);
        foreach ($array as $arr) {
            $cont++;
            if ($cont == 10) {
                $win = $arr['cantidad'];
                break;
            }
        }
        // dd($win);
        $array->push([
            'id' => auth()->user()->id,
            'username' => auth()->user()->username,
            'nombre' => auth()->user()->name,
            'foto' => auth()->user()->foto,
            'cantidad' => $totalUserLoged,
        ]); //agregamos ell usuario logueado al array de referidos
        // dd($array);
        if ($win - $totalUserLoged == 0) {
            $cantfalta = 1;
        } else {
            $cantfalta = $win - $totalUserLoged;
        }
        // dd($cantfalta);
        $emojis = ['👑', '🏆', '⭐️', '🤩', '🔥', '👏', '🎉', '🙌', '💯', '👍', '👍'];

        return view('disruptive.top-referrals.index', compact('array', 'emojis', 'cantfalta', 'regalo', 'nivelUser', 'ico'));
    }
}
