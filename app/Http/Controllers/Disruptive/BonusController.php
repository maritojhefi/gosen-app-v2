<?php

namespace App\Http\Controllers\disruptive;

use App\Http\Controllers\Controller;
use App\Models\BonusDolar;
use App\Models\User;

class BonusController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Bonus');
    }

    public function index()
    {
        $tipoWithdraw = BonusDolar::TYPE1;
        $sum = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', $tipoWithdraw)->sum('amount');
        $bonus = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', $tipoWithdraw)->orderBy('created_at', 'desc')->paginate(5);
        $trnxWith = BonusDolar::where('user_id', auth()->user()->id)->where('type', '=', $tipoWithdraw)->orderBy('created_at', 'desc')->get();
        $monto = auth()->user()->dolarBonus;
        $user = User::find(auth()->user()->id);
        $totalRequestAmount = $user->bonus->where('status', 'pending')->where('type', BonusDolar::TYPE1)->sum('amount');
        $withdrawn = BonusDolar::where([
            ['user_id', auth()->user()->id],
            ['status', '=', BonusDolar::STATUS2],
            ['type', '=', $tipoWithdraw],
        ])->sum('amount');

        $bonusUsdBalanceWithdraw = BonusDolar::where('user_id', $user->id)->where('status', BonusDolar::STATUS2)->sum('amount');

        $bonusUsdBalancePending = BonusDolar::where('user_id', $user->id)->where('status', BonusDolar::STATUS1)->sum('amount');

        return view('disruptive.bonus.index', compact('bonus', 'monto', 'trnxWith', 'sum', 'withdrawn', 'totalRequestAmount', 'bonusUsdBalanceWithdraw', 'bonusUsdBalancePending'));
    }
}
