<?php

namespace App\Http\Controllers\disruptive;

use App\Helpers\GosenHelper;
use App\Helpers\TokenCalculate as TC;
use App\Http\Controllers\Controller;
use App\Models\Paquete;
use App\Models\PaymentMethod;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class PackagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('status_page:Package');
    }

    public function index()
    {
        $has_sidebar = false;
        $paquetes = Paquete::orderBy('posicion', 'asc')->where('estado', true)->get();
        $paquetesVendidos = GosenHelper::cantidadPaquetesVendidos();

        return view('disruptive.packages.index', compact('has_sidebar', 'paquetes', 'paquetesVendidos'));
    }

    public function buy($package, $pack, Request $request)
    {
        $paquete = Paquete::find($pack);
        if ($paquete) {
            if ($paquete->stock == 0) {
                $ret['msg'] = 'warning';
                $ret['message'] = ___('No existe stock suficiente para este paquete');
                if ($request->ajax()) {
                    return response()->json($ret);
                } else {
                    return back()->with('error', ___('No existe stock suficiente para este paquete'));
                }
            }
        } else {
            $ret['msg'] = 'warning';
            $ret['message'] = ___('Algo salio mal, intente de nuevo');
            if ($request->ajax()) {
                return response()->json($ret);
            } else {
                return back()->with('danger', ___('Algo salio mal, intente de nuevo'));
            }
        }
        session(['pack' => $pack]);
        $monto = Crypt::decrypt($package);
        if (token('before_kyc') == '1') {
            $check = User::find(auth()->user()->id);
            if ($check && ! isset($check->kyc_info->status)) {
                return redirect(route('user.kyc'))->with(['warning' => __('messages.kyc.mandatory')]);
            } else {
                if ($check->kyc_info->status != 'approved') {
                    return redirect(route('user.kyc.application'))->with(['warning' => __('messages.kyc.mandatory')]);
                }
            }
        }
        $stage = active_stage();
        $tc = new TC();
        $currencies = Setting::active_currency();
        $currencies['base'] = base_currency();
        $bonus = $tc->get_current_bonus(null);
        $bonus_amount = $tc->get_current_bonus('amount');
        $price = Setting::exchange_rate($tc->get_current_price());
        $minimum = $tc->get_current_price('min');
        $active_bonus = $tc->get_current_bonus('active');
        $pm_currency = PaymentMethod::Currency;
        $pm_active = DB::table('payment_methods')->where('status', 'active')->get();
        $token_prices = $tc->calc_token(1, 'price');
        $is_price_show = token('price_show');
        $contribution = Transaction::user_contribution();
        if ($request->ajax() && active_stage_status() == 'expired') {
            $ret['msg'] = 'warning';
            $ret['message'] = ___('The stage is expired, operation cancelled');

            return response()->json($ret);
        }
        if ($price <= 0 || $stage == null || count($pm_active) <= 0 || token_symbol() == '') {
            return redirect()->route('user.home')->with(['info' => __('messages.ico_not_setup')]);
        }

        $descuento = Paquete::where('tokens', $monto)->first();
        $montoDescuento = ($descuento->valor_usd * $descuento->descuento) / 100 == 0 ? $descuento->valor_usd : ($descuento->valor_usd - ($descuento->valor_usd * $descuento->descuento) / 100);
        $tokenConDescuento = token_calc($montoDescuento);

        $btcc = 'btc';
        $btcPrice = to_num(($tc->calc_token($tokenConDescuento, 'price')->$btcc) / $token_prices->base, 'max', ',');

        return view('disruptive.packages.buy-package', compact('monto', 'tc', 'stage', 'currencies', 'bonus', 'bonus_amount', 'price', 'token_prices', 'is_price_show', 'minimum', 'active_bonus', 'pm_currency', 'contribution', 'pack', 'btcPrice', 'tokenConDescuento'));
    }
}
