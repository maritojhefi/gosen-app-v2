<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class PruebasController extends Controller
{
    public function pruebaCompresion()
    {
        //comprimir la foto
        $manager = new ImageManager(['driver' => 'imagick']);
        $image = $manager->make('1653609702.png')->resize(300, 200);
        dd($image);
        // $img = Image::make('public/1653609702.png');
        // $img->resize(320, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // });
        // $img->rotate(0);
        // $img->save('comprimidos/adrian.png');
    }

    public function pruebaQR()
    {
        return view('qrcode');
    }
}
