<?php

namespace App\Http\Controllers;

use Orhanerday\OpenAi\OpenAi;

class OpenAIController extends Controller
{
    public function crearImagenes()
    {
        $open_ai = new OpenAi('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');

        // get prompt parameter
        $prompt = 'laravel en persona';
        // set api data
        $complete = $open_ai->image([
            'prompt' => $prompt,
            'n' => 2, //número de imágenes
            'size' => '512x512', //dimensiones de la imagen
            'response_format' => 'b64_json', //use "url" for less credit usage
        ]);
        $complete = json_decode($complete);
        // dd($complete);
        $string = '<img loading="lazy" src="data:image/png;base64,'.$complete->data[0]->b64_json.'" />';

        return view('prueba', compact('string'));
    }
}
