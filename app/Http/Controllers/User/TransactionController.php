<?php

namespace App\Http\Controllers\User;

/**
 * Transaction Controller
 *
 *
 * @author Softnio
 *
 * @version 1.0.0
 */

use App\Http\Controllers\Controller;
use App\Models\BonusDolar;
use App\Models\IcoStage;
use App\Models\Transaction;
use App\PayModule\Module;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function index()
    {
        return redirect(route('all.notifications', ['filtrar' => 'transacciones']));
        $user = Auth::user();
        $transactions = Transaction::where('user', auth()->user()->id)->orderBy('created_at', 'desc')->get();
        $bonusDollarsTransaction = BonusDolar::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();
        Transaction::where(['user' => auth()->id(), 'status' => 'new'])->delete();
        $trnxs = Transaction::where('user', Auth::id())
            ->where('status', '!=', 'deleted')
            ->where('status', '!=', 'new')
            ->whereNotIn('tnx_type', ['bonus'])
            ->orderBy('created_at', 'DESC')->get();
        $transfers = Transaction::get_by_own(['tnx_type' => 'transfer'])->get()->count();
        $referrals = Transaction::get_by_own(['tnx_type' => 'referral'])->get()->count();
        $bonuses = Transaction::get_by_own(['tnx_type' => 'bonus'])->get()->count();
        $refunds = Transaction::get_by_own(['tnx_type' => 'refund'])->get()->count();
        $typeuser1 = BonusDolar::TYPEUSER1;
        $typeuser2 = BonusDolar::TYPEUSER2;
        $typeuser3 = BonusDolar::TYPEUSER3;
        $tipobono = BonusDolar::BONUSCODETNX;

        //variables con todos los registros para los tabs, para llenar las tablas en la vista
        $trnxBonusDollarsAll = $bonusDollarsTransaction->whereIn('type', ['bonus', 'withdraw']);
        $trnxBonusStart = $transactions->whereIn('tnx_type', ['launch', 'joined', 'gift']);
        $trnxPurBonus = $transactions->whereNotIn('tnx_type', ['launch', 'joined', 'gift', 'bonus']);

        //para los Bonus Dollars sacar el total "Bonus USD"
        $transBonus = $bonusDollarsTransaction->where('type', 'bonus')->sum('amount');
        $canceledBonus = $bonusDollarsTransaction->where('type', 'bonus')->where('status', 'canceled')->sum('amount');
        $transBonusWithdraw = $bonusDollarsTransaction->where('type', 'withdraw')->where('status', 'approved')->sum('amount');
        $totalBonusDolars = $transBonus - $transBonusWithdraw - $canceledBonus;

        //(GOS TOKEN)total para las transacciones (solo compras)
        $totalTokenGos2 = number_format($user->tokenBalance, 2);
        $totalUsdAmount = $transactions->whereNotIn('tnx_type', ['withdraw', 'refund', 'joined', 'launch', 'gift', 'bonus'])->where('status', 'approved')->sum('base_amount');
        $withdraw = $transactions->where('tnx_type', 'withdraw')->where('status', 'approved')->sum('base_amount');
        $refu = $transactions->where('tnx_type', 'refund')->where('status', 'approved')->sum('base_amount');
        $refund = str_replace('-', '', $refu);
        $total = $withdraw + $refund;
        $totalUsdAmountTokens = $totalUsdAmount - $total;

        //(Balance GOS Token)total de tokens para el usuario actual incluyendo (gift,launch,joined)
        $gift = $user->gift_bonus;
        $balance = $user->tokenBalance;
        $totalTokenGos = $gift + $balance;
        // monto usd para todas las transacciones incluyendo (gift,launch,joined)
        $usdregalos = $transactions->whereIn('tnx_type', ['joined', 'gift', 'launch']);
        $total = 0;
        foreach ($usdregalos as $reg) {
            $total2 = $reg->total_tokens * $reg->base_currency_rate;
            $total = $total + $total2;
        }
        $totalUSDGosBalance = $totalUsdAmountTokens + $total;

        $has_trnxs = (object) [
            'transfer' => ($transfers > 0) ? true : false,
            'referral' => ($referrals > 0) ? true : false,
            'bonus' => ($bonuses > 0) ? true : false,
            'refund' => ($refunds > 0) ? true : false,
        ];

        return view('user.transactions', compact(
            'trnxs',
            'has_trnxs',
            'totalBonusDolars',
            'trnxBonusDollarsAll',
            'tipobono',
            'typeuser1',
            'typeuser2',
            'typeuser3',
            'totalUSDGosBalance',
            'trnxBonusStart',
            'trnxPurBonus',
            'totalTokenGos',
            'totalTokenGos2',
            'totalUsdAmountTokens'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     *
     * @throws \Throwable
     */
    public function show(Request $request, $id = '')
    {
        $module = new Module();
        $tid = ($id == '' ? $request->input('tnx_id') : $id);
        if ($tid != null) {
            $tnx = Transaction::find($tid);

            return $module->show_details($tnx);
        } else {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     */
    public function destroy(Request $request, $id = '')
    {
        $tid = ($id == '' ? $request->input('tnx_id') : $id);
        if ($tid != null) {
            $tnx = Transaction::FindOrFail($tid);
            if ($tnx) {
                $old = $tnx->status;
                $tnx->status = 'deleted';
                $tnx->save();
                if ($old == 'pending' || $old == 'onhold') {
                    IcoStage::token_add_to_account($tnx, 'sub');
                }
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.delete.delete', ['what' => 'Transaction']);
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = 'This transaction is not available now!';
            }
        } else {
            $ret['msg'] = 'warning';
            $ret['message'] = __('messages.delete.failed', ['what' => 'Transaction']);
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function exportarTransactionDetailsPdf($id)
    {
        // $transactionDetails=Transaction::where('tnx_id',$tnx_id);
        $transaction = Transaction::find($id);
        $pdf = PDF::loadView('pdf.transaction-details-pdf', compact('transaction'));

        return $pdf->setPaper('a4', 'portrait')->stream('TransactionsDetails-'.date('d-m-Y').'.pdf');
    }
}
