<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Valoration;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ValorationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ordenar = $request->input('orden');
        // dd($ordenar);

        if ($ordenar == 1) {
            $valorations = Valoration::orderBy('puntaje', 'DESC');
        } elseif ($ordenar == 0) {
            $valorations = Valoration::orderBy('puntaje', 'ASC');
        }
        if ($ordenar == null) {
            $valorations = Valoration::orderBy('created_at', 'desc');
        }

        $valorations = $valorations->paginate(10);

        return view('admin.valoration.index', compact('valorations', 'ordenar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $conPermiso = true;
        $usuario = auth()->user()->id;
        $valoraciones = Valoration::where('idUser', $usuario)->latest()->first();
        if ($valoraciones) {
            $now = Carbon::now();
            $diasDiferencia = $valoraciones->created_at->diffInDays($now);
            if ($diasDiferencia <= 5) {
                $conPermiso = false;
            }
        }
        $valora = new Valoration();
        $valorations = Valoration::where('estado', 1)->orderBy('puntaje', 'desc')->take(10)->get();

        return view('user.valoration.create', compact('valorations', 'conPermiso'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = auth()->user()->id;
        $valoraciones = Valoration::where('idUser', $usuario)->latest()->first();
        if ($valoraciones) {
            $now = Carbon::now();
            $diasDiferencia = $valoraciones->created_at->diffInDays($now);
            if ($diasDiferencia >= 5) {
                Valoration::create([
                    'comentario' => $request->comentario,
                    'puntaje' => $request->puntaje,
                    'estado' => false,
                    'idUser' => $usuario,
                ]);

                return back()->with('success', ___('Valuation Completed Successfully!'));
            } else {
                return back()->with('error', ___('You cannot make a comment within ').(5 - $diasDiferencia).___(' days'));
            }
        } else {
            Valoration::create([
                'comentario' => $request->comentario,
                'puntaje' => $request->puntaje,
                'estado' => false,
                'idUser' => $usuario,
            ]);
        }

        return back()->with('success', ___('Valuation Completed Successfully!'));
    }

    public function changestatus($id)
    {
        $valoration = Valoration::find((int) $id);
        if ($valoration->estado == false) {
            $valoration->estado = true;
            $valoration->save();

            return 'true';
        } else {
            $valoration->estado = false;
            $valoration->save();

            return 'false';
        }
    }
}
