<?php

namespace App\Http\Controllers\User;

/**
 * User Controller
 *
 *
 * @author Softnio
 *
 * @version 1.0.6
 */

use App\Events\AnalyticAdminEvent;
use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Mail\ConfirmUserWithdrawBonusDollarsMail;
use App\Mail\WithdrawGosenRequestMail;
use App\Models\Activity;
use App\Models\BonusDolar;
use App\Models\GlobalMeta;
use App\Models\Page;
use App\Models\RewardCoin;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserMeta;
use App\Notifications\PasswordChange;
use Auth;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use IcoHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use PragmaRX\Google2FA\Google2FA;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    protected $handler;

    public function __construct(IcoHandler $handler)
    {
        $this->middleware('auth')->except('crearCookie');
        $this->handler = $handler;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function index()
    {
        $rewardsUser = auth()->user()->reward_coin;
        if (!$rewardsUser) {
            $rewardsUser = RewardCoin::create([
                'idUser' => auth()->user()->id,
            ]);
        }

        if (!$this->handler->check_body() && empty(app_key())) {
            Auth::logout();

            return redirect()->route('login')->with([
                'warning' => $this->handler->accessMessage(),
            ]);
        }
        $user = Auth::user();
        $stage = active_stage();
        $contribution = Transaction::user_contribution();
        $tc = new \App\Helpers\TokenCalculate();
        $active_bonus = $tc->get_current_bonus('active');
        //Cookie::queue('cookisprueba','valor',60);
        $bonus = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', BonusDolar::TYPE1)->get();
        $monto = auth()->user()->dolarBonus;
        session(['color' => asset(theme_color_user($user->theme_color, 'base'))]);
        session(['color2' => asset(theme_color_user($user->theme_color, 'sidebar'))]);

        return view('user.dashboard', compact('rewardsUser', 'user', 'stage', 'active_bonus', 'contribution', 'monto', 'bonus'));
        //return Cookie::get('cookisprueba');
    }

    /**
     * Show the user account page.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function account($var = 1)
    {
        //dd($var);
        $countries = $this->handler->getCountries();
        $user = Auth::user();
        $userMeta = UserMeta::getMeta($user->id);

        $g2fa = new Google2FA();
        $google2fa_secret = $g2fa->generateSecretKey();
        $google2fa = $g2fa->getQRCodeUrl(
            site_info() . '-' . $user->name,
            $user->email,
            $google2fa_secret
        );

        return view('user.account', compact('user', 'userMeta', 'countries', 'google2fa', 'google2fa_secret', 'var'));
    }

    /**
     * Show the user account activity page.
     * and Delete Activity
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function account_activity()
    {
        $user = Auth::user();
        $activities = Activity::where('user_id', $user->id)->orderBy('created_at', 'DESC')->limit(50)->get();

        return view('user.activity', compact('user', 'activities'));
    }

    /**
     * Show the user account token management page.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.1.2
     *
     * @return void
     */
    public function mytoken_balance()
    {
        if (gws('user_mytoken_page') != 1) {
            return redirect()->route('user.home');
        }
        $user = Auth::user();
        $transaction = Transaction::where('user', $user->id)->get();
        $joined = $transaction->where('tnx_type', 'joined')->sum('total_tokens');
        $gift = $transaction->where('tnx_type', 'gift')->sum('total_tokens');
        $launch = $transaction->where('tnx_type', 'launch')->sum('total_tokens');
        $refund = $transaction->where('tnx_type', 'refund')->sum('total_tokens');
        $purchaseAll = $transaction->where('status', 'approved')->where('tnx_type', 'purchase')->sum('total_tokens');
        $sumatoria = $purchaseAll + $launch + $joined + $gift;
        $token_account = Transaction::user_mytoken('balance');
        $token_stages = Transaction::user_mytoken('stages');
        $user_modules = nio_module()->user_modules();
        $gift = Transaction::where('user', $user->id)->where('tnx_type', 'gift')->sum('total_tokens');
        $balance = $user->tokenBalance;
        $totalTokenGos = $gift + $balance;

        return view('user.account-token', compact('user', 'token_account', 'token_stages', 'user_modules', 'gift', 'launch', 'joined', 'refund', 'purchaseAll', 'sumatoria', 'totalTokenGos'));
    }

    /**
     * Activity delete
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function account_activity_delete(Request $request)
    {
        $id = $request->input('delete_activity');
        $ret['msg'] = 'info';
        $ret['message'] = 'Nothing to do!';

        if ($id !== 'all') {
            $remove = Activity::where('id', $id)->where('user_id', Auth::id())->delete();
        } else {
            $remove = Activity::where('user_id', Auth::id())->delete();
        }
        if ($remove) {
            $ret['msg'] = 'success';
            $ret['message'] = __('messages.delete.delete', ['what' => 'Activity']);
        } else {
            $ret['msg'] = 'warning';
            $ret['message'] = __('messages.form.wrong');
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * update the user account page.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.2
     *
     * @since 1.0
     *
     * @return void
     */
    public function account_update(Request $request)
    {
        $type = $request->input('action_type');
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($type == 'personal_data') {
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:3',
                'name' => 'required|min:3',
                'email' => 'required|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix',
                'dateOfBirth' => 'required|date_format:"m/d/Y"',
            ], [
                'email.regex' => ___('Please enter a valid email address.'),
            ]);

            if ($validator->fails()) {
                $msg = __('messages.form.wrong');
                if ($validator->errors()->hasAny(['name', 'email', 'dateOfBirth'])) {
                    $msg = $validator->errors()->first();
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $user = User::FindOrFail(Auth::id());
                $user->username = strip_tags($request->input('username'));
                $user->name = strip_tags($request->input('name'));
                $user->email = $request->input('email');
                // $user->mobile = strip_tags($request->input('mobile'));
                $user->dateOfBirth = $request->input('dateOfBirth');
                $user->nationality = strip_tags($request->input('nationality'));
                $user_saved = $user->save();
                $array = GosenHelper::paisesCantidadUsuarios();
                event(new AnalyticAdminEvent($array[0], $array[1]));
                if ($user) {
                    $ret['msg'] = 'success';
                    $ret['message'] = __('messages.update.success', ['what' => 'Account']);
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.update.warning');
                }
            }
        }
        if ($type == 'wallet') {
            $validator = Validator::make($request->all(), [
                'wallet_name' => 'required',
                'wallet_address' => 'required|min:10',
            ]);

            if ($validator->fails()) {
                $msg = __('messages.form.wrong');
                if ($validator->errors()->hasAny(['wallet_name', 'wallet_address'])) {
                    $msg = $validator->errors()->first();
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $is_valid = $this->handler->validate_address($request->input('wallet_address'), $request->input('wallet_name'));
                if ($is_valid) {
                    $user = User::FindOrFail(Auth::id());
                    $user->walletType = $request->input('wallet_name');
                    $user->walletAddress = $request->input('wallet_address');
                    $user_saved = $user->save();

                    if ($user) {
                        $ret['msg'] = 'success';
                        $ret['message'] = __('messages.update.success', ['what' => 'Wallet']);
                    } else {
                        $ret['msg'] = 'warning';
                        $ret['message'] = __('messages.update.warning');
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.invalid.address');
                }
            }
        }
        if ($type == 'wallet_request') {
            $validator = Validator::make($request->all(), [
                'wallet_name' => 'required',
                'wallet_address' => 'required|min:10',
            ]);

            if ($validator->fails()) {
                $msg = __('messages.form.wrong');
                if ($validator->errors()->hasAny(['wallet_name', 'wallet_address'])) {
                    $msg = $validator->errors()->first();
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $is_valid = $this->handler->validate_address($request->input('wallet_address'), $request->input('wallet_name'));
                if ($is_valid) {
                    $meta_data = ['name' => $request->input('wallet_name'), 'address' => $request->input('wallet_address')];
                    $meta_request = GlobalMeta::save_meta('user_wallet_address_change_request', json_encode($meta_data), auth()->id());

                    if ($meta_request) {
                        $ret['msg'] = 'success';
                        $ret['message'] = __('messages.wallet.change');
                    } else {
                        $ret['msg'] = 'warning';
                        $ret['message'] = __('messages.wallet.failed');
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.invalid.address');
                }
            }
        }
        if ($type == 'notification') {
            $notify_admin = $newsletter = $unusual = 0;

            if (isset($request['notify_admin'])) {
                $notify_admin = 1;
            }
            if (isset($request['newsletter'])) {
                $newsletter = 1;
            }
            if (isset($request['unusual'])) {
                $unusual = 1;
            }

            $user = User::FindOrFail(Auth::id());
            if ($user) {
                $userMeta = UserMeta::where('userId', $user->id)->first();
                if ($userMeta == null) {
                    $userMeta = new UserMeta();
                    $userMeta->userId = $user->id;
                }
                $userMeta->notify_admin = $notify_admin;
                $userMeta->newsletter = $newsletter;
                $userMeta->unusual = $unusual;
                $userMeta->save();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Notification']);
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.update.warning');
            }
        }
        if ($type == 'security') {
            $save_activity = $mail_pwd = 'FALSE';

            if (isset($request['save_activity'])) {
                $save_activity = 'TRUE';
            }
            if (isset($request['mail_pwd'])) {
                $mail_pwd = 'TRUE';
            }

            $user = User::FindOrFail(Auth::id());
            if ($user) {
                $userMeta = UserMeta::where('userId', $user->id)->first();
                if ($userMeta == null) {
                    $userMeta = new UserMeta();
                    $userMeta->userId = $user->id;
                }
                $userMeta->pwd_chng = $mail_pwd;
                $userMeta->save_activity = $save_activity;
                $userMeta->save();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Security']);
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.update.warning');
            }
        }
        if ($type == 'account_setting') {
            $notify_admin = $newsletter = $unusual = 0;
            $save_activity = $mail_pwd = 'FALSE';
            $user = User::FindOrFail(Auth::id());

            if (isset($request['save_activity'])) {
                $save_activity = 'TRUE';
            }
            if (isset($request['mail_pwd'])) {
                $mail_pwd = 'TRUE';
            }

            $mail_pwd = 'TRUE'; //by default true
            if (isset($request['notify_admin'])) {
                $notify_admin = 1;
            }
            if (isset($request['newsletter'])) {
                $newsletter = 1;
            }
            if (isset($request['unusual'])) {
                $unusual = 1;
            }

            if ($user) {
                $userMeta = UserMeta::where('userId', $user->id)->first();
                if ($userMeta == null) {
                    $userMeta = new UserMeta();
                    $userMeta->userId = $user->id;
                }

                $userMeta->notify_admin = $notify_admin;
                $userMeta->newsletter = $newsletter;
                $userMeta->unusual = $unusual;

                $userMeta->pwd_chng = $mail_pwd;
                $userMeta->save_activity = $save_activity;

                $userMeta->save();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Account Settings']);
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.update.warning');
            }
        }
        if ($type == 'pwd_change') {
            //validate data
            $validator = Validator::make($request->all(), [
                'old-password' => 'required|min:6',
                'new-password' => 'required|min:6',
                're-password' => 'required|min:6|same:new-password',
            ]);
            if ($validator->fails()) {
                $msg = __('messages.form.wrong');
                if ($validator->errors()->hasAny(['old-password', 'new-password', 're-password'])) {
                    $msg = $validator->errors()->first();
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $user = Auth::user();
                if ($user) {
                    if (!Hash::check($request->input('old-password'), $user->password)) {
                        $ret['msg'] = 'warning';
                        $ret['message'] = __('messages.password.old_err');
                    } else {
                        $userMeta = UserMeta::where('userId', $user->id)->first();
                        $userMeta->pwd_temp = Hash::make($request->input('new-password'));
                        $cd = Carbon::now();
                        $userMeta->email_expire = $cd->copy()->addDays(90);
                        $userMeta->email_token = str_random(65);
                        if ($userMeta->save()) {
                            try {
                                $user->notify(new PasswordChange($user, $userMeta));
                                $ret['msg'] = 'success';
                                $ret['message'] = __('messages.password.changed');
                            } catch (\Exception $e) {
                                $ret['msg'] = 'warning';
                                $ret['message'] = __('messages.email.password_change', ['email' => get_setting('site_email')]);
                            }
                        } else {
                            $ret['msg'] = 'warning';
                            $ret['message'] = __('messages.form.wrong');
                        }
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.form.wrong');
                }
            }
        }
        if ($type == 'google2fa_setup') {
            $google2fa = $request->input('google2fa', 0);
            $user = User::FindOrFail(Auth::id());
            if ($user) {
                // Google 2FA
                $ret['link'] = route('user.account', 2);
                if (!empty($request->google2fa_code)) {
                    $g2fa = new Google2FA();
                    if ($google2fa == 1) {
                        $verify = $g2fa->verifyKey($request->google2fa_secret, $request->google2fa_code);
                    } else {
                        $verify = $g2fa->verify($request->google2fa_code, $user->google2fa_secret);
                    }

                    if ($verify) {
                        $user->google2fa = $google2fa;
                        $user->google2fa_secret = ($google2fa == 1 ? $request->google2fa_secret : null);
                        $user->save();
                        $ret['msg'] = 'success';
                        $ret['message'] = ___('Successfully ' . ($google2fa == 1 ? 'enable' : 'disable') . ' 2FA security in your account.');
                    } else {
                        $ret['msg'] = 'error';
                        $ret['message'] = ___('You have provide a invalid 2FA authentication code!');
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = ___('Please enter a valid authentication code!');
                }
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function password_confirm($token)
    {
        $user = Auth::user();
        $userMeta = UserMeta::where('userId', $user->id)->first();
        if ($token == $userMeta->email_token) {
            if (_date($userMeta->email_expire, 'Y-m-d H:i:s') >= date('Y-m-d H:i:s')) {
                $user->password = $userMeta->pwd_temp;
                $user->save();
                $userMeta->pwd_temp = null;
                $userMeta->email_token = null;
                $userMeta->email_expire = null;
                $userMeta->save();

                $ret['msg'] = 'success';
                $ret['message'] = __('messages.password.success');
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.password.failed');
            }
        } else {
            $ret['msg'] = 'warning';
            $ret['message'] = __('messages.password.token');
        }

        return redirect()->route('user.account', 2)->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Get pay now form
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function get_wallet_form(Request $request)
    {
        return view('modals.user_wallet')->render();
    }

    /**
     * Show the user Referral page
     *
     * @version 1.0.0
     *
     * @since 1.0.3
     *
     * @return void
     */
    public function referral()
    {
        $page = Page::where('slug', 'referral')->first();
        $reffered = User::where('referral', auth()->id())->get();

        return view('user.referral', compact('page', 'reffered'));
    }

    public function uploadFoto(Request $request, User $imagen)
    {
        $request->validate([
            'foto' => 'required|mimes:jpeg,bmp,gif,png,webp,jpg|max:10240', //10Mb
        ]);
        //borrar la imagen actual si existe antes de cargar la nueva
        if ($imagen->foto != null) {
            Storage::disk('gosendisk')->delete('/imagenes/perfil/' . $imagen->foto);
        }
        $filename = time() . '.' . $request->foto->extension();
        $request->foto->move(rutaGuardado() . '/imagenes/perfil', $filename);
        $user = User::find(auth()->user()->id);
        $user->foto = $filename;
        $user->save();

        return back()->with('success', ___('Imagen cargada con exito!'));
    }

    public function uploadPerfil(Request $request)
    {
        $request->validate([
            'image' => 'required', //10Mb
        ]);
        $base64 = explode(',', $request->image);
        $imagen = base64_decode($base64[1]);
        // Borrar la imagen actual si existe antes de cargar la nueva
        $user = User::find(auth()->user()->id);
        if ($user->foto != null) {
            Storage::disk('gosendisk')->delete('/imagenes/perfil/' . $user->foto);
        }
        $filename = uniqid() . '.jpg';
        $filePath = '/imagenes/perfil/' . $filename;
        // Guardar la imagen original en Storage
        Storage::disk('gosendisk')->put($filePath, $imagen);
        // Abrir la imagen desde Storage
        $imagen = Image::make(Storage::disk('gosendisk')->get($filePath));
        // Ajustar la calidad y tamaño de la imagen
        $imagen->resize(720, 720, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
            ->encode('jpg', 80);
        // Guardar la imagen optimizada en Storage
        Storage::disk('gosendisk')->put($filePath, $imagen);
        // Actualizar el modelo de usuario con la nueva foto
        $user->foto = $filename;
        $user->save();
        $ret['estado'] = 'success';
        $ret['mensaje'] = ___('Se actualizó la foto correctamente');
        $ret['url'] = asset2('imagenes/perfil/' . $user->foto);

        return response()->json($ret);
    }

    public function eliminarfoto(User $imagen)
    {
        Storage::disk('gosendisk')->delete('/imagenes/perfil/' . $imagen->foto);
        $imagen->foto = null;
        $imagen->save();

        return back()->with('info', ___('Imagen eliminada con exito!'));
    }

    public function crearCookie()
    {
        Cookie::queue('cookiePrueba', true, 60 * 24 * 7);
    }

    public function topReferralindex()
    {
        $referrals = User::where('referral', '!=', 'null')->pluck('referral');
        $totalReferidos = $referrals->countBy();
        //dd($totalReferidos);
        $array = collect();
        $totalUserLoged = User::where('referral', auth()->user()->id)->count();
        //dd($totalUserLoged);
        $cont = 0;
        $win = 0;
        foreach ($totalReferidos as $idUser => $cantidad) {
            $user = User::find($idUser);
            if ($user) {
                $array->push([
                    'id' => $user->id,
                    'username' => $user->username,
                    'nombre' => $user->name,
                    'foto' => $user->foto,
                    'cantidad' => $cantidad,

                ]);
            }
        }

        $array = $array->sortBy([
            ['cantidad', 'desc'],

        ]);
        foreach ($array as $arr) {
            $cont++;
            if ($cont == 5) {
                $win = $arr['cantidad'];
                break;
            }
        }
        $array->push([
            'id' => auth()->user()->id,
            'username' => auth()->user()->username,
            'nombre' => auth()->user()->name,
            'foto' => auth()->user()->foto,
            'cantidad' => $totalUserLoged,
        ]);
        if ($win - $totalUserLoged == 0) {
            $cantfalta = 1;
        } else {
            $cantfalta = $win - $totalUserLoged;
        }
        $emojis = ['👑', '🏆', '⭐️', '🤩', '🔥', '👏', '🎉', '🙌', '💯', '👍', '👍'];

        return view('user.user-top-referrals', compact('array', 'emojis', 'cantfalta'));
    }

    public function marketing()
    {
        return view('user.marketing');
    }

    public function bonusDolares()
    {
        $tipoWithdraw = BonusDolar::TYPE1;
        $sum = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', $tipoWithdraw)->sum('amount');
        $bonus = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', $tipoWithdraw)->orderBy('created_at', 'desc')->paginate(5);
        $trnxWith = BonusDolar::where('user_id', auth()->user()->id)->where('type', '=', $tipoWithdraw)->orderBy('created_at', 'desc')->get();
        $monto = auth()->user()->dolarBonus;
        $user = User::find(auth()->user()->id);
        $totalRequestAmount = $user->bonus->where('status', 'pending')->where('type', BonusDolar::TYPE1)->sum('amount');
        $withdrawn = BonusDolar::where([
            ['user_id', auth()->user()->id],
            ['status', '=', BonusDolar::STATUS2],
            ['type', '=', $tipoWithdraw],
        ])->sum('amount');

        return view('user.bonus.show-bonus', compact('bonus', 'monto', 'trnxWith', 'sum', 'withdrawn', 'totalRequestAmount'));
    }

    public function withdrawBonus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric|min:1',
            'wallet' => 'required|string|min:20',
        ]);
        if ($request->amount >= 10) {
            if (auth()->user()->dolarBonus < $request->amount) {
                $ret['msg'] = 'error';
                $ret['message'] = ___('Withdrawal amount must be less than or equal to your dollar balance. Your current balance is: : ' . auth()->user()->dolarBonus . ' (USDT)');
                return response()->json($ret);
            } else {
                if ($validator->fails()) {
                    $ret['msg'] = 'error';
                    $ret['message'] = 'The form data is incorrect';

                    return response()->json($ret);
                }
                $stage = active_stage()->id;
                $user = User::find(auth()->user()->id);
                $totalRequestAmount = $user->bonus->where('status', 'pending')->where('type', BonusDolar::TYPE1)->sum('amount');
                $totalRequested = $totalRequestAmount + $request->amount;
                if ($totalRequested > $user->dolarBonus) {
                    $ret['msg'] = 'error';
                    $ret['message'] = ___('Your total withdrawal requests exceeded your current balance');

                    return response()->json($ret);
                }
                $bonus = BonusDolar::create([
                    'stage_id' => $stage,
                    'transaction_id' => 0,
                    'user_id' => $user->id,
                    'amount' => $request->amount,
                    'checked_by' => 0,
                    'type_value' => 0,
                    'type_bonus' => '',
                    'status' => BonusDolar::STATUS1,
                    'user_from' => 0,
                    'approved_bonus' => false,
                    'type' => BonusDolar::TYPE1,
                    'wallet' => $request->wallet,
                ]);
                $bonus->code = BonusDolar::BONUSCODEWTD . $bonus->id;
                $bonus->save();
                $user = User::find($user->id);
                Mail::to($user->email)
                    ->send(new WithdrawGosenRequestMail($bonus));
                // GosenHelper::createNotification(auth()->user()->id, GosenHelper::contenidoNotificacion('retiro_usd_pendiente', [$request->amount, 0, 0]));
                $ret['msg'] = 'success';
                $ret['message'] = ___('Your transaction was successfully requested');

                return response()->json($ret);
            }
        } else {
            $ret['msg'] = 'error';
            $ret['message'] = ___('El monto minimo para retiro es de 10 (USDT)');
            return response()->json($ret);
        }
    }

    public function exportarPDF(Request $request)
    {
        $tipo = $request->tipo;
        //contadores
        $contadorDolares = $request->contadorDolares;
        $contadorBonusStart = $request->contadorBonusStart;
        $contadorGosToken = $request->contadorGosToken;
        $contadorBalanceGosToken = $request->contadorBalanceGosToken;
        //totalesTokens
        $totalTokensUsd = '~';
        $totalTokensBonusStart = $request->totalTokensBonusStart;
        $totalTokensGosToken = $request->totalTokensGosToken;
        $totalTokensBalanceGosTokens = $request->toatlTokensBalanceGosTokens;
        //totales USD Amount
        $totalUsdAmount = $request->totalUsdAmount;
        $totalUsdAmountBonusStart = $request->totalUsdAmountBonusStart;
        $totalUsdAmountGosToken = $request->totalUsdAmountGosToken;
        $totalUsdAmountBalanceGosToken = $request->totalUsdAmountBalanceGosToken;

        switch ($tipo) {
            case '1':
                //consulta para todos los bonos
                $trnxs = Transaction::select(
                    'transactions.*',
                    'users.name'
                )
                    ->leftjoin('users', 'users.id', 'transactions.user')
                    ->where('user', auth()->user()->id)
                    ->where('transactions.status', '!=', 'deleted')
                    ->where('transactions.status', '!=', 'new')
                    ->whereNotIn('tnx_type', ['withdraw'])
                    ->orderBy('created_at', 'DESC')->get();
                $pdf = PDF::loadView('pdf.pdf-bonus-dollars', compact('trnxs', 'tipo', 'contadorBalanceGosToken', 'totalTokensBalanceGosTokens', 'totalUsdAmountBalanceGosToken'))->setPaper('A3', 'landscape');
                break;
            case '2':
                // consultas para purchase y bonus de compra
                $trnxPurBonus = Transaction::select(
                    'transactions.*',
                    'users.name'
                )
                    ->leftjoin('users', 'users.id', 'transactions.user')
                    ->where('user', auth()->user()->id)
                    ->whereNotIn('tnx_type', ['launch', 'joined'])
                    ->orderBy('created_at', 'DESC')->get();
                $pdf = PDF::loadView('pdf.pdf-bonus-dollars', compact('trnxPurBonus', 'tipo', 'contadorGosToken', 'totalTokensGosToken', 'totalUsdAmountGosToken'))->setPaper('A3', 'landscape');
                break;
            case '3':
                // consultas para el bonus start
                $trnxBonusStart = Transaction::where('user', auth()->user()->id)
                    ->whereIn('tnx_type', ['launch', 'joined'])
                    ->orderBy('created_at', 'DESC')->get();
                $pdf = PDF::loadView('pdf.pdf-bonus-dollars', compact('trnxBonusStart', 'tipo', 'contadorBonusStart', 'totalTokensBonusStart', 'totalUsdAmountBonusStart'))->setPaper('A3', 'landscape');
                break;
            case '4':
                // consultas para el bonus dollars
                $trnxBonusDollarsAll = BonusDolar::select(
                    'bonus_dolars.*',
                    'transactions.tnx_id',
                    'users.name'
                )
                    ->leftjoin('users', 'users.id', 'bonus_dolars.user_id')
                    ->leftjoin('transactions', 'transactions.id', 'bonus_dolars.transaction_id')
                    ->where('user_id', auth()->user()->id)
                    ->orderBy('created_at', 'desc')->get();
                $pdf = PDF::loadView('pdf.pdf-bonus-dollars', compact('trnxBonusDollarsAll', 'tipo', 'contadorDolares', 'totalTokensUsd', 'totalUsdAmount'))->setPaper('A3', 'landscape');
                break;
            default:

                break;
        }

        return $pdf->stream('Report/' . date('d-m-Y') . '.pdf');
    }

    public function confirmUsdWithdraw($id)
    {
        $transactionDolar = BonusDolar::findOrFail(Crypt::decrypt($id));
        $fechaTransaction = $transactionDolar->created_at->format('Y-m-d H:i:s');
        $diferencia = Carbon::now()->diffInHours($fechaTransaction);
        if ($diferencia <= 24 && $transactionDolar->approved_bonus == 0) {
            try {
                Mail::to($transactionDolar->userBy->email)
                    ->send(new ConfirmUserWithdrawBonusDollarsMail($transactionDolar));
                $transactionDolar->approved_bonus = 1;
                $transactionDolar->save();
                GosenHelper::createNotification($transactionDolar->user_id, GosenHelper::contenidoNotificacion('aprobacion_user_retiro_usd', [$transactionDolar->amount, $transactionDolar->code, 0]));

                return redirect()->route('home');
            } catch (\Throwable $e) {
                if ($e instanceof NotFoundHttpException) {
                    return response()->view('errors/500', ['invalid_url' => true], 500);
                }
            }
        } else {
            return response()->view('errors/404', ['invalid_url' => true], 404);
        }
    }
}
