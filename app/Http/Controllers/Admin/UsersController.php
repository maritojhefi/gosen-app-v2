<?php

namespace App\Http\Controllers\Admin;

/**
 * Users Controller
 *
 * @author Softnio
 *
 * @version 1.1.3
 */

use App\Events\AnalyticTypeUsersAdminEvent;
use App\Exports\UsersExport;
use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Mail\EmailToUser;
use App\Models\GlobalMeta;
use App\Models\Menu;
use App\Models\Paquete;
use App\Models\User;
use App\Models\UserMeta;
use App\Notifications\ConfirmEmail;
use App\Notifications\PasswordResetByAdmin;
use App\Notifications\Reset2FA;
use App\Notifications\UserRegistered;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.1
     *
     * @since 1.0
     *
     * @return void
     */
    public function index(Request $request, $role = '')
    {
        $role_data = '';
        $per_page = gmvl('user_per_page', 10);
        $order_by = (gmvl('user_order_by', 'id') == 'token') ? 'tokenBalance' : gmvl('user_order_by', 'id');
        $ordered = gmvl('user_ordered', 'DESC');
        $is_page = (empty($role) ? 'all' : ($role == 'user' ? 'investor' : $role));

        if (!empty($role)) {
            $users = User::with('kyc_info')->whereNotIn('status', ['deleted'])->where('role', $role)->orderBy($order_by, $ordered)->paginate($per_page);
        } else {
            $users = User::with('kyc_info')->whereNotIn('status', ['deleted'])->orderBy($order_by, $ordered)->paginate($per_page);
        }

        if ($request->s) {
            $users = User::AdvancedFilter($request)->with('kyc_info')
                ->orderBy($order_by, $ordered)->paginate($per_page);
        }

        if ($request->filter) {
            $users = User::AdvancedFilter($request)->with('kyc_info')
                ->orderBy($order_by, $ordered)->paginate($per_page);
        }

        $pagi = $users->appends(request()->all());
        $menu = Menu::all();

        return view('admin.users', compact('users', 'role_data', 'is_page', 'pagi', 'menu'));
    }

    /**
     * Send email to specific user
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.1
     *
     * @since 1.0
     *
     * @return void
     */
    public function send_email(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ], [
            'user_id.required' => ___('Select a user first!'),
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('name')) {
                $msg = $validator->errors()->first();
            } else {
                $msg = __('messages.somthing_wrong');
            }

            $ret['msg'] = 'warning';
            $ret['message'] = $msg;
        } else {
            $user = User::FindOrFail($request->input('user_id'));

            if ($user) {
                $msg = $request->input('message');
                $msg = replace_with($msg, '[[user_name]]', $user->name);
                $data = (object) [
                    'user' => (object) ['name' => $user->name, 'email' => $user->email],
                    'subject' => $request->input('subject'),
                    'greeting' => $request->input('greeting'),
                    'text' => str_replace("\n", '<br>', $msg),
                ];
                $when = now()->addMinutes(2);

                try {
                    Mail::to($user->email)
                        ->later($when, new EmailToUser($data));
                    $ret['msg'] = 'success';
                    $ret['message'] = __('messages.mail.send');
                } catch (\Exception $e) {
                    $ret['errors'] = $e->getMessage();
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.mail.issues');
                }
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.mail.failed');
            }

            if ($request->ajax()) {
                return response()->json($ret);
            }

            return back()->with([$ret['msg'] => $ret['message']]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.2
     *
     * @since 1.0
     *
     * @return void
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'username' => 'required|min:3',
            'name' => 'required|min:3',
            'email' => 'required|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix|unique:users,email',
            'password' => 'nullable|min:6',
        ], [
            'email.unique' => __('messages.email.unique'),
            'email.regex' => ___('Please enter a valid email address.'),
        ]);

        if ($validator->fails()) {
            $msg = '';
            if ($validator->errors()->hasAny(['name', 'email', 'password'])) {
                $msg = $validator->errors()->first();
            } else {
                $msg = __('messages.somthing_wrong');
            }

            $ret['msg'] = 'warning';
            $ret['message'] = $msg;

            return response()->json($ret);
        } else {
            if ($request->input('role') == 'admin' && !super_access()) {
                $ret['msg'] = 'warning';
                $ret['message'] = ___('You do not have enough permissions to perform requested operation.');
            } else {
                $req_password = $request->input('password') ? $request->input('password') : str_random(12);
                $password = Hash::make($req_password);
                $lastLogin = date('Y-m-d H:i:s');
                $user = User::create([
                    'username' => $request->input('username'),
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => $password,
                    'role' => $request->input('role'),
                    'lastLogin' => $lastLogin,
                ]);

                if ($user) {
                    $user->email_verified_at = isset($request->email_req) ? null : date('Y-m-d H:i:s');
                    $user->registerMethod = 'Internal';
                    // $user->referral = ($user->id.'.'.str_random(50));
                    $user->save();
                    $meta = UserMeta::create([
                        'userId' => $user->id,
                    ]);
                    $meta->notify_admin = ($request->input('role') == 'user') ? 0 : 1;
                    $meta->email_token = str_random(65);
                    $meta->email_expire = now()->addDays(90);
                    $meta->save();
                    $extra = (object) [
                        'username' => $user->username,
                        'name' => $user->name,
                        'email' => $user->email,
                        'password' => $req_password,
                    ];
                    $global_meta = GlobalMeta::create([
                        'pid' => $user->id,
                        'name' => 'manage_access',
                    ]);
                    $global_meta->save();

                    try {
                        if (isset($request->email_req)) {
                            $user->notify(new ConfirmEmail($user, $extra));
                        }
                        // $user->notify(new AddUserEmail($user));
                        //$ret['link'] = route('admin.user-create');
                        $ret['msg'] = 'success';
                        $ret['message'] = __('messages.insert.success', ['what' => 'User']);
                    } catch (\Exception $e) {
                        $ret['errors'] = $e->getMessage();
                        $ret['link'] = route('admin.user-create');
                        $ret['msg'] = 'warning';
                        $ret['message'] = __('messages.insert.success', ['what' => 'User']) . ' ' . __('messages.email.failed');
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.insert.warning', ['what' => 'User']);
                }
            }

            if ($request->ajax()) {
                return response()->json($ret);
            }

            return back()->with([$ret['msg'] => $ret['message']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @param  string  $type
     * @return \Illuminate\Http\Response
     *
     * @throws \Throwable
     *
     * @version 1.1
     *
     * @since 1.0
     */
    public function show(Request $request, $id = null, $type = null)
    {
        if ($request->ajax()) {
            $id = $request->input('uid');
            $type = $request->input('req_type');
            $user = User::FindOrFail($id);
            if ($type == 'transactions') {
                $transactions = \App\Models\Transaction::where(['user' => $id, 'status' => 'approved'])->latest()->get();

                return view('modals.user_transactions', compact('user', 'transactions'))->render();
            }
            if ($type == 'activities') {
                $activities = \App\Models\Activity::where('user_id', $id)->latest()->limit(15)->get();

                return view('modals.user_activities', compact('user', 'activities'))->render();
            }
            // v1.1
            if ($type == 'referrals') {
                $refered = User::where('referral', $user->id)->get(['id', 'name', 'created_at']);
                foreach ($refered as $refer) {
                    $ref_count = User::where('referral', $refer->id)->count();
                    if ($ref_count > 0) {
                        $refer->refer_to = $ref_count;
                    } else {
                        $refer->refer_to = 0;
                    }
                }

                return view('modals.user_referrals', compact('user', 'refered'))->render();
            }
        }

        $user = User::FindOrFail($id);
        if ($type == 'details') {
            $refered = User::FindOrFail($id)->referrals();
            $transacciones = $user->transactions->where('tnx_type', 'purchase');

            $tipospaquetes = Paquete::where('estado', 1)->get();
            $transaccionesportipo = $user->transactions->where('tnx_type', 'purchase')->where('status', 'approved');
            return view('admin.user_details', compact('user', 'refered', 'transacciones', 'transaccionesportipo', 'tipospaquetes'))->render();
        }
        if ($type == 'type_user') {
            $menu = Menu::all();

            return view('admin.user_type', compact('user', 'menu'))->render();
        }
    }

    public function status(Request $request)
    {
        $id = $request->input('uid');
        $type = $request->input('req_type');

        if (!super_access()) {
            $up = User::where('id', $id)->first();
            if ($up) {
                if ($up->role != 'user') {
                    $result['msg'] = 'warning';
                    $result['message'] = ___('You do not have enough permissions to perform requested operation.');

                    return response()->json($result);
                }
            }
        }

        if ($type == 'suspend_user') {
            $admin_count = User::where('role', 'admin')->count();
            if ($admin_count >= 1) {
                $up = User::where('id', $id)->update([
                    'status' => 'suspend',
                ]);
                if ($up) {
                    $result['msg'] = 'warning';
                    $result['css'] = 'danger';
                    $result['status'] = 'active_user';
                    $result['message'] = 'User Suspend Success!!';
                } else {
                    $result['msg'] = 'warning';
                    $result['message'] = 'Failed to Suspend!!';
                }
            } else {
                $result['msg'] = 'warning';
                $result['message'] = 'Minimum one admin account is required!';
            }

            return response()->json($result);
        }
        if ($type == 'active_user') {
            $up = User::where('id', $id)->update([
                'status' => 'active',
            ]);
            if ($up) {
                $result['msg'] = 'success';
                $result['css'] = 'success';
                $result['status'] = 'suspend_user';
                $result['message'] = 'User Active Success!!';
            } else {
                $result['msg'] = 'warning';
                $result['message'] = 'Failed to Active!!';
            }

            return response()->json($result);
        }
        if ($type == 'reset_pwd') {
            $pwd = str_random(15);
            $up = User::where('id', $id)->first();
            $up->password = Hash::make($pwd);

            $update = (object) [
                'new_password' => $pwd,
                'name' => $up->name,
                'email' => $up->email,
                'id' => $up->id,
            ];
            if ($up->save()) {
                try {
                    $up->notify(new PasswordResetByAdmin($update));
                    $result['msg'] = 'success';
                    $result['message'] = 'Password Changed!! ';
                } catch (\Exception $e) {
                    $ret['errors'] = $e->getMessage();
                    $result['msg'] = 'warning';
                    $result['message'] = 'Password Changed!! but user was not notified. Please! check your email setting and try again.';
                }
            } else {
                $result['msg'] = 'warning';
                $result['message'] = 'Failed to Changed!!';
            }

            return response()->json($result);
        }
        if ($type == 'reset_2fa') {
            $user = User::where('id', $id)->first();
            if ($user) {
                $user->notify(new Reset2FA($user));
                $result['msg'] = 'success';
                $result['message'] = '2FA confirmation email send to the user.';
            } else {
                $result['msg'] = 'warning';
                $result['message'] = 'Failed to reset 2FA!!';
            }

            return response()->json($result);
        }
        if ($type == 'verify_email') {
            $user = User::where('id', $id)->first();
            if ($user) {
                $user->email_verified_at = now();
                $user->meta->email_token = null;
                $user->meta->email_expire = null;
                $user->save();
                $user->meta->save();
                $result['reload'] = true;
                try {
                    $user->notify(new UserRegistered($user));
                    $result['msg'] = 'success';
                    $result['message'] = ___('Email Verified');
                } catch (\Exception $e) {
                    $result['errors'] = $e->getMessage();
                    $result['msg'] = 'warning';
                    $result['message'] = ___('Email Verified') . '. ' . __('messages.email.failed');
                }
            } else {
                $result['msg'] = 'warning';
                $result['message'] = 'Failed to verify email!!';
            }

            return response()->json($result);
        }
    }

    /**
     * wallet change request
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     */
    public function wallet_change_request()
    {
        $meta_data = GlobalMeta::where('name', 'user_wallet_address_change_request')->get();

        return view('admin.user-request', compact('meta_data'));
    }

    public function wallet_change_request_action(Request $request)
    {
        $meta = GlobalMeta::FindOrFail($request->id);
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');
        if ($meta) {
            $action = $request->action;

            if ($action == 'approve') {
                $meta->user->walletType = $meta->data()->name;
                $meta->user->walletAddress = $meta->data()->address;

                $meta->user->save();
                $meta->delete();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.wallet.approved');
            }
            if ($action == 'reject') {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.wallet.cancel');
                $meta->delete();
            }
        } else {
            $ret['msg'] = 'warning';
            $ret['message'] = __('messages.wallet.failed');
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Delete all unverified users
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     */
    public function delete_unverified_user(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        $user = User::where(['registerMethod' => 'Email', 'email_verified_at' => null])->get();
        if ($user->count()) {
            $data = $user->each(function ($item) {
                $item->meta()->delete();
                $item->logs()->delete();
                $item->delete();
            });

            if ($data) {
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.delete.delete', ['what' => 'Unvarified users']);
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.delete.delete_failed', ['what' => 'Unvarified users']);
            }
        } else {
            $ret['msg'] = 'success';
            $ret['alt'] = 'no';
            $ret['message'] = ___('There has not any unvarified users!');
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function exportarExcel()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function exportarPDF()
    {
        $user = User::all();
        $pdf = PDF::loadView('pdf.pdf-users', compact('user'))->setPaper('A3', 'landscape');

        return $pdf->stream('Users' . date('d-m-Y') . '.pdf');
    }

    public function create()
    {
        return view('admin.user-create');
    }

    public function menu(Request $request, $id = null)
    {
        User::where('id', $id)
            ->update(['type_user' => request('menu_show_as') - 1, 'theme_color' => 'style']);
        $array = GosenHelper::tiposCantidadUsuarios();
        event(new AnalyticTypeUsersAdminEvent($array[0], $array[1]));

        return redirect()->route('admin.users', 'user');

        //return back();
    }

    public function loginAsUser($id)
    {
        session(['adminId' => auth()->user()->id]);
        Auth::loginUsingId(Crypt::decrypt($id));

        return redirect('/user');
    }

    public function topReferrals(Request $request, $role = '')
    {
        $referrals = User::where('referral', '!=', 'null')->pluck('referral');
        $totalReferidos = $referrals->countBy();
        $array = collect();
        foreach ($totalReferidos as $idUser => $cantidad) {
            $user = User::find($idUser);
            $array->push([
                'id' => $user->id,
                'nombre' => $user->name,
                'cantidad' => $cantidad,

            ]);
        }
        //dd($array);
        $array = $array->sortBy([
            ['cantidad', 'desc'],

        ]);

        return view('admin.admin-top-referrals', compact('array'));
    }

    public function UserTopReferrals(Request $request, $role = '')
    {
        $referrals = User::where('referral', '!=', 'null')->pluck('referral');
        $totalReferidos = $referrals->countBy();
        $array = collect();
        foreach ($totalReferidos as $idUser => $cantidad) {
            $user = User::find($idUser);
            $array->push([
                'id' => $user->id,
                'nombre' => $user->name,
                'cantidad' => $cantidad,

            ]);
        }
        //dd($array);
        $array = $array->sortBy([
            ['cantidad', 'desc'],

        ]);

        return view('user.user-top-referrals', compact('array'));
    }
}
