<?php

namespace App\Http\Controllers\Admin;

/**
 * Admin Controller
 *
 * @author Softnio
 *
 * @version 1.0.3
 */

use App\Helpers\GosenHelper;
use App\Helpers\IcoHandler;
use App\Http\Controllers\Controller;
use App\Jobs\SendMassiveMailsJob;
use App\Models\Activity;
use App\Models\HistorySendedMail;
use App\Models\Language;
use App\Models\Menu;
use App\Models\Page;
use App\Models\User;
use App\Models\UserMeta;
use App\Notifications\PasswordChange;
use Auth;
use Carbon\Carbon;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FA\Google2FA;

class AdminController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $get_tnx = ($request->get('chart') ? $request->get('chart') : 15) - 1;
        $get_user = ($request->get('user') ? $request->get('user') : 15) - 1;
        $stage = \App\Models\IcoStage::dashboard();
        $users = User::dashboard($get_user);
        $trnxs = \App\Models\Transaction::dashboard($get_tnx);

        if (isset($request->user)) {
            $data = $users;
        } elseif (isset($request->chart)) {
            $data = $trnxs;
        } else {
            $data = null;
        }
        if ($request->ajax()) {
            return response()->json((empty($data) ? [] : $data));
        }
        if (gup('translator') && ! is_super_admin(auth()->user()->id, true)) {
            return redirect(route('admin.diccionario'));
        } else {
            return view('admin.dashboard', compact('stage', 'users', 'trnxs'));
        }
    }

    /**
     * Show the application User Profile.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function profile()
    {
        $user = Auth::user();
        $userMeta = UserMeta::getMeta($user->id);

        $g2fa = new Google2FA();
        $google2fa_secret = $g2fa->generateSecretKey();
        $google2fa = $g2fa->getQRCodeUrl(
            site_info().'-'.$user->name,
            $user->email,
            $google2fa_secret
        );

        return view('admin.profile', compact('user', 'userMeta', 'google2fa', 'google2fa_secret'));
    }

    /**
     * Show the application User Profile.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function profile_update(Request $request)
    {
        $type = $request->input('action_type');
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($type == 'personal_data') {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:4',
            ]);

            if ($validator->fails()) {
                $msg = '';
                if ($validator->errors()->has('name')) {
                    $msg = $validator->errors()->first();
                } else {
                    $msg = __('messages.form.wrong');
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $user = User::FindOrFail(Auth::id());
                $user->name = $request->input('name');
                //$user->email = $request->input('email');
                $user->mobile = $request->input('mobile');
                $user_saved = $user->save();

                if ($user) {
                    $ret['msg'] = 'success';
                    $ret['message'] = __('messages.update.success', ['what' => 'Profile']);
                } else {
                    $ret['msg'] = 'error';
                    $ret['message'] = __('messages.update.warning');
                }
            }
        }

        if ($type == 'notification') {
            $notify_admin = $newsletter = $unusual = 0;

            if (isset($request['newsletter'])) {
                $newsletter = 1;
            }
            if (isset($request['unusual'])) {
                $unusual = 1;
            }

            $user = User::FindOrFail(Auth::id());
            if ($user) {
                $userMeta = UserMeta::where('userId', $user->id)->first();
                if ($userMeta == null) {
                    $userMeta = new UserMeta();
                    $userMeta->userId = $user->id;
                }
                $userMeta->notify_admin = $notify_admin;
                $userMeta->newsletter = $newsletter;
                $userMeta->unusual = $unusual;
                $userMeta->save();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Notification']);
            } else {
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.update.warning');
            }
        }
        if ($type == 'security') {
            $save_activity = $mail_pwd = 'FALSE';
            $unusual = $notify_admin = 0;

            if (isset($request['notify_admin'])) {
                $notify_admin = 1;
            }
            if (isset($request['save_activity'])) {
                $save_activity = 'TRUE';
            }
            if (isset($request['mail_pwd'])) {
                $mail_pwd = 'TRUE';
            }

            $mail_pwd = 'TRUE';

            if (isset($request['unusual'])) {
                $unusual = 1;
            }

            $user = User::FindOrFail(Auth::id());
            if ($user) {
                $userMeta = UserMeta::where('userId', $user->id)->first();
                if ($userMeta == null) {
                    $userMeta = new UserMeta();
                    $userMeta->userId = $user->id;
                }
                $userMeta->unusual = $unusual;
                $userMeta->pwd_chng = $mail_pwd;
                $userMeta->save_activity = $save_activity;
                $userMeta->notify_admin = $notify_admin;
                $userMeta->save();
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Security']);
            } else {
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.update.warning');
            }
        }
        if ($type == 'pwd_change') {
            //validate data
            $validator = Validator::make($request->all(), [
                'old-password' => 'required|min:6',
                'new-password' => 'required|min:6',
                're-password' => 'required|min:6|same:new-password',
            ]);
            if ($validator->fails()) {
                $msg = '';
                if ($validator->errors()->hasAny(['old-password', 'new-password', 're-password'])) {
                    $msg = $validator->errors()->first();
                } else {
                    $msg = __('messages.form.wrong');
                }

                $ret['msg'] = 'warning';
                $ret['message'] = $msg;

                return response()->json($ret);
            } else {
                $user = User::FindOrFail(Auth::id());
                if ($user) {
                    if (! Hash::check($request->input('old-password'), $user->password)) {
                        $ret['msg'] = 'warning';
                        $ret['message'] = __('messages.password.old_err');
                    } else {
                        $userMeta = UserMeta::where('userId', $user->id)->first();
                        $userMeta->pwd_temp = Hash::make($request->input('new-password'));
                        $cd = Carbon::now();
                        $userMeta->email_expire = $cd->copy()->addDays(90);
                        $userMeta->email_token = str_random(65);
                        if ($userMeta->save()) {
                            try {
                                $user->notify(new PasswordChange($user, $userMeta));
                                $ret['msg'] = 'success';
                                $ret['message'] = __('messages.password.changed');
                            } catch (\Exception $e) {
                                $ret['msg'] = 'warning';
                                $ret['message'] = __('messages.email.password_change', ['email' => get_setting('site_email')]);
                            }
                        } else {
                            $ret['msg'] = 'error';
                            $ret['message'] = __('messages.form.wrong');
                        }
                    }
                } else {
                    $ret['msg'] = 'error';
                    $ret['message'] = __('messages.form.wrong');
                }
            }
        }

        if ($type == 'google2fa_setup') {
            $google2fa = $request->input('google2fa', 0);
            $user = User::FindOrFail(Auth::id());
            if ($user) {
                // Google 2FA
                $ret['link'] = route('user.account', 2);
                if (! empty($request->google2fa_code)) {
                    $g2fa = new Google2FA();
                    if ($google2fa == 1) {
                        $verify = $g2fa->verifyKey($request->google2fa_secret, $request->google2fa_code);
                    } else {
                        $verify = $g2fa->verify($request->google2fa_code, $user->google2fa_secret);
                    }

                    if ($verify) {
                        $user->google2fa = $google2fa;
                        $user->google2fa_secret = ($google2fa == 1 ? $request->google2fa_secret : null);
                        $user->save();
                        $ret['msg'] = 'success';
                        $ret['message'] = ___('Successfully '.($google2fa == 1 ? 'enable' : 'disable').' 2FA security in your account.');
                    } else {
                        $ret['msg'] = 'error';
                        $ret['message'] = ___('You provided a invalid 2FA authentication code!');
                    }
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = ___('Please enter a valid authentication code!');
                }
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * @version 1.1
     *
     * @since 1.1.0
     */
    public function system_info(Request $request)
    {
        return view('admin.system');
    }

    /**
     * @version 1.0
     *
     * @since 1.1.0
     */
    public function treatment(Request $request)
    {
        $handle = (new IcoHandler());
        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'purchase_code' => 'required|min:24|max:40',
            ], [
                'name.required' => ___('Envato Username is required for activation.'),
                'email.required' => ___('Your Email address is required for activation.'),
                'email.email' => ___('Please enter a valid email address.'),
                'purchase_code.required' => ___('Envato Purchase Code is required for activation.'),
                'purchase_code.min' => ___('Please enter a valid purchase code.'),
            ]);

            if ($validator->fails()) {
                $error = ['msg' => 'warning', 'message' => $validator->errors()->first()];
                if ($request->ajax()) {
                    return response()->json($error);
                }

                return back()->with($error);
            }

            return $handle->checkHelth($request);
        }
        if (is_demo_user() || is_demo_preview()) {
            $error['warning'] = (is_demo_preview()) ? __('messages.demo_preview') : __('messages.demo_user');

            return redirect()->route('admin.system')->with($error);
        }
        if ($request->skip && $request->skip == 'reg') {
            Cookie::queue(Cookie::make('ico_nio_reg_skip', 1, 1440));
            $last = (int) get_setting('piks_ger_oin_oci', 0);
            add_setting('piks_ger_oin_oci', $last + 1);

            return redirect()->route('admin.home');
        }
        if ($request->revoke && $request->revoke == 'license') {
            delete_setting(['env_pcode', 'nio_lkey', 'nio_email', 'env_uname', 'env_ptype']);
            add_setting('tokenlite_update', time());
            add_setting('tokenlite_credible', str_random(48));
            add_setting('site_api_secret', str_random(16));
            Cookie::queue(Cookie::forget('ico_nio_reg_skip'));

            return redirect()->route('admin.home');
        }
        if ($handle->check_body() && str_contains(app_key(), $handle->find_the_path($handle->getDomain()))) {
            return redirect()->route('admin.home');
        }

        return view('auth.chamber');
    }

    public function password_confirm($token)
    {
        $user = Auth::user();
        $userMeta = UserMeta::where('userId', $user->id)->first();
        if ($token == $userMeta->email_token) {
            if (_date($userMeta->email_expire, 'Y-m-d H:i:s') >= date('Y-m-d H:i:s')) {
                $user->password = $userMeta->pwd_temp;
                $user->save();
                $userMeta->pwd_temp = null;
                $userMeta->email_token = null;
                $userMeta->email_expire = null;
                $userMeta->save();

                $ret['msg'] = 'success';
                $ret['message'] = __('messages.password.success');
            } else {
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.password.failed');
            }
        } else {
            $ret['msg'] = 'error';
            $ret['message'] = __('messages.password.token');
        }

        return redirect()->route('admin.profile')->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Show the user account activity page.
     * and Delete Activity
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function activity()
    {
        $user = Auth::user();
        $activities = Activity::where('user_id', $user->id)->orderBy('created_at', 'DESC')->limit(50)->get();

        return view('admin.activity', compact('user', 'activities'));
    }

    /**
     * Delete activity
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function activity_delete(Request $request)
    {
        $id = $request->input('delete_activity');
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($id !== 'all') {
            $remove = Activity::where('id', $id)->where('user_id', Auth::id())->delete();
        } else {
            $remove = Activity::where('user_id', Auth::id())->delete();
        }
        if ($remove) {
            $ret['msg'] = 'success';
            $ret['message'] = __('messages.delete.delete', ['what' => 'Activity']);
        } else {
            $ret['msg'] = 'error';
            $ret['message'] = __('messages.form.wrong');
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function massiveMailCreator()
    {
        $users = User::where('email_verified_at', '!=', null)->get();
        $tiposUsuario = Menu::all();
        $lenguages = Language::where('status', true)->get();

        return view('admin.massiveMails.massive-mail-creator', compact('lenguages', 'tiposUsuario', 'users'));
    }

    public function historyMailSent()
    {
        $historys = HistorySendedMail::all()->sortByDesc('created_at');

        return view('admin.massiveMails.history-mail-sent', compact('historys'));
    }

    public function sendMails(Request $request)
    {
        // dd($request);
        try {
            if (isset($request->type_mail) && $request->type_mail == 1) {
                $tipoCorreo = 'design';
                $validator = Validator::make($request->all(), [
                    'content' => 'required',
                    'header' => 'required|string|max:100',
                ]);
                if ($validator->fails()) {
                    $errores = '';
                    foreach ($validator->errors()->messages() as $error) {
                        $errores = $errores.$error[0].'<br>';
                    }
                    $ret['msg'] = 'error';
                    $ret['message'] = $errores;

                    return response()->json($ret);
                }
                $resultadoContent = GosenHelper::verifyImgTrumbowyg($request->content);
                $historial = new HistorySendedMail();
                $historial->fill($request->only(['header', 'footer']));
                $historial->content = $resultadoContent;
                $historial->type_mail = $tipoCorreo;
                $historial->type_user = json_encode(($request->check_personal == 1) ? $request->users : $request->type_user);
                $historial->to_specific_users = ($request->check_personal == 1) ? 1 : 0;
                $historial->save();
            } else {
                $tipoCorreo = 'simple';
                $validator = Validator::make($request->all(), [
                    'header' => 'required|string|max:100',
                    'content' => 'required',
                    'header' => 'string|max:100',
                ]);
                if ($validator->fails()) {
                    $errores = '';
                    foreach ($validator->errors()->messages() as $error) {
                        $errores = $errores.$error[0].'<br>';
                    }
                    $ret['msg'] = 'error';
                    $ret['message'] = $errores;

                    return response()->json($ret);
                }
                $resultadoContent = GosenHelper::verifyImgTrumbowyg($request->content);
                $historial = new HistorySendedMail();
                $historial->fill($request->only(['header']));
                $historial->content = $resultadoContent;
                $historial->type_mail = $tipoCorreo;
                $historial->type_user = json_encode(($request->check_personal == 1) ? $request->users : $request->type_user);
                $historial->to_specific_users = ($request->check_personal == 1) ? 1 : 0;
                $historial->save();
            }
            $header = $historial->header;
            $content = $historial->content;
            $footer = $historial->footer;
            $type_mail = $historial->type_mail;
            $nombreTipo = Menu::orderBy('id', 'asc')->pluck('valor');
            $tiposString = '';
            $totalUsuarios = 0;
            $users = [];
            if ($request->check_personal == 1) {
                $cantidadUsers = collect();
                foreach ($request->users as $item) {
                    $us = User::find($item);
                    $cantidadUsers->push($us);
                }
                foreach ($cantidadUsers as $item) {
                    array_push($users, $item->email);
                }
                $totalUsuarios += $cantidadUsers->count();
            } else {
                foreach ($request->type_user as $tipos) {
                    $cantidadUsers = User::where([
                        ['type_user', $tipos],
                        ['role', 'user'],
                        ['status', 'active'],
                        ['lang', $request->lenguage],
                    ])->get();
                    foreach ($cantidadUsers as $item) {
                        array_push($users, $item->email);
                    }
                    switch ($tipos) {
                        case '1':
                            $tiposString = $nombreTipo[0];
                            break;
                        case '2':
                            $tiposString = $nombreTipo[1];
                            break;
                        case '3':
                            $tiposString = $nombreTipo[2];
                            break;
                        case '4':
                            $tiposString = $nombreTipo[3];
                            break;
                        case '5':
                            $tiposString = $nombreTipo[4];
                            break;
                        case '6':
                            $tiposString = $nombreTipo[5];
                            break;
                        case '7':
                            $tiposString = $nombreTipo[6];
                            break;
                        case '8':
                            $tiposString = $nombreTipo[7];
                            break;
                        case '9':
                            $tiposString = $nombreTipo[8];
                            break;
                        case '10':
                            $tiposString = $nombreTipo[9];
                            break;
                    }
                    $totalUsuarios += $cantidadUsers->count();
                }
            }
            try {
                if ($totalUsuarios > 0) {
                    SendMassiveMailsJob::dispatch($users, $header, $content, $footer, $type_mail, $tiposString, $totalUsuarios);
                    $ret['msg'] = 'success';
                    $ret['message'] = ___('Los correos fueron enviados correctamente');

                    return response()->json($ret);
                } else {
                    $ret['msg'] = 'error';
                    $ret['message'] = ___('Actualmente no existen usuario de ese tipo');

                    return response()->json($ret);
                }
            } catch (\Throwable $th) {
                $ret['msg'] = 'error';
                $ret['message'] = ___('Ocurrio un error intentelo nuevamente');

                return response()->json($ret);
            }
        } catch (\Throwable $th) {
            $ret['msg'] = 'error';
            $ret['message'] = ___('Ocurrio un error intentelo nuevamente');

            return response()->json($ret);
        }
    }
}
