<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FrequentQuestion;
use Illuminate\Http\Request;

class FrequentQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = FrequentQuestion::all();

        return view('admin/frequentQuestions.index', compact('questions'));
    }

    public function indexCliente()
    {
        $questions = FrequentQuestion::all();

        return view('user.frequentQuestions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.frequentQuestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'titulo' => 'required|min:3|max:255',
            'contenido' => 'required|min:5',
            'observacion' => 'required|min:3',
        ]);
        $question = FrequentQuestion::create($request->all());

        return back()->with('success', ___('Success create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\FrequentQuestion  $frequentQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(FrequentQuestion $frequentQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\FrequentQuestion  $frequentQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit($question)
    {
        $question = FrequentQuestion::find((int) $question);

        return view('admin.frequentQuestions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Models\FrequentQuestion  $frequentQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $question)
    {
        $validated = $request->validate([
            'titulo' => 'required|min:3|max:255',
            'contenido' => 'required|min:5',
            'observacion' => 'required|min:3',
        ]);

        $question = FrequentQuestion::find((int) $question);
        $question->fill($request->all());
        $question->save();

        return back()->with('success', ___('Success update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\FrequentQuestion  $frequentQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($question)
    {
        $question = FrequentQuestion::find((int) $question);

        $question->delete();

        return back();
    }

    public function eliminar($question)
    {
        try {
            $question = FrequentQuestion::find((int) $question);
            $question->delete();

            return 'eliminado';
        } catch (\Throwable $th) {
            return 'error';
        }
    }
}
