<?php

namespace App\Http\Controllers\Admin;

/**
 * Regalo Controller
 */
use App\Helpers\IcoHandler;
use App\Http\Controllers\Controller;
use App\Models\Bitacora;
use App\Models\RegaloType;
use Auth;
use Illuminate\Http\Request;
use Validator;

class RegaloController extends Controller
{
    protected $handler;

    public function __construct(IcoHandler $handler)
    {
        $this->handler = $handler;
    }

    public function index()
    {
        $gift = RegaloType::all();

        return view('admin.regalo-type', compact('gift'));
    }

    public function view_action(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Unable to proceed request!');

        $regalo_id = (int) $request->input('gift');
        $action = $request->input('action');
        $view = $request->input('view');

        $regalo = RegaloType::findOrFail($regalo_id);
        //dd($stage);

        if ($regalo && ! empty(env_file()) && str_contains(app_key(), $this->handler->find_the_path($this->handler->getDomain()))) {
            if ($action && $action == 'overview' && $view == 'modal') {
                $symbol = token_symbol();
                $Nombre = $regalo->name;
                $xToken = $regalo->total_tokens;
                $xInvitado = $regalo->total_guests;
                $xStatus = $regalo->status;
                $xComentario = $regalo->comment;

                return response()->json(['modal' => view('modals.overview-regalo', compact('symbol', 'Nombre', 'xToken', 'xInvitado', 'xStatus', 'xComentario'))->render()]);
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function edit_stage($id)
    {
        $gift = RegaloType::findOrFail($id);

        return view('admin.regalo-type-edit', compact('gift'));
    }

    public function update(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        // Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'total_tokens' => 'required|integer|min:1',
            'total_referrals' => 'required|integer|min:1',
            'comment' => 'max:200',
        ]);

        if ($validator->fails()) {
            $msg = '';
            if ($validator->errors()->hasAny(['name', 'total_tokens', 'total_referrals', 'comment'])) {
                $msg = $validator->errors()->first();
            } else {
                $msg = __('messages.form.wrong');
            }

            $ret['msg'] = 'warning';
            $ret['message'] = $msg;

            return response()->json($ret);
        } else {
            $id = $request->input('gift_id');
            $obj = RegaloType::find($id);

            if ($obj == null) {
                //$obj = new RegaloType();
                $ret['msg'] = 'success';
                $ret['message'] = 'Gift not found.';
            } elseif ($obj && ! empty(env_file())) {
                // Update or Create
                $obj->name = $request->input('name');
                $obj->total_tokens = (int) $request->input('total_tokens');
                $obj->total_guests = (int) $request->input('total_referrals');
                $obj->status = ($request->input('status') == null) ? 'inactive' : 'active';
                $obj->comment = (! empty($request->input('comment'))) ? $request->input('comment') : '';

                //$ret['ico'] = $obj;
                //check validity
                $save = $obj->save();

                if ($save) {
                    $objBit = new Bitacora();
                    $objBit->host = Auth::user()->id;
                    $objBit->usuario = Auth::user()->name;
                    $objBit->operacion = 'update';
                    $objBit->tabla = 'regalo_tipos';
                    $objBit->description = json_encode(['name' => $obj->name, 'total_tokens' => $obj->total_tokens, 'total_guests' => $obj->total_guests, 'status' => $obj->status, 'comment' => $obj->comment]);
                    $objBit->save();
                }
                if ($save) {
                    $ret['msg'] = 'success';
                    $ret['message'] = __('messages.update.success', ['what' => 'Revutokens']);
                } else {
                    $ret['msg'] = 'warning';
                    $ret['message'] = __('messages.update.failed', ['what' => 'Revutokens']);
                }
            } else {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.errors');
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }
}
