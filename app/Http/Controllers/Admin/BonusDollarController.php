<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Mail\WithdrawDollarsConfirmMail;
use App\Models\BonusDolar;
use App\Models\IcoStage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class BonusDollarController extends Controller
{
    public function bonusDolares($filtro)
    {
        switch ($filtro) {
            case '1':
                $bonus = BonusDolar::where('type', BonusDolar::TYPE2)->orderBy('created_at', 'desc')->get();
                $tipo = 'bonus';
                break;
            case '2':
                $bonus = BonusDolar::where('type', BonusDolar::TYPE1)->orderBy('created_at', 'desc')->get();
                $tipo = 'withdraw';
                break;
            case '3':
                $bonus = BonusDolar::orderBy('created_at', 'desc')->get();
                $tipo = 'all';
                break;
            default:
                $bonus = BonusDolar::orderBy('created_at', 'desc')->get();
                break;
        }
        //variables para vista withdraw
        $trnxWith = BonusDolar::where('type', '=', BonusDolar::TYPE1)->orderBy('created_at', 'desc')->get();
        $monto = auth()->user()->dolarBonus;
        //variables para vista bono
        $end = IcoStage::all();
        $stag = active_stage();
        $trnxBonus = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS2);
        $trnxBonusCanceled = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS3);
        $trnxBonusPending = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS1);
        $trnxAll = BonusDolar::where('type', '=', BonusDolar::TYPE1);
        $typeuser1 = BonusDolar::TYPEUSER1;
        $typeuser2 = BonusDolar::TYPEUSER2;
        $typeuser3 = BonusDolar::TYPEUSER3;
        $typeUser0 = 0;
        $typeUser1 = 0;
        $typeUser2 = 0;
        $typeUser3 = 0;
        $typeUser4 = 0;
        $typeUser5 = 0;
        $typeUser6 = 0;
        $typeUser7 = 0;
        $typeUser8 = 0;
        $typeUser9 = 0;
        $filtro = BonusDolar::pluck('user_id', 'id')->countBy();
        foreach ($filtro as $llave => $registro) {
            $usuario = User::find($llave);
            switch ($usuario->type_user) {
                case 0:
                    $typeUser0++;
                    break;
                case 1:
                    $typeUser1++;
                    break;
                case 2:
                    $typeUser2++;
                    break;
                case 3:
                    $typeUser3++;
                    break;
                case 4:
                    $typeUser4++;
                    break;
                case 5:
                    $typeUser5++;
                    break;
                case 6:
                    $typeUser6++;
                    break;
                case 7:
                    $typeUser7++;
                    break;
                case 8:
                    $typeUser8++;
                    break;
                case 9:
                    $typeUser9++;
                    break;
                default:
                    // code...
                    break;
            }
        }
        $tipobono = BonusDolar::BONUSCODETNX;
        $bonusReferrals = BonusDolar::where('type', '!=', BonusDolar::TYPE1);

        return view('admin.bonus.show-bonus', compact('typeUser0', 'typeUser1', 'typeUser2', 'typeUser3', 'typeUser4', 'typeUser5', 'typeUser6', 'typeUser7', 'typeUser8', 'typeUser9', 'trnxAll', 'tipobono', 'bonus', 'bonusReferrals', 'end', 'stag', 'tipo', 'trnxWith', 'monto', 'trnxBonus', 'trnxBonusCanceled', 'trnxBonusPending'));
    }

    public function WithdrawBonusDollars($filtro)
    {
        switch ($filtro) {
            case '1':
                $bonus = BonusDolar::where('type', BonusDolar::TYPE2)->orderBy('created_at', 'desc')->get();
                $tipo = 'bonus';
                break;
            case '2':
                $bonus = BonusDolar::where('type', BonusDolar::TYPE1)->orderBy('created_at', 'desc')->get();
                $tipo = 'withdraw';
                break;
            case '3':
                $bonus = BonusDolar::orderBy('created_at', 'desc')->get();
                $tipo = 'all';
                break;
            default:
                $bonus = BonusDolar::orderBy('created_at', 'desc')->get();
                break;
        }
        //variables para vista withdraw
        $trnxWith = BonusDolar::where('type', '=', BonusDolar::TYPE1)->orderBy('created_at', 'desc')->get();
        $monto = auth()->user()->dolarBonus;
        //variables para vista bono
        $end = IcoStage::all();
        $stag = active_stage();
        $trnxBonus = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS2);
        $trnxBonusCanceled = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS3);
        $trnxBonusPending = BonusDolar::where('type', '=', BonusDolar::TYPE1)->where('status', '=', BonusDolar::STATUS1);
        $trnxAll = BonusDolar::where('type', '=', BonusDolar::TYPE1);
        $typeuser1 = BonusDolar::TYPEUSER1;
        $typeuser2 = BonusDolar::TYPEUSER2;
        $typeuser3 = BonusDolar::TYPEUSER3;
        $typeUser0 = 0;
        $typeUser1 = 0;
        $typeUser2 = 0;
        $typeUser3 = 0;
        $typeUser4 = 0;
        $typeUser5 = 0;
        $typeUser6 = 0;
        $typeUser7 = 0;
        $typeUser8 = 0;
        $typeUser9 = 0;
        $typeUser10 = 0;
        $filtro = BonusDolar::pluck('user_id', 'id')->countBy();
        foreach ($filtro as $llave => $registro) {
            $usuario = User::find($llave);
            switch ($usuario->type_user) {
                case 0:
                    $typeUser0++;
                    break;
                case 1:
                    $typeUser1++;
                    break;
                case 2:
                    $typeUser2++;
                    break;
                case 3:
                    $typeUser3++;
                    break;
                case 4:
                    $typeUser4++;
                    break;
                case 5:
                    $typeUser5++;
                    break;
                case 6:
                    $typeUser6++;
                    break;
                case 7:
                    $typeUser7++;
                    break;
                case 8:
                    $typeUser8++;
                    break;
                case 9:
                    $typeUser9++;
                    break;
                case 10:
                    $typeUser10++;
                    break;
                default:
                    // code...
                    break;
            }
        }
        $tipobono = BonusDolar::BONUSCODETNX;
        $bonusReferrals = BonusDolar::where('type', '!=', BonusDolar::TYPE1);

        return view('admin.bonus.show-withdraws', compact('trnxAll', 'tipobono', 'bonus', 'bonusReferrals', 'end', 'stag', 'tipo', 'trnxWith', 'monto', 'trnxBonus', 'trnxBonusCanceled', 'trnxBonusPending'));
    }

    public function withdrawBonusApproved(Request $request)
    {
        $check = $request->check;
        $idWithdraw = Crypt::decrypt($request->id);
        $hash = $request->hash;
        $idUser = Crypt::decrypt($request->user_id);
        $user = User::find($idUser);
        $usr = $usr = Auth::user();
        $user->dolarBonus = $user->dolarBonus - $request->amount;

        $user->save();
        $registro = BonusDolar::find($idWithdraw);
        if ($registro) {
            $registro->status = BonusDolar::STATUS2;
            $registro->checked_by = $usr->id;
            if ($hash != null) {
                $registro->hash = $hash;
            }
            $registro->save();
            GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('retiro_usd_exitoso', [$request->amount, 0, 0]));
        }
        if ($check) {
            Mail::to($user->email)
                ->send(new WithdrawDollarsConfirmMail($user, $registro));

            return back()->with('success', ___('The withdrawal of dollars was successfully approved, an email was sent to the user with the details'));
        } else {
            return back()->with('success', ___('The withdrawal of dollars was successfully approved'));
        }
    }

    public function withdrawBonusCanceled(Request $request)
    {
        $idWithdraw = Crypt::decrypt($request->id);
        $idUser = Crypt::decrypt($request->user_id);
        $user = User::find($idUser);
        $usr = $usr = Auth::user();
        $registro = BonusDolar::find($idWithdraw);
        if ($registro) {
            $registro->comment = $request->comment;
            $registro->status = BonusDolar::STATUS3;
            $registro->checked_by = $usr->id;
            $registro->save();
        }
        GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('retiro_usd_cancelado', [$registro->amount, 0, 0]));
        Mail::to($user->email)
            ->send(new WithdrawDollarsConfirmMail($user, $registro));

        return back()->with('info', ___('The withdrawal of dollars was successfully canceled'));
    }
}
