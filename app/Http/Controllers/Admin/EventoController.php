<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\AddEventToUserJob;
use App\Models\Evento;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = Evento::all();

        return view('admin.eventos.index', compact('eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required|max:80',
            'alcance' => 'required|max:30',
            'image' => 'mimes:png,jpg,bmp,webp,gif,jpeg|max:10240',
        ]);
        $evento = new Evento;
        if ($request->image != null) {
            $filename = time().'.'.$request->image->extension();
            // $evento->image = $request->image->storeAs('eventos', $filename, 'gosendisk');
            $request->image->move(rutaGuardado().'/imagenes/eventos', $filename);

            $evento->image = 'eventos/'.$filename;
        } else {
            $evento->image = $request->image;
        }
        $evento->body = $request->body;
        $evento->footer = $request->footer;
        $evento->alcance = $request->alcance;
        $evento->save();

        if ($evento->alcance == 'all') {
            $users = User::all();
        } else {
            $users = User::where('role', $evento->alcance)->get();
        }

        AddEventToUserJob::dispatch($users, $evento);

        return redirect()->route('admin.eventos.index')->with('success', ___('Event has been created successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $request->validate([
            'body' => 'required',
            'image' => 'mimes:png,jpg,bmp,webp,gif,jpeg|max:10240',
        ]);
        $evento = Evento::find($id);
        //dd($evento);
        if ($request->image != null) {
            // Storage::disk('gosendisk')->delete($evento->image);
            Storage::disk('gosendisk')->delete('imagenes/'.$evento->image);
            $filename = time().'.'.$request->image->extension();
            $request->image->move(rutaGuardado().'/imagenes/eventos', $filename);

            $evento->image = 'eventos/'.$filename;
        }

        $evento->body = $request->body;
        $evento->footer = $request->footer;
        //dd($evento);
        $evento->save();

        return redirect()->route('admin.eventos.index')->with('success', 'Event has been update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evento = Evento::find((int) $id);

        $evento->delete();

        return back();
    }

    public function eliminar($id)
    {
        try {
            $evento = Evento::find((int) $id);
            // Storage::disk('gosendisk')->delete($evento->image);
            Storage::disk('gosendisk')->delete('imagenes/'.$evento->image);
            $evento->users()->detach();
            $evento->delete();

            return 'eliminado';
        } catch (\Throwable $th) {
            return 'error';
        }
    }

    public function eliminarEvento($eve)
    {
        try {
            DB::table('evento_user')->where('id', $eve)->delete();

            return 'eliminado';
        } catch (\Throwable $th) {
            return 'error';
        }
    }
}
