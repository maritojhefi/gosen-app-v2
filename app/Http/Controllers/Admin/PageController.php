<?php

namespace App\Http\Controllers\Admin;

/**
 * Page Controller
 *
 * @author Softnio
 *
 * @version 1.0.3
 */
use App\Http\Controllers\Controller;
use App\Models\MainPage;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function index()
    {
        $pages = Page::where('status', '!=', 'inactive')->orderBy('id', 'ASC')->get();
        $mainPages = MainPage::all();

        return view('admin.pages', compact('pages', 'mainPages'));
    }

    /**
     * Display the specified resource for edit.
     *
     * @param $slug
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     */
    public function addPage()
    {
        return view('admin.new-page');
    }

    public function store(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        $validator = Validator::make($request->all(), [
            // 'page_id' => 'required',
            'custom_slug' => 'required|unique:pages,custom_slug,',
            'title' => 'required',
            'menu_title' => 'required',
            'type_user' => 'required',
            'icon' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has(['title', 'menu_title', 'custom_slug'])) {
                $msg = $validator->errors()->first();
            } else {
                $msg = __('messages.form.wrong');
            }
            $ret['msg'] = 'warning';
            $ret['message'] = $msg;
        } else {
            $data = Page::create([
                'title' => $request->input('title'),
                'menu_title' => $request->input('menu_title'),
                'slug' => preg_replace('/\s+/', '-', $request->input('custom_slug')),
                'custom_slug' => preg_replace('/\s+/', '-', $request->input('custom_slug')),
                'description' => str_replace(['<script>', '</script>'], ['&lt;script&gt;', '&lt;/script&gt;'], trim($request->input('description'))),
                'status' => $request->input('status'),
                'type_user' => $request->input('type_user'),
                'icon' => $request->input('icon'),
            ]);
            $data->save();

            if ($data) {
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.create.success', ['what' => 'Page']);
            } else {
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.create.failed', ['what' => 'Page']);
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function eliminarPage($pag)
    {
        try {
            DB::table('pages')->where('id', $pag)->delete();

            return 'eliminado';
        } catch (\Throwable $th) {
            return 'error';
        }
    }

    public function updateMainPage($id)
    {
        $page = MainPage::find($id);
        if ($page->status) {
            $page->status = false;
            $page->save();

            return 'false';
        } else {
            $page->status = true;
            $page->save();

            return 'true';
        }
    }

    public function edit($slug)
    {
        $page_data = Page::where('slug', $slug)->first();

        return view('admin.edit-page', compact('page_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     */
    public function update(Request $request)
    {
        //dd($request);
        $page = $request->input('page_id');
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        $validator = Validator::make($request->all(), [
            'page_id' => 'required',
            'custom_slug' => 'required|unique:pages,custom_slug,'.$page,
            'title' => 'required',
            'menu_title' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has(['title', 'menu_title', 'custom_slug'])) {
                $msg = $validator->errors()->first();
            } else {
                $msg = __('messages.form.wrong');
            }
            $ret['msg'] = 'warning';
            $ret['message'] = $msg;
        } else {
            $data = Page::where('id', $page)->first();
            $data->title = $request->input('title');
            $data->menu_title = $request->input('menu_title');
            $data->custom_slug = preg_replace('/\s+/', '-', $request->input('custom_slug'));
            $data->description = str_replace(['<script>', '</script>'], ['&lt;script&gt;', '&lt;/script&gt;'], trim($request->input('description')));
            $data->status = $request->input('status');
            $data->type_user = $request->type_user;
            $data->icon = $request->input('icon');
            $data->save();

            if ($data) {
                $ret['msg'] = 'success';
                $ret['message'] = __('messages.update.success', ['what' => 'Page']);
            } else {
                $ret['msg'] = 'error';
                $ret['message'] = __('messages.update.failed', ['what' => 'Page']);
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Upload the files
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function upload_zone(Request $request)
    {
        //passport upload
        if ($request->hasFile('whitepaper')) {
            $cleanData = Validator::make($request->all(), ['whitepaper' => 'required|mimetypes:application/pdf']);
            $old = storage_path('app/public/'.get_setting('site_white_paper'));
            if ($cleanData->fails()) {
                $ret['msg'] = 'warning';
                $ret['message'] = __('messages.upload.invalid');
            } else {
                $file = $request->file('whitepaper');
                $name = 'white-paper'.time().'.'.$file->extension();
                $file->move(storage_path('app/public/'), $name);
                Setting::updateValue('site_white_paper', $name);

                $ret['msg'] = 'success';
                $ret['message'] = __('messages.upload.success', ['what' => 'White Paper']);
                $ret['file_name'] = $name;
                if (! is_dir($old) && ! starts_with($old, 'assets')) {
                    unlink($old);
                }
            }

            return response()->json($ret);
        }
    }
}
