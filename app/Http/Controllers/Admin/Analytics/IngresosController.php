<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IngresosController extends Controller
{
    public function index(Request $request)
    {
        $get_tnx = ($request->get('chart') ? $request->get('chart') : 15) - 1;
        $get_token = ($request->get('chart') ? $request->get('chart') : 30) - 1;
        $get_user = ($request->get('user') ? $request->get('user') : 15) - 1;
        $stage = \App\Models\IcoStage::dashboard();
        $trnxs = \App\Models\Transaction::dashboard($get_tnx);

        //tabla ingresos tokens
        $data = \App\Models\Transaction::chart($get_token);
        $array = json_decode(json_encode($data), true);
        $ingresos_tokens = collect();
        foreach ($array['data_alt'] as $key => $value) {
            $ingresos_tokens->push([
                'fecha' => $array['days_alt'][$key],
                'tokens' => $value,
            ]);
        }

        $ingresos_tokens = $ingresos_tokens->sortKeysDesc();

        $total_tokens = Transaction::where('tnx_type', 'purchase')->where('status', 'approved')->sum('total_tokens');

        //grafico ingresos USD
        $fecha = $get_user;
        $cd = Carbon::now();
        $lw = $cd->copy()->subDays($fecha);

        $cd = $cd->copy()->addDays(1);
        $df = $cd->diffInDays($lw);
        $transactions = Transaction::where(['status' => 'approved', 'tnx_type' => 'purchase'])
            ->whereBetween('created_at', [$lw, $cd])
            ->orderBy('created_at', 'DESC')
            ->get();
        $data_usd['days'] = null;
        $data_usd['data'] = null;
        $data_usd['data_alt'] = null;
        $data_usd['days_alt'] = null;
        for ($i = 1; $i <= $df; $i++) {
            $usd = 0;
            foreach ($transactions as $tnx) {
                $tnxDate = date('Y-m-d', strtotime($tnx->tnx_time));
                if ($lw->format('Y-m-d') == $tnxDate) {
                    $usd += $tnx->base_amount;
                } else {
                    $usd += 0;
                }
            }
            $data_usd['data'] .= $usd . ',';
            $data_usd['data_alt'][$i] = $usd;
            $data_usd['days_alt'][$i] = ($fecha > 27 ? $lw->format('d M Y') : $lw->format('d M'));
            $data_usd['days'] .= '"' . $lw->format('d M') . '",';
            $lw->addDay();
        }

        //para filtro de los graficos
        if (isset($request->user)) {
            $data = $data_usd;
        } elseif (isset($request->chart)) {
            $data = $trnxs;
        } else {
            $data = null;
        }
        if ($request->ajax()) {
            return response()->json((empty($data) ? [] : $data));
        }

        //tabla ingresos usd
        $ingresos_usd = collect();
        foreach ($data_usd['data_alt'] as $key => $value) {
            $ingresos_usd->push([
                'fecha' => $data_usd['days_alt'][$key],
                'usd' => $value,
            ]);
        }
        $ingresos_usd = $ingresos_usd->sortKeysDesc();

        $total_usd = Transaction::where('tnx_type', 'purchase')->where('status', 'approved')->sum('base_amount');



















        //grafico ingresos USD
        $fecha = $get_user;
        $cd = Carbon::now();
        $lw = $cd->copy()->subDays($fecha);

        $cd = $cd->copy()->addDays(1);
        $df = $cd->diffInDays($lw);
        $transactions = Transaction::where(['tnx_type' => 'bonus_package'])
            ->whereBetween('created_at', [$lw, $cd])
            ->orderBy('created_at', 'DESC')
            ->get();
        $data_staking['days'] = null;
        $data_staking['data'] = null;
        $data_staking['data_alt'] = null;
        $data_staking['days_alt'] = null;
        for ($i = 1; $i <= $df; $i++) {
            $tokens = 0;
            foreach ($transactions as $tnx) {
                $tnxDate = date('Y-m-d', strtotime($tnx->tnx_time));
                if ($lw->format('Y-m-d') == $tnxDate) {
                    $tokens += $tnx->total_tokens;
                } else {
                    $tokens += 0;
                }
            }
            $data_staking['data'] .= $tokens . ',';
            $data_staking['data_alt'][$i] = $tokens;
            $data_staking['days_alt'][$i] = ($fecha > 27 ? $lw->format('d M Y') : $lw->format('d M'));
            $data_staking['days'] .= '"' . $lw->format('d M') . '",';
            $lw->addDay();
        }

        //tabla ingresos tokens staking
        $ingresos_staking = collect();
        foreach ($data_staking['data_alt'] as $key => $value) {
            $ingresos_staking->push([
                'fecha' => $data_staking['days_alt'][$key],
                'tokens' => $value,
            ]);
        }
        $ingresos_staking = $ingresos_staking->sortKeysDesc();

        $total_ingresos_staking = Transaction::where('tnx_type', 'bonus_package')->sum('total_tokens');
        return view('admin.analytics.ingresos', compact('stage', 'trnxs', 'ingresos_tokens', 'total_tokens', 'data_usd', 'ingresos_usd', 'total_usd', 'total_ingresos_staking', 'ingresos_staking', 'data_staking'));
    }
}
