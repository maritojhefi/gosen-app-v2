<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use App\Models\Transaction;

class TiposPagosController extends Controller
{
    public function index()
    {
        $suma = Transaction::where('receive_currency', '!=', null)->where('tnx_type', 'purchase')->get();
        $tipo_suma = $suma->groupBy('receive_currency');
        $array_pagos = collect();
        $label_moneda = [];
        $label_cantidad = [];
        foreach ($tipo_suma as $mone => $array) {
            $array_pagos->push([
                'imagen' => $mone,
                'moneda' => short_to_full($mone),
                'monto_moneda' => $array->sum('receive_amount'),
                'monto_usd' => $array->sum('base_amount'),
            ]);
        }
        $array_pagos = $array_pagos->sortBy([
            ['monto_usd', 'desc'],

        ]);
        foreach ($array_pagos as $key) {
            array_push($label_moneda, $key['moneda']);
            array_push($label_cantidad, $key['monto_usd']);
        }

        return view('admin.analytics.tipos-pagos', compact('array_pagos', 'label_moneda', 'label_cantidad'));
    }
}
