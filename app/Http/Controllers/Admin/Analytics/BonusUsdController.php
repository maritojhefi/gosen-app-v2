<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use App\Models\BonusDolar;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BonusUsdController extends Controller
{
    public function index(Request $request)
    {
        $get_user = ($request->get('user') ? $request->get('user') : 15) - 1;
        $fecha = $get_user;
        $fecha_bonus = $get_user;
        $cd = Carbon::now();
        $lw = $cd->copy()->subDays($fecha);
        $lw_bonus = $cd->copy()->subDays($fecha_bonus);

        $cd = $cd->copy()->addDays(1);
        $df = $cd->diffInDays($lw);
        $df_bonus = $cd->diffInDays($lw_bonus);
        $transactions = Transaction::where(['status' => 'approved', 'tnx_type' => 'purchase'])
            ->whereBetween('created_at', [$lw, $cd])
            ->orderBy('created_at', 'DESC')
            ->get();

        $bonus = BonusDolar::where(['type' => 'bonus'])
            ->whereBetween('created_at', [$lw_bonus, $cd])
            ->orderBy('created_at', 'DESC')
            ->get();

        $data_usd['days'] = null;
        $data_usd['data'] = null;
        $data_usd['data_alt'] = null;
        $data_usd['days_alt'] = null;

        $bonus_usd['days'] = null;
        $bonus_usd['data'] = null;
        $bonus_usd['data_alt'] = null;
        $bonus_usd['days_alt'] = null;

        for ($i = 1; $i <= $df; $i++) {
            $usd = 0;
            $bon = 0;
            foreach ($transactions as $tnx) {
                $tnxDate = date('Y-m-d', strtotime($tnx->tnx_time));
                if ($lw->format('Y-m-d') == $tnxDate) {
                    $usd += $tnx->base_amount;
                } else {
                    $usd += 0;
                }
            }
            $data_usd['data'] .= $usd.',';
            $data_usd['data_alt'][$i] = $usd;
            $data_usd['days_alt'][$i] = ($fecha > 27 ? $lw->format('d M Y') : $lw->format('d M'));
            $data_usd['days'] .= '"'.$lw->format('d M').'",';
            $lw->addDay();
        }

        for ($i = 1; $i <= $df_bonus; $i++) {
            $bon = 0;
            foreach ($bonus as $bn) {
                $tnxDate_bonus = date('Y-m-d', strtotime($bn->created_at));
                if ($lw_bonus->format('Y-m-d') == $tnxDate_bonus) {
                    $bon += $bn->amount;
                } else {
                    $bon += 0;
                }
            }

            $bonus_usd['data'] .= $bon.',';
            $bonus_usd['data_alt'][$i] = $bon;
            $bonus_usd['days_alt'][$i] = ($fecha_bonus > 27 ? $lw_bonus->format('d M Y') : $lw_bonus->format('d M'));
            $bonus_usd['days'] .= '"'.$lw_bonus->format('d M').'",';
            $lw_bonus->addDay();
        }

        //para filtro de los graficos
        if (isset($request->user)) {
            $data = $data_usd;
        } else {
            $data = null;
        }
        if ($request->ajax()) {
            return response()->json((empty($data) ? [] : $data));
        }

        //tabla ingresos usd
        $ingresos_usd = collect();
        foreach ($data_usd['data_alt'] as $key => $value) {
            $ingresos_usd->push([
                'fecha' => $data_usd['days_alt'][$key],
                'usd' => $value,
                'bonus' => $bonus_usd['data_alt'][$key],
            ]);
        }
        $ingresos_usd = $ingresos_usd->sortKeysDesc();

        $total_usd = Transaction::where('tnx_type', 'purchase')->where('status', 'approved')->sum('base_amount');
        $total_bonus = BonusDolar::where('type', 'bonus')->sum('amount');

        return view('admin.analytics.bonus-usd', compact('ingresos_usd', 'data_usd', 'bonus_usd', 'total_usd', 'total_bonus'));
    }
}
