<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Models\Paquete;
use App\Models\Transaction;

class VentaPackagesController extends Controller
{
    public function index()
    {
        $paquetes = Paquete::where('estado', 1)->orderBy('id', 'asc')->get();
        $TotalpaquetesDisponibles = $paquetes->pluck('stock')->sum();
        // $transactions = Transaction::where([['tnx_type', 'purchase'], ['status', 'approved']])->get();
        $paquetesInfoVendidos = GosenHelper::informePaquetesVendidos();
        $infoTotales = GosenHelper::informePaquetesTotales($paquetesInfoVendidos);
        $porcentajeTotal = round((array_sum($infoTotales) - $TotalpaquetesDisponibles) / array_sum($infoTotales) * 100);
        $stock = Paquete::select('stock')->get();

        return view('admin.analytics.venta-package', compact('stock', 'TotalpaquetesDisponibles', 'porcentajeTotal', 'paquetes', 'paquetesInfoVendidos', 'infoTotales'));
    }
}
