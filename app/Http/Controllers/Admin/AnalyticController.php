<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\LevelBonusDollar;
use App\Models\Menu;
use App\Models\User;

class AnalyticController extends Controller
{
    public function index()
    {
        //para el grafico de paises mas populares
        $paises = Country::orderBy('users', 'DESC')->get();
        $arrayNomPais = [];
        $arrayUserPais = [];
        $cont = 0;
        foreach ($paises as $pais) {
            $cont++;
            if ($cont <= 20) {
                if ($pais->name != 'United States of America') {
                    array_push($arrayNomPais, $pais->name);
                } else {
                    array_push($arrayNomPais, 'USA');
                }
                array_push($arrayUserPais, $pais->users);
            } else {
                break;
            }
        }

        //para el grafico de ciudades mas populares
        $ciudades = User::select('city', 'country')->where('city', '!=', 'null')->get();
        $totalcity = $ciudades->countBy('city');
        $arrayRegionesPorCiudades = collect();
        foreach ($totalcity as $city => $cantidad) {
            $user = User::where('city', $city)->first();
            $flag = Country::where('name', $user->country)->pluck('flag');
            $region = User::where('city', $city)->pluck('region');
            $totalRegion = $region->countBy();
            $arrayRegionesPorCiudades->push([
                'city' => $city,
                'region' => $totalRegion,
                'cantidad' => $cantidad,
                'flag' => $flag[0],
            ]);
        }

        $arrayRegionesPorCiudades = $arrayRegionesPorCiudades->sortBy([
            ['cantidad', 'desc'],

        ]);
        $arrayNomCiudad = [];
        $arrayCantCiudad = [];
        $cont = 0;
        foreach ($arrayRegionesPorCiudades as $key) {
            $cont++;
            if ($cont <= 20) {
                array_push($arrayNomCiudad, $key['city']);
                array_push($arrayCantCiudad, $key['cantidad']);
            } else {
                break;
            }
        }

        //para el grafico de tipo de usuarios
        $menu = Menu::all();
        $tipos = [];
        foreach ($menu as $tipo) {
            array_push($tipos, $tipo->valor);
        }

        $levels = LevelBonusDollar::orderBy('type_user')->get();
        $cant_levels = $levels->groupBy('type_user')->count();
        $typeUsers = collect();
        $cant_user_type = [];
        $total = User::where('role', 'user')->count();
        for ($i = 0; $i < $cant_levels + 1; $i++) {
            $cant_typ = User::where('type_user', $i)->where('role', 'user')->count();
            $typeUsers->push([
                'user_type' => $i,
                'cantidad_type' => $cant_typ,
            ]);
            array_push($cant_user_type, $cant_typ);
        }
        // dd($typeUsers);
        return view('admin.analytics', compact('paises', 'arrayNomPais', 'arrayUserPais', 'typeUsers', 'total', 'cant_user_type', 'tipos', 'arrayRegionesPorCiudades', 'arrayNomCiudad', 'arrayCantCiudad'));
    }
}
