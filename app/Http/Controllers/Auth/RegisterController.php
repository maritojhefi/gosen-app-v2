<?php

namespace App\Http\Controllers\Auth;

/**
 * Register Controller
 *
 * @author Softnio
 *
 * @version 1.1.2
 */

use App\Helpers\GosenHelper;
use App\Helpers\IcoHandler;
use App\Helpers\ReCaptcha;
use App\Http\Controllers\Controller;
use App\Models\LogsGosen;
use App\Models\Referral;
use App\Models\User;
use App\Models\UserMeta;
use App\Notifications\ConfirmEmail;
use Carbon\Carbon;
use Cookie;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers, ReCaptcha;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     *
     * @version 1.0.0
     */
    protected $redirectTo = '/register/success';

    /**
     * Create a new controller instance.
     *
     * @version 1.0.0
     *
     * @return void
     */
    protected $handler;

    public function __construct(IcoHandler $handler)
    {
        $this->handler = $handler;
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        if (application_installed(true) == false) {
            return redirect(url('/install'));
        }
        $cookieLanding = Cookie::get('landing');

        if ($cookieLanding) {
            $landing = $cookieLanding;

            return view('auth.register', compact('landing'));
        }

        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if (recaptcha()) {
            $this->checkReCaptcha($request->recaptcha);
        }
        // $have_user = User::where('role', 'admin')->count();
        // if ($have_user >= 1 && !$this->handler->check_body()) {
        //     return back()->withInput()->with([
        //         'warning' => $this->handler->accessMessage()
        //     ]);
        // }
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @version 1.0.1
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $term = get_page('terms', 'status') == 'active' ? 'required' : 'nullable';

        return Validator::make(
            $data,
            [
                'username' => ['required', 'string', 'min:3', 'max:255', 'unique:users'],
                'name' => ['required', 'string', 'min:3', 'max:255'],
                'mobile' => ['required', 'min:8'],
                'email' => ['required', 'string', 'email', 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix', 'max:255', 'unique:users'],

                'password' => [
                    'required',
                    'max:50',
                    Password::min(8)

                        ->mixedCase()
                        ->letters()
                        ->numbers()
                        ->symbols()
                        ->uncompromised(),
                ],
                'password_confirmation' => ['required', 'same:password'],
                //  se comento esta parte para evitar el funcionamiento del check en el register
                // ,

                // 'terms' => [$term],
            ]
            // , [
            //     'terms.required' => __('messages.agree'),
            //     'email.regex' => ___('Please enter a valid email address.'),
            //     'email.unique' => 'The email address you have entered is already registered. Did you <a href="' . route('password.request') . '">forget your login</a> information?',
            // ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @version 1.2.1
     *
     * @since 1.0.0
     *
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $have_user = User::where('role', 'admin')->count();
        $type = ($have_user >= 1) ? 'user' : 'admin';
        $email_verified = ($have_user >= 1) ? null : now();
        $user = User::create([
            'username' => strip_tags($data['username']),
            'name' => strip_tags($data['name']),
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => Hash::make($data['password']),
            'lastLogin' => date('Y-m-d H:i:s'),
            'role' => $type,
            'lang' => (Cookie::get('app_language') != null) ? Cookie::get('app_language') : Cookie::get('idioma'),
            'type_user' => 0,
        ]);
        try {
            GosenHelper::registerLocation($user);
        } catch (\Throwable $th) {
            LogsGosen::create([
                'titulo' => 'No se pudo ubicar al usuario id: '.$user->id,
                'log' => $th->getMessage(),
            ]);
        }

        if ($user) {
            if ($have_user <= 0) {
                save_gmeta('site_super_admin', 1, $user->id);
            }
            $user->email_verified_at = $email_verified;
            $refer_blank = true;
            // if (is_active_referral_system()) {
            if (Cookie::has('ico_nio_ref_by')) {
                $ref_id = (int) Cookie::get('ico_nio_ref_by');
                $ref_user = User::where('id', $ref_id)->where('email_verified_at', '!=', null)->first();
                if ($ref_user) {
                    $user->referral = $ref_user->id;
                    $user->referralInfo = json_encode([
                        'user' => $ref_user->id,
                        'name' => $ref_user->name,
                        'time' => now(),
                    ]);
                    $refer_blank = false;
                    $this->create_referral_or_not($user->id, $ref_user->id);
                    Cookie::queue(Cookie::forget('ico_nio_ref_by'));
                }
            }
            // }
            if ($user->role == 'user' && $refer_blank == true) {
                $this->create_referral_or_not($user->id);
            }

            $user->save();
            $meta = UserMeta::create(['userId' => $user->id]);

            $meta->notify_admin = ($type == 'user') ? 0 : 1;
            $meta->email_token = str_random(65);
            $cd = Carbon::now(); //->toDateTimeString();
            $meta->email_expire = $cd->copy()->addDays(90);
            $meta->save();

            if (Cookie::has('retargeting')) {
                Cookie::queue(Cookie::forget('retargeting'));
            }

            if ($user->email_verified_at == null) {
                try {
                    $user->notify(new ConfirmEmail($user));
                } catch (\Exception $e) {
                    session('warning', 'User registered successfully, but we unable to send confirmation email!');
                }
            }
        }

        return $user;
    }

    /**
     * Create user in referral table.
     *
     * @param    $user, $refer
     *
     * @version 1.0
     *
     * @since 1.1.2
     *
     * @return \App\Models\User
     */
    protected function create_referral_or_not($user, $refer = 0)
    {
        Referral::create(['user_id' => $user, 'user_bonus' => 0, 'refer_by' => $refer, 'refer_bonus' => 0]);
    }
}
