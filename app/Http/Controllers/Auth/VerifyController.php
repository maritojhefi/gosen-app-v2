<?php

namespace App\Http\Controllers\Auth;

use App\Events\AnalyticAdminEvent;
use App\Events\AnalyticCiudadAdminEvent;
use App\Events\AnalyticTypeUsersAdminEvent;
use App\Events\MapAdminUsersEvent;
use App\Events\RefreshUsersQuantityEvent;
use App\Helpers\GosenHelper;
use App\Http\Controllers\Controller;
use App\Jobs\SendWelcomeMailToUserJob;
use App\Models\LogsGosen;
use App\Models\User;
use App\Notifications\ConfirmEmail;
use App\Notifications\UserRegistered;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class VerifyController extends Controller
{
    protected $redirectTo = '/verify/success';

    public function __construct()
    {
        $this->middleware('auth')->except(['verify']);
    }

    public function index(Request $request)
    {
        // return view('auth.verify');;
        return $request->user()->hasVerifiedEmail()
            ? redirect($this->redirectTo)
            : (view('auth.verify'));
    }

    public function resend(Request $request)
    {
        $cd = Carbon::now();
        $user = Auth::user();
        $chk = $cd->copy()->addMinutes(10);

        $user->meta->email_token = str_random(65);
        $user->meta->email_expire = $cd->copy()->addDays(90);
        $user->meta->save();

        try {
            $user->notify(new ConfirmEmail($user));

            return back()->with('resent', true);
        } catch (\Exception $e) {
            return back()->withErrors(__('messages.email.verify', ['email' => get_setting('site_email')]));
        }
    }

    public function verify(Request $request, $id = '', $token = '')
    {
        if ($id != null || Auth::id() != null) {
            $id = $id != null ? $id : Auth::id();
            $user = User::find($id);
            if ($user) {
                if ($user->email_verified_at != null) {
                    return redirect()->route('login'); //->with('info', __('messages.verify.verified'));
                }
                if ($user->meta->email_token == $token && ! Cache::has($user->id)) {
                    Cache::put($user->id, $user, $seconds = 10);
                    if (_date($user->meta->email_expire, 'Y-m-d H:i:s', false, false) >= date('Y-m-d H:i:s')) {
                        DB::beginTransaction();
                        $user->email_verified_at = now();
                        $user->meta->email_token = null;
                        $user->meta->email_expire = null;
                        $user->save();
                        $user->meta->save();
                        // $user->notify(new UserRegistered($user));
                        try {
                            if ($user->role == 'user') {
                                $array = GosenHelper::mapaGeneralUsuariosPorPais();
                                event(new MapAdminUsersEvent($array));
                                event(new RefreshUsersQuantityEvent());
                                $pais = GosenHelper::paisesCantidadUsuarios();
                                event(new AnalyticAdminEvent($pais[0], $pais[1]));
                                $tipo = GosenHelper::tiposCantidadUsuarios();
                                event(new AnalyticTypeUsersAdminEvent($tipo[0], $tipo[1]));
                                $arrayCiudades = GosenHelper::ciudadesPopulares();
                                event(new AnalyticCiudadAdminEvent($arrayCiudades[1], $arrayCiudades[2]));
                                GosenHelper::refreshBonusGift($user);
                                SendWelcomeMailToUserJob::dispatch($user);
                                GosenHelper::createNotification($user->id, GosenHelper::contenidoNotificacion('bienvenida', [0, 0, 0]));
                                GosenHelper::sendNotificationReferrals($user->id);
                            }
                        } catch (\Throwable $th) {
                            LogsGosen::create([
                                'titulo' => 'Fallo al realizar acciones iniciales para la cuenta del usuario: '.$user->name.' id: '.$user->id,
                                'log' => $th->getMessage(),
                            ]);
                        }
                        Auth::logout();
                        DB::commit();
                        session()->forget('notifiable');

                        return redirect($this->redirectTo);
                    } else {
                        if (! Auth::guest()) {
                            Auth::logout();
                        }

                        return redirect()->route('login')->with('info', __('messages.verify.expired'));
                    }
                } else {
                    if (! Auth::guest()) {
                        Auth::logout();
                    }

                    return redirect()->route('login')->with('error', __('messages.verify.invalid'));
                }
            } else {
                if (! Auth::guest()) {
                    Auth::logout();
                }

                return redirect()->route('login')->with('warning', __('messages.verify.not_found'));
            }
        } else {
            return redirect()->route('login')->with('warning', __('messages.verify.not_found'));
        }
    }
}
