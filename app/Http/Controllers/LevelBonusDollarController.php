<?php

namespace App\Http\Controllers;

use App\Models\LevelBonusDollar;
use Illuminate\Http\Request;

class LevelBonusDollarController extends Controller
{
    public function save(Request $request)
    {
        $request->validate([
            'max' => 'required',
            'type' => 'required',
            'value' => 'required',
        ]);

        $lvlbonusdollar = LevelBonusDollar::findOrFail($request->id);
        $lvlbonusdollar->max = $request->max;
        $lvlbonusdollar->type = $request->type;
        $lvlbonusdollar->value = $request->value;
        $lvlbonusdollar->save();
        $ret['msg'] = 'success';
        $ret['message'] = 'The changes were applied successfully';

        return response()->json($ret);
    }
}
