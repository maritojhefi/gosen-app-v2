<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class WithdrawBonusDollarExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $data;

    public $periodo1;

    public $periodo2;

    public $estadoReporte;

    public function __construct($data, $periodo1, $periodo2, $estadoReporte)
    {
        $this->data = $data;
        $this->periodo1 = $periodo1;
        $this->periodo2 = $periodo2;
        $this->estadoReporte = $estadoReporte;
    }

    public function view(): View
    {
        $withdraw = $this->data;
        $periodo1 = $this->periodo1;
        $periodo2 = $this->periodo2;
        $estadoReporte = $this->estadoReporte;
        $date = Carbon::parse(Carbon::now())->format('Y-m-d (g:i A)');

        return view('excel.withdraw-bonus-dollars', compact('withdraw', 'date', 'periodo1', 'periodo2', 'estadoReporte'));
    }
}
