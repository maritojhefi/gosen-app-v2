<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TransactionsExport implements FromCollection, WithHeadings, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function headings(): array
    {
        return [
            'Id',
            'Tranx id',
            'Tranx type',
            'Tranx time',
            'Tokens',
            'Bonus on base',
            'Bonus on token',
            'Total bonus',
            'Total Tokens',
            'Stage',
            'User',
            'Amount',
            'Receive amount',
            'Receive currency',
            'Base amount',
            'Base currency',
            'Base currency rate',
            'Currency',
            'Currency rate',
            'Payment method',
            'Payment id',
            'Payment to',
            'Checked by',
            'Added by',
            'Checked time',
            'Details',
            'Extra',
            'Status',
            'Dist',
            'Create at',
            'Update at',
        ];
    }

    public function collection()
    {
        $transaction = Transaction::select(
            'transactions.id',
            'tnx_id',
            'tnx_type',
            'tnx_time',
            'tokens',
            'bonus_on_base',
            'bonus_on_token',
            'total_bonus',
            'transactions.total_tokens',
            'stage',
            'user',
            'amount',
            'receive_amount',
            'receive_currency',
            'base_amount',
            'base_currency',
            'base_currency_rate',
            'currency',
            'currency_rate',
            'payment_method',
            'payment_id',
            'payment_to',
            'checked_by',
            'added_by',
            'checked_time',
            'details',
            'extra',
            'transactions.status',
            'dist',
            'transactions.created_at',
            'transactions.updated_at'
        )->get();
        foreach ($transaction as $trans) {
            $trans->user = $trans->tnxUser != null ? $trans->tnxUser->name : 'N/A';
            $trans->stage = $trans->ico_stage->name;
        }

        return $transaction;
    }
}
