<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class UsersExport implements FromCollection, WithHeadings, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     **/
    public function headings(): array
    {
        return
            [
                '#',
                'Name',
                'Email',
                'Status',
                'Register Method',
                'Social  id',
                'Mobile',
                'Date Birth',
                'Nationality',
                'Wallet Type',
                'Wallet Address',
                'Role',
                'Contributed',
                'Token Balance',
                'Referral Info',
                'Photo',
                'Type',
            ];
    }

    public function collection()
    {
        $user = DB::table('users')->select(
            'id',
            'name',
            'email',
            'status',
            'registerMethod',
            'social_id',
            'mobile',
            'dateOfBirth',
            'nationality',
            'walletType',
            'walletAddress',
            'role',
            'contributed',
            'tokenBalance',
            'referralInfo',
            'foto',
            'type_user'
        )->get();

        return $user;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')
            ->getFont()
            ->setBold(true);
    }
}
