<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactUsersExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $users;

    public $language;

    public function __construct($users, $language)
    {
        $this->users = $users;
        $this->language = $language;
    }

    public function view(): View
    {
        $users = $this->users;
        $date = Carbon::parse(Carbon::now())->format('Y-m-d (g:i A)');
        $language = $this->language;

        return view('excel.contact-users', compact('users', 'date', 'language'));
    }
}
