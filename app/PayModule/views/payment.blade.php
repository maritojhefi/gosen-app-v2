@php
    $pm_check = !empty($methods) ? true : false;
    $dot_1 = '.';
    $dot_2 = '';
    if ($data->total_bonus > 0) {
        $dot_1 = '';
        $dot_2 = '.';
    }
    $activeMethods = data_get(get_defined_vars(), 'activeMethods', []);
@endphp

<a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
<div class="popup-body">
    <div class="popup-content">
        <form class="validate-modern validate-form" action="{{ route('user.ajax.payment') }}" method="POST"
            id="online_payment">
            @csrf
            <input type="hidden" name="pp_token" id="token_amount" value="{{ $data->token }}">
            <input type="hidden" name="pp_currency" id="pay_currency" value="{{ $data->currency }}">
            <h4 class="popup-title">{{ ___('Payment Process') }}</h4>
            <p class="lead">
                {!! $data->total_bonus == 0
                    ? ___('Please make payment of :amount to receive :token_amount token.', [
                        'amount' =>
                            '<strong>' . $data->amount . ' <span class="pay-currency ucap">' . $data->currency . '</span></strong>',
                        'token_amount' =>
                            '<strong><span class="token-total">' .
                            to_num_token($data->total_tokens) .
                            ' ' .
                            token('symbol') .
                            '</span></strong>',
                    ])
                    : __('Please make payment of :amount to receive :token_amount token including bonus :token_bonus token.', [
                        'amount' =>
                            '<strong>' .
                            to_num($data->amount, 'max', ',') .
                            ' <span class="pay-currency ucap">' .
                            $data->currency .
                            '</span></strong>',
                        'token_amount' =>
                            '<strong><span class="token-total">' .
                            number_format($data->total_tokens, 2) .
                            ' ' .
                            token('symbol') .
                            '</span></strong>',
                        'token_bonus' =>
                            '<strong><span class="token-bonuses">' .
                            number_format($data->total_bonus, 2) .
                            ' ' .
                            token('symbol') .
                            '</span></strong>',
                    ]) !!}
            </p>
            @if ($pm_check)
                <p>{{ ___('You can choose any of following payment method to make your payment. The token balance will appear in your account after successful payment.') }}
                </p>
                <h5 class="mgt-1-5x font-mid">{{ ___('Select payment method:') }}</h5>
                <ul class="pay-list guttar-12px">
                    @foreach ($methods as $method)
                        {{ $method }}
                    @endforeach
                </ul>
                <p class=" font-italic mgb-1-5x"><small>*
                        {{ ___('Payment gateway may charge you a processing fees.') }}</small></p>
                <div class="pdb-0-5x">
                    <div class="input-item text-left">
                        <input type="checkbox" data-msg-required="{{ ___('You should accept our terms and policy.') }}"
                            class="input-checkbox input-checkbox-md" id="agree-terms" name="agree" required>
                        <label
                            for="agree-terms">{{ ___('I hereby agree to the token purchase agreement and token sale term.') }}</label>
                    </div>
                </div>
                <ul class="d-flex flex-wrap align-items-center guttar-30px">
                    <li><a href="#" id="botonComprar" class="btn btn-alt btn-primary payment-btn">
                            {{ ___('Buy Token Now') }} <em class="ti ti-arrow-right mgl-2x"></em></a></li>
                </ul>
                <div class="gaps-3x"></div>
                <div class="note note-plane note-light">
                    <em class="fas fa-info-circle"></em>
                    <p class="">
                        {{ ___('Our payment address will appear or redirect you for payment after the order is placed.') }}
                    </p>
                </div>
            @else
                <div class="gaps-4x"></div>
                <div class="alert alert-danger text-center">
                    {{ ___('Sorry! There is no payment method available for this currency. Please choose another currency or contact our support team.') }}
                </div>
                <div class="gaps-5x"></div>
            @endif

        </form>
    </div>
</div>

@if (in_array('Stripe', $activeMethods))
    <script src="https://js.stripe.com/v3/"></script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $("#botonComprar").click(function() {
            purchase_form()

            function purchase_form(
                a = $(".validate-form"),
                b = !0,
                c = "ti ti-info-alt"
            ) {

                $(a).ajaxSubmit({
                    dataType: "json",
                    success: function(e) {
                        if (
                            (btn_actived(a.find("button.save-disabled"), !1),
                                e.trnx || show_toast(e.msg, e.message, c),
                                "success" == e.msg || (!0 === b && $(d).clearForm()),
                                e.link)
                        ) {
                            if (e.param) {
                                var f = e.param,
                                    g = f.cta,
                                    h = {
                                        tnx: f.tnx,
                                        notify: f.notify,
                                        user: f.user,
                                        system: f.system,
                                    };
                                ajax_email(g, h);
                            }
                            setTimeout(function() {
                                window.location.href = e.link;
                            }, 1500);
                        }
                        if (e.modal) {
                            var i = a.parents(".modal"),
                                j = !0;
                            (is_changed = !0),
                            i.modal("hide").addClass("hold"),
                                i.find(".modal-content").html(e.modal),
                                init_inside_modal(),
                                i.on("hidden.bs.modal", function() {
                                    !0 == j ? (i.modal("show"), (j = !1)) : i.modal(
                                        "hide");
                                });
                        }
                        if (e.stripe) {
                            var k = Stripe(e.stripe.pk);
                            k.redirectToCheckout({
                                sessionId: e.stripe.session_id
                            });
                        }
                    }

                });
            }
            (function($) {
                var $_p_form = $('form#online_payment');
                if ($_p_form.length > 0) {
                    purchase_form_submit($_p_form);
                }
            })(jQuery);
        });
    });
</script>
