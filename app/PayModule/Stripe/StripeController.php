<?php

namespace App\PayModule\Stripe;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\PayModule\ModuleHelper;
use Illuminate\Http\Request;

class StripeController extends Controller
{
    private $instance;

    public function __construct(StripePayment $stripeInstance)
    {
        $this->instance = $stripeInstance;
    }

    public function success(Request $request)
    {
        if (method_exists($this->instance, 'success')) {
            return $this->instance->success($request);
        }
    }

    public function cancel(Request $request)
    {
        return redirect(route('all.notifications', ['filtrar' => 'transacciones']));
        if (method_exists($this->instance, 'cancel')) {
            return $this->instance->cancel($request);
        }
    }

    public function email_notify(Request $request)
    {
        $tnx_id = isset($request->tnx) ? $request->tnx : false;
        $mail_type = isset($request->notify) ? $request->notify : false;

        if ($tnx_id && $mail_type) {
            $tnx = Transaction::where('id', $tnx_id)->first();
            if (empty($tnx)) {
                return false;
            }

            return ModuleHelper::enotify($tnx, $mail_type, $request);
        }

        return false;
    }
}
