<a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
<div class="popup-body">
    <h4 class="popup-title">{{ ___('Confirmation Your Payment') }}</h4>
    <div class="popup-content">
        <form action="{{ route('payment.bank.update') }}" method="POST" id="payment-confirm" class="validate-modern"
            autocomplete="off">
            @csrf
            @php
                $num = isset($coinpay['result']['confirms_needed']) && !empty($coinpay['result']['confirms_needed']) ? $coinpay['result']['confirms_needed'] : 2;
                $cur = strtolower($transaction->currency);
                $_CUR = strtoupper($cur);
                $_gateway = ucfirst($transaction->payment_method);
            @endphp
            <input type="hidden" name="trnx_id" value="{{ $transaction->id }}">
            <p class="lead-lg text-primary">{!! ___('Your Order no. :orderid has been placed successfully.', [
                'orderid' => '<strong>' . $transaction->tnx_id . '</strong>',
            ]) !!}</p>

            <p>{!! __('Please send :amount :currency to the address below.', [
                'amount' => '<strong class="text-primary">' . $transaction->amount . '</strong>',
                'currency' => '<strong class="text-primary">' . $_CUR . '</strong>',
            ]) !!} {!! __(
                'The token balance will appear in your account only after you transaction gets :num confirmations and approved by :gateway.',
                ['num' => '<strong>' . $num . '</strong>', 'gateway' => '<strong>' . $_gateway . '</strong>'],
            ) !!}</p>

            <div class="pay-wallet-address pay-wallet-{{ $cur }}">
                <h6 class="text-head font-bold">{{ ___('Make your payment to the below address') }}</h6>
                <p>
                <div class="row guttar-1px guttar-vr-15px">
                    @if (isset($transaction->qr_code))
                        <div class="col-sm-3">
                            <p class="text-center"><img loading="lazy" title="{{ ___('Scan QR code to payment') }}"
                                    class="img-thumbnail" width="120"
                                    src="https://api.qrserver.com/v1/create-qr-code/?size=120x120&data={{ $transaction->qr_code }}"
                                    alt="QR"></p>
                        </div>
                    @elseif (isset($coinpay['result']['qrcode_url']))
                        <div class="col-sm-3">
                            <p class="text-center"><img loading="lazy" title="{{ ___('Scan QR code to payment') }}"
                                    class="img-thumbnail" width="120" src="{{ $coinpay['result']['qrcode_url'] }}"
                                    alt="QR"></p>
                        </div>
                    @endif
                    <div class="col-sm-9">
                        <div class="fake-class pl-sm-3">
                            @if (isset($coinpay['result']['address']))
                                <div class="copy-wrap mgb-0-5x">
                                    <span class="copy-feedback"></span>
                                    <em class="copy-icon ikon ikon-sign-{{ $cur }}"></em>
                                    <input type="text" class="copy-address"
                                        value="{{ $coinpay['result']['address'] }}" disabled=""
                                        id="valor-direccion">
                                    <button type="button" class="copy-trigger copy-clipboard"
                                        id="button-copiar-direcion"><em class="ti ti-files"></em></button>
                                </div>
                                <div class="gaps-2x"></div>
                            @endif
                            <ul class="d-flex flex-wrap align-items-center guttar-20px guttar-vr-15px">
                                <li><a name="action" class="btn btn-primary btn-sm" id="goto" target="_blank"
                                        href="{{ $coinpay['result']['status_url'] }}">{{ __('Go to :gateway', ['gateway' => $_gateway]) }}</a>
                                </li>
                                <li><a href="{{ route('user.transactions') }}"
                                        class="btn btn-light btn-sm text-white">{{ ___('View Transaction') }}</a></li>

                            </ul>
                            @if (isset($coinpay['result']['status_url']))
                                <p class="mt-2"><button type="submit" name="action" value="cancel"
                                        class="btn btn-cancel btn-danger-alt payment-cancel-btn payment-btn btn-simple">{{ ___('Cancel Order') }}</button>
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    (function($) {
        var $_p_form = $('form#payment-confirm');
        if ($_p_form.length > 0) {
            purchase_form_submit($_p_form);
        }
        var clipboardModal = new ClipboardJS('.copy-trigger', {
            container: document.querySelector('.modal')
        });
        clipboardModal.on('success', function(e) {
            feedback(e.trigger, 'success');
            e.clearSelection();
        }).on('error', function(e) {
            feedback(e.trigger, 'fail');
        });
    })(jQuery);
    $(document).ready(function() {
        $('#goto').click(function() {
            $(this).addClass('disabled');
            $(this).html(
                `<span class="spinner-border spinner-border-sm pt-2" role="status" aria-hidden="true"></span>`
            );
        });
    });
</script>
<script>
    $(document).ready(function() {
        // script de copiar enlace de referido desde el layout
        $("#button-copiar-direcion").click(function() {
            var valorCopiado = $("#valor-direccion").val();
            navigator.clipboard.writeText(valorCopiado)
                .then(function() {
                    // $(".copiado").removeClass('d-none');
                    // $(".no-copiado").addClass('d-none');
                    toastr.success("{{ ___('URL copiada al portapapeles.') }}");
                    // setTimeout(function() {
                    //     $(".copiado").addClass('d-none');
                    //     $(".no-copiado").removeClass('d-none');
                    // }, 1000)
                })
                .catch(function(err) {
                    toastr.warning("{{ ___('No se pudo copiar la URL al portapapeles.') }}");
                });
        });
    });
</script>
