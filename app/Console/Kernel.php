<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AddBonusDollarsToUsersCommand::class,
        Commands\UpdateCountriesCommand::class,
        Commands\VerifyTransactionsApprovedCommand::class,
        Commands\ApprovedTransactionBonusDollarCommand::class,
        Commands\CanceledWithdrawBonusUsdCommand::class,
        Commands\SendMailScheduleUsersCommand::class,
        Commands\SendMailSundayBonusStagingCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('bonusdollars-trans:approved')
            ->everyFiveMinutes()->appendOutputTo('public/logs/transactionsBonusDollarApproved.txt');
        $schedule->command('transactions:approved')
            ->everyMinute()->appendOutputTo('public/logs/transactionsApproved.txt');
        $schedule->command('approved:bonusdollars')
            ->dailyAt('01:00')->appendOutputTo('public/logs/texto.txt');
        $schedule->command('update:countries')
            ->dailyAt('02:00')->appendOutputTo('public/logs/countries.txt');
        $schedule->command('refresh:rates')
            ->everyTenMinutes()->appendOutputTo('public/logs/logs.text');
        $schedule->command('reset:rates')
            ->everyThirtyMinutes();
        $schedule->command('withdraw-user:canceled')
            ->hourly()->appendOutputTo('public/logs/logsretirobonusdollar.txt');
        $schedule->command('daily:backup')
            ->daily();
        $schedule->command('mails:scheduled')
            ->everyTenMinutes()->appendOutputTo('public/logs/mail-scheduled.txt');
        $schedule->command('whatsapp:scheduled')
            ->dailyAt('09:00')->appendOutputTo('public/logs/whatsapp-scheduled.txt');
        // $schedule->command('bonus:package')
        //     ->dailyAt('08:00')->appendOutputTo('public/logs/bonus-package-periodical.txt');
        // $schedule->command('refresh:sessions')
        //     ->everySixHours()->appendOutputTo('public/logs/sessionsLogs.text');
        // $schedule->command('bonus-staging:mail')
        //     ->weekly()->appendOutputTo('public/logs/bonus-staging.txt');

        $schedule->command('status:services')
            ->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
