<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ForceSettingRatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resetea el campo pmc_fx_exrates de settings para evitar el error de memoria llena';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('settings')->where('field', 'pmc_fx_exrates')->update(['value' => '{"currencies":{"ADA":2.856,"AUD":1.503,"BCH":0.00979,"BNB":0.003486,"BRL":5.36,"BTC":5.927e-5,"CAD":1.328,"CHF":0.9497,"CLP":884.16,"COP":4853.6,"CZK":24.02,"DKK":7.335,"EGP":26.6,"ETH":0.0007947,"EUR":0.9728,"GBP":0.8518,"HKD":7.841,"HUF":382.84,"IDR":15686.53,"INR":81.29,"JMD":222.93,"JPY":140.08,"KES":132.72,"LTC":0.01648,"MXN":19.56,"MYR":4.613,"NAD":23.71,"NGN":704.46,"NOK":9.96,"NZD":1.647,"PHP":56.78,"PKR":213.9,"PLN":4.56,"RUB":60.96,"SEK":10.41,"SGD":2.137,"SOL":0.06158,"THB":36.39,"TRX":17.95,"TRY":19.05,"UNI":0.1739,"XLM":10.66,"XMR":0.007945,"XRP":2.657,"ZAR":17.83,"BUSD":0.9995,"CAKE":0.2475,"DASH":0.02818,"DOGE":12.07,"LINK":0.1473,"USDC":1,"USDT":1.002,"WAVES":0.4288,"USD":1}}']);
    }
}
