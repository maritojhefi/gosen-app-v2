<?php

namespace App\Console\Commands;

use App\Helpers\GosenHelper;
use App\Mail\CustomMassiveMail;
use App\Models\LogsGosen;
use App\Models\User;
use App\Models\WhatsappCorreo;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendMailScheduleUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando envia de acuerdo a la cantidad de dias del registro del usuario los correos programados en la tabla de whatsapp_correos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Ejecutando cron para envio de correos automaticos personalizados a los usuarios a partir de la creacion de su cuenta');
        $correos = WhatsappCorreo::where([['number_days', '!=', 0], ['status', true], ['time', '!=', null], ['lang', '!=', null]])->get();
        $cont = 0;
        $actual = Carbon::now();
        foreach ($correos as $correo) {
            if ($actual->diffInMinutes(Carbon::parse($correo->time)) < 9) {
                $fechaActual = Carbon::now();
                $date = $fechaActual->subDays($correo->number_days)->format('Y-m-d');
                $users = User::select('id', 'email', 'mobile', 'lang', 'name')->where('first_login', true)->whereDate('email_verified_at', $date)->get();
                foreach ($users as $user) {
                    $status = null;
                    if ($user->lang == $correo->lang) {
                        $activeMail = GosenHelper::notification_email_active($user->id);
                        if ($activeMail == true) {
                            if (! $correo->users()->where('user_id', $user->id)->wherePivot('correo_status', true)->exists()) {
                                try {
                                    // Reemplazar el string {user} por el nombre del usuario
                                    $header = str_replace('{user}', $user->name, $correo->header);
                                    $content = str_replace('{user}', $user->name, $correo->content);
                                    // Reemplazar el string {name_app} por "disruptive"
                                    $header = str_replace('{name_app}', ucfirst(env('APP_NAME')), $header);
                                    $content = str_replace('{name_app}', ucfirst(env('APP_NAME')), $content);
                                    Mail::to($user->email)
                                        ->send(new CustomMassiveMail($header, $content, $correo->footer, $correo->type_mail));
                                    $this->info('Se envio el correo con el id : '.$correo->id.' para el usuario con el id : '.$user->id);
                                    $cont++;
                                    $this->info('Se creo el registro en la tabla user_whatsapp_correo');
                                    $status['correo_status'] = true;
                                } catch (\Throwable $th) {
                                    $status['correo_log'] = $th->getMessage();
                                    $this->error('Fallo al crear el registro en la tabla user_whatsapp_correo');
                                    $this->error($th->getMessage());
                                }
                                DB::beginTransaction();
                                $correo->users()->attach($user->id, $status);
                                DB::commit();
                            }
                        } else {
                            $this->info('El usuario'.$user->id.', tiene desactivado el envio de correos');
                            LogsGosen::create([
                                'titulo' => 'Envio de correos Desactivado',
                                'log' => 'El usuario '.$user->id.', tiene desactivado el envio de correos',
                            ]);
                        }
                    }
                }
            }
        }
        $this->info('Se ejecuto el cron para '.$cont.' usuario(s), para el envio de correos programados en fecha : '.Carbon::now());
        $this->info('');
        $this->info('');
    }
}
