<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Events\AnalyticTypeUsersAdminEvent;
use App\Events\RefreshStatusServicesEvent;
use App\Helpers\WhatsappNotification;
use App\Models\LogsGosen;
use App\Models\StatusService;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class StatusServicesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta todos los servicios del proyecto (apis) y guarda/notifica los resultados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $supportMembers;

    public $administradoresList;

    public function __construct()
    {
        $this->supportMembers = config('support-members');
        $this->administradoresList = User::where('role', 'admin')->get(['id', 'name', 'email', 'mobile'])->map->only(['id', 'name', 'email', 'mobile']);

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $servicios = StatusService::all();

        $cambios = false;
        $receptor = null;
        foreach ($servicios as $servicio) {
            try {
                switch ($servicio->service) {
                    case 'whatsapp(Disruptive)':
                        $accessToken = env('MESSAGEBIRD_KEY');
                        $response = Http::withHeaders([
                            'Authorization' => 'AccessKey '.$accessToken,
                        ])->get($servicio->url_base.$servicio->uri);
                        $servicio->log = $response->json();
                        $servicio->status = $response->status();
                        $saldo = floatval($response->json()['amount']);
                        if (! isset($saldo)) {
                            $servicio->description = 'El nombre del campo de estado ha sido cambiado a otro nombre. Por favor, verifica la API y corrige esta funcionalidad.';
                            $servicio->color = 'danger';
                            $receptor = 'soporte';
                        } elseif ($saldo <= 20) {
                            $servicio->description = 'Tu saldo actual es de *'.$saldo.'* dólares, el cual cumple con límite mínimo establecido.';
                            $servicio->color = 'warning';
                            $receptor = 'admin';
                        } elseif ($servicio->status == 200) {
                            $servicio->color = 'success';
                            $servicio->description = 'Tu servicio se encuentra activo.';
                        } else {
                            $servicio->color = 'danger';
                            $servicio->description = 'Tu servicio se encuentra con problemas.';
                            $receptor = 'soporte';
                        }
                        break;

                    case 'whatsapp(Soporte)':
                        $accessToken = config('whatsapp-credentials-support');
                        $response = Http::withHeaders([
                            'Authorization' => 'AccessKey '.$accessToken['api_key'],
                        ])->get($servicio->url_base.$servicio->uri);
                        $servicio->log = $response->json();
                        $servicio->status = $response->status();
                        $saldo = floatval($response->json()['amount']);
                        if (! isset($saldo)) {
                            $servicio->description = 'El nombre del campo de estado ha sido cambiado a otro nombre. Por favor, verifica la API y corrige esta funcionalidad.';
                            $servicio->color = 'danger';
                            $receptor = 'soporte';
                        } elseif ($saldo <= 20) {
                            $servicio->description = 'Tu saldo actual es de *'.$saldo.'* dólares, el cual cumple con límite mínimo establecido.';
                            $servicio->color = 'warning';
                            $receptor = 'admin';
                        } elseif ($servicio->status == 200) {
                            $servicio->color = 'success';
                            $servicio->description = 'Tu servicio se encuentra activo.';
                        } else {
                            $servicio->color = 'danger';
                            $servicio->description = 'Tu servicio se encuentra con problemas.';
                            $receptor = 'soporte';
                        }
                        break;

                    case 'apigps':
                        $response = Http::get($servicio->url_base);
                        $codigo = $response->status();
                        $servicio->status = $codigo;
                        $servicio->log = $response->json();
                        if ($codigo == 200) {
                            $servicio->color = 'success';
                            $servicio->description = 'Tu servicio  se encuentra activo';
                        } else {
                            $servicio->color = 'danger';
                            $servicio->description = 'Tu servicio se encuentra con problemas.';
                            $receptor = 'soporte';
                        }
                        break;

                    case 'ratecoin':
                        $accessToken = '2b93eacc7d125a745hcP2b28fba7691594d9';
                        $headers = ['X-Api-Signature' => 'bG9jYWxob3N0L2Rpc3J1cHRpdmU='];
                        $params = [
                            'access_key' => $accessToken,
                            'app' => '5hcPWdxQ',
                            'ver' => '1.5.0',
                            'valid' => 'localhost/tokenlite',
                            'base' => 'USD',
                            'currencies' => 'EUR,GBP,CAD,AUD,TRY,RUB,INR,BRL,NZD,PLN,JPY,MYR,IDR,NGN,MXN,PHP,CHF,THB,SGD,CZK,NOK,ZAR,SEK,KES,NAD,DKK,HKD,HUF,PKR,EGP,CLP,COP,JMD,ETH,BTC,LTC,XRP,XLM,BCH,BNB',
                        ];

                        $response = Http::withHeaders($headers)->get($servicio->url_base.$servicio->uri, $params);

                        $code = $response->status();
                        $servicio->log = $response->json();
                        $servicio->status = $code;
                        if ($code == 200) {
                            $servicio->color = 'success';
                            $servicio->description = 'Tu servicio se encuentra activo.';
                        } else {
                            $servicio->color = 'danger';
                            $servicio->description = 'Tu servicio se encuentra con problemas.';
                            $receptor = 'soporte';
                        }
                        break;

                    case 'stability':
                        $accessToken = 'Bearer sk-0FLk0Kfmz2eB33snhVULWaREThNEebMYFpDyKUkaJWujZ4D8';
                        $response = Http::withHeaders([
                            'Authorization' => $accessToken,
                        ])->get($servicio->url_base.$servicio->uri);
                        $creditos = $response->json()['credits'];
                        $servicio->status = $response->status();
                        $servicio->log = $response->json();
                        if (! isset($creditos)) {
                            $servicio->description = 'El nombre del campo de estado ha sido cambiado a otro nombre. Por favor, verifica la API y corrige esta funcionalidad.';
                            $servicio->color = 'danger';
                            $receptor = 'soporte';
                        } elseif ($creditos < 2400) {
                            $servicio->color = 'warning';
                            $servicio->description = 'Tu crédito actual es de *'.$creditos.'*, el cual cumple con límite mínimo establecido.';
                            $receptor = 'admin';
                        } elseif ($servicio->status == 200) {
                            $servicio->color = 'success';
                            $servicio->description = 'Tu servicio se encuentra activo.';
                        } else {
                            $servicio->color = 'danger';
                            $servicio->description = 'Tu servicio se encuentra con problemas.';
                            $receptor = 'soporte';
                        }
                        break;
                }
                if ($servicio->isDirty('status') || $servicio->isDirty('description') || $servicio->isDirty('color')) {
                    $cambios = true;
                    $fechaActual = Carbon::now();
                    $fechaNotificacion = $servicio->fecha_notificacion ? Carbon::parse($servicio->fecha_notificacion) : null;
                    if ($fechaNotificacion == null || (!$fechaNotificacion->isSameDay($fechaActual) || $servicio->isDirty('color'))) {
                        if ($receptor == "soporte" && $servicio->color != "success") {
                            $this->sendWhatsappSupport($servicio->service, $servicio->status, $servicio->description);
                        } else if ($receptor == "admin" && $servicio->color != "success") {
                            $this->sendWhatsappAdmin($servicio->service, $servicio->description);
                        }
                        $servicio->fecha_notificacion = Carbon::now();
                    }
                    $servicio->save();
                    $this->error('Cambios nuevos para el servicio: ' . $servicio->service);
                } else {
                    $this->info('Ningun cambio para el servicio: '.$servicio->service);
                }
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'Error en estado de servicio '.$servicio->service,
                    'log' => $th->getMessage(),
                ]);
                $this->error('Error en evento: '.$servicio->service.', log:'.$th->getMessage());
            }
        }
        if ($cambios) {
            event(new RefreshStatusServicesEvent('Hubo cambios en los servicios'));
            $this->info('Hubo cambios en los servicios');
        } else {
            $this->line('No se detectaron cambios');
        }
    }

    public function sendWhatsappSupport($servicio, $codigo, $descripcion)
    {
        foreach ($this->supportMembers as $nombre => $valores) {
            WhatsappNotification::sendTemplate()
                ->templateName('disruptive_error_services')
                ->body([$nombre, $servicio, strval($codigo), $descripcion])
                ->to($valores['whatsapp'])
                ->lang('es')
                ->send();
        }
        foreach ($this->administradoresList as $user) {
            WhatsappNotification::sendTemplate()
                ->templateName('disruptive_error_services')
                ->body([$user['name'], $servicio, strval($codigo), $descripcion])
                ->to($user['mobile'])
                ->lang('es')
                ->send();
        }
    }

    public function sendWhatsappAdmin($servicio, $descripcion)
    {
        foreach ($this->administradoresList as $user) {
            WhatsappNotification::sendTemplate()
                ->templateName('disruptive_warning_services')
                ->body([$user['name'], $servicio, $descripcion])
                ->to($user['mobile'])
                ->lang('es')
                ->send();
        }
    }
}
