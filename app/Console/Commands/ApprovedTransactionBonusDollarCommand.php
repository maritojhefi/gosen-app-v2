<?php

namespace App\Console\Commands;

use App\Models\LogsGosen;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ApprovedTransactionBonusDollarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bonusdollars-trans:approved';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que se ejecuta para aprobar las transacciones de compra de tokens realizadas por los bonos en dolares de los usuarios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $hace10Min = Carbon::now()->subMinutes(10)->format('Y-m-d H:i:s');
        $transacciones = Transaction::where([
            ['currency', 'bn.usd'],
            ['status', 'pending'],
            ['tnx_time', '>', $hace10Min],
        ])->get();
        foreach ($transacciones as $transaccion) {
            try {
                DB::beginTransaction();
                $transaccion->tnx_time = now();
                $transaccion->checked_time = now();
                $transaccion->receive_amount = $transaccion->amount;
                $transaccion->status = 'approved';
                $transaccion->details = 'Token Purchase';
                $transaccion->save();
                DB::commit();
                $this->info('Se aprobo la transacccion :'.$transaccion->id.' , y se ajusto el balance de dolares para el usuario: '.$transaccion->user);
                $this->info('');
            } catch (\Throwable $th) {
                DB::rollBack();
                LogsGosen::create([
                    'titulo' => 'Error al aprobar la transaccion: '.$transaccion->id.', para el usuario'.$transaccion->user,
                    'log' => $th->getMessage(),
                ]);
                $this->error('----------------Errores en ejecucion de eventos para la transaccion '.$transaccion->id.'----------');
                $this->info('');
            }
        }
    }
}
