<?php

namespace App\Console\Commands;

use App\Helpers\GosenHelper;
use App\Helpers\TokenCalculate as TC;
use App\Models\LogsGosen;
use App\Models\Paquete;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AddBonusByPackageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bonus:package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adiciona transacciones con un porcentaje del paquete que compro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $paquetes = Paquete::where('estado_bono', true);
        $transacciones = Transaction::where('tnx_type', 'purchase')->where('package', '!=', null)->where('status', 'approved')->get();
        $fechaHoy = Carbon::now();
        foreach ($transacciones as $transaccion) {
            try {
                $paquete = $transaccion->paquete;
                $porcentaje = $paquete->porcentaje_bono;
                $dias = $paquete->dias_bono;
                $limiteFecha = $paquete->fecha_limite_bono->format('Y-m-d');
                $estado = $paquete->estado_bono;
                /*para el bono*/
                $diasVigencia = $paquete->dias_vigencia;
                if ($paquete->estado_fecha_limite != true) {
                    if ($estado && $porcentaje > 0 && $dias > 0 && $diasVigencia > 0) {
                        $diferenciaDias = $fechaHoy->diffInDays(Carbon::parse($transaccion->checked_time));
                        if ($diferenciaDias < $diasVigencia) {
                            if ($diferenciaDias / $dias > 0 && fmod($diferenciaDias, $dias) == 0) {
                                //crea la transaccion
                                $siExiste = Transaction::where('tnx_type', 'bonus_package')->where('details', 'Transaction belongs to tnx_id:' . $transaccion->id)->whereDate('created_at', $fechaHoy)->first();
                                if (!$siExiste) {
                                    $producto = bcmul($porcentaje, $transaccion->tokens, 20); // Producto con 20 decimales de precisión
                                    $tokens = bcdiv($producto, 100, 20); // División con 20 decimales de precisión
                                    $tc = new TC();
                                    $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
                                    $transaction = Transaction::create([
                                        'tokens' => $tokens,
                                        'total_tokens' => $tokens,
                                        'user' => $transaccion->user,
                                        'stage' => active_stage()->id,
                                        'tnx_type' => 'bonus_package',
                                        'transaction_from' => $transaccion->id,
                                        'tnx_time' => now(),
                                        'payment_method' => 'system',
                                        'base_currency' => base_currency(),
                                        'base_currency_rate' => $base_currency_rate,
                                        'checked_by' => '{"name":"System","id":"0"}',
                                        'added_by' => 'SYS-00000',
                                        'checked_time' => now(),
                                        'details' => 'Transaction belongs to tnx_id:' . $transaccion->id,
                                        'tnx_id' => 1,
                                        'package' => $transaccion->package
                                    ]);
                                    $transaction->tnx_id = 'BONUS' . sprintf('%06s', $transaction->id);
                                    $transaction->save();
                                    User::find($transaccion->user)->increment('token_bonus', $tokens);
                                    $this->info('Se creo un bono de paquete para la tnx_id : ' . $transaccion->id . '  usuario: ' . $transaccion->user . '  fecha : ' . $fechaHoy);
                                    GosenHelper::createNotification($transaccion->user, GosenHelper::contenidoNotificacion('bonus_por_paquete', [$transaccion->id, number_format($tokens, 4), 0]));
                                }
                            }
                        }
                    }
                } else {
                    if ($estado && $porcentaje > 0 && $dias > 0 && $fechaHoy->format('Y-m-d') <= $limiteFecha) {
                        $diferenciaDias = $fechaHoy->diffInDays(Carbon::parse($transaccion->checked_time));
                        if ($diferenciaDias / $dias > 0 && fmod($diferenciaDias, $dias) == 0) {
                            //crea la transaccion
                            $siExiste = Transaction::where('tnx_type', 'bonus_package')->where('details', 'Transaction belongs to tnx_id:' . $transaccion->id)->whereDate('created_at', $fechaHoy)->first();
                            if (!$siExiste) {
                                $tokens = ($porcentaje * $transaccion->tokens) / 100;
                                $tc = new TC();
                                $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
                                $transaction = Transaction::create([
                                    'tokens' => $tokens,
                                    'total_tokens' => $tokens,
                                    'user' => $transaccion->user,
                                    'stage' => active_stage()->id,
                                    'tnx_type' => 'bonus_package',
                                    'transaction_from' => $transaccion->id,
                                    'tnx_time' => now(),
                                    'payment_method' => 'system',
                                    'base_currency' => base_currency(),
                                    'base_currency_rate' => $base_currency_rate,
                                    'checked_by' => '{"name":"System","id":"0"}',
                                    'added_by' => 'SYS-00000',
                                    'checked_time' => now(),
                                    'details' => 'Transaction belongs to tnx_id:' . $transaccion->id,
                                    'tnx_id' => 1,
                                    'package' => $transaccion->package
                                ]);
                                $transaction->tnx_id = 'BONUS' . sprintf('%06s', $transaction->id);
                                $transaction->save();
                                User::find($transaccion->user)->increment('token_bonus', $tokens);
                                $this->info('Se creo un bono de paquete para la tnx_id : ' . $transaccion->id . '  usuario: ' . $transaccion->user . '  fecha : ' . $fechaHoy);
                                GosenHelper::createNotification($transaccion->user, GosenHelper::contenidoNotificacion('bonus_por_paquete', [$transaccion->id, number_format($tokens, 4), 0]));
                            }
                        }
                    }
                }
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'Errores al otorgar bono de paquete, ID transaccion : ' . $transaccion->id . ', usuario' . $transaccion->user,
                    'log' => $th->getMessage(),
                ]);
            }
        }
    }
}
