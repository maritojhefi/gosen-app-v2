<?php

namespace App\Console\Commands;

use App\Models\Country;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCountriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando revisa una vez al dia la cantidad de usuarios por Pais';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Ejecutando cron para actualizar la cantidad de usuarios por pais y actualizarlos');
        $usuarios = User::where('email_verified_at', '!=', null)->get();
        $paises = $usuarios->groupBy('country');
        $countries = Country::all();
        $arrayNoExiste = [];
        foreach ($countries as $countries) {
            $paisesCountry = User::where('country', $countries->name)->exists();
            if ($paisesCountry == false) {
                array_push($arrayNoExiste, $countries);
            }
        }
        foreach ($arrayNoExiste as $pais) {
            $pais->delete();
        }
        foreach ($paises as $key => $value) {
            try {
                DB::beginTransaction();
                $pais = Country::where('name', $key)->first();
                $pais->users = $value->count();
                $pais->save();
                $this->info('Se actualizo el numero de usuarios registrados en: '.$pais->name.' el actual numero de usuarios es'.$value->count());
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $this->error('Ocurrio un problema al tratar de actualizar la cantidad del pais: '.$key);
                $this->error($th->getMessage());
            }
        }
        $this->info('Se ejecuto el cron para '.$value->count().' paises  en fecha: '.now());
        $this->info('');
        $this->info('');
    }
}
