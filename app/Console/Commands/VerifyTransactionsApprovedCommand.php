<?php

namespace App\Console\Commands;

use App\Events\AnalyticTypeUsersAdminEvent;
use App\Helpers\GosenHelper;
use App\Models\LogsGosen;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class VerifyTransactionsApprovedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions:approved';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica las transacciones nuevas aprobadas para generar notificaciones, y agregar los bonos dolares a los referidos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $hace10Min = Carbon::now()->subMinutes(1)->format('Y-m-d H:i:s');
        $transacciones = Transaction::where('status', 'approved')->where('updated_at', '>', $hace10Min)->where('notify_sended', false)->get();
        foreach ($transacciones as $transaccion) {
            try {
                $user = User::where('id', $transaccion->user)->first();
                GosenHelper::addDolarBonus($transaccion);
                GosenHelper::createNotification($transaccion->user, GosenHelper::contenidoNotificacion('compra_exitosa_paquete', [$transaccion->package, 0, 0]));
                if ($user->type_user < $transaccion->paquete->type_user) {
                    DB::beginTransaction();
                    $user->type_user = $transaccion->paquete->type_user;
                    $user->save();
                    DB::commit();
                    $array = GosenHelper::tiposCantidadUsuarios();
                    event(new AnalyticTypeUsersAdminEvent($array[0], $array[1]));
                    $this->info('El usuario id: ' . $user->id . ' tuvo cambios en type_user');
                }
                // DB::table('transactions')->where('id',$transaccion->id)->update(['notify_sended'=>true]);
                $transaccion->notify_sended = true;
                $transaccion->save();
                $this->info('Se envio una notificacion al usuario: '.$user->id.' , idTransaccion: '.$transaccion->id);
                $this->info('');
            } catch (\Throwable $th) {
                DB::rollBack();
                LogsGosen::create([
                    'titulo' => 'Error al ejecutar eventos para el usuario: ' . $transaccion->user . ' ' . $user->type_user,
                    'log' => $th->getMessage() . ' linea de error:' . $th->getLine()
                ]);
                $this->error('----------------Errores en ejecucion de eventos para idUsuario ' . $transaccion->user . '----------');
                $this->info('');
            }
        }
    }
}
