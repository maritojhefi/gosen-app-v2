<?php

namespace App\Console\Commands;

use App\Helpers\GosenHelper;
use App\Models\BonusDolar;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddBonusDollarsToUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approved:bonusdollars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando cambia a estado aprobado y disponible los bonos que se dieron en las anteriores 72 hrs y no fueron reeembolsados-refund por el usuario que compro los tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Ejecutando cron para cambiar el estado de los bonos que se dieron en las anteriores 72hrs');
        $fechaBonus = Carbon::now()->subDays(3);
        $bonus = BonusDolar::where('created_at', '<', $fechaBonus)->where([
            'status' => 'pending',
            'approved_bonus' => false,
            'type' => 'bonus',
        ])->get();
        $cont = 0;
        foreach ($bonus as $bon) {
            try {
                DB::beginTransaction();
                // GosenHelper::addDolarBonus($bon);
                $bon->status = BonusDolar::STATUS2;
                $bon->approved_bonus = true;
                $bon->save();
                $this->info('Se cambio exitosamente el estado de la transaccion: '.$bon->code);
                $cont++;
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $this->error('Ocurrio un problema al cambiar el estado para la transacion: '.$bon->code);
                $this->error($th->getMessage());
            }
        }
        $this->info('Se ejecuto el cron para '.$cont.' Bonos Dolares en fecha : '.now());
        $this->info('');
        $this->info('');
    }
}
