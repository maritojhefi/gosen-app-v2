<?php

namespace App\Console\Commands;

use App\Helpers\WhatsappNotification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendWhatsappScheduleUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whatsapp:scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de mensajes whatsapp despues de verificar la cuenta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $whatsapps = config('whatsapps-messages');
        $contador = 0;
        foreach ($whatsapps as $key => $dias) {
            $fechaActual = Carbon::now();
            $date = $fechaActual->subDays($dias)->format('Y-m-d');
            $users = User::select('id', 'email', 'mobile', 'lang', 'name')->where('first_login', true)->whereDate('whatsapp_verified_at', $date)->get();
            foreach ($users as $user) {
                switch ($user->lang) {
                    case 'pr':
                        $lang = 'pt_BR';
                        break;
                    default:
                        $lang = $user->lang;
                        break;
                }
                switch ($key) {
                    case 'dis_verified_1':
                        // dd($user->toArray(),$key,$lang);
                        WhatsappNotification::sendTemplate()
                            ->templateName($key)
                            ->body([$user->name])
                            ->to($user->mobile)
                            ->lang($lang)
                            ->send();

                        $this->info('Se envio el whatsapp de clave: '.$key.' para el usuario id: '.$user->id);
                        $contador++;
                        break;
                    case 'dis_verified_2':
                        WhatsappNotification::sendTemplate()
                            ->templateName($key)
                            ->header(asset('dis_verified_2.jpg'), 'image')
                            ->body([$user->name])
                            ->button('user/referral')
                            ->to($user->mobile)
                            ->lang($lang)
                            ->send();
                        $this->info('Se envio el whatsapp de clave: '.$key.' para el usuario id: '.$user->id);
                        $contador++;
                        break;
                    case 'dis_verified_3':
                        WhatsappNotification::sendTemplate()
                            ->templateName($key)
                            ->body([$user->name])
                            ->to($user->mobile)
                            ->lang($lang)
                            ->send();
                        $this->info('Se envio el whatsapp de clave: '.$key.' para el usuario id: '.$user->id);
                        $contador++;
                        break;
                    default:
                        // code...
                        break;
                }
            }
        }
        if ($contador > 0) {
            $this->info('Se enviaron '.$contador.' mensajes whatsapp en fecha: '.Carbon::now());
        }
    }
}
