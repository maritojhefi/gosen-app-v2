<?php

namespace App\Console\Commands;

use App\Helpers\GosenHelper;
use App\Mail\BonusStagingUserMail;
use App\Models\LogsGosen;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMailSundayBonusStagingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bonus-staging:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando se ejecuta los dias domingos mostrando las ganancias del usuario de acuerdo a la membresia que compro y de acuerdo a la configuracion en admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fecha = GosenHelper::iniciofindesemana();
        $transactions = Transaction::select('tnx_type', 'transaction_from', 'tnx_id', 'tnx_time', 'tokens', 'transaction_from', 'package', 'user')
            ->where('tnx_type', 'bonus_package')
            ->where([['tnx_time', '>', $fecha[0]], ['tnx_time', '<', $fecha[1]]])
            ->get();
        if ($transactions->isNotEmpty()) {
            foreach ($transactions->groupBy('transaction_from') as $trans) {
                try {
                    $acumuladosemana = 0;
                    $transaccion = '';
                    $contadordetransacciones = 0;
                    foreach ($trans as $item) {
                        $acumuladosemana = $acumuladosemana + $item->tokens;
                        $transaccion = $item->transaction_from;
                        $contadordetransacciones++;
                    }
                    $transaccionOriginal = Transaction::find($transaccion);
                    $activeMail = GosenHelper::notification_email_active($transaccionOriginal->tnxUser->id);
                    if ($activeMail == true) {
                        Mail::to($transaccionOriginal->tnxUser->email)
                            ->send(new BonusStagingUserMail($transaccionOriginal, $acumuladosemana, $contadordetransacciones));
                        $this->info('Se ejecuto el cron para dar enviar correos de bonus staking acumulado para la transaccion:'.$transaccionOriginal->id);
                    } else {
                        $this->info('El usuario'.$transaccionOriginal->tnxUser->id.', tiene desactivado el envio de correos');
                        LogsGosen::create([
                            'titulo' => 'Envio de correos Desactivado',
                            'log' => 'El usuario '.$transaccionOriginal->tnxUser->id.', tiene desactivado el envio de correos',
                        ]);
                    }
                } catch (\Throwable $th) {
                    $this->error('No se pudo enviar el correo de bonus staking');
                    $this->error($th->getMessage());
                    LogsGosen::create([
                        'titulo' => 'Error al ejecutar el envio de correo para bonus staking',
                        'log' => $th->getMessage().' linea de error:'.$th->getLine(),
                    ]);
                }
            }
        }
    }
}
