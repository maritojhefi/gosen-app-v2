<?php

namespace App\Console\Commands;

use App\Models\DatabaseSize;
use App\Models\LogsGosen;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DailyBackUpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para realizar backups diarios de la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $filename = Carbon::now()->format('Y-m-d').'.gz';
            $directory = public_path('backup');
            // Verificar si el directorio no existe y crearlo si es necesario
            if (! File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }
            $command = 'mysqldump --user='.env('DB_USERNAME').' --password='.env('DB_PASSWORD').' --host='.env('DB_HOST').' '.env('DB_DATABASE').'  | gzip > '.public_path().'/backup/'.$filename;
            $returnVar = null;
            $output = null;
            exec($command, $output, $returnVar);
            $result = DB::select('SELECT ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size (MB)" FROM information_schema.tables WHERE table_schema = ?', [env('DB_DATABASE')]);
            $size = $result[0]->{'Size (MB)'};
            $pesados = [];
            // Obtener las 3 tablas más pesadas con su peso en MB
            $heaviestTables = DB::select('SELECT table_name AS "Table", ROUND(((data_length + index_length) / 1024 / 1024), 2) AS "Size (MB)" FROM information_schema.tables WHERE table_schema = ? ORDER BY (data_length + index_length) DESC LIMIT 3', [env('DB_DATABASE')]);
            foreach ($heaviestTables as $table) {
                array_push($pesados, $table->{'Table'}.' => '.$table->{'Size (MB)'}.' MB');
            }
            $livianos = [];
            // Obtener las 3 tablas más livianas con su peso en MB
            $lightestTables = DB::select('SELECT table_name AS "Table", ROUND(((data_length + index_length) / 1024 / 1024), 2) AS "Size (MB)" FROM information_schema.tables WHERE table_schema = ? ORDER BY (data_length + index_length) ASC LIMIT 3', [env('DB_DATABASE')]);
            foreach ($lightestTables as $table) {
                array_push($livianos, $table->{'Table'}.' => '.$table->{'Size (MB)'}.' MB');
            }
            DB::beginTransaction();
            $databaseInfo = DatabaseSize::create([
                'name_backup' => $filename,
                'weight' => $size,
                'top_three_heavier' => json_encode($pesados),
                'top_three_lightest' => json_encode($livianos),
            ]);
            $databaseInfo->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            LogsGosen::create([
                'titulo' => 'Error al ejecutar el backup diario de la base de datos',
                'log' => $th->getMessage().' linea de error:'.$th->getLine(),
            ]);
        }
    }
}
