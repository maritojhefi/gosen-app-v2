<?php

namespace App\Console\Commands;

use App\Helpers\GosenHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RefreshCoinRatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza los valores de cada moneda en base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client();
        $request = $client->request('GET', 'https://app.disruptive.center/api/refresh/rates');
        $request = $client->request('GET', 'https://test.disruptive.center/api/refresh/rates');
        //GosenHelper::refreshRates();
        $this->info('ejecucion finalizada ');
        $this->info(Carbon::now());
        $this->info('');
    }
}
