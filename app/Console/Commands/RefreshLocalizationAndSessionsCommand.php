<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class RefreshLocalizationAndSessionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza y elimina las sesiones inactivas ademas de actualizar en DB los registros de localizacion de los usuarios que tienen una sesion activa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Ejecutando cron para actualizar sesiones y ubicaciones...');
        $this->info('');
        $sesiones = DB::table('sessions')->get();
        foreach ($sesiones as $sesion) {
            $sesionRevisada = Session::getHandler()->read(''.$sesion->id);
            if ($sesionRevisada == null || $sesionRevisada == '') {
                DB::table('sessions')->where('id', $sesion->id)->delete();
                if ($sesion->user_id) {
                    $this->info('Se borro la sesion para el usuario id = '.$sesion->id);
                } else {
                    $this->info('Se borro una sesion desconocida');
                }
            }
        }
        $sesiones = DB::table('sessions')->where('user_id', '!=', null)->get();
        $sesiones = $sesiones->groupBy('user_id');
        $cont = 0;
        foreach ($sesiones as $idUser => $array) {
            try {
                DB::beginTransaction();
                $user = User::find($idUser);
                $sesion = $array->first();
                $response = Http::get('http://ipwho.is/'.$sesion->ip_address);
                $respuesta = json_decode($response->body());
                $user->latitude = $respuesta->latitude;
                $user->longitude = $respuesta->longitude;
                $user->city = $respuesta->city;
                $user->region = $respuesta->region;
                $user->country = $respuesta->country;
                $user->save();
                DB::commit();
                $cont++;
                $this->info('se actualizo la ubicacion para el usuario: '.$user->id);
            } catch (\Throwable $th) {
                DB::rollBack();
                $this->error('Algo salio mal al intentar actualizar la ubicacion para el usuario: '.$user->id);
                $this->error($th->getMessage());
            }
        }
        $this->info('Ejecucion finalizada, actualizacion para : '.$cont.' usuarios, finalizado en fecha: '.now());
        $this->info('');
    }
}
