<?php

namespace App\Console\Commands;

use App\Mail\WithdrawGosenRejectMail;
use App\Models\BonusDolar;
use App\Models\LogsGosen;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CanceledWithdrawBonusUsdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw-user:canceled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando cancela en un plazo de 24 horas las transacciones de retiro de los bonus USD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $transWithdraw = BonusDolar::where([
            ['status', BonusDolar::STATUS1],
            ['type', BonusDolar::TYPE1],
            ['approved_bonus', 0],
        ])->get();
        foreach ($transWithdraw as $item) {
            $fechaTransaction = $item->created_at->format('Y-m-d H:i:s');
            $diferencia = Carbon::now()->diffInHours($fechaTransaction);
            if ($diferencia > 24) {
                try {
                    DB::beginTransaction();
                    $item->status = BonusDolar::STATUS3;
                    $item->save();
                    Mail::to($item->userBy->email)
                    ->send(new WithdrawGosenRejectMail($item));
                    DB::commit();
                    $this->info('Se cancelo la transacccion de retiro con el id:'.$item->id.'para el usuario'.$item->user_id);
                    $this->info('');
                } catch (\Throwable $th) {
                    DB::rollBack();
                    LogsGosen::create([
                        'titulo' => 'Error al cancelar retiro bonus dollar con el id:'.$item->id.'para el usuario'.$item->user_id,
                        'log' => $th->getMessage(),
                    ]);
                    $this->error('----------------Errores en ejecucion de eventos para la transaccion '.$item->id.'----------');
                    $this->info('');
                }
            }
        }
    }
}
