<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AnalyticTypeUsersAdminEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $array3;

    public $array4;

    public function __construct($array3, $array4)
    {
        $array = [];
        foreach ($array3 as $item) {
            array_push($array, str_replace(' ', '', $item));
        }
        $this->array3 = $array;
        $this->array4 = $array4;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('refresh-type');
    }

    public function broadcastAs()
    {
        return 'hola.event';
    }
}
