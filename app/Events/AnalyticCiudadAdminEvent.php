<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AnalyticCiudadAdminEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $arrayNomCiudad;

    public $arrayCantCiudad;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($arrayNomCiudad, $arrayCantCiudad)
    {
        $this->arrayNomCiudad = $arrayNomCiudad;
        $this->arrayCantCiudad = $arrayCantCiudad;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('refresh-ciudad');
    }

    public function broadcastAs()
    {
        return 'ciudades.populares';
    }
}
