<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ModalBonusReferralGiftEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user;

    public $cantidadRegalo;

    public $icono;

    public $nombre;

    public function __construct($user, $cantidadRegalo, $icono, $nombre)
    {
        $this->user = $user;
        $this->cantidadRegalo = $cantidadRegalo;
        $this->icono = $icono;
        $this->nombre = $nombre;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // return new Channel('refresh-gift');
        return new Channel('gift-user-'.$this->user);
    }

    public function broadcastAs()
    {
        return 'refresh.gift';
    }
}
