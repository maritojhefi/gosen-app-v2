<?php

namespace App\Observers;

use App\Models\LogsGosen;
use App\Models\Paquete;
use App\Models\Transaction;

class TransactionObserver
{
    /**
     * Handle the Transaction "created" event.
     *
     * @return void
     */
    public function created(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the Transaction "updated" event.
     *
     * @return void
     */
    public function updated(Transaction $transaction)
    {
        if (($transaction->package == 1 || $transaction->package == 10) && $transaction->nft == null) {
            try {
                if($transaction->package == 1 ){
                    $transaction->nft = 'tarjeta-plata.jpg';
                }else{
                    $transaction->nft = 'tarjeta-dorada.jpg';
                }
                $transaction->save();
                LogsGosen::create([
                    'titulo' => 'Se dio el nft basico para la transaccion : '.$transaction->id,
                    'log' => '',
                ]);
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'error al momento de darle el nft basico para la transaccion: '.$transaction->id,
                    'log' => $th->getMessage().' linea de error:'.$th->getLine(),
                ]);
            }
        }

        if ($transaction->status == 'approved' && $transaction->isDirty('notify_sended')) {
            try {
                $paquete = Paquete::find($transaction->package);
                $paquete->stock = $paquete->stock - 1;
                $paquete->save();
                LogsGosen::create([
                    'titulo' => 'se redujo el stock del paquete'.$paquete->id.'para la trasaccion: '.$transaction->id.'el stock actual es de'.$paquete->stock,
                    'log' => '',
                ]);
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'error en el observador de transaccion: '.$transaction->id,
                    'log' => $th->getMessage().' linea de error:'.$th->getLine(),
                ]);
            }
        }
    }

    /**
     * Handle the Transaction "deleted" event.
     *
     * @return void
     */
    public function deleted(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the Transaction "restored" event.
     *
     * @return void
     */
    public function restored(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the Transaction "force deleted" event.
     *
     * @return void
     */
    public function forceDeleted(Transaction $transaction)
    {
        //
    }
}
