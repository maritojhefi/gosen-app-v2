<?php

namespace App\Observers;

use App\Helpers\GosenHelper;
use App\Mail\LevelUpGiftUserMail;
use App\Models\BonusReferral;
use Illuminate\Support\Facades\Mail;

class BonusReferralObserver
{
    /**
     * Handle the BonusReferral "created" event.
     *
     * @return void
     */
    public function created(BonusReferral $bonusReferral)
    {
        //
    }

    /**
     * Handle the BonusReferral "updated" event.
     *
     * @return void
     */
    public function updating(BonusReferral $bonusReferral)
    {
        if ($bonusReferral->isDirty('regalo_tipo_id')) {
            if ($bonusReferral->regalo_tipo_id > 1) {
                $iconoGift = GosenHelper::getIconGiftUserReferrals($bonusReferral->regalo_tipo_id);
                Mail::to($bonusReferral->user->email)
                    ->send(new LevelUpGiftUserMail($iconoGift, $bonusReferral));
            }
        }
    }

    public function updated(BonusReferral $bonusReferral)
    {
        //
    }

    /**
     * Handle the BonusReferral "deleted" event.
     *
     * @return void
     */
    public function deleted(BonusReferral $bonusReferral)
    {
        //
    }

    /**
     * Handle the BonusReferral "restored" event.
     *
     * @return void
     */
    public function restored(BonusReferral $bonusReferral)
    {
        //
    }

    /**
     * Handle the BonusReferral "force deleted" event.
     *
     * @return void
     */
    public function forceDeleted(BonusReferral $bonusReferral)
    {
        //
    }
}
