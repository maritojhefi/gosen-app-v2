<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->configureRateLimiting();

        $this->routes(function () {
            $this->mapApiRoutes();
            $this->mapWebRoutes();
            $this->mapPayModuleRoutes();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }

    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapPayModuleRoutes();

        // $this->mapNioModuleRoutes();  @v1.1.2
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->group(base_path('routes/tenant.php'));

        Route::middleware('web')
            ->group(base_path('routes/tenant-mario.php'));

        Route::middleware('web')
            ->group(base_path('routes/tenant-ervin.php'));

        Route::middleware('web')
            ->group(base_path('routes/tenant-rodrick.php'));

        Route::middleware('web')
            ->group(base_path('routes/disruptive/disruptive-mario.php'));

        Route::middleware('web')
            ->group(base_path('routes/disruptive/disruptive-ervin.php'));

        Route::middleware('web')
            ->group(base_path('routes/disruptive/disruptive-rodrick.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            // ->domain($domain)
            ->middleware('api')
            ->name('api.')
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "payment-module" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapPayModuleRoutes()
    {
        Route::prefix('payment')
            ->name('payment.')

            ->middleware('web')
            ->group(app_path('PayModule/routes.php'));
    }

    /**
     * Define the "module" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapNioModuleRoutes()
    {
        foreach (nio_module()->loadModule() as $module) {
            if (is_dir(app_path('NioModules/'.$module)) && file_exists(app_path('NioModules/'.$module.'/routes.php'))) {
                Route::name('module::'.strtolower($module).'.')
                    // ->prefix('admin')

                    ->middleware('web')
                    ->group(app_path('NioModules/'.$module.'/routes.php'));
            }
        }
    }
}
