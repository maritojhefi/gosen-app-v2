<?php

namespace App\Jobs;

use App\Helpers\GosenHelper;
use App\Mail\CustomMassiveMail;
use App\Models\HistorySendedMail;
use App\Models\LogsGosen;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMassiveMailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $users;

    public $header;

    public $content;

    public $footer;

    public $type_mail;

    public $tiposString;

    public $totalUsuarios;

    public function __construct($users, $header, $content, $footer, $type_mail, $tiposString, $totalUsuarios)
    {
        $this->users = $users;
        $this->header = $header;
        $this->content = $content;
        $this->footer = $footer;
        $this->type_mail = $type_mail;
        $this->tiposString = $tiposString;
        $this->totalUsuarios = $totalUsuarios;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $total = $this->totalUsuarios;
        foreach ($this->users as $user) {
            $usuario_email_active = User::select('id', 'name', 'email')->where('email', $user)->get();
            $activeMail = GosenHelper::notification_email_active($usuario_email_active[0]->id);
            if ($activeMail == true) {
                try {
                    $header = str_replace('{user}', $usuario_email_active[0]->name, $this->header);
                    $content = str_replace('{user}', $usuario_email_active[0]->name, $this->content);
                    // Reemplazar el string {name_app} por "disruptive"
                    $header = str_replace('{name_app}', ucfirst(env('APP_NAME')), $header);
                    $content = str_replace('{name_app}', ucfirst(env('APP_NAME')), $content);
                    Mail::to($user)
                        ->send(new CustomMassiveMail($header, $content, $this->footer, $this->type_mail));
                    LogsGosen::create(['titulo' => 'email enviado a '.$user, 'log' => '']);
                } catch (\Throwable $th) {
                    LogsGosen::create(['titulo' => 'Error al enviar correo a: '.$user, 'log' => $th->getMessage()]);
                }
            } else {
                LogsGosen::create([
                    'titulo' => 'Envio de correos Desactivado',
                    'log' => 'El usuario '.$usuario_email_active[0]->id.', tiene desactivado el envio de correos',
                ]);
            }
        }
        $registro = HistorySendedMail::where('content', $this->content)->where('header', $this->header)->where('footer', $this->footer)->first();
        $registro->total = $total;
        $registro->save();
        LogsGosen::create(['titulo' => 'Correo enviado a administrador', 'log' => '']);
    }
}
