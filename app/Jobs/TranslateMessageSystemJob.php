<?php

namespace App\Jobs;

use App\Models\Translate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateMessageSystemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $messages = Translate::all();
        foreach ($messages  as $msg) {
            $texto = $msg->text;
            $llave = $msg->key;
            $exp = '/[\'^£$%&*}{#~><>|=_+¬]/';
            if (preg_match($exp, $texto)) {
                continue;
            }
            if ((strpos($texto, 'Check out here')) == true || (strpos($texto, 'laquo')) == true || (strpos($texto, 'raquo')) == true || (strpos($texto, 'quot')) == true) {
                continue;
            }
            $espanol = GoogleTranslate::trans($texto, 'es');
            $ingles = GoogleTranslate::trans($texto, 'en');
            $portugues = GoogleTranslate::trans($texto, 'pt');
            $ruso = GoogleTranslate::trans($texto, 'ru');
            $italiano = GoogleTranslate::trans($texto, 'it');
            $frances = GoogleTranslate::trans($texto, 'fr');
            $hindu = GoogleTranslate::trans($texto, 'hi');
            // $arabe = GoogleTranslate::trans($texto, 'ar');
            $turkish = GoogleTranslate::trans($texto, 'tr');
            $german = GoogleTranslate::trans($texto, 'de');
            $chinese = GoogleTranslate::trans($texto, 'zh');
            $idiomas = [
                'es' => $espanol,
                'en' => $ingles,
                'pr' => $portugues,
                'ru' => $ruso,
                'it' => $italiano,
                'fr' => $frances,
                'hi' => $hindu,
                // 'ar' => $arabe,
                'tr' => $turkish,
                'de' => $german,
                'zh' => $chinese,
            ];
            $coleccion = collect();
            $contador = 0;
            foreach ($idiomas as $idioma => $traduccion) {
                $jsonString = file_get_contents(base_path('resources/lang/'.$idioma.'.json'));
                $data = json_decode($jsonString, true);
                if (! isset($data[$llave])) {
                    $coleccion->push([
                        'key' => $llave,
                        'value' => $traduccion,
                    ]);
                    $contador++;
                    $data[$llave] = str_replace('\\', '', $traduccion);
                    $newJsonString = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    file_put_contents(base_path('resources/lang/'.$idioma.'.json'), stripslashes($newJsonString));
                }
            }
        }
    }
}
