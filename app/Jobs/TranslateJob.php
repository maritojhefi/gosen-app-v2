<?php

namespace App\Jobs;

use App\Models\LogsGosen;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $array;

    public function __construct($array)
    {
        $this->array = $array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //dd($this->array);

        foreach ($this->array as $key => $input) {
            $input = str_replace('\\', '', $input);
            if ($key == '_token') {
                continue;
            }
            if (is_numeric($input)) {
                continue;
            }
            if (is_array($input)) {
                continue;
            }
            if (preg_match('/[\'^£$%&*}{#~><>|=_+¬]/', $input)) {
                continue;
            }
            $espanol = GoogleTranslate::trans($input, 'es');
            $ingles = GoogleTranslate::trans($input, 'en');
            $portugues = GoogleTranslate::trans($input, 'pt');
            // $ruso = GoogleTranslate::trans($input, 'ru');
            // $italiano = GoogleTranslate::trans($input, 'it');
            // $frances = GoogleTranslate::trans($input, 'fr');
            // $hindu = GoogleTranslate::trans($input, 'hi');
            // // $arabe = GoogleTranslate::trans($input, 'ar');
            // $turkish = GoogleTranslate::trans($input, 'tr');
            // $german = GoogleTranslate::trans($input, 'de');
            // $chinese = GoogleTranslate::trans($input, 'zh');
            //dd($espanol.' '.$ingles.' '.$portugues);
            $idiomas = [
                'es' => $espanol,
                'en' => $ingles,
                'pr' => $portugues,
                // 'ru' => $ruso,
                // 'it' => $italiano,
                // 'fr' => $frances,
                // 'hi' => $hindu,
                // // 'ar' => $arabe,
                // 'tr' => $turkish,
                // 'de' => $german,
                // 'zh' => $chinese

            ];
            $input = str_replace('"', '´´', $input);
            $input = str_replace('\\', '', $input);
            foreach ($idiomas as $idioma => $traduccion) {
                $traduccion = str_replace('/[\'^£$%&*}{#~><>|=_+¬]/', '', $traduccion);
                $jsonString = file_get_contents(base_path('resources/lang/'.$idioma.'.json'));
                $data = json_decode($jsonString, true);
                $cantidadOriginal = count($data);
                // Update Key
                if (! isset($data[$input])) {
                    $data[$input] = str_replace('\\', '', $traduccion);
                    $data[$input] = str_replace('"', '´´', $traduccion);
                    $newJsonString = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    $cantidadNueva = count(json_decode($newJsonString, true));
                    try {
                        if ($cantidadNueva >= $cantidadOriginal) {
                            file_put_contents(base_path('resources/lang/'.$idioma.'.json'), stripslashes($newJsonString));
                        }
                    } catch (\Throwable $th) {
                        LogsGosen::create([
                            'titulo' => 'Fallo de traduccion',
                            'log' => 'llave: '.$input,
                        ]);
                    }
                }

                //usleep(2000000);
            }
        }
    }
}
