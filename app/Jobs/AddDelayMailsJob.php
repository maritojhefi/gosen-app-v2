<?php

namespace App\Jobs;

use App\Mail\CustomMassiveMail;
use App\Models\LogsGosen;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AddDelayMailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $users;

    public $header;

    public $content;

    public $footer;

    public function __construct($users, $header, $content, $footer)
    {
        $this->users = $users;
        $this->header = $header;
        $this->content = $content;
        $this->footer = $footer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            try {
                Mail::to('villaortiz110@gmail.com')
                ->send(new CustomMassiveMail($this->header, $this->content, $this->footer));

                LogsGosen::create(['titulo' => 'email enviado a '.$user->email, 'log' => '']);
            } catch (\Throwable $th) {
                LogsGosen::create(['titulo' => 'Error al enviar correo a: '.$user->email, 'log' => $th->getMessage()]);
            }
        }
    }
}
