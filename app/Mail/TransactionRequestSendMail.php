<?php

namespace App\Mail;

use App\Helpers\TokenCalculate as TC;
use App\Models\Paquete;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransactionRequestSendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $tnx;

    public $user;

    public $sigla;

    public function __construct($tnx, $user, $sigla)
    {
        $this->tnx = $tnx;
        $this->user = $user;
        $this->sigla = $sigla;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tc = new TC();
        $btcc = 'btc';
        $token_prices = $tc->calc_token(1, 'price');
        $curre = $this->tnx->currency;
        $precioUsd = $this->tnx->amount;
        $btcPrice = $this->tnx->amount;
        // $paquete = Paquete::find($this->tnx->package);
        // $priceInUsd = (($paquete->valor_usd * ($paquete->descuento) / 100) == 0 ? $paquete->valor_usd : ($paquete->valor_usd - ($paquete->valor_usd * ($paquete->descuento) / 100))).' '.'USD';
        $priceInUsd = $this->tnx->base_amount;
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Solicitud de compra'))
            ->view('mail.layout-2022.mailRequestTransaction', compact('btcPrice', 'precioUsd', 'priceInUsd'));
    }
}
