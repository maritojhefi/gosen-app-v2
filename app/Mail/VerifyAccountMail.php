<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyAccountMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $ruta;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $ruta)
    {
        $this->user = $user;
        $this->ruta = $ruta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Account Verification'))
            ->view('mail.layout-2022.mailVerifyAccount');
    }
}
