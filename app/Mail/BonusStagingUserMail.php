<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BonusStagingUserMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $transaction;

    public $acumulado;

    public $contador;

    public function __construct($transaction, $acumulado, $contador)
    {
        $this->transaction = $transaction;
        $this->acumulado = $acumulado;
        $this->contador = $contador;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Recibiste Bonus Staking'))
            ->view('mail.layout-2022.mailBonusStagingUser');
    }
}
