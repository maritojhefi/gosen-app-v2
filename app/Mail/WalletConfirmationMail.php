<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WalletConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;

    public $ruta;

    public $wallet;

    public function __construct($user, $ruta, $wallet)
    {
        $this->user = $user;
        $this->ruta = $ruta;
        $this->wallet = $wallet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Wallet Update Request'))
            ->view('mail.layout-2022.mailUpdateWallet');
    }
}
