<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LevelUpGiftUserMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $icono;

    public $bonusReferral;

    public function __construct($icono, $bonusReferral)
    {
        $this->icono = $icono;
        $this->bonusReferral = $bonusReferral;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Level Up'))
            ->view('mail.layout-2022.mailLevelUpGiftUserMail');
    }
}
