<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $change;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $change)
    {
        $this->user = $user;
        $this->change = $change;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Mail change request'))
            ->view('mail.layout-2022.mailChange');
    }
}
