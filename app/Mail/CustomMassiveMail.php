<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomMassiveMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $header;

    public $content;

    public $footer;

    public $type_mail;

    public function __construct($header, $content, $footer, $type_mail)
    {
        $this->header = $header;
        $this->footer = $footer;
        if ($header == null || $header == '') {
            $this->header = 'Disruptive';
            $this->footer = ' ';
        }
        $this->content = $content;
        $this->type_mail = $type_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___($this->header))
            ->view('mail.layout-2022.mailCustomLayout');
    }
}
