<?php

namespace App\Mail;

use App\Helpers\TokenCalculate as TC;
use App\Models\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransactionRequestApprovedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $trnx_id;

    public $stage;

    public $tokens;

    public $total_bonus;

    public $total_tokens;

    public $status;

    public $amount;

    public $currency;

    public $sigla;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trnx_id, $stage, $tokens, $total_bonus, $total_tokens, $status, $amount, $currency, $sigla)
    {
        $this->trnx_id = $trnx_id;
        $this->stage = $stage;
        $this->tokens = $tokens;
        $this->total_bonus = $total_bonus;
        $this->total_tokens = $total_tokens;
        $this->status = $status;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->sigla = $sigla;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $trans = Transaction::where('tnx_id', $this->trnx_id)->first();
        // $package = $trans->paquete->nombre;
        $tc = new TC();
        $btcc = 'btc';
        $token_prices = $tc->calc_token(1, 'price');
        $curre = $this->currency;
        $precioUsd = $this->amount;
        $btcPrice = $this->amount;

        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Transaction Approved'))
            ->view('mail.layout-2022.mailConfirmTransaction', compact('btcPrice', 'precioUsd'/*, 'package'*/));
    }
}
