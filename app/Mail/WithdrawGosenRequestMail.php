<?php

namespace App\Mail;

use App\Models\BonusDolar;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class WithdrawGosenRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $bonus;

    public function __construct($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = Crypt::encrypt($this->bonus->id);
        $ruta = route('disruptive.confirm.withdraw.usd', $id);
        $user = User::find($this->bonus->user_id);
        $registroBonus = BonusDolar::find($this->bonus->id);
        $horaRetiro = Carbon::parse($registroBonus->created_at)->format('g:i A');

        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject(___('Withdrawal Request'))
            ->view('mail.layout-2022.mailWithdrawGosenRequest', compact('user', 'ruta', 'horaRetiro'));
    }
}
