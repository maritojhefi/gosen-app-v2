<?php

/**
 * Here is the permissions and permissions actions
 *
 * This file for determine route permission automatically, when a user try to access the Controller@action.
 * Usage Note: you can use controller as like resource controller, just write the controller.
 * uses: 'manage_user' => ['ControllerName'] @array
 * Middleware will check all method in this controller automatically.
 */

return [

];
