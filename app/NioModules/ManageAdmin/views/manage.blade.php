@extends('layouts.admin')
@section('title', 'Admin Management')
@section('content')

    <div class="page-content">
        <div class="container">
            @include('layouts.messages')
            @include('vendor.notice')
            <div class="card content-area content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">Admin Management</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li>
                                    <a href="#" class="btn btn-sm btn-auto btn-primary" data-toggle="modal"
                                        data-target="#role-details">
                                        <em class="ti ti-settings"></em><span
                                            class="d-none d-sm-inline-block">Settings</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="gaps-1x"></div>
                    @if ($users)
                        <table class="data-table user-list">
                            <thead>
                                <tr class="data-item data-head">
                                    <th class="data-col data-col-wd-md dt-user">User</th>
                                    <th class="data-col data-col-wd-md dt-email">Email</th>
                                    <th class="data-col dt-login">Last Login</th>
                                    <th class="data-col dt-level">Level</th>
                                    <th class="data-col dt-status">Status</th>
                                    <th class="data-col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr class="data-item">
                                        <td class="data-col data-col-wd-md dt-user">
                                            <div class="d-flex align-items-center">
                                                <div class="fake-class">
                                                    <span class="lead user-name text-wrap">{{ $user->name }}</span>
                                                    <span class="sub user-id">{{ set_id($user->id, 'user') }}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="data-col data-col-wd-md dt-email">
                                            <span
                                                class="sub sub-s2 sub-email text-wrap">{{ explode_user_for_demo($user->email, auth()->user()->type) }}</span>
                                        </td>
                                        <td class="data-col dt-login">
                                            <span
                                                class="sub sub-s2 sub-time">{{ $user->lastLogin && $user->email_verified_at !== null ? _date($user->lastLogin) : 'Not logged yet' }}</span>
                                        </td>
                                        <td class="data-col dt-level">
                                            @if (is_super_admin($user->id, true))
                                                <span
                                                    class="badge badge-md badge-auto {{ is_super_admin($user->id, true) ? 'badge-purple' : 'badge-purple badge-dim' }}">Super
                                                    Admin</span>
                                            @else
                                                @if (json_decode($user->access->value) != null)
                                                    @foreach (json_decode($user->access->value) as $levelsUsers)
                                                        <span
                                                            class="badge p-1 badge-md badge-auto badge-primary{{ $levelsUsers == 'default' ? ' badge-dim' : '' }}">
                                                            {{ ucfirst(str_replace('level', 'Level ', $levelsUsers)) }}
                                                        </span>
                                                    @endforeach
                                                @else
                                                    <small class="text-center">
                                                        {{ __('Este usuario no tiene un rol establecido') }}
                                                    </small>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="data-col dt-status">
                                            <span
                                                class="dt-status-md badge badge-outline badge-md badge-{{ __status($user->status, 'status') }}">{{ __status($user->status, 'text') }}</span>
                                            <span
                                                class="dt-status-sm badge badge-sq badge-outline badge-md badge-{{ __status($user->status, 'status') }}">{{ substr(__status($user->status, 'text'), 0, 1) }}</span>
                                        </td>
                                        <td class="data-col-2 text-right">
                                            @if (!is_super_admin($user->id, true))
                                                <a href="#" class="btn btn-sm btn-auto btn-primary buttonPermisos"
                                                    {{-- data-toggle="modal" " --}} class="ti-control-shuffle"
                                                    id="{{ $user->id }}">
                                                    <i class="ti ti-settings mr-1"></i>{{ ___('Permisos') }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>{{-- .card-innr --}}
            </div>{{-- .card --}}
        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection

@section('modals')
    <div class="modal fade" id="role-details" tabindex="-1">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body popup-body-md">
                    <h3 class="popup-title">Admin Management Settings</h3>
                    <form class="validate-modern manage-admin-setting _reload"
                        action="{{ route('manage_access:admin.setting') }}" method="POST" id="manage-admin-update">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-item">
                                    <div class="input-wrap input-wrap-switch">
                                        <input class="input-switch" name="site_admin_management"
                                            type="checkbox"{{ gws('site_admin_management') == 1 ? ' checked' : '' }}
                                            id="admin-manage-enable">
                                        <label for="admin-manage-enable">Enable Admin Access Management</label>
                                    </div>
                                    <div class="input-note">All <strong>Admin</strong> account will restricted once enable
                                        access management.</div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">Default Level</label>
                                    <div class="input-wrap">
                                        <select class="select select-block select-bordered" name="manage_access_default">
                                            @php
                                                $opt = array_keys(json_decode(gws('manage_access_default', json_encode(['level' => ['none']])), true));
                                            @endphp
                                            @foreach ($levels as $lv)
                                                <option
                                                    {{ $opt[0] == $lv ? 'selected ' : '' }}value="{{ $lv }}">
                                                    {{ str_replace('level', 'Level ', $lv) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                @csrf
                                <input type="hidden" name="actions" value="settings">
                                <button type="submit" class="btn btn-primary">{{ ___('Update') }}</button>
                                <div class="input-note text-danger mt-1">By default all <strong>Admin</strong> account
                                    consider as <strong>SUPER ADMIN</strong> if disable the settings.</div>
                            </div>
                        </div>
                    </form>
                    <div class="sap sap-gap-sm"></div>
                    <table class="fs-12">
                        <thead>
                            <tr>
                                <th class="wide-12 ucap">Levels</th>
                                <th class="ucap">Access Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="border-top">
                                <td><strong>Level 0</strong></td>
                                <td><span
                                        class="badge badgee-auto badge-sm badge-dim badge-success mgt-0-5x mgb-0-5x">super_admin</span>
                                </td>
                            </tr>
                            @foreach ($roles as $role => $permision)
                                <tr class="border-top">
                                    <td><strong>{{ $role }}</strong></td>
                                    <td>
                                        @foreach ($permision as $value)
                                            <span
                                                class="badge badgee-auto badge-sm badge-dim badge-info mgt-0-5x mgb-0-5x">{{ $value }}</span>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>{{-- .modal-content --}}
        </div>
    </div>

    <div class="modal fade " id="admin-permission" tabindex="-1" data-backdrop="static" data-keyboard="false"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-bs-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <div class="popup-content">
                        <div>
                            <h3 class="popup-title mb-3">{{ ___('Admin Management Settings') }} - Admin (<strong
                                    class="admin-id"></strong>)
                            </h3>
                        </div>
                        <span>
                            <strong>
                                {{ ___('Seleccione a continuacion los permisos para el administrador de acuerdo a los niveles') }}:
                            </strong>
                        </span>
                        <form id="formulario">
                            <div class="col-12 mt-3" id="div-select">
                                <select class="js-example-basic-multiple" name="value[]" multiple="multiple"
                                    style="width: 100%;
                            height: auto;">
                                    @foreach ($levels as $lv)
                                        <option id="{{ $lv }}" value="{{ $lv }}" name="value[]">
                                            {{ ucfirst($lv) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-3" style="display: grid; place-items:center;">
                                <button type="submit" id="buttonRolesSave" disabled
                                    class="role-assign-disruptive btn btn-sm btn-success">{{ ___('Guardar') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('header')
    <link preload href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('footer')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#buttonRolesSave').click(function() {
                $(this).addClass('disabled');
                $(this).html(
                    `<span class="spinner-border spinner-border-sm pt-2" role="status" aria-hidden="true"></span>`
                );
            });
            if ($('#admin-permission').hasClass('show')) {
                $(this).addClass('disabled');
            } else {
                $(this).removeClass('disabled');
            }
        });
    </script>
    <script type="text/javascript">
        var idUser;
        //forzar funcionamiento de select2 a partir de un "div" padre
        var select2 = $('.js-example-basic-multiple').select2({
            dropdownParent: $('#div-select'),
            multiple: true,
            tags: "true",
        });
        $(".buttonPermisos").click(function() {
            var myVariable;
            idUser = $(this).prop("id");
            var url = "{{ route('admin.permission') }}";
            var htmloriginal = $('#package-info').html();
            $.ajax({
                method: "get",
                url: url + "?user=" + idUser,
                beforeSend: function() {
                    $('#package-info').html(
                        `<div class="spinner-border d-block mx-auto" style="width: 5rem; height: 5rem;" role="status"></div>`
                    );
                    $('#backk').hide();
                    $("#logocontainer").hide();
                    $(".card-pack").removeClass('d-none');
                },
                success: function(a) {
                    $("#admin-permission").modal('show')
                    $(".admin-id").html(a[0]);
                    myVariable = a[4];
                    // Definir el array de valores por defecto
                    valoresPorDefecto = JSON.parse(myVariable);
                    // Inicializar el Select2 múltiple y establecer los valores por defecto
                    select2.val(valoresPorDefecto).trigger("change");
                }
            })
            $('.role-assign-disruptive').on('click', function(e) {
                var valoresSeleccionados = $('.js-example-basic-multiple').val();
                var user = parseInt(idUser);
                var datos = {
                    role: JSON.stringify(valoresSeleccionados),
                    user: user
                };
                e.preventDefault();
                var $this = $(this),
                    post_url = "{{ route('manage_access:admin.assign') }}";
                swal({
                    title: "Are you sure?",
                    text: "The previous level (access) will be revoke once you assign new level.",
                    icon: "warning",
                    buttons: ['Cancel', 'Confirm'],
                    dangerMode: true
                }).then((data) => {
                    if (data) {
                        post_submit(post_url, datos);
                        window.setTimeout(function() {
                            location.reload(true)
                        }, 1000);
                    }
                });
            });

        });
        //detectamos que opcion se selecciono
        select2.on("select2:select", function(e) {
            $('#buttonRolesSave').removeAttr("disabled");
        });
        //detectamos se opcion se quito funciona con select multiple
        select2.on("select2:unselect", function(e) {
            $('#buttonRolesSave').removeAttr("disabled");
        });
    </script>
@endpush
