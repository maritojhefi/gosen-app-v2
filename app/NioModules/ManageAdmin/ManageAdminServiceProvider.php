<?php

namespace App\NioModules\ManageAdmin;

use Config;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class ManageAdminServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //#
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(nio_module()->getPath('ManageAdmin/views'), 'manage_access');
        $this->loadRoutesFrom(nio_module()->getPath('ManageAdmin/routes.php'));
        $this->registerConfig();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function registerConfig()
    {
        Config::set([
            'admin_roles' => [
                'level1' => ['name' => 'Level 1', 'access' => ['as_super_admin']],
                'level2' => ['name' => 'Level 2', 'access' => ['manage_user', 'manage_tranx', 'manage_kyc']],
                'level3' => ['name' => 'Level 3', 'access' => ['view_user', 'add_user', 'suspend_user']],
                'level4' => ['name' => 'Level 4', 'access' => ['manage_tranx', 'view_tranx', 'add_tranx', 'update_tranx']],
                'level5' => ['name' => 'Level 5', 'access' => ['manage_kyc', 'view_kyc', 'update_kyc']],
                'level6' => ['name' => 'Level 6', 'access' => ['manage_withdraw', 'view_withdraw', 'update_withdraw']],
                'level7' => ['name' => 'Level 7', 'access' => ['manage_bonusdollar']],
                'level8' => ['name' => 'Level 8', 'access' => ['view_all']],
                'level9' => ['name' => 'Level 9', 'access' => ['translator']],
            ],
        ]);
        $this->mergeConfigFrom(__DIR__.'/permissions.php', 'permissions');
    }

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param  string  $path
     * @param  string  $key
     * @return void
     */
    protected function mergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);
        $this->app['config']->set($key, $this->mergeConfig(require $path, $config));
    }

    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @return array
     */
    protected function mergeConfig(array $merge, array $original)
    {
        $array = array_merge($merge, $original);
        foreach ($merge as $key => $value) {
            if (! is_array($value)) {
                continue;
            }
            if (! Arr::exists($original, $key)) {
                continue;
            }
            if (is_numeric($key)) {
                continue;
            }
            $array[$key] = array_unique($this->mergeConfig($value, $original[$key]));
        }

        return $array;
    }
}
