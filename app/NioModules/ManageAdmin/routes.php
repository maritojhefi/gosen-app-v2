<?php

use App\Http\Controllers\App;
use Illuminate\Support\Facades\Route;
use App\NioModules\ManageAdmin\ManageAdminController;

/**
 * Manage Admin Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * ManageAdmin Routes
 *
 * @author Softnio
 *
 * @version 1.0
 */
Route::middleware('web')->name('manage_access:')->group(function () {
    Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin', 'g2fa'])->group(function () {
        Route::get('manage-role', [ManageAdminController::class, 'index'])->name('index');
        Route::post('manage-role/assign', [ManageAdminController::class, 'assign_role'])->name('assign');
        Route::post('manage-role/setting', [ManageAdminController::class, 'manage_settings'])->name('setting');
    });
});
