<?php

namespace App\NioModules\ManageAdmin;

/**
 * Manage Admin Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * ManageAdmin Controller
 *
 * @author Softnio
 *
 * @version 1.0
 */

use App\Http\Controllers\Controller;
use App\Models\GlobalMeta;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;

class ManageAdminController extends Controller
{
    /**
     * Display Manage admin index
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function index()
    {
        $users = User::with('access')->whereNotIn('status', ['deleted'])->where(['role' => 'admin'])->get();
        $roles = $this->get_roles();
        $levels = $this->get_access('only');

        return view('manage_access::manage', ['users' => $users, 'levels' => $levels, 'roles' => $roles]);
    }

    /**
     * Update manage admin settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function manage_settings(Request $request)
    {
        $result['error'] = true;
        $result['msg'] = 'warning';
        $result['icon'] = 'ti ti-alert';
        $result['message'] = __('messages.wrong');
        $actions = $request->input('actions');
        $user_id = auth()->id();
        if (! is_super_admin($user_id, true)) {
            $result['message'] = ___('You do not have enough permissions to perform requested operation.');
        } else {
            if ($actions == 'settings' && (nio_feature() == 'cool' && env_file('3', 2))) {
                $roles = $this->get_access('only');
                $role = $request->input('manage_access_default');
                if (in_array($role, $roles)) {
                    $access[$role] = $this->get_access($role);
                    Setting::updateValue('manage_access_default', json_encode($access));
                }
                Setting::updateValue('site_admin_management', (isset($request->site_admin_management) ? 1 : 0));

                $result['error'] = false;
                $result['msg'] = 'success';
                $result['icon'] = 'ti ti-info-alt';
                $result['message'] = __('messages.update.success', ['what' => 'Admin Management Settings']);
            }
        }

        if ($request->ajax()) {
            return response()->json($result);
        }

        return back()->with([$result['msg'] => $result['message']]);
    }

    /**
     * Update and assign role to the requested user.
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function assign_role(Request $request)
    {
        $result['error'] = true;
        $result['msg'] = 'warning';
        $result['icon'] = 'ti ti-alert';
        $result['message'] = __('messages.wrong');
        $user_id = auth()->id();
        if (! is_super_admin($user_id, true)) {
            $result['message'] = ___('You do not have enough permissions to perform requested operation.');

            return $result;
        } else {
            $roles = $this->get_access('only');
            $role = json_decode($request->role);
            $encontrado = false;
            $posicion = 0;
            foreach ($roles as $item) {
                if (in_array($role[$posicion], $roles)) {
                    $encontrado = true;
                    $posicion += 1;
                    break;
                } else {
                    $encontrado = false;
                }
            }
            if ($encontrado) {
                $user = $request->user;
                if ($user) {
                    $access = $this->get_accessRole($role);
                    $update = GlobalMeta::save_meta((string) 'manage_access', (array) $role, (string) $user, (string) json_encode($access));
                    if ($update) {
                        $result['error'] = false;
                        $result['reload'] = true;
                        $result['msg'] = 'success';
                        $result['icon'] = 'ti ti-info-alt';
                        $result['message'] = __('messages.update.success', ['what' => 'Admin Level']);
                    }
                } else {
                    $result['message'] = ___('Unable to assign admin level.');
                }
            }
        }

        if ($request->ajax()) {
            return response()->json($result);
        }

        return back()->with([$result['msg'] => $result['message']]);
    }

    /**
     * Get Access of Roles from settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function get_access($lv = null)
    {
        $roles = config('admin_roles');
        $role = [];
        foreach ($roles as $level => $setting) {
            $role[$level] = $setting['access'];
        }
        if (! empty($lv)) {
            if ($lv == 'only') {
                return array_keys($role);
            } elseif (isset($role[$lv])) {
                return $role[$lv];
            } else {
                return false;
            }
        }

        return (object) $role;
    }

    private function get_accessRole($roleUser)
    {
        $roles = config('admin_roles');
        $role = [];
        $posicion = 0;
        foreach ($roles as $level => $setting) {
            if (in_array($level, $roleUser)) {
                foreach ($setting['access'] as $item) {
                    if (! in_array($item, $role)) {
                        array_push($role, $item);
                    }
                }
                $posicion += 1;
            }
        }

        return $role;
    }

    /**
     * Get Roles Name and Access from settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function get_roles()
    {
        $roles = config('admin_roles');
        $role = [];
        foreach ($roles as $level) {
            $role[$level['name']] = $level['access'];
        }

        return $role;
    }
}
