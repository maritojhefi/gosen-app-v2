<?php

namespace App\NioModules\Withdraw;

/**
 * Withdraw Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Withdraw Notification Class
 *
 * @author Softnio
 *
 * @version 1.0
 */
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WithdrawNotification extends Notification
{
    /**
     * The withdraw transaction.
     *
     * @var string
     */
    public $withdraw;

    public $template;

    public $wdm;

    /**
     * Create a notification instance.
     *
     * @param  object  $withdraw
     * @return void
     */
    public function __construct($withdraw, $template)
    {
        $this->withdraw = $withdraw;
        $this->template = $template;
        $this->wdm = new WithdrawModule();
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $from_name = email_setting('from_name', get_setting('site_name'));
        $from_email = email_setting('from_email', get_setting('site_email'));

        $et = $this->shortcode_replace($this->wdm->template($this->template));

        return (new MailMessage)
            ->from($from_email, $from_name)
            ->subject($et->subject)
            ->greeting($et->greeting)
            ->salutation($et->regards)
            ->markdown('mail.base')->with(['__message' => $et->message]);
    }

    public function shortcode_replace($template)
    {
        if ($template->slug == $this->wdm->email('admin')) {
            $greeting = str_replace('[[user_name]]', get_admin('name'), $template->greeting);
            $template->greeting = replace_shortcode($greeting);
        }
        $template->subject = replace_shortcode($template->subject);
        $template->greeting = str_replace('[[user_name]]', $this->withdraw->tnxUser->name, $template->greeting);
        $template->regards = $template->regards == true ? replace_shortcode(get_setting('site_mail_footer')) : null;
        $message = $this->replace_msg($template->message);
        $message = str_replace('[[withdraw_details]]', $this->details(), $message);
        $template->message = replace_shortcode($message);

        return $template;
    }

    public function details()
    {
        $table = '<table class="table order">';
        $table .= '<thead><th colspan="3">Withdraw Details:</th></thead>';
        $table .= '<tbody class="text-left">';

        $table .= '<tr><td width="150">Token</td><td width="10">:</td><td><strong>'.abs($this->withdraw->total_tokens).' '.token_symbol().'</strong></td></tr>';
        $table .= '<tr><td width="150">Amount</td><td width="10">:</td><td><strong>'.abs($this->withdraw->amount).' '.strtoupper($this->withdraw->currency).'</strong></td></tr>';
        $table .= '<tr><td>Address ('.short_to_full($this->withdraw->currency).')</td><td>:</td><td><strong>'.$this->withdraw->wallet_address.'</strong></td></tr>';
        if (! empty($this->withdraw->payment_id)) {
            $table .= '<tr><td>TnxHash</td><td>:</td><td><strong>'.$this->withdraw->payment_id.'</strong></td></tr>';
        }
        //$table .= '<tr><td>Request at</td><td>:</td><td><strong>'._date($this->withdraw->created_at).'</strong></td></tr>';

        $table .= '</tbody></table>';

        return $table;
    }

    public function replace_msg($message)
    {
        $wd = $this->withdraw;
        $note = (empty($wd->extra) ? 'Provide wrong wallet address.' : $wd->extra);
        $tnxHash = (empty($wd->payment_id) ? 'not set' : $wd->payment_id);
        $total_token = abs($wd->total_tokens).' '.token_symbol();

        return str_replace(
            ['[[wallet_address]]', '[[total_tokens]]', '[[withdrawal_note]]', '[[payment_hash]]'],
            [$wd->wallet_address, $total_token, $note, $tnxHash],
            $message
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
