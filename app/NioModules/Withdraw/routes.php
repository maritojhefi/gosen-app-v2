<?php

use App\Http\Controllers\App;
use Illuminate\Support\Facades\Route;
use App\NioModules\Withdraw\WithdrawController;

/**
 * Withdraw Module for TokenLite Application
 * To run this module, required TokenLite v1.1.4+ version.
 *
 * Withdraw Module Route
 *
 * @version 1.0
 *
 * @since 1.0.0
 */
Route::middleware('web')->name('withdraw:')->group(function () {
    Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin', 'g2fa'])->group(function () {
        Route::get('/withdraw', [WithdrawController::class, 'index'])->name('index');
        Route::post('/withdraw/update', [WithdrawController::class, 'update_transaction'])->name('update');
        Route::post('/withdraw/setting', [WithdrawController::class, 'update_settings'])->name('update.setting');
        Route::post('/withdraw/details', [WithdrawController::class, 'view_details'])->name('details');
    });
    Route::prefix('user')->name('user.')->middleware(['auth', 'user', 'verify_user', 'g2fa'])->group(function () {
        Route::get('/withdraw', [WithdrawController::class, 'user_index'])->name('index');
        Route::post('/withdraw/action', [WithdrawController::class, 'user_withdraw_action'])->name('update');
        Route::post('/withdraw/request', [WithdrawController::class, 'user_request'])->name('request');
        Route::post('/withdraw/request-bonus', [WithdrawController::class, 'user_request_bonus'])->name('request-bonus');
        Route::post('/withdraw/details', [WithdrawController::class, 'view_details'])->name('details');
    });
});
