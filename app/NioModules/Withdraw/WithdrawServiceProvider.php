<?php

namespace App\NioModules\Withdraw;

use Config;
use Illuminate\Support\ServiceProvider;

class WithdrawServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //#
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(nio_module()->getPath('Withdraw/views'), 'withdraw');
        $this->loadRoutesFrom(nio_module()->getPath('Withdraw/routes.php'));
        $this->registerConfig();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function registerConfig()
    {
        $is_enabled = gws('withdraw_enable', 0);
        $get_status = gws('withdraw_status', 0);
        Config::set([
            'withdraw' => [
                'userpanel' => [
                    'enable' => $is_enabled,
                    'status' => $get_status,
                    'message' => gws('withdraw_status_message'),
                    'title' => ___('Withdraw Your Token'),
                    'desc' => ___('You can withdraw your token any time, we will processed withing 24 hours after your withdraw request.'),
                    'view' => ___('See Withdraw History'),
                    'view_route' => ('withdraw:user.index'),
                    'view_class' => '',
                    'cta' => 'Withdraw',
                    'cta_route' => '',
                    'cta_class' => css_class('user-modal-request'.(($get_status == 1) ? ' disabled' : '')),
                    'cta_daction' => 'withdraw-token',
                    'cta_dtype' => 'modal',
                ],
                'settings' => [
                    'enable' => $is_enabled,
                    'minimum' => gws('withdraw_minimum', 1000),
                    'maximum' => gws('withdraw_maximum', 10000),
                    'price' => gws('withdraw_price', 'active'),
                    'custom_price' => gws('withdraw_custom_price', 0),
                    'currency' => json_decode(gws('withdraw_currency', json_encode(['eth' => 1, 'btc' => 1, 'ltc' => 1])), true),
                    'notes' => gws('withdraw_notes', 'You can withdraw the your funds and we will process soon.'),
                    'status' => $get_status,
                    'message' => gws('withdraw_status_message', 'Sorry! right now you can not withdraw token. Please contact us if you have any question.'),
                ],
                'view_setting' => 'withdraw::settings',
            ],
        ]);
    }
}
