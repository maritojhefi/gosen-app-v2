@extends('layouts.user')
@section('title', 'Withdraw History')
@php 
$user = auth()->user();
$tokenBalance = (!empty($user->tokenBalance)) ? $user->tokenBalance : 0;
$token_symbol = token_symbol();
@endphp

@section('content')
<div class="card content-area content-area-mh">
    <div class="card-innr">
        <div class="card-head has-aside">
            <h4 class="card-title">{{___('Withdraw History')}}</h4>
            <div class="card-opt">
                {{-- <ul class="btn-grp btn-grp-block guttar-20px">
                    <li><a href="javascript:void(0)" data-toggle="modal" class="btn btn-auto btn-sm btn-primary user-modal-request" data-action="withdraw-token" data-type="modal"><em class="ti ti-share"></em><span>Withdraw</span></a></li>
                </ul> --}}
            </div>
        </div>
        <div class="gaps-1x"></div>
        <div class="row">
            <div class="col-lg-4">
                
                <div class="token-statistics card card-token card-full-height">
                    <div class="card-innr">
                        <div class="token-balance token-balance-with-icon mb-0" style="align-items: flex-start;">
                            <div class="token-balance-icon bg-transparent">
                                <img loading="lazy" src="{{asset('images/token-symbol-light.png')}}" alt="">
                            </div>
                            <div class="token-balance-text" style="width:100%;">
                                <h6 class="card-sub-title">Gosen Token</h6>
                                <span class="lead">{{($user->tokenBalance > 0 ? $user->tokenBalance : '0')}}<span> {{$token_symbol}}</span></span>
                                <div class="token-balance-sub" style="font-size: 11px;">
                                    <span class="sub">{{token_price($user->tokenBalance, base_currency())}} USD </span>
                                </div>
                            </div>
                        </div>
                        <div class="token-balance token-balance-s2 mb-0">
                            <ul class="token-balance-list pl-0">
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">TIER 1</h6>
                                    <span class="lead">{{$tokensStage->where('stage',1)->where('tnx_type','purchase')->where('status','approved')->sum('total_tokens')}}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">TIER 2</h6>
                                    <span class="lead">{{$tokensStage->where('stage',2)->where('tnx_type','purchase')->where('status','approved')->sum('total_tokens')}}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">TIER 3</h6>
                                    <span class="lead">{{$tokensStage->where('stage',3)->where('tnx_type','purchase')->where('status','approved')->sum('total_tokens')}}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                            </ul>
                        </div>
                        @if (showWithdrawCard('switch_gosen_token'))
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="javascript:void(0)" data-toggle="modal" class="btn btn-success btn-sm user-modal-request" data-action="withdraw-token" data-type="modal"><em class="ti ti-share"></em><span>{{___('Withdraw')}} {{$token_symbol}}</span></a>
                        </div>
                        @else
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="#" class="btn btn-secondary btn-sm" disabled><em class="ti ti-share"></em><span>{{___('Disabled')}}</span></a>
                        </div>
                        @endif
                    </div>
                </div>
                
            </div>
            <div class="col-lg-4 col-md-6">
                
                <div class="card card-token card-full-height" style="background-image: linear-gradient(45deg, #45474d 0%, #acb2be 100%); color: #fff;">
                    <div class="card-innr">
                        <div class="token-balance token-balance-with-icon mb-0" style="align-items: flex-start;">
                            <div class="token-balance-icon bg-transparent">
                                <img loading="lazy" src="{{asset('images/token-symbol-light.png')}}" alt="">
                            </div>
                            <div class="token-balance-text" style="width:100%;">
                                <h6 class="card-sub-title">Bonus Token</h6>
                                <span class="lead">{{$user->gift_bonus}} <span>{{$token_symbol}}</span></span>
                                <div class="token-balance-sub" style="font-size: 11px;">
                                    <span class="sub">{{token_price($user->gift_bonus)}} USD </span>
                                </div>
                            </div>
                        </div>
                        <div class="token-balance token-balance-s2 mb-0">
                            <ul class="token-balance-list pl-0">
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">{{___('REFERRAL')}}</h6>
                                    <span class="lead">{{ $tokensStage->where('tnx_type','joined')->sum('total_tokens') > 0 ? $tokensStage->where('tnx_type','joined')->sum('total_tokens') : '0' }}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">{{___('GIFT')}}</h6>
                                    <span class="lead">{{ $tokensStage->where('tnx_type','gift')->sum('total_tokens') > 0 ? $tokensStage->where('tnx_type','gift')->sum('total_tokens'): '0' }}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                                <li class="token-balance-sub">
                                    <h6 class="card-sub-title">{{___('LAUNCH')}}</h6>
                                    <span class="lead">{{ $tokensStage->where('tnx_type','launch')->sum('total_tokens') > 0 ? $tokensStage->where('tnx_type','launch')->sum('total_tokens'): '0' }}</span>
                                    <span class="sub">{{$token_symbol}}</span>
                                </li>
                            </ul>
                        </div>
                        @if (showWithdrawCard('switch_bonus_token'))
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="javascript:void(0)" data-toggle="modal" class="btn btn-success btn-sm ajax-mod" data-type="modal">
                                <em class="ti ti-share"></em><span>{{___('Withdraw GIFT')}}</span>
                            </a>
                        </div>
                        @else
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="#" class="btn btn-secondary btn-sm" disabled><em class="ti ti-share"></em><span>{{___('Disabled')}}</span></a>
                        </div>
                        @endif
                    </div>
                </div>
                <input type="hidden" name="" id="url" value="{{ asset2() }}">
                @push('footer')
                    <script>
                        var ruta = $('#url').val();
                        $(".ajax-mod").click(function() {
                            $.ajax({
                                method: "post",
                                data:{
                                    type: 'modal',
                                    '_token': '{{ csrf_token() }}'
                                },
                                url: ruta+"user/withdraw/request-bonus",
                                success: function(result) {
                                    $('#ajax-modal').html(result.modal);
                                    $('#myModal').modal('show')
                                }
                            })
                        });
                
                    </script>
                @endpush
                
            </div>

            <div class="col-lg-4 col-md-6">
                
                <div class="token-statistics card card-token card-full-height" style="background-image: linear-gradient(rgb(0 0 0 / 40%), rgb(0 0 0 / 40%)), url('  {{ asset('/assets/images/billete2.png')}}   '); background-size: 120% 100%;">
                    <div class="card-innr">
                        <div class="token-balance token-balance-with-icon" style="margin-bottom: 10px;">
                            <div class="token-balance-icon bg-transparent">
                            <img loading="lazy" src="{{ asset('/assets/images/dolar.png')}}" style="width: 100px;" alt="">                      
                            </div>
                            <div class="token-balance-text" style="width:100%;">
                                <h6 class="card-sub-title">Bonus USD</h6>
                                <span class="lead">{{ $monto }} <span>USD</span></span>
                            </div>
                        </div>
                        <div class="token-balance token-balance-s2" style="margin-bottom: 8px;">
                            <h6 class="card-sub-title" style="color: #1dd69f; font-weight:bold; margin-bottom: 16px;">{{___('Your Bonuses In Gos')}}</h6>
                            <ul class="token-balance-list mb-0">
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead" >{{$bonus->count()}}
                                    </span>
                                    <span
                                        class="sub" style="color: white;">{{___('Transactions')}}
                                    </span>
                                </li>
                                <li class="token-balance-sub">
                                    <span style="align:center;" class="lead">{{ $bonus->groupBy('user_from')->count()}}         
                                    </span>
                                    <span class="sub" style="color: white;">{{___('Referrals')}}
                                    </span>
                                </li>
                            </ul>
                        </div>
                        @if (showWithdrawCard('switch_bonus_usd'))
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-small">
                                    <i class="ti ti-share"></i>
                                    {{___('Withdraw')}} USD
                                </a>
                        </div>
                        @else
                        <div class="token-balance token-balance-s2 text-right">
                            <a href="#" class="btn btn-secondary btn-sm" disabled><em class="ti ti-share"></em><span>{{___('Disabled')}}</span></a>
                        </div>
                        @endif
                    </div>
                </div>
                
            </div>
        </div>
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#withdrawTotalToken">{{ __('Withdraw Total Token') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#withdrawGosToken">{{ __('Withdraw GOS Token') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#withdrawBonusStart">{{ __('Withdraw Bonus Start') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#withdrawBonusUSD" style="color:#46d246">{{ __('Withdraw Bonus USD') }}</a>
                </li>
            </ul>{{-- .nav-tabs-line --}}
             <div class="tab-content mt-3">

                <div class="tab-pane fade show active" id="withdrawTotalToken">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" style ="font-size:20px;" data-toggle="tooltip" data-placement="left" data-original-title="Filters"></em> </a>
                                <div
                                    class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                    <div id="office">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Status') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="all"
                                                    name="ofc" type="radio" value="" checked>
                                                <label for="all">All
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="approved"
                                                    name="ofc" type="radio" value="approved">
                                                <label for="approved">Approved
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="pending"
                                                    name="ofc" type="radio" value="pending">
                                                <label for="pending">Pending
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="rejected"
                                                    name="ofc" type="radio" value="rejected">
                                                <label for="rejected">Rejected
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init" id="miTabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status dt-tnxno">{{ ___('Withdraw ID') }}</th>
                                <th class="data-col dt-token dt-xs">{{ ___('Tokens') }}</th>
                                <th class="data-col dt-amount">{{ ___('Amount') }}</th>
                                <th class="data-col dt-account dt-lg">{{ ___('Wallet Address') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Information') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Status') }}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $trnx)
                            <tr class="data-item wd-item-{{ $trnx->id }}">
                                <td class="data-col dt-tnxno" >
                                    <div class="d-flex align-items-center">
                                        <div data-toggle="tooltip" title="{{ (!empty($trnx->extra) && $trnx->status == 'rejected' ? $trnx->extra : '') }}" id="ds-{{ $trnx->id }}" class="data-state data-state-{{ str_replace(['progress','canceled'], ['pending','canceled'], __status($trnx->status, 'icon')) }}">
                                            <span class="d-none">{{ ($trnx->status=='onhold') ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                        </div>
                                        <div class="fake-class">
                                            <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                            <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col dt-token dt-xs">
                                    @php
                                        $color = '';
                                        $signo = '';
                                        if ($trnx->status== 'approved') {
                                            $color = 'red';
                                            $signo = '-';
                                        } else {
                                            $color = 'black';
                                            $signo = '';
                                        };
                                    @endphp
                                        <span class="lead token-amount" style="color:{{ $color }}">{{ $signo }} {{ $trnx->total_tokens }}</span>
                                        {{-- <span class="sub sub-symbol">{{ token_symbol() }}</span>
                                    <span class="lead token-amountger">{{ $trnx->total_tokens }}</span> --}}
                                    <span class="sub sub-symbol">{{ $token_symbol }}</span>
                                </td>
                                <td class="data-col dt-amount">
                                    <span class="lead token-amount">{{ $trnx->amount }}</span>
                                    <span class="sub sub-symbol ucap">{{ $trnx->currency }}</span>
                                </td>
                                <td class="data-col data-account dt-lg">
                                    <span class="lead user-info">{{ $trnx->wallet_address }}</span>
                                    <span class="sub sub-symbol ucap">{{ short_to_full($trnx->currency) }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    @if($trnx->status=='rejected'||$trnx->status=='canceled')
                                    <span class="lead tnx-note text-wrap"><span class="small">{{ $trnx->extra }}</span></span>
                                    @elseif(!empty($trnx->payment_id))
                                    <span class="lead tnx-hash text-wrap"><span>{{ $trnx->payment_id }}</span></span>
                                    @endif
                                    <span class="sub sub-id">{{ ( $trnx->checked_time ? _date($trnx->checked_time) : '~') }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span>{{ __($trnx->status) }}</span>
                                </td>
                                <td class="data-col ">
                                    <div class="relative d-inline-block">
                                        <a href="javascript:void(0)" class="btn btn-light-alt btn-xs btn-icon {{ $trnx->status == 'approved' ? '__view' : 'toggle-tigger' }}" data-id="{{ $trnx->id }}"><em class="ti {{ $trnx->status == 'approved' ? 'ti-eye' : 'ti-more-alt' }}" style="color:#46d246; font-size:20px;"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list">
                                                <li><a data-id="{{ $trnx->id }}" href="javascript:void(0)" class="__view"><em class="fa fa-eye"></em> Details</a></li>
                                                @if($trnx->status == 'pending')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="cancel" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-times-circle"></em> Cancel</a></li>
                                                @elseif($trnx->status == 'canceled')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="delete" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-trash"></em> Delete</a></li>
                                                @else
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>{{-- .data-item --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>{{-- .tab-pane --}}

                <div class="tab-pane fade" id="withdrawGosToken">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" style ="font-size:20px;" data-toggle="tooltip" data-placement="left" data-original-title="Filters"></em> </a>
                                <div
                                    class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                    <div id="officegos">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Status') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="allgos"
                                                    name="ofcgos" type="radio" value="" checked>
                                                <label for="allgos">All
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="approvedgos"
                                                    name="ofcgos" type="radio" value="approved">
                                                <label for="approvedgos">Approved
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="pendinggos"
                                                    name="ofcgos" type="radio" value="pending">
                                                <label for="pendinggos">Pending
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="rejectedgos"
                                                    name="ofcgos" type="radio" value="rejected">
                                                <label for="rejectedgos">Rejected
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init" id="gosTabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status dt-tnxno">{{ ___('Withdraw ID') }}</th>
                                <th class="data-col dt-token dt-xs">{{ ___('Tokens') }}</th>
                                <th class="data-col dt-amount">{{ ___('Amount') }}</th>
                                <th class="data-col dt-account dt-lg">{{ ___('Wallet Address') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Information') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Status') }}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $trnx)
                            @if ($trnx->details == 'Tokens Withdraw')
                            <tr class="data-item wd-item-{{ $trnx->id }}">
                                <td class="data-col dt-tnxno" >
                                    <div class="d-flex align-items-center">
                                        <div data-toggle="tooltip" title="{{ (!empty($trnx->extra) && $trnx->status == 'rejected' ? $trnx->extra : '') }}" id="ds-{{ $trnx->id }}" class="data-state data-state-{{ str_replace(['progress','canceled'], ['pending','canceled'], __status($trnx->status, 'icon')) }}">
                                            <span class="d-none">{{ ($trnx->status=='onhold') ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                        </div>
                                        <div class="fake-class">
                                            <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                            <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col dt-token dt-xs">
                                    @php
                                    $color = '';
                                    $signo = '';
                                    if ($trnx->status== 'approved') {
                                        $color = 'red';
                                        $signo = '-';
                                    } else {
                                        $color = 'black';
                                        $signo = '';
                                    };
                                @endphp
                                    <span class="lead token-amount" style="color:{{ $color }}">{{ $signo }} {{ $trnx->total_tokens }}</span>
                                    {{-- <span class="lead token-amountger">{{ $trnx->total_tokens }}</span> --}}
                                    <span class="sub sub-symbol">{{ $token_symbol }}</span>
                                </td>
                                <td class="data-col dt-amount">
                                    <span class="lead token-amount">{{ $trnx->amount }}</span>
                                    <span class="sub sub-symbol ucap">{{ $trnx->currency }}</span>
                                </td>
                                <td class="data-col data-account dt-lg">
                                    <span class="lead user-info">{{ $trnx->wallet_address }}</span>
                                    <span class="sub sub-symbol ucap">{{ short_to_full($trnx->currency) }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    @if($trnx->status=='rejected'||$trnx->status=='canceled')
                                    <span class="lead tnx-note text-wrap"><span class="small">{{ $trnx->extra }}</span></span>
                                    @elseif(!empty($trnx->payment_id))
                                    <span class="lead tnx-hash text-wrap"><span>{{ $trnx->payment_id }}</span></span>
                                    @endif
                                    <span class="sub sub-id">{{ ( $trnx->checked_time ? _date($trnx->checked_time) : '~') }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span>{{ __($trnx->status) }}</span>
                                </td>
                                <td class="data-col ">
                                    <div class="relative d-inline-block">
                                        <a href="javascript:void(0)" class="btn btn-light-alt btn-xs btn-icon {{ $trnx->status == 'approved' ? '__view' : 'toggle-tigger' }}" data-id="{{ $trnx->id }}"><em class="ti {{ $trnx->status == 'approved' ? 'ti-eye' : 'ti-more-alt' }}" style="color:#46d246; font-size:20px;"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list">
                                                <li><a data-id="{{ $trnx->id }}" href="javascript:void(0)" class="__view"><em class="fa fa-eye"></em> Details</a></li>
                                                @if($trnx->status == 'pending')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="cancel" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-times-circle"></em> Cancel</a></li>
                                                @elseif($trnx->status == 'canceled')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="delete" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-trash"></em> Delete</a></li>
                                                @else
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>{{-- .data-item --}}
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>{{-- .tab-pane --}}

                <div class="tab-pane fade" id="withdrawBonusStart">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" style ="font-size:20px;" data-toggle="tooltip" data-placement="left" data-original-title="Filters"></em> </a>
                                <div
                                    class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                    <div id="officebonus">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Status') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="allbonus"
                                                    name="ofcbonus" type="radio" value="" checked>
                                                <label for="allbonus">All
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="approvedbonus"
                                                    name="ofcbonus" type="radio" value="approved">
                                                <label for="approvedbonus">Approved
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="pendingbonus"
                                                    name="ofcbonus" type="radio" value="pending">
                                                <label for="pendingbonus">Pending
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="rejectedbonus"
                                                    name="ofcbonus" type="radio" value="rejected">
                                                <label for="rejectedbonus">Rejected
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init" id="bonusTabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status dt-tnxno">{{ ___('Withdraw ID') }}</th>
                                <th class="data-col dt-token dt-xs">{{ ___('Tokens') }}</th>
                                <th class="data-col dt-amount">{{ ___('Amount') }}</th>
                                <th class="data-col dt-account dt-lg">{{ ___('Wallet Address') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Information') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Status') }}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $trnx)
                            @if ($trnx->details == 'Tokens Withdraw Bonus')
                            <tr class="data-item wd-item-{{ $trnx->id }}">
                                <td class="data-col dt-tnxno" >
                                    <div class="d-flex align-items-center">
                                        <div data-toggle="tooltip" title="{{ (!empty($trnx->extra) && $trnx->status == 'rejected' ? $trnx->extra : '') }}" id="ds-{{ $trnx->id }}" class="data-state data-state-{{ str_replace(['progress','canceled'], ['pending','canceled'], __status($trnx->status, 'icon')) }}">
                                            <span class="d-none">{{ ($trnx->status=='onhold') ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                        </div>
                                        <div class="fake-class">
                                            <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                            <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col dt-token dt-xs">
                                    @php
                                        $color = '';
                                        $signo = '';
                                        if ($trnx->status== 'approved') {
                                            $color = 'red';
                                            $signo = '-';
                                        } else {
                                            $color = 'black';
                                            $signo = '';
                                        };
                                    @endphp
                                    <span class="lead token-amount" style="color:{{ $color }}">{{ $signo }} {{ $trnx->total_tokens }}</span>
                                    {{-- <span class="lead token-amountger">{{ $trnx->total_tokens }}</span> --}}
                                    <span class="sub sub-symbol">{{ $token_symbol }}</span>
                                </td>
                                <td class="data-col dt-amount">
                                    <span class="lead token-amount">{{ $trnx->amount }}</span>
                                    <span class="sub sub-symbol ucap">{{ $trnx->currency }}</span>
                                </td>
                                <td class="data-col data-account dt-lg">
                                    <span class="lead user-info">{{ $trnx->wallet_address }}</span>
                                    <span class="sub sub-symbol ucap">{{ short_to_full($trnx->currency) }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    @if($trnx->status=='rejected'||$trnx->status=='canceled')
                                    <span class="lead tnx-note text-wrap"><span class="small">{{ $trnx->extra }}</span></span>
                                    @elseif(!empty($trnx->payment_id))
                                    <span class="lead tnx-hash text-wrap"><span>{{ $trnx->payment_id }}</span></span>
                                    @endif
                                    <span class="sub sub-id">{{ ( $trnx->checked_time ? _date($trnx->checked_time) : '~') }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span>{{ __($trnx->status) }}</span>
                                </td>
                                <td class="data-col ">
                                    <div class="relative d-inline-block">
                                        <a href="javascript:void(0)" class="btn btn-light-alt btn-xs btn-icon {{ $trnx->status == 'approved' ? '__view' : 'toggle-tigger' }}" data-id="{{ $trnx->id }}"><em class="ti {{ $trnx->status == 'approved' ? 'ti-eye' : 'ti-more-alt' }}" style="color:#46d246; font-size:20px;"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list">
                                                <li><a data-id="{{ $trnx->id }}" href="javascript:void(0)" class="__view"><em class="fa fa-eye"></em> Details</a></li>
                                                @if($trnx->status == 'pending')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="cancel" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-times-circle"></em> Cancel</a></li>
                                                @elseif($trnx->status == 'canceled')
                                                <li> <a data-id="{{ $trnx->id }}" data-action="delete" href="javascript:void(0)" class="user-withdraw-action"><em class="fa fa-trash"></em> Delete</a></li>
                                                @else
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>{{-- .data-item --}}
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>{{-- .tab-pane --}}

                <div class="tab-pane fade" id="withdrawBonusUSD">
                <div class="row">
                        <div class="col-md-12">
                            <div class="float-right position-relative">
                                <a href="#" class="btn btn-light-alt btn-xs dt-filter-text btn-icon toggle-tigger">
                                    <em class="fa fa-sliders" style ="font-size:20px;" data-toggle="tooltip" data-placement="left" data-original-title="Filters"></em> </a>
                                <div
                                    class="toggle-class toggle-datatable-filter dropdown-content dropdown-dt-filter-text dropdown-content-top-left text-left">
                                    <div id="officeusd">
                                        <ul class="dropdown-list dropdown-list-s2">
                                            <li>
                                                <h6 class="dropdown-title">{{ ___('Status') }}</h6>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="all2"
                                                    name="ofcusd" type="radio" value="" checked>
                                                <label for="all2">All
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="approved2"
                                                    name="ofcusd" type="radio" value="approved">
                                                <label for="approved2">Approved
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="pending2"
                                                    name="ofcusd" type="radio" value="pending">
                                                <label for="pending2">Pending
                                                </label>
                                            </li>
                                            <li>
                                                <input class="input-checkbox input-checkbox-sm " id="canceled2"
                                                    name="ofcusd" type="radio" value="canceled">
                                                <label for="canceled2">Canceled
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="data-table dt-filter-init" id="usdTabla">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status dt-tnxno">{{ ___('Bonus Withdraw ID') }}</th>
                                <th class="data-col dt-amount">{{ ___('Amount') }}</th>
                                <th class="data-col dt-account dt-lg">{{ ___('Wallet Address') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Information') }}</th>
                                <th class="data-col dt-info dt-xl">{{ ___('Status') }}</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bonusDolar as $trnx)
                            <tr class="data-item wd-item-{{ $trnx->id }}">
                                <td class="data-col dt-tnxno" >
                                    <div class="d-flex align-items-center">
                                        <div data-toggle="tooltip" title="{{  $trnx->status }}" id="ds-{{ $trnx->id }}" class="data-state data-state-{{ str_replace(['progress','canceled'], ['pending','canceled'], __status($trnx->status, 'icon')) }}">
                                            <span class="d-none">{{ ($trnx->status=='onhold') ? ucfirst('pending') : ucfirst($trnx->status) }}</span>
                                        </div>
                                        <div class="fake-class">
                                            <span class="lead tnx-id">{{ $trnx->code }}</span>
                                            <span class="sub sub-date">{{ _date($trnx->update_at) }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="data-col dt-amount">
                                    @php
                                        $color = '';
                                        $signo = '';
                                        if ($trnx->status== 'approved') {
                                            $color = 'red';
                                            $signo = '-';
                                        } else {
                                            $color = 'black';
                                            $signo = '';
                                        };
                                    @endphp
                                    <span class="lead token-amount" style="color:{{ $color }}">{{ $signo }} {{ $trnx->amount }}</span>
                                    {{-- <span class="lead token-amountger">{{ $trnx->amount }}</span> --}}
                                    <span class="sub sub-symbol">(USDT)</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span class="lead token-amount">{{ $trnx->wallet }}</span>
                                    <span class="sub sub-symbol ucap">{{ $trnx->currency }}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span class="lead user-info">{{ date('jS F, Y / h:i:s', strtotime($trnx->created_at)) }}</span>
                                    <span class="sub sub-date"> {!! fechaOrden(\Carbon\Carbon::parse($trnx->created_at)) !!}</span>
                                </td>
                                <td class="data-col dt-info dt-xl">
                                    <span>{{ __($trnx->status) }}</span>
                                </td>
                                @if ($trnx->status == 'approved')
                                            <td class="data-col text-right">
                                                <a href="#" data-toggle="modal" data-target="#modal-medium2{{ $trnx->id }}"> <em class="ti ti-eye" style="color:#46d246; font-size:20px;"></em></a>
                                            </td>
                                        @elseif($trnx->status == 'canceled')
                                            <td class="data-col text-right">
                                                <div class="relative d-inline-block">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt" style="color:#46d246; font-size:20px;"></em></a>
                                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                        <ul id="more-menu-102" class="dropdown-list">
                                                            <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-medium2{{ $trnx->id }}">
                                                                    <em class="ti ti-eye"></em>
                                                                    {{ __('View Details') }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        @else
                                            <td class="data-col text-right">
                                                <div class="relative d-inline-block">
                                                    <a href="#"
                                                        class="btn btn-light-alt btn-xs btn-icon toggle-tigger"><em
                                                            class="ti ti-more-alt" style="color:#46d246; font-size:20px;"></em></a>
                                                    <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                        <ul id="more-menu-102" class="dropdown-list">
                                                            {{-- <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-small{{ $trnx->id }}">
                                                                    <em class="ti ti-check"></em> {{ __('Approved') }}</a>
                                                            </li> --}}
                                                            <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-smallo{{ $trnx->id }}">
                                                                    <em class="ti ti-close"></em> {{ __('Decline') }}</a>
                                                            </li>
                                                            <li><a href="#" data-toggle="modal"
                                                                    data-target="#modal-medium2{{ $trnx->id }}">
                                                                    <em class="ti ti-eye"></em>
                                                                    {{ __('View Details') }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                            </tr> {{-- data-item --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>{{-- .tab-pane --}}

            </div>
    </div>{{-- .card-innr --}}
</div>{{-- .card --}}
@endsection

@section('modals')
    {{-- @if ($tipo == 'withdraw') --}}
        @foreach ($bonusDolar as $trnx)
            <div class="modal fade" id="modal-medium2{{$trnx->id}}" tabindex="-1" style="padding-right: 17px;">
                <div class="modal-dialog modal-dialog-md modal-dialog-centered">
                    <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                            aria-label="Close"><em class="ti ti-close"></em></a>
                        <div class="popup-body">
                            <div class="content-area popup-content">
                                <div class="card-head d-flex justify-content-between align-items-center">
                                    <h4 class="card-title mb-0">{{ __('Bonus Dollars Transaction from') }}
                                        ({{ $trnx->code }})
                                    </h4>
                                </div>
                                <div class="trans-status">
                                    <span
                                        class="badge badge-{{ $trnx->status == 'canceled' ? 'danger' : 'warning' }} ucap">
                                        {{ __($trnx->status) }}
                                    </span>
                                </div>
                                <div class="p-2"></div>
                                <h6 class="card-sub-title">{{ __('Transaction Bonus Details') }}</h6>
                                <ul class="data-details-list">
                                    <li>
                                        <div class="data-details-head">ID {{ __('Bonus') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->code }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ __('Name o Username') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->userBy->name ? $trnx->userBy->name : $trnx->userBy->username }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ __('Email') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->userBy->email }}</span>
                                        </div>
                                    </li>
                                    {{-- <li>
                                        <div class="data-details-head">{{ __('Type User') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->userBy->type_user }}
                                                @if ($trnx->userBy->type_user == 1)
                                                    <small class="sub sub-date">
                                                        ( {{ __(ucfirst($typeuser1)) }} )
                                                    </small>
                                                @elseif($trnx->userBy->type_user == 2)
                                                    <small class="sub sub-date">
                                                        ( {{ __(ucfirst($typeuser2)) }} )
                                                    </small>
                                                @else
                                                    <small class="sub sub-date">
                                                        ( {{ __(ucfirst($typeuser3)) }} )
                                                    </small>
                                                @endif
                                            </span>
                                        </div>
                                    </li> --}}
                                    <li>
                                        <div class="data-details-head">{{ __('Amount') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->amount }} (USDT)
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ __('Wallet of this withdrawal') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->wallet }}
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="data-details-head">{{ __('Date') }}</div>
                                        <div class="data-details-des">
                                            <span>{{ $trnx->created_at }}
                                            </span>
                                            <span>{!! fechaOrden(\Carbon\Carbon::parse($trnx->created_at)) !!} </span>
                                        </div>
                                    </li>
                                    @if ($trnx->status == 'canceled')
                                        <li>
                                            <div class="data-details-head">{{ __('Comment') }}</div>
                                            <div class="data-details-des">
                                                <span>{{ $trnx->comment }}</span>
                                            </div>
                                        </li>
                                    @endif
                            </div>
                        </div>
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        
        <div class="modal fade" id="modal-smallo{{ $trnx->id }}" tabindex="-1" style="display: none;"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
                <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                        aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="popup-body text-center">
                        <form class="" method="post" action="{{ route('user.withdraw.dollars.cancel') }}">
                            @csrf
                            <div class="swal-icon swal-icon--warning">
                                <span class="swal-icon--warning__body">
                                    <span class="swal-icon--warning__dot"></span>
                                </span>
                            </div>
                            <div class="col-12 swal-title" style="text-align:center;">
                                <strong>
                                    {{ __('Are you sure you want to decline this dollar withdrawal') }}?
                                </strong>
                            </div>
                            <input type="hidden" name="status" value="{{ $trnx->status }}">
                            <input type="hidden" name="id" value="{{ Crypt::encrypt($trnx->id) }}">
                            <input type="hidden" name="user_id" value="{{ Crypt::encrypt($trnx->user_id) }}">
                            
                            <div class="col mt-4">
                                <button type="button" data-dismiss="modal" class="btn"
                                    style="color:#fff; background: gray;">
                                    {{ __('Cancel') }}
                                </button>
                                <button type="submit" id="decline" class="btn btn-danger mr-4">
                                    {{ __('Decline') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
        @endforeach
        {{-- modal de nuevo withdraw --}}
    <div class="modal fade" id="modal-small" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal"
                    aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h3 class="popup-title">{{___('Withdraw Bonuses')}}</h3>

                    <div class="alert alert-xs alert-success">
                        <strong>Available Bonus Balance:
                            <span class="text-primary text-bold fs-12">{{ auth()->user()->dolarBonus }} </span>(USDT)
                        </strong>
                    </div>
                    <input type="hidden" name="amount" value="{{ auth()->user()->dolarBonus }}" id="monto">
                    <form action="{{ route('user.bonus.withdraw') }}" id="form1" method="POST"
                        class="validate-modern">
                        @csrf
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Withdraw Amount (USDT)</label>
                            <div class="input-wrap">

                                <input id="amo" autofocus class="input-bordered " name="amount" placeholder="0"
                                    type="text" maxlength="{{ strlen(auth()->user()->dolarBonus) }}" value="">

                                <span id="advertencia" style="color:rgb(0, 123, 255); font-size:10px;left:-15px;"
                                    class="col">
                                    <i class="fa fa-info" style="font-size:10px;"></i>
                                    &nbsp;The withdrawal amount must be less than or equal to your dollar balance
                                </span>


                            </div>
                            <span class="input-note">Minimum amount to withdraw: <strong class="text-head">10
                                    USD</strong></span>
                        </div>
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Wallet Address for Receiving</label>
                            <div class="input-wrap">
                                <input class="input-bordered form-control {{ auth()->user()->wallet_dollar_status==0 ? 'is-invalid':''}}" type="text" name="wallet" id="wallet"
                                    value="{{ auth()->user()->wallet_dollar}}" readonly required=""
                                    aria-required="true" placeholder="{{___('Remember to register a wallet before withdrawing')}}">
                                    @if(auth()->user()->wallet_dollar_status==0)
                                    <span class="badge badge-danger">This wallet is not verified, check your email</span>
                                @endif
                            </div>
                            <div class="col-3" style="top: 5px;
                            left: -15px;">
                                <span class="badge bagde-sm badge-primary text-white p-2">

                                    <a href="{{ route('user.account', 1) }}   " type="button" class="text-white">
                                        Change Wallet
                                    </a>
                                </span>
                            </div>
                            <span class="input-note">Please provide a valid Tether (USDT) wallet address to withdraw your
                                bonuses in dollars.</span>
                        </div>
                        <div class="row guttar-20px guttar-vr-20px">
                            <div class="col-sm-7">
                                <p class="ucap fs-13">
                                    <strong>You are going to withdraw:
                                        <br>
                                        <span id="amount" value=""
                                            class="text-primary fs-16 amount-withdraw-currency">
                                        </span>
                                    </strong>
                                </p>
                            </div>
                            <div class="col-sm-5">
                                <button type="button" id="BtnEnviar" data-id="{{ auth()->user()->wallet_dollar_status }}" data-dismiss="modal"
                                    class="btn btn-primary disabled">{{___('Withdraw')}}</button>
                                <em class="fas fa-info-circle ml-3" data-toggle="tooltip" data-placement="right"
                                    title=""
                                    data-original-title="{{___('You can withdraw the your funds and we will process soon')}}."></em>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    
@endsection
@include('user.includes.script-bonus-withdraw')

@push('footer')
<script type="text/javascript">
    var withdraw_user_action = "{{ route('withdraw:user.update') }}";
    var withdraw_user_details = "{{ route('withdraw:user.details') }}";
    (function($){      
        $(document).on('click', '.__view', function(){
            var $this = $(this);
            $.post(withdraw_user_details, {
                    id: $this.data('id'),
                    _token: csrf_token
                }).done(function(response){
                    $ajax_container = $('#ajax-modal').html(response);
                    $modal = $ajax_container.find('.modal');
                    if($modal.length > 0){
                        $modal.modal('show');
                    }
                });
        });

        $(document).on('click', '.user-withdraw-action', function(){
            var $this = $(this), _id = $this.data('id'), _action = $this.data('action');

            if(confirm('Are you sure?')){
                $.post(withdraw_user_action, {
                    id: _id, action: _action, _token: csrf_token
                }).done(response => {
                    console.log(response);
                    if( typeof(response.reload) != 'undefined'){
                        setTimeout(function(){
                            window.location.reload();
                        }, 1200);
                    }
                    $('#ds-'+_id).removeAttr('class').addClass('data-state data-state-'+response.status);
                    if(_action == 'delete'){
                        $('tr.wd-item-'+_id).fadeOut(400);
                    }
                    if(_action == 'cancel'){
                        $this.attr('data-action', 'delete');
                        $this.html('<em class="fa fa-trash"></em> Delete');
                    }
                    show_toast(response.msg, response.message);
                }).fail(function(xhr, status, error) {
                    show_toast('error','Something is wrong!\n'+error);
                    _log(xhr, status, error);
                });
            }
        });
    })(jQuery)
</script>
<script>
    $(document).ready(function() {
        var table = $('#miTabla').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofc"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(5).search(offices, true, false, false).draw(false);
        });
    });
    $(document).ready(function() {
        var table = $('#gosTabla').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofcgos"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(5).search(offices, true, false, false).draw(false);
        });
    });
    $(document).ready(function() {
        var table = $('#bonusTabla').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofcbonus"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(5).search(offices, true, false, false).draw(false);
        });
    });
    $(document).ready(function() {
        var table = $('#usdTabla').DataTable();
        $('input:radio').on('change', function() {
            //construir una cadena de filtro con una condición o(|)
            var offices = $('input:radio[name="ofcusd"]:checked').map(function() {
                return this.value;
            }).get().join('|');
            // ahora filtre en la columna 2, sin expresiones regulares, sin filtrado inteligente, sin distinción entre mayúsculas y minúsculas
            table.column(4).search(offices, true, false, false).draw(false);
        });
    });
</script>

@endpush