<div class="modal fade" id="transaction-details" tabindex="-1">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
        <div class="modal-content">
            @if($transaction)
            <a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            @endif
            <div class="popup-body">
                <div class="content-area popup-content">
                    {{-- Badge  --}}
                    <div class="card-head d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">{{ ___('Withdrawal'). ' #'.$transaction->tnx_id }}</h4>
                        <div class="trans-status">
                            @if($transaction->status == 'approved')
                            <span class="badge badge-success ucap">{{___('Approved')}}</span>
                            @elseif($transaction->status == 'pending')
                            <span class="badge badge-warning ucap">{{___('Pending')}}</span>
                            @elseif($transaction->status == 'onhold')
                            <span class="badge badge-info ucap">{{___('Progress')}}</span>
                            @else
                            <span class="badge badge-danger ucap">{{___('Rejected')}}</span>
                            @endif
                        </div>
                    </div>
                    <p class="text-primary mb-0"><strong>{{ ___('Withdraw Wallet') .' ('.short_to_full($transaction->currency).')' }}</strong> <br> <span class="text-head">{{ $transaction->wallet_address }}</span></p>
                    @if(!empty($transaction->payment_id))
                    <p class="text-primary mb-0 mt-2"><strong>{{___('Reference Hash')}}</strong> <br> <span class="text-head">{{ $transaction->payment_id }}</span></p>
                    @endif
                    {{-- Details  --}}
                    <div class="gaps-2-5x"></div>
                    <h6 class="card-sub-title">{{___('Withdraw Details')}}</h6>
                    <ul class="data-details-list">
                        @if(is_admin())
                        <li>
                            <div class="data-details-head">{{ ___('Request By') }}</div>
                            <div class="data-details-des">
                                <span>{{ ucfirst($transaction->tnxUser->name) }}</span>
                                <span>{{ $transaction->tnxUser->email }}</span>
                            </div>
                        </li>
                        @endif
                        <li>
                            <div class="data-details-head">{{ ___('Request At') }}</div>
                            <div class="data-details-des">
                                <span>{{ _date($transaction->created_at) }}
                            </div>
                        </li>
                        <li>
                            <div class="data-details-head">{{ ___('Tokens') }}</div>
                            <div class="data-details-des">
                                <span style="color:red;">-{{ to_num($transaction->tokens, 'zero', ',', false) }} {{ token_symbol() }}</span>
                            </div>
                        </li>
                        <li>
                            <div class="data-details-head">{{ ___('Amount') }}</div>
                            <div class="data-details-des">
                                <span style="color:red;">{{ to_num($transaction->amount, 'auto', ',', false) }} {{ strtoupper($transaction->currency) }}</span>
                            </div>
                        </li>
                        <li>
                            <div class="data-details-head">{{ ___('Details') }}</div>
                            <div class="data-details-des">
                                <span>{{ $transaction->details }}
                            </div>
                        </li>
                        <li>
                            <div class="data-details-head">{{ ___('Wallet') }}</div>
                            <div class="data-details-des">
                                <span>{{ $transaction->wallet_address }}
                            </div>
                        </li>
                        @if(!empty($transaction->extra))
                        <li>
                            <div class="data-details-head">{{ ___('Notes') }}</div>
                            <div class="data-details-des">
                                <span>{{ $transaction->extra }}
                            </div>
                        </li>
                        @endif
                    </ul>
                    @if($transaction->status == 'pending')
                        <p class="text-primary pt-3 fs-12">{{ ___('We have received your withdraw request and our team will proceed shortly. We will send you an email once we have completed your withdraw.') }}</p>
                    @endif
                    @if($transaction->checked_time != NUll)
                        <p class="text-primary pt-3 fs-12"><em>{!! ___('Withdraw transaction has been :status at :time.', ['time'=>_date($transaction->checked_time), 'status' => $transaction->status])  !!}</em></p>
                    @elseif($transaction->status == 'canceled')
                        <p class="text-danger pt-3 fs-12"><em>{{ ___('You have canceled the withdraw request.')}}</em></p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#transaction-details').on('hide.bs.modal', function() {
        console.log('hola')
        $('.detail').removeClass('disabled');
        $('.detail').html(
            `<em class="ti ti-eye"></em>`
        );
    });
</script>