@php 
$token_symbol = token_symbol();
@endphp
<style>
/* .wrapper {
  width: 90%;
  max-width: 600px;
  margin: 0 auto;
} */

circle {
  transition: stroke-dashoffset 350ms ease-out;
}

input {
  cursor: ew-resize;
}

input,
svg {
  display: block;
  margin: 0 auto;
}

svg text {
  font-weight: bold;
  font-size: 10px;
  fill: #454545;
}
</style>
<div class="modal fade" id="user-withdraws" tabindex="-1">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
        <div class="modal-content">
            @if($settings->status==0 && $balance > 0)
            <a href="#" class="modal-close" data-bs-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="popup-body popup-body-md">
                <h3 class="popup-title">{{ ___('Withdraw Token') }}</h3>
                <div class="popup-content">
                    <div class="alert alert-xs" style="background-color: #99b2bd"><strong>{{ ___('Available Token Balance:') }} <span class="text-primary fs-12">{{ to_num_token($balance).' '.$token_symbol }}</span></strong></div>
                    <form id="withdraw-request-form" class="validate-modern" action="{{ route('withdraw:user.request') }}" method="POST">
                        <div class="row">

                                <div class="col-sm-6">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">{{ ___('Withdraw Amount')}}: {{ $token_symbol }}</label>
                                        <div class="input-wrap">
                                            <input type="hidden" name="gos" value="modal-gos" required>
                                            <input class="input-bordered _amount"  autofocus type="number" name="amount" placeholder="0" required id="slider" value="{{ $settings->minimum }}" step="1" min="{{ $settings->minimum }}" max="{{ $balance }}">
                                        </div>
                                        <span class="input-note">{!! ___('Minimum Withdraw:').' <strong class="text-head">'.to_num_token($settings->minimum).' '.$token_symbol.'</strong>' !!}</span>
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <div class="wrapper">
                                        {{-- <input type="number" id="slider" min="0" value="0" max="{{$balance}}" step="1"/> --}}
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" width="100%" height="150">
                                        <circle stroke-width="10" stroke="#e3e3e3" fill="none" cx="40" cy="40" r="30" ></circle>
                                        <circle id="circle" stroke-width="10" stroke="#252b3b" fill="none" cx="40" cy="40" r="30" stroke-dasharray="0" stroke-dashoffset=""></circle>
                                        <text id="perc" text-anchor="middle" x="42" y="44">0%</text>
                                        </svg>
                                        </div>
                                </div>
                                {{-- <div class="col-sm-6">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">{{ ___('Choose Coin') }}</label>
                                        <div class="input-wrap">
                                            <select class="select select-block select-bordered _currency" name="currency">
                                                @foreach($currencies as $cur => $cur_name)
                                                @if(isset($settings->currency[$cur]) && $settings->currency[$cur]==1)
                                                <option value="{{ $cur }}">{{ strtoupper($cur) }} ({{ $cur_name }})</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="input-note">{{ ___('Withdraw will be on same.') }}</span>
                                    </div>
                                </div> --}}
                            
                            <div class="col-12">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{ ___('Wallet Address for Receiving') }}</label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" name="address" placeholder="{{ ___('Your wallet address') }}" required>
                                    </div>
                                    <span class="input-note">{{ ___('Provide a valid wallet address for withdraw your token.') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="sap"></div>
                        <div class="gaps gaps-2x"></div>
                        <div class="row guttar-20px guttar-vr-20px">
                            <div class="col-sm-7">
                                <p class="ucap fs-13"><strong>{{ ___('Your will Get:') }}<br><span class="text-primary fs-16 amount-withdraw-currency">0.0</span></strong></p>
                            </div>
                            <div class="col-sm-5">
                                @csrf
                                <input type="hidden" name="type" value="request">
                                <button type="submit" class="btn btn-primary">{{ ___('Withdraw') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
                @if($settings->notes)
                <div class="gaps gaps-3-5x"></div>
                <div class="note note-plane note-light note-md font-italic">
                    <em class="fas fa-info-circle"></em>
                    <p>{{ $settings->notes }}</p>
                </div>
                @endif
            </div>
            @else
            <div class="popup-body popup-body-lg">
                <div class="gaps gaps-0-5x"></div>
                <div class="alert alert-warning mb-0 text-center">
                    {{ ($balance==0) ? ___('You do not have enough token to withdraw.') : ($settings->message ?? ___('You can not withdraw right now.')) }}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    (function($){
        var $wd_form = $('#withdraw-request-form');
        if($wd_form.length > 0){ ajax_form_submit($wd_form, true); }

        var _amount = '._amount';
        var _currency = '._currency';
        var $_rate = $('.amount-withdraw-currency');
        var max_decimal = {{ gws('token_decimal_max', 4) }};
        $(document).on('keypress', _amount, function(){
            var $this = $(this),
                cur = $(_currency).val();
            var price = Number($this.val()) * get_price(cur);
            $_rate.text($.number(price, max_decimal) + ' ' + cur.toUpperCase());
        });
        $(document).on('change', _currency, function(){
            var $this = $(this),
                token = $(_amount).val(),
                cur = $this.val();
            var price = Number(token) * get_price(cur);
            $_rate.text($.number(price, max_decimal) + ' ' + cur.toUpperCase());
        });

        function get_price(currency){
            var prices = @json($prices);
            for(i in prices){
                if( i === currency.toLowerCase()){
                    return prices[i];
                }
            }
            return prices['base'];
        }
    })(jQuery)
</script>

<script id="rendered-js">


value = {{ $settings->minimum }};
var porcentaje = {{ $settings->minimum }};
var monto = {{$balance}};

slider = document.getElementById('slider');
circle = document.getElementById('circle');
rad = circle.getAttribute('r');
perc = document.getElementById('perc');

circle.setAttribute('stroke-dasharray', 2 * rad * Math.PI);
// circle.setAttribute('stroke-dashoffset', 2 * rad * Math.PI * ((100 - slider.value) / 100));
        porcentaje = (slider.value / monto) * 100;
        perc.innerHTML = Math.trunc(porcentaje) + '%';
        value = 2 * rad * Math.PI * ((100 - Math.trunc(porcentaje)) / 100);
        circle.style.strokeDashoffset = value + 'px';

slider.addEventListener('input', function () {
    if (slider.value <= monto) {
        if (slider.value >= {{ $settings->minimum }}){
            porcentaje = (slider.value / monto) * 100;
            perc.innerHTML = Math.trunc(porcentaje) + '%';
            value = 2 * rad * Math.PI * ((100 - Math.trunc(porcentaje)) / 100);
            circle.style.strokeDashoffset = value + 'px';
        } else {
            porcentaje = ({{ $settings->minimum }} / monto) * 100;
            perc.innerHTML = Math.trunc(porcentaje) + '%';
            value = 2 * rad * Math.PI * ((100 - Math.trunc(porcentaje)) / 100);
            circle.style.strokeDashoffset = value + 'px';
        }
    }else{
        porcentaje = (monto / monto) * 100;
        perc.innerHTML = Math.trunc(porcentaje) + '%';
        value = 2 * rad * Math.PI * ((100 - Math.trunc(porcentaje)) / 100);
        circle.style.strokeDashoffset = value + 'px';
    }
});

$("#user-withdraws").on("hidden.bs.modal", function () {
delete value;
delete slider;
console.clear();
});

</script>

<script>
    document.getElementById("slider").addEventListener("keyup", testpa);
    // document.getElementById("min").addEventListener("keyup", testpa);

    function testpa() {
        //es el campo input 
        var txtInput = document.querySelector('#slider');
        //es el span
        // var divCopia = document.getElementById('amount');
        //monto total que tiene el usuario
        var gosen = {{$balance}};
        var min = {{ $settings->minimum }};
        if (Number(txtInput.value) > gosen) {
            $('#slider').prop('value', gosen);
        } 
        // else if (Number(txtInput.value) <= min){
        //     $('#slider').prop('value', min);
        // }
    }
</script>