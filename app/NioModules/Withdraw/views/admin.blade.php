@extends('layouts.admin')
@section('title', 'Withdraw Transactions')
@push('header')
    <style type="text/css">
        .input-inline.input-wrap {
            display: inline-flex;
            flex-direction: row;
        }

        .input-inline.input-wrap input {
            align-items: center;
        }
    </style>
@endpush
@section('content')
    <div class="page-content">
        <div class="container">
            <div class="content-area card content-area-mh">
                <div class="card-innr">
                    <div class="card-head has-aside">
                        <h4 class="card-title">Withdraw Transactions</h4>
                        <div class="card-opt">
                            <ul class="btn-grp btn-grp-block guttar-20px">
                                <li><a href="#settings" data-toggle="modal" class="btn btn-auto btn-sm btn-primary"><em
                                            class="ti ti-settings"></em><span
                                            class="d-none d-sm-inline-block">Settings</span></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="gaps-1x"></div>
                    <table class="data-table dt-filter-init">
                        <thead>
                            <tr class="data-item data-head">
                                <th class="data-col tnx-status dt-tnxno">Withdraw ID</th>
                                <th class="data-col dt-token dt-xs">Tokens</th>
                                <th class="data-col dt-amount">Amount</th>
                                <th class="data-col dt-account dt-lg data-col-wd-sm">Wallet Address</th>
                                <th class="data-col request-user dt-mb">User</th>
                                <th class="data-col dt-info dt-xl">Information</th>
                                <th class="data-col"></th>
                            </tr>
                        </thead>
                        <tbody id="request-list">
                            @foreach ($transactions as $trnx)
                                <tr class="data-item request-{{ $trnx->id }}">
                                    <td class="data-col dt-tnxno">
                                        <div class="d-flex align-items-center">
                                            <div data-toggle="tooltip" title="{{ __status($trnx->status, 'text') }}"
                                                id="ds-{{ $trnx->id }}"
                                                class="data-state data-state-{{ __status($trnx->status, 'icon') }}">
                                                <span class="d-none">{{ ucfirst($trnx->status) }}</span>
                                            </div>
                                            <div class="fake-class">
                                                <span class="lead tnx-id">{{ $trnx->tnx_id }}</span>
                                                <span class="sub sub-date">{{ _date($trnx->tnx_time) }}</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="data-col dt-token dt-xs">
                                        <span class="lead token-amount text-danger">-{{ $trnx->total_tokens }}</span>
                                        <span class="sub sub-symbol">{{ token_symbol() }}</span>
                                    </td>
                                    <td class="data-col dt-amount">
                                        <span class="lead token-amount">{{ $trnx->amount }}</span>
                                        <span class="sub sub-symbol ucap">{{ $trnx->currency }}</span>
                                    </td>
                                    <td class="data-col data-account dt-lg data-col-wd-sm">
                                        <span
                                            class="lead user-info text-wrap"><span>{{ $trnx->wallet_address }}</span></span>
                                        <span class="sub sub-symbol ucap">{{ short_to_full($trnx->currency) }}</span>
                                    </td>
                                    <td class="data-col data-user dt-mb">
                                        <span class="lead user-name">{{ $trnx->tnxUser->name }}</span>
                                        <span class="sub sub-id">{{ $trnx->tnxUser->email }}</span>
                                    </td>
                                    <td class="data-col dt-info dt-xl">
                                        @if ($trnx->status == 'rejected' || $trnx->status == 'canceled')
                                            <span class="lead tnx-note text-wrap"><span
                                                    class="small">{{ $trnx->extra }}</span></span>
                                        @elseif(!empty($trnx->payment_id))
                                            <span
                                                class="lead tnx-hash text-wrap"><span>{{ $trnx->payment_id }}</span></span>
                                        @endif
                                        <span
                                            class="sub sub-id">{{ $trnx->checked_time ? _date($trnx->checked_time) : '~' }}</span>
                                    </td>
                                    <td class="data-col text-right">
                                        <div class="relative d-inline-block">
                                            <a href="javascript:void(0)"
                                                class="btn btn-light-alt btn-xs btn-icon {{ $trnx->status == 'approved' ? 'detail' : 'toggle-tigger' }}"
                                                data-id="{{ $trnx->id }}"><em
                                                    class="ti {{ $trnx->status == 'approved' ? 'ti-eye' : 'ti-more-alt' }}"></em></a>
                                            <div class="toggle-class dropdown-content dropdown-content-top-left">
                                                <ul class="dropdown-list">
                                                    <li><a data-id="{{ $trnx->id }}" href="javascript:void(0)"
                                                            class="__view"><em class="fa fa-eye"></em> Details</a></li>
                                                    @if ($trnx->status == 'pending')
                                                        <li><a data-id="{{ $trnx->id }}" data-action="approve"
                                                                href="javascript:void(0)" class="withdraw-action"><em
                                                                    class="fa fa-check-circle"></em> Approve</a></li>
                                                    @endif
                                                    @if ($trnx->status == 'pending')
                                                        <li> <a data-id="{{ $trnx->id }}" data-action="reject"
                                                                href="javascript:void(0)" class="withdraw-action"><em
                                                                    class="fa fa-times-circle"></em> Reject</a></li>
                                                    @endif
                                                    @if ($trnx->status == 'canceled' || $trnx->status == 'rejected')
                                                        <li> <a data-id="{{ $trnx->id }}" data-action="delete"
                                                                href="javascript:void(0)" class="withdraw-action"><em
                                                                    class="fa fa-trash"></em> Delete</a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>{{-- .card --}}
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="settings" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    @include('withdraw::settings')
                </div>
            </div>{{-- .modal-content --}}
        </div>{{-- .modal-dialog --}}
    </div>
@endsection

@push('footer')
    <script>
        $('.detail').click(function() {
            $(this).addClass('disabled');
            $(this).html(
                `<span class="spinner-border spinner-border-sm pt-2" role="status" aria-hidden="true"></span>`
            );
            var $this = $(this);
            $.post(withdraw_details, {
                id: $this.data('id'),
                _token: csrf_token
            }).done(function(response) {
                $ajax_container = $('#ajax-modal').html(response);
                $modal = $ajax_container.find('.modal');
                if ($modal.length > 0) {
                    $modal.modal('show');
                }
            });
            $('.modal-close').attr('data-dismiss', 'modal');
        });
    </script>
    <script type="text/javascript">
        var withdraw_url = "{{ route('withdraw:admin.update') }}";
        var withdraw_details = "{{ route('withdraw:admin.details') }}";
        (function($) {

            var $withdraw_form = $('#withdraw-form');
            if ($withdraw_form.length > 0) {
                ajax_form_submit($withdraw_form, false);
            }

            $(document).on('change', '._stage', function() {
                var $this = $(this);
                if ($this.val() == 'custom') {
                    $('._price').removeClass('d-none');
                } else {
                    $('._price').addClass('d-none');
                }
            });

            // $(document).on('click', '.__view', function(){
            //   var $this = $(this);
            //     $.post(withdraw_details, {
            //             id: $this.data('id'),
            //             _token: csrf_token
            //         }).done(function(response){
            //             $ajax_container = $('#ajax-modal').html(response);
            //             $modal = $ajax_container.find('.modal');
            //             if($modal.length > 0){
            //                 $modal.modal('show');
            //             }
            //         });
            //         $('.modal-close').attr('data-dismiss', 'modal');
            // });

            var withdraw_action = '.withdraw-action';
            $(document).on('click', withdraw_action, function(e) {
                e.preventDefault();
                var _id = $(this).data('id'),
                    _action = $(this).data('action'),
                    $_this = $(this);
                console.log(_id, _action);
                swal({
                    title: "Are you sure to " + _action + " this request.",
                    icon: (_action == "approve") ? 'info' : 'warning',
                    buttons: ['Cancel', 'Yes'],
                    dangerMode: true,
                    content: {
                        element: (_action != 'delete' ? "input" : 'span'),
                        attributes: {
                            placeholder: (_action == "approve" ? "Enter the transaction hash" :
                                "Write a note..."),
                            type: "text"
                        },
                    }
                }).then(function(data) {
                    console.log(data);
                    if (data != null || data == "") {
                        $.post(withdraw_url, {
                            id: _id,
                            action: _action,
                            extra: data,
                            _token: csrf_token
                        }).done(response => {
                            cl(response);
                            $('#ds-' + _id).removeAttr('class').addClass(
                                'data-state data-state-' + response.status)

                            if (_action == 'delete') {
                                $('tr.request-' + _id).hide(400);
                            }
                            if (_action == 'reject') {
                                $_this.attr('data-action', 'delete').html(
                                    '<em class="fa fa-trash"></em> Delete</a>')
                            }
                            show_toast(response.msg, response.message);
                        });
                    }
                });

            });
        })(jQuery)
    </script>
@endpush
