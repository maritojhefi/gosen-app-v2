<h3 class="title">Withdraw Settings</h3>
<p class="wide-max-lg">You can manage your withdraw system from here. You can sepecify minimum and maximum token number for each withdraw request and also disable new request.</p>
<div class="gaps gaps-1x"></div>
<form id="withdraw-form" class="validate-modern" action="{{ route('withdraw:admin.update.setting') }}" method="POST">
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="input-item input-with-label">
                <label class="input-item-label">Withdraw System</label>
                <div class="input-wrap input-wrap-switch">
                    <input class="input-switch switch-toggle" data-switch="switch-to-withdraw" name="withdraw_enable" type="checkbox" {{ get_setting('withdraw_enable')==1 ? 'checked ' : '' }}id="withdraw-system-enable">
                    <label for="withdraw-system-enable"><span>Disable</span><span class="over">Enable</span></label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="switch-content switch-to-withdraw">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Minimum Token</label>
                            <div class="input-wrap">
                                <input class="input-bordered" type="number" name="withdraw_minimum" min="1" placeholder="Minimum" value="{{ $settings->minimum ?? 0 }}" required>
                            </div>
                            <span class="input-note">Set minimum limit of withdrawal per request.</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Maximum Token</label>
                            <div class="input-wrap">
                                <input class="input-bordered" type="number" name="withdraw_maximum" min="1" placeholder="Maximum" value="{{ $settings->maximum ?? 0 }}" required>
                            </div>
                            <span class="input-note">Set maximum limit of withdrawal per request.</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Withdraw Token Price From</label><br>
                            <div class="input-wrap input-inline">
                                {{-- @dd($settings) --}}
                                <select name="withdraw_price" class="_stage select-bordered wide-18">
                                    <option {{ $settings->price == 'active' ? 'selected' : '' }} value="active">Active Stage</option>
                                    <option {{ $settings->price == 'custom' ? 'selected' : '' }} value="custom">Manual Price</option>
                                    @foreach($stages as $stage)
                                    <option {{ $settings->price == $stage->id ? 'selected' : '' }} value="{{ $stage->id }}">{{ $stage->name }}</option>
                                    @endforeach
                                </select>
                                <input class="{{ $settings->price == 'custom' ? '' : 'd-none' }} input-bordered ml-3 _price wide-12" type="number" name="withdraw_custom_price" placeholder="Token Price" value="{{ $settings->custom_price }}" required>
                            </div>
                            <span class="input-note">Set token price based on base currency({{ base_currency(true) }})</span>
                        </div>
                    </div>
                    <div class="col-12">
                        <label class="input-item-label">Withdraw With</label>
                        <ul class="d-flex flex-wrap checkbox-list list-col3x">
                            @foreach($currencies as $cur => $cur_name)
                            <li>
                                <div class="input-item text-left">
                                    <div class="input-wrap">
                                        <input class="input-checkbox input-checkbox-sm" name="withdraw_currency[{{ $cur }}]" id="wd-{{ $cur }}" {{ (isset($settings->currency[$cur]) && $settings->currency[$cur] == 1)  ? 'checked ' : ' '}} type="checkbox">
                                        <label for="wd-{{ $cur }}">{{ $cur_name .' ('.strtoupper($cur).')'}}</label>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="gaps-1-5x"></div>
                    </div>
                    <div class="col-12">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Withdraw Notes</label>
                            <div class="input-wrap">
                                <input class="input-bordered" name="withdraw_notes" placeholder="Withdraw footer notes" value="{{ $settings->notes ?? '' }}">
                            </div>
                            <span class="input-note">Add a footer note on withdraw request dialog box.</span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-item">
                            <label class="input-item-label">Disable Withdraw Request</label>
                            <div class="input-wrap">
                                <input type="checkbox" class="input-switch switch-toggle" data-switch="switch-to-msg" name="withdraw_status" id="withdraw_status" {{ $settings->status ? 'checked' : '' }}>
                                <label for="withdraw_status"><span>No</span> <span class="over">Yes</span></label>
                            </div>
                            <span class="input-note">If you want to temporarily stop new withdrawal request from users.</span>
                        </div>
                        <div class="switch-content switch-to-msg">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">Message to Users</label>
                                <div class="input-wrap">
                                    <textarea class="input-bordered" name="withdraw_status_message" placeholder="Enter your note">{{ $settings->message }}</textarea>
                                </div>
                                <span class="input-note">The message will display if you disable withdraw request from user.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gaps-1x"></div>
    @csrf
    <input type="hidden" name="action" value="settings">
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@push('footer')
<script type="text/javascript">
    (function($){
        var $withdraw_form = $('#withdraw-form');
        if($withdraw_form.length > 0){
            ajax_form_submit($withdraw_form, false);
        }
    })(jQuery)
</script>
@endpush