<?php

namespace App\NioModules\Withdraw;

/**
 * Withdraw Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Withdraw Module
 *
 * @author Softnio
 *
 * @version 1.0.1
 */

use App\Models\EmailTemplate;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

class WithdrawModule
{
    /**
     * Support Currency in Withdraw
     *
     * @var array
     *
     * @version 1.0.1
     *
     * @since 1.0
     */
    const Currency = [
        'eth' => 'Ethereum',
        'btc' => 'Bitcoin',
        'ltc' => 'Litecoin',
        'xrp' => 'Ripple',
        'xlm' => 'Stellar',
        'bch' => 'Bitcoin Cash',
        'bnb' => 'Binance Coin',
        'usdt' => 'Tether',
        'trx' => 'TRON',
        'usdc' => 'USD Coin',
        'dash' => 'Dash',
        'waves' => 'Waves',
        'xmr' => 'Monero',
    ];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->email_demo();
        $this->add_settings();
    }

    /**
     * Currency Symbol
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function support_currency($output = null)
    {
        $get_currency = self::Currency;
        $all_currency_sym = array_keys($get_currency);
        $currencies = array_map('strtolower', $all_currency_sym);

        if ($output == 'all') {
            return $get_currency;
        }

        return $currencies;
    }

    /**
     * Emailing
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function email($name)
    {
        $emails = [
            'user' => 'withdraw-user',
            'user-success' => 'withdraw-action-success',
            'user-reject' => 'withdraw-action-reject',
            'admin' => 'withdraw-admin',
        ];

        return isset($emails[$name]) ? $emails[$name] : 'default';
    }

    /**
     * Template
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function template($name = 'default')
    {
        $template = EmailTemplate::get_template($name);
        if (isset($template->message) && empty($template->message)) {
            $template->message = '[[withdraw_details]]';
        }

        return $template;
    }

    /**
     * Notify to Admin
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function notifyAdmins($tnx)
    {
        $admins = User::join('user_metas', 'users.id', '=', 'user_metas.userId')->where(['users.role' => 'admin', 'users.status' => 'active', 'user_metas.notify_admin' => 1])->select('users.*')->get();
        $to_all = get_setting('send_notification_to', 'all');
        $admin = (is_numeric($to_all) ? User::find($to_all) : null);
        if ($to_all == 'all') {
            $when = now()->addMinutes(2);
            try {
                Notification::send($admins, new WithdrawNotification($tnx, $this->email('admin')));
            } catch (\Exception $e) {
                //info($e->getMessage());
            }
        } elseif ($admin) {
            $when = now()->addMinutes(2);
            try {
                $admin->notify((new WithdrawNotification($tnx, $this->email('admin'))));
            } catch (\Exception $e) {
                //info($e->getMessage());
            }
        }
    }

    /**
     * Email Template
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function email_demo()
    {
        $data = [
            $this->email('user') => [
                'name' => 'Withdraw - New Request (USER)',
                'slug' => $this->email('user'),
                'subject' => 'Your Withdraw Request Has Been Received',
                'greeting' => 'Hello [[user_name]],',
                'message' => "We received your request to withdraw tokens to your wallet from [[site_name]]. We will review your request shortly and should be processed with 24-72 hours. You will be notified by email when we have completed your withdraw.\n\n[[withdraw_details]]\n\nNote: If you did not initiate this request, please cancel the request immediately from withdraw page after login into your account.\nAlso you can cancel the withdraw at any time before its authorized by our team.\n\nFor security purpose, withdraw will process after 2 hours from requested time.\n\nIf you have any questions, please feel free to contact us.\n",
                'regards' => 'true',
            ],
            $this->email('user-success') => [
                'name' => 'Withdraw - Approved Request (USER)',
                'slug' => $this->email('user-success'),
                'subject' => 'Your Token Withdraw Request Has Been Completed',
                'greeting' => 'Hello [[user_name]],',
                'message' => "Congratulations!\nWe have successfully transfered [[total_tokens]] into your wallet address as below -\n[[wallet_address]] \n\nWithdraw Reference ID -\n[[payment_hash]].\n\nIf you have not received token into your wallet yet, please feel free to contact us.\n",
                'regards' => 'true',
            ],
            $this->email('user-reject') => [
                'name' => 'Withdraw - Rejected Request (USER)',
                'slug' => $this->email('user-reject'),
                'subject' => 'Your Withdraw Request Has Been Rejected',
                'greeting' => 'Hello [[user_name]],',
                'message' => "We have received your request to withdraw tokens to your wallet. Unfortunately, your withdraw request has been rejected for following reason -\n[[withdrawal_note]]\n\nNote: Your requested amount added into your account.\n\nIf you have any questions, please feel free to contact us.",
                'regards' => 'true',
            ],
            $this->email('admin') => [
                'name' => 'Withdraw - Notification (ADMIN)',
                'slug' => $this->email('admin'),
                'subject' => 'Withdraw request from [[user_name]]',
                'greeting' => 'Hello [[user_name]],',
                'message' => "A user requested to withdraw token. Please review the withdraw request as soon as possible.\n[[withdraw_details]]\n\nPlease login into account and take necessary steps for withdraw.\n\n\nPS. Do not reply to this email.\nThank you.\n",
                'regards' => 'false',
            ],
        ];

        foreach ($data as $key => $value) {
            $check = EmailTemplate::where('slug', $key)->count();
            if ($check <= 0) {
                EmailTemplate::create($value);
            }
        }
    }

    /**
     * Add Defauts Settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function add_settings()
    {
        if (get_setting('withdraw_enable', null) == null) {
            add_setting('withdraw_enable', 0);
        }
        if (get_setting('withdraw_minimum', null) == null) {
            add_setting('withdraw_minimum', 1000);
        }
        if (get_setting('withdraw_maximum', null) == null) {
            add_setting('withdraw_maximum', 10000);
        }
        if (get_setting('withdraw_price', null) == null) {
            add_setting('withdraw_price', 'active');
        }
        if (get_setting('withdraw_custom_price', null) == null) {
            add_setting('withdraw_custom_price', 0);
        }
        if (get_setting('withdraw_status', null) == null) {
            add_setting('withdraw_status', 0);
        }
        if (get_setting('withdraw_status_message', null) == null) {
            add_setting('withdraw_status_message', 'Sorry! right now you can not withdraw token. Please contact us if you have any question.');
        }
        if (get_setting('withdraw_currency', null) == null) {
            add_setting('withdraw_currency', json_encode(['eth' => 1, 'btc' => 1, 'ltc' => 1]));
        }
        if (get_setting('withdraw_notes', null) == null) {
            add_setting('withdraw_notes', 'You can withdraw the your funds and we will process soon.');
        }
    }
}
