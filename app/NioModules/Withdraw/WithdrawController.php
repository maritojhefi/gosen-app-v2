<?php

namespace App\NioModules\Withdraw;

/**
 * Withdraw Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Withdraw Controller
 *
 * @author Softnio
 *
 * @version 1.0
 */

use App\Http\Controllers\Controller;
use App\Mail\WithdrawGosenApproveMail;
use App\Mail\WithdrawGosenRejectMail;
use App\Mail\WithdrawGosenRequestMail;
use App\Models\BonusDolar;
use App\Models\IcoStage;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class WithdrawController extends Controller
{
    protected $wdm;

    public function __construct()
    {
        $this->wdm = new WithdrawModule();
    }

    /**
     * Display the withdraw page @Admin
     *
     * @return \Illuminate\Http\Response
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public function index()
    {
        $stages = IcoStage::whereNotIn('status', ['deleted'])->get();
        $transactions = Transaction::where('tnx_type', 'withdraw')->whereNotIn('status', ['new', 'deleted'])->latest()->get();
        $currencies = WithdrawModule::support_currency('all');
        $settings = $this->settings('all');

        return view('withdraw::admin', compact('transactions', 'currencies', 'settings', 'stages'));
    }

    /**
     * Display the withdraw page @Users
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function user_index()
    {
        $prices = Setting::exchange_rate($this->get_current_price());
        $transactions = Transaction::where('tnx_type', 'withdraw')->whereNotIn('status', ['new', 'deleted'])->where('user', auth()->id())->latest()->get();
        $currencies = WithdrawModule::support_currency('all');
        $settings = $this->settings('all');
        $user = Auth::user();
        // $launch = Transaction::where('user',$user->id)->first();

        // $transactions = Transaction::where('user', auth()->user()->id)->orderBy('created_at', 'desc')->get();
        // $totalTokensCredito2 = $transactions->whereNotIn('tnx_type',['withdraw', 'refund', 'joined', 'launch','gift'])->where('status', 'approved')->sum('total_tokens');
        // $totalTokensRetiro2 = $transactions->whereIn('tnx_type',['withdraw', 'refund'])->where('status', 'approved')->sum('tokens');
        // $totaltokens = $totalTokensCredito2-$totalTokensRetiro2;
        // dd($totaltokens);

        // $token_balance_sum = Transaction::where('user',$user->id)->whereIn('tnx_type',['purchase'])->where('status','approved')->sum('total_tokens');

        $tokensStage = Transaction::where('user', $user->id)->select('total_tokens', 'stage', 'tnx_type', 'status')->get();
        // dd($tokensStage);
        $bonusDolar = BonusDolar::where('user_id', $user->id)->where('type', 'withdraw')->get();
        // dd($bonusDolar);
        $bonus = BonusDolar::where('user_id', auth()->user()->id)->where('type', '!=', BonusDolar::TYPE1)->get();
        $monto = auth()->user()->dolarBonus;

        return view('withdraw::user', compact('transactions', 'currencies', 'settings', 'prices', 'tokensStage', 'user', 'bonusDolar', 'monto', 'bonus'));
    }

    /**
     * Details for of withdraw transaction
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function view_details(Request $request)
    {
        $transaction = Transaction::where('id', $request->id)->first();

        return view('withdraw::details', compact('transaction'))->render();
    }

    /**
     * Update withdraw transaction
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function update_transaction(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($request->input('id') && $request->input('action') && nio_feature() == 'cool' && env_file('3', 2)) {
            $id = $request->input('id');
            $action = $request->input('action');
            $trnx = Transaction::find($id);
            if ($trnx) {
                $ret['status'] = __status($trnx->status, 'icon');
                if ($action == 'approve' && $trnx->status != 'approved' && $trnx->status != 'canceled') {
                    $trnx->status = 'approved';
                    $trnx->checked_by = json_encode(['name' => Auth::user()->name, 'id' => Auth::id()]);
                    if ($request->input('extra', null) != null) {
                        $trnx->payment_id = $request->input('extra');
                    }
                    $trnx->checked_time = now()->toDateTimeString();
                    $trnx->save();
                    if ($trnx->details == 'Tokens Withdraw') {
                        $trnx->tnxUser->tokenBalance = $trnx->tnxUser->tokenBalance - $trnx->total_tokens;
                    } else {
                        $trnx->tnxUser->gift_bonus = $trnx->tnxUser->gift_bonus - $trnx->total_tokens;
                    }
                    $trnx->tnxUser->save();
                    try {
                        Mail::to($trnx->tnxUser->email)
                        ->send(new WithdrawGosenApproveMail($trnx));
                        // antiguo correo
                        // $trnx->tnxUser->notify(new WithdrawNotification($trnx, $this->wdm->email('user-success')));
                    } catch (\Exception $e) {
                        //info($e->getMessage());
                    }
                    $ret['msg'] = 'success';
                    $ret['data'] = $trnx;
                    $ret['status'] = __status($trnx->status, 'icon');
                    $ret['message'] = ___('Withdraw transaction has been approved successfully.');
                }
                if ($action == 'reject' && $trnx->status != 'approved' && $trnx->status != 'rejected' && $trnx->status != 'canceled') {
                    $trnx->status = 'rejected';
                    $trnx->checked_by = json_encode(['name' => Auth::user()->name, 'id' => Auth::id()]);
                    $trnx->checked_time = now()->toDateTimeString();
                    $trnx->extra = $request->input('extra');
                    if ($request->input('extra') != null) {
                        $trnx->abstract = json_encode(['trnx' => $trnx->id, 'message' => $request->input('extra')]);
                    }
                    $trnx->save();

                    // if($trnx->details == 'Tokens Withdraw'){
                    //     $trnx->tnxUser->tokenBalance = $trnx->tnxUser->tokenBalance + $trnx->total_tokens;
                    // }else{
                    //     $trnx->tnxUser->gift_bonus = $trnx->tnxUser->gift_bonus + $trnx->total_tokens;
                    // }
                    // $trnx->tnxUser->save();
                    try {
                        Mail::to($trnx->tnxUser->email)
                            ->send(new WithdrawGosenRejectMail($trnx));
                        // antiguo correo
                        // $trnx->tnxUser->notify(new WithdrawNotification($trnx, $this->wdm->email('user-reject')));
                    } catch (\Exception $e) {
                        //info($e->getMessage());
                    }
                    $ret['msg'] = 'error';
                    $ret['status'] = __status($trnx->status, 'icon');
                    $ret['message'] = ___('Withdraw transaction has been rejected.');
                }
                if ($action == 'delete' && ($trnx->status == 'rejected' || $trnx->status != 'canceled')) {
                    $trnx->delete();
                    $ret['msg'] = 'error';
                    $ret['status'] = __status($trnx->status, 'icon');
                    $ret['message'] = ___('Withdraw transaction has been deleted.');
                }
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Update withdraw settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function update_settings(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($request->input('action') == 'settings' && nio_feature() == 'cool' && env_file('3', 2)) {
            if ($request->input('withdraw_minimum')) {
                add_setting('withdraw_minimum', $request->input('withdraw_minimum'));
            }
            if ($request->input('withdraw_maximum')) {
                add_setting('withdraw_maximum', $request->input('withdraw_maximum'));
            }
            if ($request->input('withdraw_price')) {
                add_setting('withdraw_price', $request->input('withdraw_price'));
            }
            if ($request->input('withdraw_custom_price')) {
                add_setting('withdraw_custom_price', $request->input('withdraw_custom_price'));
            }

            $currencies = array_fill_keys(WithdrawModule::support_currency(), 0);
            $currency_req = ($request->input('withdraw_currency')) ? array_keys($request->input('withdraw_currency')) : null;
            if ($currency_req) {
                $currency_checked = array_fill_keys($currency_req, 1);
                $currencies = array_merge($currencies, $currency_checked);
            }
            add_setting('withdraw_currency', json_encode($currencies));
            add_setting('withdraw_notes', $request->input('withdraw_notes'));
            add_setting('withdraw_enable', (isset($request->withdraw_enable) ? 1 : 0));
            add_setting('withdraw_status', (isset($request->withdraw_status) ? 1 : 0));
            add_setting('withdraw_status_message', $request->input('withdraw_status_message'));

            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'Withdraw Settings']);
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * User actions for withdraw transaction
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function user_withdraw_action(Request $request)
    {
        $ret['msg'] = 'warning';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Unable to proceed request!');
        $user = Auth::user();
        $actions = $request->input('action');
        $tnxid = $request->input('id');

        if ($tnxid && $actions) {
            $trnx = Transaction::find($tnxid);
            // dd($trnx->details);
            if ($trnx) {
                if ($actions == 'cancel') {
                    if ($trnx->status != 'approved' && $trnx->status != 'canceled' && $trnx->status != 'rejected') {
                        $trnx->status = 'canceled';
                        $trnx->checked_by = json_encode(['name' => auth()->user()->name, 'id' => auth()->user()->id]);

                        $trnx->save();
                        // if($trnx->details == 'Tokens Withdraw'){
                        //     $user->tokenBalance = $user->tokenBalance + $trnx->total_tokens;
                        // }else{
                        //     $user->gift_bonus = $user->gift_bonus + $trnx->total_tokens;
                        // }
                        // $user->save();
                        $ret['reload'] = true;
                    }
                    $ret['msg'] = 'error';
                    $ret['status'] = __status($trnx->status, 'icon');
                    $ret['message'] = ($trnx->status == 'approved' ? ___('Withdraw transaction has been already approved.') : ___('Withdraw transaction has been canceled.'));
                }
                if ($actions == 'delete') {
                    $ret['message'] = ___('Unable delete the withdraw transaction.');
                    if ($trnx->status == 'canceled') {
                        $trnx->status = 'deleted';
                        $trnx->save();
                        $ret['message'] = ___('Withdraw transaction has been deleted.');
                    }
                    $ret['msg'] = 'error';
                    $ret['status'] = __status($trnx->status, 'icon');
                }
            } else {
                $ret['message'] = ___('Invalid transaction or not found!');
            }
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * User request handle for withdraw
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function user_request(Request $request)
    {
        // dd($request);
        $ret['msg'] = 'warning';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Unable to proceed request!');

        // get data
        $user = Auth::user();
        $balance = (! empty($user->tokenBalance)) ? (float) $user->tokenBalance : 0;
        $type = $request->type;

        if ($type) {
            if ($type == 'modal') {
                $prices = Setting::exchange_rate($this->get_current_price());
                $currencies = WithdrawModule::support_currency('all');
                $settings = $this->settings('all');
                $minimo = (float) $this->settings('minimum');

                if ($user->tokenBalance < $minimo) {
                    $ret['msg'] = 'warning';
                    $ret['icon'] = 'ti ti-reload';
                    $ret['message'] = ___('Your minimum of Token balance must be').' : '.$minimo.' '.token_symbol();

                    return response()->json($ret);
                } else {
                    return response()->json(['modal' => view('withdraw::request', compact('balance', 'prices', 'currencies', 'settings'))->render()]);
                }
            } elseif ($type == 'request') {
                $validator = Validator::make($request->all(), [
                    'address' => 'required',
                    'amount' => 'required|integer|min:1',
                ], [
                    'address.required' => ___('Enter your wallet address for withdraw.'),
                    'amount.required' => ___('Please enter minimum number of token.'),
                    'amount.min' => ___('Please enter minimum number of token.'),
                ]);
                if ($validator->fails()) {
                    $ret['msg'] = 'warning';
                    $ret['message'] = $validator->errors()->first();
                } else {
                    $totalAmount = Transaction::where([
                        ['user', $user->id],
                        ['status', 'pending'],
                        ['tnx_type', 'withdraw'],
                        ['details', 'Tokens Withdraw'],
                    ])->sum('total_tokens');
                    $mod = $request->input('gos');
                    $minimum = (float) $this->settings('minimum');
                    $maximum = (float) $this->settings('maximum');
                    $token_sign = token_symbol();
                    $tokens = (float) $request->input('amount');
                    $total = $totalAmount + $tokens;
                    $currency = $request->input('currency');
                    $address = $request->input('address');
                    if ($tokens >= $minimum && $tokens <= $maximum) {
                        if ($tokens <= $balance) {
                            if ($total > $balance) {
                                $ret['msg'] = 'error';
                                $ret['message'] = 'Your total withdrawal requests exceeded your current balance';

                                return response()->json($ret);
                            }
                            $create = $this->create_withdraw($address, $tokens, $currency, $user, $mod);
                            if ($create) {
                                $ret['data'] = $create;
                                $ret['msg'] = 'success';
                                $ret['link'] = route('withdraw:user.index');
                                $ret['reload'] = true;
                                $ret['message'] = ___('Your withdrawal has been successfully placed. You can see its progress on this withdrawal page.');
                            } else {
                                $ret['msg'] = 'error';
                                $ret['icon'] = 'ti ti-reload';
                                $ret['message'] = ___('Something is wrong! Unable to process your request.');
                            }
                        } else {
                            $ret['msg'] = 'error';
                            $ret['icon'] = 'ti ti-help-alt';
                            $ret['message'] = ___('You balance is insufficient for a withdrawal.');
                        }
                    } else {
                        $ret['msg'] = 'info';
                        $ret['icon'] = 'ti ti-help-alt';
                        $ret['message'] = ($tokens > $maximum) ? ___('You can withdraw maximum :amount :symbol at once.', ['amount' => $maximum, 'symbol' => $token_sign]) : ___('Minimum :amount :symbol required to withdraw.', ['amount' => $minimum, 'symbol' => $token_sign]);
                    }
                }
            } else {
                return response()->json($ret);
            }
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function user_request_bonus(Request $request)
    {
        $ret['msg'] = 'warning';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Unable to proceed request!');

        // get data
        $user = Auth::user();
        $balance = (! empty($user->gift_bonus)) ? (float) $user->gift_bonus : 0;
        $type = $request->type;

        if ($type) {
            if ($type == 'modal') {
                $prices = Setting::exchange_rate($this->get_current_price());
                $currencies = WithdrawModule::support_currency('all');
                $settings = $this->settings('all');

                return response()->json(['modal' => view('withdraw::request-bonus', compact('balance', 'prices', 'currencies', 'settings'))->render()]);
            } elseif ($type == 'request') {
                $validator = Validator::make($request->all(), [
                    'address' => 'required',
                    'amount' => 'required|integer|min:1',
                ], [
                    'address.required' => ___('Enter your wallet address for withdraw.'),
                    'amount.required' => ___('Please enter minimum number of token.'),
                    'amount.min' => ___('Please enter minimum number of token.'),
                ]);
                if ($validator->fails()) {
                    $ret['msg'] = 'warning';
                    $ret['message'] = $validator->errors()->first();
                } else {
                    $totalAmount = Transaction::where([
                        ['user', $user->id],
                        ['status', 'pending'],
                        ['tnx_type', 'withdraw'],
                        ['details', 'Tokens Withdraw Bonus'],
                    ])->sum('total_tokens');
                    $mod = $request->input('bonus');
                    $minimum = 0;
                    $maximum = $user->gift_bonus;
                    $token_sign = token_symbol();
                    $tokens = (float) $request->input('amount');
                    $total = $totalAmount + $tokens;
                    $currency = $request->input('currency');
                    $address = $request->input('address');
                    if ($tokens >= $minimum && $tokens <= $maximum) {
                        if ($tokens <= $balance) {
                            if ($total > $balance) {
                                $ret['msg'] = 'error';
                                $ret['message'] = 'Your total withdrawal Bonus requests exceeded your current balance';

                                return response()->json($ret);
                            }
                            $create = $this->create_withdraw($address, $tokens, $currency, $user, $mod);
                            if ($create) {
                                $ret['data'] = $create;
                                $ret['msg'] = 'success';
                                $ret['link'] = route('withdraw:user.index');
                                $ret['reload'] = true;
                                $ret['message'] = ___('Your withdrawal has been successfully placed. You can see its progress on this withdrawal page.');
                            } else {
                                $ret['msg'] = 'error';
                                $ret['icon'] = 'ti ti-reload';
                                $ret['message'] = ___('Something is wrong! Unable to process your request.');
                            }
                        } else {
                            $ret['msg'] = 'error';
                            $ret['icon'] = 'ti ti-help-alt';
                            $ret['message'] = ___('You balance is insufficient for a withdrawal.');
                        }
                    } else {
                        $ret['msg'] = 'info';
                        $ret['icon'] = 'ti ti-help-alt';
                        $ret['message'] = ($tokens > $maximum) ? ___('You can withdraw maximum :amount :symbol at once.', ['amount' => $maximum, 'symbol' => $token_sign]) : ___('Minimum :amount :symbol required to withdraw.', ['amount' => $minimum, 'symbol' => $token_sign]);
                    }
                }
            } else {
                return response()->json($ret);
            }
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Create withdraw transaction
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function create_withdraw($address, $token, $currency, $user = null, $mod = null)
    {
        $user = (! empty($user)) ? $user : Auth::user();
        $tokens = round($token, min_decimal());
        $prices = Setting::exchange_rate($this->get_current_price());
        $rate_cur = (isset($prices[$currency])) ? $prices[$currency] : active_stage()->base_price;
        $base_currency = base_currency();
        $base_amount = round(($tokens * $prices['base']), max_decimal());
        $base_currency_rate = round($prices['base'], max_decimal());
        $amount = round(($tokens * $rate_cur), max_decimal());
        $currency_rate = round($rate_cur, max_decimal());

        if ($user->tokenBalance >= $tokens) {
            $save_data = [
                'tnx_id' => set_id(rand(100, 999), 'withdraw'),
                'tnx_type' => 'withdraw',
                'tnx_time' => now()->toDateTimeString(),
                'tokens' => $tokens,
                'bonus_on_base' => 0,
                'bonus_on_token' => 0,
                'total_bonus' => 0,
                'total_tokens' => $tokens,
                'stage' => active_stage()->id,
                'user' => $user->id,
                'amount' => $amount,
                'base_amount' => $base_amount,
                'base_currency' => $base_currency,
                'base_currency_rate' => $base_currency_rate,
                'currency' => $currency,
                'currency_rate' => $currency_rate,
                'payment_method' => 'withdraw',
                'wallet_address' => $address,
                'all_currency_rate' => json_encode($prices),
                'payment_to' => '',
                'details' => $mod == 'modal-bonus' ? 'Tokens Withdraw Bonus' : 'Tokens Withdraw',
                'status' => 'pending',
            ];
            $transaction = new Transaction();
            $transaction->fill($save_data)->save();
            $transaction->tnx_id = set_id($transaction->id, 'withdraw');
            $transaction->save();
            // if(isset($mod) && $mod=="modal-bonus")
            // {
            //     if ($transaction) {
            //         $user->gift_bonus = $user->gift_bonus - $tokens;
            //         $user->save();
            //     }
            // }
            // else
            // {
            //     if ($transaction) {
            //         $user->tokenBalance = $user->tokenBalance - $tokens;
            //         $user->save();
            //     }
            // }

            try {
                Mail::to($transaction->tnxUser->email)
                    ->send(new WithdrawGosenRequestMail($transaction));
                //                 antiguo metodo por notificacion para mandar correo de peticion de retiro
                //                 $transaction->tnxUser->notify(new WithdrawNotification($transaction, $this->wdm->email('user')));
                //                 $this->wdm->notifyAdmins($transaction);
            } catch (\Exception $e) {
                //info($e->getMessage());
            }

            return $transaction ? $transaction : null;
        } else {
            return false;
        }
    }

    /**
     * Get current token rate for withdraw
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function get_current_price()
    {
        $is_stage = $this->settings('price');
        if ($is_stage == 'custom') {
            return $this->settings('custom_price');
        } elseif ($is_stage == 'active') {
            return active_stage()->base_price;
        } else {
            $stage = IcoStage::find($is_stage);
            if ($stage) {
                return $stage->base_price;
            }
        }

        return active_stage()->base_price;
    }

    /**
     * Get all settings for withdraw
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function settings($key)
    {
        $settings = (object) config('withdraw.settings');

        return ($key == 'all') ? $settings : ($settings->$key ?? false);
    }

    /**
     * Merge supported currency and enabled currency
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function get_currency()
    {
        $supported = array_fill_keys(WithdrawModule::support_currency(), 0);
        $currency = json_decode(gws('withdraw_currency'), true);

        return array_merge($supported, $currency);
    }
}
