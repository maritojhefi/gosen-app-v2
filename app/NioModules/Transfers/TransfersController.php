<?php

namespace App\NioModules\Transfers;

/**
 * Transfers Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Transfers Controller
 *
 * @author Softnio
 *
 * @version 1.0
 */

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class TransfersController extends Controller
{
    public function __construct()
    {
        $this->tsm = new TransfersModule();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_token(Request $request)
    {
        $ret['msg'] = 'warning';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Unable to proceed request!');
        $user = auth()->user();
        $type = $request->type;
        if ($type) {
            if ($type == 'modal') {
                $settings = $this->settings('all');
                $balance = (! empty($user->tokenBalance)) ? (float) $user->tokenBalance : 0;

                return response()->json(['modal' => view('transfer::request', compact('settings', 'balance'))->render()]);
            } elseif ($type == 'request') {
                return $this->create_transaction($request);
            } else {
                return response()->json($ret);
            }
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    protected function create_transaction($request)
    {
        if (session()->has('pack')) {
            $pack = session('pack');
        } else {
            $pack = null;
        }
        if (version_compare(phpversion(), '7.1', '>=')) {
            ini_set('precision', 17);
            ini_set('serialize_precision', -1);
        }
        $ret['msg'] = 'info';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = __('messages.nothing');
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'token_amount' => 'required|integer|min:1',
        ], [
            'token_amount.required' => ___('Enter number of token you would like to send.'),
            'token_amount.min' => ___('Please enter minimum number of token.'),
        ]);

        if ($validator->fails()) {
            $ret['msg'] = 'warning';
            $ret['message'] = $validator->errors()->first();
        } else {
            $minimum = (float) $this->settings('minimum');
            $maximum = (float) $this->settings('maximum');
            $token_sign = token_symbol();
            $tokens = (float) $request->input('token_amount');

            if ($tokens >= $minimum && $tokens <= $maximum) {
                $sender = Auth::user();
                $balance = (float) $sender->tokenBalance;
                $receiver = User::where('role', 'user')->where('email', $request->email)->first();

                if ($receiver && ($sender->id != $receiver->id)) {
                    if ($tokens <= $balance) {
                        $base_currency = base_currency();
                        $amount = round(token_price($tokens, $base_currency), max_decimal());
                        $tnx_data = [
                            'tnx_id' => set_id(rand(100, 999), 'trnx'),
                            'tnx_type' => 'transfer',
                            'tnx_time' => now(),
                            'tokens' => $tokens,
                            'total_tokens' => (-$tokens),
                            'stage' => 0,
                            'user' => auth()->id(),
                            'amount' => $amount,
                            'receive_amount' => 0,
                            'receive_currency' => $base_currency,
                            'base_amount' => $amount,
                            'base_currency' => $base_currency,
                            'base_currency_rate' => null,
                            'currency' => $base_currency,
                            'payment_to' => $request->email,
                            'payment_id' => rand(1000, 9999),
                            'details' => 'Transfered Tokens',
                            'added_by' => set_added_by(auth()->id(), auth()->user()->role),
                            'extra' => 'sent',
                            'status' => 'pending',
                        ];
                        $transaction = Transaction::create($tnx_data);

                        if ($transaction) {
                            $transaction->tnx_id = set_id($transaction->id, 'trnx');
                            $transaction->tnxUser->tokenBalance = ($transaction->tnxUser->tokenBalance - abs($transaction->tokens));
                            $transaction->package = $pack;
                            $transaction->push();

                            try {
                                $transaction->tnxUser->notify(new TransfersNotification($transaction, $this->tsm->email('sender')));
                                $this->tsm->notifyAdmins($transaction);
                            } catch (\Exception $e) {
                                //info($e->getMessage());
                            }

                            $ret['link'] = route('user.transactions');
                            $ret['msg'] = 'success';
                            $ret['icon'] = 'ti ti-check';
                            $ret['message'] = ___('Your request successfully submitted and waiting for our team approval.');
                        } else {
                            $ret['msg'] = 'error';
                            $ret['icon'] = 'ti ti-reload';
                            $ret['message'] = ___('Something is wrong, please try again later!');
                            $transaction->delete();
                        }
                    } else {
                        $ret['msg'] = 'error';
                        $ret['icon'] = 'ti ti-help-alt';
                        $ret['message'] = ___('You balance is insufficient for send token.');
                    }
                } else {
                    $ret['msg'] = 'error';
                    $ret['icon'] = 'ti ti-na';
                    $ret['message'] = ($receiver && ($sender->id == $receiver->id)) ? ___('You can not send your token to your own account.') : ___('Sorry! your provided email address is invalid or not associated with our platform.');
                }
            } else {
                $ret['msg'] = 'info';
                $ret['icon'] = 'ti ti-help-alt';
                $ret['message'] = ($tokens > $maximum) ? ___('You can send maximum :amount :symbol at once.', ['amount' => $maximum, 'symbol' => $token_sign]) : ___('Minimum :amount :symbol required to send.', ['amount' => $minimum, 'symbol' => $token_sign]);
            }
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    /**
     * Update a transaction.
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     */
    public function update_transaction(Request $request)
    {
        $tnxid = $request->tnx_id;
        $update_type = $request->status;
        $ret['msg'] = 'warning';
        $ret['icon'] = 'ti ti-info-alt';
        $ret['message'] = ___('Invalid transaction or not found!');
        if (nio_feature() == 'cool' && env_file('3', 2)) {
            if ($transaction = Transaction::find($tnxid)) {
                if ($transaction->status == 'pending') {
                    if ($update_type == 'approved') {
                        $transaction->status = 'approved';
                        $transaction->checked_by = set_added_by(auth()->id(), auth()->user()->role);
                        $transaction->checked_time = now();

                        $receiver = User::where('email', $transaction->payment_to)->first();
                        if ($receiver && $transaction->save()) {
                            $copy = $transaction->replicate();
                            $copy->user = $receiver->id;
                            $copy->receive_amount = abs($copy->amount);
                            $copy->payment_id = $transaction->id;

                            $copy->total_tokens = abs($copy->total_tokens);
                            $copy->amount = abs($copy->amount);
                            $copy->base_amount = abs($copy->base_amount);

                            $copy->payment_to = $transaction->tnxUser->email;
                            $copy->extra = 'received';
                            $copy->details = 'Received Tokens';
                            $copy->added_by = set_added_by('00');
                            $copy->checked_by = set_added_by(auth()->id(), auth()->user()->role);
                            $copy->checked_time = now();
                            $copy->status = 'approved';
                            $copy->save();

                            $copy->tnx_id = set_id($copy->id, 'trnx');
                            $copy->save();

                            $transaction->payment_id = $copy->id;
                            $transaction->save();

                            $receiver->tokenBalance += abs($copy->tokens);
                            $receiver->save();
                            try {
                                $transaction->tnxUser->notify(new TransfersNotification($transaction, $this->tsm->email('sender-success')));
                                $receiver->notify(new TransfersNotification($copy, $this->tsm->email('receiver')));
                            } catch (\Exception $e) {
                                //info($e->getMessage());
                            }
                            $ret['msg'] = 'success';
                            $ret['icon'] = 'ti ti-check';
                            $ret['message'] = ___('Transaction approved successfully.');
                        } else {
                            $ret['msg'] = 'warning';
                            $ret['icon'] = 'ti ti-help-alt';
                            $ret['message'] = ___('The receiver account not found.');
                        }
                    } elseif (in_array($update_type, ['canceled', 'rejected'])) {
                        $transaction->status = 'canceled';
                        $transaction->checked_by = set_added_by(auth()->id(), auth()->user()->role);
                        $transaction->checked_time = now();
                        if ($transaction->save()) {
                            $transaction->tnxUser->tokenBalance += $transaction->tokens;
                            $transaction->tnxUser->save();
                            try {
                                $transaction->tnxUser->notify(new TransfersNotification($transaction, $this->tsm->email('sender-reject')));
                            } catch (\Exception $e) {
                                //info($e->getMessage());
                            }
                        }
                        $ret['msg'] = 'success';
                        $ret['icon'] = 'ti ti-check';
                        $ret['message'] = ___('Transaction has been rejected.');
                    }
                } else {
                    $ret['msg'] = 'info';
                    $ret['icon'] = 'ti ti-info-alt';
                    $ret['message'] = ___('Transaction already :status.', ['status' => $transaction->status]);
                }
            }
        }
        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function settings($key)
    {
        $settings = (object) config('transfer.settings');

        return ($key == 'all') ? $settings : ($settings->$key ?? false);
    }

    /**
     * Update transfer settings
     *
     * @version 1.0.0
     *
     * @since 1.0.0
     */
    public function update_settings(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($request->input('action') == 'settings' && nio_feature() == 'cool' && env_file('3', 2)) {
            if ($request->input('transfer_minimum')) {
                add_setting('transfer_minimum', $request->input('transfer_minimum'));
            }
            if ($request->input('transfer_maximum')) {
                add_setting('transfer_maximum', $request->input('transfer_maximum'));
            }
            add_setting('transfer_enable', (isset($request->transfer_enable) ? 1 : 0));
            add_setting('transfer_notes', $request->input('transfer_notes'));
            add_setting('transfer_status', (isset($request->transfer_status) ? 1 : 0));
            add_setting('transfer_status_message', $request->input('transfer_status_message'));

            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'Transfers Settings']);
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }

        return back()->with([$ret['msg'] => $ret['message']]);
    }
}
