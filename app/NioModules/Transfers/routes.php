<?php

use App\Http\Controllers\App;
use Illuminate\Support\Facades\Route;
use App\NioModules\Transfers\TransfersController;

/**
 * Transfers Module for TokenLite Application
 * To run this module, required TokenLite v1.1.4+ version.
 *
 * Transfers Module Route
 *
 * @author Softnio
 *
 * @version 1.0
 */
Route::middleware('web')->name('transfer:')->group(function () {
    Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin', 'g2fa', 'ico'])->group(function () {
        Route::post('/transfer/update', [TransfersController::class, 'update_transaction'])->name('update');
        Route::post('/transfer/setting', [TransfersController::class, 'update_settings'])->name('update.setting');
    });
    Route::prefix('user')->name('user.')->middleware(['auth', 'user', 'verify_user', 'g2fa'])->group(function () {
        Route::post('/send-token', [TransfersController::class, 'send_token'])->name('send');
    });
});
