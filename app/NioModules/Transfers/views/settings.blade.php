<h3 class="title">Transfer Settings</h3>
<p class="wide-lg">You can manage your internal token transfer system from here. You can sepecify minimum and maximum token number for each transfer request.</p>
<div class="gaps gaps-1x"></div>
<form id="transfers-form" class="validate-modern" action="{{ route('transfer:admin.update.setting') }}" method="POST">
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="input-item input-with-label">
                <label class="input-item-label">Transfers System</label>
                <div class="input-wrap input-wrap-switch">
                    <input class="input-switch switch-toggle" data-switch="switch-to-transfer" name="transfer_enable" type="checkbox" {{ get_setting('transfer_enable')==1 ? 'checked ' : '' }}id="transfers-system-enable">
                    <label for="transfers-system-enable"><span>Disable</span><span class="over">Enable</span></label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="switch-content switch-to-transfer">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Minimum Token</label>
                            <div class="input-wrap">
                                <input class="input-bordered" type="number" name="transfer_minimum" min="1" placeholder="Minimum" value="{{ $settings->minimum ?? 0 }}" required>
                            </div>
                            <span class="input-note">Set minimum limit of transferal per request.</span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Maximum Token</label>
                            <div class="input-wrap">
                                <input class="input-bordered" type="number" name="transfer_maximum" min="1" placeholder="Maximum" value="{{ $settings->maximum ?? 0 }}" required>
                            </div>
                            <span class="input-note">Set maximum limit of transferal per request.</span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-item input-with-label">
                            <label class="input-item-label">Transfers Notes</label>
                            <div class="input-wrap">
                                <input class="input-bordered" name="transfer_notes" placeholder="Transfers footer notes" value="{{ $settings->notes ?? '' }}">
                            </div>
                            <span class="input-note">Add a footer note on transfer request dialog box.</span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-item">
                            <label class="input-item-label">Disable Transfers Request</label>
                            <div class="input-wrap">
                                <input type="checkbox" class="input-switch switch-toggle" data-switch="switch-to-msg" name="transfer_status" id="transfer_status" {{ $settings->status ? 'checked' : '' }}>
                                <label for="transfer_status"><span>No</span> <span class="over">Yes</span></label>
                            </div>
                            <span class="input-note">If you want to temporarily stop new transferal request from users.</span>
                        </div>
                        <div class="switch-content switch-to-msg">
                            <div class="input-item input-with-label">
                                <label class="input-item-label">Message to Users</label>
                                <div class="input-wrap">
                                    <textarea class="input-bordered" name="transfer_status_message" placeholder="Enter your note">{{ $settings->message }}</textarea>
                                </div>
                                <span class="input-note">The message will display if you disable transfer request from user.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gaps-1x"></div>
    @csrf
    <input type="hidden" name="action" value="settings">
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@push('footer')
<script type="text/javascript">
    (function($){
        var $transfer_form = $('#transfers-form');
        if($transfer_form.length > 0){
            ajax_form_submit($transfer_form, false);
        }
    })(jQuery)
</script>
@endpush