<div class="modal fade" id="user-transfers" tabindex="-1">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
        <div class="modal-content">
            @if($settings->status==0 && $balance > 0)
            <a href="#" class="modal-close" data-dismiss="modal"><em class="ti ti-close"></em></a>
            <div class="popup-body">
                <h2 class="popup-title">{{ ___('Send Token to Your Friend') }}</h2>
                <div class="popup-content">
                    <div class="alert alert-xs alert-warning"><strong>{{ ___('Available Token Balance:') }} <span class="text-primary fs-12">{{ to_num_token($balance).' '.token_symbol() }}</span></strong></div>
                    <form id="send-token-form" class="validate-modern" action="{{ route('transfer:user.send') }}" method="post">
                        <div class="input-item input-with-label">
                            <label for="full-name" class="input-item-label">{{ ___('Receiver/Friend Email') }}</label>
                            <div class="input-wrap">
                                <input id="" name="email" placeholder="{{ ___('Enter receiver email') }}" class="input-bordered" type="email" required="">
                            </div>
                            <span class="input-note">{{ ___('Enter a valid email address, which is associated with our platform.') }}</span>
                        </div>
                        <div class="input-item">
                            <div class="input-wrap">
                                <input name="token_amount" placeholder="{{ ___('Enter token number') }}" class="input-bordered token-number" type="number" required="">
                            </div>
                            <span class="input-note">{!! __( 'Minimum Send: :amount :symbol', ['amount' => '<strong>'.$settings->minimum.'</strong>', 'symbol' => '<strong>'.token_symbol().'</strong>'] ) !!}</span>
                        </div>
                        <div class="gaps-0-5x"></div>
                        @csrf
                        <input type="hidden" name="type" value="request">
                        <button type="submit" class="btn btn-auto btn-alt btn-primary payment-btn">{{ ___('Proceed') }} <em class="ml-3 ti-arrow-right"></em></button>
                    </form>
                    @if($settings->notes)
                    <div class="gaps gaps-3-5x"></div>
                    <div class="note note-plane note-light">
                        <em class="fas fa-info-circle"></em>
                        <p>{{ $settings->notes }}</p>
                    </div>
                    @endif
                </div>
            </div>
            @else 
                <div class="popup-body popup-body-lg">
                    <div class="gaps gaps-0-5x"></div>
                    <div class="alert alert-warning mb-0 text-center">
                        {{ ($balance==0) ? ___('You do not have enough token to send.') : ($settings->message ?? ___('You can not send token right now.')) }}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    (function($) {
        var $sendtoken = $('form#send-token-form');
        if ($sendtoken.length > 0) {
            $sendtoken.validate({
                errorElement: "span",
                errorClass: "input-border-error error",
                submitHandler: function(form) {
                    $(form).ajaxSubmit ({
                        dataType: 'json',
                        success: function(data) { show_toast( data.msg, data.message, ((data.icon) ? data.icon : 'ti ti-info-alt') );
                            if( data.msg == 'success' ){ $(form).closest('.modal').modal('hide'); }
                        },
                        error: function(xhr, response, status){
                            //console.log(xhr, response, status); 
                            show_toast('warning', '{{ ___('Something is wrong!') }}', 'ti ti-alert');
                        }
                    });
                }
            });
        }
    })(jQuery);
</script>