<?php

namespace App\NioModules\Transfers;

/**
 * Transfers Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Transfers Notification Class
 *
 * @author Softnio
 *
 * @version 1.0
 */
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TransfersNotification extends Notification
{
    /**
     * The transfers transaction.
     *
     * @var string
     */
    public $transfers;

    public $template;

    public $tsm;

    /**
     * Create a notification instance.
     *
     * @param  object  $transfers
     * @return void
     */
    public function __construct($transfers, $template)
    {
        $this->transfers = $transfers;
        $this->template = $template;
        $this->tsm = new TransfersModule();
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $from_name = email_setting('from_name', get_setting('site_name'));
        $from_email = email_setting('from_email', get_setting('site_email'));

        $et = $this->shortcode_replace($this->tsm->template($this->template));

        return (new MailMessage)
            ->from($from_email, $from_name)
            ->subject($et->subject)
            ->greeting($et->greeting)
            ->salutation($et->regards)
            ->markdown('mail.base')->with(['__message' => $et->message]);
    }

    public function shortcode_replace($template)
    {
        if ($template->slug == $this->tsm->email('admin')) {
            $greeting = str_replace('[[user_name]]', get_admin('name'), $template->greeting);
            $template->greeting = replace_shortcode($greeting);
        }
        $template->subject = replace_shortcode($template->subject);
        $template->greeting = str_replace('[[user_name]]', $this->transfers->tnxUser->name, $template->greeting);
        $template->regards = $template->regards == true ? replace_shortcode(get_setting('site_mail_footer')) : null;
        $message = $this->replace_msg($template->message);
        $template->message = replace_shortcode($message);

        return $template;
    }

    public function replace_msg($message)
    {
        $tsf = $this->transfers;
        $transfer_note = (empty($tsf->extra) ? 'We cannot accept your request right now.' : $tsf->extra);
        $short_name = [
            "\n",
            '[[order_id]]',
            '[[total_tokens]]',
            '[[sender_name]]',
            '[[receiver_name]]',
            '[[sender_details]]',
            '[[sender_email]]',
            '[[receiver_email]]',
            '[[transfer_note]]',
        ];
        $replace_with = [
            '<br>',
            $tsf->tnx_id,
            abs($tsf->tokens).' '.token_symbol(),
            (str_contains($this->template, 'admin') ? ($tsf->tnxUser->name ?? 'a user') : $tsf->tnxUser->name),
            (str_contains($this->template, 'admin') ? ($tsf->receiver->name ?? 'another user') : $tsf->receiver->name),
            $this->sender_details(),
            $tsf->tnxUser->email,
            $tsf->receiver->email,
            $transfer_note,
        ];
        $return = str_replace($short_name, $replace_with, $message);

        return $return;
    }

    public function sender_details()
    {
        $text = str_contains($this->template, 'receiver') ? ($this->transfer->receiver ?? false) : false;
        if ($text) {
            return $text->name.' - '.$text->email;
        }

        return '';
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
