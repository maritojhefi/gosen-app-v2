<?php

namespace App\NioModules\Transfers;

/**
 * Transfers Module for TokenLite Application
 * To run this application, required TokenLite v1.1.4+ version.
 *
 * Transfers Module
 *
 * @author Softnio
 *
 * @version 1.0
 */

use App\Models\EmailTemplate;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

class TransfersModule
{
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->email_demo();
        $this->add_settings();
    }

    /** Emailing  @version 1.0.0 @since 1.0.0 */
    public function email($name)
    {
        $emails = [
            'sender' => 'transfer-token-sender',
            'sender-reject' => 'transfer-token-sender-reject',
            'sender-success' => 'transfer-token-sender-success',
            'receiver' => 'transfer-token-receiver',
            'admin' => 'transfer-token-admin',
        ];

        return isset($emails[$name]) ? $emails[$name] : 'default';
    }

    /** Template  @version 1.0.0 @since 1.0.0 */
    public function template($name = 'default')
    {
        $template = EmailTemplate::get_template($name);

        return $template;
    }

    /** Notify to Admin @version 1.0.0 @since 1.0.0 */
    public function notifyAdmins($tnx)
    {
        $admins = User::join('user_metas', 'users.id', '=', 'user_metas.userId')->where(['users.role' => 'admin', 'users.status' => 'active', 'user_metas.notify_admin' => 1])->select('users.*')->get();
        $to_all = get_setting('send_notification_to', 'all');
        $admin = (is_numeric($to_all) ? User::find($to_all) : null);
        if ($to_all == 'all') {
            $when = now()->addMinutes(2);
            try {
                Notification::send($admins, new TransfersNotification($tnx, $this->email('admin')));
            } catch (\Exception $e) {
                //info($e->getMessage());
            }
        } elseif ($admin) {
            $when = now()->addMinutes(2);
            try {
                $admin->notify((new TransfersNotification($tnx, $this->email('admin'))));
            } catch (\Exception $e) {
                //info($e->getMessage());
            }
        }
    }

    /** Email Demo Template @version 1.0.0 @since 1.0.0 */
    private function email_demo()
    {
        $data = [
            $this->email('sender') => [
                'name' => 'Token Transfer - Request (SENDER)',
                'slug' => $this->email('sender'),
                'subject' => 'Request for [[token_symbol]] transfer',
                'greeting' => 'Hello [[user_name]],',
                'message' => "We received your request to transfer [[total_tokens]] to your friend ([[receiver_name]]) account. We will review your request shortly and should be processed with 24-72 hours.\n\nNote: If you did not initiate this request, please contact us immediately.\n\nIf you have any questions, please feel free to contact us.\n",
                'regards' => 'true',
            ],
            $this->email('sender-reject') => [
                'name' => 'Token Transfer - Rejected (SENDER)',
                'slug' => $this->email('sender-reject'),
                'subject' => 'Your token transfer request has been rejected',
                'greeting' => 'Hello [[user_name]],',
                'message' => "We have received your request to transfer [[total_tokens]] to your friend account. Unfortunately, your request has been rejected.\n\nNote: Your requested amount added into your account.\n\nIf you have any questions, please feel free to contact us.\n",
                'regards' => 'true',
            ],
            $this->email('sender-success') => [
                'name' => 'Token Transfer - Approved (SENDER)',
                'slug' => $this->email('sender-success'),
                'subject' => 'Your token transfer request has been approved',
                'greeting' => 'Hello [[user_name]],',
                'message' => "We have successfully processed your transfer request and added [[total_tokens]] into your friend account.\n\nFeel free to contact us if you have any questions.\n",
                'regards' => 'true',
            ],
            $this->email('receiver') => [
                'name' => 'Token Transfer - Approved (RECEIVER)',
                'slug' => $this->email('receiver'),
                'subject' => 'You have been received [[token_symbol]] Token',
                'greeting' => 'Hello [[user_name]],',
                'message' => "You have received [[total_tokens]] from '[[sender_details]]'.\n\nYour token balance now appear in your account. Please login into your and check your balance.\n\nFeel free to contact us if you have any questions.\n",
                'regards' => 'true',
            ],
            $this->email('admin') => [
                'name' => 'Token Transfer - Notification (ADMIN)',
                'slug' => $this->email('admin'),
                'subject' => 'Token Transfer Request from [[user_name]]',
                'greeting' => 'Hello [[user_name]],',
                'message' => "You have received a token ([[total_tokens]]) transfer request form '[[sender_name]]' to '[[receiver_name]]' account.\n\nPlease login into account and check details of transaction and take necessary steps.\n\n\nPS. Do not reply to this email.\nThank you.\n",
                'regards' => 'false',
            ],
        ];

        foreach ($data as $key => $value) {
            $check = EmailTemplate::where('slug', $key)->count();
            if ($check <= 0) {
                EmailTemplate::create($value);
            }
        }
    }

    /** Add Defaults Settings @version 1.0.0 @since 1.0.0 */
    private function add_settings()
    {
        if (get_setting('transfer_enable', null) == null) {
            add_setting('transfer_enable', 0);
        }
        if (get_setting('transfer_minimum', null) == null) {
            add_setting('transfer_minimum', 1000);
        }
        if (get_setting('transfer_maximum', null) == null) {
            add_setting('transfer_maximum', 10000);
        }
        if (get_setting('transfer_notes', null) == null) {
            add_setting('transfer_notes', 'Before clicking the "Proceed" button, be sure that you want to send token. You can not cancel the request once you proceed.');
        }
        if (get_setting('transfer_status', null) == null) {
            add_setting('transfer_status', 0);
        }
        if (get_setting('transfer_status_message', null) == null) {
            add_setting('transfer_status_message', 'Sorry! right now you can not send token to your friend account. Please contact us if you have any question.');
        }
    }
}
