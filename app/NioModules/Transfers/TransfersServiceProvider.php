<?php

namespace App\NioModules\Transfers;

use Config;
use Illuminate\Support\ServiceProvider;

class TransfersServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //#
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(nio_module()->getPath('Transfers/views'), 'transfer');
        $this->loadRoutesFrom(nio_module()->getPath('Transfers/routes.php'));
        $this->registerConfig();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function registerConfig()
    {
        $is_enabled = gws('transfer_enable', 0);
        $get_status = gws('transfer_status', 0);
        Config::set([
            'transfer' => [
                'userpanel' => [
                    'enable' => $is_enabled,
                    'status' => $get_status,
                    'message' => gws('transfer_status_message'),
                    'title' => ___('Send Token to Your Friend'),
                    'desc' => ___('You can invite your friend to join our platform and also you can send your token to your friend using his email address.'),
                    'view' => ___('See Transactions'),
                    'view_route' => ('user.transactions'),
                    'view_class' => '',
                    'cta' => 'Send Token',
                    'cta_route' => '',
                    'cta_class' => css_class('user-modal-request'.(($get_status == 1) ? ' disabled' : '')),
                    'cta_daction' => 'send-token',
                    'cta_dtype' => 'modal',
                ],
                'settings' => [
                    'enable' => gws('transfer_enable', 0),
                    'minimum' => gws('transfer_minimum', 1000),
                    'maximum' => gws('transfer_maximum', 10000),
                    'notes' => gws('transfer_notes', 'Before clicking the "Proceed" button, be sure that you want to send token. You can not cancel the request once you proceed.'),
                    'status' => $get_status,
                    'message' => gws('transfer_status_message', 'Sorry! right now you can not send token to your friend account. Please contact us if you have any question.'),
                ],
                'view_setting' => 'transfer::settings',
            ],
        ]);
    }
}
