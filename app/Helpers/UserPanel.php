<?php
/**
 * UserPanel Helper
 *
 * This class for manage user panel data etc.
 *
 * @author Softnio
 *
 * @version 1.1.6
 */

namespace App\Helpers;

use App\Models\Language;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

/**
 * UserPanel Class
 */
class UserPanel
{
    /**
     * user_info()
     *
     * @version 1.3
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_info($data = null, $atttr = '')
    {
        $user = (empty($data)) ? auth()->user() : $data;
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $return = '<div'.$g_id.' class="user-dropdown-head'.$g_cls.'">
        <h6 class="user-dropdown-name">'.$user->name.'<span>('.set_id($user->id).')</span></h6>
        <span class="user-dropdown-email">'.$user->email.'</span>
        </div>

        <div class="user-status">
        <h6 class="user-status-title">'.__('Token Balance').'</h6>
        <div class="user-status-balance">'.number_format($user->tokenBalance, 2).' <small>'.token('symbol').'</small></div>
        </div>';

        return $return;
    }

    /**
     * user_balance()
     *
     * @version 1.3
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_balance($data = null, $atttr = '')
    {
        $user = (empty($data)) ? auth()->user() : $data;
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $return = '<div'.$g_id.' class="user-status'.$g_cls.'">
        <h6 class="text-white">'.$user->email.' <small class="text-white-50">('.set_id($user->id).')</small></h6>
        <h6 class="user-status-title">'.__('Token Balance').'</h6>
        <div class="user-status-balance">'.to_num_token($user->tokenBalance).' <small>'.token('symbol').'</small></div>
        </div>';

        return $return;
    }

    /**
     * user_balance_card()
     *
     * @version 1.3.1
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_balance_card($data = null, $atttr = '')
    {
        $user = auth()->user();
        $referral = Transaction::where([
            ['user', $user->id],
            ['tnx_type', 'joined'],
        ])->sum('tokens');
        $gift = Transaction::where([
            ['user', $user->id],
            ['tnx_type', 'gift'],
        ])->sum('tokens');
        $launch = Transaction::where('user', $user->id)->where('tnx_type', 'launch')->first();

        $tier1 = Transaction::where([
            ['user', $user->id],
            ['stage', 1],
            ['tnx_type', 'purchase'],
            ['status', 'approved'],
        ])->sum('total_tokens');

        $tier2 = Transaction::where([
            ['user', $user->id],
            ['stage', 2],
            ['tnx_type', 'purchase'],
            ['status', 'approved'],
        ])->sum('total_tokens');

        $tier3 = Transaction::where([
            ['user', $user->id],
            ['stage', 3],
            ['tnx_type', 'purchase'],
            ['status', 'approved'],
        ])->sum('total_tokens');
        // dd($tier2);
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $ver_cls = ($vers == 'side') ? ' token-balance-with-icon' : '';
        $ver_icon = ($vers == 'side') ? '<div class="token-balance-icon" style="background: rgba(255, 255, 255, 0);"><img loading="lazy" src="'.asset('images/token-symbol-light.png').'" alt=""></div>' : '';

        $base_cur = base_currency();
        $base_con = isset($data->$base_cur) ? to_num($data->$base_cur, 'auto') : 0;
        // $base_out = '<li class="token-balance-sub"><h6 class="card-sub-title">' . __('REFERRAL') . '</h6><span class="lead">' . ($base_con > 0 ? $base_con : '~') . '</span><span class="sub">' . strtoupper($base_cur) . '</span></li>';
        $base_out = '<li class="token-balance-sub" style="min-width: 25%;"><h6 class="card-sub-title">'.__('REFERRAL').'</h6><span class="lead">'.($referral > 0 ? $referral : '0').'</span><span class="sub">'.token('symbol').'</span></li>';

        $cur1_out = $cur2_out = '';
        if (gws('user_in_cur1', 'eth') != 'hide') {
            $cur1 = gws('user_in_cur1', 'eth');
            $cur1_con = (gws('pmc_active_'.$cur1) == 1) ? to_num($data->$cur1, 'auto') : 0;
            // $cur1_out = ($cur1 != $base_cur) ? '<li class="token-balance-sub"><h6 class="card-sub-title">' . __('BUY') . '</h6><span class="lead">' . ($cur1_con > 0 ? $cur1_con : '~') . '</span><span class="sub">' . strtoupper($cur1) . '</span></li>' : '';
            $cur1_out = ($cur1 != $base_cur) ? '<li class="token-balance-sub" style="min-width: 25%;"><h6 class="card-sub-title">'.__('BUY').'</h6><span class="lead">'.to_num_token($user->tokenBalance).'</span><span class="sub">'.token('symbol').'</span></li>' : '';
        }

        if (gws('user_in_cur2', 'btc') != 'hide') {
            $cur2 = gws('user_in_cur2', 'btc');
            $cur2_con = (gws('pmc_active_'.$cur2) == 1) ? to_num($data->$cur2, 'auto') : 0;
            // $cur2_out = ($cur2 != $base_cur) ? '<li class="token-balance-sub"><h6 class="card-sub-title">' . __('LOUNGE') . '</h6><span class="lead">' . ($cur2_con > 0 ? $cur2_con : '~') . '</span><span class="sub">' . strtoupper($cur2) . '</span></li>' : '';
            $cur2_out = '<li class="token-balance-sub" style="min-width: 25%;"><h6 class="card-sub-title">'.__('LAUNCH').'</h6><span class="lead">'.($launch != null ? $launch->tokens : '0').'</span><span class="sub">'.token('symbol').'</span></li>';
            $cur3_out = '<li class="token-balance-sub" style="min-width: 25%;"><h6 class="card-sub-title">'.__('GIFTS').'</h6><span class="lead">'.($gift > 0 ? $gift : '0').'</span><span class="sub">'.token('symbol').'</span></li>';
        }
        $token_balance_sum = $user->gift_bonus + $user->tokenBalance;
        $contribute = ($base_out || $cur1_out || $cur2_out) ? '<div class="token-balance token-balance-s2"><ul class="token-balance-list pl-0">'.$base_out.$cur1_out.$cur2_out.$cur3_out.'</ul></div>' : '';

        $return = '<div'.$g_id.' class="token-statistics card card-token'.$g_cls.'">
        <div class="card-innr"><div class="token-balance'.$ver_cls.'" style="align-items: flex-start;">'.$ver_icon.'
        <div class="token-balance-text" style="width:100%;"><h6 class="card-sub-title">'.__('Token Balance').'</h6>
        <span class="lead">'.$token_balance_sum.' <span>'.token('symbol').' <em class="fas fa-info-circle fs-11" data-toggle="tooltip" data-placement="right" title="'.__('Equivalent to').' '.token_price($token_balance_sum, base_currency()).' '.base_currency(true).'"></em></span></span>
        <div class="token-balance-sub" style="font-size: 11px;">
        <span class="sub">'.token_price($token_balance_sum, base_currency()).' '.base_currency(true).' </span>
        </div>
        </div>
        <div class="token-balance-text" style="text-align: right;">
            <div class="dropdown">
                <button class="btn waves-effect p-0 " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <h6 class="card-sub-title">'.__('TIERS').'<i class="mdi mdi-chevron-down d-xl-inline-block"></i></h6>
                </button>
                <div class="dropdown-menu dropdown-menu-end" style="border-radius:20px; min-width: max-content; font-size: 0.1rem; border: 1px solid rgb(255 255 255); padding: 0.2rem 0;" aria-labelledby="dropdownMenuButton">
                    <ul class="pl-3 pr-3 m-0 text-center" >
                        <li class="token-balance-sub"><h6 class="card-sub-title mb-0" style="font-size: 13px;">'.__('TIER 1').'</h6><span class="lead" style="color: #fff; font-size: 13px;">'.($tier1).'</span><span class="sub" style="font-size: 9px; color:#fff; display: flex; justify-content: center;"> '.token('symbol').'</span></li>
                        <li class="token-balance-sub"><h6 class="card-sub-title mb-0" style="font-size: 13px;">'.__('TIER 2').'</h6><span class="lead" style="color: #fff; font-size: 13px;">'.($tier2).'</span><span class="sub" style="font-size: 9px; color:#fff; display: flex; justify-content: center;"> '.token('symbol').'</span></li>
                        <li class="token-balance-sub"><h6 class="card-sub-title mb-0" style="font-size: 13px;">'.__('TIER 3').'</h6><span class="lead" style="color: #fff; font-size: 13px;">'.($tier3).'</span><span class="sub" style="font-size: 9px; color:#fff; display: flex; justify-content: center;"> '.token('symbol').'</span></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>'.$contribute.'</div></div>';

        return $return;
    }

    /**
     * user_token_block()
     *
     * @version 1.2
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_token_block($data = '', $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';
        $_CUR = base_currency(true);
        $_SYM = token_symbol();
        $base_currency = base_currency();
        $token_1price = token_calc(1, 'price')->$base_currency;
        $token_1rate = token_rate(1, token('default_in_userpanel', 'eth'));
        $token_ratec = token('default_in_userpanel', 'ETH');

        $card1 = '<div class="token-info text-center">
        <img loading="lazy" class="token-info-icon" src="'.asset('images/token-symbol.png').'" alt="">
        <div class="gaps-2x"></div>
        <h3 class="token-info-head text-light">1 '.$_SYM.' = '.to_num($token_1price, 'max', ',', true).' '.$_CUR.'
        </h3>
        <h5 class="token-info-sub">1 '.$_CUR.' = '.to_num($token_1rate, 'max', ',', true).' '.$token_ratec.'</h5>
        </div>';
        $card2 = '<div class="token-info bdr-tl">
        <div>
        <ul class="token-info-list">
        <li><span>'.__('Token Name').':</span>'.token('name').'</li>
        <li><span>'.__('Token Symbol').':</span>'.$_SYM.'</li>
        </ul>';
        $card2 .= (get_setting('site_white_paper') != '' ? '<a href="'.route('public.white.paper').'" target="_blank" class="btn btn-primary"><em class="fas fa-download mr-3"></em>'.__('Download Whitepaper').'</a>' : '');
        $card2 .= '</div>
        </div>';

        $return = '';
        $status = ucfirst(active_stage_status());
        if ($vers == 'buy') {
            $return .= '<div class="card card-full-height"><div class="card-innr">';
            $return .= '<h6 class="card-title card-title-sm">'.active_stage()->name.'<span class="badge badge-success ucap">'.__($status).'</span></h6>';
            $return .= '<h3 class="text-dark">1 '.$_SYM.' = '.to_num($token_1price, 'max', ',', true).' '.$_CUR.' <span class="d-block text-exlight ucap fs-12">1 '.$_CUR.' = '.to_num($token_1rate, 'max', ',', true).' '.$token_ratec.'</span></h3>';
            $return .= '<div class="gaps-0-5x"></div><div class="d-flex align-items-center justify-content-between mb-0"><a href="'.route('user.token').'" class="btn btn-md btn-primary">'.__('Buy Token Now').'</a></div>';

            $return .= '</div></div>';
        } else {
            $return .= '<div'.$g_id.' class="token-information card card-full-height'.$g_cls.'">';
            if ($vers == 'prices') {
                $return .= $card1;
            } elseif ($vers == 'info') {
                $return .= $card2;
            } else {
                $return .= '<div class="row no-gutters height-100">
                <div class="col-md-6">'.$card1.'</div>
                <div class="col-md-6">'.$card2.'</div>
                </div>';
            }
            $return .= '</div>';
        }

        return $return;
    }

    /**
     * add_wallet_alert()
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public static function add_wallet_alert()
    {
        return '<a href="javascript:void(0)" class="btn btn-danger btn-xl btn-between w-100 mgb-1-5x user-wallet">'.__('Add your wallet address before buy').' <em class="ti ti-arrow-right"></em></a>
        <div class="gaps-1x mgb-0-5x d-lg-none d-none d-sm-block"></div>';
    }

    /**
     * user_account_status()
     *
     * @version 1.1
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_kyc_info_usuario($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $user = auth()->user();
        $title_cls = ' card-title-sm';

        $heading = '<h6 class="card-title'.$title_cls.'">'.__('Identity Verification - KYC').'</h6>';
        $ukyc = $heading.'<p>'.__('To comply with regulation, participant will have to go through identity verification.').'</p>';
        if (! isset($user->kyc_info->status)) {
            $ukyc .= '<p class="lead text-dark pdb-0-5x">'.__('You have not submitted your documents to verify your identity (KYC).').'</p><a href="'.route('user.kyc.application').'" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Click to Proceed').'</a>';
        }
        if (isset($user->kyc_info->status) && $user->kyc_info->status == 'pending') {
            $ukyc .= '<p class="lead text-info pdb-0-5x">'.__('We have received your document.').'</p><p class="small">'.__('We will review your information and if all is in order will approve your identity. You will be notified by email once we verified your identity (KYC).').'</p>';
        }
        if (isset($user->kyc_info->status) && ($user->kyc_info->status == 'rejected' || $user->kyc_info->status == 'missing')) {
            $ukyc .= '<p class="lead text-danger pdb-0-5x">'.__('KYC Application has been rejected!').'</p><p>'.__('We were having difficulties verifying your identity. In our verification process, we found information are incorrect or missing. Please re-submit the application again and verify your identity.').'</p><a href="'.route('user.kyc.application').'?state=resubmit" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Resubmit').'</a><a href="'.route('user.kyc.application.view').'" class="btn btn-sm m-2 btn-icon btn-secondary">'.__('View KYC').'</a>';
        }
        if (isset($user->kyc_info->status) && $user->kyc_info->status == 'approved') {
            $ukyc .= '<p class="lead text-success pdb-0-5x"><strong>'.__('Identity (KYC) has been verified.').'</strong></p><p>'.__('One for our team verified your identity. You are eligible to participate in our token sale.').'</p><a href="'.route('user.token').'" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Purchase Token').'</a><a href="'.route('user.kyc.application.view').'" class="btn btn-sm m-2 btn-icon btn-success">'.__('View KYC').'</a>';
        }
        if (token('before_kyc') == '1') {
            $ukyc .= '<h6 class="kyc-alert text-danger">* '.__('KYC verification required for purchase token').'</h6>';
        }

        $return = ($ukyc) ? '<div'.$g_id.' class="kyc-info card'.$g_cls.'"><div class="card-innr">'.$ukyc.'</div></div>' : '';

        return $return;
    }

    public static function user_account_status_usuario($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $user = auth()->user();
        $heading = '<h6 class="card-title card-title-sm">'.__('Your Account Status').'</h6><div class="gaps-1-5x"></div>';
        $email_status = $kyc_staus = '';
        if ($user->email_verified_at == null) {
            $email_status = '<li class="pl-0"><a href="'.route('verify.resend').'" class="btn btn-xs btn-auto btn-info">'.__('Resend Email').'</a></li>';
        } else {
            $email_status = '<li class="pl-0"><a href="javascript:void(0)" class="btn btn-xs btn-auto btn-success">'.__('Email Verified').'</a></li>';
        }
        if (! is_kyc_hide()) {
            if (isset($user->kyc_info->status) && $user->kyc_info->status == 'approved') {
                $kyc_staus = '<li><a href="javascript:void(0)" class="btn btn-xs btn-auto btn-success">'.__('KYC Approved').'</a></li>';
            } elseif (isset($user->kyc_info->status) && $user->kyc_info->status == 'pending') {
                $kyc_staus = '<li><a href="'.route('user.kyc').'" class="btn btn-xs btn-auto btn-warning">'.__('KYC Pending').'</a></li>';
            } else {
                $kyc_staus = '<li><a href="'.route('user.kyc').'" class="btn btn-xs btn-auto btn-info"><span>'.__('Submit KYC').'</span></a></li>';
            }
        }
        $return = ($email_status || $kyc_staus) ? '<div'.$g_id.' class="user-account-status'.$g_cls.'">'.$heading.'<ul class="btn-grp pl-0">'.$email_status.$kyc_staus.'</ul></div>' : '';

        return $return;
    }

    public static function user_account_status($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $user = auth()->user();
        $heading = '<h6 class="card-title card-title-sm">'.__('Your Account Status').'</h6><div class="gaps-1-5x"></div>';
        $email_status = $kyc_staus = '';
        if ($user->email_verified_at == null) {
            $email_status = '<li><a href="'.route('verify.resend').'" class="btn btn-xs btn-auto btn-info">'.__('Resend Email').'</a></li>';
        } else {
            $email_status = '<li><a href="javascript:void(0)" class="btn btn-xs btn-auto btn-success">'.__('Email Verified').'</a></li>';
        }
        if (! is_kyc_hide()) {
            if (isset($user->kyc_info->status) && $user->kyc_info->status == 'approved') {
                $kyc_staus = '<li><a href="javascript:void(0)" class="btn btn-xs btn-auto btn-success">'.__('KYC Approved').'</a></li>';
            } elseif (isset($user->kyc_info->status) && $user->kyc_info->status == 'pending') {
                $kyc_staus = '<li><a href="'.route('user.kyc').'" class="btn btn-xs btn-auto btn-warning">'.__('KYC Pending').'</a></li>';
            } else {
                $kyc_staus = '<li><a href="'.route('user.kyc').'" class="btn btn-xs btn-auto btn-info"><span>'.__('Submit KYC').'</span></a></li>';
            }
        }
        $return = ($email_status || $kyc_staus) ? '<div'.$g_id.' class="user-account-status'.$g_cls.'">'.$heading.'<ul class="btn-grp">'.$email_status.$kyc_staus.'</ul></div>' : '';

        return $return;
        // $heading = '<h6 class="card-title card-title-sm">' . __('Your bond wallet') . '</h6><div class="gaps-1-5x"></div>';
        // $base_cur =  base_currency();
        // $bono = '55'.' '.$base_cur;

        // $return = '<div' . $g_id . ' class="' . $g_cls . '">' . $heading . '&nbsp;&nbsp;<h3 class="btn-grp">' . strtoupper($bono) . '</h3></div>';
        // return $return;
    }

    /**
     * user_account_wallet()
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_account_wallet($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $user = auth()->user();
        $title_cls = ' card-title-sm';
        $btn_cls = ' link link-ucap';

        $uwallet = '<h6 class="card-title'.$title_cls.'">'.__('Receiving Wallet').'</h6><div class="gaps-1x"></div>';
        $uwallet .= '<div class="d-flex justify-content-between">';
        if ($user->walletAddress) {
            $uwallet .= '<span>'.show_str($user->walletAddress, 8).' ';
            if ($user->wallet() == 'pending') {
                $uwallet .= ' <em title="'.__('New address under review for approve.').'" data-toggle="tooltip" class="fas fa-info-circle text-warning"></em></span>';
            }
        } else {
            $uwallet .= __('Add Your Wallet Address');
        }
        $uwallet .= '<a href="javascript:void(0)" data-toggle="modal" data-target="#edit-wallet" class="modal-wallet '.$btn_cls.'">'.($user->walletAddress != null ? __('Edit') : __('Add')).'</a></div>';
        $support_token_wallet = (empty(token_wallet()) ? false : true);
        $return = ($uwallet && $support_token_wallet) ? '<div'.$g_id.' class="user-receive-wallet'.$g_cls.'">'.$uwallet.'</div>' : '';

        return $return;
    }

    /**
     * user_kyc_info()
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_kyc_info($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $user = auth()->user();
        $title_cls = ' card-title-sm';

        $heading = '<h6 class="card-title'.$title_cls.'">'.__('Identity Verification - KYC').'</h6>';
        $ukyc = $heading.'<p>'.__('To comply with regulation, participant will have to go through identity verification.').'</p>';
        if (! isset($user->kyc_info->status)) {
            $ukyc .= '<p class="lead text-light pdb-0-5x">'.__('You have not submitted your documents to verify your identity (KYC).').'</p><a href="'.route('user.kyc.application').'" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Click to Proceed').'</a>';
        }
        if (isset($user->kyc_info->status) && $user->kyc_info->status == 'pending') {
            $ukyc .= '<p class="lead text-info pdb-0-5x">'.__('We have received your document.').'</p><p class="small">'.__('We will review your information and if all is in order will approve your identity. You will be notified by email once we verified your identity (KYC).').'</p>';
        }
        if (isset($user->kyc_info->status) && ($user->kyc_info->status == 'rejected' || $user->kyc_info->status == 'missing')) {
            $ukyc .= '<p class="lead text-danger pdb-0-5x">'.__('KYC Application has been rejected!').'</p><p>'.__('We were having difficulties verifying your identity. In our verification process, we found information are incorrect or missing. Please re-submit the application again and verify your identity.').'</p><a href="'.route('user.kyc.application').'?state=resubmit" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Resubmit').'</a><a href="'.route('user.kyc.application.view').'" class="btn btn-sm m-2 btn-icon btn-secondary">'.__('View KYC').'</a>';
        }
        if (isset($user->kyc_info->status) && $user->kyc_info->status == 'approved') {
            $ukyc .= '<p class="lead text-success pdb-0-5x"><strong>'.__('Identity (KYC) has been verified.').'</strong></p><p>'.__('One for our team verified your identity. You are eligible to participate in our token sale.').'</p><a href="'.route('user.token').'" class="btn btn-sm m-2 btn-icon btn-primary">'.__('Purchase Token').'</a><a href="'.route('user.kyc.application.view').'" class="btn btn-sm m-2 btn-icon btn-success">'.__('View KYC').'</a>';
        }
        if (token('before_kyc') == '1') {
            $ukyc .= '<h6 class="kyc-alert text-danger">* '.__('KYC verification required for purchase token').'</h6>';
        }

        $return = ($ukyc) ? '<div'.$g_id.' class="kyc-info card'.$g_cls.'"><div class="card-innr">'.$ukyc.'</div></div>' : '';

        return $return;
    }

    /**
     * user_logout_link()
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_logout_link($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $return = '<ul'.$g_id.' class="user-links bg-light'.$g_cls.'">
        <li><a href="'.route('log-out').'" onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();"><i class="ti ti-power-off"></i>'.__('Logout').'</a></li>
        </ul>
        <form id="logout-form" action="'.route('logout').'" method="POST" style="display: none;"> <input type="hidden" name="_token" value="'.csrf_token().'"> </form>';

        return $return;
    }

    /**
     * user_menu_links()
     *
     * @version 1.2
     *
     * @since 1.0
     *
     * @return void
     */
    public static function user_menu_links($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        // v1.0.3 > v1.1.1
        $referral_link = (get_page('referral', 'status') == 'active' && is_active_referral_system()) ? '<li><a href="'.route('user.referral').'"><i class="ti ti-infinite"></i>'.get_page('referral', 'menu_title').'</a></li>' : '';
        // v1.1.2
        $withdraw_link = (nio_module()->has('Withdraw') && gws('withdraw_enable', 0) == 1 && has_route('withdraw:user.index')) ? '<li><a href="'.route('withdraw:user.index').'"><i class="ti ti-wallet"></i>'.__('Withdraw').'</a></li>' : '';
        $return = '<ul'.$g_id.' class="user-links'.$g_cls.'"><li><a href="'.route('user.account', 2).'"><i class="ti ti-id-badge"></i>'.__('My Profile').'</a></li>'.$withdraw_link.$referral_link;
        $return .= '<li><a href="'.route('user.account.activity').'"><i class="ti ti-eye"></i>'.__('Activity').'</a></li>';
        $return .= '</ul>';

        return $return;
    }

    /**
     * kyc_footer_info()
     *
     * @version 1.0.0
     *
     * @since 1.0
     *
     * @return void
     */
    public static function kyc_footer_info($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $email = (get_setting('site_support_email', get_setting('site_email'))) ? ' <a href="mailto:'.get_setting('site_support_email', get_setting('site_email')).'">'.get_setting('site_support_email', get_setting('site_email')).'</a>' : '';
        $gaps = '<div class="gaps-3x d-none d-sm-block"></div>';

        $return = ($email) ? '<p class="text-light text-center">'.(__('Contact our support team via email')).' - '.$email.'</p><div class="gaps-1x"></div>'.$gaps : '';

        return $return;
    }

    /**
     * language_switcher()
     *
     * @version 1.0.1
     *
     * @since 1.0.2
     *
     * @return string
     */
    public static function language_switcher()
    {
        $l = str_replace('_', '-', current_lang());

        $text = '<div class="lang-switch relative"><a href="javascript:void(0)" class="lang-switch-btn toggle-tigger">'.strtoupper($l).'<em class="ti ti-angle-up"></em></a>';
        $text .= '<div class="toggle-class dropdown-content dropdown-content-up"><ul class="lang-list">';
        foreach (config('icoapp.supported_languages') as $lng) {
            $text .= '<li><a href="'.route('language').'?lang='.$lng.'">'.get_lang($lng).'</a></li>';
        }
        $text .= '</ul></div></div>';

        return (is_lang_switch()) ? $text : '';
    }

    /**
     * social_links()
     *
     * @version 1.0.2
     *
     * @since 1.0
     *
     * @return void
     */
    public static function social_links($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $link = json_decode(get_setting('site_social_links'));

        $fb = (isset($link->facebook) && $link->facebook != null) ? '<li><a href="'.$link->facebook.'"><em class="fab fa-facebook-f"></em></a></li>' : '';
        $tw = (isset($link->twitter) && $link->twitter != null) ? '<li><a href="'.$link->twitter.'""><em class="fab fa-twitter"></em></a></li>' : '';
        $in = (isset($link->linkedin) && $link->linkedin != null) ? '<li><a href="'.$link->linkedin.'"><em class="fab fa-linkedin-in"></em></a></li>' : '';
        $gh = (isset($link->github) && $link->github != null) ? '<li><a href="'.$link->github.'"><em class="fab fa-github-alt"></em></a></li>' : '';

        $yt = (isset($link->youtube) && $link->youtube != null) ? '<li><a href="'.$link->youtube.'"><em class="fab fa-youtube"></em></a></li>' : '';
        $md = (isset($link->medium) && $link->medium != null) ? '<li><a href="'.$link->medium.'"><em class="fab fa-medium-m"></em></a></li>' : '';
        $tg = (isset($link->telegram) && $link->telegram != null) ? '<li><a href="'.$link->telegram.'"><em class="fab fa-telegram-plane"></em></a></li>' : '';

        $social_exist = ($fb || $tw || $in || $gh || $yt || $md || $tg) ? true : false;
        $return = ($social_exist) ? '<ul'.$g_id.' class="socials'.$g_cls.'">'.$fb.$tw.$in.$gh.$yt.$md.$tg.'</ul>' : '';

        return ($data == 'exists') ? $social_exist : $return;
    }

    /**
     * footer_links()
     *
     * @version 1.0.2
     *
     * @since 1.0
     *
     * @return void
     */
    public static function footer_links($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $how_to = (get_page('how_buy', 'status') == 'active') ? '<li><a href="'.route('public.pages', get_slug('how_buy')).'">'.get_page('how_buy', 'menu_title').'</a></li>' : '';
        $cs_page = (get_page('custom_page', 'status') == 'active') ? '<li><a href="'.route('public.pages', get_slug('custom_page')).'">'.get_page('custom_page', 'menu_title').'</a></li>' : '';
        $faqs = (get_page('faq', 'status') == 'active') ? '<li><a href="'.route('public.pages', get_slug('faq')).'">'.get_page('faq', 'menu_title').'</a></li>' : '';
        if (! auth()->check() || is_2fa_lock()) {
            $how_to = $faqs = $cs_page = '';
        }
        // $privacy = (get_page('privacy', 'status') == 'active') ? '<li><a href="' . route('public.pages', get_slug('privacy')) . '">' . get_page('privacy', 'menu_title') . '</a></li>' : '';
        // $terms = (get_page('terms', 'status') == 'active') ? '<li><a href="' . route('public.pages', get_slug('terms')) . '">' . get_page('terms', 'menu_title') . '</a></li>' : '';

        $is_copyright = ((isset($data['copyright']) && $data['copyright'] == true) || $vers == 'copyright') ? true : false;
        $copyrights = ($is_copyright) ? '<li>&copy; '.date('Y').' Gosen. All Right Reserved.</li>' : '';

        $is_lang = ((isset($data['lang']) && $data['lang'] == true) && is_lang_switch()) ? true : false;
        $lang = ($is_lang) ? '<li>'.Userpanel::language_switcher().'</li>' : '';

        // $return = ($privacy || $terms) ? '<ul' . $g_id . ' class="footer-links' . $g_cls . '">' . $cs_page . $how_to . $faqs . $privacy . $terms . $copyrights . $lang . '</ul>' : '';

        $return = '<ul'.$g_id.' class="footer-links'.$g_cls.'">'.$cs_page.$how_to.$faqs.$copyrights.$lang.'</ul>';

        return ! is_maintenance() ? $return : '';
    }

    /**
     * copyrights()
     *
     * @version 1.0.1
     *
     * @since 1.0.2
     *
     * @return void
     */
    public static function copyrights($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $copyrights = ($data == 'div') ? '<div'.$g_id.' class="copyright-text'.$g_cls.'">'.site_copyrights().'</div>' : site_copyrights();

        $return = $copyrights;

        return $return;
    }

    /**
     * content_block()
     *
     * @version 1.1
     *
     * @since 1.0
     *
     * @return void
     */
    public static function content_block($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $return = '';
        $img_url = (isset($image) && $image != '') ? asset('images/'.$image) : '';
        if ($data == 'welcome') {
            $return .= '<div'.$g_id.' class="card content-welcome-block'.$g_cls.'"><div class="card-innr">';
            $return .= ($img_url) ? '<div class="row guttar-vr-20px">' : '';

            if ($img_url) {
                $return .= '<div class="col-sm-5 col-md-4"><div class="card-image card-image-sm"><img loading="lazy" width="240" src="'.$img_url.'" alt=""></div></div><div class="col-sm-7 col-md-8">';
            }
            $return .= '<div class="card-content">';
            $return .= '<h4>'.__(get_page('home_top', 'title')).'</h4>';
            $return .= __(get_page('home_top', 'description'));
            $return .= '</div>';

            $return .= ($img_url) ? '</div></div>' : '';
            $return .= '<div class="d-block d-md-none gaps-0-5x mb-0"></div></div></div>';
        }

        if ($data == 'bottom') {
            $return = '<div'.$g_id.' class="content-bottom-block card'.$g_cls.'"><div class="card-innr"><div class="table-responsive">'.get_page('home_bottom', 'description').'</div></div></div>';
        }

        return $return;
    }

    /**
     * token_sales_progress()
     *
     * @version 1.2
     *
     * @since 1.0
     *
     * @return void
     */
    public static function token_sales_progress($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';

        $sales_raised = (token('sales_raised')) ? token('sales_raised') : 'token';
        $sales_total = (token('sales_total')) ? token('sales_total') : 'token';
        $sales_caps = (token('sales_cap')) ? token('sales_cap') : 'token';
        $title = $progress = $progress_bar = $sales_end_in = $sales_start_in = '';

        $title .= '<div class="card-head"><h5 class="card-title card-title-sm">'.__('Token Sales Progress').'</h5></div>';

        $progress .= '<ul class="progress-info"><li><span>'.__('Raised Amount').' <br></span>'.ico_stage_progress('raised', $sales_raised).'</li><li><span>'.__('Total Token').' <br></span>'.ico_stage_progress('total', $sales_total).'</li></ul>';

        $no_class = ((active_stage()->hard_cap < 10) && (active_stage()->soft_cap < 10)) ? ' no-had-soft' : '';

        $progress_bar = '<div class="progress-bar'.$no_class.'">';
        if (active_stage()->hard_cap >= 10) {
            $progress_bar .= '<div class="progress-hcap" data-percent="'.ico_stage_progress('hard').'"><div>'.__('Hard Cap').' <span>'.ico_stage_progress('hardtoken', $sales_caps).'</span></div></div>';
        }
        if (active_stage()->soft_cap >= 10) {
            $progress_bar .= '<div class="progress-scap" data-percent="'.ico_stage_progress('soft').'"><div>'.__('Soft Cap').' <span>'.ico_stage_progress('softtoken', $sales_caps).'</span></div></div>';
        }
        $percent = (sale_percent(active_stage()) > 100) ? 100 : sale_percent(active_stage());
        $progress_bar .= '<div class="progress-percent" data-percent = "'.$percent.'"></div></div>';

        $sales_state = '';
        if (! hide_opt_count()) {
            $starts = Carbon::parse(active_stage()->start_date, gws('site_timezone', 'UTC'));
            $ends = Carbon::parse(active_stage()->end_date, gws('site_timezone', 'UTC'));

            $sales_end_in .= '<span class="card-sub-title ucap mgb-0-5x">'.__('Sales End in').'</span><div class="countdown-clock" data-date="'.$ends->getTimestampMs().'"></div>';
            $sales_start_in .= '<span class="card-sub-title ucap mgb-0-5x">'.__('Sales Start in').'</span><div class="countdown-clock" data-date="'.$starts->getTimestampMs().'"></div>';
            $sales_state = (is_upcoming() ? $sales_start_in : $sales_end_in);
        }

        // If expaired or Completed
        if (is_expired() || is_completed()) {
            $sales_state = '<div class="gaps-2x"></div><h4 class="text-light text-center">'.__('Our token sales has been finished. Thank you very much for your contribution.').'</h4>';
        }

        $return = '<div'.$g_id.' class="card card-sales-progress'.$g_cls.'"><div class="card-innr">'.$title.$progress.$progress_bar.$sales_state.'</div></div>';

        return $return;
    }

    /**
     * user_referral_info()
     *
     * @version 1.0.0
     *
     * @since 1.0.3
     *
     * @return void
     */
    public static function user_referral_info($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';
        $auth = auth();
        $refers = $heading = '';
        $user = (empty($data)) ? auth()->user() : $data;
        $idioma = $user->lang;
        $ref_url = route('public.referral').'?ref='.substr(set_id(auth()->user()->username), 2).'&lang='.$idioma;
        $ref_page = route('user.referral', get_slug('referral'));
        $more = (isset($data['more']) && $data['more'] == 'hide') ? '' : '<div class="card-opt"><a href="'.$ref_page.'" class="link ucap">'.__('More').'<em class="fas fa-angle-right ml-1"></em></a></div>';
        $heading .= '<div class="card-head has-aside"><h6 class="card-title card-title-sm">'.__('Earn with Referral').'</h6>'.$more.'</div>';
        $refers .= '<p class="pdb-0-5x"><strong>'.__('Invite your friends & family.').'</strong></p>';
        $refers .= '<div class="copy-wrap mgb-0-5x"><span class="copy-feedback"></span><em class="copy-icon fas fa-link"></em><input type="text" class="copy-address" value="'.___('Copy your referral URL').'" disabled /><button class="copy-trigger copy-clipboard" data-clipboard-text="'.$ref_url.'"><em class="ti ti-files"></em></button></div>';

        $return = ($refers) ? '<div'.$g_id.' class="referral-info card'.$g_cls.'"><div class="card-innr">'.$heading.$refers.'</div></div>' : '';

        return get_page('referral', 'status') == 'active' ? $return : '';
    }

    /*----------------------------------------------------*/

    public static function token_revutoken($data = null, $atttr = '')
    {
        $Local_MaxNivel = 6;
        $user = (empty($data)) ? auth()->user() : $data;
        $timeline_nameuser = $user->name;
        $img_url = asset('images/check.gif');
        $img2_url = asset('images/loader.gif');

        // $timeline_giftEarned = Regalo_GetTotalTokenUser($user->id);
        $timeline_giftEarned = $user->gift_bonus;
        if (empty($timeline_giftEarned)) {
            $timeline_giftEarned = 0;
        }

        $RegaloMax = Regalo_NivelCurrent($user->id);
        if (empty($RegaloMax)) {
            $RegaloMax = 0;
        }

        $symbol = 'Token New';
        $symbol1 = 'Token';

        $timeline_ReqReferral = 0;
        $timeline_NeedPack = '';
        $dtRegalos = RegaloType_GetAll();
        $PackName1 = 'N/A';
        $PackValue1 = 'N/A';
        $PackName2 = 'N/A';
        $PackValue2 = 'N/A';
        $PackName3 = 'N/A';
        $PackValue3 = 'N/A';
        $PackName4 = 'N/A';
        $PackValue4 = 'N/A';
        $PackName5 = 'N/A';
        $PackValue5 = 'N/A';
        $PackName6 = 'N/A';
        $PackValue6 = 'N/A';
        foreach ($dtRegalos as $row) {
            if ($row->id == 1) {
                $PackName1 = $row->name;
                $PackValue1 = $row->total_tokens.' '.$symbol1;
            } elseif ($row->id == 2) {
                $PackName2 = $row->name;
                $PackValue2 = $row->total_tokens.' '.$symbol1;
            } elseif ($row->id == 3) {
                $PackName3 = $row->name;
                $PackValue3 = $row->total_tokens.' '.$symbol1;
            } elseif ($row->id == 4) {
                $PackName4 = $row->name;
                $PackValue4 = $row->total_tokens.' '.$symbol1;
            } elseif ($row->id == 5) {
                $PackName5 = $row->name;
                $PackValue5 = $row->total_tokens.' '.$symbol1;
            } elseif ($row->id == 6) {
                $PackName6 = $row->name;
                $PackValue6 = $row->total_tokens.' '.$symbol1;
            }
            if ($RegaloMax < $Local_MaxNivel) {
                if (($RegaloMax + 1) == $row->id) {
                    $timeline_ReqReferral = $row->total_guests;
                    $timeline_NeedPack = $row->name;
                }
            }
        }

        if ($RegaloMax <= $Local_MaxNivel) {
            $timeline_CurrentReferral = RegaloType_User_IsReferralCount($user->id);
            $timeline_NeedReferral = $timeline_ReqReferral - $timeline_CurrentReferral;
        }

        $dtvalue_onelist = '';
        $dtvalue_onetop = "class='ps-top'";
        $dtvalue_onebot = "class='ps-bot'";
        $dtvalue_seclist = '';
        $dtvalue_sectop = "class='ps-top'";
        $dtvalue_secbot = "class='ps-bot'";
        $dtvalue_thilist = '';
        $dtvalue_thitop = "class='ps-top'";
        $dtvalue_thibot = "class='ps-bot'";
        $dtvalue_forlist = '';
        $dtvalue_fortop = "class='ps-top'";
        $dtvalue_forbot = "class='ps-bot'";
        $dtvalue_fivlist = '';
        $dtvalue_fivtop = "class='ps-top'";
        $dtvalue_fivbot = "class='ps-bot'";
        $dtvalue_sixlist = '';
        $dtvalue_sixtop = "class='ps-top divs'";
        $dtvalue_sixbot = "class='ps-bot divs'";

        if (auth()->user()->theme_color == 'style-black') {
            $timelinePoint1 = '#000000';
            $timelinePoint2 = '#000000';
        } else {
            $timelinePoint1 = '#d6d7d7';
            $timelinePoint2 = '#d6d7d7';
        }

        if ($RegaloMax >= 1) {
            $dtvalue_onelist = "class='passed'";
            $dtvalue_onetop = "class='ps-top-passed'";
            $dtvalue_onebot = "class='ps-bot-passed'";
            if (auth()->user()->theme_color != 'style') {
                if (auth()->user()->theme_color == 'style-black') {
                    $timelinePoint1 = '#fff';
                } else {
                    $timelinePoint1 = asset(theme_color_user(auth()->user()->theme_color, 'base'));
                }
            } else {
                $timelinePoint1 = '#3e5880';
            }
        }
        if ($RegaloMax >= 2) {
            $dtvalue_seclist = "class='passed'";
            $dtvalue_sectop = "class='ps-top-passed'";
            $dtvalue_secbot = "class='ps-bot-passed'";
        }
        if ($RegaloMax >= 3) {
            $dtvalue_thilist = "class='passed'";
            $dtvalue_thitop = "class='ps-top-passed'";
            $dtvalue_thibot = "class='ps-bot-passed'";
        }
        if ($RegaloMax >= 4) {
            $dtvalue_forlist = "class='passed'";
            $dtvalue_fortop = "class='ps-top-passed'";
            $dtvalue_forbot = "class='ps-bot-passed'";
        }
        if ($RegaloMax >= 5) {
            $dtvalue_fivlist = "class='passed lis5'";
            $dtvalue_fivtop = "class='ps-top-passed'";
            $dtvalue_fivbot = "class='ps-bot-passed'";
        }
        if ($RegaloMax >= 6) {
            $dtvalue_sixlist = "class='passed lis'";
            $dtvalue_sixtop = "class='ps-top-passed divs'";
            $dtvalue_sixbot = "class='ps-bot-passed divs'";
            if (auth()->user()->theme_color != 'style') {
                if (auth()->user()->theme_color == 'style-black') {
                    $timelinePoint1 = '#fff';
                } else {
                    $timelinePoint1 = asset(theme_color_user(auth()->user()->theme_color, 'base'));
                }
            } else {
                $timelinePoint2 = '#3e5880';
            }
        }

        for ($i = 1; $i <= $Local_MaxNivel; $i++) {
            if ($i == 1) {
                if ($dtvalue_onelist == "class='passed'") {
                    $strPack = "<li $dtvalue_onelist>";
                    $strPack .= "<div $dtvalue_onetop><p>$PackName1 <i class='fas fa-check'></i></p></div>";
                    $strPack .= "<div $dtvalue_onebot><p>$PackValue1</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check' src='$img_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack = "<li $dtvalue_onelist>";
                    $strPack .= "<div $dtvalue_onetop><p>$PackName1 <i class='fas fa-check'></i></p></div>";
                    $strPack .= "<div $dtvalue_onebot><p>$PackValue1</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader' src='$img2_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                }
            } elseif ($i == 2) {
                if ($dtvalue_seclist == "class='passed'") {
                    $strPack .= "<li $dtvalue_seclist>";
                    $strPack .= "<div $dtvalue_sectop><p>$PackName2 <i class='fas fa-award'></i></p></div>";
                    $strPack .= "<div $dtvalue_secbot ><p>$PackValue2</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check' src='$img_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack .= "<li $dtvalue_seclist>";
                    $strPack .= "<div $dtvalue_sectop><p>$PackName2 <i class='fas fa-award'></i></p></div>";
                    $strPack .= "<div $dtvalue_secbot ><p>$PackValue2</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader' src='$img2_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                }
            } elseif ($i == 3) {
                if ($dtvalue_thilist == "class='passed'") {
                    $strPack .= "<li $dtvalue_thilist>";
                    $strPack .= "<div $dtvalue_thitop><p>$PackName3 <i class='fas fa-star'></i></p></div>";
                    $strPack .= "<div $dtvalue_thibot><p>$PackValue3</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check' src='$img_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack .= "<li $dtvalue_thilist>";
                    $strPack .= "<div $dtvalue_thitop><p>$PackName3 <i class='fas fa-star'></i></p></div>";
                    $strPack .= "<div $dtvalue_thibot><p>$PackValue3</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader' src='$img2_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                }
            } elseif ($i == 4) {
                if ($dtvalue_forlist == "class='passed'") {
                    $strPack .= "<li $dtvalue_forlist>";
                    $strPack .= "<div $dtvalue_fortop><p>$PackName4 <i class='fas fa-medal'></i></p></div>";
                    $strPack .= "<div $dtvalue_forbot><p>$PackValue4</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check' src='$img_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack .= "<li $dtvalue_forlist>";
                    $strPack .= "<div $dtvalue_fortop><p>$PackName4 <i class='fas fa-medal'></i></p></div>";
                    $strPack .= "<div $dtvalue_forbot><p>$PackValue4</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader' src='$img2_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                }
            } elseif ($i == 5) {
                if ($dtvalue_fivlist == "class='passed lis5'") {
                    $strPack .= "<li $dtvalue_fivlist >";
                    $strPack .= "<div $dtvalue_fivtop><p>$PackName5 <i class='fas fa-trophy'></i></p></div>";
                    $strPack .= "<div $dtvalue_fivbot><p>$PackValue5</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check' src='$img_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack .= "<li $dtvalue_fivlist class='lis5' >";
                    $strPack .= "<div $dtvalue_fivtop><p>$PackName5 <i class='fas fa-trophy'></i></p></div>";
                    $strPack .= "<div $dtvalue_fivbot><p>$PackValue5</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader' src='$img2_url' alt='' style='margin-left: -32px'>";
                    $strPack .= '</li>';
                }
            } elseif ($i == 6) {
                if ($dtvalue_sixlist == "class='passed lis'") {
                    $strPack .= "<li $dtvalue_sixlist >";
                    $strPack .= "<div $dtvalue_sixtop ><p>$PackName6 <i class='fas fa-crown'></i></p></div>";
                    $strPack .= "<div $dtvalue_sixbot ><p>$PackValue6</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-check imag' src='$img_url' alt='' style='margin-left: 23px'>";
                    $strPack .= '</li>';
                } else {
                    $strPack .= "<li $dtvalue_sixlist class='lis' >";
                    $strPack .= "<div $dtvalue_sixtop ><p>$PackName6 <i class='fas fa-crown'></i></p></div>";
                    $strPack .= "<div $dtvalue_sixbot ><p>$PackValue6</p></div>";
                    $strPack .= "<img loading='lazy' class='gif-loader imag' src='$img2_url' alt='' style='margin-left: 23px'>";
                    $strPack .= '</li>';
                }
            }
        }

        $strwelcome = "<section class='ps-timeline-sec'>";
        $strwelcome .= "<div class='containerevu'style='--ps-timeline-valfirt:".$timelinePoint1.";'>";
        $strwelcome .= "<ol class='ps-timeline'>";
        $strwelcome .= $strPack;
        $strwelcome .= '</ol>';
        $strwelcome .= '</div>';
        $strwelcome .= '</section>';

        if ($timeline_CurrentReferral > 0) {
            $strwelcome_title = '<div class="ps-timeline-welcome">'.___('Hola ').' '.__(':fullname', ['fullname' => $timeline_nameuser]).___(', ¡felicitaciones!').'</div>';
            $strwelcome_title .= '<div class="ps-timeline-welcome">'.___('Hasta ahora, ganaste')." $timeline_giftEarned ".' <small>('.token_symbol().') Tokens</small>'.'</div>';
        } else {
            $strwelcome_title = '<div class="ps-timeline-welcome">'.___('Hola ').' '.__(':fullname', ['fullname' => $timeline_nameuser]).___(', ¡felicitaciones!').'</div>';
            $strwelcome_title .= '<div class="ps-timeline-welcome">'.___('Hasta ahora, ganaste')." $timeline_giftEarned ".' <small>('.token_symbol().') Tokens</small>'.'</div>';
        }
        if ($RegaloMax < $Local_MaxNivel) {
            $strwelcome_title .= '<div class="ps-timeline-welcome-detail">'.___('To unlock :level stage invite :number more friends', ['level' => $timeline_NeedPack, 'number' => $timeline_NeedReferral]).'</div>';
        }

        $auth = auth();
        // $urlCript= Crypt::encrypt(set_id(auth()->user()->id) ) ;
        $urlCript = substr(set_id(auth()->user()->username), 2);
        $idioma = $user->lang;
        $timeline_referral = route('public.referral').'?ref='.substr(set_id(auth()->user()->username), 2).'&lang='.$idioma;

        $strwelcome_titlebox = '<div class="ps-timeline-welcome-notebox">'.__('To unlock other benefits and earn more :currency, share your Referral Link with others:', ['currency' => $symbol]).'</div>';

        $strwelcome_box = '<div class="ps-timeline-welcome-box"><div class="copy-wrap mgb-1-5x mgt-1-5x buto" style="width:65%; margin: 0 auto;">';
        $strwelcome_box .= '<span class="copy-feedback" style="display: none;">'.___('Copied to Clipboard').'</span>';
        $strwelcome_box .= '<em class="copy-icon ri-link"></em><input type="text" class="copy-address text-center" value="'.___('Copy your referral URL').'" disabled="">';
        $strwelcome_box .= '<button class="copy-trigger copy-clipboard" data-clipboard-text="'.$timeline_referral.'"><em class="ti ti-files"></em></button>';
        $strwelcome_box .= '</div></div><div class="ps-timeline-welcome-box"><div class="copy-wrap mgb-1-5x mgt-1-5x buto d-flex flex-row" style="width:100%; margin: 0 auto;"><a href="#" class="btn-primary btn-block btn-sm" data-bs-toggle="modal" data-bs-target="#modalVideo" style="border-radius: 15px; width:fit-content; margin: 0 auto;">'
        .___('¿Cómo funciona?').'</a></div></div>';

        $link = json_decode(get_setting('site_social_links'));
        $fb = (isset($link->facebook) && $link->facebook != null) ? '<li><a target="_blank" href="'.$link->facebook.'"><em class="fab fa-facebook-f"></em></a></li>' : '';
        $tw = (isset($link->twitter) && $link->twitter != null) ? '<li><a target="_blank" href="'.$link->twitter.'""><em class="fab fa-twitter"></em></a></li>' : '';
        $in = (isset($link->linkedin) && $link->linkedin != null) ? '<li><a target="_blank" href="'.$link->linkedin.'"><em class="fab fa-linkedin-in"></em></a></li>' : '';
        $gh = (isset($link->github) && $link->github != null) ? '<li><a target="_blank" href="'.$link->github.'"><em class="fab fa-github-alt"></em></a></li>' : '';
        $yt = (isset($link->youtube) && $link->youtube != null) ? '<li><a target="_blank" href="'.$link->youtube.'"><em class="fab fa-youtube"></em></a></li>' : '';
        $md = (isset($link->medium) && $link->medium != null) ? '<li><a target="_blank" href="'.$link->medium.'"><em class="fab fa-medium-m"></em></a></li>' : '';
        $tg = (isset($link->telegram) && $link->telegram != null) ? '<li><a target="_blank" href="'.$link->telegram.'"><em class="fab fa-telegram-plane"></em></a></li>' : '';
        $twh = (isset($link->whatsapp) && $link->whatsapp != null) ? '<li><a target="_blank" href="https://api.whatsapp.com/send?phone='.$link->whatsapp.'"><em class="fab fa-whatsapp" style="font-size: 1.28em;"></em></a></li>' : '';
        $tco = (isset($link->correo) && $link->correo != null) ? '<li><a href="mailto:'.$link->correo.'"><em class="fas fa-envelope" style="font-size: 1em;"></em></a></li>' : '';

        $social_exist = ($fb || $tw || $in || $gh || $yt || $md || $tg || $twh || $tco) ? true : false;
        if ($social_exist) {
            $strwelcome_socialbox = '<div class="ps-timeline-welcome-social"><ul class="socials mb-0">';
            $strwelcome_socialbox .= $tw.$fb.$in.$gh.$yt.$md.$tg.$twh.$tco;
            $strwelcome_socialbox .= '</ul></div>';
        }

        $htmlrevu = '<div class="account-info card card-full-height">';
        $htmlrevu .= '<div class="card-innr">';
        $htmlrevu .= '<div class="user-account-status">'.$strwelcome_title.'</div>';
        $htmlrevu .= '<div class="user-account-status">'.$strwelcome.'</div>';

        $htmlrevu .= '';

        $htmlrevu .= '<div class="user-account-status">'.$strwelcome_titlebox.'</div>';
        $htmlrevu .= '<div class="gaps-1-1x"></div>';
        $htmlrevu .= '<div class="user-account-status row">'.$strwelcome_box.'</div>';

        $htmlrevu .= '';

        if ($social_exist) {
            $htmlrevu .= '<div class="user-account-status">'.$strwelcome_socialbox.'</div>';
        }
        $htmlrevu .= '</div>';
        $htmlrevu .= '</div>';

        return $htmlrevu;
    }

    // public static function user_url_usuario($data=null, $atttr='')
    // {
    //     $atttr_def = array( 'id'    => '', 'class' => '', 'vers' => '' );
    //     $opt_atttr = parse_args( $atttr, $atttr_def ); extract($opt_atttr);
    //     $g_id = ($id) ? ' id="'.$id.'"' : ''; $g_cls = ($class) ? css_class($class) : '';
    //     $auth       = auth(); $refers = $heading = '';
    //     $user = (empty($data)) ? auth()->user() : $data;
    //     $idioma = $user->lang;
    //     $ref_url    = route('public.referral').'?ref='.substr(set_id(auth()->user()->username), 2).'&lang='.$idioma;

    //     $user = (empty($data)) ? auth()->user() : $data;
    // 	return '<div class="copy-wrap mgb-1-5x mgt-1-5x d-none d-sm-inline-block">
    //                 <span class="copy-feedback">
    //                 </span>
    //                 <button onclick="show()" class="cssbuttons-io-button  copy-clipboard" data-clipboard-text="'.$ref_url.'">'.__('Copy your Link').'
    //                 <div class="iconz">

    //                 <svg class="show" id="svg1" height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"></path><path
    //                     d="M7 6V3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1h-3v3c0 .552-.45 1-1.007 1H4.007A1.001 1.001 0 0 1 3 21l.003-14c0-.552.45-1 1.006-1H7zM5.002 8L5 20h10V8H5.002zM9 6h8v10h2V4H9v2zm-2 5h6v2H7v-2zm0 4h6v2H7v-2z" fill="currentColor"></path></svg>

    //                     <svg class="hide" version="1.1" id="svg2"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    //                     width="533.973px" height="533.973px" viewBox="0 0 533.973 533.973" style="enable-background:new 0 0 533.973 533.973;"
    //                     xml:space="preserve">
    //                         <path fill="currentColor" d="M477.931,52.261c-12.821-12.821-33.605-12.821-46.427,0l-266.96,266.954l-62.075-62.069
    //                             c-12.821-12.821-33.604-12.821-46.426,0L9.616,303.572c-12.821,12.821-12.821,33.604,0,46.426l85.289,85.289l46.426,46.426
    //                             c12.821,12.821,33.611,12.821,46.426,0l46.426-46.426l290.173-290.174c12.821-12.821,12.821-33.605,0-46.426L477.931,52.261z"/>
    //                             </svg>
    //                 </div>
    //                 </button>
    //             </div>

    // 	';
    // }
    public static function user_url_usuario($data = null, $atttr = '')
    {
        $atttr_def = ['id' => '', 'class' => '', 'vers' => ''];
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="'.$id.'"' : '';
        $g_cls = ($class) ? css_class($class) : '';
        $auth = auth();
        $refers = $heading = '';
        $user = (empty($data)) ? auth()->user() : $data;
        $idioma = $user->lang;
        $ref_url = route('public.referral').'?ref='.substr(set_id(auth()->user()->username), 2).'&lang='.$idioma;

        $user = (empty($data)) ? auth()->user() : $data;

        return '
                <div class="copy-wrap mgb-1-5x mgt-1-5x d-none d-sm-inline-block">
                    <input type="text" id="url-copy-referrals-global" value="'.$ref_url.'" hidden="hidden">
                    <button id="button2" class="copy-trigger copy-clipboard btn-copy-url-referrals-global" style="height: 40px !important;">
                        <i class="ri-link"></i>
                        <p class="no-copiado">'.___('Copy your referral URL').'</p>
                        <p class="copiado d-none">'.___('Copied to Clipboard').'</p>
                    </button>
                </div>
		';
    }

    public static function user_referal_gift($data = null, $atttr = '')
    {
        $user = (empty($data)) ? auth()->user() : $data;
        $asociados = RegaloType_User_IsReferralCount($user->id);
        $gift = $user->gift_bonus;

        return '<div class="copy-wrap mgb-1-5x mgt-1-5x d-none d-sm-inline-block" style="width:fit-content">
        <div class="card gift-card mb-0 ml-2" >
            <div class="card-innr" style="padding: 8px 0px; padding-left: 0px;">
                <div class="row pr-4 pl-4" style="">
                    <div class="col-6 pl-2">
                        <p style="width:max-content;" >'.___('Asociados').':'.$asociados.'</p>
                    </div>
                    <div class="col-6  pr-2">
                        <p style="width:max-content; padding-right: 25px; left:10px;">Tokens :'.to_num($gift, 'max', ',').'</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
		';
    }

    public static function language_switcher_getflag_usuario($fl)
    {
        $fl = strtoupper($fl);
        if ($fl == 'EN') {
            return 'us.jpg';
        }
        if ($fl == 'ES') {
            return 'spain.jpg';
        }
        if ($fl == 'PR') {
            return 'portugal.jpg';
        }
        // if($fl == 'DE')
        // 	return 'germany.jpg';
        // if($fl == 'IT')
        // 	return 'italy.jpg';
        // if($fl == 'RU')
        // 	return 'russia.jpg';
        // if($fl == 'FR')
        // 	return 'french.jpg';
        // if($fl == 'AR')
        // 	return 'arab.jpg';
        // if($fl == 'HI')
        // 	return 'hindi.jpg';
        // if($fl == 'TR')
        // 	return 'turkish.jpg';
        // if($fl == 'ZH')
        // 	return 'chinese.jpg';
    }

    public static function language_switcher_usuario()
    {
        $l = str_replace('_', '-', current_lang());

        $flags = static::language_switcher_getflag_usuario($l);
        $flags = asset('assets/app/images/flags/'.$flags);
        $text = '<div class="dropdown">';
        $text .= '<button type="button" class="btn header-item waves-effect pl-1 pr-1 popover-btn" data-bs-toggle="dropdown" id="page-header-user-dropdown" aria-haspopup="true" aria-expanded="false" data-popover="popover-idioma">';
        $text .= '<span class="align-middle color" style="color: #758698; font-size: 20px;font-weight: 300; height:auto; width: auto;">'.get_lang($l).'<i class="mdi mdi-chevron-down d-xl-inline-block"></i></span>';
        $text .= '</button>';
        $text .= '<div class="popover" id="popover-idioma"><div class="popover-content"><p>'.___('Idioma').'</p></div></div>';
        $text .= '<div class="dropdown-menu dropdown-menu-end" style="min-width: max-content;border-radius: 20px;">';
        $idiomas = ['en', 'pr', 'es'/*,'ru','it','fr','hi','ar','tr','de','zh'*/];
        foreach ($idiomas as $lng) {
            $image = static::language_switcher_getflag_usuario($lng);
            $text .= '<a href="'.route('language').'?lang='.$lng.'" class="dropdown-item notify-item">';
            $text .= '<span class="align-middle">'.get_lang($lng).'</span>';
            $text .= '</a>';
        }
        $text .= '</div></div>';

        return $text;
    }

    public static function language_switcher_login()
    {
        $l = str_replace('_', '-', current_lang());
        // dd($l);
        $actived_lang = Language::where('code', $l)->get(['name', 'label', 'short', 'code']);
        // dd($actived_lang[0]->label);
        $flags = static::language_switcher_getflag_usuario($l);
        $flags = asset('assets/app/images/flags/'.$flags);
        $text = '<div class="dropdown">';
        $text .= '<button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="left: calc(100% - 160px);">';
        $text .= '<img loading="lazy" src="'.$flags.'" alt="'.$l.'" height="20" width="20" style="border-radius: 50%;"> <span class="align-middle" style=" font-size: smaller;">'.$actived_lang[0]->label.'</span> <i class="fa fa-chevron-down"></i>';
        $text .= '</button>';
        $text .= '<div class="dropdown-menu dropdown-menu-end" style="min-width: max-content;border-radius: 20px;">';
        $idiomas = ['en', 'pr', 'es'/*,'ru','it','fr','hi','ar','tr','de','zh'*/];
        foreach ($idiomas as $lng) {
            $image = static::language_switcher_getflag_usuario($lng);
            $text .= '<a href="'.route('language').'?lang='.$lng.'" class="dropdown-item notify-item">';
            $text .= '<img loading="lazy" src="'.asset('assets/app/images/flags/'.$image).'" alt="'.$lng.'" class="me-1" height="20" width="20" style="border-radius: 50%;"> <span class="align-middle">'.get_lang($lng).'</span>';
            $text .= '</a>';
        }
        $text .= '</div></div>';

        return $text;
    }

    public static function lenguaje_switcher($data = null, $atttr = '')
    {
        /*$user = (empty($data)) ? auth()->user() : $data;
        $atttr_def = array('id' => '', 'class' => '', 'vers' => '');
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="' . $id . '"' : '';
        $g_cls = ($class) ? css_class($class) : '';*/$g_id = '';
        $g_cls = '';

        if (! is_lang_switch()) {
            $return = '<li class="topbar-nav-item relative m-0">';
            $return .= '<span class="user-welcome d-none d-lg-inline-block mr-0">'.__('Welcome!').' '.auth()->user()->name.'</span>';
            $return .= '</li>';

            return $return;
        }

        $l = strtoupper(str_replace('_', '-', current_lang()));
        $flag = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTExLjk5OSA1MTEuOTk5IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTEuOTk5IDUxMS45OTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiMwM0FBNkY7IiBkPSJNNDQxLjM3NCw3OS41MTlsLTI2LjQ4NSwxNy42NTZsLTQ0LjE0MS04LjgyOGwtMjYuNDg1LTguODI4bC0zNS4zMTMsOC44MjhsLTI2LjQ4NS04LjgyOA0KCQlsMTcuNjU2LTM1LjMxM2gzNS4zMTNsMzQuNjA3LTE3LjMwM0MzOTYuNzMsNDAuMTAxLDQyMC44NzUsNTcuOTE2LDQ0MS4zNzQsNzkuNTE5eiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiMwM0FBNkY7IiBkPSJNMjM4LjMyNSw0NC4yMDZsLTguODI4LDI2LjQ4NWwtMjYuNDg1LDguODI4bC0yNi40ODUsNDQuMTQxbC00NC4xNDEsMjYuNDg1bC02MS43OTcsOC44Mjh2MjYuNDg1DQoJCWwxNy42NTYsMTcuNjU2djM1LjMxM0w2MS43NjIsMjIwLjc3bC0yNi40ODUtMTcuNjU2bC0xNS4zNjEtNDYuMTcxQzQ2LjM1Niw5My45MzUsOTYuODk4LDQ0LjEyNiwxNjAuMjg0LDE4LjYwNGwyNS4wNzIsMTYuNzc0DQoJCUwyMzguMzI1LDQ0LjIwNnoiLz4NCgk8cG9seWdvbiBzdHlsZT0iZmlsbDojMDNBQTZGOyIgcG9pbnRzPSIyNTUuOTgyLDI5MS4zOTUgMjQ3LjE1NCwzMzUuNTM2IDIxMS44NDEsMzcwLjg0OSAyMTEuODQxLDM5Ny4zMzQgMTc2LjUyOCw0MjMuODE4IA0KCQkxNzYuNTI4LDQ2Ny45NTkgMTUwLjA0NCw0NTkuMTMxIDEzMi4zODcsNDE0Ljk5IDEzMi4zODcsMzI2LjcwOCA4OC4yNDYsMzE3Ljg4IDcwLjU5LDI4Mi41NjcgNzAuNTksMjU2LjA4MiA4OC4yNDYsMjM4LjQyNiANCgkJMTE0LjczMSwyMTEuOTQxIDEzMi4zODcsMjQ3LjI1NCAxOTQuMTg0LDI0Ny4yNTQgMjIwLjY2OSwyOTEuMzk1IAkiLz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMDNBQTZGOyIgZD0iTTQ3OS4zMzUsMTMwLjk4N2M0OC4xOTMsODUuOTc4LDQyLjcwMiwxOTEuOTk2LTE0LjEyNSwyNzIuNTI2bC0yMy44MzYtMjMuODM2di0zNS4zMTMNCgkJbC0xNy42NTYtMzUuMzEzbC0xNy42NTYtMzUuMzEzdi0zNS4zMTNsLTI2LjQ4NS0xNy42NTZsLTM1LjMxMyw4LjgyOGwtNjEuNzk3LTI2LjQ4NWwtOC44MjgtNjEuNzk3bDI2LjQ4NS0yNi40ODVoNTIuOTY5DQoJCWwxNy42NTYsMjYuNDg1bDUyLjk2OSw4LjgyOGw1Mi45NjktMTcuNjU2TDQ3OS4zMzUsMTMwLjk4N3oiLz4NCjwvZz4NCjxwYXRoIHN0eWxlPSJmaWxsOiM4NkRBRjE7IiBkPSJNMzA4Ljk1MSw4OC4zNDdsMzUuMzEzLTguODI4bDI2LjQ4NSw4LjgyOGw0NC4xNDEsOC44MjhsMjYuNDg1LTE3LjY1Ng0KCWMxNC43ODcsMTUuNDc2LDI3LjU0NCwzMi43NywzNy45NjEsNTEuNDY4bC0yLjY0OCwxLjUwMWwtNTIuOTY5LDE3LjY1NmwtNTIuOTY5LTguODI4bC0xNy42NTYtMjYuNDg1aC01Mi45NjlsLTI2LjQ4NSwyNi40ODUNCglsOC44MjgsNjEuNzk3bDYxLjc5NywyNi40ODVsMzUuMzEzLTguODI4bDI2LjQ4NSwxNy42NTZ2MzUuMzEzbDE3LjY1NiwzNS4zMTNsMTcuNjU2LDM1LjMxM3YzNS4zMTNsMjMuODM2LDIzLjgzNg0KCWMtODEuNTI4LDExNS41MjYtMjQxLjI3NSwxNDMuMDc5LTM1Ni43OTIsNjEuNTVDMTAuMzExLDM5NS44MTUtMjYuNDk0LDI2Ny42OTEsMTkuOTE2LDE1Ni45NDJsMTUuMzYxLDQ2LjE3MWwyNi40ODUsMTcuNjU2DQoJbDI2LjQ4NSwxNy42NTZMNzAuNTksMjU2LjA4MnYyNi40ODVsMTcuNjU2LDM1LjMxM2w0NC4xNDEsOC44Mjh2ODguMjgybDE3LjY1Niw0NC4xNDFsMjYuNDg1LDguODI4di00NC4xNDFsMzUuMzEzLTI2LjQ4NXYtMjYuNDg1DQoJbDM1LjMxMy0zNS4zMTNsOC44MjgtNDQuMTQxaC0zNS4zMTNsLTI2LjQ4NS00NC4xNDFoLTYxLjc5N2wtMTcuNjU2LTM1LjMxM2wtMjYuNDg1LDI2LjQ4NXYtMzUuMzEzTDcwLjU5LDE4NS40NTd2LTI2LjQ4NQ0KCWw2MS43OTctOC44MjhsNDQuMTQxLTI2LjQ4NWwyNi40ODUtNDQuMTQxbDI2LjQ4NS04LjgyOGw4LjgyOC0yNi40ODVsLTUyLjk2OS04LjgyOGwtMjUuMDcyLTE2Ljc3NA0KCWM2Ny44ODktMjcuMzQxLDE0NC4yMzUtMjQuMzIyLDIwOS43NTgsOC4yOTlsLTM0LjYwNywxNy4zMDNoLTM1LjMxM2wtMTcuNjU2LDM1LjMxM0wzMDguOTUxLDg4LjM0N3oiLz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K';
        $lang = '';
        $text = '<ul'.$g_id.' class="user-links'.$g_cls.'">';
        foreach (config('icoapp.supported_languages') as $lng) {
            $lang = strtoupper(get_lang($lng));
            $text .= '<li><a class="text-white" href="'.route('language').'?lang='.$lng.'">'.$lang.'</a></li>';

            if ($l == $lang && $lang == 'EN') {
                $flag = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxjaXJjbGUgc3R5bGU9ImZpbGw6I0YwRjBGMDsiIGN4PSIyNTYiIGN5PSIyNTYiIHI9IjI1NiIvPg0KPGc+DQoJPHBhdGggc3R5bGU9ImZpbGw6I0Q4MDAyNzsiIGQ9Ik0yNDQuODcsMjU2SDUxMmMwLTIzLjEwNi0zLjA4LTQ1LjQ5LTguODE5LTY2Ljc4M0gyNDQuODdWMjU2eiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMjQ0Ljg3LDEyMi40MzVoMjI5LjU1NmMtMTUuNjcxLTI1LjU3Mi0zNS43MDgtNDguMTc1LTU5LjA3LTY2Ljc4M0gyNDQuODdWMTIyLjQzNXoiLz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojRDgwMDI3OyIgZD0iTTI1Niw1MTJjNjAuMjQ5LDAsMTE1LjYyNi0yMC44MjQsMTU5LjM1Ni01NS42NTJIOTYuNjQ0QzE0MC4zNzQsNDkxLjE3NiwxOTUuNzUxLDUxMiwyNTYsNTEyeiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMzcuNTc0LDM4OS41NjVoNDM2Ljg1MmMxMi41ODEtMjAuNTI5LDIyLjMzOC00Mi45NjksMjguNzU1LTY2Ljc4M0g4LjgxOQ0KCQlDMTUuMjM2LDM0Ni41OTYsMjQuOTkzLDM2OS4wMzYsMzcuNTc0LDM4OS41NjV6Ii8+DQo8L2c+DQo8cGF0aCBzdHlsZT0iZmlsbDojMDA1MkI0OyIgZD0iTTExOC41ODQsMzkuOTc4aDIzLjMyOWwtMjEuNywxNS43NjVsOC4yODksMjUuNTA5bC0yMS42OTktMTUuNzY1TDg1LjEwNCw4MS4yNTJsNy4xNi0yMi4wMzcNCglDNzMuMTU4LDc1LjEzLDU2LjQxMiw5My43NzYsNDIuNjEyLDExNC41NTJoNy40NzVsLTEzLjgxMywxMC4wMzVjLTIuMTUyLDMuNTktNC4yMTYsNy4yMzctNi4xOTQsMTAuOTM4bDYuNTk2LDIwLjMwMWwtMTIuMzA2LTguOTQxDQoJYy0zLjA1OSw2LjQ4MS01Ljg1NywxMy4xMDgtOC4zNzIsMTkuODczbDcuMjY3LDIyLjM2OGgyNi44MjJsLTIxLjcsMTUuNzY1bDguMjg5LDI1LjUwOWwtMjEuNjk5LTE1Ljc2NWwtMTIuOTk4LDkuNDQ0DQoJQzAuNjc4LDIzNC41MzcsMCwyNDUuMTg5LDAsMjU2aDI1NmMwLTE0MS4zODQsMC0xNTguMDUyLDAtMjU2QzIwNS40MjgsMCwxNTguMjg1LDE0LjY3LDExOC41ODQsMzkuOTc4eiBNMTI4LjUwMiwyMzAuNA0KCWwtMjEuNjk5LTE1Ljc2NUw4NS4xMDQsMjMwLjRsOC4yODktMjUuNTA5bC0yMS43LTE1Ljc2NWgyNi44MjJsOC4yODgtMjUuNTA5bDguMjg4LDI1LjUwOWgyNi44MjJsLTIxLjcsMTUuNzY1TDEyOC41MDIsMjMwLjR6DQoJIE0xMjAuMjEzLDEzMC4zMTdsOC4yODksMjUuNTA5bC0yMS42OTktMTUuNzY1bC0yMS42OTksMTUuNzY1bDguMjg5LTI1LjUwOWwtMjEuNy0xNS43NjVoMjYuODIybDguMjg4LTI1LjUwOWw4LjI4OCwyNS41MDloMjYuODIyDQoJTDEyMC4yMTMsMTMwLjMxN3ogTTIyMC4zMjgsMjMwLjRsLTIxLjY5OS0xNS43NjVMMTc2LjkzLDIzMC40bDguMjg5LTI1LjUwOWwtMjEuNy0xNS43NjVoMjYuODIybDguMjg4LTI1LjUwOWw4LjI4OCwyNS41MDloMjYuODIyDQoJbC0yMS43LDE1Ljc2NUwyMjAuMzI4LDIzMC40eiBNMjEyLjAzOSwxMzAuMzE3bDguMjg5LDI1LjUwOWwtMjEuNjk5LTE1Ljc2NWwtMjEuNjk5LDE1Ljc2NWw4LjI4OS0yNS41MDlsLTIxLjctMTUuNzY1aDI2LjgyMg0KCWw4LjI4OC0yNS41MDlsOC4yODgsMjUuNTA5aDI2LjgyMkwyMTIuMDM5LDEzMC4zMTd6IE0yMTIuMDM5LDU1Ljc0M2w4LjI4OSwyNS41MDlsLTIxLjY5OS0xNS43NjVMMTc2LjkzLDgxLjI1Mmw4LjI4OS0yNS41MDkNCglsLTIxLjctMTUuNzY1aDI2LjgyMmw4LjI4OC0yNS41MDlsOC4yODgsMjUuNTA5aDI2LjgyMkwyMTIuMDM5LDU1Ljc0M3oiLz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K';
            } elseif ($l == $lang && $lang == 'ES') {
                $flag = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxwYXRoIHN0eWxlPSJmaWxsOiNGRkRBNDQ7IiBkPSJNMCwyNTZjMCwzMS4zMTQsNS42MzMsNjEuMzEsMTUuOTIzLDg5LjA0M0wyNTYsMzY3LjMwNGwyNDAuMDc3LTIyLjI2MQ0KCUM1MDYuMzY3LDMxNy4zMSw1MTIsMjg3LjMxNCw1MTIsMjU2cy01LjYzMy02MS4zMS0xNS45MjMtODkuMDQzTDI1NiwxNDQuNjk2TDE1LjkyMywxNjYuOTU3QzUuNjMzLDE5NC42OSwwLDIyNC42ODYsMCwyNTZ6Ii8+DQo8Zz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojRDgwMDI3OyIgZD0iTTQ5Ni4wNzcsMTY2Ljk1N0M0NTkuOTA2LDY5LjQ3MywzNjYuMDcxLDAsMjU2LDBTNTIuMDk0LDY5LjQ3MywxNS45MjMsMTY2Ljk1N0g0OTYuMDc3eiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMTUuOTIzLDM0NS4wNDNDNTIuMDk0LDQ0Mi41MjcsMTQ1LjkyOSw1MTIsMjU2LDUxMnMyMDMuOTA2LTY5LjQ3MywyNDAuMDc3LTE2Ni45NTdIMTUuOTIzeiIvPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo=';
            }
        }
        $htmlrevu = '<div'.$g_id.' class="gosen-user-status'.$g_cls.'">'.$text.'</div>';

        $return = '<li class="topbar-nav-item relative mr-0">';
        $return .= '<span class="user-welcome d-none d-lg-inline-block">'.__('Welcome!').' '.auth()->user()->name.'</span>';
        $return .= '<a class="toggle-tigger gosen-user-thumb" href="#"><img loading="lazy" alt="'.$lang.'" src="'.$flag.'" /></a>';
        $return .= '<div class="toggle-class dropdown-content dropdown-content-right dropdown-arrow-right user-dropdown">';
        $return .= $htmlrevu;
        $return .= '</div>';
        $return .= '</li>';

        return $return;
    }

    public static function transaccion_user($data = null, $atttr = '', $link = null)
    {
        $user = (empty($data)) ? auth()->user() : $data;
        /*$atttr_def = array('id' => '', 'class' => '', 'vers' => '');
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="' . $id . '"' : '';
        $g_cls = ($class) ? css_class($class) : '';*/

        $trnxs = Transacciones_All($user->id);
        $table = '';
        $table = '<table class="table tnx-table"><tbody>';
        if (count($trnxs) == 0) {
            $table .= '<tr class="data-item text-center"><td class="data-col m-20" colspan="4"><p style="padding:50px 0;text-align:center;">No available transaction!</p></td></tr>';
        } else {
            foreach ($trnxs as $tnx) {
                $table .= '<tr>';
                $table .= '<td><h5 class="lead mb-1">'.$tnx->tnx_id.'</h5>';
                $table .= '<span class="sub">'.date('Y-m-d H:i', strtotime($tnx->tnx_time)).'</span></td>';

                $table .= '<td class="d-none d-sm-table-cell">';
                $table .= '<h5 class="lead mb-1'.($tnx->tnx_type == 'refund' ? ' text-danger' : '').'">';
                $table .= (starts_with($tnx->total_tokens, '-') ? '' : '+').to_round($tnx->total_tokens, 'min');
                $table .= '</h5>';
                $table .= '<span class="sub ucap">'.to_num($tnx->amount, 'max').' '.$tnx->currency.'</span>';
                $table .= '</td>';

                $table .= '<td class="text-right">';
                $table .= '<div class="data-state data-state-'.__status($tnx->status, 'icon').'"></div>';
                $table .= '</td></tr>';
            }
        }
        $table .= '</tbody></table>';

        $return = '';
        $return = '<div class="token-transaction card card-full-height">';
        $return .= '<div class="card-innr">';
        $return .= '<div class="card-head has-aside">';
        $return .= '<h4 class="card-title card-title-sm">'.__('Recent Transaction').'</h4>';
        $return .= '<div class="card-opt"><a href="'.$link.'" class="link ucap">'.__('View ALL').' <em class="fas fa-angle-right ml-2"></em></a></div>';
        $return .= '</div>';
        $return .= $table;
        $return .= '</div></div>';

        return $return;
    }

    public static function referidos_user($data = null, $atttr = '', $link = null)
    {
        $user = (empty($data)) ? auth()->user() : $data;
        /*$atttr_def = array('id' => '', 'class' => '', 'vers' => '');
        $opt_atttr = parse_args($atttr, $atttr_def);
        extract($opt_atttr);
        $g_id = ($id) ? ' id="' . $id . '"' : '';
        $g_cls = ($class) ? css_class($class) : '';*/

        $trnxs = Referidos_All_Active($user->id);
        $table = '';

        if (count($trnxs) == 0) {
            $table = '<tr class="data-item text-center"><td class="data-col" colspan="4"><p style="padding:50px 0;text-align:center;">User found!</p></td></tr>';
        } else {
            $table = '<table class="table tnx-table"><tbody>';
            foreach ($trnxs as $tnx) {
                $table .= '<tr>';
                $table .= '<td><h5 class="lead mb-1">'.(strlen($tnx->name) > 25 ? substr($tnx->name, 0, 25).' ..' : ucfirst($tnx->name)).'</h5>';
                $table .= '<span class="sub">'.set_id($tnx->id).'</span></td>';

                $table .= '<td class="d-none d-sm-table-cell">';
                $table .= '<h5 class="lead mb-1">'.(strlen($tnx->email) > 28 ? substr($tnx->email, 0, 28).' ..' : strtolower($tnx->email)).'</h5>';

                $table .= '<span class="sub">'.date('Y-m-d H:i', strtotime($tnx->created_at)).'</span>';
                $table .= '</td>';

                $table .= '<td class="text-right">';
                $table .= '<div class="data-state data-state-sm data-state-'.($tnx->email_verified_at !== null ? 'approved' : 'pending').'"></div>';

                $table .= '</td></tr>';
            }
            $table .= '</tbody></table>';
        }

        $return = '';
        $return = '<div class="token-transaction card card-full-height">';
        $return .= '<div class="card-innr">';
        $return .= '<div class="card-head has-aside">';
        $return .= '<h4 class="card-title card-title-sm">'.__('Recent Users').'</h4>';
        $return .= '<div class="card-opt"><a href="'.$link.'" class="link ucap">'.__('View ALL').' <em class="fas fa-angle-right ml-2"></em></a></div>';
        $return .= '</div>';
        $return .= $table;
        $return .= '</div></div>';

        return $return;
    }

    public static function get_referrals()
    {
        $cantidadReferidos = RegaloType_User_IsReferralCount(auth()->user()->id);

        return $cantidadReferidos > 0 ? true : false;
    }
}
