<?php

namespace App\Helpers;

/**
 * Google ReCaptha
 *
 * @version 1.0
 *
 * @since 1.1.4
 */

use App\Events\ModalBonusReferralGiftEvent;
use App\Events\NotificationDataEvent;
use App\Events\NotificationEvent;
use App\Helpers\TokenCalculate as TC;
use App\Models\BonusDolar;
use App\Models\BonusReferral;
use App\Models\Country;
use App\Models\HistorialNotificacion;
use App\Models\LevelBonusDollar;
use App\Models\LogsGosen;
use App\Models\Menu;
use App\Models\Notification;
use App\Models\Paquete;
use App\Models\PaymentMethod;
use App\Models\RegaloType;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Orhanerday\OpenAi\OpenAi;
use stdClass;

class GosenHelper
{
    public static function addDolarBonus($trnx)
    {
        $TNX = $trnx;
        // cancelar bonos de compra y agregar niveles (07/10/2022)
        // $transaccionOriginal = json_decode($trans->extra);

        // $TNX = DB::table('transactions')->where('tnx_id', $transaccionOriginal->tnx_id)->first();
        //TNX es la transaccion original donde se compraron los tokens
        $referido = User::find($TNX->user);
        // if ($referido->referral != null) {
        //     $tipoCalculo = DB::table('settings')->where('field', 'referral_calc')->first();
        //     $cantidadCalculo = DB::table('settings')->where('field', 'referral_bonus')->first();
        //     if ($tipoCalculo->value == 'percent') {
        //         $valorDolarToken = TokenCalculate::obtenerPrecio($TNX->stage);
        //         foreach ((array) $valorDolarToken as $valor) {
        //             $totalDolar = (float)$valor->price * ($TNX->tokens * (float)($cantidadCalculo->value / 100));
        //             break;
        //         }
        //     } else {
        //         $totalDolar = $cantidadCalculo->value;
        //     }

        //     $bonus = BonusDolar::create([
        //         'stage_id' => $TNX->stage,
        //         'transaction_id' => $TNX->id,
        //         'user_id' => $referido->referral,
        //         'checked_by' => Auth::check() ? auth()->user()->id : 0,
        //         'amount' => $totalDolar,
        //         'type_value' => $cantidadCalculo->value,
        //         'type_bonus' => $tipoCalculo->value,
        //         'user_from' => $TNX->user,
        //         'level_bonus' => 1,
        //     ]);
        //     $bonus->code = BonusDolar::BONUSCODETNX . $bonus->id;
        //     $bonus->save();
        //     //para traes el bono recien creado con todos sus datos

        //     $referido = User::find($bonus->user_from);

        //     GosenHelper::createNotification($bonus->user_id, GosenHelper::contenidoNotificacion('recibiste_bono', [$referido->username, $bonus->level_bonus, $bonus->amount]));

        //     DB::table('users')->where('id', $referido->referral)->increment('dolarBonus', $totalDolar);
        // }
        $niveles = LevelBonusDollar::all();
        $nivelMaxTipo0 = 0;
        $nivelMaxTipo1 = 0;
        $nivelMaxTipo2 = 0;
        $nivelMaxTipo3 = 0;
        $nivelMaxTipo4 = 0;
        $nivelMaxTipo5 = 0;
        $nivelMaxTipo6 = 0;
        $nivelMaxTipo7 = 0;
        $nivelMaxTipo8 = 0;
        $nivelMaxTipo9 = 0;
        $nivelMaxTipo10 = 0;
        for ($i = 0; $i <= 10; $i++) {
            foreach ($niveles->where('type_user', $i) as $nivel) {
                if ($nivel->status == LevelBonusDollar::STATUS2) {
                    switch ($i) {
                        case 0:
                            $nivelMaxTipo0++;
                            break;
                        case 1:
                            $nivelMaxTipo1++;
                            break;
                        case 2:
                            $nivelMaxTipo2++;
                            break;
                        case 3:
                            $nivelMaxTipo3++;
                            break;
                        case 4:
                            $nivelMaxTipo4++;
                            break;
                        case 5:
                            $nivelMaxTipo5++;
                            break;
                        case 6:
                            $nivelMaxTipo6++;
                            break;
                        case 7:
                            $nivelMaxTipo7++;
                            break;
                        case 8:
                            $nivelMaxTipo8++;
                            break;
                        case 9:
                            $nivelMaxTipo9++;
                            break;
                        case 10:
                            $nivelMaxTipo9++;
                            break;
                        default:
                            // code...
                            break;
                    }
                } else {
                    break;
                }
            }
        }

        //asignamos bonos por niveles de usuarios
        if ($referido->referral != null) {
            $nivel = 1;
            $referidoNiveles = $referido; //empieza en el nivel 0
            $array = [];
            do {
                if ($referidoNiveles->referral != null) { //es el usuario siguiente que debe recibir el bono
                    $tipoUsuario = User::find($referidoNiveles->referral)->type_user;
                    //dd($tipoUsuario);
                    array_push($array, $tipoUsuario);
                    switch ($tipoUsuario) {
                        case 0:
                            if ($nivel <= $nivelMaxTipo0) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 0)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 1:
                            if ($nivel <= $nivelMaxTipo1) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 1)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 2:
                            if ($nivel <= $nivelMaxTipo2) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 2)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 3:
                            if ($nivel <= $nivelMaxTipo3) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 3)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 4:
                            if ($nivel <= $nivelMaxTipo4) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 4)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 5:
                            if ($nivel <= $nivelMaxTipo5) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 5)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 6:
                            if ($nivel <= $nivelMaxTipo6) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 6)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 7:
                            if ($nivel <= $nivelMaxTipo7) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 7)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 8:
                            if ($nivel <= $nivelMaxTipo8) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 8)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 9:
                            if ($nivel <= $nivelMaxTipo9) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 9)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        case 10:
                            if ($nivel <= $nivelMaxTipo9) {
                                $levelBonusDollar = LevelBonusDollar::where('name', 'level' . $nivel)->where('type_user', 10)->first();
                                $userAddedBonus = GosenHelper::addLevelBonusDollar($levelBonusDollar, $referidoNiveles->referral, $TNX, $referidoNiveles->id, $nivel);
                                $referidoNiveles = User::find($userAddedBonus);
                            } else {
                                $referidoNiveles = User::find($referidoNiveles->referral);
                            }
                            break;
                        default:

                            break;
                    }
                }
                $nivel++;
            } while ($nivel <= 5);
        }
    }

    public static function addLevelBonusDollar($levelBonusDollar, $idUserReferral, $TNX, $idUserFrom, $nivel)
    {
        $tipoCalculo = $levelBonusDollar->type;
        $cantidadCalculo = $levelBonusDollar->value;
        //dd(0.2* ($TNX->tokens * (float)($cantidadCalculo / 100)));
        if ($tipoCalculo == 'percent') {
            $valorDolarToken = TokenCalculate::obtenerPrecio($TNX->stage);
            foreach ((array) $valorDolarToken as $valor) {
                $totalDolar = ($TNX->base_amount * (float) ($cantidadCalculo / 100));
                break;
            }
        } else {
            $totalDolar = $cantidadCalculo;
        }

        $bonus = BonusDolar::create([
            'stage_id' => $TNX->stage,
            'transaction_id' => $TNX->id,
            'user_id' => $idUserReferral,
            'checked_by' => Auth::check() ? auth()->user()->id : 0,
            'amount' => $totalDolar,
            'type_value' => $cantidadCalculo,
            'type_bonus' => $tipoCalculo,
            'user_from' => $idUserFrom,
            'level_bonus' => $nivel,
        ]);
        $bonus->code = BonusDolar::BONUSCODETNX . $bonus->id;
        $bonus->save();

        $referido = User::find($TNX->user);
        GosenHelper::createNotification($bonus->user_id, GosenHelper::contenidoNotificacion('recibiste_bono', [$referido->username, $bonus->level_bonus, $bonus->amount]));
        DB::table('users')->where('id', $idUserReferral)->increment('dolarBonus', $totalDolar);

        return $idUserReferral;
    }

    public static function verifyImgTrumbowyg(string $stringHtml) //analiza los tags img para anadir estilos y no desbordar la imagen
    {
        $contenido = '';
        if (strpos($stringHtml, '<') === false && strpos($stringHtml, '>') === false) {
            $contenido = $stringHtml;
        } else {
            preg_match_all(
                '/\<\w[^<>]*?\>([^<>]+?\<\/\w+?\>)?|\<\/\w+?\>/i',
                $stringHtml,
                $matches
            );
            $html = $matches[0];
            foreach ($html as $tag) {
                $recorte = Str::substr($tag, 1, 3);

                if ($recorte == 'img') {
                    $contieneStyle = Str::contains($tag, 'style');
                    if (!$contieneStyle) {
                        $tag = Str::substr($tag, 0, -1) . ' style="width:90%" >';
                    }
                }
                $contenido = $contenido . $tag;
            }
        }

        return $contenido;
    }

    public static function sendNotificationReferrals($idUsuario)
    {
        $nombreUsuario = User::find($idUsuario);
        $nombreUsuario = $nombreUsuario->name;
        for ($i = 1; $i <= 5; $i++) {
            $user = User::find($idUsuario);
            if ($user && isset($user->referral)) {
                GosenHelper::createNotification($user->referral, GosenHelper::contenidoNotificacion('registro_nuevo_asociado', [$nombreUsuario, $i, 0]));
                $idUsuario = $user->referral;
            } else {
                break;
            }
        }
    }

    public static function refreshBonusGift($user)
    {
        //dd($user);
        $tc = new TC();
        $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
        $cant_referal = RegaloType_User_IsReferralCount($user->referral);
        $tipos = RegaloType::orderBy('total_guests', 'ASC')->get();
        $idTipo = null;
        $regalo = null;
        BonusReferral::create([
            'regalo_tipo_id' => $tipos[0]->id,
            'user_id' => $user->id,
            'total_referral' => 0,
        ]);
        $trans = Transaction::create([
            'tokens' => 10,
            'total_tokens' => 10,
            'user' => $user->id,
            'stage' => active_stage()->id,
            'tnx_type' => 'launch',
            'payment_method' => 'system',
            'base_currency' => base_currency(),
            'base_currency_rate' => $base_currency_rate,
            'checked_by' => '{"name":"System","id":"0"}',
            'added_by' => 'SYS-00000',
            'checked_time' => now(),
            'tnx_time' => now(),
            'details' => 'Bonus Token for register account',
            'tnx_id' => 1,
            'regalo_tipo_id' => $tipos[0]->id,
        ]);
        $trans->tnx_id = 'GIFT' . sprintf('%06s', $trans->id);
        $trans->save();
        User::find($user->id)->increment('gift_bonus', 10);
        foreach ($tipos as $tipo) {
            if ($cant_referal >= $tipo->total_guests) {
                $idTipo = $tipo->id;
                $regalo = $tipo->total_tokens;
                $siExiste = BonusReferral::where('user_id', $user->referral)->first();
                if ($siExiste) {
                    $siExiste->total_referral = $cant_referal;
                    $siTransExists = Transaction::where('user', $user->referral)->where('regalo_tipo_id', $tipo->id)->first();
                    if (!$siTransExists) {
                        $tc = new TC();
                        $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
                        $transaction = Transaction::create([
                            'tokens' => $regalo,
                            'total_tokens' => $regalo,
                            'user' => $user->referral,
                            'stage' => active_stage()->id,
                            'tnx_type' => 'joined',
                            'tnx_time' => now(),
                            'payment_method' => 'system',
                            'base_currency' => base_currency(),
                            'base_currency_rate' => $base_currency_rate,
                            'checked_by' => '{"name":"System","id":"0"}',
                            'added_by' => 'SYS-00000',
                            'checked_time' => now(),
                            'details' => 'Bonus Token for Referral Join',
                            'tnx_id' => 1,
                            'regalo_tipo_id' => $tipo->id,
                        ]);
                        $transaction->tnx_id = 'GIFT' . sprintf('%06s', $transaction->id);
                        $transaction->save();
                        User::find($user->referral)->increment('gift_bonus', $regalo);
                        $siExiste->regalo_tipo_id = $idTipo;
                        $regaloActual = GosenHelper::getIconGiftUserReferrals($idTipo);
                        event(new ModalBonusReferralGiftEvent($siExiste->user_id, $regalo, $regaloActual[0], $regaloActual[1]));
                    }
                    $siExiste->save();
                } else {
                    BonusReferral::create([
                        'regalo_tipo_id' => $idTipo,
                        'user_id' => $user->referral,
                        'total_referral' => $cant_referal,
                    ]);
                    $tc = new TC();
                    $base_currency_rate = Setting::exchange_rate($tc->get_current_price(), base_currency());
                    $transaction = Transaction::create([
                        'tokens' => 10,
                        'total_tokens' => 10,
                        'user' => $user->referral,
                        'stage' => active_stage()->id,
                        'tnx_type' => 'launch',
                        'payment_method' => 'system',
                        'base_currency' => base_currency(),
                        'base_currency_rate' => $base_currency_rate,
                        'checked_by' => '{"name":"System","id":"0"}',
                        'added_by' => 'SYS-00000',
                        'checked_time' => now(),
                        'tnx_time' => now(),
                        'details' => 'Bonus Token for register account',
                        'tnx_id' => 1,
                        'regalo_tipo_id' => $tipo->id,
                    ]);
                    $transaction->tnx_id = 'GIFT' . sprintf('%06s', $transaction->id);
                    $transaction->save();
                    User::find($user->referral)->increment('gift_bonus', 10);
                }
            }
        }
    }

    public static function refundDollarsTransactions($trnx)
    {
        $transaction = $trnx;
        $bonusDolars = BonusDolar::where('transaction_id', $transaction->id)->get();
        if ($bonusDolars->count() > 0) {
            try {
                foreach ($bonusDolars as $bonus) {
                    $bonus->status = BonusDolar::STATUS3;
                    $bonus->save();
                    $bonus->userBy->dolarBonus = $bonus->userBy->dolarBonus - $bonus->amount;
                    $bonus->userBy->save();
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
    }

    public static function refreshRates()
    {
        $base = null;
        $cl = new Client();
        $response = $cl->request('GET', 'https://data.exratesapi.com/rates', [
            'headers' => ['X-Api-Signature' => base64_encode(gdmn())],
            'query' => array_merge(['access_key' => _joaat(env_file('code')) . app_key(), 'app' => app_info('key'), 'ver' => app_info('version')], PaymentMethod::getApiData($base)),
        ]);
        if ($response->getStatusCode() == 200) {
            $getBody = json_decode($response->getBody(), true);
            $rates = $getBody['rates'];
            foreach ($rates as $symbol => $value) {
                try {
                    $field = 'pmc_rate_' . strtolower($symbol);
                    Setting::where('field', $field)->update(['value' => $value]);
                } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $th) {
                    Setting::create(['field' => $field, 'value' => $value]);
                    $active = 'pmc_active_' . strtolower($symbol);
                    Setting::create(['field' => $active, 'value' => 1]);
                }
            }
        }
    }

    public static function registerLocation($user)
    {
        if (env('APP_ENV') == 'production') {
            $response = Http::get('http://ipwho.is/' . request()->ip());
            $respuesta = json_decode($response->body());
            $country = Country::where('name', $respuesta->country)->first();
            if (!$country) {
                $country = Country::create([
                    'name' => $respuesta->country,
                    'latitude' => $respuesta->latitude,
                    'longitude' => $respuesta->longitude,
                    'users' => 1,
                    'flag' => $respuesta->flag->img,
                    'code' => $respuesta->country_code,
                ]);
            } else {
                $cantidad = $country->users + 1;
                $country->users = $cantidad;
                $country->flag = $respuesta->flag->img;
                $country->save();
            }
            $user->country_id = $country->id;
            $user->latitude = $respuesta->latitude;
            $user->longitude = $respuesta->longitude;
            $user->city = $respuesta->city;
            $user->region = $respuesta->region;
            $user->country = $respuesta->country;
            $user->save();
        }
    }

    public static function getLastNDays($days, $format = 'd/m')
    {
        $dateArray = [];
        for ($i = 0; $i <= $days - 1; $i++) {
            $fecha = Carbon::now()->subDay($i);

            $nombreDia = GosenHelper::saber_dia($fecha);
            $dateArray[] = $nombreDia . ' ' . Carbon::parse($fecha)->format('d');
        }

        return array_reverse($dateArray);
    }

    public static function saber_dia($nombredia)
    {
        // dd(date('N', strtotime($nombredia)));
        $dias = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
        $fecha = $dias[date('N', strtotime($nombredia))];

        return ___($fecha);
    }

    public static function getLastNMonth($month, $format = 'd/m')
    {
        $dateArray = [];
        for ($i = 0; $i <= $month - 1; $i++) {
            $fecha = Carbon::now()->subMonths($i);
            $nombreMes = GosenHelper::saber_mes(Carbon::parse($fecha)->format('m'));
            $dateArray[] = $nombreMes;
        }

        return array_reverse($dateArray);
    }

    public static function saber_mes(int $numeroMes)
    {
        $meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $mes = $meses[$numeroMes];

        return $mes;
    }

    public static function allReferral(array $idUsers, $cantidad = 3)
    {
        $array = [$idUsers];
        for ($i = 0; $i < $cantidad; $i++) {
            $usuarios = User::where('referral', $idUsers[0]);
            for ($j = 1; $j < count($idUsers); $j++) {
                $usuarios = $usuarios->orWhere('referral', $idUsers[$j]);
            }
            $usuarios = $usuarios->pluck('referral', 'id');

            if ($usuarios->count() > 0) {
                $users = [];
                foreach ($usuarios as $user => $ref) {
                    array_push($users, $user);
                }
                $idUsers = $users;
                array_push($array, $users);
            } else {
                break;
            }
        }

        return $array;
    }

    public static function timeago($date)
    {
        $timestamp = strtotime($date);
        $strTimeEs = ['segundo', 'minuto', 'hora', 'dia', 'mes', 'año'];
        $strTimeEn = ['second', 'minute', 'hour', 'day', 'month', 'year'];
        $strTimePr = ['segundo', 'minuto', 'hora', 'dia', 'mês', 'ano'];
        $length = ['60', '60', '24', '30', '12', '10'];
        $idioma = auth()->user()->lang;

        $currentTime = time();
        if ($currentTime >= $timestamp) {
            $diff = time() - $timestamp;
            for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                $diff = $diff / $length[$i];
            }
            $diff = round($diff);
            $devolucion = '';
            $plural = '';
            if ($diff > 1) {
                $plural = 's';
                if ($idioma == 'es' && $i == 4) {
                    $plural = 'es';
                }
                if ($idioma == 'pr' && $i == 4) {
                    $strTimeEs[$i] = 'mes';
                    $plural = 'es';
                }
            }
            switch ($idioma) {
                case 'es':
                    $devolucion = 'Hace' . ' ' . $diff . ' ' . $strTimeEs[$i] . $plural;
                    break;
                case 'en':
                    $devolucion = $diff . ' ' . $strTimeEn[$i] . $plural . ' ago';
                    break;

                case 'pr':
                    $devolucion = $diff . ' ' . $strTimeEs[$i] . $plural . ' atrás';
                    break;

                default:
                    $devolucion = $diff . ' ' . $strTimeEn[$i] . $plural . ' ago';
                    break;
            }

            return $devolucion;
        }
    }

    public static function contenidoNotificacion($notificacion, $parametros = [0, 0, 0])
    {
        //notificaciones sin usar
        //subir nivel
        //sesion_nuevo_dispositivo
        //recibiste_gift
        //compra_paquete_cancelado
        //compra_paquete_incompleta

        //
        $array = [
            'bienvenida' => ['Bienvenido a Disruptive', '', 'primary', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'compra_exitosa_paquete' => ['Compra de paquete exitosa', 'Acabas de adquirir el paquete numero ' . $parametros[0], 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'subir_nivel' => ['Subiste de nivel', 'Acabas de subir a ' . $parametros[0] . ' y ganaste ' . $parametros[1] . ' tokens', 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'recibiste_bono' => ['Recibiste BONO USD', 'Tu asociado ' . $parametros[0] . ' ' . 'te bonifico' . ' ' . $parametros[2] . ' ' . 'USD' . ' en el nivel' . ' ' . $parametros[1], 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'ganancias']), 'ganancias', 'ti ti-check'],
            'cambio_de_password' => ['Cambio de contraseña Exitosa', '', 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'sesion_nuevo_dispositivo' => ['Iniciaste sesion desde otro dispositivo', '', 'info', 'fa fa-info', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-info'],
            'recibiste_gift' => ['Recibiste un Gift', 'Se te acredito ' . $parametros[0] . ' tokens por ' . $parametros[1] . ' Disruptive', 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'ganancias']), 'ganancias', 'ti ti-check'],
            'retiro_usd_exitoso' => ['Retiro BONO USD Exitoso', 'Realizaste el retiro de ' . $parametros[0] . ' USD a una billetera externa', 'success', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'retiro_usd_cancelado' => ['Retiro BONO USD cancelado', 'No se pudo realizar el retiro de ' . $parametros[0] . ' USD a una billetera externa', 'danger', 'fa fa-close', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-close'],
            'retiro_usd_pendiente' => ['Retiro BONO USD Pendiente', 'Retiro de ' . $parametros[0] . ' USD pendiente', 'warning', 'fa fa-exclamation', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-help-alt'],
            'email_verificado' => ['Email Verificado', 'Tu nuevo correo electronico fue verificado exitosamente', 'primary', 'fa fa-check', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-check'],
            'compra_paquete_cancelado' => ['Compra de paquete cancelada', 'La adquisicion del paquete ' . $parametros[0] . ' fue cancelada', 'danger', 'fa fa-close', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-close'],
            'compra_paquete_incompleta' => ['Compra de paquete incompleta', 'La adquisicion del paquete ' . $parametros[0] . ' fue cancelada', 'danger', 'fa fa-close', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-close'],
            'registro_nuevo_asociado' => ['Registro de nuevo asociado', 'Tu asociado ' . $parametros[0] . ' se registró en el nivel ' . $parametros[1], 'info', 'fa fa-info', route('all.notifications', ['filtrar' => 'asociados']), 'asociados', 'ti ti-info'],
            'ultimos_dias_paquetes' => ['Ultimos dias para adquirir tu paquete Disruptive!', 'Quedan solo ' . $parametros[0] . ' dias para que puedas ser parte de este proyecto', 'info', 'fa fa-info', route('all.notifications', ['filtrar' => 'noticias']), 'noticias', 'ti ti-info'],
            'cambio_billetera' => ['Su billetera (USDT) fue actualizada con exito!', '', 'info', 'fas fa-wallet', route('disruptive.user.profile', 2), 'informacion', 'ti ti-wallet'],
            'aprobacion_user_retiro_usd' => ['Solicitud de Retiro', 'Aprobaste el retiro de' . $parametros[0] . ' ' . '(USDT), en las proximas 48 horas se depositara el monto y se te notificara por correo. Codigo del retiro(' . $parametros[1] . ')', 'info', 'fa fa-info', route('all.notifications', ['filtrar' => 'informacion']), 'informacion', 'ti ti-info'],
            'whatsapp_verificacion_finalizada' => ['¡Verificación exitosa!', 'Finalizaste la verificación y ganaste 5 tokens!', 'success', 'fab fa-whatsapp', route('all.notifications', ['filtrar' => 'noticias']), 'noticias', 'ti ti-check'],
            'whatsapp_actualizado_verificado' => ['¡Verificación exitosa!', 'Finalizaste la actualizacion y verificacion de tu numero de whatsapp!', 'info', 'fab fa-whatsapp', route('all.notifications', ['filtrar' => 'noticias']), 'noticias', 'ti ti-check'],
            'bonus_por_paquete' => ['¡Recibiste un bono!', 'Tu transaccion de ID: ' . $parametros[0] . ' genero una bonificacion, recibiste ' . $parametros[1] . ' tokens de regalo!', 'success', 'fa fa-gift', route('all.notifications', ['filtrar' => 'noticias']), 'noticias', 'ti ti-gift'],
        ];

        return $array[$notificacion];
    }

    public static function createNotification(int $idUser, array $mensaje)
    {
        $usuario = User::find($idUser);
        if ($usuario) {
            $notificacion = new stdClass();
            $notificacion->ruta = $mensaje[4] != null ? $mensaje[4] : '#';
            $notificacion->mensaje = $mensaje[0];
            $notificacion->icono = $mensaje[3];
            $notificacion->created_at = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            $notificacion->detalle = $mensaje[1];
            $notificacion->color = $mensaje[2];
            $notificacion->estado = true;
            $notificacion->popup = false;
            $notificacion->id = uniqid();
            $notificacion->tag = $mensaje[5];
            $notificacion->icono_ti = $mensaje[6];
            $notificacion->estado_header = false;
            if (isset($usuario->notifications_setting)) {
                $ajustesNotificaciones = json_decode($usuario->notifications_setting, true);

                if (isset($ajustesNotificaciones[$mensaje[5]])) {
                    if ($ajustesNotificaciones[$mensaje[5]]) {
                        $notificacion->estado_header = true;
                        event(new NotificationEvent());
                        event(new NotificationDataEvent($notificacion, $usuario->id));
                    }
                }
            } else {
                $notificacion->estado_header = true;
                event(new NotificationDataEvent($notificacion, $usuario->id));
                event(new NotificationEvent());
            }
            $notificacionesUsuario = Notification::where('user_id', $idUser)->first();
            if ($notificacionesUsuario) {
                $mensajes = json_decode($notificacionesUsuario->messages);
                array_push($mensajes, $notificacion);
                $notificacionesUsuario->messages = $mensajes;
            } else {
                $notificacionesUsuario = Notification::create([
                    'user_id' => $idUser,
                    'messages' => '[' . json_encode($notificacion) . ']',
                ]);
            }
            if (isset($notificacion->estado_header)) {
                if ($notificacion->estado_header) {
                    $notificacionesUsuario->campana = true;
                }
            }

            $notificacionesUsuario->save();
        } else {
            LogsGosen::create([
                'titulo' => 'Fallo al crear notificacion para usuario:' . $idUser,
                'log' => 'Mensaje: ' . $mensaje[0] . ' Detalle: ' . $mensaje[1],
            ]);
        }
    }

    public static function leerNotificacion($idNotificacion, $mensajes)
    {
        try {
            DB::beginTransaction();
            $notificaciones = json_decode($mensajes->messages);
            $encontrado = array_search($idNotificacion, array_column($notificaciones, 'id'));
            $notificaciones[$encontrado]->estado = false;
            $mensajes->messages = json_encode($notificaciones);
            $mensajes->save();
            DB::commit();

            return [$mensajes, $notificaciones[$encontrado]->ruta];
        } catch (\Throwable $th) {
            DB::rollBack();

            return false;
        }
    }

    public static function eliminarNotificacion($idNotificacion, $mensajes)
    {
        try {
            DB::beginTransaction();
            $notificaciones = json_decode($mensajes->messages);
            $encontrado = array_search($idNotificacion, array_column($notificaciones, 'id'));

            $ruta = $notificaciones[$encontrado]->ruta;
            HistorialNotificacion::create([
                'titulo' => $notificaciones[$encontrado]->mensaje,
                'notificacion' => key($notificaciones[$encontrado]),
                'contenido' => $notificaciones[$encontrado]->detalle,
                'fecha_creacion' => $notificaciones[$encontrado]->created_at,
                'user_id' => auth()->user()->id,
            ]);
            unset($notificaciones[$encontrado]);
            $mensajes->messages = json_encode(array_values($notificaciones));
            $mensajes->save();
            DB::commit();

            return [$mensajes, $ruta];
        } catch (\Throwable $th) {
            DB::rollBack();

            return false;
        }
    }

    public static function referidosUsuarioPorPais($idUsuario)
    {
        $porPais = User::where('referral', $idUsuario)->where('latitude', '!=', null)->get();

        $asociadosPorPais = $porPais->groupBy('country')->sortDesc();

        $usuariosPorPais = collect();
        foreach ($asociadosPorPais as $pais => $cantidad) {
            //dd($respuesta);
            $usuariosPorPais->push([
                'name' => $pais . ' : ' . $cantidad->count() . ' ' . ___('users'),
                'coords' => [$cantidad[0]->countries->latitude, $cantidad[0]->countries->longitude],
                'status' => 'mrk',
            ]);
        }

        return $usuariosPorPais;
    }

    public static function referidosPorPaisYNivel($idUsuario, $niveles = 5, $returnJsonMapa = true)
    {
        $respuesta = GosenHelper::allReferral([$idUsuario], $niveles);
        $usuariosPorNivel = [];
        $contador = 0;
        foreach ($respuesta as $array) {
            if ($contador != 0) {
                foreach ($array as $idUsuario) {
                    $usuario = User::find($idUsuario);
                    if ($usuario->email_verified_at) {
                        if (isset($usuariosPorNivel[$usuario->country][$contador])) {
                            $usuariosPorNivel[$usuario->country][$contador] = $usuariosPorNivel[$usuario->country][$contador] + 1;
                        } else {
                            $usuariosPorNivel[$usuario->country][$contador] = 1;
                        }
                    }
                }
            }
            $contador++;
        }
        if (!$returnJsonMapa) {
            return $usuariosPorNivel;
        }
        $usuariosPorPais = collect();
        foreach ($usuariosPorNivel as $pais => $niveles) {
            $mensaje = '';
            $total = 0;
            foreach ($niveles as $nivel => $cantidad) {
                $mensaje = $mensaje . ' ' . ___('Nivel') . ' ' . $nivel . ' : ' . $cantidad . ' |';
                $total = $total + $cantidad;
            }
            $mensaje = $mensaje . ' Total: ' . $total;
            $infoPais = Country::where('name', $pais)->first();
            if ($infoPais) {
                $usuariosPorPais->push([
                    'name' => $pais . ' | ' . $mensaje . ' ' . ___('users'),
                    'coords' => [$infoPais->latitude, $infoPais->longitude],
                    'status' => 'mrk',
                    'code' => $infoPais->code,
                ]);
            }
        }

        return $usuariosPorPais;
    }

    public static function mapaGeneralUsuariosPorPais()
    {
        $paises = Country::orderBy('users', 'DESC')->get();
        $usuariosPorPais = collect();
        foreach ($paises as $pais) {
            $usuariosPorPais->push([
                'name' => $pais->name . ' : ' . $pais->users . ' ' . ___('users'),
                'coords' => [$pais->latitude, $pais->longitude],
                'status' => 'mrk',
            ]);
        }
        $jsonPaises = $usuariosPorPais;

        return $jsonPaises;
    }

    public static function controlarTipoUsuario($idUsuario)
    {
        $usuario = User::findOrFail($idUsuario);
        $tiposUsuario = Menu::orderBy('meta', 'desc')->get();
        $totalTokens = $usuario->transactions->where('status', 'approved')->where('tnx_type', 'purchase')->sum('total_tokens');
        if ($totalTokens > 0) {
            foreach ($tiposUsuario as $tipo) {
                if ($totalTokens > $tipo->meta) {
                    if ($usuario->type_user != $tipo->meta) {
                        $usuario->type_user = $tipo->id;
                        $usuario->save();

                        return 'El usuario cambio al nivel ' . $usuario->type_user;
                    } else {
                        return 'El usuario no sufrio cambios';
                    }
                    break;
                }
            }
        } else {
            return 'el usuario no tiene transacciones';
        }
    }

    public static function paisesCantidadUsuarios()
    {
        $paises = Country::orderBy('users', 'DESC')->get();
        $arrayNomPais = [];
        $arrayUserPais = [];
        foreach ($paises as $pais) {
            if ($pais->name != 'United States of America') {
                array_push($arrayNomPais, $pais->name);
            } else {
                array_push($arrayNomPais, 'USA');
            }
            array_push($arrayUserPais, $pais->users);
        }

        return [$arrayNomPais, $arrayUserPais];
    }

    public static function tiposCantidadUsuarios()
    {
        $tipos = ['Tipo 0', 'Tipo 1', 'Tipo 2', 'Tipo 3', 'Tipo 4', 'Tipo 5', 'Tipo 6', 'Tipo 7', 'Tipo 8', 'Tipo 9', 'Tipo10'];
        $levels = LevelBonusDollar::orderBy('type_user')->get();
        $cant_levels = $levels->groupBy('type_user')->count();
        $typeUsers = collect();
        $cant_user_type = [];
        $total = User::where('role', 'user')->count();
        for ($i = 1; $i < $cant_levels + 1; $i++) {
            $cant_typ = User::where('type_user', $i)->where('role', 'user')->count();
            $typeUsers->push([
                'user_type' => $i,
                'cantidad_type' => $cant_typ,
            ]);
            array_push($cant_user_type, $cant_typ);
        }

        return [$tipos, $cant_user_type];
    }

    public static function arrayNotifySettings()
    {
        return [
            'asociados' => true,
            'ganancias' => true,
            'noticias' => true,
            'informacion' => true,
            'whatsapp' => true,
            'correos' => true,
        ];
    }

    public static function ciudadesPopulares()
    {
        $ciudades = User::select('city', 'country')->where('city', '!=', 'null')->get();
        $totalcity = $ciudades->countBy('city');
        $arrayRegionesPorCiudades = collect();
        foreach ($totalcity as $city => $cantidad) {
            $user = User::where('city', $city)->first();
            if ($user) {
                $flag = Country::where('name', $user->country)->pluck('flag');
                $region = User::where('city', $city)->pluck('region');
                $totalRegion = $region->countBy();
                $arrayRegionesPorCiudades->push([
                    'city' => $city,
                    'region' => $totalRegion,
                    'cantidad' => $cantidad,
                    'flag' => $flag[0],
                ]);
            }
        }

        $arrayRegionesPorCiudades = $arrayRegionesPorCiudades->sortBy([
            ['cantidad', 'desc'],

        ]);
        $arrayNomCiudad = [];
        $arrayCantCiudad = [];
        $cont = 0;
        foreach ($arrayRegionesPorCiudades as $key) {
            $cont++;
            if ($cont <= 20) {
                array_push($arrayNomCiudad, $key['city']);
                array_push($arrayCantCiudad, $key['cantidad']);
            } else {
                break;
            }
        }

        return [$arrayRegionesPorCiudades, $arrayNomCiudad, $arrayCantCiudad];
    }

    public static function cantidadPaquetesVendidos()
    {
        $paquetes = Transaction::where('status', 'approved')->pluck('package');
        if (isset($paquetes)) {
            $array = $paquetes->countBy();

            return $array;
        } else {
            return null;
        }
    }

    public static function getColorTipoUsuario()
    {
        $niveles = LevelBonusDollar::select('type_user')->groupBy('type_user')->get();
        $array = [];
        $arrayColores = [
            'morado' => ['style-purple', '#452c97', '#452c97'],
            'celeste' => ['style-blue', '#2c80ff', '#2c80ff'],
            'verde' => ['style-green', '#21a184', '#21a184'],
            'azul' => ['style', '#334574', '#334574'],
            'rojo' => ['style-coral', '#ce2e2e', '#ce2e2e'],
            'negro' => ['style-black', '#000', '#000'],
        ];
        $colores = [
            '0' => [$arrayColores['morado']],
            '1' => [$arrayColores['morado'], $arrayColores['celeste']],
            '2' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde']],
            '3' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul']],
            '4' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo']],
            '5' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
            '6' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
            '7' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
            '8' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
            '9' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
            '10' => [$arrayColores['morado'], $arrayColores['celeste'], $arrayColores['verde'], $arrayColores['azul'], $arrayColores['rojo'], $arrayColores['negro']],
        ];
        $i = 0;
        foreach ($niveles as $key) {
            $valor = $colores[$i];
            array_push($array, $valor);
            $i += 1;
        }
        // array_unshift($array, 1);
        // unset($array[0]);
        $coloresUser = $array[auth()->user()->type_user];

        return $coloresUser;
    }

    public static function getIconGiftUserReferrals($user_gift_id)
    {
        switch ($user_gift_id) {
            case 1:
                $iconoGift = 'fas fa-check';
                break;
            case 2:
                $iconoGift = 'fas fa-award';
                break;
            case 3:
                $iconoGift = 'fas fa-star';
                break;
            case 4:
                $iconoGift = 'fas fa-medal';
                break;
            case 5:
                $iconoGift = 'fas fa-trophy';
                break;
            case 6:
                $iconoGift = 'fas fa-crown';
                break;
        }
        $nombreGift = RegaloType::where('id', $user_gift_id)->first();

        return [$iconoGift, $nombreGift->name];
    }

    public static function informePaquetesVendidos()
    {
        $transactions = Transaction::where([['tnx_type', 'purchase'], ['status', 'approved'], ['notify_sended', true]])->get();
        $array = [];
        foreach ($transactions->groupBy('package')->sortBy('id') as $key => $value) {
            $array[$key] = $value->count();
        }

        return $array;
    }

    public static function informePaquetesTotales($array)
    {
        $paquetes = Paquete::where('estado', 1)->get();
        $array2 = [];
        foreach ($paquetes as $pack) {
            if (isset($array[$pack->id])) {
                $array2[$pack->id] = $pack->stock + $array[$pack->id];
            } else {
                $array2[$pack->id] = $pack->stock;
            }
        }

        return $array2;
    }

    public static function transaccionesSinNftMayora1($usuario)
    {
        return Transaction::select('id')->where([['user', $usuario], ['status', 'approved'], ['tnx_type', 'purchase'], ['nft', null], ['package', '>', 1]])->count();
    }

    public static function transaccionesSinNft1($usuario)
    {
        return Transaction::select('id')->where([['user', $usuario], ['status', 'approved'], ['tnx_type', 'purchase'], ['nft', null], ['package', 1]])->count();
    }

    public static function transaccionesConNft($usuario)
    {
        return Transaction::select('id')->where([['user', $usuario], ['status', 'approved'], ['tnx_type', 'purchase'], ['nft', '!=', null]])->count();
    }

    public static function getTextToImageAiImage($prompt, $cantidad, $ai, $modelo = ' ')
    {
        if ($ai == 'stability') {
            try {
                $modelo = 'stable-diffusion-xl-beta-v2-2-2';
                $client = new Client();
                $url = 'https://api.stability.ai/v1/generation/' . $modelo . '/text-to-image';
                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer sk-0FLk0Kfmz2eB33snhVULWaREThNEebMYFpDyKUkaJWujZ4D8',
                    'Accept' => 'application/json',
                ];
                $body = [
                    'cfg_scale' => 7,
                    'clip_guidance_preset' => 'FAST_BLUE',
                    'height' => 512,
                    'width' => 512,
                    'sampler' => 'K_DPM_2_ANCESTRAL',
                    'samples' => $cantidad,
                    'steps' => 75,
                    'text_prompts' => [
                        [
                            'text' => $prompt,
                            'weight' => 1,
                        ],
                    ],
                ];
                $response = $client->post($url, [
                    'headers' => $headers,
                    'json' => $body,
                ]);
                $response = json_decode($response->getBody()->getContents())->artifacts;

                return $response;
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'Stability Error ',
                    'log' => $th->getMessage(),
                ]);

                return $th->getMessage();
            }
        } elseif ('dall-e') {
            try {
                $open_ai = new OpenAi(env('OPEN_AI_KEY', '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'));
                $response = $open_ai->image([
                    'prompt' => $prompt,
                    'n' => $cantidad,
                    //número de imágenes
                    'size' => '512x512',
                    //dimensiones de la imagen
                    'response_format' => 'b64_json', //use "url" for less credit usage
                ]);
                $response = json_decode($response);

                return $response;
            } catch (\Throwable $th) {
                LogsGosen::create([
                    'titulo' => 'Dall-e Error ',
                    'log' => $th->getMessage(),
                ]);

                return $th->getMessage();
            }
        }
    }

    public static function fechaFormateada(int $level, $fecha = null)
    {
        $formato1 = [
            'en' => 'dddd D \d\e MMMM',
            'es' => 'dddd D \d\e MMMM',
            'pr' => 'dddd D \d\e MMMM',
        ];
        $formato2 = [
            'en' => 'dddd D \d\e MMMM \d\e\l Y',
            'es' => 'dddd D \d\e MMMM \d\e\l Y',
            'pr' => 'dddd D \d\e MMMM \d\e\l Y',
        ];
        $formato3 = [
            'en' => 'D \d\e MMMM',
            'es' => 'D \d\e MMMM',
            'pr' => 'D \d\e MMMM',
        ];
        $formato4 = [
            'en' => 'MMMM D \,  Y',
            'es' => 'D \d\e MMMM \d\e\l Y',
            'pr' => 'MMMM D \,  Y',
        ];
        $formato5 = [
            'en' => 'dddd D \d\e MMMM \d\e\l Y',
            'es' => 'dddd D \d\e MMMM \d\e\l Y',
            'pr' => 'dddd D \d\e MMMM \d\e\l Y',
        ];
        switch ($level) {
            case 1:
                $formato = 'dddd D';
                break;
            case 2:
                $formato = $formato1[auth()->user()->lang];
                break;
            case 3:
                $formato = $formato2[auth()->user()->lang];
                break;
            case 4:
                $formato = $formato3[auth()->user()->lang];
                break;
            case 5:
                $formato = $formato4[auth()->user()->lang];
                break;
            default:
                $formato = $formato5[auth()->user()->lang];
                break;
        }
        if ($fecha != null) {
            return ucfirst(Carbon::parse($fecha)->locale(auth()->user()->lang)->isoFormat($formato));
        } else {
            return ucfirst(Carbon::now()->locale(auth()->user()->lang)->isoFormat($formato));
        }
    }

    public static function iniciofindesemana($dia = null)
    {
        if ($dia == null) {
            $fecha = Carbon::now();
            $inicioSemana = $fecha->startOfWeek(Carbon::MONDAY)->format('Y-m-d H:i:s');
            $finSemana = $fecha->endOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i:s');
        } else {
            $hoy = Carbon::now()->startOfWeek(); // Obtener el inicio de la semana actual
            // Asegurarse de que el día de la semana esté dentro del rango válido
            if ($dia < 1 || $dia > 7) {
                throw new InvalidArgumentException('El día de la semana debe estar entre 1 y 7.');
            }
            // Agregar el número de días necesarios para llegar al día de la semana deseado
            $fecha = $hoy->copy()->addDays($dia - 1);
            $finSemana = $fecha->format('Y-m-d') . ' 12:59:59';
            $inicioSemana = $fecha->subDays(6)->format('Y-m-d H:i:s');
        }

        return [
            $inicioSemana,
            $finSemana,
        ];
    }

    public static function validar(array $array)
    {
        $arrayItems = [];
        $arrayValidacion = [];
        $mensajesPersonalizados = [];
        foreach ($array as $key => $val) {
            $arrayItems[$key] = $val[0];
            $arrayValidacion[$key] = $val[1];
            if (isset($val[2])) {
                foreach ($val[2] as $llave => $regla) {
                    $mensajesPersonalizados[$llave] = $regla;
                }
            }
        }
        $validator = Validator::make($arrayItems, $arrayValidacion, $mensajesPersonalizados);
        if ($validator->fails()) {
            // dd($validator);
            foreach ($validator->errors()->messages() as $key => $mensaje) {
                return $mensaje;
            }
        } else {
            return null;
        }
    }

    public static function filtrarCaracteres($string)
    {
        $specialCharacters = '/[^a-zA-Z0-9_]+/';
        $replacement = ' ';
        $result = preg_replace($specialCharacters, $replacement, $string);
        $result = explode(' ', $result);

        return $result;
    }

    public static function notification_email_active($user)
    {
        $respuesta = '';
        $usuario = User::find($user);
        if ($usuario->notifications_setting == null) {
            $notificaciones = GosenHelper::arrayNotifySettings();
            $usuario->notifications_setting = json_encode($notificaciones);
            $usuario->save();
            $activeMail = (array) json_decode($usuario->notifications_setting);
            if (array_key_exists('correos', $activeMail)) {
                if ($activeMail['correos'] == true) {
                    $respuesta = true;
                } else {
                    $respuesta = false;
                }
            }
        } else {
            $activeMail = (array) json_decode($usuario->notifications_setting);
            if (array_key_exists('correos', $activeMail)) {
                if ($activeMail['correos'] == true) {
                    $respuesta = true;
                } else {
                    $respuesta = false;
                }
            } elseif (!array_key_exists('correos', $activeMail)) {
                $notificacionesArray = GosenHelper::arrayNotifySettings();
                $notificaciones = json_decode($usuario->notifications_setting, true);
                $keys_diff = array_diff_key($notificacionesArray, $notificaciones);
                foreach ($keys_diff as $key => $value) {
                    $notificaciones[$key] = $value;
                }
                $usuario->notifications_setting = $notificaciones;
                $usuario->save();
                if (array_key_exists('correos', $usuario->notifications_setting)) {
                    if ($usuario->notifications_setting['correos'] == true) {
                        $respuesta = true;
                    } else {
                        $respuesta = false;
                    }
                }
            }
        }

        return $respuesta;
    }

    public static function userConversationWhatsappNumberExist($number)
    {
        $user = '';
        $numeroDisruptive = env('NUMERO_WHATSAPP_BUSINESS');
        if ($numeroDisruptive == $number) {
            $user = 'disruptive';
        } else {
            $usuario = User::select('id', 'name', 'lang', 'country', 'foto', 'email', 'mobile')->where('mobile', $number)->first();
            if ($usuario) {
                $user = $usuario;
            } else {
                $user = null;
            }
        }

        return $user;
    }
    public static function getDataTimeFromPackage($dia_compra, $meses)
    {
        $inputDate = $dia_compra;
        $months = $meses;
        $startDate = Carbon::parse($inputDate)->startOfDay();
        $today = Carbon::now()->startOfDay();
        $daysConversion = $months * 30;
        $daysPassed = $startDate->diffInDays($today);
        $daysRemaining = max($daysConversion - $daysPassed, 0);
        $monthsPassed = (int) floor($daysPassed / 30);
        $monthsRemaining = max($months - $monthsPassed, 0);
        if ($daysRemaining === 0) {
            $daysResult = "0/0";
        } else {
            $daysResult = "{$daysPassed}/{$daysConversion}";
        }
        if ($monthsRemaining === 0) {
            $monthsResult = "0/0";
        } else {
            $monthsResult = "{$monthsPassed}/{$months}";
        }
        $results = [
            'days' => $daysResult,
            'months' => $monthsResult,
        ];
        return $results;
    }
    public static function amountTokensBonusPackageFromTransaction($trans)
    {
        $amount = Transaction::select('id')->where([['tnx_type', 'bonus_package'], ['user', auth()->user()->id], ['details', 'Transaction belongs to tnx_id:' . $trans]])->sum('total_tokens');
        return $amount;
    }
}
