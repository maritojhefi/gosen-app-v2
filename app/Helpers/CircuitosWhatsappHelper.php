<?php

namespace App\Helpers;

use App\Models\LogsGosen;
use App\Models\WhatsappConversacione;

class CircuitosWhatsappHelper
{
    public static function circuitoMenuWhatsapp($json)
    {
        try {
            if (isset($json->message->from)) {
                $cuerpo = $json->message->content;
                $tipo = $json->message->type; //('text','image','video','location','whatsappSticker','audio')
                $contenido = '';
                if (isset($json->message->content->text)) {
                    $contenido = $json->message->content->text;
                }
                $idConversacion = $json->conversation->id;
                $desdeUsuario = $json->message->from ? true : false; //si es que es true es que el mensaje vino de un usuario, false si vino desde api
                $destino = $json->message->to;
                $numeroOrigen = $json->message->from;
                $estadoMensaje = $json->message->status; //si es pending algo genero error, received es correcto
                preg_match_all('!\d+!', $contenido, $matches); //matches es un array que obtiene numeros dentro del cuerpo del mensaje recibido
                WhatsappConversacione::create([
                    'conversacion_id' => $idConversacion,
                    'origen' => $desdeUsuario ? $numeroOrigen : env('NUMERO_WHATSAPP_BUSINESS'),
                    'destino' => $destino,
                    'mensaje' => json_encode($cuerpo),
                ]);
            }
        } catch (\Throwable $th) {
            LogsGosen::create([
                'titulo' => 'Error linea '.$th->getLine(),
                'log' => $th->getMessage(),
            ]);
        }
    }

    public static function crearSupportReply($tipo, $ticket, $idConversacion, $cuerpo, $isAdmin)
    {
        LogsGosen::create([
            'titulo' => 'Mensaje whatsapp recibido desde webhook tipo: '.$tipo,
            'log' => '',
        ]);
        switch ($tipo) {
            case 'image':

                break;
            case 'text':

                break;
            default:
                // code...
                break;
        }

        // $content = WhatsappAPIHelper::reenviarMensaje($idConversacion, $tipo, json_encode($cuerpo));
    }
}
