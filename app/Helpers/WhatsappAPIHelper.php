<?php

namespace App\Helpers;

use App\Models\WhatsappApiLog;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class WhatsappAPIHelper
{
    public static function enviarTemplateMultimedia(string $nombreTemplate, array $parametros, string $linkMultimedia, string $tipo, $destinatario, string $idioma)
    {
        $cliente = new Client();
        $arrayParametros = '';
        if ($parametros) {
            foreach ($parametros as $parametro) {
                $arrayParametros = $arrayParametros.'{"type": "text","text":"'.$parametro.'"},';
            }
            $arrayParametros = substr($arrayParametros, 0, -1);
        }

        $respuesta = $cliente->request('POST', 'https://conversations.messagebird.com/v1/conversations/start', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => '{
                "to": "'.$destinatario.'",
                "type": "hsm",
                "channelId": "'.env('MESSAGEBIRD_CHANNEL').'",
                "content": {
                    "hsm": {
                        "namespace": "'.env('MESSAGEBIRD_NAMESPACE').'",
                        "templateName": "'.$nombreTemplate.'",
                        "language": {
                            "policy": "deterministic",
                            "code": "'.$idioma.'"
                        },
                        "components": [
                            {
                                "type": "header",
                                "parameters": [
                                    {
                                        "type": "'.$tipo.'",
                                        "image": {
                                            "url": "'.$linkMultimedia.'"
                                        }
                                    }
                                ]
                            },
                            {
                                "type": "body",
                                "parameters": [
                                    '.$arrayParametros.'
                                ]
                            }
                        ]
                    }
                }
            }',

        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());

        //WhatsappAPIHelper::historialConversacion($devolucion->id);
        //dd($devolucion);
        return $devolucion;
    }

    public static function reenviarMensaje($idConversacion, $tipo, $cuerpo)
    {
        $jsonCuerpo = json_decode($cuerpo);

        if (isset($jsonCuerpo->video)) {
            $mime = 'video/mp4';
            $format = '.mp4';
            $urlContenido = $jsonCuerpo->video->url;
            $urlPublica = WhatsappAPIHelper::uploadFileS3($urlContenido, $format, $mime);
            $jsonCuerpo->video->url = $urlPublica;
        }
        if (isset($jsonCuerpo->image)) {
            $mime = 'img/png';
            $format = '.png';
            $urlContenido = $jsonCuerpo->image->url;
            $urlPublica = WhatsappAPIHelper::uploadFileS3($urlContenido, $format, $mime);
            $jsonCuerpo->image->url = $urlPublica;
        }
        if (isset($jsonCuerpo->audio)) {
            $mime = 'audio/aac';
            $format = '.mp3';
            $urlContenido = $jsonCuerpo->audio->url;
            $urlPublica = WhatsappAPIHelper::uploadFileS3($urlContenido, $format, $mime);
            $jsonCuerpo->audio->url = $urlPublica;
        }
        $cuerpo = json_encode($jsonCuerpo);
        WhatsappApiLog::create([
            'titulo' => 'Es un whatsapp multimedia tipo '.$tipo,
            'log' => $cuerpo,
        ]);
        $string = '{
            "type": "'.$tipo.'",
            "content": '.$cuerpo.'
            }';
        $cliente = new Client();
        $respuesta = $cliente->request('POST', 'https://conversations.messagebird.com/v1/conversations/'.$idConversacion.'/messages', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => $string,
        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());
        WhatsappApiLog::create([
            'titulo' => 'Log devolucion api '.$tipo,
            'log' => json_encode($devolucion),
        ]);

        return $cuerpo;
    }

    public static function uploadFile($urlContenido, $format, $mime, string $disk = 'public_path')
    {
        $cliente = new Client();
        $respuesta = $cliente->request('GET', ''.$urlContenido.'', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],

        ]);
        $body = $respuesta->getBody()->getContents();
        $path = 'uploads/tickets/'.uniqid().$format;
        //$base64 = base64_encode($body);
        //$file = ('data:' . $mime . ';base64,' . $base64);
        $respuesta = Storage::disk($disk)->put($path, $body, 'public');
        // $urlPublica = Storage::disk($disk)->url($path);
        return $path;
    }

    public static function uploadFileS3($urlContenido, $format, $mime)
    {
        $cliente = new Client();
        $respuesta = $cliente->request('GET', ''.$urlContenido.'', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],

        ]);
        $body = $respuesta->getBody()->getContents();
        $path = 'whatsapp/'.uniqid().$format;
        //$base64 = base64_encode($body);
        //$file = ('data:' . $mime . ';base64,' . $base64);
        $respuesta = Storage::disk('s3')->put($path, $body, 'public');
        $urlPublica = Storage::disk('s3')->url($path);

        return $urlPublica;
    }

    public static function timeago($date)
    {
        $timestamp = strtotime($date);

        $strTime = ['segundo', 'minuto', 'hora', 'dia', 'mes', 'año'];
        $length = ['60', '60', '24', '30', '12', '10'];

        $currentTime = time();
        if ($currentTime >= $timestamp) {
            $diff = time() - $timestamp;
            for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                $diff = $diff / $length[$i];
            }

            $diff = round($diff);

            return 'Hace '.$diff.' '.$strTime[$i].'(s)';
        }
    }

    public static function enviarTemplate(string $nombreTemplate, array $parametros, $destinatario, string $idioma)
    {
        $cliente = new Client();
        $arrayParametros = '';
        if ($parametros) {
            foreach ($parametros as $parametro) {
                $arrayParametros = $arrayParametros.'{"default":"'.$parametro.'"},';
            }
            $arrayParametros = substr($arrayParametros, 0, -1);
        }

        $respuesta = $cliente->request('POST', 'https://conversations.messagebird.com/v1/conversations/start', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => '{
                "to": "'.$destinatario.'",
                "type": "hsm",
                
                "channelId": "'.env('MESSAGEBIRD_CHANNEL').'",
                "content":{
                  "hsm": {
                    "namespace": "'.env('MESSAGEBIRD_NAMESPACE').'",
                    "templateName": "'.$nombreTemplate.'",
                    "language": {
                    "policy": "deterministic",
                    "code": "'.$idioma.'"
                    },
                    "params": ['.$arrayParametros.'
                    ]
                  }
                }
              }',

        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());
        //WhatsappAPIHelper::historialConversacion($devolucion->id);
        //dd($devolucion);
        return $devolucion;
    }

    public static function enviarTemplateButton(string $nombreTemplate, array $parametros, $destinatario, string $idioma, array $body = null)
    {
        $stringTotal = '';
        $stringBody = '';
        if ($parametros) {
            foreach ($parametros as $parametro) {
                $arrayParametros = json_decode('{
                    "type": "button",
                    "sub_type": "url",
                    "parameters": [
                        {
                            "type": "text",
                            "text": "login"
                        }
                    ]
                }');
                $arrayParametros->parameters[0]->text = $parametro;
                $stringTotal = $stringTotal.json_encode($arrayParametros).',';
            }
            $stringTotal = substr($stringTotal, 0, -1);
        }
        if ($body) {
            foreach ($body as $texto) {
                $bodyArmado = json_decode('{
                        "type": "text",
                        "text": ""
                    }');

                $bodyArmado->text = $texto;
                //dd($bodyArmado);
                $stringBody = $stringBody.json_encode($bodyArmado).',';
            }
            $bodyArmado = substr($stringBody, 0, -1);
            $stringBody = '{
                "type": "body",
                "parameters": [
                    '.$bodyArmado.'
                ]
            },';
        }
        //dd($stringBody);
        $cliente = new Client();
        $respuesta = $cliente->request('POST', 'https://conversations.messagebird.com/v1/conversations/start', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => '
                {
                    "to": "'.$destinatario.'",
                    "type": "hsm",
                    "channelId": "'.env('MESSAGEBIRD_CHANNEL').'",
                    "content": {
                        "hsm": {
                            "namespace": "'.env('MESSAGEBIRD_NAMESPACE').'",
                            "templateName": "'.$nombreTemplate.'",
                            "language": {
                                "policy": "deterministic",
                                "code": "'.$idioma.'"
                            },
                            "components": [
                                '.$stringBody.$stringTotal.'
                            ]
                        }
                    }
                }
              ',

        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());
        //WhatsappAPIHelper::historialConversacion($devolucion->id);
        //dd($devolucion);

        return $devolucion;
    }

    public static function historialConversacion(string $idConversacion)
    {
        $cliente = new Client();
        $respuesta = $cliente->request('GET', 'https://conversations.messagebird.com/v1/conversations/'.$idConversacion.'/messages', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());

        return $devolucion;
        //dd($devolucion);
    }

    public static function enviarMensajePersonalizado(string $idConversacion, string $tipo, string $contenido, string $caption = '')
    {
        $cliente = new Client();
        $respuesta = $cliente->request('POST', 'https://conversations.messagebird.com/v1/conversations/'.$idConversacion.'/messages', [
            'headers' => [
                'Authorization' => 'AccessKey '.env('MESSAGEBIRD_KEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => '{
                "type": "'.$tipo.'",
                "content": '.WhatsappAPIHelper::armarBodyMensaje($tipo, $contenido, $caption).'
                }',
        ]);
        $devolucion = json_decode($respuesta->getBody()->getContents());

        return $devolucion;
        //dd(WhatsappAPIHelper::historialConversacion($idConversacion));
    }

    public static function armarBodyMensaje($tipo, $contenido, $caption)
    {
        if ($caption != '') {
            if ($tipo == 'location') {
                $contenido2 = $caption;
            } else {
                $contenido2 = '"caption": "'.$caption.'",';
            }
        } else {
            $contenido2 = '';
        }
        switch ($tipo) {
            case 'video':
                $textoContenido = '{
                    "video": {
                        '.$contenido2.'
                        "url": "'.$contenido.'"
                    }
                }';
                break;
            case 'image':
                $textoContenido = '{
                    "image": {
                        '.$contenido2.'
                        "url": "'.$contenido.'"
                    }
                }';
                break;
            case 'audio':
                $textoContenido = '{
                    "audio": {
                        "url": "'.$contenido.'"
                    }
                }';
                break;
            case 'text':
                $textoContenido = '{
                    "text": "'.$contenido.'"
                }';
                break;
            case 'location':
                $textoContenido = '{
                    "location": {
                        "latitude": '.$contenido.',
                        "longitude": '.$contenido2.'
                    }
                }';
                break;
            case 'whatsappSticker':
                $textoContenido = '{
                    "whatsappSticker": {
                        "link": '.$contenido.'"
                    }
                }';
                break;
            default:
                $textoContenido = '{
                "text": "'.$contenido.'"
            }';
                break;
        }

        return $textoContenido;
    }
}
