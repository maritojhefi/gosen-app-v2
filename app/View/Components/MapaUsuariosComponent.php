<?php

namespace App\View\Components;

use App\Helpers\GosenHelper;
use App\Models\Country;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class MapaUsuariosComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user = Auth::user();
        $paises = Country::orderBy('users', 'DESC')->get();

        $jsonPaises = GosenHelper::mapaGeneralUsuariosPorPais();

        // dd((string)$usuariosPorPais);
        $asociados = User::where('referral', $user->id)->get();
        $misreferidos = collect();
        foreach ($asociados as $referral) {
            $cant_refer = User::where('referral', $referral->id)->count();
            $misreferidos->push([
                'foto' => $referral->foto,
                'asociado' => $referral->name,
                'canrefer' => $cant_refer,
            ]);
        }
        $misasociados = $misreferidos->sortByDesc('canrefer');
        // dd($misasociados);

        return view('components.mapa-usuarios-component', compact('jsonPaises', 'paises', 'misasociados'));
    }
}
