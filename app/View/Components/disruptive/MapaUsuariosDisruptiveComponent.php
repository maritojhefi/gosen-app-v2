<?php

namespace App\View\Components\Disruptive;

use App\Helpers\GosenHelper;
use Illuminate\View\Component;

class MapaUsuariosDisruptiveComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $jsonPaises = GosenHelper::mapaGeneralUsuariosPorPais();

        return view('components.disruptive.mapa-usuarios-disruptive-component', compact('jsonPaises'));
    }
}
