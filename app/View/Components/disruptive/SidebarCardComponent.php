<?php

namespace App\View\Components\Disruptive;

use App\Models\BonusReferral;
use App\Models\User;
use Illuminate\View\Component;

class SidebarCardComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $col = 'col-4';

    public function __construct($col = 'col-4')
    {
        $this->col = $col;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $tipo = BonusReferral::where('user_id', auth()->user()->id)->first();
        $referidos = User::where('referral', auth()->user()->id)->get();

        return view('components.disruptive.sidebar-card-component', compact('referidos', 'tipo'));
    }
}
