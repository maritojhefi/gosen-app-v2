<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TableComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $variable;

    public $campos;

    public $title;

    public $contador;

    public $totaltokens;

    public $totalusdamount;

    public function __construct($variable, $campos, $title, $contador, $totaltokens, $totalusdamount)
    {
        $this->variable = $variable;
        $this->campos = collect($campos);
        $this->title = $title;
        $this->contador = $contador;
        $this->totaltokens = $totaltokens;
        $this->totalusdamount = $totalusdamount;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $variable = $this->variable;
        $campos = $this->campos;
        $title = $this->title;
        $contador = $this->contador;
        $totaltokens = $this->totaltokens;
        $totalusdamount = $this->totalusdamount;

        return view('components.table-component', compact('variable', 'campos', 'title', 'contador', 'totaltokens', 'totalusdamount'));
    }
}
