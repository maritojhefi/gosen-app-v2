<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ImgRandomComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $landing;

    public function __construct($landing)
    {
        $this->landing = $landing;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $collection = collect();
        if ($this->landing == '') {
            for ($i = 1; $i <= 11; $i++) {
                $collection->push($i);
            }
            $random = $collection->random();
        } else {
            switch ($this->landing) {
                case 'd1':
                    $random = 1;
                    break;
                case 'd2':
                    $random = 2;
                    break;
                case 'd3':
                    $random = 3;
                    break;
                case 'd4':
                    $random = 4;
                    break;
                case 'd5':
                    $random = 5;
                    break;
                case 'd6':
                    $random = 6;
                    break;
                case 'd7':
                    $random = 7;
                    break;
                case 'd8':
                    $random = 8;
                    break;
                case 'd9':
                    $random = 9;
                    break;
                case 'd10':
                    $random = 10;
                    break;
                case 'd11':
                    $random = 11;
                    break;
                default:
                    $random = 1;
                    break;
            }
        }
        return view('components.img-random-component', compact('random'));
    }
}
