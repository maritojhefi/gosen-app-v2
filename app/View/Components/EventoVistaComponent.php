<?php

namespace App\View\Components;

use Illuminate\View\Component;

class EventoVistaComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $eventos = auth()->user()->eventos;

        return view('components.evento-vista-component', compact('eventos'));
    }
}
