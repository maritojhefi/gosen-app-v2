<?php

namespace App\View\Components;

use App\Models\User;
use Illuminate\View\Component;
use App\Helpers\TokenCalculate as TC;

class PackageBuyUsdWallet extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $montousd;

    public $tokens;

    public $currency;

    public function __construct($montousd, $tokens, $currency)
    {
        $this->montousd = $montousd;
        $this->tokens = $tokens;
        $this->currency = $currency;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $montousd = $this->montousd;
        $tokens = $this->tokens;
        $cur = $this->currency;
        $user = User::find(auth()->user()->id);
        $tc = new TC();
        $minimum = $tc->get_current_price('min');
        $min_token = $minimum ? $minimum : active_stage()->min_purchase;
        return view('components.package-buy-usd-wallet', compact('user', 'cur', 'montousd', 'tokens', 'min_token'));
    }
}
