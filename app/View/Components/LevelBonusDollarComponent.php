<?php

namespace App\View\Components;

use App\Models\LevelBonusDollar;
use Illuminate\View\Component;

class LevelBonusDollarComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $tiposUser = LevelBonusDollar::select('type_user')->groupBy('type_user')->get();
        $bonuslvl0 = LevelBonusDollar::where('type_user', 0)->get();
        $bonuslvl1 = LevelBonusDollar::where('type_user', 1)->get();
        $bonuslvl2 = LevelBonusDollar::where('type_user', 2)->get();
        $bonuslvl3 = LevelBonusDollar::where('type_user', 3)->get();
        $bonuslvl4 = LevelBonusDollar::where('type_user', 4)->get();
        $bonuslvl5 = LevelBonusDollar::where('type_user', 5)->get();
        $bonuslvl6 = LevelBonusDollar::where('type_user', 6)->get();
        $bonuslvl7 = LevelBonusDollar::where('type_user', 7)->get();
        $bonuslvl8 = LevelBonusDollar::where('type_user', 8)->get();
        $bonuslvl9 = LevelBonusDollar::where('type_user', 9)->get();
        $bonuslvl10 = LevelBonusDollar::where('type_user', 10)->get();
        return view('components.level-bonus-dollar-component', compact('bonuslvl0', 'bonuslvl1', 'bonuslvl2', 'bonuslvl3', 'bonuslvl4', 'bonuslvl5', 'bonuslvl6', 'bonuslvl7', 'bonuslvl8', 'bonuslvl9', 'bonuslvl10', 'tiposUser'));
    }
}
