<?php

namespace App\View\Components\Dis;

use App\Helpers\GosenHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class MapaUsuariosComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user = Auth::user();
        $jsonPaises = GosenHelper::referidosPorPaisYNivel($user->id);

        return view('components.dis.mapa-usuarios-component', compact('jsonPaises'));
    }
}
