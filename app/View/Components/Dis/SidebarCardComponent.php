<?php

namespace App\View\Components\Dis;

use App\Models\BonusReferral;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\View\Component;

class SidebarCardComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $col = 'col-4';

    public $paquete;

    public function __construct($col = 'col-4', $paquete = null)
    {
        $this->paquete = $paquete;
        $this->col = $col;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $tipo = BonusReferral::where('user_id', auth()->user()->id)->first();
        $referidos = User::where('referral', auth()->user()->id)->get();
        $paquete = Transaction::where('user', auth()->user()->id)->where('tnx_type', 'purchase')->where('package', '!=', null)->where('status', 'approved')->orderBy('tnx_time', 'desc')->get();

        return view('components.dis.sidebar-card-component', compact('referidos', 'tipo', 'paquete'));
    }
}
