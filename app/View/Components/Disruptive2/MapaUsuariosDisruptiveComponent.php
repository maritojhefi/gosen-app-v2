<?php

namespace App\View\Components\Disruptive;

use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class MapaUsuariosDisruptiveComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user = Auth::user();
        $paises = Country::orderBy('users', 'DESC')->get();
        $usuariosPorPais = collect();
        foreach ($paises as $pais) {
            $usuariosPorPais->push([
                'name' => $pais->name.' : '.$pais->users.' '.___('users'),
                'coords' => [$pais->latitude, $pais->longitude],
                'status' => 'mrk',
            ]);
        }
        $jsonPaises = (string) json_encode($usuariosPorPais);
        // dd((string)$usuariosPorPais);
        return view('components.disruptive.mapa-usuarios-disruptive-component', compact('jsonPaises'));
    }
}
