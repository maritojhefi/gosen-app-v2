<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            [1, 'Thank you for your interest to our [[site_name]]', 'Welcome block', 'home_top', 'home-top-block', null, null, 1, 'You can contribute [[token_symbol]] token go through Buy Token page. \n\n You can get a quick response to any questions, and chat with the project in our Telegram: https://t.me/icocrypto \n\n<strong>Don’t hesitate to invite your friends!</strong> \n\n[[whitepaper_download_button]]', null, 'active', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [2, 'How to buy?', 'How to buy?', 'how_buy', 'how-to-buy', null, null, 1, 'Login with your email and password then go to Buy Token!', null, 'active', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [3, 'FAQ ', 'FAQ ', 'faq', 'faq', null, null, 1, 'Frequently Ask Questions...', null, 'active', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [4, 'Privacy and Policy', 'Privacy and Policy', 'privacy', 'privacy-policy', null, null, 1, '[[site_name]] Privacy and Policies...', null, 'active', 'en', 1, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [5, 'Terms and Condition', 'Terms and Condition', 'terms', 'terms-and-condition', null, null, 1, '[[site_name]] Terms and Condition...', null, 'active', 'en', 1, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],                [6, 'ICO Distribution', 'ICO Distribution', 'distribution', 'ico-distribution', null, null, 1, 'Distribution page content', null, 'hide', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [7, 'Referral', 'Referral', 'referral', 'referral', null, null, 1, 'Invite your friends and family and receive free tokens. \nThe referral link may be used during a token contribution, in the pre-sale and the ICO. \n\nImagine giving your unique referral link to your crypto-friend and he or she contributes tokens using your link, the bonus will be sent to your account automatically. The strategy is simple: the more links you send to your colleagues, family and friends - the more tokens you may earn!', null, 'hide', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
            [8, 'Custom Page', 'Custom Page', 'custom_page', 'custom-page', null, null, 1, 'Details about the page!', null, 'hide', 'en', 0, '2022-04-20 21:50:43', '2022-04-20 21:50:43'],
        ];

        foreach ($items as $page) {
            $siExiste = Page::where('title', $page[1])->first();
            if (! $siExiste) {
                Page::create([
                    'title' => $page[1],
                    'menu_title' => $page[2],
                    'slug' => $page[3],
                    'custom_slug' => $page[4],
                    'meta_keyword' => $page[5],
                    'meta_description' => $page[6],
                    'meta_index' => $page[7],
                    'description' => $page[8],
                    'external_link' => $page[9],
                    'status' => $page[10],
                    'lang' => $page[11],
                    'public' => $page[12],
                ]);
            }
        }
    }
}
