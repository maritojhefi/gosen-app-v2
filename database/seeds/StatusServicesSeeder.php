<?php

namespace Database\Seeders;

use App\Models\StatusService;
use Illuminate\Database\Seeder;

class StatusServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servicios = StatusService::SERVICIOS;
        foreach ($servicios as $servicio => $valores) {
            $siExiste = StatusService::where('service', $servicio)->first();
            if (! $siExiste) {
                StatusService::create([
                    'service' => $servicio,
                    'url_base' => $valores[0],
                    'uri' => $valores[1],
                ]);
            }
        }
    }
}
