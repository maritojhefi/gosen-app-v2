<?php

namespace Database\Seeders;

use App\Models\RegaloType;
use Illuminate\Database\Seeder;

class RegaloTiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, 'Beginner', 10, 2, 'active', 'nota', '2021-07-18 08:00:00', '2022-03-02 02:09:03'],
            [2, 'Basic', 100, 7, 'active', '', '2021-07-18 16:25:00', '2021-07-31 22:22:37'],
            [3, 'Advanced', 300, 31, 'active', '...', '2021-07-18 08:00:00', '2021-08-05 22:54:51'],
            [4, 'Pro', 600, 60, 'active', 'Nota .....', '2021-07-18 08:00:00', '2021-08-02 00:54:05'],
            [5, 'Master Pro', 1000, 100, 'active', 'comentario', '2021-07-18 08:00:00', '2021-10-01 13:29:23'],
        ];

        foreach ($items as $regaloTipo) {
            $siExiste = RegaloType::where('name', $regaloTipo[1])->first();
            if (! $siExiste) {
                RegaloType::create([
                    'name' => $regaloTipo[1],
                    'total_tokens' => $regaloTipo[2],
                    'total_guests' => $regaloTipo[3],
                    'status' => $regaloTipo[4],
                    'comment' => $regaloTipo[5],
                ]);
            }
        }
    }
}
