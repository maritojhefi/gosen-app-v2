<?php

namespace Database\Seeders;

use App\Models\IcoMeta;
use Illuminate\Database\Seeder;

class IcoMetasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            [1, 1, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [2, 1, 'price_option', '{"tire_1":{"price":2,"min_purchase":10,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-05-25 13:32:43'],
            [3, 2, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [4, 2, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [5, 3, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [6, 3, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [7, 4, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [8, 4, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [9, 5, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [10, 5, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [11, 6, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [12, 6, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [13, 7, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [14, 7, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [15, 8, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [16, 8, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [17, 9, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [18, 9, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [19, 10, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [20, 10, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [21, 11, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [22, 11, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [23, 12, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [24, 12, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [25, 13, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [26, 13, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [27, 14, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [28, 14, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [29, 15, 'bonus_option', '{"base":{"amount":25,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":1},"bonus_amount":{"status":1,"tire_1":{"amount":15,"token":2500},"tire_2":{"amount":null,"token":null},"tire_3":{"amount":null,"token":null}}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
            [30, 15, 'price_option', '{"tire_1":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_2":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0},"tire_3":{"price":0,"min_purchase":0,"start_date":"2000-01-01 00:00:00","end_date":"2000-01-01 23:59:00","status":0}}', '1', '2022-04-20 21:50:42', '2022-04-20 21:50:42'],
        ];
        foreach ($items as $icoMeta) {
            $siExiste = IcoMeta::where('stage_id', $icoMeta[1])->where('option_name', $icoMeta[2])->first();
            if (! $siExiste) {
                IcoMeta::create([
                    'stage_id' => $icoMeta[1],
                    'option_name' => $icoMeta[2],
                    'option_value' => $icoMeta[3],
                    'status' => $icoMeta[4],
                ]);
            }
        }
    }
}
