<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SwitchsWithdrawSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['switch_gosen_token', 'switch_bonus_token', 'switch_bonus_usd'];
        foreach ($array as $field) {
            $siExiste = Setting::where('field', $field)->first();
            if (! $siExiste) {
                Setting::create([
                    'field' => $field,
                    'value' => true,
                ]);
            }
        }
    }
}
