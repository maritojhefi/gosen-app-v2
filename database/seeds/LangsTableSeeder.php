<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Portugués' => 'PR', 'Spanish' => 'ES', 'English' => 'EN', 'Russian' => 'RU',
            'Italian' => 'IT', 'French' => 'FR', 'Hindi' => 'HI', /*'Arab'=>'AR',*/'Turkish' => 'TR',
            'German' => 'DE', 'Chinese' => 'ZH'];
        foreach ($array as $nombre => $key) {
            $siExiste = Language::where('name', $nombre)->first();
            if (! $siExiste) {
                Language::create([
                    'name' => $nombre,
                    'label' => $nombre,
                    'short' => $key,
                    'code' => strtolower($key),
                    'status' => 1,
                ]);
            }
        }
    }
}
