<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingAuthFooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buscar = Setting::where('field', 'site_footer_auth_code')->first();
        if (! $buscar) {
            Setting::create([
                'field' => 'site_footer_auth_code',
                'value' => '',
            ]);
        }
    }
}
