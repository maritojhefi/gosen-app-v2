<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, 'manual', 'Pay via Crypto', 'You can send payment direct to our wallets. We will manually verify.', '{"eth":{"status":"inactive","address":null,"limit":null,"price":null,"num":3,"req":"no","network":"default"},"btc":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"ltc":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"bch":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"bnb":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"trx":{"status":"inactive","address":null,"num":3,"req":"no"},"xlm":{"status":"inactive","address":null,"num":3,"req":"no"},"xrp":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"usdt":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"usdc":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"dash":{"status":"inactive","address":null,"num":3,"req":"no"},"waves":{"status":"inactive","address":null,"num":3,"req":"no"},"xmr":{"status":"inactive","address":null,"num":3,"req":"no"},"busd":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"ada":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"doge":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"sol":{"status":"inactive","address":null,"num":3,"req":"no"},"uni":{"status":"inactive","address":null,"num":3,"req":"no"},"link":{"status":"inactive","address":null,"num":3,"req":"no","network":"default"},"cake":{"status":"inactive","address":null,"num":3,"req":"no"}}', 'inactive', '2022-04-20 17:50:42', '2022-04-20 17:50:42'],
            [2, 'bank', 'Pay via Bank Transfer', 'You can send payment direct to our bank account.', '{"bank_account_name":null,"bank_account_number":null,"bank_holder_address":null,"bank_name":null,"bank_address":null,"routing_number":null,"iban":null,"swift_bic":null}', 'inactive', '2022-04-20 17:50:42', '2022-04-20 17:50:42'],
            [3, 'paypal', 'Pay with PayPal', 'You can send your payment using your PayPal account.', '{"email":null,"sandbox":0,"clientId":null,"clientSecret":null,"is_active":0}', 'inactive', '2022-04-20 17:50:42', '2022-04-20 17:50:42'],
        ];

        //dd($items);
        foreach ($items as $payments) {
            $siExiste = PaymentMethod::where('payment_method', $payments[1])->first();

            if (! $siExiste) {
                // dd($payments);
                DB::table('payment_methods')->insert([

                    'payment_method' => $payments[1],
                    'title' => $payments[2],
                    'description' => $payments[3],
                    'data' => $payments[4],
                    'status' => $payments[5],
                    'created_at' => $payments[6],
                    'updated_at' => $payments[7],
                ]);
            }
        }
    }
}
