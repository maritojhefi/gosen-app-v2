<?php

namespace Database\Seeders;

use App\Models\LevelBonusDollar;
use Illuminate\Database\Seeder;

class LevelBonusDollarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, 'level1', LevelBonusDollar::TYPE2, '10', LevelBonusDollar::STATUS1, '5000'],
            [2, 'level2', LevelBonusDollar::TYPE2, '8', LevelBonusDollar::STATUS1, '10000'],
            [3, 'level3', LevelBonusDollar::TYPE2, '6', LevelBonusDollar::STATUS1, '25000'],
            [4, 'level4', LevelBonusDollar::TYPE2, '4', LevelBonusDollar::STATUS1, '50000'],
            [5, 'level5', LevelBonusDollar::TYPE2, '2', LevelBonusDollar::STATUS1, '75000'],
        ];

        for ($i=0; $i <= 10; $i++) { 
            foreach ($items as $levelBonusDollars) {
                $siExiste = LevelBonusDollar::where('name', $levelBonusDollars[1])->where('type_user', $i)->first();
                if (! $siExiste) {
                    LevelBonusDollar::create([
                        'name' => $levelBonusDollars[1],
                        'type' => $levelBonusDollars[2],
                        'value' => $levelBonusDollars[3],
                        'status' => $levelBonusDollars[4],
                        'max' => $levelBonusDollars[5],
                        'type_user' => $i,
                    ]);
                }
            }
        }
    }
}
