<?php

namespace Database\Seeders;

use App\Models\Paquete;
use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, '200', 20],
            [2, '300', 30],
            [3, '400', 40],
            [4, '500', 50],
            [5, '600', 60],
            [6, '700', 70],
            [7, '800', 80],
            [8, '900', 90],
            [9, '1000', 100],
        ];
        foreach ($items as $paq) {
            $siExiste = Paquete::where('posicion', $paq[0])->first();
            if (! $siExiste) {
                Paquete::create([
                    'tokens' => $paq[1],
                    'nombre' => 'Paquete '.$paq[0],
                    'posicion' => $paq[0],
                    'valor_usd' => $paq[2],
                ]);
            }
        }
    }
}
