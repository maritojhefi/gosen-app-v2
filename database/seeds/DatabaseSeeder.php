<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LangsTableSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(EmailTemplatesSeeder::class);
        $this->call(IcoMetasSeeder::class);
        $this->call(IcoStagesSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(RegaloTiposSeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(LevelBonusDollarsSeeder::class);
        $this->call(SwitchsWithdrawSeeder::class);
        $this->call(ProgresoSeeder::class);
    }
}
