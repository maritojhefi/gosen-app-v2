<?php

namespace Database\Seeders;

use App\Models\MainPage;
use Illuminate\Database\Seeder;

class MainPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, 'Dashboard', true],
            [2, 'Package', true],
            [3, 'Referral', true],
            [4, 'Bonus', true],
            [5, 'Cooperativa Publicitaria', true],
            [6, 'F101', true],
            [7, 'Top Referrals', true],
            [8, 'Maps', true],
            [9, 'FaQ', true],
            [10, 'NFT', true],
            [11, 'Panel Fase 2', true],
            [12, 'Panel Fase 3', true],
            [13, 'Panel Fase 4', true],
            [14, 'Buy Token', true],
        ];
        foreach ($items as $page) {
            $siExiste = MainPage::where('title', $page[1])->first();
            if (!$siExiste) {
                MainPage::create([
                    'title' => $page[1],
                    'status' => $page[2],
                ]);
            } else if ($siExiste->title == 'NFT') {
                $siExiste->delete();
            }
        }
    }
}
