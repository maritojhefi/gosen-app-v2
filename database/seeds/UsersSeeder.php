<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, null, 'Adrian Trigo', 'adrian.trigo@gmail.com', '2021-08-01 21:29:11', '$2y$10$NiaIMl5kUbpxDbHlrHiluuSIDlTEts1CflWY85lS.hZo9SbY9zIjy', 'active', 'Email', null, null, null, null, '2022-05-25 13:28:03', null, null, 'admin', null, null, '15', null, 0, null, 'main', 'tNqOWx5RKkHsxb6s2aPs9jiDcnU7qFnGory1NVNsVIDJqVbywXi4ooTDmfbm', '2021-08-01 21:29:12', '2022-05-25 13:28:03', 'avatar/user.png', 1, 0],
            [2, null, 'adrianceo101', 'adrian@adriantrigo.com', '2021-09-30 13:46:16', '$2y$10$ix45/1K7TwX0oWaWtg8vFe8UPIkwv/WpiIk/JDPC4EpsziTx28WgW', 'active', 'Internal', null, null, null, null, '2022-05-25 20:10:54', 'Tether USDT /TRC20', 'TBfEz4mfXfwVncJZmJukZnxKJz5p8bhLkG', 'user', 44.2, 233.1, null, null, 0, null, 'main', 'kLVcNEiVZoTNmELiW5zb9MhoQxFcbfEsZxbDalMKjQN2qV4FNUwx42NtfSWk', '2021-09-30 13:46:16', '2022-05-25 20:10:54', null, 1, 0],
            [3, null, 'Jhoanny', 'jhoanny.yamel@gmail.com', '2022-03-02 21:34:28', '$2y$10$/JfxUxoNGu0N2YkL4hpoBubgw4raxCCFkA6qA/UfIuszQX/RMuJMW', 'active', 'Email', null, '', null, 'Bolivia', '2022-03-05 18:49:29', 'ethereum', '0x4C48C0FCc4bCd0c7D947cF537CEc50f8D19a92a2', 'user', null, null, '8', '{"user":8,"name":"adrianceo101","time":[]}', 0, null, 'main', 'lydgV8DSZ5r47ZlYZWZUtO3Jeud4xCBRxd1NwUM2rPMRDE0ToXhW3r9AyJt5', '2022-03-02 21:34:12', '2022-05-18 21:29:26', 'avatar/user.png', 1, 1],
            [4, 'Rodrick555', 'Rodrigo', 'villaortiz110@gmail.com', '2022-04-29 14:29:11', '$2y$10$DVi9Daz.oRZts5fvq8ovA.sEzAlEy0G0L7uLa4T4XEdQUBjt8thPO', 'active', 'Internal', null, '75141260', '06/19/1996', 'Bolivia', '2022-05-24 21:40:11', null, null, 'user', 102.8, 514, null, null, 0, null, 'main', '4rLEy43a2IcbIyxCXBPP1eQgpd7LzcpxBu60NQVEJMOmgHK5jsZnIFJqD0sC', '2022-04-29 14:29:11', '2022-05-24 21:40:11', '1653406321.jpg', 1, 1],
        ];

        foreach ($items as $users) {
            $siExiste = User::where('email', $users[1])->first();
            if (! $siExiste) {
                User::create([
                    'username' => $users[1],
                    'name' => $users[2],
                    'email' => $users[3],
                    'email_verified_at' => $users[4],
                    'password' => $users[5],
                    'status' => $users[6],
                    'registerMethod' => $users[7],
                    'social_id' => $users[8],
                    'mobile' => $users[9],
                    'dateOfBirth' => $users[10],
                    'nationality' => $users[11],
                    'lastLogin' => $users[12],
                    'walletType' => $users[13],
                    'walletAddress' => $users[14],
                    'role' => $users[15],
                    'contributed' => $users[16],
                    'tokenBalance' => $users[17],
                    'referral' => $users[18],
                    'referralInfo' => $users[19],
                    'google2fa' => $users[20],
                    'google2fa_secret' => $users[21],
                    'type' => $users[22],
                    'remember_token' => $users[23],
                ]);
            }
        }
    }
}
