<?php

namespace Database\Seeders;

use App\Models\EmailBlocked;
use Illuminate\Database\Seeder;

class EmailBlockedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [1, '@naturalsrs.com', 'spam', 'correo quitado por spam', true],
            [2, '@icznn.com', 'spam', 'correo quitado por spam', true],
            [3, '@wuuvo.com', 'spam', 'correo quitado por spam', true],
            [4, '@qiott.com', 'spam', 'correo quitado por spam', true],
            [5, '@ezztt.com', 'spam', 'correo quitado por spam', true],
            [6, '@i.xcode.ro', 'spam', 'correo quitado por spam', true],
            [7, '@sroff.com', 'spam', 'correo quitado por spam', true],
            [8, '@t.psh.me', 'spam', 'correo quitado por spam', true],
            [9, '@kusrc.com', 'spam', 'correo quitado por spam', true],
            [10, '@o.spamtrap.ro', 'spam', 'correo quitado por spam', true],
            [11, '@kmdt.cm', 'spam', 'correo quitado por spam', true],
            [12, '@ppetw.com', 'spam', 'correo quitado por spam', true],
            [13, '@xww.ro', 'spam', 'correo quitado por spam', true],
            [14, '@kzccv.com', 'spam', 'correo quitado por spam', true],
            [15, '@onlcool.com', 'spam', 'correo quitado por spam', true],
            [16, '@vaband.com', 'spam', 'correo quitado por spam', true],
            [17, '@ozatvn.com', 'spam', 'correo quitado por spam', true],
            [18, '@soremap.com', 'spam', 'correo quitado por spam', true],
            [19, '@peogi.com', 'spam', 'correo quitado por spam', true],
            [20, '@sharklasers.com', 'spam', 'correo quitado por spam', true],
            [21, '@bbitj.com', 'spam', 'correo quitado por spam', true],
            [22, '@bbitf.com', 'spam', 'correo quitado por spam', true],
            [23, '@bbitq.com', 'spam', 'correo quitado por spam', true],
            [24, '@fullangle.org', 'spam', 'correo quitado por spam', true],
            [25, '@matra.site', 'spam', 'correo quitado por spam', true],
            [26, '@wemel.top', 'spam', 'correo quitado por spam', true],
            [27, '@memsg.top', 'spam', 'correo quitado por spam', true],
            [28, '@mywrld.site', 'spam', 'correo quitado por spam', true],
            [29, '@mybx.site', 'spam', 'correo quitado por spam', true],
            [30, '@xww.ro', 'spam', 'correo quitado por spam', true],
            [31, '@explodemail.com', 'spam', 'correo quitado por spam', true],
            [32, '@mailsac.com', 'spam', 'correo quitado por spam', true],
            [33, '@mymaily.lol', 'spam', 'correo quitado por spam', true],
            [34, '@inboxbear.com', 'spam', 'correo quitado por spam', true],
            [35, '@gufum.com', 'spam', 'correo quitado por spam', true],
            [36, '@tcwlx.com', 'spam', 'correo quitado por spam', true],
            [37, '@tcwlm.com', 'spam', 'correo quitado por spam', true],
        ];

        for ($i = 0; $i <= 37; $i++) {
            foreach ($items as $emailblocked) {
                $siExiste = EmailBlocked::where('correo', $emailblocked[1])->first();
                if (! $siExiste) {
                    EmailBlocked::create([
                        'correo' => $emailblocked[1],
                        'tipo' => $emailblocked[2],
                        'descripcion' => $emailblocked[3],
                        'estado' => $emailblocked[4],
                    ]);
                }
            }
        }
    }
}
