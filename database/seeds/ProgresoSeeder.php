<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class ProgresoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $siExiste2 = Setting::where('field', 'perfil_progreso_card_2')->first();
        $siExiste3 = Setting::where('field', 'perfil_progreso_card_3')->first();
        $siExiste4 = Setting::where('field', 'perfil_progreso_users_type')->first();
        if (!$siExiste2) {
            Setting::create(
                [
                    'field' => 'perfil_progreso_card_2',
                    'value' => false,
                ]
            );
        }
        if (!$siExiste3) {
            Setting::create(
                [
                    'field' => 'perfil_progreso_card_3',
                    'value' => false,
                ]
            );
        }
        if (!$siExiste4) {
            Setting::create(
                [
                    'field' => 'perfil_progreso_users_type',
                    'value' => '{"type_0":{"card1":true,"card2":false},"type_1":{"card1":true,"card2":false},"type_2":{"card1":false,"card2":true},"type_3":{"card1":true,"card2":true},"type_4":{"card1":true,"card2":true},"type_5":{"card1":true,"card2":true},"type_6":{"card1":true,"card2":true},"type_7":{"card1":true,"card2":true},"type_8":{"card1":true,"card2":true},"type_9":{"card1":true,"card2":true},"type_10":{"card1":false,"card2":false}}'
                ]
            );
        }
    }
}
