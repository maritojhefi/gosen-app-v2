<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::truncate();

        for ($i = 0; $i < 10; $i++) {
            Menu::create([
                'id' => $i + 1,
                'valor' => 'Tipo '.$i,
            ]);
        }
    }
}
