<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_sended_mails', function (Blueprint $table) {
            $table->boolean('to_specific_users')->after('type_user')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_sended_mails', function (Blueprint $table) {
            $table->dropColumn('to_specific_users');
        });
    }
};
