<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_whatsapp_correo', function (Blueprint $table) {
            $table->id();
            $table->integer('whatsapp_correo_id');
            $table->integer('user_id');
            $table->boolean('whatsapp_status')->default(false);
            $table->boolean('correo_status')->default(false);
            $table->longText('whatsapp_log')->nullable();
            $table->longText('correo_log')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_whatsapp_correo');
    }
};
