<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->integer('dias_bono')->unsigned()->nullable()->default(0);
            $table->float('porcentaje_bono')->unsigned()->nullable()->default(0);
            $table->boolean('estado_bono')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->dropColumn('dias_bono');
            $table->dropColumn('porcentaje_bono');
            $table->dropColumn('estado_bono');
        });
    }
};
