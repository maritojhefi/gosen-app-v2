<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_services', function (Blueprint $table) {
            $table->id();
            $table->string('service');
            $table->string('status')->nullable();
            $table->longText('log')->nullable();
            $table->string('url_base');
            $table->string('uri');
            $table->string('description')->nullable();
            $table->string('color')->default('success');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_services');
    }
};
