<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_sizes', function (Blueprint $table) {
            $table->id();
            $table->longText('name_backup')->nullable();
            $table->double('weight')->nullable();
            $table->string('top_three_heavier')->nullable();
            $table->string('top_three_lightest')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_sizes');
    }
};
