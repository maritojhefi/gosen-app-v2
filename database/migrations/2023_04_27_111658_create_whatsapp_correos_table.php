<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatsapp_correos', function (Blueprint $table) {
            $table->id();
            $table->string('header')->nullable();
            $table->longText('content');
            $table->string('footer')->nullable();
            $table->string('type_mail')->nullable();
            $table->integer('number_days')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatsapp_correos');
    }
};
