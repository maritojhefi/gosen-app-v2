<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regalos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user');
            $table->integer('referral');
            $table->integer('gift_id');
            $table->integer('gift_referral');
            $table->double('gift_token');
            $table->integer('total_referral');
            $table->string('staus');
            $table->string('tnx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regalos');
    }
};
