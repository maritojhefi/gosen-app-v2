<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_dolars', function (Blueprint $table) {
            $table->id();
            $table->integer('stage_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->double('amount');
            $table->integer('checked_by')->unsigned();
            $table->integer('type_value')->unsigned();
            $table->string('type_bonus');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_dolars');
    }
};
