<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_notificacions', function (Blueprint $table) {
            $table->id();
            $table->string('titulo')->nullable();
            $table->string('contenido')->nullable();
            $table->string('notificacion')->nullable();
            $table->dateTime('fecha_creacion')->nullable();
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_notificacions');
    }
};
