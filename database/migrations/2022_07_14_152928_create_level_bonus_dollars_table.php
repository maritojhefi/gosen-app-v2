<?php

use App\Models\LevelBonusDollar;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_bonus_dollars', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type')->default(LevelBonusDollar::TYPE2);
            $table->double('value');
            $table->string('status');
            $table->bigInteger('max');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_bonus_dollars');
    }
};
