<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('whatsapp_correos', function (Blueprint $table) {
            $table->string('slug')->nullable();
            $table->time('time')->nullable();
            $table->string('lang')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('whatsapp_correos', function (Blueprint $table) {
            $table->dropColumn('slug');
            $table->dropColumn('time');
            $table->dropColumn('lang');
        });
    }
};
