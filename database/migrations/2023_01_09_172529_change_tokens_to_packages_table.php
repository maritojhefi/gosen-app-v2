<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->string('tokens')->nullable()->change();
            $table->string('descuento')->nullable()->change();
            $table->string('valor_usd')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquetes', function (Blueprint $table) {
            $table->float('tokens')->nullable()->change();
            $table->float('descuento')->nullable()->change();
            $table->float('valor_usd')->nullable()->change();
        });
    }
};
