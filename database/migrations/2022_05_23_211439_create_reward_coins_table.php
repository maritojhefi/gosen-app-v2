<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_coins', function (Blueprint $table) {
            $table->id();
            $table->boolean('video1')->nullable()->default(false);
            $table->boolean('video2')->nullable()->default(false);
            $table->boolean('video3')->nullable()->default(false);
            $table->boolean('pdf')->nullable()->default(false);
            $table->boolean('modal')->nullable()->default(false);
            $table->integer('idUser')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_coins');
    }
};
